import pymysql
import pandas as pd
import numpy as np
from sklearn.preprocessing import Imputer
from sklearn.externals import joblib
from sklearn.preprocessing import Normalizer
from sklearn.ensemble import RandomForestClassifier
import matplotlib as mpl
import matplotlib.pyplot as plt



pathstr = ''

try:
#获取一个数据库连接，注意如果是UTF-8类型的，需要制定数据库
    conn=pymysql.connect(host='localhost',user='root',passwd='obadmin',db='boe5',port=3306,charset='utf8')
    cur=conn.cursor()#获取一个游标
    cur.execute('select *  from ani_energynum')
    col = cur.description
    results=cur.fetchall()
    df = pd.DataFrame(list(results[0]))
    df_=pd.DataFrame(list(results))
    target_value=df_.loc[:,0]
    selected_para_col = [tuple[0] for tuple in cur.description][1:]

    para_df=pd.DataFrame(list(results))
    para_df=para_df.drop(columns=[0])
    para_df.columns=selected_para_col

    cur.close()#关闭游标
    conn.close()#释放数据库资源
except  Exception :print("发生异常") 
# ------ featuer engine --------------
# define the value of NaN
imp=Imputer(missing_values='NaN', strategy='median', axis=0)
features = para_df
imp.fit(features[features.columns])
features = pd.DataFrame(imp.transform(features[features.columns]), columns=features.columns)

# save the imputer model
joblib.dump(imp, pathstr+'imputer.pkl')

#  normalize the input parameter value
scaler = Normalizer()
scaler.fit(features[selected_para_col])
joblib.dump(scaler, pathstr + 'scaler.pkl')
features[selected_para_col] = scaler.transform(features[selected_para_col])


# define a random_forest model
def random_forest_classifier(train_x, train_y):
    model=RandomForestClassifier(n_estimators=2000)

    model.fit(train_x, train_y)
    return model


# classifier the energy_consum valuse class
def _recode_class(inputvalue):
    clen = len(inputvalue)
    recode_value = np.zeros(clen)
    # class the energy consumption
    for ii in range(1, clen+1):
        if inputvalue[ii-1] < 60:    # low
            recode_value[ii-1] = 1
        elif inputvalue[ii-1] < 90:  # median
            recode_value[ii-1] = 2
        elif inputvalue[ii-1] < 100:  # high
            recode_value[ii-1] = 3
        else:
            recode_value[ii-1] = 6   # very high
    return recode_value


# ------- model training -------------
# The energey consumption can be seemed as a classification model by using different parameters
# The random-forest can by used to train a model and form a feater importance list for users

#print(target_value)
# define different class
re_class = _recode_class(target_value)
#print(features)
#print(re_class)
# define a model and fit it
rfr = random_forest_classifier(features, re_class)
# print the feautre_importance list
rfr_para_list = rfr.feature_importances_
# get the parameter_names and importance value
para_value = list(rfr_para_list)
importance_para_value = pd.Series(para_value)
importance_para_name = pd.Series(selected_para_col)
# merge the value and name
parameter_importance_df = pd.concat([importance_para_name.reset_index(drop=True),
                                     importance_para_value.reset_index(drop=True)],
                                    axis=1
                                    )
parameter_importance_df.columns = ['parameter_name', 'para_import_value']
# sorted the importance value
para_importance_sorted = parameter_importance_df.sort_values(by='para_import_value',
                                                             axis=0, ascending=False)
# redefine the index
final_param_importance = para_importance_sorted.reset_index(drop=True)
# save the result
#print(final_param_importance)
final_param_importance.to_csv('parameter_importance_value.csv', encoding='gbk')  