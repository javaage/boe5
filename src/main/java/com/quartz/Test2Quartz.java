package com.quartz;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
/**
 * 测试Quartz
 * @author： one源码
 * @date：2018-05-06
 */
public class Test2Quartz extends BaseQuartz implements Job  {

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		System.out.println("测试调度2 - 正在运行......");
	}

}
