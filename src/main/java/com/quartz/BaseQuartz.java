package com.quartz;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.transaction.Transaction;

import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.model.tool.Quarz;
import com.service.tool.QuarzService;
import com.util.QuarzUtils;

/**
 * 基础Quartz
 * 
 * @功能说明：
 * @作者： herun
 * @创建日期：2015-11-19
 * @版本号：V1.0
 */
public class BaseQuartz {
	
	@Autowired
	private QuarzService quarzService;
	
	/**
	 * 更改调度状态,且停止任务
	 */
	public void updateQuartzState(Quarz quarz, ServletContext servletContext) {
		ApplicationContext aContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		quarz.setState("1");
		quarzService.update(quarz);
		try {
			QuarzUtils.pauseJob(quarz.getJobName(), quarz.getJobGroup());
		} catch (SchedulerException e) {
			e.printStackTrace();
		}
	}

}
