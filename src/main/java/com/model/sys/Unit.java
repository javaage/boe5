package com.model.sys;

import com.model.page.PageDto;

public class Unit extends PageDto{
	int id;
	int userid;
	int unitid;
	String unit_name;
	String unit_value;
	public String getPageName() {
		return pageName;
	}
	public void setPageName(String pageName) {
		this.pageName = pageName;
	}
	String pageName;
	public String getUnit_name() {
		return unit_name;
	}
	public void setUnit_name(String unit_name) {
		this.unit_name = unit_name;
	}
	public String getUnit_value() {
		return unit_value;
	}
	public void setUnit_value(String unit_value) {
		this.unit_value = unit_value;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	public int getUnitid() {
		return unitid;
	}
	public void setUnitid(int unitid) {
		this.unitid = unitid;
	}
	
	
	
}
