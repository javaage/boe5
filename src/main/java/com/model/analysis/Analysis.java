package com.model.analysis;

import com.model.page.PageDto;

public class Analysis extends PageDto{
	String parameter_name;
	double para_import_value;

	public String getParameter_name() {
		return parameter_name;
	}
	public void setParameter_name(String parameter_name) {
		this.parameter_name = parameter_name;
	}
	public double getPara_import_value() {
		return para_import_value;
	}
	public void setPara_import_value(double para_import_value) {
		this.para_import_value = para_import_value;
	}

	
}
