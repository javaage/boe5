package com.controller.user;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.model.user.User;
import com.service.user.UserService;

@Controller
@RequestMapping("/user")
public class UserController {

	@Autowired
	private UserService userService;
	

	/**
	 * 修改密码跳转页面
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/pwd")
	public String pwd(HttpServletRequest request, Model model) {
		//获取当前用户
		User user = (User) request.getSession().getAttribute("user");
		model.addAttribute("user", user);
		return "views/user/password";
	}
	
	/**
	 * 个人信息跳转页面
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/personInfo")
	public String personInfo(HttpServletRequest request, Model model) {
		//获取当前用户
		User user = (User) request.getSession().getAttribute("user");
		model.addAttribute("user", user);
		return "views/user/personInfo";
	}
	
	
	/**
	 * 用户管理跳转
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/list")
	public String list(HttpServletRequest request, Model model) {
		//获取当前用户
		User user = (User) request.getSession().getAttribute("user");
		user = (User) userService.get(user);
		model.addAttribute("user", user);
		
		return "views/user/userList";
	}
}
