package com.controller.quota;


import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("/quota")
public class QuotaController {
	
	
	@RequestMapping(value = "/factory_quota")
	private String factory_quota(HttpServletRequest request, Model model) {
		return "views/gynh/factory_quota";
	}
	@RequestMapping(value = "/dispatcher_quota")
	public String dispaterQuota(HttpServletRequest request) {
		return "views/quota/quota_setting";
	}
	}
