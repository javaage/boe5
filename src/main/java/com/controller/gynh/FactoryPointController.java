package com.controller.gynh;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;



@Controller
@RequestMapping("/gynh")
public class FactoryPointController {

	@RequestMapping(value = "/total_point")
	private String total_point(HttpServletRequest request, Model model) {
		return "/views/gynh/points_value";
	}
	
	@RequestMapping(value = "/array_total_point")
	private String array_total_point(HttpServletRequest request, Model model) {
		model.addAttribute("factory_name", "ARRAY");
		return "/views/gynh/factory_points_value";
	}
	
	@RequestMapping(value = "/cell_total_point")
	private String cell_total_point(HttpServletRequest request, Model model) {
		model.addAttribute("factory_name", "CELL");
		return "/views/gynh/factory_points_value";
	}
	
	@RequestMapping(value = "/cf_total_point")
	private String cf_total_point(HttpServletRequest request, Model model) {
		model.addAttribute("factory_name", "CF");
		return "/views/gynh/factory_points_value";
	}
	
	@RequestMapping(value = "/mdl_total_point")
	private String mdl_total_point(HttpServletRequest request, Model model) {
		model.addAttribute("factory_name", "MDL");
		return "/views/gynh/factory_points_value";
	}
}
