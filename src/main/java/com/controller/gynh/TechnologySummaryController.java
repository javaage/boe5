package com.controller.gynh;


import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("/gynh")
public class TechnologySummaryController {
	
	
	@RequestMapping(value = "/array_technology_summary")
	public String array_technology_summary(HttpServletRequest request, Model model) {
		model.addAttribute("factory_name", "ARRAY");
		return "views/gynh/technology_summary";
	}
	@RequestMapping(value = "/cf_technology_summary")
	public String cf_technology_summary(HttpServletRequest request, Model model) {
		model.addAttribute("factory_name", "CF");
		return "views/gynh/technology_summary";
	}
	@RequestMapping(value = "/mdl_technology_summary")
	public String mdl_technology_summary(HttpServletRequest request, Model model) {
		model.addAttribute("factory_name", "MDL");
		return "views/gynh/technology_summary";
	}
	@RequestMapping(value = "/cell_technology_summary")
	public String cell_technology_summary(HttpServletRequest request, Model model) {
		model.addAttribute("factory_name", "CELL");
		return "views/gynh/technology_summary";
	}
}
