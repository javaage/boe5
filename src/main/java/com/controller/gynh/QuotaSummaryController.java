package com.controller.gynh;


import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/gynh")
public class QuotaSummaryController {
	
	
	@RequestMapping(value = "/quota_summary")
	private String factory_quota(HttpServletRequest request, Model model) {
		model.addAttribute("factory_name", "ALL");
		return "views/gynh/quota_summary";
	}
	@RequestMapping(value = "/array_quota_summary")
	private String array_quota_summary(HttpServletRequest request, Model model) {
		model.addAttribute("factory_name", "ARRAY");
		return "views/gynh/quota_summary";
	}
	@RequestMapping(value = "/cf_quota_summary")
	private String cf_quota_summary(HttpServletRequest request, Model model) {
		model.addAttribute("factory_name", "CF");
		return "views/gynh/quota_summary";
	}
	@RequestMapping(value = "/mdl_quota_summary")
	private String mdl_quota_summary(HttpServletRequest request, Model model) {
		model.addAttribute("factory_name", "MDL");
		return "views/gynh/quota_summary";
	}
	@RequestMapping(value = "/cell_quota_summary")
	private String cell_quota_summary(HttpServletRequest request, Model model) {
		model.addAttribute("factory_name", "CELL");
		return "views/gynh/quota_summary";
	}
}
