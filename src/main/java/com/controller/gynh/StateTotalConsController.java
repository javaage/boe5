package com.controller.gynh;


import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


//import com.model.unitcons.UnitCons;
//import com.service.unitcons.UnitConsService;

@Controller
@RequestMapping("/gynh")
public class StateTotalConsController {

	/**
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/statetotalcons")
	public String list(HttpServletRequest request, Model model) {
		return "views/gynh/statetotalcons";
	}
}
