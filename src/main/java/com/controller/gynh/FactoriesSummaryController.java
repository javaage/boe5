package com.controller.gynh;


import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("/gynh")
public class FactoriesSummaryController {


	@RequestMapping(value = "/factories_summary")
	public String factor_array(HttpServletRequest request, Model model) {
		return "views/gynh/factories_summary";
	}
}
