var test_dep_sys_cons = [

	{'dp':	'HVAC'	,'sys':	'CHS'	,'day':	'2018-01-01'	,'c':	69	},					
	{'dp':	'HVAC'	,'sys':	'FFU'	,'day':	'2018-01-01'	,'c':	127	},					
	{'dp':	'HVAC'	,'sys':	'PCW'	,'day':	'2018-01-01'	,'c':	104	},	
	{'dp':	'HVAC'	,'sys':	'SCR'	,'day':	'2018-01-01'	,'c':	69	},					
	{'dp':	'HVAC'	,'sys':	'VOC'	,'day':	'2018-01-01'	,'c':	127	},					
	{'dp':	'HVAC'	,'sys':	'FAN'	,'day':	'2018-01-01'	,'c':	104	},					
	{'dp':	'HVAC'	,'sys':	'RS'	,'day':	'2018-01-01'	,'c':	104	},					
	{'dp':	'HVAC'	,'sys':	'MAU'	,'day':	'2018-01-01'	,'c':	138	},					
	{'dp':	'HVAC'	,'sys':	'PGEX'	,'day':	'2018-01-01'	,'c':	198	},		

	{'dp':	'HVAC'	,'sys':	'CHS'	,'day':	'2018-01-02'	,'c':	81	},					
	{'dp':	'HVAC'	,'sys':	'FFU'	,'day':	'2018-01-02'	,'c':	174	},					
	{'dp':	'HVAC'	,'sys':	'PCW'	,'day':	'2018-01-02'	,'c':	131	},	
	{'dp':	'HVAC'	,'sys':	'SCR'	,'day':	'2018-01-02'	,'c':	69	},					
	{'dp':	'HVAC'	,'sys':	'VOC'	,'day':	'2018-01-02'	,'c':	127	},					
	{'dp':	'HVAC'	,'sys':	'FAN'	,'day':	'2018-01-02'	,'c':	104	},					
	{'dp':	'HVAC'	,'sys':	'RS'	,'day':	'2018-01-02'	,'c':	104	},					
	{'dp':	'HVAC'	,'sys':	'MAU'	,'day':	'2018-01-02'	,'c':	138	},					
	{'dp':	'HVAC'	,'sys':	'PGEX'	,'day':	'2018-01-02'	,'c':	198	},		
	
	{'dp':	'HVAC'	,'sys':	'CHS'	,'day':	'2018-01-03'	,'c':	151	},					
	{'dp':	'HVAC'	,'sys':	'FFU'	,'day':	'2018-01-03'	,'c':	138	},					
	{'dp':	'HVAC'	,'sys':	'PCW'	,'day':	'2018-01-03'	,'c':	198	},		
	{'dp':	'HVAC'	,'sys':	'SCR'	,'day':	'2018-01-03'	,'c':	69	},					
	{'dp':	'HVAC'	,'sys':	'VOC'	,'day':	'2018-01-03'	,'c':	127	},					
	{'dp':	'HVAC'	,'sys':	'FAN'	,'day':	'2018-01-03'	,'c':	104	},					
	{'dp':	'HVAC'	,'sys':	'RS'	,'day':	'2018-01-03'	,'c':	104	},					
	{'dp':	'HVAC'	,'sys':	'MAU'	,'day':	'2018-01-03'	,'c':	138	},					
	{'dp':	'HVAC'	,'sys':	'PGEX'	,'day':	'2018-01-03'	,'c':	198	},		

	{'dp':	'HVAC'	,'sys':	'CHS'	,'day':	'2018-01-04'	,'c':	151	},					
	{'dp':	'HVAC'	,'sys':	'FFU'	,'day':	'2018-01-04'	,'c':	138	},					
	{'dp':	'HVAC'	,'sys':	'PCW'	,'day':	'2018-01-04'	,'c':	198	},		
	{'dp':	'HVAC'	,'sys':	'SCR'	,'day':	'2018-01-04'	,'c':	69	},					
	{'dp':	'HVAC'	,'sys':	'VOC'	,'day':	'2018-01-04'	,'c':	127	},					
	{'dp':	'HVAC'	,'sys':	'FAN'	,'day':	'2018-01-04'	,'c':	104	},					
	{'dp':	'HVAC'	,'sys':	'RS'	,'day':	'2018-01-04'	,'c':	104	},					
	{'dp':	'HVAC'	,'sys':	'MAU'	,'day':	'2018-01-04'	,'c':	138	},					
	{'dp':	'HVAC'	,'sys':	'PGEX'	,'day':	'2018-01-04'	,'c':	198	},		

	{'dp':	'HVAC'	,'sys':	'CHS'	,'day':	'2018-01-05'	,'c':	151	},					
	{'dp':	'HVAC'	,'sys':	'FFU'	,'day':	'2018-01-05'	,'c':	138	},					
	{'dp':	'HVAC'	,'sys':	'PCW'	,'day':	'2018-01-05'	,'c':	198	},		
	{'dp':	'HVAC'	,'sys':	'SCR'	,'day':	'2018-01-05'	,'c':	69	},					
	{'dp':	'HVAC'	,'sys':	'VOC'	,'day':	'2018-01-05'	,'c':	127	},					
	{'dp':	'HVAC'	,'sys':	'FAN'	,'day':	'2018-01-05'	,'c':	104	},					
	{'dp':	'HVAC'	,'sys':	'RS'	,'day':	'2018-01-05'	,'c':	104	},					
	{'dp':	'HVAC'	,'sys':	'MAU'	,'day':	'2018-01-05'	,'c':	138	},					
	{'dp':	'HVAC'	,'sys':	'PGEX'	,'day':	'2018-01-05'	,'c':	198	},		

	{'dp':	'HVAC'	,'sys':	'CHS'	,'day':	'2018-01-06'	,'c':	151	},					
	{'dp':	'HVAC'	,'sys':	'FFU'	,'day':	'2018-01-06'	,'c':	138	},					
	{'dp':	'HVAC'	,'sys':	'PCW'	,'day':	'2018-01-06'	,'c':	198	},		
	{'dp':	'HVAC'	,'sys':	'SCR'	,'day':	'2018-01-06'	,'c':	69	},					
	{'dp':	'HVAC'	,'sys':	'VOC'	,'day':	'2018-01-06'	,'c':	127	},					
	{'dp':	'HVAC'	,'sys':	'FAN'	,'day':	'2018-01-06'	,'c':	104	},					
	{'dp':	'HVAC'	,'sys':	'RS'	,'day':	'2018-01-06'	,'c':	104	},					
	{'dp':	'HVAC'	,'sys':	'MAU'	,'day':	'2018-01-06'	,'c':	138	},					
	{'dp':	'HVAC'	,'sys':	'PGEX'	,'day':	'2018-01-06'	,'c':	198	},		

	{'dp':	'HVAC'	,'sys':	'CHS'	,'day':	'2018-01-07'	,'c':	151	},					
	{'dp':	'HVAC'	,'sys':	'FFU'	,'day':	'2018-01-07'	,'c':	138	},					
	{'dp':	'HVAC'	,'sys':	'PCW'	,'day':	'2018-01-07'	,'c':	198	},		
	{'dp':	'HVAC'	,'sys':	'SCR'	,'day':	'2018-01-07'	,'c':	69	},					
	{'dp':	'HVAC'	,'sys':	'VOC'	,'day':	'2018-01-07'	,'c':	127	},					
	{'dp':	'HVAC'	,'sys':	'FAN'	,'day':	'2018-01-07'	,'c':	104	},					
	{'dp':	'HVAC'	,'sys':	'RS'	,'day':	'2018-01-07'	,'c':	104	},					
	{'dp':	'HVAC'	,'sys':	'MAU'	,'day':	'2018-01-07'	,'c':	138	},					
	{'dp':	'HVAC'	,'sys':	'PGEX'	,'day':	'2018-01-07'	,'c':	198	},		

	{'dp':	'WATER'	,'sys':	'UPW'	,'day':	'2018-01-01'	,'c':	69	},					
	{'dp':	'WATER'	,'sys':	'CCSS'	,'day':	'2018-01-01'	,'c':	127	},					
	{'dp':	'WATER'	,'sys':	'WWT'	,'day':	'2018-01-01'	,'c':	104	},	
	
	{'dp':	'WATER'	,'sys':	'UPW'	,'day':	'2018-01-02'	,'c':	81	},					
	{'dp':	'WATER'	,'sys':	'CCSS'	,'day':	'2018-01-02'	,'c':	174	},					
	{'dp':	'WATER'	,'sys':	'WWT'	,'day':	'2018-01-02'	,'c':	131	},	
	
	{'dp':	'WATER'	,'sys':	'UPW'	,'day':	'2018-01-03'	,'c':	151	},					
	{'dp':	'WATER'	,'sys':	'CCSS'	,'day':	'2018-01-03'	,'c':	138	},					
	{'dp':	'WATER'	,'sys':	'WWT'	,'day':	'2018-01-03'	,'c':	198	},		

	{'dp':	'WATER'	,'sys':	'UPW'	,'day':	'2018-01-04'	,'c':	151	},					
	{'dp':	'WATER'	,'sys':	'CCSS'	,'day':	'2018-01-04'	,'c':	138	},					
	{'dp':	'WATER'	,'sys':	'WWT'	,'day':	'2018-01-04'	,'c':	198	},		

	{'dp':	'WATER'	,'sys':	'UPW'	,'day':	'2018-01-05'	,'c':	151	},					
	{'dp':	'WATER'	,'sys':	'CCSS'	,'day':	'2018-01-05'	,'c':	138	},					
	{'dp':	'WATER'	,'sys':	'WWT'	,'day':	'2018-01-05'	,'c':	198	},		

	{'dp':	'WATER'	,'sys':	'UPW'	,'day':	'2018-01-06'	,'c':	151	},					
	{'dp':	'WATER'	,'sys':	'CCSS'	,'day':	'2018-01-06'	,'c':	138	},					
	{'dp':	'WATER'	,'sys':	'WWT'	,'day':	'2018-01-06'	,'c':	198	},		

	{'dp':	'WATER'	,'sys':	'UPW'	,'day':	'2018-01-07'	,'c':	151	},					
	{'dp':	'WATER'	,'sys':	'CCSS'	,'day':	'2018-01-07'	,'c':	138	},					
	{'dp':	'WATER'	,'sys':	'WWT'	,'day':	'2018-01-07'	,'c':	198	},		

	{'dp':	'G&C'	,'sys':	'CDA'	,'day':	'2018-01-01'	,'c':	69	},					
	{'dp':	'G&C'	,'sys':	'PVHV'	,'day':	'2018-01-01'	,'c':	127	},					
	{'dp':	'G&C'	,'sys':	'CCSS'	,'day':	'2018-01-01'	,'c':	104	},	
	{'dp':	'G&C'	,'sys':	'GAS'	,'day':	'2018-01-01'	,'c':	104	},	
	
	{'dp':	'G&C'	,'sys':	'CDA'	,'day':	'2018-01-02'	,'c':	81	},					
	{'dp':	'G&C'	,'sys':	'PVHV'	,'day':	'2018-01-02'	,'c':	174	},					
	{'dp':	'G&C'	,'sys':	'CCSS'	,'day':	'2018-01-02'	,'c':	131	},	
	{'dp':	'G&C'	,'sys':	'GAS'	,'day':	'2018-01-02'	,'c':	104	},	
	
	{'dp':	'G&C'	,'sys':	'CDA'	,'day':	'2018-01-03'	,'c':	151	},					
	{'dp':	'G&C'	,'sys':	'PVHV'	,'day':	'2018-01-03'	,'c':	138	},					
	{'dp':	'G&C'	,'sys':	'CCSS'	,'day':	'2018-01-03'	,'c':	198	},		
	{'dp':	'G&C'	,'sys':	'GAS'	,'day':	'2018-01-03'	,'c':	104	},	

	{'dp':	'G&C'	,'sys':	'CDA'	,'day':	'2018-01-04'	,'c':	151	},					
	{'dp':	'G&C'	,'sys':	'PVHV'	,'day':	'2018-01-04'	,'c':	138	},					
	{'dp':	'G&C'	,'sys':	'CCSS'	,'day':	'2018-01-04'	,'c':	198	},		
	{'dp':	'G&C'	,'sys':	'GAS'	,'day':	'2018-01-04'	,'c':	104	},	

	{'dp':	'G&C'	,'sys':	'CDA'	,'day':	'2018-01-05'	,'c':	151	},					
	{'dp':	'G&C'	,'sys':	'PVHV'	,'day':	'2018-01-05'	,'c':	138	},					
	{'dp':	'G&C'	,'sys':	'CCSS'	,'day':	'2018-01-05'	,'c':	198	},		
	{'dp':	'G&C'	,'sys':	'GAS'	,'day':	'2018-01-05'	,'c':	104	},	

	{'dp':	'G&C'	,'sys':	'CDA'	,'day':	'2018-01-06'	,'c':	151	},					
	{'dp':	'G&C'	,'sys':	'PVHV'	,'day':	'2018-01-06'	,'c':	138	},					
	{'dp':	'G&C'	,'sys':	'CCSS'	,'day':	'2018-01-06'	,'c':	198	},		
	{'dp':	'G&C'	,'sys':	'GAS'	,'day':	'2018-01-06'	,'c':	104	},	

	{'dp':	'G&C'	,'sys':	'CDA'	,'day':	'2018-01-07'	,'c':	151	},					
	{'dp':	'G&C'	,'sys':	'PVHV'	,'day':	'2018-01-07'	,'c':	138	},					
	{'dp':	'G&C'	,'sys':	'CCSS'	,'day':	'2018-01-07'	,'c':	198	},		
	{'dp':	'G&C'	,'sys':	'GAS'	,'day':	'2018-01-07'	,'c':	104	},	

];

var test_dep_unit_cons = [

	{'dp':	'HVAC'	,'day':	'2018-01-01'	,'in':	100	,'out':	37	,'uc':	0.37	},					
	{'dp':	'WATER'	,'day':	'2018-01-01'	,'in':	120	,'out':	36	,'uc':	0.3	},					
	{'dp':	'G&C'	,'day':	'2018-01-01'	,'in':	100	,'out':	15	,'uc':	0.15},	
	
	{'dp':	'HVAC'	,'day':	'2018-01-02'	,'in':	100	,'out':	20	,'uc':	0.2	},							
	{'dp':	'WATER'	,'day':	'2018-01-02'	,'in':	120	,'out':	12	,'uc':	0.10	},						
	{'dp':	'G&C'	,'day':	'2018-01-02'	,'in':	100	,'out':	25	,'uc':	0.25},
	
	{'dp':	'HVAC'	,'day':	'2018-01-03'	,'in':	80	,'out':	20	,'uc':	0.4	},									
	{'dp':	'WATER'	,'day':	'2018-01-03'	,'in':	120	,'out':	120	,'uc':	1	},						
	{'dp':	'G&C'	,'day':	'2018-01-03'	,'in':	150	,'out':	10	,'uc':	0.15},		

	{'dp':	'HVAC'	,'day':	'2018-01-04'	,'in':	100	,'out':	37	,'uc':	0.37	},					
	{'dp':	'WATER'	,'day':	'2018-01-04'	,'in':	120	,'out':	36	,'uc':	0.3	},					
	{'dp':	'G&C'	,'day':	'2018-01-04'	,'in':	100	,'out':	15	,'uc':	0.15},	
	
	{'dp':	'HVAC'	,'day':	'2018-01-05'	,'in':	400	,'out':	40	,'uc':	0.1	},							
	{'dp':	'WATER'	,'day':	'2018-01-05'	,'in':	120	,'out':	12	,'uc':	0.10	},						
	{'dp':	'G&C'	,'day':	'2018-01-05'	,'in':	100	,'out':	25	,'uc':	0.25},
	
	{'dp':	'HVAC'	,'day':	'2018-01-06'	,'in':	200	,'out':	20	,'uc':	0.1	},									
	{'dp':	'WATER'	,'day':	'2018-01-06'	,'in':	120	,'out':	120	,'uc':	1	},						
	{'dp':	'G&C'	,'day':	'2018-01-06'	,'in':	100	,'out':	10	,'uc':	0.10},		

	{'dp':	'HVAC'	,'day':	'2018-01-07'	,'in':	100	,'out':	20	,'uc':	0.2	},							
	{'dp':	'WATER'	,'day':	'2018-01-07'	,'in':	120	,'out':	12	,'uc':	0.10	},						
	{'dp':	'G&C'	,'day':	'2018-01-07'	,'in':	100	,'out':	25	,'uc':	0.25},
];

var hvac_sys_array = ['CHS', 'FFU', 'PCW', 'SCR', 'VOC', 'FAN', 'RS', 'MAU', 'PGEX'];
var water_sys_array = ['UPW', 'CCSS', 'WWT'];
var gc_sys_array = ['CDA', 'PVHV', 'CCSS', 'GAS'];

var hvac_unit_array = ['Power', 'RC', 'EER'];
var water_unit_array = ['Power', 'UPW', 'EER'];
var gc_unit_array = ['Power', 'CDA', 'EER'];

var dep_sys_daily_cons = {};
var dep_unit_cons = {};

function loadOtherData(data) {
	var temp_dep_sys_cons = test_dep_sys_cons;
	var temp_dep_unit_cons = test_dep_unit_cons;

	var begin_date = $('#beginTimeBox').val();
	var end_date  = $('#endTimeBox').val();
	if (begin_date == "") begin_date = init_begin_date;
	if (end_date == "") end_date = init_end_date;

	var begin_year = begin_date.substr(0,4)*1;
	var begin_month = begin_date.substr(5,2)*1;
	var begin_day = begin_date.substr(8,2)*1;

	var d1 = new Date(begin_date.replace(/-/g,   "/"))
	var d2 = new Date(end_date.replace(/-/g,   "/"))
	var days = (d2.getTime()-d1.getTime()) / (1000*60*60*24) + 1;

	var xAxis = new Array();

	for (var i = 0; i < days; i++) {
	    var temp = new Date(begin_year, begin_month-1, begin_day);
	    xAxis[i] = getFormatDate2(temp, i);
	    xAxis[i] = xAxis[i].substring(8);
	}
	
	hvac_sys_daily_chart_option.xAxis.data = xAxis;
	water_sys_daily_chart_option.xAxis.data = xAxis;
	gc_sys_daily_chart_option.xAxis.data = xAxis;
	
	hvac_unit_cons_chart_option.xAxis.data = xAxis;
	water_unit_cons_chart_option.xAxis.data = xAxis;
	gc_unit_cons_chart_option.xAxis.data = xAxis;
	
	// dep_sys_daily_cons
	dep_sys_daily_cons = {};
	for (var i = 0; i < temp_dep_sys_cons.length; i++) {
		if (typeof(dep_sys_daily_cons[temp_dep_sys_cons[i].dp]) == "undefined") {
			dep_sys_daily_cons[temp_dep_sys_cons[i].dp] = {};
		}
		if (typeof(dep_sys_daily_cons[temp_dep_sys_cons[i].dp][temp_dep_sys_cons[i].sys]) == "undefined") {
			dep_sys_daily_cons[temp_dep_sys_cons[i].dp][temp_dep_sys_cons[i].sys] = new Array(days).fill('-');
		}
		
		var dateArray = temp_dep_sys_cons[i].day .split("-");
		var idx = (new Date(dateArray[0], dateArray[1]-1, dateArray[2]) - new Date(begin_year, begin_month-1, begin_day))/(24*60*60*1000);
		dep_sys_daily_cons[temp_dep_sys_cons[i].dp][temp_dep_sys_cons[i].sys][idx] = temp_dep_sys_cons[i].c;
	}
	
	// dep_unit_cons
	dep_unit_cons = {};
	for (var i = 0; i < temp_dep_unit_cons.length; i++) {
		if (typeof(dep_unit_cons[temp_dep_unit_cons[i].dp]) == "undefined") {
			dep_unit_cons[temp_dep_unit_cons[i].dp] = {};
		}
		if (typeof(dep_unit_cons[temp_dep_unit_cons[i].dp]['in']) == "undefined") {
			dep_unit_cons[temp_dep_unit_cons[i].dp]['in'] = new Array(days).fill('-');
		}
		if (typeof(dep_unit_cons[temp_dep_unit_cons[i].dp]['out']) == "undefined") {
			dep_unit_cons[temp_dep_unit_cons[i].dp]['out'] = new Array(days).fill('-');
		}
		if (typeof(dep_unit_cons[temp_dep_unit_cons[i].dp]['uc']) == "undefined") {
			dep_unit_cons[temp_dep_unit_cons[i].dp]['uc'] = new Array(days).fill('-');
		}
		
		var dateArray = temp_dep_unit_cons[i].day .split("-");
		var idx = (new Date(dateArray[0], dateArray[1]-1, dateArray[2]) - new Date(begin_year, begin_month-1, begin_day))/(24*60*60*1000);
		dep_unit_cons[temp_dep_unit_cons[i].dp]['in'][idx] = temp_dep_unit_cons[i].in;
		dep_unit_cons[temp_dep_unit_cons[i].dp]['out'][idx] = temp_dep_unit_cons[i].out;
		dep_unit_cons[temp_dep_unit_cons[i].dp]['uc'][idx] = temp_dep_unit_cons[i].uc;
	}
	
	hvac_sys_daily_chart_option.series.splice(0, hvac_sys_daily_chart_option.series.length);
	water_sys_daily_chart_option.series.splice(0, water_sys_daily_chart_option.series.length);
	gc_sys_daily_chart_option.series.splice(0, gc_sys_daily_chart_option.series.length);

	hvac_unit_cons_chart_option.series.splice(0, hvac_unit_cons_chart_option.series.length);
	water_unit_cons_chart_option.series.splice(0, water_unit_cons_chart_option.series.length);
	gc_unit_cons_chart_option.series.splice(0, gc_unit_cons_chart_option.series.length);


	for (var i = 0; i < hvac_sys_array.length; i++) {
		var sys_name = hvac_sys_array[i];
		if (typeof(dep_sys_daily_cons['HVAC']) != "undefined" && typeof(dep_sys_daily_cons['HVAC'][sys_name]) != "undefined") {
			hvac_sys_daily_chart_option.series.push(
				{
		            name: sys_name,
		            type: 'bar',
		            stack: 'total',
		            data: dep_sys_daily_cons['HVAC'][sys_name],
		            animation: false,
		        }
			);
		}
	}

	for (var i = 0; i < water_sys_array.length; i++) {
		var sys_name = water_sys_array[i];
		if (typeof(dep_sys_daily_cons['WATER']) != "undefined" && typeof(dep_sys_daily_cons['WATER'][sys_name]) != "undefined") {
			water_sys_daily_chart_option.series.push(
				{
		            name: sys_name,
		            type: 'bar',
		            stack: 'total',
		            data: dep_sys_daily_cons['WATER'][sys_name],
		            animation: false,
		        }
			);
		}
	}
	
	for (var i = 0; i < gc_sys_array.length; i++) {
		var sys_name = gc_sys_array[i];
		if (typeof(dep_sys_daily_cons['G&C']) != "undefined" && typeof(dep_sys_daily_cons['G&C'][sys_name]) != "undefined") {
			gc_sys_daily_chart_option.series.push(
				{
		            name: sys_name,
		            type: 'bar',
		            stack: 'total',
		            data: dep_sys_daily_cons['G&C'][sys_name],
		            animation: false,
		        }
			);
		}
	}
	
	if (typeof(dep_unit_cons['HVAC']) != "undefined" && typeof(dep_unit_cons['HVAC']['in']) != "undefined") {
		hvac_unit_cons_chart_option.series.push(
			{
	            name: hvac_unit_array[0],
	            type: 'bar',
	            data: dep_unit_cons['HVAC']['in'],
	            animation: false,
	            yAxisIndex:0,
	        }
		);
	}
	if (typeof(dep_unit_cons['HVAC']) != "undefined" && typeof(dep_unit_cons['HVAC']['out']) != "undefined") {
		hvac_unit_cons_chart_option.series.push(
			{
	            name: hvac_unit_array[1],
	            type: 'bar',
	            data: dep_unit_cons['HVAC']['out'],
	            animation: false,
	            yAxisIndex:1,
	        }
		);
	}
	if (typeof(dep_unit_cons['HVAC']) != "undefined" && typeof(dep_unit_cons['HVAC']['uc']) != "undefined") {
		hvac_unit_cons_chart_option.series.push(
			{
	            name: hvac_unit_array[2],
	            type: 'line',
	            data: dep_unit_cons['HVAC']['uc'],
	            animation: false,
	            yAxisIndex:2,
	        }
		);
	}
	
	if (typeof(dep_unit_cons['WATER']) != "undefined" && typeof(dep_unit_cons['WATER']['in']) != "undefined") {
		water_unit_cons_chart_option.series.push(
			{
	            name: water_unit_array[0],
	            type: 'bar',
	            data: dep_unit_cons['WATER']['in'],
	            animation: false,
	            yAxisIndex:0,
	        }
		);
	}
	if (typeof(dep_unit_cons['WATER']) != "undefined" && typeof(dep_unit_cons['WATER']['out']) != "undefined") {
		water_unit_cons_chart_option.series.push(
			{
	            name: water_unit_array[1],
	            type: 'bar',
	            data: dep_unit_cons['WATER']['out'],
	            animation: false,
	            yAxisIndex:1,
	        }
		);
	}
	if (typeof(dep_unit_cons['WATER']) != "undefined" && typeof(dep_unit_cons['WATER']['uc']) != "undefined") {
		water_unit_cons_chart_option.series.push(
			{
	            name: water_unit_array[2],
	            type: 'line',
	            data: dep_unit_cons['WATER']['uc'],
	            animation: false,
	            yAxisIndex:2,
	        }
		);
	}

	if (typeof(dep_unit_cons['G&C']) != "undefined" && typeof(dep_unit_cons['G&C']['in']) != "undefined") {
		gc_unit_cons_chart_option.series.push(
			{
	            name: gc_unit_array[0],
	            type: 'bar',
	            data: dep_unit_cons['G&C']['in'],
	            animation: false,
	            yAxisIndex:0,
	        }
		);
	}
	if (typeof(dep_unit_cons['G&C']) != "undefined" && typeof(dep_unit_cons['G&C']['out']) != "undefined") {
		gc_unit_cons_chart_option.series.push(
			{
	            name: gc_unit_array[1],
	            type: 'bar',
	            data: dep_unit_cons['G&C']['out'],
	            animation: false,
	            yAxisIndex:1,
	        }
		);
	}
	if (typeof(dep_unit_cons['G&C']) != "undefined" && typeof(dep_unit_cons['G&C']['uc']) != "undefined") {
		gc_unit_cons_chart_option.series.push(
			{
	            name: gc_unit_array[2],
	            type: 'line',
	            data: dep_unit_cons['G&C']['uc'],
	            animation: false,
	            yAxisIndex:2,
	        }
		);
	}
	
	hvac_sys_daily_chart.setOption(hvac_sys_daily_chart_option, true);
	water_sys_daily_chart.setOption(water_sys_daily_chart_option, true);
	gc_sys_daily_chart.setOption(gc_sys_daily_chart_option, true);
	
	hvac_unit_cons_chart.setOption(hvac_unit_cons_chart_option, true);
	water_unit_cons_chart.setOption(water_unit_cons_chart_option, true);
	gc_unit_cons_chart.setOption(gc_unit_cons_chart_option, true);
}

function page_custom_init() {
	$("#cub_sum_charts_div").show();
	$("#echarts_div").hide();
}

function page_custom_resize() {
	let w = $('.chart-container').width();
	let h = $('.chart-container').height();
	console.log(w);
	console.log(h);

	document.getElementById('hvac_sys_daily_chart').style.width = 20 + 'px';
	document.getElementById('hvac_sys_daily_chart').style.height = 20 + 'px';
	document.getElementById('water_sys_daily_chart').style.width = 20 + 'px';
	document.getElementById('water_sys_daily_chart').style.height = 20 + 'px';
	document.getElementById('gc_sys_daily_chart').style.width = 20 + 'px';
	document.getElementById('gc_sys_daily_chart').style.height = 20 + 'px';

	document.getElementById('hvac_unit_cons_chart').style.width = 20 + 'px';
	document.getElementById('hvac_unit_cons_chart').style.height = 20 + 'px';
	document.getElementById('water_unit_cons_chart').style.width = 20 + 'px';
	document.getElementById('water_unit_cons_chart').style.height = 20 + 'px';
	document.getElementById('gc_unit_cons_chart').style.width = 20 + 'px';
	document.getElementById('gc_unit_cons_chart').style.height = 20 + 'px';

	
	$("#gc_sys_daily_td").width(w/3);
	$("#water_sys_daily_td").width(w/3);
	$("#hvac_sys_daily_td").width(w/3);
	
	$("#hvac_sys_daily_chart_td").height((h-70)/3);
	$("#hvac_unit_cons_chart_td").height((h-70)/3);
	$("#hvac_waring_chart_td").height((h-70)/3);

	
	document.getElementById('hvac_sys_daily_chart').style.width = document.getElementById('hvac_sys_daily_chart').parentNode.offsetWidth-4 + 'px';
	document.getElementById('hvac_sys_daily_chart').style.height = document.getElementById('hvac_sys_daily_chart').parentNode.offsetHeight + 'px';
	hvac_sys_daily_chart.resize();
	document.getElementById('water_sys_daily_chart').style.width = document.getElementById('water_sys_daily_chart').parentNode.offsetWidth-4 + 'px';
	document.getElementById('water_sys_daily_chart').style.height = document.getElementById('water_sys_daily_chart').parentNode.offsetHeight + 'px';
	water_sys_daily_chart.resize();
	document.getElementById('gc_sys_daily_chart').style.width = document.getElementById('gc_sys_daily_chart').parentNode.offsetWidth-4 + 'px';
	document.getElementById('gc_sys_daily_chart').style.height = document.getElementById('gc_sys_daily_chart').parentNode.offsetHeight + 'px';
	gc_sys_daily_chart.resize();

	document.getElementById('hvac_unit_cons_chart').style.width = document.getElementById('hvac_unit_cons_chart').parentNode.offsetWidth-4 + 'px';
	document.getElementById('hvac_unit_cons_chart').style.height = document.getElementById('hvac_unit_cons_chart').parentNode.offsetHeight + 'px';
	hvac_unit_cons_chart.resize();
	document.getElementById('water_unit_cons_chart').style.width = document.getElementById('water_unit_cons_chart').parentNode.offsetWidth-4 + 'px';
	document.getElementById('water_unit_cons_chart').style.height = document.getElementById('water_unit_cons_chart').parentNode.offsetHeight + 'px';
	water_unit_cons_chart.resize();
	document.getElementById('gc_unit_cons_chart').style.width = document.getElementById('gc_unit_cons_chart').parentNode.offsetWidth-4 + 'px';
	document.getElementById('gc_unit_cons_chart').style.height = document.getElementById('gc_unit_cons_chart').parentNode.offsetHeight + 'px';
	gc_unit_cons_chart.resize();
	
	console.log(w);
	console.log(h);
	console.log($("#hvac_sys_daily_td").width());
	console.log($("#water_sys_daily_td").width());
	console.log($("#gc_sys_daily_td").width());
	
	console.log($("#hvac_sys_daily_chart_td").height());
	console.log($("#hvac_unit_cons_chart_td").height());
	console.log($("#hvac_waring_chart_td").height());
}

