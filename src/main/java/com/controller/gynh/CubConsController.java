package com.controller.gynh;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("/gynh")
public class CubConsController {


	@RequestMapping(value = "/cub_cons")
	public String cub_gc(HttpServletRequest request, Model model) {
		return "views/gynh/cub_cons";
	}
}
