package com.controller.gynh;


import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;



@Controller
@RequestMapping("/gynh")
public class ExportController {
	
	@RequestMapping(value = "/factory_cons/factory_cons")
	private String test01(HttpServletRequest request, Model model) {
		model.addAttribute("factory_name", "'(\"ARRAY\")'");
		model.addAttribute("export_name", "ARRAY");
		return "views/gynh/factory_cons";
	}
	
	@RequestMapping(value = "/export_array")
	private String export_array(HttpServletRequest request, Model model) {
		model.addAttribute("export_name", "ARRAY");
		return "views/gynh/export";
	}
	@RequestMapping(value = "/export_cf")
	private String export_cf(HttpServletRequest request, Model model) {
		model.addAttribute("export_name", "CF");
		return "views/gynh/export";
	}
	@RequestMapping(value = "/export_cell")
	private String export_cell(HttpServletRequest request, Model model) {
		model.addAttribute("export_name", "CELL");
		return "views/gynh/export";
	}
	@RequestMapping(value = "/export_mdl")
	private String export_mdl(HttpServletRequest request, Model model) {
		model.addAttribute("export_name", "MDL");
		return "views/gynh/export";
	}
}
