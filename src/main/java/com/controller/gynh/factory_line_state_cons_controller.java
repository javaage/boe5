package com.controller.gynh;


import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("/gynh")
public class factory_line_state_cons_controller {


	@RequestMapping(value = "/array_line_state_cons")
	public String array_line_state_cons(HttpServletRequest request, Model model) {
		model.addAttribute("factory", "'ARRAY'");
		return "views/gynh/factory_line_state_cons";
	}

	@RequestMapping(value = "/cf_line_state_cons")
	public String cf_line_state_cons(HttpServletRequest request, Model model) {
		model.addAttribute("factory", "'CF'");
		return "views/gynh/factory_line_state_cons";
	}

	@RequestMapping(value = "/cell_line_state_cons")
	public String cell_line_state_cons(HttpServletRequest request, Model model) {
		model.addAttribute("factory", "'CELL'");
		return "views/gynh/factory_line_state_cons";
	}

	@RequestMapping(value = "/md_line_state_cons")
	public String md_line_state_cons(HttpServletRequest request, Model model) {
		model.addAttribute("factory", "'MD'");
		return "views/gynh/factory_line_state_cons";
	}
}
