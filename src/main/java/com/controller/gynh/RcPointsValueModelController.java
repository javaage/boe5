package com.controller.gynh;


import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;



@Controller
@RequestMapping("/gynh")
public class RcPointsValueModelController {


	
	@RequestMapping(value = "/mchw_flow_graph")
	public String mchw_flow_graph(HttpServletRequest request, Model model) {
		return "views/gynh/mchw_flow_graph";
	}
}
