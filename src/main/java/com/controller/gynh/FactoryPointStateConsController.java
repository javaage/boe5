package com.controller.gynh;


import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("/gynh")
public class FactoryPointStateConsController {


	@RequestMapping(value = "/factor_array")
	public String factor_array(HttpServletRequest request, Model model) {
		model.addAttribute("factory", "'ARRAY'");
		return "views/gynh/factory";
	}
	
	@RequestMapping(value = "/factor_cell")
	public String factor_cell(HttpServletRequest request, Model model) {
		model.addAttribute("factory", "'CELL'");
		return "views/gynh/factory";
	}
	
	@RequestMapping(value = "/factor_cf")
	public String factor_cf(HttpServletRequest request, Model model) {
		model.addAttribute("factory", "'CF'");
		return "views/gynh/factory";
	}
	
	@RequestMapping(value = "/factor_mdl")
	public String factor_mdl(HttpServletRequest request, Model model) {
		model.addAttribute("factory", "'MD'");
		return "views/gynh/factory";
	}
	@RequestMapping(value = "/factory_point_cons_hourly_array")
	public String factory_point_cons_hourly_array(HttpServletRequest request, Model model) {
		model.addAttribute("factory", "'ARRAY'");
		return "views/gynh/factory_point_cons_hourly";
	}
	
	@RequestMapping(value = "/factory_point_cons_hourly_cell")
	public String factory_point_cons_hourly_cell(HttpServletRequest request, Model model) {
		model.addAttribute("factory", "'CELL'");
		return "views/gynh/factory_point_cons_hourly";
	}
	
	@RequestMapping(value = "/factory_point_cons_hourly_cf")
	public String factory_point_cons_hourly_cf(HttpServletRequest request, Model model) {
		model.addAttribute("factory", "'CF'");
		return "views/gynh/factory_point_cons_hourly";
	}
	
	@RequestMapping(value = "/factory_point_cons_hourly_mdl")
	public String factory_point_cons_hourly_mdl(HttpServletRequest request, Model model) {
		model.addAttribute("factory", "'MD'");
		return "views/gynh/factory_point_cons_hourly";
	}
}
