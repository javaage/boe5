package com.controller.gynh;


import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("/gynh")
public class ConsFlowController {
	
	@RequestMapping(value = "/factories_cons_flow")
	public String factories_cons_flow(HttpServletRequest request, Model model) {
		model.addAttribute("factories_name", "factories_Power");
		model.addAttribute("factory_name", "'(\"ARRAY\")'");
		return "views/gynh/cons_flow";
	}
	@RequestMapping(value = "/array_cons_flow")
	public String array_cons_flow(HttpServletRequest request, Model model) {
		model.addAttribute("factories_name", "ARRAY_Power");
		model.addAttribute("factory_name", "'(\"ARRAY\")'");
		return "views/gynh/cons_flow";
	}
	
	@RequestMapping(value = "/cf_cons_flow")
	public String cf_cons_flow(HttpServletRequest request, Model model) {
		model.addAttribute("factories_name", "CF_Power");
		model.addAttribute("factory_name", "'(\"CF\")'");
		return "views/gynh/cons_flow";
	}
	
	@RequestMapping(value = "/mdl_cons_flow")
	public String mdl_cons_flow(HttpServletRequest request, Model model) {
		model.addAttribute("factories_name", "MDL_Power");
		model.addAttribute("factory_name", "'(\"MD\")'");
		return "views/gynh/cons_flow";
	}
	
	@RequestMapping(value = "/cell_cons_flow")
	public String cell_cons_flow(HttpServletRequest request, Model model) {
		model.addAttribute("factories_name", "CELL_Power");
		model.addAttribute("factory_name", "'(\"CELL\")'");
		return "views/gynh/cons_flow";
	}
}


