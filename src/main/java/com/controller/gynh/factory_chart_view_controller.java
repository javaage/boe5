package com.controller.gynh;


import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("/gynh")
public class factory_chart_view_controller {

	@RequestMapping(value = "/array_chart_view")
	public String array_chart_view(HttpServletRequest request, Model model) {
		model.addAttribute("factory_name", "'(\"ARRAY\")'");
		return "views/gynh/factory_chart_view";
	}
	@RequestMapping(value = "/cf_chart_view")
	public String cf_chart_view(HttpServletRequest request, Model model) {
		model.addAttribute("factory_name", "'(\"CF\")'");
		return "views/gynh/factory_chart_view";
	}
	@RequestMapping(value = "/cell_chart_view")
	public String cell_chart_view(HttpServletRequest request, Model model) {
		model.addAttribute("factory_name", "'(\"CELL\")'");
		return "views/gynh/factory_chart_view";
	}
	@RequestMapping(value = "/mdl_chart_view")
	public String mdl_chart_view(HttpServletRequest request, Model model) {
		model.addAttribute("factory_name", "'(\"MD\")'");
		return "views/gynh/factory_chart_view";
	}
}
