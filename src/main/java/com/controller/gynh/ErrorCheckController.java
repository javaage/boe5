package com.controller.gynh;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/gynh")
public class ErrorCheckController {


	@RequestMapping(value = "/error_check")
	public String error_check(HttpServletRequest request, Model model) {
		return "views/gynh/errorcheck";
	}
	
	@RequestMapping(value = "/factory_point_error_check")
	public String factory_point_error_check(HttpServletRequest request, Model model) {
		model.addAttribute("errorcheck_name", "ARRAY_Power");
		return "views/gynh/errorcheck";
	}
	
	@RequestMapping(value = "/cub_point_error_check")
	public String cub_point_error_check(HttpServletRequest request, Model model) {
		model.addAttribute("errorcheck_name", "MDL_Power");
		return "views/gynh/errorcheck";
	}
	
	@RequestMapping(value = "/fmcs_point_error_check")
	public String fmcs_point_error_check(HttpServletRequest request, Model model) {
		model.addAttribute("errorcheck_name", "CELL_Power");
		return "views/gynh/errorcheck";
	}
	@RequestMapping(value = "/pacada_point_error_check")
	public String pacada_point_error_check(HttpServletRequest request, Model model) {
		model.addAttribute("errorcheck_name", "CUB_HVAV");
		return "views/gynh/errorcheck";
	}

	@RequestMapping(value = "/fmcs_inst_point_error_check")
	public String fmcs_inst_point_error_check(HttpServletRequest request, Model model) {
		model.addAttribute("errorcheck_name", "CUB_HVAV");
		return "views/gynh/errorcheck";
	}
}
