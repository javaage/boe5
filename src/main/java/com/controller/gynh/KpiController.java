package com.controller.gynh;


import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("/gynh")
public class KpiController {


	@RequestMapping(value = "/kpi_total")
	public String kpi_total(HttpServletRequest request, Model model) {
		model.addAttribute("kpi", "'(\"TOTAL\")'");
		return "views/gynh/kpi";
	}

	@RequestMapping(value = "/kpi_array")
	public String kpi_array(HttpServletRequest request, Model model) {
		model.addAttribute("kpi", "'(\"ARRAY\")'");
		return "views/gynh/kpi";
	}
	
	@RequestMapping(value = "/kpi_cell")
	public String kpi_cell(HttpServletRequest request, Model model) {
		model.addAttribute("kpi", "'(\"CELL\")'");
		return "views/gynh/kpi";
	}
	
	@RequestMapping(value = "/kpi_cf")
	public String kpi_cf(HttpServletRequest request, Model model) {
		model.addAttribute("kpi", "'(\"CF\")'");
		return "views/gynh/kpi";
	}
	
	@RequestMapping(value = "/kpi_mdl")
	public String kpi_mdl(HttpServletRequest request, Model model) {
		model.addAttribute("kpi", "'(\"MDL\")'");
		return "views/gynh/kpi";
	}
	@RequestMapping(value = "/kpi_havc")
	public String kpi_havc(HttpServletRequest request, Model model) {
		model.addAttribute("kpi", "'(\"HVAC\")'");
		return "views/gynh/kpi";
	}
	
	@RequestMapping(value = "/kpi_water")
	public String kpi_water(HttpServletRequest request, Model model) {
		model.addAttribute("kpi", "'(\"Water\")'");
		return "views/gynh/kpi";
	}
	
	@RequestMapping(value = "/kpi_gc")
	public String kpi_gc(HttpServletRequest request, Model model) {
		model.addAttribute("kpi", "'(\"GC\")'");
		return "views/gynh/kpi";
	}
}
