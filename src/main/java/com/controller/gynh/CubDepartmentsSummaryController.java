package com.controller.gynh;


import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("/gynh")
public class CubDepartmentsSummaryController {


	@RequestMapping(value = "/cub_departments_summary")
	public String get_page(HttpServletRequest request, Model model) {
		return "views/gynh/cub_departments_summary";
	}
}
