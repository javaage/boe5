package com.controller.gynh;


import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("/gynh")
public class first_summary_controller {


	@RequestMapping(value = "/first_summary")
	public String factor_array(HttpServletRequest request, Model model) {
		return "views/gynh/first_summary";
	}
}
