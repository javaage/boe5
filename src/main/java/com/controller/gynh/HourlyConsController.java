package com.controller.gynh;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;



@Controller
@RequestMapping("/gynh")
public class HourlyConsController {

	
	@RequestMapping(value = "/hourlycons")
	private String hourly_cons(HttpServletRequest request, Model model) {
	
		return "views/gynh/hourly_cons";
	}
}
