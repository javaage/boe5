package com.controller.gynh;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("/gynh")
public class CubDepConsController {


	@RequestMapping(value = "/cub_gc")
	public String cub_gc(HttpServletRequest request, Model model) {
		model.addAttribute("department", "'(\"G&C\")'");
		return "views/gynh/cub_dep_cons";
	}
	
	@RequestMapping(value = "/cub_hvac")
	public String cub_hvac(HttpServletRequest request, Model model) {
		model.addAttribute("department", "'(\"HVAC\")'");
		return "views/gynh/cub_dep_cons";
	}
	
	@RequestMapping(value = "/cub_water")
	public String cub_water(HttpServletRequest request, Model model) {
		model.addAttribute("department", "'(\"WATER\")'");
		return "views/gynh/cub_dep_cons";
	}
}
