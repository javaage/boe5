package com.controller.gynh;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("/consumption")
public class ConsumptionAnalysisController {
	// 菜单跳转至冷机系统页面
	@RequestMapping(value = "/consumption_analysis")
	private String export_array(HttpServletRequest request, Model model) {
		return "views/gynh/consumption_analysis";
	}
}
