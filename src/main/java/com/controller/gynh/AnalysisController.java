package com.controller.gynh;


import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/gynh")
public class AnalysisController {
	
	/**
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/analysis")
	public String list(HttpServletRequest request, Model model) {
		return "views/gynh/analysis";
	}
}
