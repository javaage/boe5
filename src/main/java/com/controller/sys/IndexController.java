package com.controller.sys;


import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.model.user.User;
import com.service.sys.RoleService;
import com.service.user.UserService;

/**
 * 首页Controller
 * @author oneyuanma
 *
 */
@Controller
@RequestMapping("/index")
public class IndexController {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private RoleService roleService;

	private static Logger log = Logger.getLogger(IndexController.class);

	/**
	 * 后台主页
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("index")
	public String index(HttpServletRequest request, Model model) {
		log.info("转到首页！");
		return "views/sys/index";
	}
	
	/**
	 * 后台主页面（默认页）
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("main")
	public String main(HttpServletRequest request, Model model) {
		
		User user = (User) request.getSession().getAttribute("user");
		user = (User) userService.get(user);
		int role_id=user.getRoleId();
		if(role_id==5)
			return "views/sys/sysmain";
		else
			return "views/sys/main";
	}
	
	/**
	 * 暂未开发页面
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("temp")
	public String temp(HttpServletRequest request, Model model) {
		return "views/sys/temp";
	}
	
}
