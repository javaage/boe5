package com.controller.sys;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.model.sys.Unit;
import com.model.user.User;
import com.service.sys.UnitService;
import com.service.user.UserService;

@Controller
@RequestMapping("/unit")
public class UnitController {

	@Autowired
	private UnitService unitService;
	
	@Autowired
	private UserService userService;
	

	@ResponseBody
	@RequestMapping(value = "/save")
	public String save(String unit_name,String unit_value, HttpServletRequest request, Model model,String pageName) {
		User user = (User) request.getSession().getAttribute("user");
		user = (User) userService.get(user);
		int userid=user.getId();
		List<Unit> list = unitService.getUnit(userid,pageName);
		for(Unit unit : list) {
			if(unit.getUnit_name().equals(unit_name)){
				unitService.delete(unit.getId());
			}
		}
		unitService.save(userid,unit_name,unit_value,pageName);
		
		String str="ok";
		return str;
	}
	@ResponseBody
	@RequestMapping(value = "/getUnit")
	public Map<String, Object> getUnit(HttpServletRequest request, Model model,String pageName) {
		
		User user = (User) request.getSession().getAttribute("user");
		user = (User) userService.get(user);
		int userid=user.getId();
		List<Unit> list = unitService.getUnit(userid,pageName);
		/*String currentPowerUnit = "kWh";//kWh,mWh
		String currentCDAUnit = "L";//L,m³,k(m³)
		String currentDIWUnit = "L";//L,m³,k(m³)
		String currentPN2Unit = "L";//L,m³,k(m³)
		String currentTimeUnit = "M";
		*/
		String currentRCUnit ="";//kWh,mWh
		String currentPowerUnit ="";//kWh,mWh
		String currentCDAUnit = "";//L,m³,k(m³)
		String currentDIWUnit = "";//L,m³,k(m³)
		String currentPN2Unit = "";//L,m³,k(m³)
		String currentTimeUnit = "";
		
		
		for(Unit unit : list) {
			if(unit.getUnit_name().equals("power_unit")){
				if(unit.getUnit_value()!=null&&unit.getUnit_value()!="") {
					currentPowerUnit = unit.getUnit_value();
				}		
			}else if(unit.getUnit_name().equals("rc_unit")){
				if(unit.getUnit_value()!=null&&unit.getUnit_value()!="") {
					currentRCUnit = unit.getUnit_value();
				}
			}else if(unit.getUnit_name().equals("cda_unit")){
				if(unit.getUnit_value()!=null&&unit.getUnit_value()!="") {
					currentCDAUnit = unit.getUnit_value();
				}
			}else if(unit.getUnit_name().equals("diw_unit")){
				if(unit.getUnit_value()!=null&&unit.getUnit_value()!="") {
					currentDIWUnit = unit.getUnit_value();
				}
			}else if(unit.getUnit_name().equals("pn2_unit")){
				if(unit.getUnit_value()!=null&&unit.getUnit_value()!="") {
					currentPN2Unit = unit.getUnit_value();
				}
			}else if(unit.getUnit_name().equals("time_unit")){
				if(unit.getUnit_value()!=null&&unit.getUnit_value()!="") {
					currentTimeUnit = unit.getUnit_value();
				}
			}
		}
		
        Map<String, Object> map = new HashMap<String, Object>();
        
        map.put("currentRCUnit", currentRCUnit);
        map.put("currentPowerUnit", currentPowerUnit);
        map.put("currentCDAUnit", currentCDAUnit);
        map.put("currentDIWUnit", currentDIWUnit);
        map.put("currentPN2Unit", currentPN2Unit);
        map.put("currentTimeUnit", currentTimeUnit);

		return map;
	}
	
	@ResponseBody
	@RequestMapping(value = "/delete")
	public String delete(HttpServletRequest request, Model model, Unit unit) {
		String result = "1";
		try {
			unitService.delete(unit.getId());
			result = "0";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	
}
