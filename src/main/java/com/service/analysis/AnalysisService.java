package com.service.analysis;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mapper.analysis.AnalysisMapper;
import com.model.analysis.Analysis;
import com.service.base.CrudService;

@Service
@Transactional
public class AnalysisService extends CrudService<AnalysisMapper, Analysis>{
	
	@Resource
	public AnalysisMapper mapper;
	@Transactional
	public List<Analysis> getListByCondition(
			Analysis obj, 
			int beginYear, 
			int beginMonth, 
			int beginDay, 
			int beginTime, 
			int endYear, 
			int endMonth, 
			int endDay, 
			int endTime
	) {
		return mapper.getListByCondition(
			obj,
			beginYear, 
			beginMonth, 
			beginDay, 
			beginTime, 
			endYear, 
			endMonth, 
			endDay, 
			endTime
		);
	}

}