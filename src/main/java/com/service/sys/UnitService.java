package com.service.sys;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mapper.sys.UnitMapper;
import com.model.sys.Unit;

@Service
@Transactional
public class UnitService {
	
	@Resource
	public UnitMapper unitMapper;

	

	public List<Unit> getUnit(int userid, String pageName) {
		return unitMapper.getUnit(userid,pageName);
		// TODO Auto-generated method stub
		
	}
	@Transactional
	public void save(int userid, String unit_name, String unit_value, String pageName) {
		// TODO Auto-generated method stub
		unitMapper.save(userid, unit_name,unit_value,pageName);

	}

	public void delete(int id) {
		// TODO Auto-generated method stub
		unitMapper.delete(id);

	}
}
