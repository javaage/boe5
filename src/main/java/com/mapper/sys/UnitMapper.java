package com.mapper.sys;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.model.sys.Unit;

public interface UnitMapper {

	/**
	 * 删除角色和菜单管理关系
	 * @param id
	 */
	
	public void delete(@Param("id") int id);

	public void save(@Param("userid") int userid, @Param("unit_name") String unit_name, 
			@Param("unit_value") String unit_value,  @Param("pageName") String pageName);
	
	public List<Unit> getUnit(@Param("userid") int userid,@Param("pageName") String pageName);
	
}
