package com.mapper.analysis;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.mapper.base.BaseDao;
import com.model.analysis.Analysis;

public interface AnalysisMapper extends BaseDao<Analysis>{

	public Long getTest(Analysis unitCons);
	
	public List<Analysis> getListByCondition(
			@Param("unitCons") 		Analysis unitCons, 
			@Param("beginYear") 	int beginYear, 
			@Param("beginMonth")	int beginMonth, 
			@Param("beginDay")		int beginDay, 
			@Param("beginTime")		int beginTime, 
			@Param("endYear")		int endYear, 
			@Param("endMonth")		int endMonth, 
			@Param("endDay")		int endDay, 
			@Param("endTime")		int endTime
	);

}
