package com.mapper.base;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.model.page.PageDto;

/**
 * Oracle数据库 数据访问层基类
 *
 * @author: 幸运草
 * @create: 2018-12-16 21:51
 */
@Repository
public abstract class OracleBaseDao<T>
{

	//todo:连不上oracle需要临时注释掉
	//@Autowired
	@Qualifier(value = "oracleSqlSessionFactory")
	private SqlSessionFactory sqlSessionFactory;

	private SqlSession sqlSession;

	public SqlSessionFactory getSqlSessionFactory() {
		return sqlSessionFactory;
	}

	public SqlSession getSqlSession() {
		if (null == sqlSession) {
			sqlSession = sqlSessionFactory.openSession();
		}
		return sqlSession;
	}

	/**
	 * 总数
	 *
	 * @param entity
	 *            查询实体
	 * @return 总记录数
	 */
	public abstract Long getCount(T entity);

	/**
	 * 获取单个对象
	 * 
	 * @param entity
	 *            查询实体
	 * @return 单个对象信息
	 */
	public abstract T get(T entity);

//	/**
//	 * 获取列表 分页
//	 * 
//	 * @param entity
//	 *            条件查询实体
//	 * @return 列表信息
//	 */
//	public abstract List<T> getListByPage(T entity);

	/**
	 * 获取全部数据
	 * 
	 * @param entity
	 *            查询条件实体
	 * @return 数据列表
	 */
	public abstract List<T> getAllList(T entity);

	/**
	 * 添加
	 * 
	 * @param entity
	 *            实体信息
	 */
	public abstract void insert(T entity);

	/**
	 * 修改
	 * 
	 * @param entity
	 *            实体信息
	 */
	public abstract void update(T entity);

	/**
	 * 删除
	 * 
	 * @param id
	 *            实体主键编号
	 */
	public abstract void delete(Integer id);

	/**
	 * 总数
	 *
	 * @param entity
	 *            查询实体
	 * @return 总记录数
	 */
	protected Long getCount(String mapper, T entity) {
		return this.getSqlSession().selectOne(mapper, entity);
	}

	/**
	 * 获取单个对象
	 * 
	 * @param entity
	 *            查询实体
	 * @return 单个对象信息
	 */
	protected T get(String mapper, T entity) {
		return this.getSqlSession().selectOne(mapper, entity);
	}

//	/**
//	 * 获取列表 分页
//	 * 
//	 * @param entity
//	 *            条件查询实体
//	 * @return 列表信息
//	 */
//	protected List<T> getListByPage(String mapper, T entity) {
//
//		// 拆分字符串
//		String[] array = mapper.split("\\.");
//
//		// 调取 *.getCount
//		Long count = this.getSqlSession().selectOne(array[0] + ".getCount", entity);
//
//		// 如果总共记录数为0 则直接返回null
//		if (0 == count) {
//			return null;
//		}
//
//		entity.setTotalCount(count);
//		return this.getSqlSession().selectList(mapper, entity);
//	}

	/**
	 * 获取全部数据
	 * 
	 * @param entity
	 *            查询条件实体
	 * @return 数据列表
	 */
	protected List<T> getAllList(String mapper, T entity) {
		return this.getSqlSession().selectList(mapper, entity);
	}

	/**
	 * 添加
	 * 
	 * @param entity
	 *            实体信息
	 */
	protected void insert(String mapper, T entity) {
		this.getSqlSession().insert(mapper, entity);
	}

	/**
	 * 修改
	 * 
	 * @param entity
	 *            实体信息
	 */
	protected void update(String mapper, T entity) {
		this.getSqlSession().update(mapper, entity);
	}

	/**
	 * 删除
	 * 
	 * @param id
	 *            实体主键编号
	 */
	protected void delete(String mapper, Integer id) {
		this.getSqlSession().delete(mapper, id);
	}

}