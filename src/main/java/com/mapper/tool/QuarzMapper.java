package com.mapper.tool;

import java.util.List;
import java.util.Map;

import com.mapper.base.BaseDao;
import com.model.tool.Quarz;

/**
 * @功能说明：调度管理
 * @作者： one源码
 * @创建日期：2018-05-06
 */
public interface QuarzMapper extends BaseDao<Quarz>{
	
	 /**
	 * 获取所有
	 * @param quarz
	 * @return
	 */
	public List<Map> getAllListByMap(Quarz entity);
	
}
