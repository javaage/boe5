package com.mapper.point;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.mapper.base.OracleBaseDao;
import com.model.point.*;


@Repository
public class FactoryPointValueMapper extends OracleBaseDao<point_value_model> {

	@Override
	public Long getCount(point_value_model entity) {
		return this.getCount("FactoryPointValueMapper.getCount", entity);
	}

	@Override
	public point_value_model get(point_value_model entity) {
		return this.get("FactoryPointValueMapper.get", entity);
	}


	@Override
	public List<point_value_model> getAllList(point_value_model entity) {
		return this.getAllList("FactoryPointValueMapper.getAllList", entity);
	}

	@Override
	public void insert(point_value_model entity) {
		this.insert("FactoryPointValueMapper.insert", entity);
	}

	@Override
	public void update(point_value_model entity) {
		this.update("FactoryPointValueMapper.update", entity);
	}

	@Override
	public void delete(Integer id) {
		this.delete("FactoryPointValueMapper.delete", id);
	}

	public point_value_model getByPointNumberAndValue(Integer pointNumber, Integer value) {
		Map<String, Integer> paramMap = new HashMap<String, Integer>(2);
		paramMap.put("pointNumber", pointNumber);
		paramMap.put("value", value);
		return this.getSqlSession().selectOne("FactoryPointValueMapper.getByPointNumberAndValue", paramMap);
	}
	
	public List<point_value_model> get_points_value(
		String time
	) {
		Map<String, String> paramMap = new HashMap<String, String>();
		paramMap.put("time", time);
		return this.getSqlSession().selectList("FactoryPointValueMapper.get_points_value", paramMap);
	}
	
	public List<point_value_model> get_inst_points_value(
		String time
	) {
		Map<String, String> paramMap = new HashMap<String, String>();
		paramMap.put("time", time);
		return this.getSqlSession().selectList("FactoryPointValueMapper.get_inst_points_value", paramMap);
	}
	
	public List<point_value_model> get_points_value_in_point_list(
		String point_list
	) {
		Map<String, String> paramMap = new HashMap<String, String>();
		paramMap.put("point_list", point_list);
		return this.getSqlSession().selectList("FactoryPointValueMapper.get_points_value_in_point_list", paramMap);
	}

	public List<time_value_model> get_points_value_by_point_number(
		String begin_time, 
		String end_time,
		int point_number
	) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("begin_time", begin_time);
		paramMap.put("end_time", end_time);
		paramMap.put("point_number", point_number);
		return this.getSqlSession().selectList("FactoryPointValueMapper.get_points_value_by_point_number", paramMap);
	}

	public List<time_value_model> get_inst_points_value_by_point_number(
		String begin_time, 
		String end_time,
		int point_number
	) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("begin_time", begin_time);
		paramMap.put("end_time", end_time);
		paramMap.put("point_number", point_number);
		return this.getSqlSession().selectList("FactoryPointValueMapper.get_inst_points_value_by_point_number", paramMap);
	}

}