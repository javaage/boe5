package com.mapper.point;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.mapper.base.BaseDao;
import com.model.point.Point;

public interface PointMapper extends BaseDao<Point>{

	public Long getTest(Point point);
	
	public List<Point> getListByCondition(
			@Param("beginDateTime") 	String beginDateTime, 
			@Param("endDateTime")		String endDateTime,
			@Param("department")	String department
	);
	public long getCount(
			@Param("beginDateTime") 	String beginDateTime, 
			@Param("endDateTime")		String endDateTime,
			@Param("department")	String department
			);
}
