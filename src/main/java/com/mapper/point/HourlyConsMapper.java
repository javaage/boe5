package com.mapper.point;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.model.point.*;

public interface HourlyConsMapper {

	List<hourly_cons_model> get_department_point_list(
			@Param("begin_day") String begin_day, 
			@Param("end_day") String end_day, 
			@Param("begin_hour") String begin_hour, 
			@Param("end_hour") String end_hour, 
			@Param("factory") String factory, 
			@Param("energy") String energy
			);

	List<hourly_cons_model> get_device_unit_point_list(
			@Param("begin_day") String begin_day, 
			@Param("end_day") String end_day, 
			@Param("begin_hour") String begin_hour, 
			@Param("end_hour") String end_hour, 
			@Param("factory") String factory, 
			@Param("energy") String energy);

	List<hourly_cons_model> get_device_point_list(
			@Param("begin_day") String begin_day, 
			@Param("end_day") String end_day, 
			@Param("begin_hour") String begin_hour, 
			@Param("end_hour") String end_hour, 
			@Param("factory") String factory, 
			@Param("energy") String energy);

	List<hourly_cons_model> get_eqp_id_point_list(
			@Param("begin_day") String begin_day, 
			@Param("end_day") String end_day, 
			@Param("begin_hour") String begin_hour, 
			@Param("end_hour") String end_hour, 
			@Param("factory") String factory, 
			@Param("energy") String energy);

	List<hourly_cons_model> get_department_qty_list(	
			@Param("begin_datetime") String begin_datetime, 
			@Param("end_datetime") String end_datetime, 
			@Param("factory") String factory,
			@Param("department") String department);

	List<hourly_cons_model> get_eqp_id_qty_list(
			@Param("begin_datetime") String begin_datetime, 
			@Param("end_datetime") String end_datetime, 
			@Param("factory") String factory,
			@Param("department") String department,
			@Param("eqp_id") String eqp_id);

	List<hourly_cons_model> get_device_qty_list(
			@Param("begin_datetime") String begin_datetime, 
			@Param("end_datetime") String end_datetime, 
			@Param("factory") String factory,
			@Param("department") String department,
			@Param("eqp_id") String eqp_id,
			@Param("device") String device);

	List<hourly_cons_model> get_device_unit_qty_list(
			@Param("begin_datetime") String begin_datetime, 
			@Param("end_datetime") String end_datetime, 
			@Param("factory") String factory,
			@Param("department") String department,
			@Param("eqp_id") String eqp_id,
			@Param("device") String device,
			@Param("device_unit") String device_unit);
			
	List<state_time_long_model> get_state_time_for_hourly_page(
		@Param("eqp_list") String eqp_list,
		@Param("start_time") String start_time,
		@Param("end_time") String end_time
	);
}