
var array_power = 0;
var cf_power = 0;
var cell_power = 0;
var md_power = 0;
var cub_power = 0;
var other_power = 0;

function loadSummaryData(begin, end) {
	if  (begin > end) {
		var temp=begin;
		begin = end;
		end = temp;
	}
	
	var beginYear = begin.substr(0,4)*1;
	var beginMonth = begin.substr(5,2)*1;
	var beginDay =  begin.substr(8,2)*1;
	var endYear = end.substr(0,4)*1;
	var endMonth = end.substr(5,2)*1;
	var endDay = end.substr(8,2)*1;
	
	var index = top.layer.msg('查询中...', {icon: 16, skin:is_dark?'layui-layer-lan':'', shade:0.1});
	$.ajax({
		url: path + "/gynh/summary/get_data.do",
		type: 'post',
		async: false,
		data: {
			beginYear:beginYear, 
			beginMonth:beginMonth, 
			beginDay:beginDay, 
			endYear:endYear, 
			endMonth:endMonth, 
			endDay:endDay, 
		},
		success: function (data) {
			array_power = data['array_power'];
			cf_power = data['cf_power'];
			cell_power = data['cell_power'];
			md_power = data['md_power'];
			cub_power = data['cub_power'];
			other_power = data['other_power'];

//			powerChart.setOption({                
//                series:[
//    				{
//    		            name: 'RUN',
//    		            type: 'bar',
//    		            stack: '总量',
//    		            data: [runPower]
//    		        },
//    		        {
//    		            name: 'PM',
//    		            type: 'bar',
//    		            stack: '总量',
//    		            data: [pmPower]
//    		        },
//                ]
//            });

			top.layer.close(index);
			top.layer.msg('数据加载完成', {icon: 1, skin: is_dark?'layui-layer-lan':''});
		},
		error: function () {
			top.layer.close(index);
			top.layer.msg('数据加载错误', {icon: 1, skin: is_dark?'layui-layer-lan':''});
		}
	});
}

window.onresize = function() {

}

