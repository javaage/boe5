
function getFormatDate(delta_day) {
    var date = new Date();
    date.setDate(date.getDate()+delta_day); 
    var seperator1 = "-";
    var month = date.getMonth() + 1;
    var strDate = date.getDate();
    if  (month >= 1 && month <= 9) {
        month = "0" + month;
    }
    if  (strDate >= 0 && strDate <= 9) {
        strDate = "0" + strDate;
    }
    var currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate;
    return currentdate;
}

function getTimeDate(delta_hour) {
	return get_offset_seconds_time(3600 * delta_hour);
	
	
}

function get_offset_seconds_time(offset_seconds) {

	var times = new Date().getTime()+1000*offset_seconds; //1000：转毫秒

	//var times = new Date().getTime()-3600000*(hours+8);
	var datetime = new Date(times);
	var year = datetime.getFullYear(); //得到年份
	var month = datetime.getMonth();//得到月份
	var date = datetime.getDate();//得到日期
	var day = datetime.getDay();//得到周几
	var hour = datetime.getHours();//得到小时
	var minu = datetime.getMinutes();//得到分钟
	var sec = datetime.getSeconds();//得到秒
	month = month + 1;
	if (month < 10) month = "0" + month;
	if (date < 10) date = "0" + date;
	if (hour < 10) hour = "0" + hour;
	if (minu < 10) minu = "0" + minu;
	if (sec < 10) sec = "0" + sec;
	var time = year + "-" + month + "-" + date+ " " + hour + ":" + minu + ":" + sec;
	return time;
}

function getTimeDate2(dates,delta_hour) {
	
	var times = new Date(dates).getTime()+3600000*delta_hour;

	//var times = new Date().getTime()-3600000*(hours+8);
	var datetime = new Date(times);
	var year = datetime.getFullYear(); //得到年份
	var month = datetime.getMonth();//得到月份
	var date = datetime.getDate();//得到日期
	var day = datetime.getDay();//得到周几
	var hour = datetime.getHours();//得到小时
	var minu = datetime.getMinutes();//得到分钟
	var sec = datetime.getSeconds();//得到秒
	month = month + 1;
	if (month < 10) month = "0" + month;
	if (date < 10) date = "0" + date;
	if (hour < 10) hour = "0" + hour;
	if (minu < 10) minu = "0" + minu;
	if (sec < 10) sec = "0" + sec;
	var time = year + "-" + month + "-" + date+ " " + hour + ":" + minu + ":" + sec;
	return time;
}

function get_formated_date_time(date_time) {
	var year = date_time.getFullYear(); //得到年份
	var month = date_time.getMonth();//得到月份
	var date = date_time.getDate();//得到日期
	var day = date_time.getDay();//得到周几
	var hour = date_time.getHours();//得到小时
	var minu = date_time.getMinutes();//得到分钟
	var sec = date_time.getSeconds();//得到秒
	month = month + 1;
	if (month < 10) month = "0" + month;
	if (date < 10) date = "0" + date;
	if (hour < 10) hour = "0" + hour;
	if (minu < 10) minu = "0" + minu;
	if (sec < 10) sec = "0" + sec;
	var time = year + "-" + month + "-" + date+ " " + hour + ":" + minu + ":" + sec;
	return time;
}


function getFormatDate2(dates, delta_day) {
	var date = new Date(dates);
    date.setDate(date.getDate()+delta_day); 
    var month = date.getMonth() + 1;
    var strDate = date.getDate();
    if  (month >= 1 && month <= 9) {
        month = "0" + month;
    }
    if  (strDate >= 0 && strDate <= 9) {
        strDate = "0" + strDate;
    }
    var currentdate = date.getFullYear() + "-" + month + "-" + strDate;
    return currentdate;
}

function bjtime_to_gmt(str_date_time) {
	var datetime = new Date(str_date_time);
	datetime = new Date(datetime.getTime()-3600000*8);
	var year = datetime.getFullYear(); //得到年份
	var month = datetime.getMonth();//得到月份
	var date = datetime.getDate();//得到日期
	var day = datetime.getDay();//得到周几
	var hour = datetime.getHours();//得到小时
	var minu = datetime.getMinutes();//得到分钟
	var sec = datetime.getSeconds();//得到秒
	month = month + 1;
	if (month < 10) month = "0" + month;
	if (date < 10) date = "0" + date;
	if (hour < 10) hour = "0" + hour;
	if (minu < 10) minu = "0" + minu;
	if (sec < 10) sec = "0" + sec;
	var time = year + "-" + month + "-" + date+ " " + hour + ":" + minu + ":" + sec;

	return time;
}

function test_date(y, m, d) {
    var temp = new Date(y, m-1, d);
	
	if (   y == temp.getFullYear()
		&& m == (temp.getMonth() + 1)
		&& d == temp.getDate()
	) {
		return true;
	}
	return false;
}


function row_to_col_daily(
	src, //源数据
	day_field, //日期字段名
	val_field, //值字段名
	begin_date, 
	end_date
) {
	var temp_dst = {}; //union_key: data[]
	
	var days = (end_date-begin_date)/(24*3600*1000) + 1;
	var key_fields = new Array();
	
	for (var i = 0; i < src.length; i++) {
		var union_key = '';
		key_fields = [];
		for (var key in src[i]) {
			if (key != day_field && key != val_field) {
				union_key += src[i][key] + '---';
				key_fields.push(key);
			}
		}

		if (typeof(temp_dst[union_key]) == "undefined") {
			temp_dst[union_key] = {};
			temp_dst[union_key].data = new Array(days).fill('-');
		}

		var idx = (new Date(src[i][day_field]) - begin_date)/(24*3600*1000); // 日期差的总毫秒/一天的毫秒 = 日期差的天数
		temp_dst[union_key].data[idx] = src[i][val_field];
	}
	
	var dst = []; // field1:v1, field2:v2, ... , data:[] 
	
	Object.keys(temp_dst).forEach(function(key){
		var field_values = key.split('---');
		var record = {};
		for (var i = 0; i < key_fields.length; i++) {
			record[key_fields[i]] = field_values[i];
		}
		record.data = temp_dst[key].data;
		dst.push(record);

	});

	return dst;
}