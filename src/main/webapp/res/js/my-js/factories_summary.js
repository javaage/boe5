var factory_array = [str_array, str_cf, str_cell, "MD"];
var energy_array = ['Power', str_upw, str_cda, str_pn2];
var param_name_array = ['产量', '稼动率', '单耗'];
var param_id_array = ['product', 'state', 'unit_cons'];
var param_chart_type_array = ['bar', 'line', 'line'];
var temp_summary_data;

var currentRCUnit = 'KW2';//kWh,mWh
var currentPowerUnit = 'kWh';//kWh,mWh
var currentCDAUnit = 'L';//L,m³,k(m³)
var currentUPWUnit = 'L';//L,m³,k(m³)
var currentPN2Unit = 'L';//L,m³,k(m³)
var currentTimeUnit = 'M';


var chart_map = {
	ARRAY :{
		Power: array_power_chart,
		UPW:   array_upw_chart,
		CDA:   array_cda_chart,
		PN2:   array_pn2_chart,
	},
	CF :{
		Power: cf_power_chart,
		UPW:   cf_upw_chart,
		CDA:   cf_cda_chart,
		PN2:   cf_pn2_chart,
	},
	CELL :{
		Power: cell_power_chart,
		UPW:   cell_upw_chart,
		CDA:   cell_cda_chart,
		PN2:   cell_pn2_chart,
	},
	MD :{
		Power: mdl_power_chart,
		UPW:   mdl_upw_chart,
		CDA:   mdl_cda_chart,
		PN2:   mdl_pn2_chart,
	},
};

var chart_option_map = {
	ARRAY :{
		Power: array_power_chart_option,
		UPW:   array_upw_chart_option,
		CDA:   array_cda_chart_option,
		PN2:   array_pn2_chart_option,
	},
	CF :{
		Power: cf_power_chart_option,
		UPW:   cf_upw_chart_option,
		CDA:   cf_cda_chart_option,
		PN2:   cf_pn2_chart_option,
	},
	CELL :{
		Power: cell_power_chart_option,
		UPW:   cell_upw_chart_option,
		CDA:   cell_cda_chart_option,
		PN2:   cell_pn2_chart_option,
	},
	MD :{
		Power: mdl_power_chart_option,
		UPW:   mdl_upw_chart_option,
		CDA:   mdl_cda_chart_option,
		PN2:   mdl_pn2_chart_option,
	},
};

var all_chart = new Array(
	array_power_chart,
	array_upw_chart,
	array_cda_chart,
	array_pn2_chart,

	cf_power_chart,
	cf_upw_chart,
	cf_cda_chart,
	cf_pn2_chart,

	cell_power_chart,
	cell_upw_chart,
	cell_cda_chart,
	cell_pn2_chart,

	mdl_power_chart,
	mdl_upw_chart,
	mdl_cda_chart,
	mdl_pn2_chart
);

var all_chart_option = new Array(
	array_power_chart_option,
	array_upw_chart_option,
	array_cda_chart_option,
	array_pn2_chart_option,

	cf_power_chart_option,
	cf_upw_chart_option,
	cf_cda_chart_option,
	cf_pn2_chart_option,

	cell_power_chart_option,
	cell_upw_chart_option,
	cell_cda_chart_option,
	cell_pn2_chart_option,

	mdl_power_chart_option,
	mdl_upw_chart_option,
	mdl_cda_chart_option,
	mdl_pn2_chart_option
);

var echart_tooltip =function (params) {
	var str="";
	for (var i=0;i<params.length;i++) {
		if (params[i].seriesName=="稼动率") {
			if (isNaN(params[i].value)) {
				str=str+'<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:'+params[i].color+';"></span>'
				str= '<span style="color:#000000">'+str+ params[i].seriesName+' : %'+params[i].value+'</span>'+'<br>';
			} else {
				str=str+'<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:'+params[i].color+';"></span>'
	    		str= '<span style="color:#000000">'+str+ params[i].seriesName+' : %'+(params[i].value*100).toFixed(2)+'</span>'+'<br>';
			}
		} else {
			if (isNaN(params[i].value)) {
				str=str+'<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:'+params[i].color+';"></span>'
				str= '<span style="color:#000000">'+str+ params[i].seriesName+' : '+params[i].value+'</span>'+'<br>';
			} else {
				str=str+'<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:'+params[i].color+';"></span>'
	    		str= '<span style="color:#000000">'+str+ params[i].seriesName+' : '+params[i].value.toFixed(4)+'</span>'+'<br>';
			}
		}
		
	}
    return str;
};

let chart_name_array = new Array(
	'array_power_chart',
	'array_upw_chart',
	'array_cda_chart',
	'array_pn2_chart',

	'cf_power_chart',
	'cf_upw_chart',
	'cf_cda_chart',
	'cf_pn2_chart',

	'cell_power_chart',
	'cell_upw_chart',
	'cell_cda_chart',
	'cell_pn2_chart',

	'mdl_power_chart',
	'mdl_upw_chart',
	'mdl_cda_chart',
	'mdl_pn2_chart'
);

var factory_summary_data = {};
var echart_style=true;

function echarts_style(){
	if(echart_style){
		for(var i=0;i<all_chart_option.length;i++){
			all_chart_option[i].yAxis[0].min=null;
			all_chart_option[i].yAxis[0].max=null;
			all_chart_option[i].yAxis[1].min=null;
			all_chart_option[i].yAxis[1].max=null;
			all_chart[i].setOption(all_chart_option[i], true);
		}
		echart_style=false;
	}else{
		for(var i=0;i<all_chart_option.length;i++){
			all_chart_option[i].yAxis[0].min=yAxis_min;
			all_chart_option[i].yAxis[0].max=yAxis_max;
			all_chart_option[i].yAxis[1].min=yAxis_min;
			all_chart_option[i].yAxis[1].max=yAxis_max;
			all_chart[i].setOption(all_chart_option[i], true);
		}
		echart_style=true;
	}
}

hvac_sys_daily_chart.on('click', function (params) { 
    
    if(params.componentType == "xAxis"){  
        alert("单击了"+params.value+"x轴标签");  
    }else if (params.componentType == "yAxis") {
        alert("单击了"+params.value+"y轴标签");  
    }
    else{  
        alert("单击了"+params.name+"柱状图"+params.value);
       
      }  })

function load_data_impl() {
	
	//test_summary_data;
	
	var begin_date = '';//$('#beginTimeBox').val();
	var end_date  = '';//$('#endTimeBox').val();
	if (begin_date == "") begin_date = defaut_begin_date;
	if (end_date == "") end_date = defaut_end_date;

	var begin_year = begin_date.substr(0,4)*1;
	var begin_month = begin_date.substr(5,2)*1;
	var begin_day = begin_date.substr(8,2)*1;

	var d1 = new Date(begin_date.replace(/-/g,   "/"))
	var d2 = new Date(end_date.replace(/-/g,   "/"))
	var days = (d2.getTime()-d1.getTime()) / (1000*60*60*24) + 1;

	var xAxis = new Array();

	for (var i = 0; i < days; i++) {
	    var temp = new Date(begin_year, begin_month-1, begin_day);
	    xAxis[i] = getFormatDate2(temp, i);
	    xAxis[i] = xAxis[i].substring(8);
	}
	
	for (var i = 0; i< all_chart_option.length; i++) {
		all_chart_option[i].xAxis.data = xAxis;
	}
	
	// factory_summary_data
	factory_summary_data = {};
	for (var i = 0; i < temp_summary_data.length; i++) {
		if (typeof(factory_summary_data[temp_summary_data[i].factory]) == "undefined") {
			factory_summary_data[temp_summary_data[i].factory] = {};
		}
		if (typeof(factory_summary_data[temp_summary_data[i].factory][temp_summary_data[i].param]) == "undefined") {
			factory_summary_data[temp_summary_data[i].factory][temp_summary_data[i].param] = new Array(days).fill('-');
		}
		
		var dateArray = temp_summary_data[i].day .split("-");
		var idx = (new Date(dateArray[0], dateArray[1]-1, dateArray[2]) - new Date(begin_year, begin_month-1, begin_day))/(24*60*60*1000);
		factory_summary_data[temp_summary_data[i].factory][temp_summary_data[i].param][idx] = temp_summary_data[i].val;
	}
	
	for (var i = 0; i< all_chart_option.length; i++) {
		all_chart_option[i].series.splice(0, all_chart_option[i].series.length);
	}

	for (var i = 0; i < factory_array.length; i++) {
		var factory_name = factory_array[i];
		for (var j = 0; j < energy_array.length; j++) {
			var energy_name = energy_array[j];
			var factory_energy = [];
			if (typeof(factory_summary_data[factory_name]) != "undefined" && typeof(factory_summary_data[factory_name][energy_name]) != "undefined") {
				factory_energy = factory_summary_data[factory_name][energy_name].map(function (item) {
					return item;
				});
			}			
			chart_option_map[factory_name][energy_name].series.push(
				{
		            name: energy_name,
		            type: 'bar',
		            yAxisIndex: 0,
		            data: factory_energy,
		            animation: true,
		        }
			);

			for (var k = 0; k < param_id_array.length; k++) {
				var src_factory_name = factory_name;
				var param_name =
					(k == 2) 
					? energy_name + '_' + param_id_array[k]
					: param_id_array[k]
				;
				
				var param_values = [];
				if (typeof(factory_summary_data[src_factory_name]) != "undefined" && typeof(factory_summary_data[src_factory_name][param_name]) != "undefined") {
					param_values = factory_summary_data[src_factory_name][param_name];
					if (k==0 && (factory_name == 'MD' || factory_name == "CELL")) { //md and cell products
						param_values = param_values.map(function (item) {
							  return item/1000;
						});
					}
				}
				chart_option_map[factory_name][energy_name].series.push(
					{
			            name: param_name_array[k],
			            type: param_chart_type_array[k],
			            yAxisIndex: k+1,
			            data: param_values,
			            animation: true,
			        }
				);
			}
		}
	}

	for (var i = 0; i< all_chart.length; i++) {
		
		all_chart_option[i].tooltip.formatter=echart_tooltip;
		all_chart[i].setOption(all_chart_option[i], true);
	}
}

function page_custom_init() {
	$("#cub_sum_charts_div").show();
	$("#echarts_div").hide();
}

function page_custom_resize() {

	let h = $(window).height();
	let w = $(window).width();
	
	$("#fac_sum_charts_div").width(w);
	$("#fac_sum_charts_div").height(h);

	for (var i = 0; i < chart_name_array.length; i++) {
		document.getElementById(chart_name_array[i]).style.width = 20 + 'px';
		document.getElementById(chart_name_array[i]).style.height = 20 + 'px';
	}

	let left = 55;
	let right = 32;
	let top = 40;
	let bottom = 48;
	var chart_w = (w-left-right)/4;
	var chart_h = (h-top-bottom)/4;
	
	$("#r0c0_td").width(left);
	$("#r0c5_td").width(right);
	$("#r0c1_td").width(chart_w);
	$("#r0c2_td").width(chart_w);
	$("#r0c3_td").width(chart_w);
	$("#r0c4_td").width(chart_w);
	
	$("#r0_tr").height(top);
	$("#r5_tr").height(bottom);
	$("#r1c0_td").height(chart_h);
	$("#r2c0_td").height(chart_h);
	$("#r3c0_td").height(chart_h);
	$("#r4c0_td").height(chart_h);

	for (var i = 0; i < chart_name_array.length; i++) {
		document.getElementById(chart_name_array[i]).style.width = chart_w-4 + 'px';
		document.getElementById(chart_name_array[i]).style.height = chart_h-4 + 'px';
		all_chart[i].resize();
	}
}

window.onload = function() {
	load_default_shortcut();
	set_units_on_controles();
	load_data(defaut_begin_date, defaut_end_date);
	page_custom_resize();
}

window.onresize = function() {
	page_custom_resize();
}

window.onunload = function(event) {
	SaveSnapshot(pageName, '', 'icon_path_1', true);
}

function SaveSnapshot(page, name, icon, when_close) {
	var cfg = {
			currentPowerUnit:currentPowerUnit,
			currentCDAUnit:currentCDAUnit,
			currentUPWUnit:currentUPWUnit,
			currentPN2Unit:currentPN2Unit,
			currentTimeUnit:currentTimeUnit,
			currentRCUnit:currentRCUnit,
		};
	var strJson = JSON.stringify(cfg, function(key, val) {
		if (typeof val === 'function') {
			return val + '';
		}
		return val;
	});
	
    $.ajax({
        url: when_close ? (path + "/gynh/biz/save_default_shortcut.do") : (path + "/gynh/biz/save_shortcut.do"),
        type: 'post',
        async: false,
        data: {
        	page:page, 
        	name:name, 
        	icon:icon, 
        	state_json:strJson,
        },
        success: function (data) {
        	if (!when_close) {
        		top.layer.msg('保存成功', {icon: 1, skin:is_dark?'layui-layer-lan':''});
	        	loadSnapshot();
        	}
        },
        error: function () {
        	if (!when_close) {
        		top.layer.msg('保存失败', {icon: 1, skin:is_dark?'layui-layer-lan':''});
        	}
        }
    });
}

function load_default_shortcut() {
	
	$.ajax({
		url: path + "/gynh/biz/load_default_shortcut.do",
		type: 'post',
		async: false,
		data: {
			page_name:pageName, 
		},
		success: function (data) {
			var shortcuts = data['shortcut'];
			if (shortcuts.length == 1) {
				
				apply_shortcut(shortcuts[0].state_json);
				
			}
        },
        error: function () {
        }
	});
	
}

function apply_shortcut(json) {
	var cfg = JSON.parse(json, function(k,v) {
		if(v.indexOf&&v.indexOf('function')>-1) {
			return eval("(function(){return "+v+" })()")
		}
		return v;
	});
	
	currentPowerUnit = cfg.currentPowerUnit;
	currentCDAUnit = cfg.currentCDAUnit;
	currentUPWUnit = cfg.currentUPWUnit;
	currentPN2Unit = cfg.currentPN2Unit;
	currentTimeUnit = cfg.currentTimeUnit;
	currentRCUnit = cfg.currentRCUnit;
    
    if(currentRCUnit==null||currentRCUnit=='')
    {
    	currentRCUnit="KW2";
    }
    if(currentPowerUnit==null||currentPowerUnit=='')
    {
    	currentPowerUnit="mWh";
    }
    if(currentCDAUnit==null||currentCDAUnit=='')
    {
    	currentCDAUnit="k(m³)";
    }
    if(currentUPWUnit==null||currentUPWUnit=='')
    {
    	currentUPWUnit="k(m³)";
    }
    if(currentPN2Unit==null||currentPN2Unit=='')
    {
    	currentPN2Unit="k(m³)";
    }
    if(currentTimeUnit==null||currentTimeUnit=='')
    {
    	currentTimeUnit="M";
    }
}

function onClickLoad() {
	
	var begin = defaut_begin_date;
	var end = defaut_end_date;
	load_data(begin, end);
}

function translate_unit(params,from,to)
{
	translate(params,from,to);
    load_data_impl();

}
function translate(params,from,to)
{
	for (var i = 0; i < temp_summary_data.length; i++) {
		var param=temp_summary_data[i].param;
		var str=param.split('_');
		param=str[0];
		if(param==params){
			if (from == 'kWh' && to == 'mWh') {
				temp_summary_data[i].val = temp_summary_data[i].val/1000.0 ;
			}
			if (from == 'mWh' && to == 'kWh') {
				temp_summary_data[i].val = temp_summary_data[i].val*1000.0 ;
			}
			if (from == 'L' && to == 'm³') {
				temp_summary_data[i].val = temp_summary_data[i].val/1000.0 ;
			}
			if (from == 'L' && to == 'k(m³)') {
				temp_summary_data[i].val = temp_summary_data[i].val/1000000.0 ;
			}
			if (from == 'm³' && to == 'L') {
				temp_summary_data[i].val = temp_summary_data[i].val*1000.0 ;
			}
			if (from == 'm³' && to == 'k(m³)') {
				temp_summary_data[i].val = temp_summary_data[i].val/1000.0 ;
			}
			if (from == 'k(m³)' && to == 'L') {
				temp_summary_data[i].val = temp_summary_data[i].val*1000000.0 ;
			}
			if (from == 'k(m³)' && to == 'm³') {
				temp_summary_data[i].val = temp_summary_data[i].val*1000.0 ;
			}
		}
		
	}
}

function set_units_on_controles()
{
	var units=unit_show.split('_');
	for(var i=0;i<units.length;i++){
		if(units[i]=='power'){
			document.getElementById("select_power").style.display="inline";
			document.getElementById("font_power").style.display="inline";
		}
		if(units[i]=='rc'){
			document.getElementById("select_rc").style.display="inline";
			document.getElementById("font_rc").style.display="inline";
		}
		if(units[i]=='upw'){
			document.getElementById("select_upw").style.display="inline";
			document.getElementById("font_upw").style.display="inline";
		}
		if(units[i]=='cda'){
			document.getElementById("select_cda").style.display="inline";
			document.getElementById("font_cda").style.display="inline";
		}
		if(units[i]=='pn2'){
			document.getElementById("select_pn2").style.display="inline";
			document.getElementById("font_pn2").style.display="inline";
		}
		if(units[i]=='time'){
			document.getElementById("select_hms").style.display="inline";
			document.getElementById("font_hms").style.display="inline";
		}
	}
	var select_hms = document.getElementById("select_hms");
	var select_power = document.getElementById("select_power");
	var select_upw = document.getElementById("select_upw");
	var select_cda = document.getElementById("select_cda");
	var select_pn2 = document.getElementById("select_pn2");
	var select_rc = document.getElementById("select_rc");
	
	for(var i=0; i<select_rc.options.length; i++){ 
		if(select_rc.options[i].value == currentRCUnit){ 
			select_rc.options[i].selected = true; 
		}
	}
	for(var i=0; i<select_power.options.length; i++){ 
		if(select_power.options[i].value == currentPowerUnit){ 
			select_power.options[i].selected = true; 
		}
	}
	for(var i=0; i<select_cda.options.length; i++){ 
		if(select_cda.options[i].value == currentCDAUnit){ 
			select_cda.options[i].selected = true; 
		}
	}
	for(var i=0; i<select_upw.options.length; i++){ 
		if(select_upw.options[i].value == currentUPWUnit){ 
			select_upw.options[i].selected = true; 
		}
	}
	for(var i=0; i<select_pn2.options.length; i++){ 
		if(select_pn2.options[i].value == currentPN2Unit){ 
			select_pn2.options[i].selected = true; 
		}
	}
	for(var i=0; i<select_hms.options.length; i++){ 
		if(select_hms.options[i].value == currentTimeUnit){ 
			select_hms.options[i].selected = true; 
		}
	}
}

function load_data(begin, end) {
	if  (begin > end) {
		var temp=begin;
		begin = end;
		end = temp;
	}
	
	var beginYear = begin.substr(0,4)*1;
	var beginMonth = begin.substr(5,2)*1;
	var beginDay =  begin.substr(8,2)*1;
	var endYear = end.substr(0,4)*1;
	var endMonth = end.substr(5,2)*1;
	var endDay = end.substr(8,2)*1;
	
    var index = top.layer.msg('查询中...',{icon: 16, skin:is_dark?'layui-layer-lan':'', shade:0.1});
    $.ajax({
        url: path + "/gynh/factories_summary/get_total.do",
        type: 'post',
        async: false,
        data: {
        	beginYear:beginYear, 
        	beginMonth:beginMonth, 
        	beginDay:beginDay, 
        	endYear:endYear, 
        	endMonth:endMonth, 
        	endDay:endDay, 
        	param1:'',
        },
        success: function (data) {
        	temp_summary_data = data['summary'];
        	translate('Power','kWh', currentPowerUnit);
        	translate('CDA','m³', currentCDAUnit);
        	translate('UPW','m³', currentUPWUnit);
        	translate('PN2','m³', currentPN2Unit);
            load_data_impl();
        	top.layer.close(index);
        	top.layer.msg('数据加载完成', {icon: 1, skin:is_dark?'layui-layer-lan':''});
        },
        error: function () {
        	top.layer.close(index);
        	top.layer.msg('数据加载错误', {icon: 1, skin:is_dark?'layui-layer-lan':''});
        }
    });
    
    page_custom_resize();
}
