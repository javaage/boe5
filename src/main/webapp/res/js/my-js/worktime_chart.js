
var sumRunTime = ['-'];
var sumUpTime = ['-'];
var sumIdleTime = ['-'];
var sumEtcTime = ['-'];
var sumETime = ['-'];
var sumJobchgTime = ['-'];
var sumMaintTime = ['-'];
var sumDownTime = ['-'];

var sumRunTimeH = ['-'];
var sumUpTimeH = ['-'];
var sumIdleTimeH = ['-'];
var sumEtcTimeH = ['-'];
var sumETimeH = ['-'];
var sumJobchgTimeH = ['-'];
var sumMaintTimeH = ['-'];
var sumDownTimeH = ['-'];

var sumRunTimeM = ['-'];
var sumUpTimeM = ['-'];
var sumIdleTimeM = ['-'];
var sumEtcTimeM = ['-'];
var sumETimeM = ['-'];
var sumJobchgTimeM = ['-'];
var sumMaintTimeM = ['-'];
var sumDownTimeM = ['-'];

var sumTimeMap = new Map();

var currentDepartment = 'SPUTTER';
/*var currentPowerUnit = 'kWh';//kWh,mWh
var currentCDAUnit = 'L';//L,m³,k(m³)
var currentDIWUnit = 'L';//L,m³,k(m³)
var currentPN2Unit = 'L';//L,m³,k(m³)
var currentTimeUnit = 'M'; // H,M,S
*/var departmentStateMap = new Map();
departmentStateMap.set('SPUTTER', ['RUN','ETIME','IDLE','DOWN','MAINT']);


function UpdateStateSumTime() {
	var devList = ['5ATSP01','5ATSP02','5ATSP03','5ATSP04','5ATSP05','5ATSP06','5ATSP07','5ATSP08','5ATSP09','5ATSP10','5ATSP11','5ATSP12','5ATSP13'];
	
	sumRunTime.length = 0;
	sumUpTime.length = 0;
	sumIdleTime.length = 0;
	sumEtcTime.length = 0;
	sumETime.length = 0;
	sumJobchgTime.length = 0;
	sumMaintTime.length = 0;
	sumDownTime.length = 0;
	
	sumRunTimeH.length = 0;
	sumUpTimeH.length = 0;
	sumIdleTimeH.length = 0;
	sumEtcTimeH.length = 0;
	sumETimeH.length = 0;
	sumJobchgTimeH.length = 0;
	sumMaintTimeH.length = 0;
	sumDownTimeH.length = 0;

	sumRunTimeM.length = 0;
	sumUpTimeM.length = 0;
	sumIdleTimeM.length = 0;
	sumEtcTimeM.length = 0;
	sumETimeM.length = 0;
	sumJobchgTimeM.length = 0;
	sumMaintTimeM.length = 0;
	sumDownTimeM.length = 0;

	
	{	
		var eqpStateTimeMap = sumTimeMap.get('RUN');
		for (var i = 0; i < devList.length; i++) {
			var timeLong = eqpStateTimeMap.get(devList[i]);
			if ("undefined" == timeLong) {
				sumRunTime.push('-');	
				sumRunTimeH.push('-');	
				sumRunTimeM.push('-');	
			} else {
				sumRunTime.push(timeLong);
				h = timeLong/3600.0;
				sumRunTimeH.push(h.toFixed(2));	
				m = timeLong/60.0;
				sumRunTimeM.push(m.toFixed(2));	
			}
		}
	}
	{	
		var eqpStateTimeMap = sumTimeMap.get('UP');
		for (var i = 0; i < devList.length; i++) {
			var timeLong = eqpStateTimeMap.get(devList[i]);
			if ("undefined" == timeLong) {
				sumUpTime.push('-');	
				sumUpTimeH.push('-');	
				sumUpTimeM.push('-');	
			} else {
				sumUpTime.push(timeLong);	
				h = timeLong/3600.0;
				sumUpTimeH.push(h.toFixed(2));	
				m = timeLong/60.0;
				sumUpTimeM.push(m.toFixed(2));	
			}
		}
	}
	{	
		var eqpStateTimeMap = sumTimeMap.get('IDLE');
		for (var i = 0; i < devList.length; i++) {
			var timeLong = eqpStateTimeMap.get(devList[i]);
			if ("undefined" == timeLong) {
				sumIdleTime.push('-');	
				sumIdleTimeH.push('-');	
				sumIdleTimeM.push('-');	
			} else {
				sumIdleTime.push(timeLong);	
				h = timeLong/3600.0;
				sumIdleTimeH.push(h.toFixed(2));	
				m = timeLong/60.0;
				sumIdleTimeM.push(m.toFixed(2));	
			}
		}
	}
	{	
		var eqpStateTimeMap = sumTimeMap.get('ETC');
		for (var i = 0; i < devList.length; i++) {
			var timeLong = eqpStateTimeMap.get(devList[i]);
			if ("undefined" == timeLong) {
				sumEtcTime.push('-');	
				sumEtcTimeH.push('-');	
				sumEtcTimeM.push('-');	
			} else {
				sumEtcTime.push(timeLong);	
				h = timeLong/3600.0;
				sumEtcTimeH.push(h.toFixed(2));	
				m = timeLong/60.0;
				sumEtcTimeM.push(m.toFixed(2));	
			}
		}
	}
	{	
		var eqpStateTimeMap = sumTimeMap.get('ETIME');
		for (var i = 0; i < devList.length; i++) {
			var timeLong = eqpStateTimeMap.get(devList[i]);
			if ("undefined" == timeLong) {
				sumETime.push('-');	
				sumETimeH.push('-');	
				sumETimeM.push('-');	
			} else {
				sumETime.push(timeLong);	
				h = timeLong/3600.0;
				sumETimeH.push(h.toFixed(2));	
				m = timeLong/60.0;
				sumETimeM.push(m.toFixed(2));	
			}
		}
	}
	{	
		var eqpStateTimeMap = sumTimeMap.get('JOBCHG');
		for (var i = 0; i < devList.length; i++) {
			var timeLong = eqpStateTimeMap.get(devList[i]);
			if ("undefined" == timeLong) {
				sumJobchgTime.push('-');	
				sumJobchgTimeH.push('-');	
				sumJobchgTimeM.push('-');	
			} else {
				sumJobchgTime.push(timeLong);	
				h = timeLong/3600.0;
				sumJobchgTimeH.push(h.toFixed(2));	
				m = timeLong/60.0;
				sumJobchgTimeM.push(m.toFixed(2));	
			}
		}
	}
	{	
		var eqpStateTimeMap = sumTimeMap.get('MAINT');
		for (var i = 0; i < devList.length; i++) {
			var timeLong = eqpStateTimeMap.get(devList[i]);
			if ("undefined" == timeLong) {
				sumMaintTime.push('-');	
				sumMaintTimeH.push('-');	
				sumMaintTimeM.push('-');	
			} else {
				sumMaintTime.push(timeLong);	
				h = timeLong/3600.0;
				sumMaintTimeH.push(h.toFixed(2));	
				m = timeLong/60.0;
				sumMaintTimeM.push(m.toFixed(2));	
			}
		}
	}
	{	
		var eqpStateTimeMap = sumTimeMap.get('DOWN');
		for (var i = 0; i < devList.length; i++) {
			var timeLong = eqpStateTimeMap.get(devList[i]);
			if ("undefined" == timeLong) {
				sumDownTime.push('-');	
				sumDownTimeH.push('-');	
				sumDownTimeM.push('-');	
			} else {
				sumDownTime.push(timeLong);	
				h = timeLong/3600.0;
				sumDownTimeH.push(h.toFixed(2));	
				m = timeLong/60.0;
				sumDownTimeM.push(m.toFixed(2));	
			}
		}
	}
	
	reloadStateSumTime();
}

function reloadStateSumTime() {
	
	if (chartMode == ChartMode.COLUMN || chartMode == ChartMode.PIE || pageName == 'lotpoint') {
		return;
	}
	
	staticChartOption = myChart.getOption();
	
	if (currentTimeUnit == 'S') {
		staticChartOption.series[0].data = sumRunTime;
		staticChartOption.series[1].data = sumETime;
		staticChartOption.series[2].data = sumUpTime;
		staticChartOption.series[3].data = sumIdleTime;
		staticChartOption.series[4].data = sumEtcTime;
		staticChartOption.series[5].data = sumJobchgTime;
		staticChartOption.series[6].data = sumMaintTime;
		staticChartOption.series[7].data = sumDownTime;
		staticChartOption.yAxis[0].name = '时间(秒)'
	} else if (currentTimeUnit == 'M') {
		staticChartOption.series[0].data = sumRunTimeM;
		staticChartOption.series[1].data = sumETimeM;
		staticChartOption.series[2].data = sumUpTimeM;
		staticChartOption.series[3].data = sumIdleTimeM;
		staticChartOption.series[4].data = sumEtcTimeM;
		staticChartOption.series[5].data = sumJobchgTimeM;
		staticChartOption.series[6].data = sumMaintTimeM;
		staticChartOption.series[7].data = sumDownTimeM;
		staticChartOption.yAxis[0].name = '时间(分钟)'
	} else if (currentTimeUnit == 'H') {
		staticChartOption.series[0].data = sumRunTimeH;
		staticChartOption.series[1].data = sumETimeH;
		staticChartOption.series[2].data = sumUpTimeH;
		staticChartOption.series[3].data = sumIdleTimeH;
		staticChartOption.series[4].data = sumEtcTimeH;
		staticChartOption.series[5].data = sumJobchgTimeH;
		staticChartOption.series[6].data = sumMaintTimeH;
		staticChartOption.series[7].data = sumDownTimeH;
		staticChartOption.yAxis[0].name = '时间(小时)'
	}
	
	myChart.setOption(staticChartOption, true);
}

var staticChartOption = {
	legend: {
		//data: ['RUN', 'UP', 'IDLE', 'ETC', 'ETIME', 'JOBCHG', 'MAINT', 'DOWN']
		data: ['RUN', 'ETIME', 'DOWN', 'IDLE', 'MAINT'] //todo：从数据库查询
	},
	toolbox: {
		show: true,
		x: 'left',
		borderColor: '#777',
		borderWidth: 0,
		feature: {
			myStack: {
				show: true,
				title: '堆叠',
				icon: 'image://'+path+'/res/images/toolbarbtn/stack.png',
				onclick: function() {
					deleteCUD();
					stackGroupTiled(0);
					updateLegend();

				}
			},
			myGroup: {
				show: true,
				title: '分组',
				icon: 'image://'+path+'/res/images/toolbarbtn/group.png',
				onclick: function() {
					deleteCUD();
					stackGroupTiled(1);
				}
			},
			myTiled: {
				show: true,
				title: '平铺',
				icon: 'image://'+path+'/res/images/toolbarbtn/tiled.png',
				onclick: function() {
					deleteCUD();
					stackGroupTiled(2);
				}
			},
			my_DynimicChart1: {
				show: true,
				title: '返回动态图',
				icon: 'image://'+path+'/res/images/toolbarbtn/return.png',
				onclick: function () {
					deleteCUD();
					chartMode = ChartMode.COLUMN;
					myChart.clear();
					deleteCUD();
					myChart.setOption(optionForColumnChart);
				}
			},
			saveAsImage: {
				show: false
			},
		}
	},
	calculable: false,
	yAxis: [{
		type: 'value',
		name: '时间（秒）'
	}],
	xAxis: [{
		type: 'category',
		name: '设备',
		data: ['5ATSP01','5ATSP02','5ATSP03','5ATSP04','5ATSP05','5ATSP06','5ATSP07','5ATSP08','5ATSP09','5ATSP10','5ATSP11','5ATSP12','5ATSP13']
	}],
	series: [
		{
			name: 'RUN',
			type: 'bar',
			stack: ' ',
			itemStyle: {
				normal: {
					color:'springgreen',
					label: {
						textStyle: {color:'blue'},
						show: true,
						position: 'insideRight'
					},
				}
			},
			data: ['-']
		}
		,
		{
			name: 'ETIME',
			type: 'bar',
			stack: ' ',
			itemStyle: {
				normal: {
					color:'skyblue',
					label: {
						textStyle: {color:'blue'},
						show: true,
						position: 'insideRight'
					}
				}
			},
			data: ['-']
		}
		,
		{
			name: 'UP',
			type: 'bar',
			stack: ' ',
			itemStyle: {
				normal: {
					color:'lawngreen',
					label: {
						textStyle: {color:'blue'},
						show: true,
						position: 'insideRight'
					}
				}
			},
			data: ['-']
		}
		,
		{
			name: 'IDLE',
			type: 'bar',
			stack: ' ',
			itemStyle: {
				normal: {
					color:'silver',
					label: {
						textStyle: {color:'blue'},
						show: true,
						position: 'insideRight'
					}
				}
			},
			data: ['-']
		}
		,
		{
			name: 'ETC',
			type: 'bar',
			stack: ' ',
			itemStyle: {
				normal: {
					color:'navajowhite',
					label: {
						textStyle: {color:'blue'},
						show: true,
						position: 'insideRight'
					}
				}
			},
			data: ['-']
		}
		,
		{
			name: 'JOBCHG',
			type: 'bar',
			stack: ' ',
			itemStyle: {
				normal: {
					color:'olivedrab',
					label: {
						textStyle: {color:'blue'},
						show: true,
						position: 'insideRight'
					}
				}
			},
			data: ['-']
		}
		,
		{
			name: 'MAINT',
			type: 'bar',
			stack: ' ',
			itemStyle: {
				normal: {
					color:'red',
					label: {
						textStyle: {color:'blue'},
						show: true,
						position: 'insideRight'
					}
				}
			},
			data: ['-']
		}
		,
		{
			name: 'DOWN',
			type: 'bar',
			stack: ' ',
			itemStyle: {
				normal: {
					color:'darkorange',
					label: {
						textStyle: {color:'blue'},
						show: true,
						position: 'insideRight'
					}
				}
			},
			data: ['-']
		}
	]
};

function initStaticChart() {
	staticChartOption.legend.data = departmentStateMap.get(currentDepartment);
	myChart.setOption(staticChartOption, true);
	chartMode = ChartMode.STATIC;
}

// 0: stack 1:group 2:tiled
function stackGroupTiled(type) {
	staticChartOption = myChart.getOption();
	if (type == 0) {
		staticChartOption.series[0].stack = ' ';
		staticChartOption.series[1].stack = ' ';
		staticChartOption.series[2].stack = ' ';
		staticChartOption.series[3].stack = ' ';
		staticChartOption.series[4].stack = ' ';
		staticChartOption.series[5].stack = ' ';
		staticChartOption.series[6].stack = ' ';
		staticChartOption.series[7].stack = ' ';
	} else if (type == 1) {
		staticChartOption.series[0].stack = 'RUN';
		staticChartOption.series[1].stack = 'RUN';
		staticChartOption.series[2].stack = 'RUN';
		staticChartOption.series[3].stack = 'PM';
		staticChartOption.series[4].stack = 'PM';
		staticChartOption.series[5].stack = 'PM';
		staticChartOption.series[6].stack = 'PM';
		staticChartOption.series[7].stack = 'PM';
	} else if (type == 2) {
		staticChartOption.series[0].stack = '';
		staticChartOption.series[1].stack = '';
		staticChartOption.series[2].stack = '';
		staticChartOption.series[3].stack = '';
		staticChartOption.series[4].stack = '';
		staticChartOption.series[5].stack = '';
		staticChartOption.series[6].stack = '';
		staticChartOption.series[7].stack = '';
	}
	myChart.setOption(staticChartOption, true);
};

function updateLegend() {
	staticChartOption = myChart.getOption();
	staticChartOption.legend.data = departmentStateMap.get(currentDepartment);
	myChart.setOption(staticChartOption, true);
};

function loadOtherData(data) {
	return;//todo
	if (pageName == 'lotpoint' || pageName == 'lotunitcons') {
		return;
	}
	stateTimeSum = data['state_time_sum'];
	
	translateTime('S', currentTimeUnit);
	
	var sumRunTimeMap = new Map();
	var sumUpTimeMap = new Map();
	var sumIdleTimeMap = new Map();
	var sumEtcTimeMap = new Map();
	var sumETimeMap = new Map();
	var sumJobchgTimeMap = new Map();
	var sumMaintTimeMap = new Map();
	var sumDownTimeMap = new Map();
	
	for (var i = 0; i < stateTimeSum.length; i++) {
		if (stateTimeSum[i].state.toUpperCase() == "RUN") {
			sumRunTimeMap.set(stateTimeSum[i].eqp_id, stateTimeSum[i].time_long);
		} else if (stateTimeSum[i].state.toUpperCase() == "UP") {
			sumUpTimeMap.set(stateTimeSum[i].eqp_id, stateTimeSum[i].time_long);
		} else if (stateTimeSum[i].state.toUpperCase() == "IDLE") {
			sumIdleTimeMap.set(stateTimeSum[i].eqp_id, stateTimeSum[i].time_long);
		} else if (stateTimeSum[i].state.toUpperCase() == "ETC") {
			sumEtcTimeMap.set(stateTimeSum[i].eqp_id, stateTimeSum[i].time_long);
		} else if (stateTimeSum[i].state.toUpperCase() == "ETIME") {
			sumETimeMap.set(stateTimeSum[i].eqp_id, stateTimeSum[i].time_long);
		} else if (stateTimeSum[i].state.toUpperCase() == "JOBCHG") {
			sumJobchgTimeMap.set(stateTimeSum[i].eqp_id, stateTimeSum[i].time_long);
		} else if (stateTimeSum[i].state.toUpperCase() == "MAINT") {
			sumMaintTimeMap.set(stateTimeSum[i].eqp_id, stateTimeSum[i].time_long);
		} else if (stateTimeSum[i].state.toUpperCase() == "DOWN") {
			sumDownTimeMap.set(stateTimeSum[i].eqp_id, stateTimeSum[i].time_long);
		} else if (stateTimeSum[i].state.substr(0,4).toUpperCase() == "IDLE") {
			sumIdleTimeMap.set(stateTimeSum[i].eqp_id, stateTimeSum[i].time_long);
		}
	}

	sumTimeMap.set("RUN", sumRunTimeMap);
	sumTimeMap.set("UP", sumUpTimeMap);
	sumTimeMap.set("IDLE", sumIdleTimeMap);
	sumTimeMap.set("ETC", sumEtcTimeMap);
	sumTimeMap.set("ETIME", sumETimeMap);
	sumTimeMap.set("JOBCHG", sumJobchgTimeMap);
	sumTimeMap.set("MAINT", sumMaintTimeMap);
	sumTimeMap.set("DOWN", sumDownTimeMap);
	
	UpdateStateSumTime();

}

