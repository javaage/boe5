var endtime=getTimeDate(0);

var begintime=endtime.substring(0,10)+" 00:00:00";

begintime=bjtime_to_gmt(begintime);
endtime=bjtime_to_gmt(endtime);

var date_show = false;
var cold_data=new Array();
var flow_data=new Array();
var temperature_data=new Array();
var date_time_data= new Array();

function button_click() {
	var mesotherm_dlg = create_modal_dlg("#mesotherm_dlg_div", "#mesotherm_dlg_close_button");
	mesotherm_dlg.init();
	mesotherm_dlg.open();
    	
}

function get_mesotherm() {  
	var energy='MCHW';
	var node = "#1-1";
	begintime="2019-03-06 00:00:04";
	//endtime="2019-03-06 03:30:04";
	endtime="2019-03-06 04:30:04";
	$.ajax({
        //url: path + '/rcpointsvalue/rcvalue/get_data.do',
        url: path + '/tmeperature/medium/get_temperatire.do',
        type: 'get',
        data:{
        	beginDateTime:begintime,
        	endDateTime:endtime,
        	energy:energy,
        	node:node
        	},
        async: false,
        success: function (data) {
        	var data_list=new Array();
        	data_list=data['data'];
        	var temp = new Array();
        	for(var i=0;i<data_list.length;i++){
        		temp = [];
    			temp.push(data_list[i].t);
    			temp.push(data_list[i].totalrc);
    			cold_data.push({value:temp});
    			
    			temp = [];
    			temp.push(data_list[i].t);
    			temp.push(data_list[i].m3h);
    			flow_data.push({value:temp});
    			
    			temp = [];
    			temp.push(data_list[i].t);
    			temp.push((data_list[i].td0c).toFixed(2));
    			temperature_data.push({value:temp});
        	}
        	cold_chart_option.series[0].data=cold_data;
        	flow_chart_option.series[0].data=flow_data;
        	temperature_chart_option.series[0].data=temperature_data;
        	
        	cold_chart.setOption(cold_chart_option,true);
    		flow_chart.setOption(flow_chart_option,true);
    		temperature_chart.setOption(temperature_chart_option,true);
        	
        }
    });
}
function one_month() {
	endtime=getTimeDate(0);
	endtime=endtime.substring(0,10)+" 00:00:00";
	begintime=getTimeDate2(begintime,-24*30);
	begintime=bjtime_to_gmt(begintime);
	endtime=bjtime_to_gmt(endtime);
	get_mesotherm();
}
function one_week() {
	endtime=getTimeDate(0);
	endtime=endtime.substring(0,10)+" 00:00:00";
	begintime=getTimeDate2(begintime,-24*7);
	begintime=bjtime_to_gmt(begintime);
	endtime=bjtime_to_gmt(endtime);
	get_mesotherm();
}
function yesterday() {
	endtime=getTimeDate(0);
	endtime=endtime.substring(0,10)+" 00:00:00";
	begintime=getTimeDate2(begintime,-24);
	begintime=bjtime_to_gmt(begintime);
	endtime=bjtime_to_gmt(endtime);
	get_mesotherm();
}
function today() {
	endtime=getTimeDate(0);
	begintime=endtime.substring(0,10)+" 00:00:00";
	begintime=bjtime_to_gmt(begintime);
	endtime=bjtime_to_gmt(endtime);
	get_mesotherm();
}
function custom() {
	if(date_show) {
		document.getElementById("date").style.display = 'none';
		date_show = false;
	}else {
		document.getElementById("date").style.display = 'inline';
		date_show = true;
	}
}


layui.use('laydate', function(){
	  var laydate = layui.laydate;
	  laydate.render({
	    elem: '#date',
	    range: true,
	    done: function(value) {
	    	begintime=value.substr(0,10)+" 00:00:00";
	    	endtime=value.substr(13,10)+" 00:00:00";
	    	begintime=bjtime_to_gmt(begintime);
	    	endtime=bjtime_to_gmt(endtime);

	    	get_mesotherm();
		}
	  });
});