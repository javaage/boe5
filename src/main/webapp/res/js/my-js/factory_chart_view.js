var currentPowerUnit = 'kWh';// kWh,mWh
var currentCDAUnit = 'L';// L,m³,k(m³)
var currentUPWUnit = 'L';// L,m³,k(m³)
var currentPN2Unit = 'L';// L,m³,k(m³)
var currentTimeUnit = 'M';
var chart_unit;

var is_daily_chart1=false;
var is_daily_chart2=false;
var is_hourly_chart1=false;
var is_hourly_chart2=false;


function set_units_on_controles() {
	
	var select_hms = document.getElementById("select_hms");
	var select_power = document.getElementById("select_power");
	var select_upw = document.getElementById("select_upw");
	var select_cda = document.getElementById("select_cda");
	var select_pn2 = document.getElementById("select_pn2");
	for (var i=0; i<select_power.options.length; i++) { 
		if (select_power.options[i].value == currentPowerUnit) { 
			select_power.options[i].selected = true; 
		}
	}
	for (var i=0; i<select_cda.options.length; i++) { 
		if(select_cda.options[i].value == currentCDAUnit) { 
			select_cda.options[i].selected = true; 
		}
	}
	for (var i=0; i<select_upw.options.length; i++) { 
		if (select_upw.options[i].value == currentUPWUnit) { 
			select_upw.options[i].selected = true; 
		}
	}
	for (var i=0; i<select_pn2.options.length; i++) { 
		if (select_pn2.options[i].value == currentPN2Unit) { 
			select_pn2.options[i].selected = true; 
		}
	}
	for (var i=0; i<select_hms.options.length; i++) { 
		if (select_hms.options[i].value == currentTimeUnit) { 
			select_hms.options[i].selected = true; 
		}
	}
}

function powerUnitChange(e) {
	var unit = e.value;
	translate_unit('Power',currentPowerUnit, unit);
	currentPowerUnit=unit;
	echarts_unit();
	daily_chart1.setOption(daily_chart1_option, true);
	daily_chart2.setOption(daily_chart2_option, true);
	hourly_chart1.setOption(hourly_chart1_option, true);
	hourly_chart2.setOption(hourly_chart2_option, true);
}
function UnitChange(e) {
	var unit = e.value;
	if (e.id == 'select_upw') {
		translate_unit('UPW',currentUPWUnit, unit);
		currentUPWUnit=unit;
	}
	if (e.id == 'select_cda') {
		translate_unit('CDA',currentCDAUnit, unit);
		currentCDAUnit=unit;
	}
	if (e.id == 'select_pn2') {
		translate_unit('PN2',currentPN2Unit, unit);
		currentPN2Unit=unit;
	}
	echarts_unit();
	daily_chart1.setOption(daily_chart1_option, true);
	daily_chart2.setOption(daily_chart2_option, true);
	hourly_chart1.setOption(hourly_chart1_option, true);
	hourly_chart2.setOption(hourly_chart2_option, true);

}
function timeUnitChange(e) {
	var unit = e.value;
	translate_unit('Time',currentTimeUnit, unit);
	currentTimeUnit=unit;
	echarts_unit();
	hourly_chart1.setOption(hourly_chart1_option, true);
	hourly_chart2.setOption(hourly_chart2_option, true);
	daily_chart1.setOption(daily_chart1_option, true);
	daily_chart2.setOption(daily_chart2_option, true);
}

function translate_unit(params,from,to)
{
	var data;
	var hourly_data;
	var unit_data;
	var hourly_unit_data;
	
	if (params=="Power") {
		data=energy_serie_array.Power;
		hourly_data=hourly_energy_serie_array.Power;
		unit_data=unit_cons_serie_array.Power;
		hourly_unit_data=hourly_unit_cons_serie_array.Power;
	} else if (params=="UPW") {
		data=energy_serie_array.UPW;
		hourly_data=hourly_energy_serie_array.UPW;
		unit_data=unit_cons_serie_array.UPW;
		hourly_unit_data=hourly_unit_cons_serie_array.UPW;
	} else if (params=="CDA") {
		data=energy_serie_array.CDA;
		hourly_data=hourly_energy_serie_array.CDA;
		unit_data=unit_cons_serie_array.CDA;
		hourly_unit_data=hourly_unit_cons_serie_array.CDA;
	} else if (params=="PN2") {
		data=energy_serie_array.PN2;
		hourly_data=hourly_energy_serie_array.PN2;
		unit_data=unit_cons_serie_array.PN2;
		hourly_unit_data=hourly_unit_cons_serie_array.PN2;
	} else if (params=="Time") {
		data=state_time_serie_array;
		hourly_data=hourly_state_time_serie_array;
		unit_data=new Array();
		hourly_unit_data=new Array();
	}  
	for (var i = 0; i < data.length; i++) {
		if (data[i].data==null||data[i].data=="") {
			continue;
		}
		for (var j = 0; j < data[i].data.length; j++) {
			if (from == 'kWh' && to == 'mWh') {
				data[i].data[j] = data[i].data[j]/1000.0 ;
			}
			if (from == 'mWh' && to == 'kWh') {
				data[i].data[j] = data[i].data[j]*1000.0 ;
			}
			if (from == 'L' && to == 'm³') {
				data[i].data[j] = data[i].data[j]/1000.0 ;
			}
			if (from == 'L' && to == 'k(m³)') {
				data[i].data[j] = data[i].data[j]/1000000.0 ;
			}
			if (from == 'm³' && to == 'L') {
				data[i].data[j] = data[i].data[j]*1000.0 ;
			}
			if (from == 'm³' && to == 'k(m³)') {
				data[i].data[j] = data[i].data[j]/1000.0 ;
			}
			if (from == 'k(m³)' && to == 'L') {
				data[i].data[j] = data[i].data[j]*1000000.0 ;
			}
			if (from == 'k(m³)' && to == 'm³') {
				data[i].data[j] = data[i].data[j]*1000.0 ;
			}
			if (from == 'S' && to == 'H') {
				data[i].data[j] = data[i].data[j]/3600.0  + 0.0000000001;
			}  
			if (from == 'S' && to == 'M') {
				data[i].data[j] = data[i].data[j]/60.0  + 0.0000000001;
			}  
			if (from == 'M' && to == 'H') {
				data[i].data[j] = data[i].data[j]/60.0  + 0.0000000001;	
			} 
			if (from == 'M' && to == 'S') {
				data[i].data[j] = data[i].data[j]*60.0  + 0.0000000001;
			}  
			if (from == 'H' && to == 'M') {
				data[i].data[j] = data[i].data[j]*60.0  + 0.0000000001;
			} 
			if (from == 'H' && to == 'S') {
				data[i].data[j] = data[i].data[j]*3600.0  + 0.0000000001;
			}
		}
	}
	for (var i = 0; i < hourly_data.length; i++) {
		if (hourly_data[i].data==null||hourly_data[i].data=="") {
			continue;
		}
		for (var j = 0; j < hourly_data[i].data.length; j++) {
			if (from == 'kWh' && to == 'mWh') {
				hourly_data[i].data[j] = hourly_data[i].data[j]/1000.0 ;
			}
			if (from == 'mWh' && to == 'kWh') {
				hourly_data[i].data[j] = hourly_data[i].data[j]*1000.0 ;
			}
			if (from == 'L' && to == 'm³') {
				hourly_data[i].data[j] = hourly_data[i].data[j]/1000.0 ;
			}
			if (from == 'L' && to == 'k(m³)') {
				hourly_data[i].data[j] = hourly_data[i].data[j]/1000000.0 ;
			}
			if (from == 'm³' && to == 'L') {
				hourly_data[i].data[j] = hourly_data[i].data[j]*1000.0 ;
			}
			if (from == 'm³' && to == 'k(m³)') {
				hourly_data[i].data[j] = hourly_data[i].data[j]/1000.0 ;
			}
			if (from == 'k(m³)' && to == 'L') {
				hourly_data[i].data[j] = hourly_data[i].data[j]*1000000.0 ;
			}
			if (from == 'k(m³)' && to == 'm³') {
				hourly_data[i].data[j] = hourly_data[i].data[j]*1000.0 ;
			}
			if (from == 'S' && to == 'H') {
				hourly_data[i].data[j] = hourly_data[i].data[j]/3600.0  + 0.0000000001;
			}  
			if (from == 'S' && to == 'M') {
				hourly_data[i].data[j] = hourly_data[i].data[j]/60.0  + 0.0000000001;
			}  
			if (from == 'M' && to == 'H') {
				hourly_data[i].data[j] = hourly_data[i].data[j]/60.0  + 0.0000000001;	
			} 
			if (from == 'M' && to == 'S') {
				hourly_data[i].data[j] = hourly_data[i].data[j]*60.0  + 0.0000000001;
			}  
			if (from == 'H' && to == 'M') {
				hourly_data[i].data[j] = hourly_data[i].data[j]*60.0  + 0.0000000001;
			} 
			if (from == 'H' && to == 'S') {
				hourly_data[i].data[j] = hourly_data[i].data[j]*3600.0  + 0.0000000001;
			}
		}
	}
	for (var i = 0; i < unit_data.length; i++) {
		if (unit_data[i].data==null||unit_data[i].data=="") {
			continue;
		}
		for (var j = 0; j < unit_data[i].data.length; j++) {
			if (from == 'kWh' && to == 'mWh') {
				unit_data[i].data[j] = unit_data[i].data[j]/1000.0 ;
			}
			if (from == 'mWh' && to == 'kWh') {
				unit_data[i].data[j] = unit_data[i].data[j]*1000.0 ;
			}
			if (from == 'L' && to == 'm³') {
				unit_data[i].data[j] = unit_data[i].data[j]/1000.0 ;
			}
			if (from == 'L' && to == 'k(m³)') {
				unit_data[i].data[j] = unit_data[i].data[j]/1000000.0 ;
			}
			if (from == 'm³' && to == 'L') {
				unit_data[i].data[j] = unit_data[i].data[j]*1000.0 ;
			}
			if (from == 'm³' && to == 'k(m³)') {
				unit_data[i].data[j] = unit_data[i].data[j]/1000.0 ;
			}
			if (from == 'k(m³)' && to == 'L') {
				unit_data[i].data[j] = unit_data[i].data[j]*1000000.0 ;
			}
			if (from == 'k(m³)' && to == 'm³') {
				unit_data[i].data[j] = unit_data[i].data[j]*1000.0 ;
			}
			if (from == 'S' && to == 'H') {
				unit_data[i].data[j] = unit_data[i].data[j]/3600.0  + 0.0000000001;
			}  
			if (from == 'S' && to == 'M') {
				unit_data[i].data[j] = unit_data[i].data[j]/60.0  + 0.0000000001;
			}  
			if (from == 'M' && to == 'H') {
				unit_data[i].data[j] = unit_data[i].data[j]/60.0  + 0.0000000001;	
			} 
			if (from == 'M' && to == 'S') {
				unit_data[i].data[j] = unit_data[i].data[j]*60.0  + 0.0000000001;
			}  
			if (from == 'H' && to == 'M') {
				unit_data[i].data[j] = unit_data[i].data[j]*60.0  + 0.0000000001;
			} 
			if (from == 'H' && to == 'S') {
				unit_data[i].data[j] = unit_data[i].data[j]*3600.0  + 0.0000000001;
			}
		}
	}
	for (var i = 0; i < hourly_unit_data.length; i++) {
		if (hourly_unit_data[i].data==null||hourly_unit_data[i].data=="") {
			continue;
		}
		for (var j = 0; j < hourly_unit_data[i].data.length; j++) {
			if (from == 'kWh' && to == 'mWh') {
				hourly_unit_data[i].data[j] = hourly_unit_data[i].data[j]/1000.0 ;
			}
			if (from == 'mWh' && to == 'kWh') {
				hourly_unit_data[i].data[j] = hourly_unit_data[i].data[j]*1000.0 ;
			}
			if (from == 'L' && to == 'm³') {
				hourly_unit_data[i].data[j] = hourly_unit_data[i].data[j]/1000.0 ;
			}
			if (from == 'L' && to == 'k(m³)') {
				hourly_unit_data[i].data[j] = hourly_unit_data[i].data[j]/1000000.0 ;
			}
			if (from == 'm³' && to == 'L') {
				hourly_unit_data[i].data[j] = hourly_unit_data[i].data[j]*1000.0 ;
			}
			if (from == 'm³' && to == 'k(m³)') {
				hourly_unit_data[i].data[j] = hourly_unit_data[i].data[j]/1000.0 ;
			}
			if (from == 'k(m³)' && to == 'L') {
				hourly_unit_data[i].data[j] = hourly_unit_data[i].data[j]*1000000.0 ;
			}
			if (from == 'k(m³)' && to == 'm³') {
				hourly_unit_data[i].data[j] = hourly_unit_data[i].data[j]*1000.0 ;
			}
			if (from == 'S' && to == 'H') {
				hourly_unit_data[i].data[j] = hourly_unit_data[i].data[j]/3600.0  + 0.0000000001;
			}  
			if (from == 'S' && to == 'M') {
				hourly_unit_data[i].data[j] = hourly_unit_data[i].data[j]/60.0  + 0.0000000001;
			}  
			if (from == 'M' && to == 'H') {
				hourly_unit_data[i].data[j] = hourly_unit_data[i].data[j]/60.0  + 0.0000000001;	
			} 
			if (from == 'M' && to == 'S') {
				hourly_unit_data[i].data[j] = hourly_unit_data[i].data[j]*60.0  + 0.0000000001;
			}  
			if (from == 'H' && to == 'M') {
				hourly_unit_data[i].data[j] = hourly_unit_data[i].data[j]*60.0  + 0.0000000001;
			} 
			if (from == 'H' && to == 'S') {
				hourly_unit_data[i].data[j] = hourly_unit_data[i].data[j]*3600.0  + 0.0000000001;
			}
		}
	}
	if (params=="Power") {
		energy_serie_array.Power=data;
		hourly_energy_serie_array.Power=hourly_data;
		hourly_unit_cons_serie_array.Power=hourly_unit_data
		unit_cons_serie_array.Power=unit_data
	} else if (params=="UPW") {
		energy_serie_array.UPW=data;
		hourly_energy_serie_array.UPW=hourly_data;
		hourly_unit_cons_serie_array.UPW=hourly_unit_data
		unit_cons_serie_array.UPW=unit_data
	} else if (params=="CDA") {
		energy_serie_array.CDA=data;
		hourly_energy_serie_array.CDA=hourly_data;
		hourly_unit_cons_serie_array.CDA=hourly_unit_data
		unit_cons_serie_array.CDA=unit_data
	} else if (params=="PN2") {
		energy_serie_array.PN2=data;
		hourly_energy_serie_array.PN2=hourly_data;
		hourly_unit_cons_serie_array.PN2=hourly_unit_data
		unit_cons_serie_array.PN2=unit_data
	} else if (params=="Time") {
		state_time_serie_array=data;
		hourly_state_time_serie_array=hourly_data;
	}  
}

var shortcuts;
// var my_colors = ['SpringGreen', 'Tomato', 'Yellow', 'DodgerBlue', 'Sienna', 'MediumOrchid', 'Orange', 'SkyBlue', 'MediumTurquoise', 'Indigo'];
var my_colors = ['#006666', '#2F75B5', '#8EA9DB', '#548235', '#00B050', '#92D050', '#C6E0B4', 'SkyBlue', 'MediumTurquoise', 'Indigo'];
//var my_colors = [
//	'SpringGreen',
//	'Tomato',
//	'SteelBlue',
//	'Tan',
//	'Teal',
//	'Thistle',
//	'Turquoise',
//	'Violet',
//	'Wheat',
//	'Yellow',
//	'MediumAquaMarine',
//	'MediumBlue',
//	'MediumOrchid',
//	'MediumSeaGreen',
//	'MediumSlateBlue',
//	'MediumVioletRed',
//	'LightSteelBlue',
//	'LightSalmon',
//	'GoldenRod',
//];
var hourly = false;
var group_by_node = false; //group by series type

function init_date_box() {
	layui.use('laydate', function(){
		var laydate = layui.laydate;
		// 日期范围
		laydate.render({
			elem: '#daily_box',
			range: true,
			theme: is_dark?'#444444':'',
			type: 'date',
			value: start_date + ' - ' + end_date,
			done: function(value) {
				start_date = value.substr(0,10);
				end_date = value.substr(13,10);
				load_daily_data();
			}
		});
	});

	layui.use('laydate', function(){
		var laydate = layui.laydate;
		// 日期范围
		laydate.render({
			elem: '#hourly_box',
			range: false,
			theme: is_dark?'#444444':'',
			type: 'date',
			value: hourly_date,
			done: function(value) {
				hourly_date = value;
				load_hourly_data();
			}
		});
	});
}

init_date_box();

//var series_box;
//var factory_box;
var department_box;
var line_box;
var device_box;
var device_unit_box;
// var point_box;

var department_list = '';
var product_line_list = ''; 
var product_line_array = [];
var device_list = '';
var device_unit_list = '';

//var eqp_long_name = {};

var state_colors = {
	RUN: '#00B050',	
	UP: '#00CC00',	
	ETIME: '#8EA9DB',	
	IDLE: '#808080',	
	ETC: '#C65911',	
	JOBCHG: '#0070C0',	
	DOWN: '#C00000',	
	MAINT: '#FFFF00',	
};

var state_list = [
	"MAINT",
	"ETC",
	"RUN",
	"UP",
	"ETIME",
	"IDLE",
	"DOWN",
	"JOBCHG",
	];

var state_color = [
	"#FFFF00",
	"#C65911",
	"#00B050",
	"#00CC00",
	"#8EA9DB",
	"#808080",
	"#C00000",
	"#0070C0",
	];


var energy_names = ["Power", "UPW", "CDA", "PN2"];

var energy_serie_array = {Power:[],UPW:[],CDA:[],PN2:[]};
var unit_cons_serie_array = {Power:[],UPW:[],CDA:[],PN2:[]};
var output_serie_array = [];
var state_time_serie_array = [];
var cur_energy = 'Power';

var hourly_energy_serie_array = {Power:[],UPW:[],CDA:[],PN2:[]};
var hourly_unit_cons_serie_array = {Power:[],UPW:[],CDA:[],PN2:[]};
var hourly_output_serie_array = [];
var hourly_state_time_serie_array = [];

var legend_data = [];
var hourly_legend_data = [];
var period = 0;
var hourly_period = 0;
//var select_chart_series = ["能耗","产量","单耗","状态时间"];

var daily_chart1_types = {
	energy:true,
	output:true,
	unit_cons:true,
	state_time:false,
};

var daily_chart2_types = {
	energy:true,
	output:false,
	unit_cons:false,
	state_time:true,
};

var hourly_chart1_types = {
	energy:true,
	output:true,
	unit_cons:true,
	state_time:false,
};

var hourly_chart2_types = {
	energy:true,
	output:false,
	unit_cons:false,
	state_time:true,
};

var daily_node_set = new Set();
var hourly_node_set = new Set();

var daily_node_output_series = {};
var hourly_node_output_series = {};
var daily_node_cons_series = {Power:{},UPW:{},CDA:{},PN2:{}};
var hourly_node_cons_series = {Power:{},UPW:{},CDA:{},PN2:{}};
var daily_node_unit_cons_series = {Power:{},UPW:{},CDA:{},PN2:{}};
var hourly_node_unit_cons_series = {Power:{},UPW:{},CDA:{},PN2:{}};
var daily_node_state_time_series = {};
var hourly_node_state_time_series = {};

$(function() {
//	series_box = $("#series_box").dxTagBox({
//        items: ["能耗","产量","单耗","状态时间"],
//        value: ["能耗","产量","单耗","状态时间"],
//        multiline: false,
//        // applyValueMode: "useButtons",
//        showSelectionControls: true,
//        showMultiTagOnly: false,
//        // maxDisplayedTags: 2,
//        height:24,
//        placeholder:"显示内容",
//        selectAllText:"全选",
//        searchEnabled: true,
//		onValueChanged: function(args) {
//			select_chart_series = args.value;
//			update_chart_series_items();
//			update_chart_series_items_hourly();
//			daily_chart1.setOption(daily_chart1_option, true);
//		},
//    }).dxTagBox("instance");
//
//	factory_box = $("#factory_box").dxTagBox({
//        items: [],
//        value: [],
//        multiline: false,
//        // applyValueMode: "useButtons",
//        showSelectionControls: true,
//        showMultiTagOnly: false,
//        maxDisplayedTags: 3,
//        height:24,
//        placeholder:"分厂",
//        selectAllText:"全选",
//        searchEnabled: true,
//		onValueChanged: function(args) {
//			factory_list = sel_items_to_list(args.value);
//			if (factory_list != "") {
//				get_department_list();
//			}
//		},
//    }).dxTagBox("instance");
	
	
	department_box = $("#department_box").dxTagBox({
        items: [],
        value: [],
        multiline: false,
      // applyValueMode: "useButtons",
        showSelectionControls: true,
        showMultiTagOnly: false,
        maxDisplayedTags: 3,
        height:24,
        placeholder:"科室",
        selectAllText:"全选",
        searchEnabled: true,
		onValueChanged: function(args) {
			department_list = sel_items_to_list(args.value);
			if (department_list != "") {
				get_product_line_list();
			}
		},
    }).dxTagBox("instance");

	line_box = $("#line_box").dxTagBox({
        items: [],
        value: [],
        multiline: false,
      // applyValueMode: "useButtons",
        showSelectionControls: true,
        showMultiTagOnly: false,
        maxDisplayedTags: 3,
        height:24,
        placeholder:"产线",
        selectAllText:"全选",
        searchEnabled: true,
		onValueChanged: function(args) {
			product_line_list = sel_items_to_list(args.value);
			product_line_array = args.value;
			if (product_line_list != "") {
				get_device_list();
			}
		},
    }).dxTagBox("instance");

	device_box = $("#device_box").dxTagBox({
        items: [],
        value: [],
        multiline: false,
      // applyValueMode: "useButtons",
        showSelectionControls: true,
        showMultiTagOnly: false,
        maxDisplayedTags: 3,
        height:24,
        placeholder:"设备",
        selectAllText:"全选",
        searchEnabled: true,
		onValueChanged: function(args) {
			device_list = sel_items_to_list(args.value);
			if (device_list != "") {
				get_device_unit_list();
			}
		},
    }).dxTagBox("instance");

	device_unit_box = $("#device_unit_box").dxTagBox({
        items: [],
        value: [],
        multiline: false,
      // applyValueMode: "useButtons",
        showSelectionControls: true,
        showMultiTagOnly: false,
        maxDisplayedTags: 3,
        height:24,
        placeholder:"设备单元",
        selectAllText:"全选",
        searchEnabled: true,
		onValueChanged: function(args) {
			device_unit_list = sel_items_to_list(args.value);
// if (device_unit_list != "") {
// get_point_list();
// }
		},
    }).dxTagBox("instance");

// point_box = $("#point_box").dxTagBox({
// items: [],
// value: [],
// multiline: false,
// //applyValueMode: "useButtons",
// showSelectionControls: true,
// showMultiTagOnly: false,
// maxDisplayedTags: 2,
// height:24,
// placeholder:"表计",
// selectAllText:"全选",
// searchEnabled: true,
// onValueChanged: function(args) {
// point_list = sel_items_to_list(args.value);
// },
// }).dxTagBox("instance");


});

var timerId=0;
window.onload = function() {
// loadSnapshot();
// timerId = self.setInterval("clock()",1000);
};

$(document).ready(function() {
	
	//get_factory_list();
	get_department_list();
	load_default_shortcut();
	echarts_unit();
	daily_chart1.setOption(daily_chart1_option, true);
	daily_chart2.setOption(daily_chart2_option, true);
	hourly_chart1.setOption(hourly_chart1_option, true);
	hourly_chart2.setOption(hourly_chart2_option, true);
	resize_screen();

});

window.onresize = function() {
	resize_screen();
}

window.onbeforeunload= function(event) {
}

window.onunload = function(event) {
	SaveSnapshot(pageName, '', 'icon_path_1', true);
}
function energy_chart_show(chart){
	if(chart=="daily_chart1"){
		if(daily_chart1_types.energy){
			daily_chart1_types.energy=false;
			daily_chart1_option.toolbox.feature.my_energy_chart.icon='image://'+path+'/res/images/toolbarbtn/energy_off_btn.png';
		}else{
			daily_chart1_types.energy=true;
			daily_chart1_option.toolbox.feature.my_energy_chart.icon='image://'+path+'/res/images/toolbarbtn/energy_btn.png';
		}
		is_daily_chart1=true;
		update_chart_series_items2();
		daily_chart1.setOption(daily_chart1_option, true);
		return;		
	}
	if(chart=="daily_chart2"){
		if(daily_chart2_types.energy){
			daily_chart2_types.energy=false;
			daily_chart2_option.toolbox.feature.my_energy_chart.icon='image://'+path+'/res/images/toolbarbtn/energy_off_btn.png';
		}else{
			daily_chart2_types.energy=true;
			daily_chart2_option.toolbox.feature.my_energy_chart.icon='image://'+path+'/res/images/toolbarbtn/energy_btn.png';
		}
		is_daily_chart2=true;
		update_chart_series_items2();
		daily_chart2.setOption(daily_chart2_option, true);
		return;
	}
	if(chart=="hourly_chart1"){
		if(hourly_chart1_types.energy){
			hourly_chart1_types.energy=false;
			hourly_chart1_option.toolbox.feature.my_energy_chart.icon='image://'+path+'/res/images/toolbarbtn/energy_off_btn.png';
		}else{
			hourly_chart1_types.energy=true;
			hourly_chart1_option.toolbox.feature.my_energy_chart.icon='image://'+path+'/res/images/toolbarbtn/energy_btn.png';
		}
		
	}
	if(chart=="hourly_chart2"){
		if(hourly_chart2_types.energy){
			hourly_chart2_types.energy=false;
			hourly_chart2_option.toolbox.feature.my_energy_chart.icon='image://'+path+'/res/images/toolbarbtn/energy_off_btn.png';
		}else{
			hourly_chart2_types.energy=true;
			hourly_chart2_option.toolbox.feature.my_energy_chart.icon='image://'+path+'/res/images/toolbarbtn/energy_btn.png';
		}
		
	}
	charts_is_show(chart);
}

function output_chart_show(chart){
	if(chart=="daily_chart1"){
		if(daily_chart1_types.output){
			daily_chart1_types.output=false;
			daily_chart1_option.toolbox.feature.my_output_chart.icon='image://'+path+'/res/images/toolbarbtn/output_off_btn.png';
		}else{
			daily_chart1_types.output=true;
			daily_chart1_option.toolbox.feature.my_output_chart.icon='image://'+path+'/res/images/toolbarbtn/output_btn.png';
		}
		is_daily_chart1=true;
		update_chart_series_items2();
		daily_chart1.setOption(daily_chart1_option, true);
		return;
	}
	if(chart=="daily_chart2"){
		if(daily_chart2_types.output){
			daily_chart2_types.output=false;
			daily_chart2_option.toolbox.feature.my_output_chart.icon='image://'+path+'/res/images/toolbarbtn/output_off_btn.png';
		}else{
			daily_chart2_types.output=true;
			daily_chart2_option.toolbox.feature.my_output_chart.icon='image://'+path+'/res/images/toolbarbtn/output_btn.png';
		}
		is_daily_chart2=true;
		update_chart_series_items2();
		daily_chart2.setOption(daily_chart2_option, true);
		return;
	}
	if(chart=="hourly_chart1"){
		if(hourly_chart1_types.output){
			hourly_chart1_types.output=false;
			hourly_chart1_option.toolbox.feature.my_output_chart.icon='image://'+path+'/res/images/toolbarbtn/output_off_btn.png';
		}else{
			hourly_chart1_types.output=true;
			hourly_chart1_option.toolbox.feature.my_output_chart.icon='image://'+path+'/res/images/toolbarbtn/output_btn.png';
		}
	}
	if(chart=="hourly_chart2"){
		if(hourly_chart2_types.output){
			hourly_chart2_types.output=false;
			hourly_chart2_option.toolbox.feature.my_output_chart.icon='image://'+path+'/res/images/toolbarbtn/output_off_btn.png';
		}else{
			hourly_chart2_types.output=true;
			hourly_chart2_option.toolbox.feature.my_output_chart.icon='image://'+path+'/res/images/toolbarbtn/output_btn.png';
		}
	}
	charts_is_show(chart);
}

function unit_cons_chart_show(chart){
	if(chart=="daily_chart1"){
		if(daily_chart1_types.unit_cons){
			daily_chart1_types.unit_cons=false;
			daily_chart1_option.toolbox.feature.my_unit_cons_chart.icon='image://'+path+'/res/images/toolbarbtn/unit_cons_off_btn.png';
		}else{
			daily_chart1_types.unit_cons=true;
			daily_chart1_option.toolbox.feature.my_unit_cons_chart.icon='image://'+path+'/res/images/toolbarbtn/unit_cons_btn.png';
		}
		is_daily_chart1=true;
		update_chart_series_items2();
		daily_chart1.setOption(daily_chart1_option, true);
		return;
	}
	if(chart=="daily_chart2"){
		if(daily_chart2_types.unit_cons){
			daily_chart2_types.unit_cons=false;
			daily_chart2_option.toolbox.feature.my_unit_cons_chart.icon='image://'+path+'/res/images/toolbarbtn/unit_cons_off_btn.png';
		}else{
			daily_chart2_types.unit_cons=true;
			daily_chart2_option.toolbox.feature.my_unit_cons_chart.icon='image://'+path+'/res/images/toolbarbtn/unit_cons_btn.png';
		}
		is_daily_chart2=true;
		update_chart_series_items2();
		daily_chart2.setOption(daily_chart2_option, true);
		return;
	}
	if(chart=="hourly_chart1"){
		if(hourly_chart1_types.unit_cons){
			hourly_chart1_types.unit_cons=false;
			hourly_chart1_option.toolbox.feature.my_unit_cons_chart.icon='image://'+path+'/res/images/toolbarbtn/unit_cons_off_btn.png';
		}else{
			hourly_chart1_types.unit_cons=true;
			hourly_chart1_option.toolbox.feature.my_unit_cons_chart.icon='image://'+path+'/res/images/toolbarbtn/unit_cons_btn.png';
		}
	}
	if(chart=="hourly_chart2"){
		if(hourly_chart2_types.unit_cons){
			hourly_chart2_types.unit_cons=false;
			hourly_chart2_option.toolbox.feature.my_unit_cons_chart.icon='image://'+path+'/res/images/toolbarbtn/unit_cons_off_btn.png';
		}else{
			hourly_chart2_types.unit_cons=true;
			hourly_chart2_option.toolbox.feature.my_unit_cons_chart.icon='image://'+path+'/res/images/toolbarbtn/unit_cons_btn.png';
		}
	}
	charts_is_show(chart);
}

function state_time_chart_show(chart){
	if(chart=="daily_chart1"){
		if(daily_chart1_types.state_time){
			daily_chart1_types.state_time=false;
			daily_chart1_option.toolbox.feature.my_state_time_chart.icon='image://'+path+'/res/images/toolbarbtn/state_time_off_btn.png';
		}else{
			daily_chart1_types.state_time=true;
			daily_chart1_option.toolbox.feature.my_state_time_chart.icon='image://'+path+'/res/images/toolbarbtn/state_time_btn.png';
		}
		is_daily_chart1=true;
		update_chart_series_items2();
		daily_chart1.setOption(daily_chart1_option, true);
		return;
	}
	if(chart=="daily_chart2"){
		if(daily_chart2_types.state_time){
			daily_chart2_types.state_time=false;
			daily_chart2_option.toolbox.feature.my_state_time_chart.icon='image://'+path+'/res/images/toolbarbtn/state_time_off_btn.png';
		}else{
			daily_chart2_types.state_time=true;
			daily_chart2_option.toolbox.feature.my_state_time_chart.icon='image://'+path+'/res/images/toolbarbtn/state_time_btn.png';
		}
		is_daily_chart2=true;
		update_chart_series_items2();
		daily_chart2.setOption(daily_chart2_option, true);
		return;
	}
	if(chart=="hourly_chart1"){
		if(hourly_chart1_types.state_time){
			hourly_chart1_types.state_time=false;
			hourly_chart1_option.toolbox.feature.my_state_time_chart.icon='image://'+path+'/res/images/toolbarbtn/state_time_off_btn.png';
		}else{
			hourly_chart1_types.state_time=true;
			hourly_chart1_option.toolbox.feature.my_state_time_chart.icon='image://'+path+'/res/images/toolbarbtn/state_time_btn.png';
		}
	}
	if(chart=="hourly_chart2"){
		if(hourly_chart2_types.state_time){
			hourly_chart2_types.state_time=false;
			hourly_chart2_option.toolbox.feature.my_state_time_chart.icon='image://'+path+'/res/images/toolbarbtn/state_time_off_btn.png';
		}else{
			hourly_chart2_types.state_time=true;
			hourly_chart2_option.toolbox.feature.my_state_time_chart.icon='image://'+path+'/res/images/toolbarbtn/state_time_btn.png';
		}
	}
	charts_is_show(chart);
}

function SaveSnapshot(page, name, icon, when_close) {
	var select_energy = document.getElementById("select_energy");
	for(var i=0; i<select_energy.options.length; i++){ 
		if(select_energy.options[i].selected == true){ 
			select_energy=select_energy.options[i].value;
			break;
		}
	}
	var cfg = {
			select_energy:select_energy,
			currentPowerUnit:currentPowerUnit,
			currentCDAUnit:currentCDAUnit,
			currentUPWUnit:currentUPWUnit,
			currentPN2Unit:currentPN2Unit,
			currentTimeUnit:currentTimeUnit,
//			series_box_items:series_box.option().items,
//			series_box_value:series_box.option().value,
//			factory_box_items:factory_box.option().items,
//			factory_box_value:factory_box.option().value,
			department_box_items:department_box.option().items,
			department_box_value:department_box.option().value,
			line_box_items:line_box.option().items,
			line_box_value:line_box.option().value,
			device_box_items:device_box.option().items,
			device_box_value:device_box.option().value,
			device_unit_box_items:device_unit_box.option().items,
			device_unit_box_value:device_unit_box.option().value,
			
			factory_list: factory_list,
			department_list: department_list,
			product_line_list: product_line_list,
			product_line_array: product_line_array,
			device_list: device_list,
			device_unit_list: device_unit_list,
		};

	var strJson = JSON.stringify(cfg, function(key, val) {
		if (typeof val === 'function') {
			return val + '';
		}
		return val;
	});
	
    $.ajax({
        url: when_close ? (path + "/gynh/biz/save_default_shortcut.do") : (path + "/gynh/biz/save_shortcut.do"),
        type: 'post',
        async: false,
        data: {
        	page:page, 
        	name:name, 
        	icon:icon, 
        	state_json:strJson,
        },
        success: function (data) {
        	if (!when_close) {
        		top.layer.msg('保存成功', {icon: 1, skin:is_dark?'layui-layer-lan':''});
	        	loadSnapshot();
        	}
        },
        error: function () {
        	if (!when_close) {
        		top.layer.msg('保存失败', {icon: 1, skin:is_dark?'layui-layer-lan':''});
        	}
        }
    });
}

function load_default_shortcut() {
	
	$.ajax({
		url: path + "/gynh/biz/load_default_shortcut.do",
		type: 'post',
		async: false,
		data: {
			page_name:pageName, 
		},
		success: function (data) {
			var shortcuts = data['shortcut'];
			if (shortcuts.length == 1) {
				
				apply_shortcut(shortcuts[0].state_json);
				
			}
        },
        error: function () {
        	// top.layer.close(index);
        	// top.layer.msg('数据加载错误', {icon: 1,
			// skin:is_dark?'layui-layer-lan':''});
        }
	});
	
}

function apply_shortcut(json) {
	var cfg = JSON.parse(json, function(k,v) {
		if(v.indexOf&&v.indexOf('function')>-1) {
			return eval("(function(){return "+v+" })()")
		}
		return v;
	});
	
	var select_energy = document.getElementById("select_energy");
	for(var i=0; i<select_energy.options.length; i++){ 
		if(select_energy.options[i].value == cfg.select_energy){ 
			select_energy.options[i].selected = true; 
		}
	}
	cur_energy=cfg.select_energy;
	
	currentPowerUnit = cfg.currentPowerUnit;
	currentCDAUnit = cfg.currentCDAUnit;
	currentUPWUnit = cfg.currentUPWUnit;
	currentPN2Unit = cfg.currentPN2Unit;
	currentTimeUnit = cfg.currentTimeUnit;

	if(currentPowerUnit==null||currentPowerUnit=='')
	{
		currentPowerUnit="kWh";
	}
	if(currentCDAUnit==null||currentCDAUnit=='')
	{
		currentCDAUnit="L";
	}
	if(currentUPWUnit==null||currentUPWUnit=='')
	{
		currentUPWUnit="L";
	}
	if(currentPN2Unit==null||currentPN2Unit=='')
	{
		currentPN2Unit="L";
	}
	if(currentTimeUnit==null||currentTimeUnit=='')
	{
		currentTimeUnit="M";
	}
	set_units_on_controles();
//	factory_box.option("items", cfg.factory_box_items);
//	factory_box.option("value", cfg.factory_box_value);
	department_box.option("items", cfg.department_box_items);
	department_box.option("value", cfg.department_box_value);
	line_box.option("items", cfg.line_box_items);
	line_box.option("value", cfg.line_box_value);
	device_box.option("items", cfg.device_box_items);
	device_box.option("value", cfg.device_box_value);
	device_unit_box.option("items", cfg.device_unit_box_items);
	device_unit_box.option("value", cfg.device_unit_box_value);
	
	factory_list = cfg.factory_list;
	department_list = cfg.department_list;
	product_line_list = cfg.product_line_list;
	product_line_array = cfg.product_line_array;
	device_list = cfg.device_list;
	device_unit_list = cfg.device_unit_list;

}

function resize_screen() {
	var windowHeight = $(window).height();
	var windowWidth = $(window).width();
	var toobar_height = $("#toobar_div").height();
	var state_bar_height = $("#state_bar").height();
	if (hourly) {
		var h_gap_div = product_line_array.length == 0 ? 30 : 40;
		var h_charts = windowHeight - toobar_height - state_bar_height - h_gap_div;
		var h_hourly_state_time_chart = product_line_array.length == 0 ? 0 : Math.min(h_charts/3, product_line_array.length * 22 + 8 + 20);

		document.getElementById('daily_chart1').style.height = '0px';
		document.getElementById('daily_chart2').style.height = '0px';
		
		document.getElementById('hourly_state_time_chart').style.height = h_hourly_state_time_chart + 'px';
		document.getElementById('hourly_chart1').style.height = (h_charts- h_hourly_state_time_chart)/2 + 'px';
		document.getElementById('hourly_chart2').style.height = (h_charts- h_hourly_state_time_chart)/2 + 'px';
		
		document.getElementById('gap_div3').style.height = product_line_array.length == 0 ? '0px' : '10px';

	} else {
		var h_gap_div = 30;

		document.getElementById('daily_chart1').style.height = (windowHeight - toobar_height - state_bar_height - h_gap_div)/2 + 'px';
		document.getElementById('daily_chart2').style.height = (windowHeight - toobar_height - state_bar_height - h_gap_div)/2 + 'px';

		document.getElementById('hourly_state_time_chart').style.height = '0px';
		document.getElementById('hourly_chart1').style.height = '0px';
		document.getElementById('hourly_chart2').style.height = '0px';
		
		document.getElementById('gap_div3').style.height = '0px';
	}
	
	document.getElementById('daily_chart1').style.width = windowWidth + 'px';
	document.getElementById('daily_chart2').style.width = windowWidth + 'px';
	document.getElementById('hourly_state_time_chart').style.width = windowWidth + 'px';
	document.getElementById('hourly_chart1').style.width = windowWidth + 'px';
	document.getElementById('hourly_chart2').style.width = windowWidth + 'px';
	
	daily_chart1.resize(); 
	daily_chart2.resize(); 
	hourly_chart1.resize(); 
	hourly_chart2.resize(); 
	hourly_state_time_chart.resize(); 
}

function sel_items_to_list(sel_items) {
	if (sel_items.length == 0) {
		return "";
	}
	var list = "(";
	for (var i = 0; i < sel_items.length; i++) {
		list = list + "'" + sel_items[i] + "',"
	}
	list = list.substr(0,list.length-1) + ")";
	return list;
}

function query_list_to_array(query_list) {
	var arr = new Array();
	for (var i = 0; i < query_list.length; i++) {
		arr.push(query_list[i].str);
	}
	return arr;
}



//function get_factory_list() {
//    $.ajax({
//        url: path + "/gynh/factory_chart_view/get_factory_list.do",
//        type: 'post',
//        async: false,
//        success: function (data) {
////        	factory_box.option("items", query_list_to_array(data['data']));
////        	factory_box.option("value", []);
//        	department_box.option("items", []);
//			department_box.option("value", []);
//			line_box.option("items", []);
//			line_box.option("value", []);
//			device_box.option("items", []);
//			device_box.option("value", []);
//			device_unit_box.option("items", []);
//			device_unit_box.option("value", []);
//        },
//        error: function () {
//        	console.log('error');
//        }
//    });
//
//}

function get_department_list() {

	var items_array = [];
	if (factory_list != "") {
	    $.ajax({
	        url: path + "/gynh/factory_chart_view/get_department_list.do",
	        type: 'post',
	        async: false,
	        data: {
				factory_list: factory_list, 
	        },
	        success: function (data) {
	        	items_array = query_list_to_array(data['data']);
	        },
	        error: function () {
	        	console.log('error');
	        }
	    });
	}
	
	department_box.option("items", items_array);
	department_box.option("value", []);
	line_box.option("items", []);
	line_box.option("value", []);
	device_box.option("items", []);
	device_box.option("value", []);
	device_unit_box.option("items", []);
	device_unit_box.option("value", []);

}

function get_product_line_list() {
	var items_array = [];
	if (department_list != "") {
		$.ajax({
			url: path + "/gynh/factory_chart_view/get_product_line_list.do",
			type: 'post',
			async: false,
			data: {
				factory_list: factory_list, 
				department_list: department_list, 
			},
			success: function (data) {
				items_array = query_list_to_array(data['data']);
			},
			error: function () {
	        	console.log('error');
			}
		});
	}
	
	line_box.option("items", items_array);
	line_box.option("value", []);
	device_box.option("items", []);
	device_box.option("value", []);
	device_unit_box.option("items", []);
	device_unit_box.option("value", []);
}

function get_device_list() {
	var items_array = [];
	if (product_line_list != "") {
		$.ajax({
			url: path + "/gynh/factory_chart_view/get_device_list.do",
			type: 'post',
			async: false,
			data: {
				factory_list: factory_list, 
				department_list: department_list, 
				product_line_list: product_line_list, 
			},
			success: function (data) {
				items_array = query_list_to_array(data['data']);
			},
			error: function () {
	        	console.log('error');
			}
		});
	}
	device_box.option("items", items_array);
	device_box.option("value", []);
	device_unit_box.option("items", []);
	device_unit_box.option("value", []);
}

function get_device_unit_list() {
	var items_array = [];
	if (device_list != "") {
		$.ajax({
			url: path + "/gynh/factory_chart_view/get_device_unit_list.do",
			type: 'post',
			async: false,
			data: {
				factory_list: factory_list, 
				department_list: department_list, 
				product_line_list: product_line_list, 
				device_list: device_list, 
			},
			success: function (data) {
				items_array = query_list_to_array(data['data']);
			},
			error: function () {
	        	console.log('error');
			}
		});
	}
	device_unit_box.option("items", items_array);
	device_unit_box.option("value", []);
}

// function get_point_list() {
// var items_array = [];
// if (device_unit_list != "") {
// $.ajax({
// url: path + "/gynh/factory_chart_view/get_point_list.do",
// type: 'post',
// async: false,
// data: {
// factory_list: factory_list,
// department_list: department_list,
// product_line_list: product_line_list,
// device_list: device_list,
// device_unit_list: device_unit_list,
// },
// success: function (data) {
// items_array = query_list_to_array(data['data']);
// },
// error: function () {
// console.log('error');
// }
// });
// }
// point_box.option("items", items_array);
// }

function load_daily_data() {
	var msg_box = top.layer.msg('查询中...',{icon: 16, skin:is_dark?'layui-layer-lan':'', shade:0.1}, 1000000);

	update_daily_xAxis();
	daily_chart1_option.series.splice(0, daily_chart1_option.series.length);
	energy_serie_array = {Power:[],UPW:[],CDA:[],PN2:[]};
	unit_cons_serie_array = {Power:[],UPW:[],CDA:[],PN2:[]};
	output_serie_array = [];
	state_time_serie_array = [];
	daily_node_set = new Set();
	
	if (device_unit_list != "") {
		get_device_unit_cons();
	} else if (device_list != "") {
		get_device_cons();
	} else if (product_line_list != "") {
		get_product_line_cons();
		get_product_line_output();
		get_product_line_unit_cons();
		get_product_line_state_time();
	} else if (department_list != "") {
		get_department_cons();
		get_department_output();
		get_department_unit_cons();
		get_department_state_time();
	} else if (factory_list != "") {
		get_factory_cons();
	}

	translate_unit("Time","S",currentTimeUnit);
	translate_unit("Power","kWh",currentPowerUnit);
	translate_unit("CDA","L",currentCDAUnit);
	translate_unit("UPW","L",currentUPWUnit);
	translate_unit("PN2","L",currentPN2Unit);
	is_daily_chart1=true;
    is_daily_chart2=true;
    is_hourly_chart1=true;
    is_hourly_chart2=true;
	update_chart_series_items2();

	echarts_unit();
	daily_chart1_option.legend.data = legend_data;

	daily_chart1.setOption(daily_chart1_option, true);

	daily_chart2_option.legend.data = legend_data;
	daily_chart2.setOption(daily_chart2_option, true);

	resize_screen();
	top.layer.close(msg_box);
	top.layer.msg('数据加载完成', {icon: 1, skin:is_dark?'layui-layer-lan':''});
}

function echarts_unit(){
	if (cur_energy=="Power") {
		chart_unit = currentPowerUnit ;
	} 
	if (cur_energy=="CDA") {
		chart_unit = currentCDAUnit ;
	} 
	if (cur_energy=="UPW") {
		chart_unit = currentUPWUnit ;
	} 
	if (cur_energy=="PN2") {
		chart_unit = currentPN2Unit ;
	}
	
	daily_chart1_option.yAxis[0].name="能耗("+ chart_unit +")"
	daily_chart1_option.yAxis[2].name="单耗("+ chart_unit +")"
	daily_chart1_option.yAxis[3].name="状态时间("+currentTimeUnit+")"
	daily_chart2_option.yAxis[0].name="能耗("+ chart_unit +")"
	daily_chart2_option.yAxis[2].name="单耗("+ chart_unit +")"
	daily_chart2_option.yAxis[3].name="状态时间("+currentTimeUnit+")"
	hourly_chart1_option.yAxis[0].name="能耗("+ chart_unit +")"
	hourly_chart1_option.yAxis[2].name="单耗("+ chart_unit +")"
	hourly_chart1_option.yAxis[3].name="状态时间("+currentTimeUnit+")"
	hourly_chart2_option.yAxis[0].name="能耗("+ chart_unit +")"
	hourly_chart2_option.yAxis[2].name="单耗("+ chart_unit +")"
	hourly_chart2_option.yAxis[3].name="状态时间("+currentTimeUnit+")"
}

function load_hourly_data() {
	var msg_box = top.layer.msg('查询中...',{icon: 16, skin:is_dark?'layui-layer-lan':'', shade:0.1}, 1000000);

	//update_hourly_xAxis();
	
	hourly_chart1_option.series.splice(0, hourly_chart1_option.series.length);
	hourly_chart2_option.series.splice(0, hourly_chart2_option.series.length);
	hourly_energy_serie_array = {Power:[],UPW:[],CDA:[],PN2:[]};
	hourly_unit_cons_serie_array = {Power:[],UPW:[],CDA:[],PN2:[]};
	hourly_output_serie_array = [];
	hourly_state_time_serie_array = [];
	hourly_node_set = new Set();

	if (device_unit_list != "") {
		get_device_unit_cons_hourly();
	} else if (device_list != "") {
		get_device_cons_hourly();
	} else if (product_line_list != "") {
		get_product_line_cons_hourly();
		get_product_line_output_hourly();
		get_product_line_unit_cons_hourly();
		load_hourly_state_time();
	} else if (department_list != "") {
		get_department_cons_hourly();
		get_department_output_hourly();
		get_department_unit_cons_hourly();
	} else if (factory_list != "") {
		get_factory_cons_hourly();
	}
	
	update_chart_series_items_hourly();
	echarts_unit();
	
	hourly_chart1_option.legend.data = hourly_legend_data;
	hourly_chart1.setOption(hourly_chart1_option, true);

	hourly_chart2_option.legend.data = hourly_legend_data;
	hourly_chart2.setOption(hourly_chart2_option, true);

	resize_screen();
	
	top.layer.close(msg_box);
	top.layer.msg('数据加载完成', {icon: 1, skin:is_dark?'layui-layer-lan':''});

}

function get_factory_cons() {
	$.ajax({
		url: path + "/gynh/factory_chart_view/get_factory_cons.do",
		type: 'post',
		async: false,
		data: {
			start_date: start_date,
			end_date: end_date,
			factory_list: factory_list, 
		},
		success: function (data) {
			load_cons_to_chart(data['data']);
     	},
		error: function () {
        	console.log('error');
		}
	});
}


function get_department_cons() {
	$.ajax({
		url: path + "/gynh/factory_chart_view/get_department_cons.do",
		type: 'post',
		async: false,
		data: {
			start_date: start_date,
			end_date: end_date,
			factory_list: factory_list, 
			department_list: department_list, 
		},
		success: function (data) {
			load_cons_to_chart(data['data']);
		},
		error: function () {
        	console.log('error');
		}
	});
}

function get_department_cons_hourly() {
	$.ajax({
		url: path + "/gynh/factory_chart_view/get_department_cons_hourly.do",
		type: 'post',
		async: false,
		data: {
			date: hourly_date,
			factory_list: factory_list, 
			department_list: department_list, 
		},
		success: function (data) {
			load_cons_to_hourly_chart(data['data']);
		},
		error: function () {
        	console.log('error');
		}
	});
}

function get_department_output() {
	// 结束时间的后一天的0点
	var datetime = new Date(end_date + " 00:00:00");
	datetime = new Date(datetime.getTime()+3600000*24);
	var formated_date_time = get_formated_date_time(datetime);

	$.ajax({
		url: path + "/gynh/factory_chart_view/get_department_output.do",
		type: 'post',
		async: false,
		data: {
			start_time: start_date + " 00:00:00",
			end_time: formated_date_time,
			factory_list: factory_list, 
			department_list: department_list, 
		},
		success: function (data) {
			load_output_to_chart(data['data']);
		},
		error: function () {
        	console.log('error');
		}
	});
}

function get_department_state_time() {
	// 结束时间的后一天的0点
	var datetime = new Date(end_date + " 00:00:00");
	datetime = new Date(datetime.getTime()+3600000*24);
	var formated_date_time = get_formated_date_time(datetime);

	$.ajax({
		url: path + "/gynh/factory_chart_view/get_department_state_time.do",
		type: 'post',
		async: false,
		data: {
			start_time: start_date + " 00:00:00",
			end_time: formated_date_time,
			factory_list: factory_list, 
			department_list: department_list, 
		},
		success: function (data) {
			load_state_time_to_chart(data['data']);
		},
		error: function () {
        	console.log('error');
		}
	});
}

function get_department_output_hourly() {
	// 结束时间的后一天的0点
	var datetime = new Date(hourly_date + " 00:00:00");
	datetime = new Date(datetime.getTime()+3600000*24);
	var formated_date_time = get_formated_date_time(datetime);

	$.ajax({
		url: path + "/gynh/factory_chart_view/get_department_output_hourly.do",
		type: 'post',
		async: false,
		data: {
			start_time: hourly_date + " 00:00:00",
			end_time: formated_date_time,
			factory_list: factory_list, 
			department_list: department_list, 
		},
		success: function (data) {
			load_output_to_chart_hourly(data['data']);
		},
		error: function () {
        	console.log('error');
		}
	});
}

function get_product_line_cons() {
	$.ajax({
		url: path + "/gynh/factory_chart_view/get_product_line_cons.do",
		type: 'post',
		async: false,
		data: {
			start_date: start_date,
			end_date: end_date,
			factory_list: factory_list, 
			department_list: department_list, 
			product_line_list: product_line_list, 
		},
		success: function (data) {
			load_cons_to_chart(data['data']);
		},
		error: function () {
        	console.log('error');
		}
	});
}

function get_product_line_cons_hourly() {
	$.ajax({
		url: path + "/gynh/factory_chart_view/get_product_line_cons_hourly.do",
		type: 'post',
		async: false,
		data: {
			date: hourly_date,
			factory_list: factory_list, 
			department_list: department_list, 
			product_line_list: product_line_list, 
		},
		success: function (data) {
			load_cons_to_hourly_chart(data['data']);
		},
		error: function () {
        	console.log('error');
		}
	});
}

function get_device_cons() {
	$.ajax({
		url: path + "/gynh/factory_chart_view/get_device_cons.do",
		type: 'post',
		async: false,
		data: {
			start_date: start_date,
			end_date: end_date,
			factory_list: factory_list, 
			department_list: department_list, 
			product_line_list: product_line_list, 
			device_list: device_list, 
		},
		success: function (data) {
			load_cons_to_chart(data['data']);
		},
		error: function () {
        	console.log('error');
		}
	});
}

function get_device_unit_cons() {
	$.ajax({
		url: path + "/gynh/factory_chart_view/get_device_unit_cons.do",
		type: 'post',
		async: false,
		data: {
			start_date: start_date,
			end_date: end_date,
			factory_list: factory_list, 
			department_list: department_list, 
			product_line_list: product_line_list, 
			device_list: device_list, 
			device_unit_list: device_unit_list, 
		},
		success: function (data) {
			load_cons_to_chart(data['data']);
		},
		error: function () {
        	console.log('error');
		}
	});
}

function get_product_line_output() {
	// 结束时间的后一天的0点
	var datetime = new Date(end_date + " 00:00:00");
	datetime = new Date(datetime.getTime()+3600000*24);
	var formated_date_time = get_formated_date_time(datetime);
	$.ajax({
		url: path + "/gynh/factory_chart_view/get_product_line_output.do",
		type: 'post',
		async: false,
		data: {
			start_time: start_date + " 00:00:00",
			end_time: formated_date_time,
			product_line_list: product_line_list, 
		},
		success: function (data) {
			load_output_to_chart(data['data']);
		},
		error: function () {
        	console.log('error');
		}
	});
}

function get_product_line_output_hourly() {
	// 结束时间的后一天的0点
	var datetime = new Date(hourly_date + " 00:00:00");
	datetime = new Date(datetime.getTime()+3600000*24);
	var formated_date_time = get_formated_date_time(datetime);
	$.ajax({
		url: path + "/gynh/factory_chart_view/get_product_line_output_hourly.do",
		type: 'post',
		async: false,
		data: {
			start_time: hourly_date + " 00:00:00",
			end_time: formated_date_time,
			product_line_list: product_line_list, 
		},
		success: function (data) {
			load_output_to_chart_hourly(data['data']);
		},
		error: function () {
        	console.log('error');
		}
	});
}

function get_department_unit_cons() {
	// 结束时间的后一天的0点
	var datetime = new Date(end_date + " 00:00:00");
	datetime = new Date(datetime.getTime()+3600000*24);
	var formated_date_time = get_formated_date_time(datetime);
	$.ajax({
		url: path + "/gynh/factory_chart_view/get_department_unit_cons.do",
		type: 'post',
		async: false,
		data: {
			cons_start_date: start_date,
			cons_end_date: end_date,
			output_start_time: start_date + " 00:00:00",
			output_end_time: formated_date_time,
			factory_list: factory_list, 
			department_list: department_list, 
		},
		success: function (data) {
			load_unit_cons_to_chart(data['data']);
		},
		error: function () {
        	console.log('error');
		}
	});
}

function get_department_unit_cons_hourly() {
	// 结束时间的后一天的0点
	var datetime = new Date(hourly_date + " 00:00:00");
	datetime = new Date(datetime.getTime()+3600000*24);
	var formated_date_time = get_formated_date_time(datetime);
	$.ajax({
		url: path + "/gynh/factory_chart_view/get_department_unit_cons_hourly.do",
		type: 'post',
		async: false,
		data: {
			cons_date: hourly_date,
			output_start_time: hourly_date + " 00:00:00",
			output_end_time: formated_date_time,
			factory_list: factory_list, 
			department_list: department_list, 
		},
		success: function (data) {
			load_unit_cons_to_chart_hourly(data['data']);
		},
		error: function () {
        	console.log('error');
		}
	});
}

function get_product_line_unit_cons() {
	// 结束时间的后一天的0点
	var datetime = new Date(end_date + " 00:00:00");
	datetime = new Date(datetime.getTime()+3600000*24);
	var formated_date_time = get_formated_date_time(datetime);
	$.ajax({
		url: path + "/gynh/factory_chart_view/get_product_line_unit_cons.do",
		type: 'post',
		async: false,
		data: {
			cons_start_date: start_date,
			cons_end_date: end_date,
			output_start_time: start_date + " 00:00:00",
			output_end_time: formated_date_time,
			factory_list: factory_list, 
			department_list: department_list, 
			product_line_list: product_line_list, 
		},
		success: function (data) {
			load_unit_cons_to_chart(data['data']);
		},
		error: function () {
        	console.log('error');
		}
	});
}

function get_product_line_unit_cons_hourly() {
	// 结束时间的后一天的0点
	var datetime = new Date(hourly_date + " 00:00:00");
	datetime = new Date(datetime.getTime()+3600000*24);
	var formated_date_time = get_formated_date_time(datetime);
	$.ajax({
		url: path + "/gynh/factory_chart_view/get_product_line_unit_cons_hourly.do",
		type: 'post',
		async: false,
		data: {
			cons_date: hourly_date,
			output_start_time: hourly_date + " 00:00:00",
			output_end_time: formated_date_time,
			factory_list: factory_list, 
			department_list: department_list, 
			product_line_list: product_line_list, 
		},
		success: function (data) {
			load_unit_cons_to_chart_hourly(data['data']);
		},
		error: function () {
        	console.log('error');
		}
	});
}

function get_product_line_state_time() {
	// 结束时间的后一天的0点
	var datetime = new Date(end_date + " 00:00:00");
	datetime = new Date(datetime.getTime()+3600000*24);
	var formated_date_time = get_formated_date_time(datetime);

	$.ajax({
		url: path + "/gynh/factory_chart_view/get_product_line_state_time.do",
		type: 'post',
		async: false,
		data: {
			start_time: start_date + " 00:00:00",
			end_time: formated_date_time,
			product_line_list: product_line_list, 
		},
		success: function (data) {
			load_state_time_to_chart(data['data']);
		},
		error: function () {
        	console.log('error');
		}
	});
}

//function get_product_line_long_name() {
//	$.ajax({
//		url: path + "/gynh/factory_chart_view/get_product_line_long_name.do",
//		type: 'post',
//		async: false,
//		data: {
//			product_line_list: product_line_list, 
//		},
//		success: function (data) {
//			eqp_long_name = {};
//			var r = data['data'];
//			for (var i = 0; i < r.length; i++) {
//				eqp_long_name[r[i].snm] = r[i].lnm;
//			}
//		},
//		error: function () {
//        	console.log('error');
//		}
//	});
//}

function select_erengy_changed() {
	var select_energy = document.getElementById("select_energy");
	cur_energy = select_energy.options[select_energy.selectedIndex].value;
	echarts_unit();
	update_chart_series_items();
	update_chart_series_items_hourly();
	daily_chart1.setOption(daily_chart1_option, true);
	daily_chart2.setOption(daily_chart2_option, true);
	hourly_chart1.setOption(hourly_chart1_option, true);
	hourly_chart2.setOption(hourly_chart2_option, true);
}

function update_daily_xAxis() {
	var temp_start_date = start_date;
	var temp_end_date = end_date;
	
	var begin_year = temp_start_date.substr(0,4)*1;
	var begin_month = temp_start_date.substr(5,2)*1;
	var begin_day = temp_start_date.substr(8,2)*1;

	var d1 = new Date(temp_start_date)
	var d2 = new Date(temp_end_date)
	period = (d2.getTime()-d1.getTime()) / (1000*60*60*24) + 1;

	var xAxis = new Array();

	for (var i = 0; i < period; i++) {
	    var temp = new Date(begin_year, begin_month-1, begin_day);
	    xAxis[i] = getFormatDate2(temp, i);
	    xAxis[i] = xAxis[i].substring(8);
	}
	
	daily_chart1_option.xAxis[0].data = xAxis;
	daily_chart2_option.xAxis[0].data = xAxis;
}

function update_hourly_xAxis() {
	hourly_period = 24;

	var xAxis = new Array();

	for (var i = 0; i < hourly_period; i++) {
	    xAxis[i] = i;
	}
	
	hourly_chart1_option.xAxis[0].data = xAxis;
}

function load_cons_to_chart(query_node_cons) {
	daily_node_cons_series = {Power:{},UPW:{},CDA:{},PN2:{}};;
	var node_cons = {};
	for (var i = 0; i < query_node_cons.length; i++) {
		daily_node_set.add(query_node_cons[i].lnm);
		
		if (typeof(node_cons[query_node_cons[i].e]) == "undefined") {
			node_cons[query_node_cons[i].e] = {};
		}
		if (typeof(node_cons[query_node_cons[i].e][query_node_cons[i].lnm]) == "undefined") {
			node_cons[query_node_cons[i].e][query_node_cons[i].lnm] = new Array(period).fill('-');
		}
		
		var idx = (new Date(query_node_cons[i].t + " 00:00:00") - new Date(start_date + " 00:00:00"))/(24*60*60*1000);
		node_cons[query_node_cons[i].e][query_node_cons[i].lnm][idx] = query_node_cons[i].c;
	}

	for (var i = 0; i < energy_names.length; i++) {
		if (typeof(node_cons[energy_names[i]]) != "undefined") {
			var j = 0;
			for (let node of daily_node_set.keys()) {
				var series = {
			            name: node,
			            type: 'bar',
			            yAxisIndex: 0,
			            data: [],
			            animation: true,
			            color: my_colors[j%my_colors.length],
			            means: "能耗",
			            itemStyle: {
			            	borderColor: '#000',
			            	borderWidth: 1,
			            },
			        };
				if (typeof(node_cons[energy_names[i]][node]) != "undefined") {
					series.data = node_cons[energy_names[i]][node];
				}
				energy_serie_array[energy_names[i]].push(series);
				daily_node_cons_series[energy_names[i]][node] = series;
				j++;
			}
		}
	}

	for (let node of daily_node_set.keys()) {
		legend_data.push(node);
	}
}

function load_cons_to_hourly_chart(query_node_cons) {
	var node_cons = {};
	for (var i = 0; i < query_node_cons.length; i++) {
		hourly_node_set.add(query_node_cons[i].lnm);
		
		if (typeof(node_cons[query_node_cons[i].e]) == "undefined") {
			node_cons[query_node_cons[i].e] = {};
		}
		if (typeof(node_cons[query_node_cons[i].e][query_node_cons[i].lnm]) == "undefined") {
			node_cons[query_node_cons[i].e][query_node_cons[i].lnm] = new Array(hourly_period).fill('-');
		}
		
		node_cons[query_node_cons[i].e][query_node_cons[i].lnm][query_node_cons[i].t] = query_node_cons[i].c;
	}

	for (var i = 0; i < energy_names.length; i++) {
		if (typeof(node_cons[energy_names[i]]) != "undefined") {
			var j = 0;
			for (let node of hourly_node_set.keys()) {
				if (typeof(node_cons[energy_names[i]][node]) != "undefined") {
					var series = {
			            name: node,
			            type: 'bar',
			            yAxisIndex: 0,
			            data: node_cons[energy_names[i]][node],
			            animation: true,
			            color: my_colors[j%my_colors.length],
			            means: "能耗",
			        };
					hourly_energy_serie_array[energy_names[i]].push(series);
				}
				j++;
			}
		}
	}

	for (let node of hourly_node_set.keys()) {
		hourly_legend_data.push(node);
	}
}

function load_unit_cons_to_chart(query_node_cons) {
	daily_node_unit_cons_series = {Power:{},UPW:{},CDA:{},PN2:{}};
	var node_cons = {};
	for (var i = 0; i < query_node_cons.length; i++) {
		daily_node_set.add(query_node_cons[i].lnm);
		
		if (typeof(node_cons[query_node_cons[i].e]) == "undefined") {
			node_cons[query_node_cons[i].e] = {};
		}
		if (typeof(node_cons[query_node_cons[i].e][query_node_cons[i].lnm]) == "undefined") {
			node_cons[query_node_cons[i].e][query_node_cons[i].lnm] = new Array(period).fill('-');
		}
		
		var idx = (new Date(query_node_cons[i].t + " 00:00:00") - new Date(start_date + " 00:00:00"))/(24*60*60*1000);
		node_cons[query_node_cons[i].e][query_node_cons[i].lnm][idx] = query_node_cons[i].c;
	}

	for (var i = 0; i < energy_names.length; i++) {
		if (typeof(node_cons[energy_names[i]]) != "undefined") {
			for (let node of daily_node_set.keys()) {
				var series = {
		            name: node,
		            type: 'line',
		            yAxisIndex: 2,
		            data: [],
		            animation: true,
		            means: "单耗",
		        };
				if (typeof(node_cons[energy_names[i]][node]) != "undefined") {
					series.data = node_cons[energy_names[i]][node];
				}
				unit_cons_serie_array[energy_names[i]].push(series);
				daily_node_unit_cons_series[energy_names[i]][node] = series;
			}
		}
	}
}

function load_unit_cons_to_chart_hourly(query_node_cons) {
	var node_cons = {};
	for (var i = 0; i < query_node_cons.length; i++) {
		hourly_node_set.add(query_node_cons[i].lnm);
		
		if (typeof(node_cons[query_node_cons[i].e]) == "undefined") {
			node_cons[query_node_cons[i].e] = {};
		}
		if (typeof(node_cons[query_node_cons[i].e][query_node_cons[i].lnm]) == "undefined") {
			node_cons[query_node_cons[i].e][query_node_cons[i].lnm] = new Array(hourly_period).fill('-');
		}
		
		node_cons[query_node_cons[i].e][query_node_cons[i].lnm][query_node_cons[i].t] = query_node_cons[i].c;
	}

	for (var i = 0; i < energy_names.length; i++) {
		if (typeof(node_cons[energy_names[i]]) != "undefined") {
			for (let node of hourly_node_set.keys()) {
				if (typeof(node_cons[energy_names[i]][node]) != "undefined") {
					var series = {
			            name: node,
			            type: 'line',
			            yAxisIndex: 2,
			            data: node_cons[energy_names[i]][node],
			            animation: true,
			            means: "单耗",
			        };
					hourly_unit_cons_serie_array[energy_names[i]].push(series);
				}
			}
		}
	}
}

function load_output_to_chart(query_node_output) {
	daily_node_output_series = {};
	var node_output = {};
	for (var i = 0; i < query_node_output.length; i++) {
		daily_node_set.add(query_node_output[i].node);
		
		if (typeof(node_output[query_node_output[i].node]) == "undefined") {
			node_output[query_node_output[i].node] = new Array(period).fill('-');
		}
		
		var idx = (new Date(query_node_output[i].t + " 00:00:00") - new Date(start_date + " 00:00:00"))/(24*60*60*1000);
		node_output[query_node_output[i].node][idx] = query_node_output[i].qty;
	}

	var j = 0;
	for (let node of daily_node_set.keys()) {
		var series =  {
            name: node,
            type: 'bar',
            yAxisIndex: 1,
            data: [],
            animation: true,
            color: my_colors[j%my_colors.length],
            itemStyle:{
            	//barBorderRadius: [150,150,0,0],
            	opacity: 0.6,
            },
            means: "产量",
        };
		if (typeof(node_output[node]) != "undefined") {
			series.data = node_output[node];
		}
		output_serie_array.push(series);
		daily_node_output_series[node] = series;
		j++;
	}
}

function load_output_to_chart_hourly(query_node_output) {
	var node_output = {};
	for (var i = 0; i < query_node_output.length; i++) {
		hourly_node_set.add(query_node_output[i].node);
		
		if (typeof(node_output[query_node_output[i].node]) == "undefined") {
			node_output[query_node_output[i].node] = new Array(hourly_period).fill('-');
		}
		
		node_output[query_node_output[i].node][query_node_output[i].t] = query_node_output[i].qty;
	}

	var j = 0;
	for (let node of hourly_node_set.keys()) {
		if (typeof(node_output[node]) != "undefined") {
			var series =  {
	            name: node,
	            type: 'bar',
	            yAxisIndex: 1,
	            data: node_output[node],
	            animation: true,
	            color: my_colors[j%my_colors.length],
	            itemStyle:{
	            	//barBorderRadius: [150,150,0,0],
	            	opacity: 0.6,
	            },
	            means: "产量",
	        };
			hourly_output_serie_array.push(series);
		}
		j++;
	}
}

function load_state_time_to_chart(query_node_state_time) {
	daily_node_state_time_series = {};
	var node_state_time = {};
	var node_set = new Set();
	for (var i = 0; i < query_node_state_time.length; i++) {
		node_set.add(query_node_state_time[i].node);

		if (typeof(node_state_time[query_node_state_time[i].node]) == "undefined") {
			node_state_time[query_node_state_time[i].node] = new Array(period).fill('-');
		}
		
		var idx = (new Date(query_node_state_time[i].t + " 00:00:00") - new Date(start_date + " 00:00:00"))/(24*60*60*1000);
		node_state_time[query_node_state_time[i].node][idx] = query_node_state_time[i].qty;
	}

	for (let node of node_set.keys()) {
		var temp = node.split('-');
		var series = {
            name: temp[0],
            type: 'bar',
            stack: temp[0],
            yAxisIndex: 3,
            data: node_state_time[node],
            animation: true,
            color: state_colors[temp[1]],
            itemStyle:{
//            	borderWidth: 1,
//            	borderColor: 'yellow',
            },
            means: "状态时间",
        };
		if (typeof(node_state_time[node]) != "undefined") {
			series.data = node_state_time[node];
		}
		state_time_serie_array.push(series);
		if (typeof(daily_node_state_time_series[temp[0]]) == "undefined") {
			daily_node_state_time_series[temp[0]] = new Array();
		}
		daily_node_state_time_series[temp[0]].push(series);
	}
}

function charts_is_show(chart) {
	if (chart=="daily_chart1") {
		daily_chart1_option.series.splice(0, daily_chart1_option.series.length);
		if (daily_chart1_types.energy) {
			for (var j = 0; j < energy_serie_array[cur_energy].length; j++) {
				daily_chart1_option.series.push(energy_serie_array[cur_energy][j]);
			}
		}
		if (daily_chart1_types.output) {
			for (var j = 0; j < output_serie_array.length; j++) {
				daily_chart1_option.series.push(output_serie_array[j]);
			}
		}
		if (daily_chart1_types.unit_cons) {
			for (var j = 0; j < unit_cons_serie_array[cur_energy].length; j++) {
				daily_chart1_option.series.push(unit_cons_serie_array[cur_energy][j]);
			}
		}
		if (daily_chart1_types.state_time) {
			for (var j = 0; j < state_time_serie_array.length; j++) {
				daily_chart2_option.series.push(state_time_serie_array[j]);
			}
		}
		daily_chart1.setOption(daily_chart1_option, true);
		
	}
	if (chart=="daily_chart2") {
		daily_chart2_option.series.splice(0, daily_chart2_option.series.length);
		if (daily_chart2_types.energy) {
    		for (var j = 0; j < energy_serie_array[cur_energy].length; j++) {
    			daily_chart2_option.series.push(energy_serie_array[cur_energy][j]);
    		}
    	}
    	if (daily_chart2_types.output) {
    		for (var j = 0; j < output_serie_array.length; j++) {
    			daily_chart2_option.series.push(output_serie_array[j]);
    		}
    	}
    	if (daily_chart2_types.unit_cons) {
    		for (var j = 0; j < unit_cons_serie_array[cur_energy].length; j++) {
    			daily_chart2_option.series.push(unit_cons_serie_array[cur_energy][j]);
    		}
    	}
    	if (daily_chart2_types.state_time) {
    		for (var j = 0; j < state_time_serie_array.length; j++) {
    			daily_chart2_option.series.push(state_time_serie_array[j]);
    		}
    	}
		
		daily_chart2.setOption(daily_chart2_option, true);
		
	}
	if (chart=="hourly_chart1") {
		hourly_chart1_option.series.splice(0, hourly_chart1_option.series.length);
		if (hourly_chart1_types.energy) {
			for (var j = 0; j < hourly_energy_serie_array[cur_energy].length; j++) {
				hourly_chart1_option.series.push(hourly_energy_serie_array[cur_energy][j]);
			}
		}
		if (hourly_chart1_types.output) {
			for (var j = 0; j < hourly_output_serie_array.length; j++) {
				hourly_chart1_option.series.push(hourly_output_serie_array[j]);
			}
		}
		if (hourly_chart1_types.unit_cons) {
			for (var j = 0; j < hourly_unit_cons_serie_array[cur_energy].length; j++) {
				hourly_chart1_option.series.push(hourly_unit_cons_serie_array[cur_energy][j]);
			}
		}
		if (hourly_chart1_types.state_time) {
			for (var j = 0; j < hourly_state_time_serie_array.length; j++) {
				hourly_chart1_option.series.push(hourly_state_time_serie_array[j]);
			}
		}
		hourly_chart1.setOption(hourly_chart1_option, true);

	}
    if (chart=="hourly_chart2") {
    	hourly_chart2_option.series.splice(0, hourly_chart2_option.series.length);
    	if (hourly_chart2_types.energy) {
			for (var j = 0; j < hourly_energy_serie_array[cur_energy].length; j++) {
				hourly_chart2_option.series.push(hourly_energy_serie_array[cur_energy][j]);
			}
		}
		if (hourly_chart2_types.output) {
			for (var j = 0; j < hourly_output_serie_array.length; j++) {
				hourly_chart2_option.series.push(hourly_output_serie_array[j]);
			}
		}
		if (hourly_chart2_types.unit_cons) {
			for (var j = 0; j < hourly_unit_cons_serie_array[cur_energy].length; j++) {
				hourly_chart2_option.series.push(hourly_unit_cons_serie_array[cur_energy][j]);
			}
		}
		if (hourly_chart2_types.state_time) {
			for (var j = 0; j < hourly_state_time_serie_array.length; j++) {
				hourly_chart2_option.series.push(hourly_state_time_serie_array[j]);
			}
		}
		hourly_chart2.setOption(hourly_chart2_option, true);

	}
}
function update_chart_series_items() {
	daily_chart1_option.series.splice(0, daily_chart1_option.series.length);
	daily_chart2_option.series.splice(0, daily_chart2_option.series.length);

	// energy
	if (daily_chart1_types.energy) {
		for (var j = 0; j < energy_serie_array[cur_energy].length; j++) {
			daily_chart1_option.series.push(energy_serie_array[cur_energy][j]);
		}
	}
	if (daily_chart2_types.energy) {
		for (var j = 0; j < energy_serie_array[cur_energy].length; j++) {
			daily_chart2_option.series.push(energy_serie_array[cur_energy][j]);
		}
	}

	// output
	if (daily_chart1_types.output) {
		for (var j = 0; j < output_serie_array.length; j++) {
			daily_chart1_option.series.push(output_serie_array[j]);
		}
	}
	if (daily_chart2_types.output) {
		for (var j = 0; j < output_serie_array.length; j++) {
			daily_chart2_option.series.push(output_serie_array[j]);
		}
	}

	// unit_cons
	if (daily_chart1_types.unit_cons) {
		for (var j = 0; j < unit_cons_serie_array[cur_energy].length; j++) {
			daily_chart1_option.series.push(unit_cons_serie_array[cur_energy][j]);
		}
	}
	if (daily_chart2_types.unit_cons) {
		for (var j = 0; j < unit_cons_serie_array[cur_energy].length; j++) {
			daily_chart2_option.series.push(unit_cons_serie_array[cur_energy][j]);
		}
	}

	// state_time
	if (daily_chart1_types.state_time) {
		for (var j = 0; j < state_time_serie_array.length; j++) {
			daily_chart1_option.series.push(state_time_serie_array[j]);
		}
	}
	if (daily_chart2_types.state_time) {
		for (var j = 0; j < state_time_serie_array.length; j++) {
			daily_chart2_option.series.push(state_time_serie_array[j]);
		}
	}
}

function update_chart_series_items_hourly() {
	hourly_chart1_option.series.splice(0, hourly_chart1_option.series.length);
	hourly_chart2_option.series.splice(0, hourly_chart2_option.series.length);

	// energy
	if (hourly_chart1_types.energy) {
		for (var j = 0; j < hourly_energy_serie_array[cur_energy].length; j++) {
			hourly_chart1_option.series.push(hourly_energy_serie_array[cur_energy][j]);
		}
	}
	if (hourly_chart2_types.energy) {
		for (var j = 0; j < hourly_energy_serie_array[cur_energy].length; j++) {
			hourly_chart2_option.series.push(hourly_energy_serie_array[cur_energy][j]);
		}
	}

	// output
	if (hourly_chart1_types.output) {
		for (var j = 0; j < hourly_output_serie_array.length; j++) {
			hourly_chart1_option.series.push(hourly_output_serie_array[j]);
		}
	}
	if (hourly_chart2_types.output) {
		for (var j = 0; j < hourly_output_serie_array.length; j++) {
			hourly_chart2_option.series.push(hourly_output_serie_array[j]);
		}
	}

	// unit_cons
	if (hourly_chart1_types.unit_cons) {
		for (var j = 0; j < hourly_unit_cons_serie_array[cur_energy].length; j++) {
			hourly_chart1_option.series.push(hourly_unit_cons_serie_array[cur_energy][j]);
		}
	}
	if (hourly_chart2_types.unit_cons) {
		for (var j = 0; j < hourly_unit_cons_serie_array[cur_energy].length; j++) {
			hourly_chart2_option.series.push(hourly_unit_cons_serie_array[cur_energy][j]);
		}
	}

	// state_time
	if (hourly_chart1_types.state_time) {
		for (var j = 0; j < hourly_state_time_serie_array.length; j++) {
			hourly_chart1_option.series.push(hourly_state_time_serie_array[j]);
		}
	}
	if (hourly_chart2_types.state_time) {
		for (var j = 0; j < hourly_state_time_serie_array.length; j++) {
			hourly_chart2_option.series.push(hourly_state_time_serie_array[j]);
		}
	}
}


function update_chart_series_items2() {
	if(is_daily_chart1){
		daily_chart1_option.series.splice(0, daily_chart1_option.series.length);
	}
	if(is_daily_chart2){
		daily_chart2_option.series.splice(0, daily_chart2_option.series.length);
	}
	
	if (group_by_node) {
		for (let node of daily_node_set.keys()) {
			if(is_daily_chart1){
				if (daily_chart1_types.energy) {
					daily_chart1_option.series.push(daily_node_cons_series[cur_energy][node]);
				}
				if (daily_chart1_types.output) {
					daily_chart1_option.series.push(daily_node_output_series[node]);
				}
				if (daily_chart1_types.unit_cons) {
					daily_chart1_option.series.push(daily_node_unit_cons_series[cur_energy][node]);
				}
				if (daily_chart1_types.state_time) {
					for (var i = 0; i < daily_node_state_time_series[node].length; i++) {
						daily_chart1_option.series.push(daily_node_state_time_series[node][i]);
					}
				}
			}
			if(is_daily_chart2){
				if (daily_chart2_types.energy) {
					daily_chart2_option.series.push(daily_node_cons_series[cur_energy][node]);
				}
				if (daily_chart2_types.output) {
					daily_chart2_option.series.push(daily_node_output_series[node]);
				}
				if (daily_chart2_types.unit_cons) {
					daily_chart2_option.series.push(daily_node_unit_cons_series[cur_energy][node]);
				}
				if (daily_chart2_types.state_time) {
					for (var i = 0; i < daily_node_state_time_series[node].length; i++) {
						daily_chart2_option.series.push(daily_node_state_time_series[node][i]);
					}
				}
			}
		}
	} else {
		for (let node of daily_node_set.keys()) {
			if (is_daily_chart1) {
				if (daily_chart1_types.energy) {
					daily_chart1_option.series.push(daily_node_cons_series[cur_energy][node]);
				}
			}
			if (is_daily_chart2) {
				if (daily_chart2_types.energy) {
					daily_chart2_option.series.push(daily_node_cons_series[cur_energy][node]);
				}
			}
		}
		
		for (let node of daily_node_set.keys()) {
			if (is_daily_chart1) {
				if (daily_chart1_types.output) {
					daily_chart1_option.series.push(daily_node_output_series[node]);
				}
			}
			if (is_daily_chart2) {
				if (daily_chart2_types.output) {
					daily_chart2_option.series.push(daily_node_output_series[node]);
				}
			}
		}
		
		for (let node of daily_node_set.keys()) {
			if (is_daily_chart1) {
				if (daily_chart1_types.unit_cons) {
					daily_chart1_option.series.push(daily_node_unit_cons_series[cur_energy][node]);
				}
			}
			if (is_daily_chart2) {
				if (daily_chart2_types.unit_cons) {
					daily_chart2_option.series.push(daily_node_unit_cons_series[cur_energy][node]);
				}
			}
		}
		
		for (let node of daily_node_set.keys()) {
			if (is_daily_chart1) {
				if (daily_chart1_types.state_time) {
					for (var i = 0; i < daily_node_state_time_series[node].length; i++) {
						daily_chart1_option.series.push(daily_node_state_time_series[node][i]);
					}
				}
			}
			if (is_daily_chart2) {
				if (daily_chart2_types.state_time) {
					for (var i = 0; i < daily_node_state_time_series[node].length; i++) {
						daily_chart2_option.series.push(daily_node_state_time_series[node][i]);
					}
				}
			}
		}
	}
	
	is_daily_chart1=false;
    is_daily_chart2=false;
    is_hourly_chart1=false;
    is_hourly_chart2=false;
	
/*	
	// energy
	if (daily_chart1_types.energy) {
		for (var j = 0; j < energy_serie_array[cur_energy].length; j++) {
			daily_chart1_option.series.push(energy_serie_array[cur_energy][j]);
		}
	}
	if (daily_chart2_types.energy) {
		for (var j = 0; j < energy_serie_array[cur_energy].length; j++) {
			daily_chart2_option.series.push(energy_serie_array[cur_energy][j]);
		}
	}

	// output
	if (daily_chart1_types.output) {
		for (var j = 0; j < output_serie_array.length; j++) {
			daily_chart1_option.series.push(output_serie_array[j]);
		}
	}
	if (daily_chart2_types.output) {
		for (var j = 0; j < output_serie_array.length; j++) {
			daily_chart2_option.series.push(output_serie_array[j]);
		}
	}

	// unit_cons
	if (daily_chart1_types.unit_cons) {
		for (var j = 0; j < unit_cons_serie_array[cur_energy].length; j++) {
			daily_chart1_option.series.push(unit_cons_serie_array[cur_energy][j]);
		}
	}
	if (daily_chart2_types.unit_cons) {
		for (var j = 0; j < unit_cons_serie_array[cur_energy].length; j++) {
			daily_chart2_option.series.push(unit_cons_serie_array[cur_energy][j]);
		}
	}

	// state_time
	if (daily_chart1_types.state_time) {
		for (var j = 0; j < state_time_serie_array.length; j++) {
			daily_chart1_option.series.push(state_time_serie_array[j]);
		}
	}
	if (daily_chart2_types.state_time) {
		for (var j = 0; j < state_time_serie_array.length; j++) {
			daily_chart2_option.series.push(state_time_serie_array[j]);
		}
	}
*/
}


function load_data() {
//	if (product_line_list != '') {
//		get_product_line_long_name();
//	}
	
	if (hourly) {
		load_hourly_data();
	} else {
		load_daily_data();
	}
}

function switch_daily_hourly() {
	hourly = !hourly;
	$("#daily_box").toggle();
	$("#hourly_box").toggle();

	resize_screen();
}

function switch_group_type_in_chart() {
	group_by_node = !group_by_node;
	
	daily_chart1_option.series.splice(0, daily_chart1_option.series.length);
	daily_chart2_option.series.splice(0, daily_chart2_option.series.length);
	hourly_chart1_option.series.splice(0, hourly_chart1_option.series.length);
	hourly_chart2_option.series.splice(0, hourly_chart2_option.series.length);
	is_daily_chart1=true;
    is_daily_chart2=true;
    is_hourly_chart1=true;
    is_hourly_chart2=true;
	update_chart_series_items2();
	update_chart_series_items_hourly();

	daily_chart1.setOption(daily_chart1_option, true);
	daily_chart2.setOption(daily_chart2_option, true);
	hourly_chart1.setOption(hourly_chart1_option, true);
	hourly_chart2.setOption(hourly_chart2_option, true);

	
}

function load_hourly_state_time() {
	// 结束时间的后一天的0点
	var datetime = new Date(hourly_date + " 00:00:00");
	datetime = new Date(datetime.getTime()+3600000*24);
	var formated_date_time = get_formated_date_time(datetime);

    $.ajax({
        url: path + '/gynh/factory_chart_view/get_hourly_state_time.do',
        type: 'post',
        async: false,
        data: {
			start_time: hourly_date + " 00:00:00",
			end_time: formated_date_time,
			product_line_list: product_line_list, 
        },
        success: function (data) {
        	load_state_time_to_hourly_chart1(data)
        },
        error: function () {
        	alert('error');
        }
    });
}

function load_state_time_to_hourly_chart1(data) {
	var state_list = [
		"MAINT",
		"ETC",
		"RUN",
		"UP",
		"ETIME",
		"IDLE",
		"DOWN",
		"JOBCHG",
		];

	var state_color = [
		"#FFFF00",
		"#C65911",
		"#00B050",
		"#00CC00",
		"#8EA9DB",
		"#808080",
		"#C00000",
		"#0070C0",
		];

	var state_list_num = {
		MAINT:0,
		ETC:1,
		RUN:2,
		UP:3,
		ETIME:4,
		IDLE:5,
		DOWN:6,
		JOBCHG:7,
	};
    var state_time_list = data['data'];
    var max_count = 0;
    var count = 0;
    var temp_eqp_id = '';
	for (var i = 0; i < state_time_list.length; i++) {
		if (state_time_list[i].ln != temp_eqp_id) {
			temp_eqp_id = state_time_list[i].ln;
			max_count = Math.max(max_count, count);
			count = 0;
		}
		count++;
	}
	max_count = Math.max(max_count, count);
	
	var series_data = new Array();
	for (var i = 0; i < product_line_array.length; i++) {
		series_data.push(0);
	}
	
	var series = [];
	var series_templ = {
        type: 'bar',
        animation: false,
        name: '',
        stack: ' ',
        barGap: '5%',
        // label: _label,
        legendHoverLink: false,
        itemStyle: {
            normal: {
                color: 'white',
				label: {
					show: false,
				}
            },
        },
        data: series_data
    };

	for (var i = 0; i < max_count; i++) {
		for (var j = 0; j < state_list.length; j++) {
			var new_series = JSON.parse(JSON.stringify(series_templ));
			new_series.name = state_list[j] + '_' + i;
			new_series.itemStyle.normal.color = state_color[j];
			series.push(new_series);
		}
	}
	
	temp_eqp_id = '';
	var k = 0;
	var eqp_idx = 0;
	for (var i = 0; i < state_time_list.length; i++, k++) {
		if (state_time_list[i].ln != temp_eqp_id) {
			temp_eqp_id = state_time_list[i].ln;
			k = 0;
			for (var e = 0; e < product_line_array.length; e++) {
				if (product_line_array[e] == temp_eqp_id) {
					eqp_idx = e;
				}
			}
		}
		state_time_list[i].ln;
		state_time_list[i].st;
		state_time_list[i].t;
		
		series[state_list.length*k + state_list_num[state_time_list[i].st]].data[eqp_idx] = state_time_list[i].t;
	}
	
	hourly_state_time_chart_option.series = series;
	hourly_state_time_chart_option.yAxis[0].data = product_line_array;
	hourly_state_time_chart.setOption(hourly_state_time_chart_option, true);
	
}
