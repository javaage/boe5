<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/inc/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title></title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport"
		content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<!-- DevExtreme themes -->
	<link rel="stylesheet" type="text/css"href="${path}/res/css/my-css/dx.common.css" />
	<link rel="stylesheet" type="text/css"href="${path}/res/css/my-css/dx.light.css" />
	<link rel="stylesheet" href="${path}/res/layui/css/layui.css" media="all" />
	<script type="text/javascript">
		var path = "${path}";
	</script>
	<script src="${path}/res/js/my-js/jquery.min.js"></script>
	<script src="${path}/res/js/my-js/jquery-3.2.1.min.js"></script>
	<script src="${path}/res/js/my-js/data.js"></script>
	<script src="${path}/res/js/my-js/points_value.js"></script>
	<script src="${path}/res/js/my-js/jszip.min.js"></script>
	<script src="${path}/res/js/my-js/dx.all.js"></script>
	<script src="${path}/res/layui/layui.js"></script>
	<script src="${path}/res/js/echarts/echarts.min.js"></script>

	<style>
		.tabpanel-item {
		    height: 200px;
		    -webkit-touch-callout: none;
		    -webkit-user-select: none;
		    -khtml-user-select: none;
		    -moz-user-select: none;
		    -ms-user-select: none;
		    user-select: none;
		    padding-left: 25px;
		    padding-top: 55px;
		}
		
		.mobile .tabpanel-item {
		    padding-top: 10px;
		}
		
		.tabpanel-item  > div {
		    float: left;
		    padding: 0 85px 10px 10px
		}
		
		.tabpanel-item  p {
		    font-size: 16px;
		    margin: 0;
		}
		
		.item-box {
		    font-size: 16px;
		    margin: 15px 0 45px 10px;
		}
		
		.options {
		    padding: 20px;
		    background-color: rgba(191, 191, 191, 0.15);
		    margin-top: 20px;
		}
		
		.caption {
		    font-size: 18px;
		    font-weight: 500;
		}

		.option {
		    margin-top: 10px;
		}
		
		.dx-datagrid-header-panel .dx-toolbar {
		    margin-bottom: 4px;
		}
	</style>

</head>

<body class="dx-viewport">
	<div style="background:white; float:left; margin-left:12px;margin-top:6px;width:125px;height:37px; position: absolute; z-index: 95;">
		<input type="number" id="hour" max="60" min="1" style="float:left; margin-left:0px;margin-top:2px;width:60px;"
		oninput="if(value>60)value=60;if(value<1)value=5">
		<div id = "table_toolbar_points_value_div" style="height:37px;width:40px; float:left; margin-left:10px;margin-right:0px;"></div>	
	</div>
	<div class="demo-container" style="margin-left:1px;position: absolute; z-index: 90;">
        <div id="longtabs" style="position: absolute; z-index: 93;left:156px;top:0px;width:600px;">
            <div class="tabs-container"></div>
        </div>
	    <div id="total_points_grid" 	style="position: absolute; z-index: 11;left:0px;top:5px;width:100px;heght:100px;"></div>
	    <div id="factory_points_grid" 	style="position: absolute; z-index: 12;left:0px;top:5px;width:100px;heght:100px;"></div>
	    <div id="pscada_points_grid" 	style="position: absolute; z-index: 12;left:0px;top:5px;width:100px;heght:100px;"></div>
	    <div id="fmcs_points_grid" 		style="position: absolute; z-index: 12;left:0px;top:5px;width:100px;heght:100px;"></div>
	    <div id="cub_points_grid" 		style="position: absolute; z-index: 12;left:0px;top:5px;width:100px;heght:100px;"></div>
	    <div id="inst_points_grid" 		style="position: absolute; z-index: 12;left:0px;top:5px;width:100px;heght:100px;"></div>
	    <div id="fmcs_inst_points_grid" style="position: absolute; z-index: 12;left:0px;top:5px;width:100px;heght:100px;"></div>
	</div>
 	<script src='${path}/res/js/my-js/table_toolbar_points_value.js'></script>
    
</body>
</html>