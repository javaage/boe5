// 表的tool bar
var table_toolbar_chart = echarts.init(document.getElementById('table_toolbar_errorcheck_div'), "<%=theme%>");

var delShortcutMode = false;

var table_toolbar_option = {
	toolbox: {
		show: true,
		x:'left',
		showTitle:true,
		borderWidth: 0,
		feature: {
			my_loadDbBtn: {
				show: true,
				title: '加载',
				icon: 'image://'+path+'/res/images/toolbarbtn/db.png',
				onclick: function() {
					ajaxForErrorCheck();
				}
			},
		}
	},
	series: [
		{
            name: ' ',
            type: 'pie',
		}
	]
};

table_toolbar_chart.setOption(table_toolbar_option);

//document.getElementById('table_toolbar_div').style.background='white';

