var department_box;
var line_box;
var product_line_list = ''; 
var product_line_array = [];

function select_box() {
	 department_box = $("#department_box").dxTagBox({
	        items: [],
	        value: [],
	        multiline: false,
	        showSelectionControls: true,
	        showMultiTagOnly: false,
	        maxDisplayedTags: 3,
	        height:35,
	        placeholder:"科室",
	        selectAllText:"全选",
	        searchEnabled: true,
			onValueChanged: function(args) {
				department_list = sel_items_to_list(args.value);
				if (department_list != "") {
					get_product_line_list();
				}
			},
	    }).dxTagBox("instance");
		line_box = $("#line_box").dxTagBox({
	        items: [],
	        value: [],
	        multiline: false,
	        showSelectionControls: true,
	        showMultiTagOnly: false,
	        maxDisplayedTags: 3,
	        height:35,
	        placeholder:"产线",
	        selectAllText:"全选",
	        searchEnabled: true,
			onValueChanged: function(args) {
				product_line_list = sel_items_to_list(args.value);
				product_line_array = args.value;
			},
	    }).dxTagBox("instance");
		
		get_department_list();
}

function get_department_list() {
	var items_array = [];
	if (factory_list != "") {
		if(!ajax_json){
			cons_flow_ajax_url1=path + "/gynh/factory_chart_view/get_department_list.do";
		}
	    $.ajax({
//	        url: path + "/gynh/factory_chart_view/get_department_list.do",
	        url: cons_flow_ajax_url1,
	        type: 'post',
	        async: false,
	        data: {
				factory_list: factory_list, 
	        },
	        success: function (data) {
	        	items_array = query_list_to_array(data['data']);
	        },
	        error: function () {
	        	console.log('error');
	        }
	    });
	}
	department_box.option("items", items_array);
	department_box.option("value", []);
	line_box.option("items", []);
	line_box.option("value", []);
}

function get_product_line_list() {
	var items_array = [];
	if (department_list != "") {
		if(!ajax_json){
			cons_flow_ajax_url2=path + "/gynh/factory_chart_view/get_product_line_list.do";
		}
		$.ajax({
//			url: path + "/gynh/factory_chart_view/get_product_line_list.do",
	        url: cons_flow_ajax_url2,
			type: 'post',
			async: false,
			data: {
				factory_list: factory_list, 
				department_list: department_list, 
			},
			success: function (data) {
				items_array = query_list_to_array(data['data']);
			},
			error: function () {
	        	console.log('error');
			}
		});
	}
	line_box.option("items", items_array);
	line_box.option("value", []);
}

function query_list_to_array(query_list) {
	var arr = new Array();
	for (var i = 0; i < query_list.length; i++) {
		arr.push(query_list[i].str);
	}
	return arr;
}

function sel_items_to_list(sel_items) {
	if (sel_items.length == 0) {
		return "";
	}
	var list = "(";
	for (var i = 0; i < sel_items.length; i++) {
		list = list + "'" + sel_items[i] + "',"
	}
	list = list.substr(0,list.length-1) + ")";
	return list;
}

function load_data_impl(data) {
	
    var nodes = data['nodes'];
    var links = data['links'];
    
	var begin_date = '';//$('#beginTimeBox').val();
	var end_date  = '';//$('#endTimeBox').val();
	if (begin_date == "") begin_date = init_begin_date;
	if (end_date == "") end_date = init_end_date;

	var begin_year = begin_date.substr(0,4)*1;
	var begin_month = begin_date.substr(5,2)*1;
	var begin_day = begin_date.substr(8,2)*1;

	var d1 = new Date(begin_date.replace(/-/g,   "/"))
	var d2 = new Date(end_date.replace(/-/g,   "/"))
	var days = (d2.getTime()-d1.getTime()) / (1000*60*60*24) + 1;
	var nodes_set= new Set();
	for(var i=0;i<links.length;i++){
		nodes_set.add(links[i].source);
		nodes_set.add(links[i].target);
	}
	nodes= new Array();
	for (let set of nodes_set) {
		nodes.push({name:set});

	}
	sankey_chart_option.series[0].data = nodes;
	sankey_chart_option.series[0].links = links;
	links_sum=links.length;
	if (links_sum<60 ){	
		nodeGap=12;
	}else {
		nodeGap=0;
	}
	if (links_sum>60){
		max_height_times=(links_sum/145)+1;
	}
	if (max_height_times>36) {
		max_height_times=36;
	}
	width_times=1;
	height_times=1;
	scroll_flag=true;
	sankey_chart_option.series[0].lineStyle.normal.color = energy_colors[current_energy];
	sankey_chart.setOption(sankey_chart_option, true);
	
}
function onClickEnergyBtn(str) {
	current_energy = str;
	var str01=param1.split('_');
	param1=str01[0]+'_'+str;
	load_data(init_begin_date, init_end_date);
	
	page_custom_resize();
}

function page_custom_init() {
	$("#cub_sum_charts_div").show();
	$("#echarts_div").hide();
}

function page_custom_resize() {

	let h = $(window).height();
	if (height_times!=1) {
		h=912;
	}
	let w = $(window).width();
	var height=h*height_times;
	if (height>16000) {
		height=16000;
	}
	$("#fac_sum_charts_div").width(w*width_times);
	$("#fac_sum_charts_div").height(height-66);
	
	sankey_chart_option.series[0].nodeGap=nodeGap;
	document.getElementById('sankey_chart').style.width = w*width_times-4 + 'px';
	document.getElementById('sankey_chart').style.height = height-88 + 'px';
	
	sankey_chart.setOption(sankey_chart_option, true);
	sankey_chart.resize();
}

window.onresize = function() {
	page_custom_resize();
}
layui.use('laydate', function(){
	  var laydate = layui.laydate;
	  //日期范围
	  laydate.render({
	    elem: '#TimeBox',
	    range: true,
		type: 'date',
		done: function(value) {
			init_begin_date=value.substr(0,10);
			init_end_date=value.substr(13,10);
			load_data(init_begin_date, init_end_date);
			page_custom_resize();
		}
	  });
});

//获取现在时间
function getNowFormatDate() {
    var date = new Date();
    var seperator1 = "-";
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var strDate = date.getDate();
    if (month >= 1 && month <= 9) {
        month = "0" + month;
    }
    if (strDate >= 0 && strDate <= 9) {
        strDate = "0" + strDate;
    }
    var currentdate = year + seperator1 + month + seperator1 + strDate;
    return currentdate;
}

//日期计算
function addDate(date,days){ 
	
    var d=new Date(date); 
    d.setDate(d.getDate()+days); 
    var m=d.getMonth()+1; 
    return d.getFullYear()+'-'+m+'-'+d.getDate(); 
} 

function load_data(begin, end) {
	if  (begin > end) {
		var temp=begin;
		begin = end;
		end = temp;
	}
    var index = top.layer.msg('查询中...',{icon: 16, skin:is_dark?'layui-layer-lan':'', shade:0.1});
    if(!ajax_json){
		cons_flow_ajax_url3=path + requestpath;
	}
    $.ajax({
//        url: path + requestpath,
        url: cons_flow_ajax_url3,
        
        type: 'post',
        async: false,
        data: {
        	start_date: begin, 
        	end_date: end,
        	department_list: department_list,
    		product_line_list: product_line_list,		
        	min_level: min_level,
        	max_level: max_level,
        	param1: param1,
        },
        success: function (data) {
        	
            load_data_impl(data);
        	top.layer.close(index);
        	top.layer.msg('数据加载完成', {icon: 1, skin:is_dark?'layui-layer-lan':''});
        },
        error: function () {
        	top.layer.close(index);
        	top.layer.msg('数据加载错误', {icon: 1, skin:is_dark?'layui-layer-lan':''});
        }
    });
    
    page_custom_resize();
}
