
////点击过滤加载框时需要调用的函数
//function addMySearchBox() {
//	$('.dx-popup-content [role="listbox"]').after("<div id='searchDiv'><input onkeyup='searchField(this)' value='' name='search_input' id='search_input' type='text' placeholder='请输入搜索关键字...' style='border: 1px #ccc solid' /></div>")
//}

//键盘输入需要调用的函数
function searchField(e) {
	//由于前一次的输入隐藏了div，所以在每次输入调用函数之前需要先让它全显示，再从中查询符合条件的隐藏或显示
	$(".itemhide").removeClass("itemhide");
	//获取输入框里的输入的值
	var searchVal = e.value.toLowerCase();
	//循环每个选项框
	$(".dx-list-item .dx-item-content").each(function() {
		//获取选项框里的内容
		var text = $(this).text().toLowerCase();
		if (searchVal == undefined || searchVal == "" || text.indexOf(searchVal) != -1) {//说明是所要模糊查找的对象
			
		} else { //不是所要查找的对象，则隐藏
			//给不符合条件的div加上一个class用于隐藏
			$(this).parent(".dx-list-item").addClass("itemhide");
		}
	});
}

$(function() {
    pivotgrid = $(pivotGridId).dxPivotGrid({
    	// scrollByContent: true,
    	// scrollByThumb: true,
        allowSortingBySummary: true,
        allowSorting: true,
        allowFiltering: true,
        allowExpandAll: true,
        showColumnGrandTotals: bShowColumnGrandTotals,
        showRowGrandTotals: bShowRowGrandTotals,
        showRowTotals: bShowRowTotals,
        showColumnTotals: bShowColumnTotals,
        dataFieldArea: bDataFieldAreaInRow ? 'row' : 'column',
        rowHeaderLayout: bRowHeaderLayoutTree ? 'tree':'standard',
        height: 240,
        scrolling: {
            mode: "virtual"
        },
        showBorders: true,
        fieldChooser: {
            enabled: true,
            allowSearch: true,
        },
        texts: {
        	collapseAll:"收起所有",
        	expandAll:"展开所有",
        	// dataNotAvailable:"",
        	exportToExcel:"导出Excel文件",
        	total:"{0} 合计",
        	grandTotal:"总计",
        	noData:"无数据",
        	removeAllSorting:"取消所有排序",
        	showFieldChooser:"定制Field",
        	sortColumnBySummary:"根据此列的值对[{0}]排序",
        	sortRowBySummary:"根据此行的值对[{0}]排序",
        },
        fieldPanel: {
            showColumnFields: bShowColumnFields,
            showDataFields: bShowDataFields,
            showFilterFields: bShowFilterFields,
            showRowFields: bShowRowFields,
            allowFieldDragging: true,
            visible: true
        },
        stateStoring: {
            enabled: false,
            //type: "localStorage",
            //storageKey: layoutStorageKey
        },
        export: {
            enabled: true,
            fileName: "table"
        },
        onCellClick: function(e) {
        	if  (!chartLocked && e.area == "data" && chartMode != ChartMode.STATIC) {
				clickCell = e.cellElement;
				clickCol = e.columnIndex; 
				clickRow = e.rowIndex;
				loadChart();
        	}
        },
        onContentReady: function(e) {
        	if (bSnapshotLoading) {//调用快照时，需先加载完pivot，再得到clickCell
        		getClickCellInSnapshot();
        		loadChart();
        		bSnapshotLoading = false;
        	}
        },
        onCellPrepared: function(e) {
        	clickCell=null;
        },
        dataSource: {
            fields: tableLayout,
            store: mark
        }
    }).dxPivotGrid("instance");
    
    $("#reset").dxButton({
        text: "Reset",
        onClick: function() {
            pivotgrid.getDataSource().state({});
        }
    });
    
    $("#optionBtn").dxButton({
        text: "选项",
        onClick: function() {
            optionsPopup.option("title", "详细数据");
            optionsPopup.show();
        }
    });
   
    $("#vertical-split").dxCheckBox({
        text: "垂直分割",
        value: false,
        onValueChanged: function(data) {
        	bHSplit = !data.value;
        	resizeScreen(true);
        }
    });


    gridPopup = $("#grid-popup").dxPopup({
        width: 600,
        height: 400,
        contentTemplate: function(contentElement) {
            $("<div />")
                .addClass("drill-down")
                .dxDataGrid({
                    width: 560,
                    height: 300,
                    columns: ["year", "month", "day", "sp"]
                })
                .appendTo(contentElement);
        },
        onShowing: function() {
            $(".drill-down")
                .dxDataGrid("instance")
                .option("dataSource", drillDownDataSource);
        }
    }).dxPopup("instance"); 

    optionsPopup = $("#options-popup").dxPopup({
        width: 500,
        height: 250,
        //contentTemplate: $('#title_template'),
        contentTemplate: function(contentElement) {
            var div = $("<div />");
            div.html(
        	   '<div id="show-row-fields"></div> \
        		<div id="show-column-fields"></div> \
        		<div id="show-data-fields"></div> \
        		<div id="show-filter-fields"></div></br></br> \
        		<div id="row-header-layout"></div> \
        		<div id="data-field-area"></div></br></br> \
        		<div id="show-column-totals"></div> \
        		<div id="show-column-grand-totals"></div> \
        		<div id="show-row-totals"></div> \
        		<div id="show-row-grand-totals"></div>'
            );
            div.appendTo(contentElement);
        },

        onShowing: function() {
    	    $("#show-data-fields").dxCheckBox({
		        text: "数据Fields ",
		        value: bShowDataFields,
		        onValueChanged: function(data) {
		        	bShowDataFields = data.value;
		            pivotgrid.option("fieldPanel.showDataFields", bShowDataFields);
		        }
		    });
		    
		    $("#show-row-fields").dxCheckBox({
		        text: "行Fields ",
		        value: bShowRowFields,
		        onValueChanged: function(data) {
		        	bShowRowFields = data.value;
		            pivotgrid.option("fieldPanel.showRowFields", bShowRowFields);
		        }
		    });
		    
		    $("#show-column-fields").dxCheckBox({
		        text: "列Fields ",
		        value: bShowColumnFields,
		        onValueChanged: function(data) {
		        	bShowColumnFields = data.value;
		            pivotgrid.option("fieldPanel.showColumnFields", bShowColumnFields);
		        }
		    });
		    
		    $("#show-filter-fields").dxCheckBox({
		        text: "过滤Fields ",
		        value: bShowFilterFields,
		        onValueChanged: function(data) {
		        	bShowFilterFields = data.value;
		            pivotgrid.option("fieldPanel.showFilterFields", bShowFilterFields);
		        }
		    });
		
		    $("#show-column-grand-totals").dxCheckBox({
		        text: "列总和  ",
		        value: bShowColumnGrandTotals,
		        onValueChanged: function(data) {
		        	bShowColumnGrandTotals = data.value;
		            pivotgrid.option("showColumnGrandTotals", bShowColumnGrandTotals);
		        }
		    });
		
		    $("#show-column-totals").dxCheckBox({
		        text: "列和 ",
		        value: bShowColumnTotals,
		        onValueChanged: function(data) {
		        	bShowColumnTotals = data.value;
		            pivotgrid.option("showColumnTotals", bShowColumnTotals);
		        }
		    });
		    
		    $("#show-row-grand-totals").dxCheckBox({
		        text: "行总和 ",
		        value: bShowRowGrandTotals,
		        onValueChanged: function(data) {
		        	bShowRowGrandTotals = data.value;
		            pivotgrid.option("showRowGrandTotals", bShowRowGrandTotals);
		        }
		    });
		
		    $("#show-row-totals").dxCheckBox({
		        text: "行和  ",
		        value: bShowRowTotals,
		        onValueChanged: function(data) {
		        	bShowRowTotals = data.value;
		            pivotgrid.option("showRowTotals", bShowRowTotals);
		        }
		    });
		    
		    $("#data-field-area").dxCheckBox({
		        text: "数据字段纵向排列",
		        value: bDataFieldAreaInRow,
		        onValueChanged: function(data) {
		        	bDataFieldAreaInRow = data.value;
		        	pivotgrid.option("dataFieldArea", bDataFieldAreaInRow ? "row" : "column");
		        }
		    });
		    
		    $("#row-header-layout").dxCheckBox({
		        text: "树状行头",
		        value: bRowHeaderLayoutTree,
		        onValueChanged: function(data) {
		        	bRowHeaderLayoutTree = data.value;
		        	pivotgrid.option("rowHeaderLayout", bRowHeaderLayoutTree ? "tree" : "standard");
		        }
		    });

        }
    }).dxPopup("instance"); 
});
