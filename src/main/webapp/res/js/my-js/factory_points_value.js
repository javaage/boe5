
/*var time = getCurrentDate();*/
var h=true;
var windowHeight = 0;
var windowWidth = 0;
var upPanelPersent = 0.5;
var leftPanelPersent = 0.5;
var bHSplit = true; //true:H横切（默认）， false:V纵切
var minute=5;
var total_point_list;
var total_points_value;
var inst_points_list;
var inst_points_value;
var factory_points_grid;
var total_points_grid;
var factory_point_list;
var pscada_point_list;
var fmcs_point_list;
var cub_point_list;
var inst_point_list;
var fmcs_inst_point_list;

var points_values = new Array();

var ptvs;

var longtabs = [
//    { idx:1, z:11, id: "total_points_grid", 	text: "全部累计量点位"},
    { idx:2, z:12, id: "factory_points_grid", 	text: "分厂累计量点位"},
//    { idx:3, z:13, id: "pscada_points_grid", 	text: "PSCADA累计量点位"},
//    { idx:4, z:14, id: "fmcs_points_grid", 		text: "FMCS累计量点位"},
//    { idx:5, z:15, id: "cub_points_grid", 		text: "CUB累计量点位"},
//    { idx:6, z:16, id: "inst_points_grid", 		text: "全部瞬时量点位"},
//    { idx:7, z:16, id: "fmcs_inst_points_grid", text: "FMCS瞬时量点位"},

];

$(document).ready(function() {
	
    typeof page_custom_init === "function" ? page_custom_init() : false;
    
	resizeScreen(true);

	$('#rightSplitter').on('resize', function (event) {
	    var panels = event.args.panels;

		if  (bHSplit) { //default,横切纵排
			upPanelPersent = panels[0].size/(panels[0].size+panels[1].size);
			for (var i = 0; i < longtabs.length; i++) {
				document.getElementById(longtabs[i].id).style.height = panels[0].size-5 + 'px';
			}

			document.getElementById('echarts').style.width = windowWidth + 'px';
			document.getElementById('echarts').style.height = panels[1].size-5 + 'px';
			$('#echarts').css('width', windowWidth + 'px');
			$('#echarts').css('height', panels[1].size-5 + 'px');
			
			echarts.resize(); 
		} else { //纵切横排
			
			leftPanelPersent = panels[0].size/(panels[0].size+panels[1].size);
			for (var i = 0; i < longtabs.length; i++) {
				document.getElementById(longtabs[i].id).style.width = panels[0].size + 'px';
			}
			document.getElementById('echarts').style.height = windowHeight + 'px';
			document.getElementById('echarts').style.width = panels[1].size + 'px';
			$('#echarts').css('width', panels[1].size + 'px');
			$('#echarts').css('height', windowHeight + 'px');
			
			echarts.resize(); 
		}
	});
});

function resizeScreen(force) {
    if (force || windowHeight != $(window).height() || windowWidth != $(window).width()) {
    	
    	windowHeight = $(window).height();
    	windowWidth = $(window).width();

		if (bHSplit) { // default，横切纵排
        	$('#rightSplitter').jqxSplitter({
        		width: $(window).width(), 
        		height: $(window).height(), 
        		orientation: 'horizontal', 
        		panels: [{ size: ((upPanelPersent==1)?0.5:upPanelPersent)*100+'%', collapsible: false }] 
        	});
        	
        	for (var i = 0; i < longtabs.length; i++) {
				document.getElementById(longtabs[i].id).style.height = windowHeight*upPanelPersent-5 + 'px';
        	}
			document.getElementById('echarts').style.width = windowWidth-10 + 'px';
			document.getElementById('echarts').style.height = windowHeight*(1-upPanelPersent)-5 + 'px';
			echarts.resize(); 
		
			if (upPanelPersent == 1) {
				$('#rightSplitter').jqxSplitter('collapse');
			}

		} else { //纵切横排
        	$('#rightSplitter').jqxSplitter({
        		width: $(window).width(), 
        		height: $(window).height(), 
        		orientation: 'vertical', 
        		panels: [{ size: ((leftPanelPersent==1)?0.5:leftPanelPersent)*100+'%', collapsible: false }] 
        	});
        	for (var i = 0; i < longtabs.length; i++) {
				document.getElementById(longtabs[i].id).style.width = windowWidth*leftPanelPersent + 'px';
        	}
			
			document.getElementById('echarts').style.height = windowHeight + 'px';
			document.getElementById('echarts').style.width = windowWidth*(1-leftPanelPersent)-10 + 'px';
			echarts.resize(); 
			
			if (leftPanelPersent == 1) {
				$('#rightSplitter').jqxSplitter('collapse');
			}
		}
    }
    
    typeof page_custom_resize === "function" ? page_custom_resize() : false;
}

function on_resize() {
	var window_height = $(window).height();
	var window_width = $(window).width();
	
	for (var i = 0; i < longtabs.length; i++) {
		$("#"+longtabs[i].id).css("width", window_width-2+'px');
	}
	document.getElementById('echarts').style.width = window_width + 'px';
	echarts.resize(); 
   	$("#longtabs").css("width", window_width-156-385+'px');
}

$(function() {
	on_resize();
	document.getElementById('hour').value = minute;
	window.onresize =function() {
  		on_resize();
   	    resizeScreen(false);
   	}
	init();
	
    var tabsInstance = $("#longtabs > .tabs-container").dxTabs({
        dataSource: longtabs,
        selectedIndex: 0,
        onItemClick: function(e) {
        	var current_z_index = $("#"+e.itemData.id).css("z-index");
        	if (current_z_index != 50) {
        		for (var i = 0; i < longtabs.length; i++) {
        			$("#"+longtabs[i].id).css("z-index", longtabs[i].z);
        		}
        		$("#"+e.itemData.id).css("z-index", 50);
        	}
        }
    }).dxTabs("instance");
    
    $("#"+longtabs[0].id).css("z-index", 50);
});

function getTimeDate_8() {
	var times = new Date().getTime()-60000*minute-3600000*8;

	//var times = new Date().getTime()-3600000*(minute+8);
	var datetime = new Date(times);
	var year = datetime.getFullYear(); //得到年份
	var month = datetime.getMonth();//得到月份
	var date = datetime.getDate();//得到日期
	var day = datetime.getDay();//得到周几
	var hour = datetime.getHours();//得到小时
	var minu = datetime.getMinutes();//得到分钟
	var sec = datetime.getSeconds();//得到秒
	month = month + 1;
	if (month < 10) month = "0" + month;
	if (date < 10) date = "0" + date;
	if (hour < 10) hour = "0" + hour;
	if (minu < 10) minu = "0" + minu;
	if (sec < 10) sec = "0" + sec;
	var time = year + "-" + month + "-" + date+ " " + hour + ":" + minu + ":" + sec;
	return time;
}

function init(){
	
	$.ajax({
		url: path + "/gynh/get_point_list.do",
		type: 'post',
		async: false,
        success: function (data) {
        	factory_point_list = data['factory_point_list'];
        	var list=new Array();
        	for(var i=0;i<factory_point_list.length;i++){
        		if(factory_point_list[i].f==param1){
        			list.push(factory_point_list[i]);
        		}
        	}
        	factory_point_list=list;
        },
        error: function () {
        	alert('error');
        }
    });
	
	//factory_points_grid
	factory_points_grid=$("#factory_points_grid").dxDataGrid({
		dataSource: null,
        showBorders: true,
        showColumnLines: true,
        allowColumnReordering: true,
        allowColumnResizing: true,
        showRowLines: true,
        rowAlternationEnabled: true,
	    columnResizingMode: "widget",
        columnMinWidth: 20,
        filterRow: { visible: true },
        headerFilter: { visible: true },
        "export": {
            enabled: true,
            fileName: "Orders"
        },
        paging: {
            enabled: false
        },
        scrolling: {
            mode: "virtual"
        },
        columnFixing: { 
            enabled: true
        },
        searchPanel: {
            visible: true,
            width: 240,
            placeholder: "搜索..."
        },
        columnChooser: {
            enabled: true,
            mode: "select"
        },
        selection: {
        	mode: "single",
        },
       
        columns: [ 
        	{
                dataField: "pn",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: "点位编号"
            },
        	{
                dataField: "pnm",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: "点位名称"
            },
        	{
                dataField: "en",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: "en"
            },
        	{
                dataField: "f",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: "f"
            },
        	{
                dataField: "d",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: "d"
            },
        	{
                dataField: "ln",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: "ln"
            },
        	{
                dataField: "dv",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: "dv"
            },
        	{
                dataField: "un",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: "un"
            },
            {
                dataField: "t",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: "时间戳"
            },
            {
                dataField: "v",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: "值"
            }
        ],
        onSelectionChanged: function (selectedItems) {
        	if(h){
        		h=false;
        		ptvs = selectedItems.selectedRowsData[0];
                get_value();
        	}
        }
    }).dxDataGrid("instance");
}
	

function get_total_points_value() {
	minute=document.getElementById('hour').value;
	var time =getTimeDate_8();
	var index = top.layer.msg('查询中...',{icon: 16, skin:'', shade:0.1});
	$.ajax({
		url: path + "/gynh/get_points_value.do",
		type: 'post',
		async: true,
		data: {
	    	time: time
	    },
        success: function (data) {
        	total_points_value = data['data'];
        	match_point_and_value();
        	factory_points_grid.option("dataSource", factory_point_list);
        	top.layer.close(index);
        },
        error: function () {
        	alert('error');
        }
    });
}

function get_points_value() {
	get_total_points_value();
}

function match_point_and_value() {
	var j = 0;
	
	for (var i = 0; i < factory_point_list.length; i++) {
		for (; j < total_points_value.length; j++) {
			if (factory_point_list[i].pn == total_points_value[j].p) {
				factory_point_list[i].t = total_points_value[j].t;
				factory_point_list[i].v = total_points_value[j].v;
				j++;
				break;
			} else if (factory_point_list[i].pn < total_points_value[j].p) {
				break;
			}
		}
	}
}

function get_echarts_value(){
	get_value();
}

function get_inst_value(){
	var sel_row = 0;
	ptvs.series = [];
	var begin_time = getTimeDate(-32);
	var end_time = getTimeDate(-8);
	
	var point_number = ptvs.pn;
    $.ajax({
        url: path + '/gynh/get_inst_points_value_by_point_number.do',
        type: 'post',
        async: true,
        data: {
        	begin_time: begin_time, 
        	end_time: end_time, 
        	point_number: point_number
        },
        success: function (data) {
            ptvs.series = data['inst_point_values'];
            set_echarts();
        },
        error: function () {
        }
    });
	
}

function get_value(){
	var sel_row = 0;
	ptvs.series = [];
	var begin_time = getTimeDate(-32);
	var end_time = getTimeDate(-8);
	
	var point_number = ptvs.pn;
	var index = top.layer.msg('查询中...',{icon: 16, skin:'', shade:0.1});
    $.ajax({
        url: path + '/gynh/get_points_value_by_point_number.do',
        type: 'post',
        async: true,
        data: {
        	begin_time: begin_time, 
        	end_time: end_time, 
        	point_number: point_number
        },
        success: function (data) {
            ptvs.series = data['point_values'];
            set_echarts();
            top.layer.close(index);
        },
        error: function () {
        }
    });
}
function set_echarts(){
	var data_series=new Array();
	var colors=new Array();
	colors.push('#AE0000');
	colors.push('#FF44FF');
	colors.push('#0066CC');
	colors.push('#548C00');
	colors.push('#707038');
	colors.push('#5151A2');

	var data_point = new Array();
	var temp = new Array();
	for (var s = 0; s < ptvs.series.length; s++) {
		temp = [];
		temp.push(ptvs.series[s].t);
		temp.push(ptvs.series[s].v);
		data_point.push({value:temp});
	}
	data_series.push({name:ptvs.pn ,type: 'line',color: colors[0],showSymbol: false,hoverAnimation: false,data:data_point});
	option.series = data_series;
	echarts.setOption(option, true);
	h=true;

}
