var pivotgrid;
var gridPopup;
var optionsPopup;
var pivotState_copy
var recordsLimitCount = 20000;

var windowHeight = 0;
var windowWidth = 0;
var upPanelPersent = 0.5;
var leftPanelPersent = 0.5;
var bHSplit = true; //true:H横切（默认）， false:V纵切
var shortcut_buttons_show = false;

var clickCell = null;
var clickCol;
var clickRow;

var currentPowerUnit = 'kWh';//kWh,mWh
var currentCDAUnit = 'L';//L,m³,k(m³)
var currentUPWUnit = 'L';//L,m³,k(m³)
var currentPN2Unit = 'L';//L,m³,k(m³)
var currentTimeUnit = 'M';

var yAxis_show= new Array(true,false,false,false);


var is_cud = false;

var shortcuts;

var ChartMode = {
	COLUMN: 1,
	PIE: 2,
	STATIC: 3,
};
var chartMode = ChartMode.COLUMN;
var isRowChart = true;
var chartLocked = false;
var axisLabelInterval = 0;
var singleLineLabel = false;
var rotateLabel = false;

var chartDiaplayGridDataCols = 1;
var chartDiaplayGridDataRows = 1;
var bDisplayRowColSeriesOnColumnChart = false; //ture:RowCol //false: ColRow [series个数][x个数]
var bDisplayColRowSeriesOnPieChart = false; //ture:RowCol //false: ColRow [pie个数][series个数]
var bStackColumnChart = false;
var bShowSeriesLable = false;
var bShowLegend = true;

var colorObj = new Array(); //被染色的行列单元格

var colTitleLines = 0;
var rowTitleLines = 0;

var snapshotJsonMap = {};
var bSnapshotLoading = false;

//var myColors = ['SpringGreen', 'Tomato', 'Yellow', 'DodgerBlue', 'Sienna', 'MediumOrchid', 'Orange', 'SkyBlue', 'MediumTurquoise', 'Indigo']; 
var myColors = ['#006666', '#2F75B5', '#8EA9DB', '#548235', '#00B050', '#92D050', '#C6E0B4', 'SkyBlue', 'MediumTurquoise', 'Indigo']; 

var init_begin_date =  (typeof defaut_begin_date != "undefined") ? defaut_begin_date : '2018-12-01';//getFormatDate(-1);
var init_end_date = (typeof defaut_end_date != "undefined") ? defaut_end_date : '2018-12-28';//getFormatDate(-1);

// init date control
layui.use('laydate', function(){
	var laydate = layui.laydate;
	if (pageName == 'kpi') {
		laydate.render({
			elem: '#beginTimeBox',
			range: false,
			theme: is_dark?'#444444':'',
			type: 'year',
			value: init_begin_date.substr(0,4),
			done: function(value) {
				init_begin_date=value.substr(0,4)+'-01-01';
				init_end_date=init_begin_date;
				onClickLoadDbBtn();
			}
		});
	}else{
		//日期范围
		laydate.render({
			elem: '#beginTimeBox',
			range: true,
			theme: is_dark?'#444444':'',
			type: 'date',
			value: init_begin_date + ' - ' + init_end_date,
			done: function(value) {
				init_begin_date=value.substr(0,10);
				init_end_date=value.substr(13,10);
				onClickLoadDbBtn();
			}
		});
	}
	  
});

function create_shortcut_dxDataGrid(){
	loadSnapshot();
	$("#gridContainer").dxDataGrid({
        dataSource: shortcuts,
        showBorders: true,
        showColumnLines: true,
        allowColumnReordering: true,
        allowColumnResizing: true,
        showRowLines: true,
        columnResizingMode: "widget",
        columnAutoWidth: true,
        filterRow: { visible: true },
        headerFilter: { visible: true },
        paging: {
            enabled: false
        },
        scrolling: {
            mode: "virtual"
        },
        columnFixing: { 
            enabled: true
        },
        selection: {
        	mode: "single",
        },
        columns: [
        	{
                dataField: "id",
                width: 48,
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: "id"
            },
        	{
                dataField: "name",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: "name"
            }
            
        ],
        onSelectionChanged: function (selectedItems) {
        	apply_shortcut(selectedItems.selectedRowsData[0].state_json);
               
        }
    });
	    
}
function create_chart_style_dxDataGrid() {
	var type_items = ["bar","line"];
	var type_yAxis = [
		{key:"0",name:"能耗"},
		{key:"1",name:"产量"},
		{key:"2",name:"时间"},
		{key:"3",name:"百分比"}];
	var grid_chart_style=$("#grid_chart_style").dxDataGrid({
        dataSource: chart_style,
        showBorders: true,
        showColumnLines: true,
        allowColumnReordering: true,
        allowColumnResizing: true,
        showRowLines: true,
        columnResizingMode: "widget",
        columnAutoWidth: true,
        filterRow: { visible: true },
        headerFilter: { visible: true },
        paging: {
            enabled: false
        },
        scrolling: {
            mode: "virtual"
        },
        columnFixing: { 
            enabled: true
        },
        selection: {
        	mode: "single",
        },
        columns: [
        	{
                dataField: "name",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: "name"
            },
            {
                caption: "type",
                cellTemplate: function(container, options) {
                	
                	$("<div />").dxSelectBox({
                        items: type_items,
                        value: options.data.type,
                        onValueChanged: function(data) {
                        	options.data.type=data.value;
                        }
                    }).appendTo(container);
                }
            },
            {
                caption: "yAxis",
                cellTemplate: function(container, options) {
                	$("<div />").dxSelectBox({
                        items:type_yAxis,
                        value: options.data.yAxis,
                        valueExpr: "key",
                        displayExpr: "name",
                        onValueChanged: function(data) {
                        	options.data.yAxis=data.value;
                        }
                    }).appendTo(container);
                }
            },
            {
                caption: "color",
                cellTemplate: function(container, options) {
                	 $("<div />").dxColorBox({
                	        value: options.data.color,
                	        applyValueMode: "instantly",
                	        onValueChanged: function (e) {
                	        	options.data.color=e.value;
                	        }
                	    }).appendTo(container);
                }
            }
            
        ],
    });	    
}


var timerId=0;
window.onload = function() {
	if (pageName == 'kpi') {
		document.getElementById("beginTimeBox").style.width=80+'px';
		document.getElementById("table_toolbar_div").style.left=100+'px';
	}
	loadSnapshot();	
	timerId = self.setInterval("clock()",1000);	
};

function set_units_on_controles() {
	var units=unit_show.split('_');
	for(var i=0;i<units.length;i++){
		if(units[i]=='power'){
			document.getElementById("select_power").style.display="inline";
			document.getElementById("font_power").style.display="inline";
		}
		if(units[i]=='upw'){
			document.getElementById("select_upw").style.display="inline";
			document.getElementById("font_upw").style.display="inline";
		}
		if(units[i]=='cda'){
			document.getElementById("select_cda").style.display="inline";
			document.getElementById("font_cda").style.display="inline";
		}
		if(units[i]=='pn2'){
			document.getElementById("select_pn2").style.display="inline";
			document.getElementById("font_pn2").style.display="inline";
		}
		if(units[i]=='time'){
			document.getElementById("select_hms").style.display="inline";
			document.getElementById("font_hms").style.display="inline";
		}
	}
	
	var select_hms = document.getElementById("select_hms");
	var select_power = document.getElementById("select_power");
	var select_upw = document.getElementById("select_upw");
	var select_cda = document.getElementById("select_cda");
	var select_pn2 = document.getElementById("select_pn2");
	for(var i=0; i<select_power.options.length; i++){ 
		if(select_power.options[i].value == currentPowerUnit){ 
			select_power.options[i].selected = true; 
		}
	}
	for(var i=0; i<select_cda.options.length; i++){ 
		if(select_cda.options[i].value == currentCDAUnit){ 
			select_cda.options[i].selected = true; 
		}
	}
	for(var i=0; i<select_upw.options.length; i++){ 
		if(select_upw.options[i].value == currentUPWUnit){ 
			select_upw.options[i].selected = true; 
		}
	}
	for(var i=0; i<select_pn2.options.length; i++){ 
		if(select_pn2.options[i].value == currentPN2Unit){ 
			select_pn2.options[i].selected = true; 
		}
	}
	for(var i=0; i<select_hms.options.length; i++){ 
		if(select_hms.options[i].value == currentTimeUnit){ 
			select_hms.options[i].selected = true; 
		}
	}
}

function clock() {
	resizeScreen(true);
	myChart.resize();
	load_default_shortcut();
	search(init_begin_date, init_end_date);
	pivotgrid.getDataSource().state(pivotState_copy);
	set_units_on_controles();
	window.clearInterval(timerId);
}

$(document).ready(function() {
	
    typeof page_custom_init === "function" ? page_custom_init() : false;
    
	resizeScreen(true);

	$('#rightSplitter').on('resize', function (event) {
	    var panels = event.args.panels;

		if  (bHSplit) { //default,横切纵排
			$(pivotGridId).dxPivotGrid({
	        	height: panels[0].size-41
       		});
			upPanelPercent = panels[0].size/(panels[0].size+panels[1].size);
			
			//document.getElementById('table_toolbar_div').style.width = windowWidth + 'px';
			$('#table_toolbar_div').css('width', windowWidth + 'px');
			table_toolbar_chart.resize();

			//document.getElementById('echarts_div').style.width = windowWidth + 'px';
			//document.getElementById('echarts_div').style.height = panels[1].size-5 + 'px';
			$('#echarts_div').css('width', windowWidth + 'px');
			$('#echarts_div').css('height', panels[1].size-5 + 'px');
			myChart.resize(); 
			document.getElementById('unit_bar').style.right = '2px';
			document.getElementById('unit_bar').style.top = '2px';
		} else { //纵切横排
			$(pivotGridId).dxPivotGrid({
	        	width: panels[0].size-1
       		});
			leftPanelPersent = panels[0].size/(panels[0].size+panels[1].size);

			//document.getElementById('table_toolbar_div').style.width = panels[0].size-153 + 'px';
			$('#table_toolbar_div').css('width', panels[0].size-153 + 'px');
			table_toolbar_chart.resize();
			
			//document.getElementById('echarts_div').style.height = windowHeight + 'px';
			//document.getElementById('echarts_div').style.width = panels[1].size + 'px';
			$('#echarts_div').css('width', panels[1].size + 'px');
			$('#echarts_div').css('height', windowHeight + 'px');
			myChart.resize(); 
			
			document.getElementById('unit_bar').style.right = panels[1].size+10 + 'px';
			document.getElementById('unit_bar').style.top = '44px';
		}
	});
});

window.onresize = function() {
    resizeScreen(false);
}

function resizeScreen(force) {
    if (force || windowHeight != $(window).height() || windowWidth != $(window).width()) {
    	windowHeight = $(window).height();
    	windowWidth = $(window).width();

		if (bHSplit) { // default，横切纵排
        	$('#rightSplitter').jqxSplitter({
        		width: $(window).width(), 
        		height: $(window).height(), 
        		orientation: 'horizontal', 
        		panels: [{ size: ((upPanelPersent==1)?0.5:upPanelPersent)*100+'%', collapsible: false }] 
        	});
       		$(pivotGridId).dxPivotGrid({
	        	height: windowHeight*upPanelPersent-41,
	        	width: windowWidth
       		});
			document.getElementById('table_toolbar_div').style.width = windowWidth-145 + 'px';
			table_toolbar_chart.resize();

			document.getElementById('echarts_div').style.width = windowWidth-10 + 'px';
			document.getElementById('echarts_div').style.height = windowHeight*(1-upPanelPersent)-5 + 'px';
			myChart.resize(); 
			document.getElementById('unit_bar').style.right = '12px';
			document.getElementById('unit_bar').style.top = '2px';
			
			if (upPanelPersent == 1) {
				$('#rightSplitter').jqxSplitter('collapse');
			}

		} else { //纵切横排
        	$('#rightSplitter').jqxSplitter({
        		width: $(window).width(), 
        		height: $(window).height(), 
        		orientation: 'vertical', 
        		panels: [{ size: ((leftPanelPersent==1)?0.5:leftPanelPersent)*100+'%', collapsible: false }] 
        	});
       		$(pivotGridId).dxPivotGrid({
	        	width: windowWidth*leftPanelPersent-1,
	        	height: windowHeight-39
       		});
			document.getElementById('table_toolbar_div').style.width = windowWidth*leftPanelPersent-177 + 'px';
			table_toolbar_chart.resize();
			
			document.getElementById('echarts_div').style.height = windowHeight + 'px';
			document.getElementById('echarts_div').style.width = windowWidth*(1-leftPanelPersent)-10 + 'px';
			myChart.resize(); 
			document.getElementById('unit_bar').style.right = windowWidth*(1-leftPanelPersent)+2 + 'px';
			document.getElementById('unit_bar').style.top = '44px';
			
			if (leftPanelPersent == 1) {
				$('#rightSplitter').jqxSplitter('collapse');
			}
		}
    }
    
    typeof page_custom_resize === "function" ? page_custom_resize() : false;
}

function onClickOptionBtn() {
	optionsPopup.option("title", "显示选项");
    optionsPopup.show();
}

function onClickResetBtn() {
	pivotgrid.getDataSource().state({});
	upPanelPersent = 0.5;
	leftPanelPersent = 0.5;
	bHSplit = true;
	resizeScreen(true);
}

function onClickSaveShortcutBtn() {
	top.layer.prompt(function(val, index){
		top.layer.close(index);
		SaveSnapshot(pageName, val, 'icon_path_1', false);
	});
}

function onClickLoadDbBtn() {
	
	var begin = init_begin_date;
	var end = init_end_date;
	
	var  d1   =   new   Date(begin.replace(/-/g,   "/"))
	var  d2   =   new   Date(end.replace(/-/g,   "/"))
	var days = (d2.getTime()-d1.getTime()) / (1000*60*60*24);
	if (days != 0) {
		searchCount(begin, end);
	} else {
		search(begin, end);
	}
}

function onClickVsplitBtn() {
	if  (bHSplit) {
		bHSplit = false;
    	resizeScreen(true);
	}
}

function onClickHsplitBtn() {
	if  (!bHSplit) {
		bHSplit = true;
    	resizeScreen(true);
    }
}

function search(begin, end) {
	if  (!bLoadFromDb) {
		return;
	}

	if  (begin > end) {
		var temp=begin;
		begin = end;
		end = temp;
	}
	
	var beginYear = begin.substr(0,4)*1;
	var beginMonth = begin.substr(5,2)*1;
	var beginDay =  begin.substr(8,2)*1;
	var endYear = end.substr(0,4)*1;
	var endMonth = end.substr(5,2)*1;
	var endDay = end.substr(8,2)*1;
	
    var index = top.layer.msg('查询中...',{icon: 16, skin:is_dark?'layui-layer-lan':'', shade:0.1});
    if(!ajax_json){
    	main_ajax_url=path + requestpath;
    }
    $.ajax({
//        url: path + requestpath,
    	url:main_ajax_url,
        type: 'post',
        async: false,
        // data: data.field,
        data: {
        	beginYear:beginYear, 
        	beginMonth:beginMonth, 
        	beginDay:beginDay, 
        	endYear:endYear, 
        	endMonth:endMonth, 
        	endDay:endDay, 
        	param1:param1,
        },
        success: function (data) {
        	var jsonStr = JSON.stringify(data,null,4);
        	console.log(jsonStr);
            mark = data['data'];
            typeof loadOtherData === "function" ? loadOtherData(data) : false;
        	translateTime('S', currentTimeUnit);
        	translate('L', currentCDAUnit,'cda');
        	translate('L', currentUPWUnit,'upw');
        	translate('L', currentPN2Unit,'pn2');
        	translatePower('kWh', currentPowerUnit);
        	pivotgrid.option("dataSource.store", mark);
        	top.layer.close(index);
        	top.layer.msg('数据加载完成', {icon: 1, skin:is_dark?'layui-layer-lan':''});
        },
        error: function () {
        	top.layer.close(index);
        	top.layer.msg('数据加载错误', {icon: 1, skin:is_dark?'layui-layer-lan':''});
        }
    });
    
	// resizeScreen(true);
}

function searchCount(begin, end) {
	if  (!bLoadFromDb) {
		return;
	}
	
	if  (begin > end) {
		var temp=begin;
		begin = end;
		end = temp;
	}
	
	var beginYear = begin.substr(0,4)*1;
	var beginMonth = begin.substr(5,2)*1;
	var beginDay =  begin.substr(8,2)*1;
	var beginTime = 0;//begin.substr(11,2)*10000 + begin.substr(14,2)*100 + begin.substr(17,2)*1;
	var endYear = end.substr(0,4)*1;
	var endMonth = end.substr(5,2)*1;
	var endDay = end.substr(8,2)*1;
	var endTime = 0;//end.substr(11,2)*10000 + end.substr(14,2)*100 + end.substr(17,2)*1;
	
	var index = top.layer.msg('查询中...',{time:1000,icon: 16, skin:is_dark?'layui-layer-lan':'', shade:0.8});
	$.ajax({
		url: path + request_count_path,
    	
		type: 'post',
		async: false,
		data: {
			beginYear:beginYear, 
			beginMonth:beginMonth, 
			beginDay:beginDay, 
			beginTime:beginTime, 
			endYear:endYear, 
			endMonth:endMonth, 
			endDay:endDay, 
			endTime:endTime,
			param1:param1,
		},
		success: function (data) {
			var jsonStr = JSON.stringify(data,null,4);
			console.log(data);
			top.layer.close(index);
			if (data.data>recordsLimitCount) {
				top.layer.confirm('本次一共有'+data.data+"条数据,是否继续加载？", {
					skin:is_dark?'layui-layer-lan':'',
					btn: ['是','否'] // 按钮
				}, function() {
					top.layer.msg('正在加载', {icon: 1, skin:is_dark?'layui-layer-lan':''});
					search(begin, end);
				}, function() {
					top.layer.msg('请重新选择范围', {icon: 1, skin:is_dark?'layui-layer-lan':''});
				});
			} else{
				search(begin, end);
			}
		},
		error: function () {
			top.layer.close(index);
			top.layer.msg('数据加载错误', {icon: 1, skin:is_dark?'layui-layer-lan':''});
		}
	});
}

function SaveSnapshot(page, name, icon, when_close) {
//	return SaveShortcut(page, user, name, icon);
	
	var pivotState = pivotgrid.getDataSource().state();
	var optionForColumnChartToSave = optionForColumnChart;
	optionForColumnChartToSave.series.splice(1, optionForColumnChartToSave.series.length);
	optionForColumnChartToSave.series[0].name = '';
	optionForColumnChartToSave.series[0].stack = '';
	optionForColumnChartToSave.series[0].data = [0];

	var optionForPieChartToSave = optionForPieChart;
	//todo:

	var cfg = {
			currentPowerUnit: currentPowerUnit,
			currentCDAUnit: currentCDAUnit,
			currentUPWUnit: currentUPWUnit,
			currentPN2Unit: currentPN2Unit,
			currentTimeUnit: currentTimeUnit,
			chart_style:chart_style,
			pivotState: pivotState,
			bShowDataFields: bShowDataFields,
			bShowRowFields: bShowRowFields,
			bShowColumnFields: bShowColumnFields,
			bShowFilterFields: bShowFilterFields,
			bShowColumnTotals: bShowColumnTotals,
			bShowColumnGrandTotals: bShowColumnGrandTotals,
			bShowRowTotals: bShowRowTotals,
			bShowRowGrandTotals: bShowRowGrandTotals,
			bDataFieldAreaInRow: bDataFieldAreaInRow,
			bRowHeaderLayoutTree: bRowHeaderLayoutTree,
			
			bHSplit: bHSplit,
			shortcut_buttons_show : shortcut_buttons_show,
			upPanelPersent: upPanelPersent,
			leftPanelPersent: leftPanelPersent,
			chartMode: chartMode,
			optionForColumnChart: optionForColumnChartToSave,
			optionForPieChart:optionForPieChartToSave,

			isRowChart: isRowChart,
			chartLocked: chartLocked,
			axisLabelInterval: axisLabelInterval,
			singleLineLabel: singleLineLabel,
			rotateLabel: rotateLabel,
			chartDiaplayGridDataCols: chartDiaplayGridDataCols,
			chartDiaplayGridDataRows: chartDiaplayGridDataRows,
			bDisplayRowColSeriesOnColumnChart: bDisplayRowColSeriesOnColumnChart,
			bDisplayColRowSeriesOnPieChart: bDisplayColRowSeriesOnPieChart,
			bStackColumnChart: bStackColumnChart,
			bShowSeriesLable: bShowSeriesLable,
			bShowLegend: bShowLegend,
			clickCol: clickCol,
			clickRow: clickRow,

		};

	strJson = JSON.stringify(cfg, function(key, val) {
		if (typeof val === 'function') {
			return val + '';
		}
		return val;
	});
	
    $.ajax({
        url: when_close ? (path + "/gynh/biz/save_default_shortcut.do") : (path + "/gynh/biz/save_shortcut.do"),
        type: 'post',
        async: false,
        data: {
        	page:page, 
        	name:name, 
        	icon:icon, 
        	state_json:strJson,
        },
        success: function (data) {
        	if (!when_close) {
        		top.layer.msg('保存成功', {icon: 1, skin:is_dark?'layui-layer-lan':''});
	        	loadSnapshot();
        	}
        },
        error: function () {
        	if (!when_close) {
        		top.layer.msg('保存失败', {icon: 1, skin:is_dark?'layui-layer-lan':''});
        	}
        }
    });
}

function loadSnapshot() {

	$.ajax({
		url: path + "/gynh/biz/load_shortcut.do",
		type: 'post',
		async: false,
		data: {
			page_name:pageName, 
		},
		success: function (data) {
			
			shortcuts = data['shortcut'];
			
			var my_idx = 0;
			var public_idx = 0;
			for (var i = 0; i < shortcuts.length; ++i) {
				var is_public = shortcuts[i].user_id == 0;
				var icon_path = '';
				var prefix = '';
				if (is_public) {
					prefix = 'my__0_'; //公有的前缀
					public_idx++;
					shortcuts[i].id=public_idx;
					icon_path = 'image://'+path+'/res/images/toolbarbtn/we' + public_idx + '.png'
				} else {
					prefix = 'my__1_'; //私有的前缀
					my_idx++;
					shortcuts[i].id=my_idx;
					icon_path = 'image://'+path+'/res/images/toolbarbtn/' + my_idx + '.png'
				}

				snapshotJsonMap[prefix + shortcuts[i].name] = shortcuts[i].state_json;
				table_toolbar_option.toolbox.feature[prefix+shortcuts[i].name] = {
					show: shortcut_buttons_show,
					title: shortcuts[i].name,
					icon: icon_path,
					onclick: function(arg1, arg2, arg3) {
						if (delShortcutMode) {
							DelShortcut(pageName, arg3); //arg3 is name
						} else {
							apply_shortcut(snapshotJsonMap[arg3]);
						}
					}
				};
			}
			table_toolbar_chart.setOption(table_toolbar_option, true);
		},
		error: function () {
		
		}
	});

}

function SaveShortcut(page, user, name, icon) {
	var pivotState = pivotgrid.getDataSource().state();
	var pivotStateJson = JSON.stringify(pivotState);
    $.ajax({
        url: path + "/gynh/biz/save_shortcut.do",
        type: 'post',
        async: false,
        data: {
        	page:page, 
        	user:user, 
        	name:name, 
        	icon:icon, 
        	state_json:pivotStateJson,
        },
        success: function (data) {
        	top.layer.msg('保存成功', {icon: 1, skin:is_dark?'layui-layer-lan':''});
        	loadSnapshot();
        },
        error: function () {
        	top.layer.msg('保存失败', {icon: 1, skin:is_dark?'layui-layer-lan':''});
        }
    });
}


function DelShortcut(page, name) {
	$.ajax({
		url: path + "/gynh/biz/del_shortcut.do",
		type: 'post',
		async: false,
		data: {
        	page:page, 
        	name:name.substr(6), 
        	is_public: name.substr(0,6) == "my__0_",
        },
		success: function (data) {
			var deleted = data['deleted'];
			if (deleted) {
				for(var key in table_toolbar_option.toolbox.feature) {
					if (key.substr(0,6) == name.substr(0,6))
						delete table_toolbar_option.toolbox.feature[key];
				}
				loadSnapshot();
	        	top.layer.msg('已删除', {icon: 1, skin:is_dark?'layui-layer-lan':''});
			}
		},
		error: function () {
        	top.layer.msg('删除失败', {icon: 1, skin:is_dark?'layui-layer-lan':''});
		}
	});
}

function loadChart() {
	
	if (clickCell == null) {
		if (chartMode == ChartMode.PIE) {
			myChart.setOption(optionForPieChart, true);
		} else
//			if (chartMode == ChartMode.COLUMN)
		{
			myChart.setOption(optionForColumnChart, true);
		} 
		return;
	} else if (typeof(clickCell.length) == "undefined" || clickCell.length == 0 || typeof(clickCell[0].nodeName) == "undefined" || clickCell[0].nodeName.toUpperCase() != 'TD') {
		clickCell = null;
		return;
	}
	
	
	yAxis_show= new Array(true,false,false,false);
	
	var clickGrid = clickCell;
	var temp = 0;
	while (true) {
		clickGrid = clickGrid.parent();
		var str=clickGrid.attr('class');
		if (typeof(str) != "undefined" && (str.indexOf("dx-pivotgrid-border") > -1 && str.indexOf("dx-word-wrap") > -1)) {
			break;
		}
		if (temp++ >= 1000) {
			clickCell = null;
			return;
		}
	} // todo:容错
	
	if (scrolling_virtual) {
		scrolling_virtual = 1;
	} else {
		scrolling_virtual = 0;
	}
	
    var maxCols = clickCell.parent().children().length;
    var maxRows = clickCell.parent().parent().children().length;

	// rowTitleMatrix
	var rowTitleMatrix = new Array();
	{
		var colTrsA = clickGrid.children().eq(3).children().eq(0).children().eq(0).children().eq(0).children().eq(0).children().eq(0).children().eq(scrolling_virtual).children().eq(1);//<tbody class="dx-pivotgrid-vertical-headers">
		//var rowTitleCols = clickGrid.children().eq(2).children().eq(0).children().eq(0).children().eq(0).children().eq(1).children().eq(0).children().length;

		var rowTitleCols = 0;
	    for (var row = 0; row < colTrsA.children().length; row++) {
	        for (var col = 0,realCol = 0; col < colTrsA.children().eq(row).children().length; col++) {
	        	var spanCols = colTrsA.children().eq(row).children().eq(col).attr('colspan');
	        	if  (typeof(spanCols) == "undefined") {
	        		spanCols = 1;
	        	} else {
	        		spanCols = spanCols*1;
	        	}
	        	rowTitleCols += spanCols;
	        }
	        break;
	    }
		// 建立内容为\t的数组
	    for (var i=0; i<colTrsA.children().length; i++) {
	    	rowTitleMatrix[i] = new Array();
	        for (var j=0;j<rowTitleCols;j++) {
	        	rowTitleMatrix[i][j] = "\t";
	        }
	    }
	    for (var row = 0; row < colTrsA.children().length; row++) {
	        for (var col = 0,realCol = 0; col < colTrsA.children().eq(row).children().length; col++) {
	        	var colText = colTrsA.children().eq(row).children().eq(col).children().eq(colTrsA.children().eq(row).children().eq(col).children().length-1).text(); //the last one is column title
	        	var spanRows = colTrsA.children().eq(row).children().eq(col).attr('rowspan');
	        	if  (typeof(spanRows) == "undefined") {
	        		spanRows = 1;
	        	} else {
	        		spanRows = spanRows*1;
	        	}
	
	        	while (rowTitleMatrix[row][realCol] != "\t") {
	        		realCol++;
	        	}
	    		rowTitleMatrix[row][realCol] = colText;
	        	for (var k = row + 1; k < row+spanRows; k++) {
	        		rowTitleMatrix[k][realCol] = colText;
	        	}
	        }
	    }
	    
	}
	
	// colTitleMatrix
	var colTitleMatrix = new Array();
	{
		// find column header: <thead class="dx-pivotgrid-horizontal-headers">
		var colTrs = clickGrid.children().eq(2).children().eq(1).children().eq(0).children().eq(0).children().eq(0).children().eq(0).children().eq(scrolling_virtual).children().eq(1);
		colTitleLines = colTrs.children().length;
	    for (var i=0;i<colTrs.children().length;i++) {
	    	colTitleMatrix[i] = new Array();
	        for (var j=0;j<maxCols;j++) {
	        	colTitleMatrix[i][j] = "";
	        }
	    }
	
	    for (var row = 0; row < colTrs.children().length; row++) {
	        for (var col = 0, realCol = 0; col < colTrs.children().eq(row).children().length; col++) {
	        	//正确取得所有的柱状图坐标
	        	var colText="";
	        	for (var i=0;i<colTrs.children().eq(row).children().eq(col).children().length;i++) {
	        		colText = colTrs.children().eq(row).children().eq(col).children().eq(i).text();
	        		if (colText != null && colText != ""){
	        			break;
	        		}
    			}
	        	var spanCols = colTrs.children().eq(row).children().eq(col).attr('colspan');
	        	if  (typeof(spanCols) == "undefined") {
	        		spanCols = 1; // default
	        	} else {
	            	spanCols = spanCols*1; // string to number
	        	}
	        	var spanRows = colTrs.children().eq(row).children().eq(col).attr('rowspan');
	        	if  (typeof(spanRows) == "undefined") {
	        		spanRows = 1;
	        	} else {
	        		spanRows = spanRows*1;
	        	}
	        	
	        	for (var j = 0; j < spanCols; j++, realCol++) {
	        		while (colTitleMatrix[row][realCol] == "\t") { // \t means skip(empty)
	        			realCol++;
	        		}
	    			colTitleMatrix[row][realCol] = colText; // fill same text to right span cells
	        		for (var k = row+1; k < row + spanRows; k++) {
	        			colTitleMatrix[k][realCol] = "\t"; // fill skip flag in down span cells
	        		} 
	        	}
	        }
	    }
    }
    
 	var colTitleArray = new Array(maxCols);
	{
	    for (var j=0;j<maxCols;j++) {
	        for (var i=0;i<colTrs.children().length;i++) {
	        	if  (typeof(colTitleArray[j]) == "undefined") {
	        		colTitleArray[j] = "";
	        	}
	        	if  (colTitleMatrix[i][j] != "\t") {
	        		colTitleArray[j] = colTitleArray[j] + "\n" + colTitleMatrix[i][j];
	        	}
	        }
	    }
	    
	    // Get rid of the first one "-"
	    for (var i = 0; i<colTitleArray.length;i++) {
	    	colTitleArray[i] = colTitleArray[i].substring(1);
	    }
	}
    
    var rowTitleArray = new Array();
    {
		for (var k = 0;k < rowTitleMatrix.length ;k++) {
			var str = "";
			for (var j = 0; j <rowTitleMatrix[k].length ; j++) {
				if (rowTitleMatrix[k][j] != "	") {
			    	str += rowTitleMatrix[k][j] + "\n";
				}
			}
			if (str.charAt(str.length-1) == '\n') {
				str = str.substr(0, str.length-1);
			}
			rowTitleArray[k] = str;
		}
	}
	
	// rowTitleLines
	var num=0;
	for (var i = 0; i<rowTitleArray.length; i++) {
		var split = rowTitleArray[i].split("\n");
		if (split.length > num) {
			num = split.length;
		}
	}
	rowTitleLines = num;

	// series data
    var seriesDataByRowCol = new Array(); //[row][col] [series个数][x个数]
    var seriesDataByColRow = new Array(); //[col][row] [series个数][x个数]
    var seriesDataByRowColForPie = new Array(); //[row][col] [pie个数][series个数]
    var seriesDataByColRowForPie = new Array(); //[col][row] [pie个数][series个数]
    
    if (isRowChart) {
	    // 控制显示的行范围不超过: [1, maxRows]
	    chartDiaplayGridDataRows = Math.min(maxRows, chartDiaplayGridDataRows);
	    chartDiaplayGridDataRows = Math.max(1, chartDiaplayGridDataRows);
	    seriesDataByRowCol = new Array(chartDiaplayGridDataRows);
	    seriesDataByColRow = new Array(maxCols); //cols
	    seriesDataByRowColForPie = new Array(chartDiaplayGridDataRows);
	    seriesDataByColRowForPie = new Array(maxCols); //cols
	    for (var col = 0; col < maxCols; col++) {
	    	seriesDataByColRow[col] = new Array(chartDiaplayGridDataRows);
	    	seriesDataByColRowForPie[col] = new Array(chartDiaplayGridDataRows);
	    }
	    for (var i = 0; i < chartDiaplayGridDataRows; i++) { //row
	    	var row = clickRow+i;
	    	seriesDataByRowCol[i] = new Array(maxCols);
	    	seriesDataByRowColForPie[i] = new Array(maxCols);
		    for (var col = 0; col < maxCols; col++) { //col
		    	var cellVal = clickCell.parent().parent().children().eq(row).children().eq(col).children().eq(0).text().replace(/,/g, "");
		    	cellVal = cellVal.replace(/%/g, "");
		    	seriesDataByRowCol[i][col] = cellVal;
		    	seriesDataByColRow[col][i] = cellVal;
		    	seriesDataByRowColForPie[i][col] = {name:colTitleArray[col], value:cellVal};
		    	seriesDataByColRowForPie[col][i] = {name:rowTitleArray[row], value:cellVal};
		    }
	    }
    } else {
	    // 控制显示的列范围不超过: [1, maxCols]
	    chartDiaplayGridDataCols = Math.min(maxCols, chartDiaplayGridDataCols);
	    chartDiaplayGridDataCols = Math.max(1, chartDiaplayGridDataCols);
	    
	    seriesDataByColRow = new Array(chartDiaplayGridDataCols);
	    seriesDataByColRowForPie = new Array(chartDiaplayGridDataCols);
	    seriesDataByRowCol = new Array(maxRows);
	    seriesDataByRowColForPie = new Array(maxRows);
	    for (var row = 0; row < maxRows; row++) {
	    	seriesDataByRowCol[row] = new Array(chartDiaplayGridDataCols);
	    	seriesDataByRowColForPie[row] = new Array(chartDiaplayGridDataCols);
	    }
	    for (var i = 0; i < chartDiaplayGridDataCols; i++) { //col
	    	var col = clickCol + i;
	    	seriesDataByColRow[i] = new Array(maxCols);
	    	seriesDataByColRowForPie[i] = new Array(maxCols);
		    for (var row = 0; row<maxRows; row++) { //row
		    	var cellVal = clickCell.parent().parent().children().eq(row).children().eq(col).text().replace(/,/g, "");
		    	cellVal = cellVal.replace(/%/g, "");
		    	seriesDataByColRow[i][row] = cellVal;
		    	seriesDataByRowCol[row][i] = cellVal;
		    	seriesDataByColRowForPie[i][row] = {name:rowTitleArray[row], value:cellVal};
		    	seriesDataByRowColForPie[row][i] = {name:colTitleArray[col], value:cellVal};
		    }
	    }
    }
    
    //clear color
    for (var i = 0; i < colorObj.length; i++) {
    	colorObj[i].style.background = "";
    	colorObj[i].style.color = "";
    }
    colorObj = [];
    
    // color
    colorSelectedCells();
    
    var seriesCount = 0;
	if (isRowChart) {
		seriesCount = (bDisplayRowColSeriesOnColumnChart ? chartDiaplayGridDataRows : maxCols);
		//只需要选中的行
		rowTitleArray = rowTitleArray.splice(clickRow, chartDiaplayGridDataRows);
	} else {
		seriesCount = (bDisplayRowColSeriesOnColumnChart ? maxRows : chartDiaplayGridDataCols);
		//列标题作为x轴坐标时，只需要选中的列
		colTitleArray = colTitleArray.splice(clickCol, chartDiaplayGridDataCols);
	}
	
	//set data
	{
		var xAXisLabels = bDisplayRowColSeriesOnColumnChart ? colTitleArray.slice() : rowTitleArray.slice();
		if (singleLineLabel) {
			for (var i = 0; i < xAXisLabels.length; i++) {
				var a = xAXisLabels[i].split("\n");
				xAXisLabels[i] = a[a.length-1];
			}
			optionForColumnChart.grid.y2 = 13+16 +'px';
		} else {
			optionForColumnChart.grid.y2 = (bDisplayRowColSeriesOnColumnChart ? colTitleLines : rowTitleLines)*13+16 + "px";
		}
		if (rotateLabel) {
			var len = 0;
			for (var i = 0; i < xAXisLabels.length; i++) {
				var list = xAXisLabels[i].split("\n");
				for (var j = 0; j < list.length; j++) {
					len = Math.max(len, list[j].length);
				}
			}
			optionForColumnChart.grid.y2 = len*6 + 24 + "px";
		}
		
		
		//柱形图
		optionForColumnChart.xAxis[0].data = xAXisLabels;
		optionForColumnChart.xAxis[0].axisLabel.interval = axisLabelInterval;

		optionForColumnChart.legend.data = bDisplayRowColSeriesOnColumnChart ? rowTitleArray : colTitleArray;
		
		//series清空重填
		optionForColumnChart.series.splice(0, optionForColumnChart.series.length);
		for (var i = 0; i < seriesCount; i++) {
			
			var series_name = optionForColumnChart.legend.data[i];
		
			var is_line_series = false;
			
			for (var lf = 0; lf < line_fileds.length; lf++) {
				if (series_name.indexOf(line_fileds[lf]) > -1) {
					is_line_series = true;
					break;
				}
			}
			var chart_color=myColors[i%myColors.length];
			
			var energy_color = [{
		            name: "Power",
		            color:power_color  
		        }, {
		        	name: "UPW",
		            color:upw_color  
		        }, {
		        	name: "CDA",
		            color:cda_color  
		        }, {
		        	name: "PN2",
		            color:pn2_color  
		        }];
			
			for (var ch=0;ch<chart_style.length;ch++) {
				if (typeof(series_name) == "undefined") {
		    		continue;
		    	}
				if (series_name.indexOf(chart_style[ch].name) > -1) {
					if(chart_style[ch].type=='line'){
						is_line_series = true;
					}else{
						is_line_series = false;
					}
					
					var yAxisIndex_series =chart_style[ch].yAxis;
					chart_color = chart_style[ch].color;
					
					for (var en=0;en<energy_color.length;en++) {
						if (series_name.indexOf(energy_color[en].name) > -1) {
							if(chart_style[ch].color==null||chart_style[ch].color==""){
								chart_color=energy_color[en].color;
							}
						}
					}
					
					break;
				}
			}
			for (var y=0; y<yAxis_show.length;y++) {
				if (yAxisIndex_series==y) {
					yAxis_show[y]=true;
				}
			}
			
			if (is_line_series) {
				seriesDataByRowCol[i] = seriesDataByRowCol[i].map(function (item) {
					return item == 0 ? '-' : item;
				});
				seriesDataByColRow[i] = seriesDataByColRow[i].map(function (item) {
					return item == 0 ? '-' : item;
				});

			}

			optionForColumnChart.series.push({
				name: series_name,
				type: is_line_series ? 'line' : 'bar',
				yAxisIndex: yAxisIndex_series,
				stack: bStackColumnChart ? ' ' : 's'+i,
				itemStyle: {
					normal: {
						color:chart_color,
						barBorderRadius:[3, 3, 3, 3],
						label: {
							textStyle: {color:'yellow'},
							show: bShowSeriesLable,
//							position: 'insideRight'
						}
					}
				},
				data: (bDisplayRowColSeriesOnColumnChart ? seriesDataByRowCol[i] : seriesDataByColRow[i])
			});
		}
		optionForColumnChart.title.text = "";
		var chart_offset=0;
		
		for (var y=0; y<yAxis_show.length;y++) {
			if (yAxis_show[y]) {
				optionForColumnChart.yAxis[y].show=true;
			} else {
				optionForColumnChart.yAxis[y].show=false;
			}
		}
		for (var y=1; y<yAxis_show.length;y++) {
			if (yAxis_show[y]) {
				optionForColumnChart.yAxis[y].offset=chart_offset;
				chart_offset=chart_offset+72;		
			}
		}

		// pie
		var pieCount = (bDisplayColRowSeriesOnPieChart ? colTitleArray.length : rowTitleArray.length);
		optionForPieChart.legend.data = bDisplayColRowSeriesOnPieChart ? rowTitleArray : colTitleArray;
		optionForPieChart.series.splice(0, optionForPieChart.series.length);
		for (var i = 0; i < pieCount; i++) {
			optionForPieChart.series.push({
				name: bDisplayColRowSeriesOnPieChart ? colTitleArray[i] : rowTitleArray[i],
				type: 'pie',
	            radius : '55%',
	            center: [(80/pieCount/2)*(2*i+1)+'%', '60%'],
				data: (bDisplayColRowSeriesOnPieChart ? seriesDataByColRowForPie[i] : seriesDataByRowColForPie[i])
			});
		}
	}
	
	if (chartMode == ChartMode.PIE) {
		myChart.setOption(optionForPieChart, true);
	} else
//		if (chartMode == ChartMode.COLUMN)
	{
		myChart.setOption(optionForColumnChart, true);
	} 
}

function getClickCellInSnapshot() {
	clickCell = null;
	if (typeof(clickRow) != "undefined" && typeof(clickCol) != "undefined") {
		var cell = $(pivotGridId).children().eq(0).children().eq(2).children().eq(3).children().eq(1).children().eq(0).children().eq(0).children().eq(0).children().eq(0).children().eq(1).children().eq(1).children().eq(clickRow).children().eq(clickCol);
		//var cellShadow = $(pivotGridId).children().eq(0).children().eq(2).children().eq(3).children().eq(1).children().eq(0).children().eq(0).children().eq(0).children().eq(0).children().eq(0).children().eq(0).children().eq(1).children().eq(clickRow).children().eq(clickCol);
		if (typeof(cell) != "undefined") {
			clickCell = cell;
		}
	}
}
	
function stackTiled() {
	bStackColumnChart = !bStackColumnChart;
	for (var i = 0; i < optionForColumnChart.series.length; i++) {
		optionForColumnChart.series[i].stack = bStackColumnChart ? ' ' : 's'+i;
	}
	myChart.setOption(optionForColumnChart, true);
}

function showHideSeriesLable() {
	bShowSeriesLable = !bShowSeriesLable;
	for (var i = 0; i < optionForColumnChart.series.length; i++) {
		optionForColumnChart.series[i].itemStyle.normal.label.show = bShowSeriesLable;
	}
	myChart.setOption(optionForColumnChart, true);
}

function colorSelectedCells() {
	if (clickCell == null) {
		return;
	}
	
    clickCell[0].style.color = is_dark ? "yellow" : "blue";
    colorObj.push(clickCell[0]);
	if (isRowChart) {
		var tbody = clickCell.parent().parent();
		for (var row = clickRow; row < Math.min(clickRow + chartDiaplayGridDataRows, tbody.children().length); row++) {
			var tr = tbody.children().eq(row);
			tr[0].style.background = is_dark ? "#646464" : "#EEEEF5";
			colorObj.push(tr[0]);
		}
	} else {
		var tbody = clickCell.parent().parent();
		for (var col = clickCol; col < Math.min(clickCol + chartDiaplayGridDataCols, clickCell.parent().children().length); col++) {
			for (var row = 0; row < tbody.children().length; row++) {
				var td = tbody.children().eq(row).children().eq(col);
				td[0].style.background = is_dark ? "#646464" : "#EEEEF5";
				colorObj.push(td[0]);
			}
		}
	}
}

function setHVSplitBtnState() {
	table_toolbar_option.toolbox.feature.my_HVSplit.icon = 'image://'+path+'/res/images/toolbarbtn/' + (bHSplit ? 'hsplit.png' : 'vsplit.png');
	table_toolbar_chart.setOption({
		toolbox: {
			feature: {
				my_HVSplit: {
					icon: table_toolbar_option.toolbox.feature.my_HVSplit.icon
				}
			}
		}
	});
}

function show_hide_shortcut_buttons() {
	shortcut_buttons_show = !shortcut_buttons_show;
	table_toolbar_option.toolbox.feature.my_show_hide_shortcut_buttons.icon = 'image://'+path+'/res/images/toolbarbtn/' + (shortcut_buttons_show ? 'collapse.png' : 'expand.png');
	table_toolbar_chart.setOption({
		toolbox: {
			feature: {
				my_show_hide_shortcut_buttons: {
					icon: table_toolbar_option.toolbox.feature.my_show_hide_shortcut_buttons.icon
				}
			}
		}
	});

	for(var key in table_toolbar_option.toolbox.feature) {
		if (key.substr(0,4) == "my__") {
			table_toolbar_option.toolbox.feature[key].show = shortcut_buttons_show;
		}
	}

	table_toolbar_chart.setOption(table_toolbar_option, true);
}

function on_click_cub_sum() {
	if ($("#echarts_div").is(':hidden')) {
		$("#echarts_div").show();
		$("#cub_sum_charts_div").hide();
	} else {
		$("#cub_sum_charts_div").show();
		$("#echarts_div").hide();
	}
}

function apply_shortcut(json) {
	bSnapshotLoading = true;
	var cfg = JSON.parse(json, function(k,v) {
		if(v.indexOf&&v.indexOf('function')>-1) {
			return eval("(function(){return "+v+" })()")
		}
		return v;
	});
	
	bHSplit = cfg.bHSplit;
	upPanelPersent = cfg.upPanelPersent;
	leftPanelPersent = cfg.leftPanelPersent;
	resizeScreen(true);
	
	currentPowerUnit= cfg.currentPowerUnit;
	currentCDAUnit= cfg.currentCDAUnit;
	currentUPWUnit= cfg.currentUPWUnit;
	currentPN2Unit= cfg.currentPN2Unit;
	currentTimeUnit=cfg.currentTimeUnit;
	
	if (currentPowerUnit==null||currentPowerUnit=='') {
    	currentPowerUnit="kWh";
    }
    if (currentCDAUnit==null||currentCDAUnit=='') {
    	currentCDAUnit="L";
    }
    if (currentUPWUnit==null||currentUPWUnit=='') {
    	currentUPWUnit="L";
    }
    if (currentPN2Unit==null||currentPN2Unit=='') {
    	currentPN2Unit="L";
    }
    if (currentTimeUnit==null||currentTimeUnit=='') {
    	currentTimeUnit="M";
    }
	
	chartMode = cfg.chartMode;
	chart_style_copy=chart_style;
	chart_style=cfg.chart_style;
	if (typeof(chart_style) == "undefined") {
		chart_style=chart_style_copy;
	}
	cfg.optionForColumnChart.grid=optionForColumnChart.grid;
	cfg.optionForColumnChart.yAxis=optionForColumnChart.yAxis;
	cfg.optionForColumnChart.toolbox.feature=optionForColumnChart.toolbox.feature;
	
	optionForColumnChart = cfg.optionForColumnChart;
	optionForPieChart = cfg.optionForPieChart;
	isRowChart = cfg.isRowChart;
	chartLocked = cfg.chartLocked;
	axisLabelInterval = cfg.axisLabelInterval;
	singleLineLabel = cfg.singleLineLabel;
	rotateLabel = cfg.rotateLabel;
	chartDiaplayGridDataCols = cfg.chartDiaplayGridDataCols;
	chartDiaplayGridDataRows = cfg.chartDiaplayGridDataRows;
	bDisplayRowColSeriesOnColumnChart = cfg.bDisplayRowColSeriesOnColumnChart;
	bDisplayColRowSeriesOnPieChart = cfg.bDisplayColRowSeriesOnPieChart;
	bStackColumnChart = cfg.bStackColumnChart;
	bShowSeriesLable = cfg.bShowSeriesLable;
	bShowLegend = cfg.bShowLegend;

	setHVSplitBtnState();
	shortcut_buttons_show = typeof(cfg.shortcut_buttons_show) == "undefined" ? false : cfg.shortcut_buttons_show;
	table_toolbar_option.toolbox.feature.my_show_hide_shortcut_buttons.icon = 'image://'+path+'/res/images/toolbarbtn/' + (shortcut_buttons_show ? 'collapse.png' : 'expand.png');
	for(var key in table_toolbar_option.toolbox.feature) {
		if (key.substr(0,4) == "my__") {
			table_toolbar_option.toolbox.feature[key].show = shortcut_buttons_show;
		}
	}
	table_toolbar_chart.setOption(table_toolbar_option, true);
	
	if (chartMode == ChartMode.PIE) {
		myChart.setOption(optionForPieChart, true);
	} else {
		myChart.setOption(optionForColumnChart, true);
	}//todo:static

	clickRow = cfg.clickRow;
	clickCol = cfg.clickCol;
	pivotState_copy=cfg.pivotState;
	bShowDataFields = cfg.bShowDataFields;
	bShowRowFields = cfg.bShowRowFields;
	bShowColumnFields = cfg.bShowColumnFields;
	bShowFilterFields = cfg.bShowFilterFields;
	bShowColumnTotals = cfg.bShowColumnTotals;
	bShowColumnGrandTotals = cfg.bShowColumnGrandTotals;
	bShowRowTotals = cfg.bShowRowTotals;
	bShowRowGrandTotals = cfg.bShowRowGrandTotals;
	bDataFieldAreaInRow = cfg.bDataFieldAreaInRow;
	bRowHeaderLayoutTree = cfg.bRowHeaderLayoutTree;

    pivotgrid.option("fieldPanel.showDataFields", bShowDataFields);
    pivotgrid.option("fieldPanel.showRowFields", bShowRowFields);
    pivotgrid.option("fieldPanel.showColumnFields", bShowColumnFields);
    pivotgrid.option("fieldPanel.showFilterFields", bShowFilterFields);
    pivotgrid.option("showColumnGrandTotals", bShowColumnGrandTotals);
    pivotgrid.option("showColumnTotals", bShowColumnTotals);
    pivotgrid.option("showRowGrandTotals", bShowRowGrandTotals);
    pivotgrid.option("showRowTotals", bShowRowTotals);
	pivotgrid.option("dataFieldArea", bDataFieldAreaInRow ? "row" : "column");
	pivotgrid.option("rowHeaderLayout", bRowHeaderLayoutTree ? "tree" : "standard");
	
}
function load_default_shortcut() {
	
	$.ajax({
		url: path + "/gynh/biz/load_default_shortcut.do",
		type: 'post',
		async: false,
		data: {
			page_name:pageName, 
		},
		success: function (data) {
			var shortcuts = data['shortcut'];
			if (shortcuts.length == 1) {
				apply_shortcut(shortcuts[0].state_json);
			}
        },
        error: function () {
        	//top.layer.close(index);
        	//top.layer.msg('数据加载错误', {icon: 1, skin:is_dark?'layui-layer-lan':''});
        }
	});
	
}

window.onbeforeunload= function(event) {
}

window.onunload = function(event) {
	SaveSnapshot(pageName, '', 'icon_path_1', true);
}








//option = {
//	    tooltip : {
//	        formatter: "{a} <br/>{c} {b}"
//	    },
//	    toolbox: {
//	        show: true,
//	        feature: {
//	            restore: {show: true},
//	            saveAsImage: {show: true}
//	        }
//	    },
//	    series : [
//	        {
//	            name: '月/定额',
//	            type: 'gauge',
//	            z: 3,
//	            min: 0,
//	            max: 1000,
//	            splitNumber: 31,
//	            radius: '50%',
//	            axisLine: {            // 坐标轴线
//	                lineStyle: {       // 属性lineStyle控制线条样式
//	                    width: 10
//	                }
//	            },
//	            itemStyle: { //仪表盘指针样式
//	                normal: {
//	                    color: '#ca8622',
//	                    shadowColor: 'rgba(0, 0, 0, 0.5)',
//	                    shadowBlur: 10,
//	                    shadowOffsetX: 2,
//	                    shadowOffsetY: 2
//	                }
//	            },
//	            axisTick: {            // 坐标轴小标记
//	                length: 15,        // 属性length控制线长
//	                lineStyle: {       // 属性lineStyle控制线条样式
//	                    color: 'auto'
//	                }
//	            },
//	            splitLine: {           // 分隔线
//	                length: 20,         // 属性length控制线长
//	                lineStyle: {       // 属性lineStyle（详见lineStyle）控制线条样式
//	                    color: 'auto'
//	                }
//	            },
//	            axisLabel: {
//	                formatter: function (value) {
//	                          return value.toFixed(0);
//	                      
//	                }
//	            },
//	            detail: {formatter: function (value) {
//	                return "";
//	                  
//	            }},
//	            data:[{value: 400, name: 'L'}]
//	        },
//	        {
//	            name: '月/实际',
//	            type: 'gauge',
//	            z: 3,
//	            min: 0,
//	            max: 1000,
//	            splitNumber: 31,
//	            radius: '50%',
//	            axisLine: {            // 坐标轴线
//	                lineStyle: {       // 属性lineStyle控制线条样式
//	                    width: 10
//	                }
//	            },
//	            axisTick: {            // 坐标轴小标记
//	                length: 15,        // 属性length控制线长
//	                lineStyle: {       // 属性lineStyle控制线条样式
//	                    color:'auto'
//	                }
//	            },
//	            splitLine: {           // 分隔线
//	                length: 20,         // 属性length控制线长
//	                lineStyle: {       // 属性lineStyle（详见lineStyle）控制线条样式
//	                    color: 'auto'
//	                }
//	            },
//	            axisLabel: {
//	                formatter: function (value) {
//	                    return value.toFixed(0);
//	                      
//	                }
//	            },
//	            detail: {formatter:'月\n{value}'},
//	            data:[{value: 380, name: 'L'}]
//	        },
//	        {
//	            name: '年/定额',
//	            type: 'gauge',
//	            center: ['20%', '55%'],    // 默认全局居中
//	            radius: '35%',
//	            min:0,
//	            max:12000,
//	            startAngle:225,
//	            endAngle:45,
//	            splitNumber:12,
//	             axisLabel: {
//	                formatter: function (value) {
//	                    return value.toFixed(0);
//	                }
//	            },
//	            itemStyle: { //仪表盘指针样式
//	                normal: {
//	                    color: '#ca8622',
//	                    shadowColor: 'rgba(0, 0, 0, 0.5)',
//	                    shadowBlur: 10,
//	                    shadowOffsetX: 2,
//	                    shadowOffsetY: 2
//	                }
//	            },
//	            axisLine: {            // 坐标轴线
//	                lineStyle: {       // 属性lineStyle控制线条样式
//	                    width: 8
//	                }
//	            },
//	            axisTick: {            // 坐标轴小标记
//	                length:12,        // 属性length控制线长
//	                lineStyle: {       // 属性lineStyle控制线条样式
//	                    color: 'auto'
//	                }
//	            },
//	            splitLine: {           // 分隔线
//	                length:20,         // 属性length控制线长
//	                lineStyle: {       // 属性lineStyle（详见lineStyle）控制线条样式
//	                    color: 'auto'
//	                }
//	            },
//	            pointer: {
//	                width:5
//	            },
//	            title: {
//	                offsetCenter: [0, '-30%'],       // x, y，单位px
//	            },
//	           
//	            data:[{value: 800, name: 'L'}],
//	             detail: { formatter: function (value) {
//	                          return "";
//	                      
//	                }},
//	        },
//	        {
//	            name: '年/实际',
//	            type: 'gauge',
//	            center: ['20%', '55%'],    // 默认全局居中
//	            radius: '35%',
//	            min:0,
//	            max:12000,
//	            startAngle:225,
//	            endAngle:45,
//	            splitNumber:12,
//	            axisLabel: {
//	                formatter: function (value) {
//	                          return value.toFixed(0);
//	                      
//	                }
//	            },
//	            axisLine: {            // 坐标轴线
//	                lineStyle: {       // 属性lineStyle控制线条样式
//	                    width: 8
//	                }
//	            },
//	            axisTick: {            // 坐标轴小标记
//	                length:12,        // 属性length控制线长
//	                lineStyle: {       // 属性lineStyle控制线条样式
//	                    color: 'auto'
//	                }
//	            },
//	            splitLine: {           // 分隔线
//	                length:20,         // 属性length控制线长
//	                lineStyle: {       // 属性lineStyle（详见lineStyle）控制线条样式
//	                    color: 'auto'
//	                }
//	            },
//	            pointer: {
//	                width:5
//	            },
//	            title: {
//	                offsetCenter: [0, '-30%'],       // x, y，单位px
//	            },
//	           
//	            data:[{value:4000 , name: 'L'}],
//	             detail: {formatter:'年\n{value}'},
//	        },
//	         {
//	            name: '日/定额',
//	            type: 'gauge',
//	            center: ['77%', '50%'],    // 默认全局居中
//	            radius: '30%',
//	            min: 0,
//	            max: 48,
//	            startAngle:135,
//	            endAngle: -45,
//	            splitNumber: 24,
//	            axisLabel: {
//	                formatter: function (value) {
//	                return value.toFixed(0);
//	                }
//	            },
//	            itemStyle: { //仪表盘指针样式
//	                normal: {
//	                    color: '#ca8622',
//	                    shadowColor: 'rgba(0, 0, 0, 0.5)',
//	                    shadowBlur: 10,
//	                    shadowOffsetX: 2,
//	                    shadowOffsetY: 2
//	                }
//	            },
//	            axisLine: {            // 坐标轴线
//	                lineStyle: {       // 属性lineStyle控制线条样式
//	                    width: 8
//	                }
//	            },
//	            axisTick: {            // 坐标轴小标记
//	                splitNumber: 5,
//	                length: 10,        // 属性length控制线长
//	                lineStyle: {        // 属性lineStyle控制线条样式
//	                    color: 'auto'
//	                }
//	            },
//	            splitLine: {           // 分隔线
//	                length: 15,         // 属性length控制线长
//	                lineStyle: {       // 属性lineStyle（详见lineStyle）控制线条样式
//	                    color: 'auto'
//	                }
//	            },
//	            pointer: {
//	                width:2
//	            },
//	            data:[{value: 40, name: 'L'}],
//	             detail: {formatter:'日\n{value}'},
//	        },
//	        {
//	            name: '日/实际',
//	            type: 'gauge',
//	            center: ['77%', '50%'],    // 默认全局居中
//	            radius: '30%',
//	            min: 0,
//	            max: 48,
//	            startAngle:135,
//	            endAngle: -45,
//	            splitNumber: 24,
//	            axisLabel: {
//	                formatter: function (value) {
//	                return value.toFixed(0);
//	            }
//	               },
//	            axisLine: {            // 坐标轴线
//	                lineStyle: {       // 属性lineStyle控制线条样式
//	                    width: 8
//	                }
//	            },
//	            axisTick: {            // 坐标轴小标记
//	                splitNumber: 5,
//	                length: 10,        // 属性length控制线长
//	                lineStyle: {        // 属性lineStyle控制线条样式
//	                    color: 'auto'
//	                }
//	            },
//	            splitLine: {           // 分隔线
//	                length: 15,         // 属性length控制线长
//	                lineStyle: {       // 属性lineStyle（详见lineStyle）控制线条样式
//	                    color: 'auto'
//	                }
//	            },
//	            pointer: {
//	                width:2
//	            },
//	            data:[{value:32, name: 'L'}],
//	            detail: {
//	                 formatter: function (value) {
//	                return "";
//	                 }
//	            },
//	        },
//	       
//	    ]
//	};
//
//
//	    myChart.setOption(option,true);


//axisLine: {            // 坐标轴线
//    lineStyle: {       // 属性lineStyle控制线条样式
//        width: 8,
//        color: [
//        [0.3, '#67e0e3'],
//        [0.7, '#37a2da'],
//        [1, '#fd666d']
//    ]
//    }
//},
