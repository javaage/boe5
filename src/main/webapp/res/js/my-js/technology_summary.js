var power_departments_cons=new Array();
var upw_departments_cons=new Array();
var cda_departments_cons=new Array();
var pn2_departments_cons=new Array();

var power_product_line_unit_cons=new Array();
var upw_product_line_unit_cons=new Array();
var pn2_product_line_unit_cons=new Array();
var cda_product_line_unit_cons=new Array();
var power_last_product_line_unit_cons=new Array();
var upw_last_product_line_unit_cons=new Array();
var pn2_last_product_line_unit_cons=new Array();
var cda_last_product_line_unit_cons=new Array();

var power_compare_data=new Array();
var upw_compare_data=new Array();
var pn2_compare_data=new Array();
var cda_compare_data=new Array();

var power_cons_trend=new Array();
var upw_cons_trend=new Array();
var cda_cons_trend=new Array();
var pn2_cons_trend=new Array();

function on_resize() {
	var window_height = $(window).height();
	var window_width = $(window).width();
	var td_div=document.getElementsByClassName('td_div');
	for(var i=0;i<td_div.length;i++) {
		td_div[i].style.width =(window_width-8*8-8*2)/4+'px';
		td_div[i].style.height =(window_height-8*8-30)/4+'px';
	}
	var spancss=document.getElementsByClassName('spancss');
	for(var i=0;i<spancss.length;i++) {
		spancss[i].style.height =(window_height-8*8-30)/4+'px';
	}
	var spanall=document.getElementsByClassName('spanall');
	for(var i=0;i<spanall.length;i++) {
		spanall[i].style.width =(window_width-8*8-8*2)/4+'px';
	}
	
	left_power_chart.resize();
	left_upw_chart.resize();
	left_cda_chart.resize();
	left_pn2_chart.resize();
	
	center_power_chart.resize();
	center_upw_chart.resize();
	center_cda_chart.resize();
	center_pn2_chart.resize();
	
	right_power_chart.resize();
	right_upw_chart.resize();
	right_cda_chart.resize();
	right_pn2_chart.resize();
	
}

$(function() {
	
	window.onresize =function() {
  		on_resize();
   	}
	get_dapartment_cons();
	get_cons_trend();
	get_total();
	get_product_line_unit_cons();
	on_resize();
	     
});

function get_dapartment_cons() { 
	if(!ajax_json){
    	technology_summary_ajax_url1=path + '/gynh/get_departments_cons.do';
    }
	$.ajax({
//        url: path + '/gynh/get_departments_cons.do',
        url: technology_summary_ajax_url1,
        type: 'get',
        data:{date:defaut_end_date,factory:factory},
        async: true,
        success: function (data) {
        	var jsonStr = JSON.stringify(data,null,4);
        	console.log(jsonStr);

        	power_departments_cons=data['power_departments_cons'];
        	upw_departments_cons=data['upw_departments_cons'];
        	cda_departments_cons=data['cda_departments_cons'];
        	pn2_departments_cons=data['pn2_departments_cons'];
        	
        	var color=['#d48265', '#91c7ae','#749f83','#ca8622','#bda29a','#6e7074', '#546570', '#c4ccd3','#FFA488','#FFBB66','	#99BBFF','#FA8072','#F4A460','#D8BFD8','#F5DEB3'];
        	var department_color=[
        		{name:'AMHS',color:'#c23531'},
        		{name:'AT',color:'#2f4554'},
        		{name:'OLED',color:'#61a0a8'},
        	]
        	var set_department= new Set();
        	for (var i = 0; i < power_departments_cons.length; i++) {
        		set_department.add(power_departments_cons[i].name);
			}
        	for (var i = 0; i < upw_departments_cons.length; i++) {
        		set_department.add(upw_departments_cons[i].name);
			}
        	for (var i = 0; i < cda_departments_cons.length; i++) {
        		set_department.add(cda_departments_cons[i].name);
			}
        	for (var i = 0; i < pn2_departments_cons.length; i++) {
        		set_department.add(pn2_departments_cons[i].name);
			}
        	var h=0;
        	for (let iterable_element of set_department) {
				if(iterable_element!='AMHS'&&iterable_element!='AT'&&iterable_element!='OLED') {
					department_color.push({
						name:iterable_element,
						color:color[h]
					}
					);
					h++;
				}
			}
        	
        	power_departments_cons=data['power_departments_cons'];
        	upw_departments_cons=data['upw_departments_cons'];
        	cda_departments_cons=data['cda_departments_cons'];
        	pn2_departments_cons=data['pn2_departments_cons'];
        	
        	for (var i = 0; i < power_departments_cons.length; i++) {
        		for (var j = 0; j < department_color.length; j++) {
        			if(power_departments_cons[i].name==department_color[j].name){
        				right_power_chart_option.series[0].data.push({
        					name:power_departments_cons[i].name,
        					value:power_departments_cons[i].value,
        					itemStyle:{
        						color:department_color[j].color,
        					}
        				}
        				)
					}
        			
        		}
			}
        	
        	for (var i = 0; i < upw_departments_cons.length; i++) {
        		for (var j = 0; j < department_color.length; j++) {
        			if(upw_departments_cons[i].name==department_color[j].name){
        				right_upw_chart_option.series[0].data.push({
        					name:upw_departments_cons[i].name,
        					value:upw_departments_cons[i].value,
        					itemStyle:{
        						color:department_color[j].color,
        					}
        				}
        				)
					}
        			
        		}
			}
        	
        	for (var i = 0; i < cda_departments_cons.length; i++) {
        		for (var j = 0; j < department_color.length; j++) {
        			if(cda_departments_cons[i].name==department_color[j].name){
        				right_cda_chart_option.series[0].data.push({
        					name:cda_departments_cons[i].name,
        					value:cda_departments_cons[i].value,
        					itemStyle:{
        						color:department_color[j].color,
        					}
        				}
        				)
					}
        			
        		}
			}
        	
        	for (var i = 0; i < pn2_departments_cons.length; i++) {
        		for (var j = 0; j < department_color.length; j++) {
        			if(pn2_departments_cons[i].name==department_color[j].name){
        				right_pn2_chart_option.series[0].data.push({
        					name:pn2_departments_cons[i].name,
        					value:pn2_departments_cons[i].value,
        					itemStyle:{
        						color:department_color[j].color,
        					}
        				}
        				)
					}
        			
        		}
			}
        	
        	right_power_chart.setOption(right_power_chart_option,true);
        	right_upw_chart.setOption(right_upw_chart_option,true);
        	right_cda_chart.setOption(right_cda_chart_option,true);
        	right_pn2_chart.setOption(right_pn2_chart_option,true);


        }
    });
}

function get_cons_trend() {
	if(!ajax_json){
    	technology_summary_ajax_url2=path + '/gynh/get_cons_trend.do';
    }
	var begin_time=defaut_end_date.substring(0,7)+'-01'
	$.ajax({
//        url: path + '/gynh/get_cons_trend.do',
        url: technology_summary_ajax_url2,
        type: 'get',
        data:{begindate:begin_time,enddate:defaut_end_date,factory:factory},
        async: true,
        success: function (data) {
        	var jsonStr = JSON.stringify(data,null,4);
        	console.log(jsonStr);
        	left_power_chart_option.xAxis[0].data=get_date_name(data['power_cons_trend']);
        	left_upw_chart_option.xAxis[0].data=get_date_name(data['upw_cons_trend']);
        	left_cda_chart_option.xAxis[0].data=get_date_name(data['cda_cons_trend']);
        	left_pn2_chart_option.xAxis[0].data=get_date_name(data['pn2_cons_trend']);
        	
        	left_power_chart_option.series[0].data=get_date_value(data['power_cons_trend']);
        	left_upw_chart_option.series[0].data=get_date_value(data['upw_cons_trend']);
        	left_cda_chart_option.series[0].data=get_date_value(data['cda_cons_trend']);
        	left_pn2_chart_option.series[0].data=get_date_value(data['pn2_cons_trend']);
        	
        	
//        	var total=data['summary'];
//        	
//        	left_power_chart_option.xAxis[0].data=get_total_data_name(total,"Power");
//        	left_upw_chart_option.xAxis[0].data=get_total_data_name(total,"UPW");
//        	left_cda_chart_option.xAxis[0].data=get_total_data_name(total,"CDA");
//        	left_pn2_chart_option.xAxis[0].data=get_total_data_name(total,"PN2");
//        	
//        	left_power_chart_option.series[0].data=get_total_data_value(total,"Power");
//        	left_upw_chart_option.series[0].data=get_total_data_value(total,'UPW');
//        	left_cda_chart_option.series[0].data=get_total_data_value(total,'CDA');
//        	left_pn2_chart_option.series[0].data=get_total_data_value(total,'PN2');
//        	
        	left_power_chart.setOption(left_power_chart_option,true);
        	left_upw_chart.setOption(left_upw_chart_option,true);
        	left_cda_chart.setOption(left_cda_chart_option,true);
        	left_pn2_chart.setOption(left_pn2_chart_option,true);


        }
    });
}

function get_product_line_unit_cons() {
	var d =0-parseInt(getFormatDate(0).substring(8,10))-1;
	var lastmonth=getFormatDate(d);
	if(!ajax_json){
    	technology_summary_ajax_url3=path + '/gynh/get_product_line_unit_cons.do';
    }
	$.ajax({
//        url: path + '/gynh/get_product_line_unit_cons.do',
        url: technology_summary_ajax_url3,
        type: 'get',
        data:{
        	begindate:getFormatDate(-1),
        	enddate:getFormatDate(0),
        	lastmonth:lastmonth,
        	factory:factory
        	},
	        async: true,
	        success: function (data) {
	        	var jsonStr = JSON.stringify(data,null,4);
	        	console.log(jsonStr);
	        	var product_line_unit_cons=data['product_line_unit_cons'];
	        	var last_product_line_unit_cons=data['last_product_line_unit_cons'];
	        	
	        	for(var i=0;i<product_line_unit_cons.length;i++){
	        		if(product_line_unit_cons[i].energy=="Power") {
	        			power_product_line_unit_cons.push(product_line_unit_cons[i])
	        		}else if(product_line_unit_cons[i].energy=="UPW") {
	        			upw_product_line_unit_cons.push(product_line_unit_cons[i])
	        		}if(product_line_unit_cons[i].energy=="PN2") {
	        			pn2_product_line_unit_cons.push(product_line_unit_cons[i])
	        		}if(product_line_unit_cons[i].energy=="CDA") {
	        			cda_product_line_unit_cons.push(product_line_unit_cons[i])
	        		}
	        	}
	        	for(var i=0;i<last_product_line_unit_cons.length;i++){
	        		if(last_product_line_unit_cons[i].energy=="Power") {
	        			power_last_product_line_unit_cons.push(last_product_line_unit_cons[i])
	        		}else if(last_product_line_unit_cons[i].energy=="UPW") {
	        			upw_last_product_line_unit_cons.push(last_product_line_unit_cons[i])
	        		}if(last_product_line_unit_cons[i].energy=="PN2") {
	        			pn2_last_product_line_unit_cons.push(last_product_line_unit_cons[i])
	        		}if(last_product_line_unit_cons[i].energy=="CDA") {
	        			cda_last_product_line_unit_cons.push(last_product_line_unit_cons[i])
	        		}
	        	}
	        	product_line_unit_cons_compare(power_product_line_unit_cons,power_last_product_line_unit_cons,'power_note');
	        	product_line_unit_cons_compare(upw_product_line_unit_cons,upw_last_product_line_unit_cons,'upw_note');
	        	product_line_unit_cons_compare(pn2_product_line_unit_cons,pn2_last_product_line_unit_cons,'cda_note');
	        	product_line_unit_cons_compare(cda_product_line_unit_cons,cda_last_product_line_unit_cons,'pn2_note');

        }
    });
}
function get_total() {
	if(!ajax_json){
    	technology_summary_ajax_url4=path + "/gynh/technology_summary/get_totals.do";
    }
	 $.ajax({
//	        url: path + "/gynh/technology_summary/get_totals.do",
	        url: technology_summary_ajax_url4,
	        type: 'post',
	        async: true,
	        data: {
	        	begindate:defaut_begin_date,
	        	enddate:defaut_end_date,
	        	factory:factory
	        },
	        success: function (data) {
	        	var jsonStr = JSON.stringify(data,null,4);
	        	console.log(jsonStr);
	        	
	        	var factory_unit_cons=data['factory_unit_cons'];
	        	var power_factory_unit_cons=new Array();
	        	var upw_factory_unit_cons=new Array();
	        	var pn2_factory_unit_cons=new Array();
	        	var cda_factory_unit_cons=new Array();
	        	
	        	var factory_product=data['factory_product'];
	        	var power_factory_product=new Array();
	        	
	        	for(var i=0;i<factory_unit_cons.length;i++){
	        		if(factory_unit_cons[i].energy=="Power") {
	        			power_factory_unit_cons.push(factory_unit_cons[i])
	        		}else if(factory_unit_cons[i].energy=="UPW") {
	        			upw_factory_unit_cons.push(factory_unit_cons[i])
	        		}if(factory_unit_cons[i].energy=="PN2") {
	        			pn2_factory_unit_cons.push(factory_unit_cons[i])
	        		}if(factory_unit_cons[i].energy=="CDA") {
	        			cda_factory_unit_cons.push(factory_unit_cons[i])
	        		}
	        	}
	        	
	        
	        	for(var i=0;i<factory_product.length;i++){
	        		power_factory_product.push(factory_product[i])
	        		
	        	}
	        	center_power_chart_option.xAxis[0].data=get_date_name(data['power_cons_trend']);
	        	center_upw_chart_option.xAxis[0].data=get_date_name(data['upw_cons_trend']);
	        	center_cda_chart_option.xAxis[0].data=get_date_name(data['cda_cons_trend']);
	        	center_pn2_chart_option.xAxis[0].data=get_date_name(data['pn2_cons_trend']);
	        	
	        	center_power_chart_option.series[0].data=get_date_value(data['power_cons_trend']);
	        	center_upw_chart_option.series[0].data=get_date_value(data['upw_cons_trend']);
	        	center_cda_chart_option.series[0].data=get_date_value(data['cda_cons_trend']);
	        	center_pn2_chart_option.series[0].data=get_date_value(data['pn2_cons_trend']);
	        	
	        	center_power_chart_option.series[2].data=power_factory_unit_cons;
	        	center_upw_chart_option.series[2].data=upw_factory_unit_cons;
	        	center_cda_chart_option.series[2].data=pn2_factory_unit_cons;
	        	center_pn2_chart_option.series[2].data=cda_factory_unit_cons;
	        	
	        	center_power_chart_option.series[1].data=power_factory_product;
	        	center_upw_chart_option.series[1].data=power_factory_product;
	        	center_cda_chart_option.series[1].data=power_factory_product;
	        	center_pn2_chart_option.series[1].data=power_factory_product;
	        	
	        	center_power_chart.setOption(center_power_chart_option,true);
	        	center_upw_chart.setOption(center_upw_chart_option,true);
	        	center_cda_chart.setOption(center_cda_chart_option,true);
	        	center_pn2_chart.setOption(center_pn2_chart_option,true);
	        },
	});
}
function get_total_data_value(data,param){
	var total_data=new Array();
	for(var i=0;i<data.length;i++) {
		if(data[i].param==param){
			total_data.push((data[i].val).toFixed(2));
		}
	}
	return total_data;
}

function get_total_data_name(data,param){
	var total_data=new Array();
	for(var i=0;i<data.length;i++) {
		if(data[i].param==param){
			total_data.push(data[i].day);
		}
	}
	return total_data;
}

function get_date_name(data){
	var date_name= new Array();
	for(var i=0;i<data.length;i++) {
		date_name.push(data[i].name);
	}
	return date_name;
	
}
function get_date_value(data){
	var date_value= new Array();
	for(var i=0;i<data.length;i++) {
		date_value.push(parseInt(data[i].value).toFixed(0));
	}
	return date_value;
}

function node_click(department,energy,div){
	
	var data=new Array();
	if(energy=="Power") {
		data=power_compare_data;
	}
	if(energy=="UPW") {
		data=upw_compare_data;
	}
	if(energy=="CDA") {
		data=pn2_compare_data;
	}
	if(energy=="PN2") {
		data=cda_compare_data;
	}
	data.sort(function(a,b){ 
		return b.value - a.value;
	})
	
	var str='&nbsp;<br>';
	for(var i=0;i<data.length;i++) {
		if(data[i].department==department){
			if(data[i].value>0){
				str=str+'<button class="myButton" onclick=node_click("'+data[i].department+'","'+data[i].energy+'","'+data[i].name+'" class="button_up")>'+data[i].name+'</button>';
			}else if(data[i].value==-10000){
				str=str+'<button class="myButton" onclick=node_click("'+data[i].department+'","'+data[i].energy+'","'+data[i].name+'" class="button_down")>'+data[i].name+'</button>';
			}else {
				str=str+'<button class="myButton" onclick=node_click("'+data[i].department+'","'+data[i].energy+'","'+data[i].name+'" class="button_none")>'+data[i].name+'</button>';
			}
		}	
	}
	document.getElementById("product_line_dlg_div").innerHTML = str; 
	document.getElementById("product_line_dlg_header_div").innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;'+department+'<button id="shortcut_dlg_close_button" class="modal-header-btn right modal-close" title="Close Modal">X</button>';
	
	var shortcut_dlg = create_modal_dlg("#shortcut_dlg_div", "#shortcut_dlg_close_button");
	shortcut_dlg.init();
	shortcut_dlg.open();
}

function back_click(energy,div) {
	var data=new Array();
	if(energy=="Power") {
		data=power_compare_data;
	}
	if(energy=="UPW") {
		data=upw_compare_data;
	}
	if(energy=="CDA") {
		data=pn2_compare_data;
	}
	if(energy=="PN2") {
		data=cda_compare_data;
	}
	
	var str="&nbsp;<br>";
	for(var i=0;i<data.length;i++) {
		if(data[i].value<=0){
			break;
		}
		str=str+'&nbsp;&nbsp;&nbsp;&nbsp;<button style="  width: 130px;background: #33cc94; border-radius: 8px;color: #fff;border: 2px solid #ddd;"    onclick=node_click("'+data[i].department+'","'+data[i].energy+'","'+div+'")><u style="text-decoration: none;">'+data[i].department+' '+data[i].name+'</u></button><span style="font-family: Microsoft Yahei;font-size: 13px;color: #e64343"> :与上月平均单耗相比: '+(data[i].value*100).toFixed(2)+'%</span><img style="    margin-top: -5px;" src="'+path+'/res/images/goup.png"><br><br>';
	}
	document.getElementById(div).innerHTML =str;
}


function product_line_unit_cons_compare(data,lastdata,div){

	for(var i=0;i<data.length;i++) {
		
		for(var j=0;j<lastdata.length;j++) {
			if(data[i].name==lastdata[j].name) {
				if(lastdata[j].value==0) {
					data[i].value=-10000;
				}else {
					data[i].value=(data[i].value-lastdata[j].value)/lastdata[j].value;
					break;
				}
				
			}
		}
	}
	data.sort(function(a,b){ 
		return b.value - a.value;
	})
	if(data[0].energy=="Power") {
		
		power_compare_data=data;
	}
	if(data[0].energy=="UPW") {
		upw_compare_data=data;
	}
	if(data[0].energy=="CDA") {
		pn2_compare_data=data;
	}
	if(data[0].energy=="PN2") {
		cda_compare_data=data;
	}
	var str="&nbsp;<br>";
	for(var i=0;i<data.length;i++) {
		if(data[i].value>0){
			str=str+'&nbsp;&nbsp;&nbsp;&nbsp;<button style="  width: 130px;background: #33cc94; border-radius: 8px;color: #fff;border: 2px solid #ddd;"  onclick=node_click("'+data[i].department+'","'+data[i].energy+'","'+div+'")><u style="text-decoration: none;">'+data[i].department+' '+data[i].name+'</u></button> <span style="font-family: Microsoft Yahei;font-size: 13px;color: #e64343">:与上月平均单耗相比: '+(data[i].value*100).toFixed(2)+'%</span><img style="    margin-top: -5px;" src="'+path+'/res/images/goup.png"><br><br>';
		}
	}
	document.getElementById(div).innerHTML =str;
}