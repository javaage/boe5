var day='2018-12-27'

$(function(){
	var windowHeight = $(window).height();
   	document.getElementById('gridContainer').style.height = windowHeight-8+'px';
	var windowWidth = $(window).width();
   	document.getElementById('gridContainer').style.width = windowWidth+'px';
	window.onresize = function() {
		var windowHeight = $(window).height();
		var windowWidth = $(window).width();
	   	document.getElementById('gridContainer').style.height = windowHeight-8+'px';
	   	document.getElementById('gridContainer').style.width = windowWidth+'px';
	}
	layui.use('laydate', function(){
		var laydate = layui.laydate;
		laydate.render({
			elem: '#day',
			type: 'date',
			value:day,
			done: function(value) {
				day=value;
				ajaxForErrorCheck();
			}
		});
	});
	ajaxForErrorCheck();
});

function ajaxForErrorCheck(){
	var errorchecks;
	$.ajax({
        url: path + '/gynh/errorcheck/get_data.do',
        type: 'get',
        data:{day:day},
        async: false,
        success: function (data) {
            errorchecks = data['list'];
        }
    });
	 $("#gridContainer").dxDataGrid({
	        dataSource: errorchecks,
	        showBorders: true,
	        showColumnLines: true,
	        allowColumnReordering: true,
	        allowColumnResizing: true,
	        showRowLines: true,
	        rowAlternationEnabled: true,
	        columnResizingMode: "widget",
	        columnMinWidth: 20,
	        columnAutoWidth: true,
	        filterRow: { visible: true },
	        headerFilter: { visible: true },
	        "export": {
	            enabled: true,
	            fileName: "Orders"
	        },
	        paging: {
	            enabled: false
	        },
	        scrolling: {
	            mode: "virtual"
	        },
	        columnFixing: { 
	            enabled: true
	        },
	        searchPanel: {
	            visible: true,
	            width: 240,
	            placeholder: "搜索..."
	        },
	        columnChooser: {
	            enabled: true,
	            mode: "select"
	        },
	        columns: [
	        	{
	                dataField: "point",
	                validationRules: [{ type: "required" }, {
	                    type: "pattern",
	                }],
	                caption: "point"
	            },
	            {
	                dataField: "begin_val",
	                validationRules: [{ type: "required" }, {
	                    type: "pattern",
	                }],
	                caption: "begin_val"
	            },
	            {
	                dataField: "end_time",
	                validationRules: [{ type: "required" }, {
	                    type: "pattern",
	                }],
	                caption: "end_time"
	            },
	            {
	                dataField: "end_val",
	                validationRules: [{ type: "required" }, {
	                    type: "pattern",
	                }],
	                caption: "end_val"
	            },
	            {
	                dataField: "min_time",
	                validationRules: [{ type: "required" }, {
	                    type: "pattern",
	                }],
	                caption: "min_time"
	            },
	            {
	                dataField: "min_val",
	                validationRules: [{ type: "required" }, {
	                    type: "pattern",
	                }],
	                caption: "min_val"
	            },
	            {
	                dataField: "max_time",
	                validationRules: [{ type: "required" }, {
	                    type: "pattern",
	                }],
	                caption: "max_time"
	            },
	            {
	                dataField: "max_val",
	                validationRules: [{ type: "required" }, {
	                    type: "pattern",
	                }],
	                caption: "max_val"
	            },
	            {
	                dataField: "begin_0_time",
	                validationRules: [{ type: "required" }, {
	                    type: "pattern",
	                }],
	                caption: "begin_0_time"
	            },
	            {
	                dataField: "end_0_time",
	                validationRules: [{ type: "required" }, {
	                    type: "pattern",
	                }],
	                caption: "end_0_time"
	            },{
	                dataField: "energy",
	                validationRules: [{ type: "required" }, {
	                    type: "pattern",
	                }],
	                caption: "energy"
	            },
	            {
	                dataField: "factory",
	                validationRules: [{ type: "required" }, {
	                    type: "pattern",
	                }],
	                caption: "factory"
	            }, {
	                dataField: "department",
	                validationRules: [{ type: "required" }, {
	                    type: "pattern",
	                }],
	                caption: "department"
	            },{
	                dataField: "eqp_id",
	                validationRules: [{ type: "required" }, {
	                    type: "pattern",
	                }],
	                caption: "eqp_id"
	            },{
	                dataField: "device",
	                validationRules: [{ type: "required" }, {
	                    type: "pattern",
	                }],
	                caption: "device"
	            },{
	                dataField: "device_unit",
	                validationRules: [{ type: "required" }, {
	                    type: "pattern",
	                }],
	                caption: "device_unit"
	            },{
	                dataField: "point_name",
	                validationRules: [{ type: "required" }, {
	                    type: "pattern",
	                }],
	                caption: "point_name"
	            },
	        ]
	    });    
}

