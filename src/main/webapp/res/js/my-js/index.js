$(function(){
	var tab = null;

	var menus;
	$.ajax({
		url: path + '/menu/getMenus.do',
		type: 'post',
		async: false,
        success: function (data) {
            menus = data;
        }
    });
	var tree = $("#simple-treeview").dxTreeView({
        items: menus,
        dataStructure: "plain",
        parentIdExpr: "categoryId",
        keyExpr: "id",
        displayExpr: "name",
        width: 300,
		onItemClick: function(e) {
			if (tab == null) {
				$ = layui.jquery,
				tab = layui.tab({
					elem: '.layout-nav-card',
					contextMenu:true
				});
			}
			var item = e.itemData;
			if (item.url) {
				var my_path = item.url.substr(0, 4).toLowerCase() == 'http' ? "" : path;
				var field = {
					href: my_path+item.url,
					//icon: icon,
					title: (e.node.parent != null) ? e.node.parent.itemData.name + '-' + item.name : item.name
				};
				if (e.event.ctrlKey) {
					window.open(my_path+item.url);
				} else {
					tab.tabAdd(field);
				}
				
			} else {
				if (item.expanded) {
					tree.collapseItem(item);
				} else {
					tree.expandItem(item);
				}
			}
        }
    }).dxTreeView("instance");
});