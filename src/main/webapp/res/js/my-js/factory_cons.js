
var department_box;
var line_box;
var device_box;
var device_unit_box;
var date_show = false;

var product_line_data="";

var department_list = '';
var product_line_list = ''; 
var product_line_lists = '';
var product_line_array = [];
var device_list = '';
var device_lists = '';
var device_unit_list = '';

var product_line_listss="";
var device_listss="";
var device_unit_listss="";
var department_cons=new Array();
var product_line_cons=new Array();
var product_line_output=new Array();
var product_line_unit_cons=new Array();
var product_line_state_time=new Array();

var product_line_cons_hourly=new Array();
var product_line_output_hourly=new Array();
var product_line_unit_cons_hourly=new Array();
var product_line_state_time_hourly=new Array();

var device_cons=new Array();
var device_output=new Array();
var device_unit_conss=new Array();
var device_state_time=new Array();

var device_cons_hourly=new Array();
var device_output_hourly=new Array();
var device_unit_conss_hourly=new Array();
var device_state_time_hourly=new Array();

var device_unit_cons=new Array();
var device_unit_output=new Array();
var device_unit_unit_cons=new Array();
var device_unit_state_time=new Array();

var device_unit_cons_hourly=new Array();
var device_unit_output_hourly=new Array();
var device_unit_unit_cons_hourly=new Array();
var device_unit_state_time_hourly=new Array();

var energy="Power";
var snm=null;

var factory_point_data=new Array();
var factory_inst_point_data=new Array();

var department_chart_series=new Array();
var product_line_chart_series=new Array();
var device_chart_series=new Array();
var device_unit_chart_series=new Array();

var product_line_hourly_chart_series=new Array();
var device_hourly_chart_series=new Array();
var device_unit_hourly_chart_series=new Array();

var department_chart_date=new Array();
var product_line_chart_date=new Array();
var device_chart_date=new Array();
var device_unit_chart_date=new Array();

var product_line_hourly_chart_date=new Array();
var device_hourly_chart_date=new Array();
var device_unit_hourly_chart_date=new Array();

var product_line_first=true;
var product_line_name_list;
var product_line_node_list;

var product_line_hourly_first=true;
var product_line_hourly_name_list;
var product_line_hourly_node_list;

var device_first=true;
var device_name_list;
var device_node_list;

var device_hourly_first=true;
var device_hourly_name_list;
var device_hourly_node_list;

var device_unit_first=true;
var device_unit_name_list;
var device_unit_node_list;

var device_unit_hourly_first=true;
var device_unit_hourly_name_list;
var device_unit_hourly_node_list;

var cons_color=power_color;
product_color='#0066ccc4';
var state_time_color='#c36c228f';
var button_id=21;




var product_line_click_events= 
{
   click: function(e) {
	   	open_day(1);  
	   	get_product_line_cons_hourly(e.point.category);
	   	get_product_line_output_hourly(e.point.category);
	   	get_product_line_unit_cons_hourly(e.point.category);
	   	set_product_line_chart_hourly();
	}
};

var device_click_events= 
{
   click: function(e) {
	   	open_day(2);  
	   	get_device_cons_hourly(e.point.category);
	   	get_product_line_output_hourly(e.point.category);
	   	set_device_chart_hourly();
	}
};

var device_unit_click_events= 
{
   click: function(e) {
	   	open_day(3);  
	   	get_device_unit_cons_hourly(e.point.category);
	   	get_product_line_output_hourly(e.point.category);
	   	set_device_unit_chart_hourly();
	}
};

function set_energy(data) {
	energy=data;
	if(energy=='Power'){
		cons_color=power_color;
	}else if(energy=='CDA'){
		cons_color=cda_color;
	}else if(energy=='PN2'){
		cons_color=pn2_color;
	}else if(energy=='UPW'){
		cons_color=upw_color;
	}
	
}
function one_month() {
	start_date = getFormatDate(-30);
	end_date = getFormatDate(-1);
}
function one_week() {
	start_date = getFormatDate(-7);
	end_date = getFormatDate(-1);
}
function yesterday() {
	start_date = getFormatDate(-1);
	end_date = getFormatDate(-1);
}

function custom() {
	if(date_show) {
		document.getElementById("date").style.display = 'none';
		date_show = false;
	}else {
		document.getElementById("date").style.display = 'inline';
		date_show = true;
	}
}

function set_product_line_select(data,id){
	var count=0;
	var point_place=-0.4;
	var pointPlacement=point_place;
	var point_width=0.2;
	var pointPadding1=0.4;
	var pointPadding2=0.46;
	var pointPadding3=0.48;
	if(product_line_first){
		product_line_first=false;
		var str_data=product_line_data.split("--");
		str_data=str_data.sort();
		product_line_name_list= new Array();
		product_line_name_list.push({name:'能耗',show:true,});
		product_line_name_list.push({name:'产量',show:true,});
		product_line_name_list.push({name:'单耗',show:true,});
		product_line_name_list.push({name:'状态',show:true,});
		product_line_node_list= new Array();
		for (var i = 1; i < str_data.length; i++) {
			product_line_node_list.push({name:str_data[i],show:true,});
		}
	}
	for (var i = 0; i < product_line_name_list.length; i++) {
		if(product_line_name_list[i].name==data) {
			if(product_line_name_list[i].show) {
				product_line_name_list[i].show=false;
				$("#button"+id).css("background-color","#9aa09d");
			}else {
				product_line_name_list[i].show=true;
				$("#button"+id).css("background-color","#50e090");
			}
		}
	}
	for (var i = 0; i < product_line_node_list.length; i++) {
		if(product_line_node_list[i].name==data) {
			if(product_line_node_list[i].show) {
				product_line_node_list[i].show=false;
				$("#button"+id).css("background-color","#9aa09d");
			}else {
				product_line_node_list[i].show=true;
				$("#button"+id).css("background-color","#50e090");
			}
		}
	}
	for (var i = 0; i < product_line_node_list.length; i++) {
		if(product_line_node_list[i].show) {
			count++;
		}
	}
	product_line_option.series.splice(0, product_line_option.series.length);
	for (var i = 0; i < product_line_chart_series.length; i++) {
		for (var j = 0; j < product_line_name_list.length; j++) {
			for (var h = 0; h < product_line_node_list.length; h++) {
				if(product_line_chart_series[i].name.indexOf(product_line_name_list[j].name)!=-1&&product_line_chart_series[i].name.indexOf(product_line_node_list[h].name)!=-1&&product_line_name_list[j].show&&product_line_node_list[h].show) {
					product_line_option.series.push(product_line_chart_series[i]);
				}
			}
		}
	}
	if(count==1) {
		point_place=0;
		pointPlacement=0;
		pointPadding1=0.1;
		pointPadding2=0.24;
		pointPadding3=0.4;
	}else if(count==2) { 
		point_place=-0.2;
		pointPlacement=-0.2;
		point_width=0.4;
		pointPadding1=0.3;
		pointPadding2=0.36;
		pointPadding3=0.42;
	}else if(count==3) { 
		point_place=-0.3;
		pointPlacement=-0.3;
		point_width=0.3;
		pointPadding1=0.3;
		pointPadding2=0.36;
		pointPadding3=0.42;
	}else if(count==4) { 
		point_place=-0.3;
		pointPlacement=point_place;
		point_width=0.2;
		pointPadding1=0.4;
		pointPadding2=0.46;
		pointPadding3=0.48;
	}
	var j=0;
	for (var i = 0; i < product_line_option.series.length; i++) {
		j++;
		if(product_line_option.series[i].name.indexOf('能耗')!=-1){
			product_line_option.series[i].pointPadding = pointPadding1;
			product_line_option.series[i].events = product_line_click_events;
			
		}
		if(product_line_option.series[i].name.indexOf('产量')!=-1){
			product_line_option.series[i].pointPadding = pointPadding2;
			product_line_option.series[i].events = product_line_click_events;
		}
		if(product_line_option.series[i].name.indexOf('状态')!=-1){
			product_line_option.series[i].pointPadding = pointPadding3;
			product_line_option.series[i].events = product_line_click_events;
		}
		if(product_line_option.series[i].name.indexOf('单耗')!=-1){
			break;
		}
		
		product_line_option.series[i].pointPlacement=pointPlacement;
		pointPlacement=pointPlacement+point_width;
		if(j==count) {
			pointPlacement=point_place;
			j=0;
		}
	}
	product_line_chart = Highcharts.chart('product_line_chart', product_line_option);
	//product_line_chart_series
	
}
function set_product_line_hourly_select(data,id){
	
	var count=0;
	var point_place=-0.4;
	var pointPlacement=point_place;
	var point_width=0.2;
	var pointPadding1=0.4;
	var pointPadding2=0.46;
	var pointPadding3=0.48;
	if(product_line_hourly_first){
		product_line_hourly_first=false;
		var str_data=product_line_data.split("--");
		str_data=str_data.sort();
		product_line_hourly_name_list= new Array();
		product_line_hourly_name_list.push({name:'能耗',show:true,});
		product_line_hourly_name_list.push({name:'产量',show:true,});
		product_line_hourly_name_list.push({name:'单耗',show:true,});
		product_line_hourly_node_list= new Array();
		for (var i = 1; i < str_data.length; i++) {
			product_line_hourly_node_list.push({name:str_data[i],show:true,});
		}
		
	}
	for (var i = 0; i < product_line_hourly_name_list.length; i++) {
		if(product_line_hourly_name_list[i].name==data) {
			if(product_line_hourly_name_list[i].show) {
				product_line_hourly_name_list[i].show=false;
				$("#button"+id).css("background-color","#9aa09d");
			}else {
				product_line_hourly_name_list[i].show=true;
				$("#button"+id).css("background-color","#50e090");
			}
		}
	}
	for (var i = 0; i < product_line_hourly_node_list.length; i++) {
		if(product_line_hourly_node_list[i].name==data) {
			if(product_line_hourly_node_list[i].show) {
				product_line_hourly_node_list[i].show=false;
				$("#button"+id).css("background-color","#9aa09d");
			}else {
				product_line_hourly_node_list[i].show=true;
				$("#button"+id).css("background-color","#50e090");
			}
		}
	}
	for (var i = 0; i < product_line_hourly_node_list.length; i++) {
		if(product_line_hourly_node_list[i].show) {
			count++;
		}
	}
	product_line_option_hourly.series.splice(0, product_line_option_hourly.series.length);
	for (var i = 0; i < product_line_hourly_chart_series.length; i++) {
		for (var j = 0; j < product_line_hourly_name_list.length; j++) {
			for (var h = 0; h < product_line_hourly_node_list.length; h++) {
				if(product_line_hourly_chart_series[i].name.indexOf(product_line_hourly_name_list[j].name)!=-1&&product_line_hourly_chart_series[i].name.indexOf(product_line_hourly_node_list[h].name)!=-1&&product_line_hourly_name_list[j].show&&product_line_hourly_node_list[h].show) {
					product_line_option_hourly.series.push(product_line_hourly_chart_series[i]);
				}
			}
		}
	}
	if(count==1) {
		point_place=0;
		pointPlacement=0;
		pointPadding1=0.1;
		pointPadding2=0.24;
		pointPadding3=0.4;
	}else if(count==2) { 
		point_place=-0.2;
		pointPlacement=-0.2;
		point_width=0.4;
		pointPadding1=0.3;
		pointPadding2=0.36;
		pointPadding3=0.42;
	}else if(count==3) { 
		point_place=-0.3;
		pointPlacement=-0.3;
		point_width=0.3;
		pointPadding1=0.3;
		pointPadding2=0.36;
		pointPadding3=0.42;
	}else if(count==4) { 
		point_place=-0.3;
		pointPlacement=point_place;
		point_width=0.2;
		pointPadding1=0.4;
		pointPadding2=0.46;
		pointPadding3=0.48;
	}
	var j=0;
	for (var i = 0; i < product_line_option_hourly.series.length; i++) {
		j++;
		if(product_line_option_hourly.series[i].name.indexOf('能耗')!=-1){
			product_line_option_hourly.series[i].pointPadding=pointPadding1;
		}
		if(product_line_option_hourly.series[i].name.indexOf('产量')!=-1){
			product_line_option_hourly.series[i].pointPadding=pointPadding2;
		}
		if(product_line_option_hourly.series[i].name.indexOf('单耗')!=-1){
			break;
		}
		
		product_line_option_hourly.series[i].pointPlacement=pointPlacement;
		pointPlacement=pointPlacement+point_width;
		if(j==count) {
			pointPlacement=point_place;
			j=0;
		}
	}
	product_line_chart_hourly = Highcharts.chart('product_line_chart_hourly', product_line_option_hourly);
	
	
}
function set_device_select(data,id){
	
	var count=0;
	var point_place=-0.4;
	var pointPlacement=point_place;
	var point_width=0.2;
	var pointPadding1=0.4;
	var pointPadding2=0.46;
	var pointPadding3=0.48;
	if(device_first){
		device_first=false;
		var str_data=new Set();
		for (var i = 0; i < device_output.length; i++) {
			str_data.add(device_output[i].snm);
		}
		device_name_list= new Array();
		device_name_list.push({name:'能耗',show:true,});
		device_name_list.push({name:'产量',show:true,});
		device_name_list.push({name:'单耗',show:true,});
		device_name_list.push({name:'状态',show:true,});
		device_node_list= new Array();
		for (let iterable_element of str_data) {
			device_node_list.push({name:iterable_element,show:true,});
		}		
	}
	for (var i = 0; i < device_name_list.length; i++) {
		if(device_name_list[i].name==data) {
			if(device_name_list[i].show) {
				device_name_list[i].show=false;
				$("#button"+id).css("background-color","#9aa09d");
			}else {
				device_name_list[i].show=true;
				$("#button"+id).css("background-color","#50e090");
			}
		}
	}
	for (var i = 0; i < device_node_list.length; i++) {
		if(device_node_list[i].name==data) {
			if(device_node_list[i].show) {
				device_node_list[i].show=false;
				$("#button"+id).css("background-color","#9aa09d");
			}else {
				device_node_list[i].show=true;
				$("#button"+id).css("background-color","#50e090");
			}
		}
	}
	for (var i = 0; i < device_node_list.length; i++) {
		if(device_node_list[i].show) {
			count++;
		}
	}
	device_option.series.splice(0, device_option.series.length);
	for (var i = 0; i < device_chart_series.length; i++) {
		for (var j = 0; j < device_name_list.length; j++) {
			for (var h = 0; h < device_node_list.length; h++) {
				if(device_chart_series[i].name.indexOf(device_name_list[j].name)!=-1&&device_chart_series[i].name.indexOf(device_node_list[h].name)!=-1&&device_name_list[j].show&&device_node_list[h].show) {
					device_option.series.push(device_chart_series[i]);
				}
			}
		}
	}
	if(count==1) {
		point_place=0;
		pointPlacement=0;
		pointPadding1=0.1;
		pointPadding2=0.24;
		pointPadding3=0.4;
	}else if(count==2) { 
		point_place=-0.2;
		pointPlacement=-0.2;
		point_width=0.4;
		pointPadding1=0.3;
		pointPadding2=0.36;
		pointPadding3=0.42;
	}else if(count==3) { 
		point_place=-0.3;
		pointPlacement=-0.3;
		point_width=0.3;
		pointPadding1=0.3;
		pointPadding2=0.36;
		pointPadding3=0.42;
	}else if(count==4) { 
		point_place=-0.3;
		pointPlacement=point_place;
		point_width=0.2;
		pointPadding1=0.4;
		pointPadding2=0.46;
		pointPadding3=0.48;
	}
	var j=0;
	for (var i = 0; i < device_option.series.length; i++) {
		j++;
		if(device_option.series[i].name.indexOf('能耗')!=-1){
			device_option.series[i].pointPadding=pointPadding1;
			device_option.series[i].events = device_click_events;
		}
		if(device_option.series[i].name.indexOf('产量')!=-1){
			device_option.series[i].pointPadding=pointPadding2;
			device_option.series[i].events = device_click_events;
		}
		if(device_option.series[i].name.indexOf('状态')!=-1){
			device_option.series[i].pointPadding=pointPadding3;
			device_option.series[i].events = device_click_events;
		}
		if(device_option.series[i].name.indexOf('单耗')!=-1){
			break;
		}
		
		device_option.series[i].pointPlacement=pointPlacement;
		pointPlacement=pointPlacement+point_width;
		if(j==count) {
			pointPlacement=point_place;
			j=0;
		}
	}
	device_chart = Highcharts.chart('device_chart', device_option);
	//device_chart_series
	
}
function set_device_hourly_select(data,id){
	var count=0;
	var point_place=-0.4;
	var pointPlacement=point_place;
	var point_width=0.2;
	var pointPadding1=0.4;
	var pointPadding2=0.46;
	var pointPadding3=0.48;
	if(device_hourly_first){
		device_hourly_first=false;
		var str_data=new Set();
		for (var i = 0; i < device_output_hourly.length; i++) {
			str_data.add(device_output_hourly[i].snm);
		}
		device_hourly_name_list= new Array();
		device_hourly_name_list.push({name:'能耗',show:true,});
		device_hourly_name_list.push({name:'产量',show:true,});
		device_hourly_name_list.push({name:'单耗',show:true,});
		device_hourly_node_list= new Array();
		for (let iterable_element of str_data) {
			device_hourly_node_list.push({name:iterable_element,show:true,});
		}	
	}
	for (var i = 0; i < device_hourly_name_list.length; i++) {
		if(device_hourly_name_list[i].name==data) {
			if(device_hourly_name_list[i].show) {
				device_hourly_name_list[i].show=false;
				$("#button"+id).css("background-color","#9aa09d");
			}else {
				device_hourly_name_list[i].show=true;
				$("#button"+id).css("background-color","#50e090");
			}
		}
	}
	for (var i = 0; i < device_hourly_node_list.length; i++) {
		if(device_hourly_node_list[i].name==data) {
			if(device_hourly_node_list[i].show) {
				device_hourly_node_list[i].show=false;
				$("#button"+id).css("background-color","#9aa09d");
			}else {
				device_hourly_node_list[i].show=true;
				$("#button"+id).css("background-color","#50e090");
			}
		}
	}
	for (var i = 0; i < device_hourly_node_list.length; i++) {
		if(device_hourly_node_list[i].show) {
			count++;
		}
	}
	device_option_hourly.series.splice(0, device_option_hourly.series.length);
	for (var i = 0; i < device_hourly_chart_series.length; i++) {
		for (var j = 0; j < device_hourly_name_list.length; j++) {
			for (var h = 0; h < device_hourly_node_list.length; h++) {
				if(device_hourly_chart_series[i].name.indexOf(device_hourly_name_list[j].name)!=-1&&device_hourly_chart_series[i].name.indexOf(device_hourly_node_list[h].name)!=-1&&device_hourly_name_list[j].show&&device_hourly_node_list[h].show) {
					device_option_hourly.series.push(device_hourly_chart_series[i]);
				}
			}
		}
	}
	if(count==1) {
		point_place=0;
		pointPlacement=0;
		pointPadding1=0.1;
		pointPadding2=0.24;
		pointPadding3=0.4;
	}else if(count==2) { 
		point_place=-0.2;
		pointPlacement=-0.2;
		point_width=0.4;
		pointPadding1=0.3;
		pointPadding2=0.36;
		pointPadding3=0.42;
	}else if(count==3) { 
		point_place=-0.3;
		pointPlacement=-0.3;
		point_width=0.3;
		pointPadding1=0.3;
		pointPadding2=0.36;
		pointPadding3=0.42;
	}else if(count==4) { 
		point_place=-0.3;
		pointPlacement=point_place;
		point_width=0.2;
		pointPadding1=0.4;
		pointPadding2=0.46;
		pointPadding3=0.48;
	}
	var j=0;
	for (var i = 0; i < device_option_hourly.series.length; i++) {
		j++;
		if(device_option_hourly.series[i].name.indexOf('能耗')!=-1){
			device_option_hourly.series[i].pointPadding=pointPadding1;
		}
		if(device_option_hourly.series[i].name.indexOf('产量')!=-1){
			device_option_hourly.series[i].pointPadding=pointPadding2;
		}
		if(device_option_hourly.series[i].name.indexOf('单耗')!=-1){
			break;
		}
		
		device_option_hourly.series[i].pointPlacement=pointPlacement;
		pointPlacement=pointPlacement+point_width;
		if(j==count) {
			pointPlacement=point_place;
			j=0;
		}
	}
	device_chart_hourly = Highcharts.chart('device_chart_hourly', device_option_hourly);
	
	
}
function set_device_unit_select(data,id){
	var count=0;
	var point_place=-0.4;
	var pointPlacement=point_place;
	var point_width=0.2;
	var pointPadding1=0.4;
	var pointPadding2=0.46;
	var pointPadding3=0.48;
	if(device_unit_first){
		device_unit_first=false;
		var str_data=new Set();
		for (var i = 0; i < device_unit_output.length; i++) {
			str_data.add(device_unit_output[i].snm);
		}
		device_unit_name_list= new Array();
		device_unit_name_list.push({name:'能耗',show:true,});
		device_unit_name_list.push({name:'产量',show:true,});
		device_unit_name_list.push({name:'单耗',show:true,});
		device_unit_name_list.push({name:'状态',show:true,});
		device_unit_node_list= new Array();
		for (let iterable_element of str_data) {
			device_unit_node_list.push({name:iterable_element,show:true,});
		}
		
	}
	for (var i = 0; i < device_unit_name_list.length; i++) {
		if(device_unit_name_list[i].name==data) {
			if(device_unit_name_list[i].show) {
				device_unit_name_list[i].show=false;
				$("#button"+id).css("background-color","#9aa09d");
			}else {
				device_unit_name_list[i].show=true;
				$("#button"+id).css("background-color","#50e090");
			}
		}
	}
	for (var i = 0; i < device_unit_node_list.length; i++) {
		if(device_unit_node_list[i].name==data) {
			if(device_unit_node_list[i].show) {
				device_unit_node_list[i].show=false;
				$("#button"+id).css("background-color","#9aa09d");
			}else {
				device_unit_node_list[i].show=true;
				$("#button"+id).css("background-color","#50e090");
			}
		}
	}
	for (var i = 0; i < device_unit_node_list.length; i++) {
		if(device_unit_node_list[i].show) {
			count++;
		}
	}
	device_unit_option.series.splice(0, device_unit_option.series.length);
	for (var i = 0; i < device_unit_chart_series.length; i++) {
		for (var j = 0; j < device_unit_name_list.length; j++) {
			for (var h = 0; h < device_unit_node_list.length; h++) {
				if(device_unit_chart_series[i].name.indexOf(device_unit_name_list[j].name)!=-1&&device_unit_chart_series[i].name.indexOf(device_unit_node_list[h].name)!=-1&&device_unit_name_list[j].show&&device_unit_node_list[h].show) {
					device_unit_option.series.push(device_unit_chart_series[i]);
				}
			}
		}
	}
	if(count==1) {
		point_place=0;
		pointPlacement=0;
		pointPadding1=0.1;
		pointPadding2=0.24;
		pointPadding3=0.4;
	}else if(count==2) { 
		point_place=-0.2;
		pointPlacement=-0.2;
		point_width=0.4;
		pointPadding1=0.3;
		pointPadding2=0.36;
		pointPadding3=0.42;
	}else if(count==3) { 
		point_place=-0.3;
		pointPlacement=-0.3;
		point_width=0.3;
		pointPadding1=0.3;
		pointPadding2=0.36;
		pointPadding3=0.42;
	}else if(count==4) { 
		point_place=-0.3;
		pointPlacement=point_place;
		point_width=0.2;
		pointPadding1=0.4;
		pointPadding2=0.46;
		pointPadding3=0.48;
	}
	var j=0;
	for (var i = 0; i < device_unit_option.series.length; i++) {
		j++;
		if(device_unit_option.series[i].name.indexOf('能耗')!=-1){
			device_unit_option.series[i].pointPadding=pointPadding1;
			device_unit_option.series[i].events = device_unit_click_events;
		}
		if(device_unit_option.series[i].name.indexOf('产量')!=-1){
			device_unit_option.series[i].pointPadding=pointPadding2;
			device_unit_option.series[i].events = device_unit_click_events;
		}
		if(device_unit_option.series[i].name.indexOf('状态')!=-1){
			device_unit_option.series[i].pointPadding=pointPadding3;
			device_unit_option.series[i].events = device_unit_click_events;
		}
		if(device_unit_option.series[i].name.indexOf('单耗')!=-1){
			break;
		}
		
		device_unit_option.series[i].pointPlacement=pointPlacement;
		pointPlacement=pointPlacement+point_width;
		if(j==count) {
			pointPlacement=point_place;
			j=0;
		}
	}
	device_unit_chart = Highcharts.chart('device_unit_chart', device_unit_option);
	//device_unit_chart_series
	
}
function set_device_unit_hourly_select(data,id){
	var count=0;
	var point_place=-0.4;
	var pointPlacement=point_place;
	var point_width=0.2;
	var pointPadding1=0.4;
	var pointPadding2=0.46;
	var pointPadding3=0.48;
	if(device_unit_hourly_first){
		device_unit_hourly_first=false;
		var str_data=new Set();
		for (var i = 0; i < device_unit_output_hourly.length; i++) {
			str_data.add(device_unit_output_hourly[i].snm);
		}
		device_unit_hourly_name_list= new Array();
		device_unit_hourly_name_list.push({name:'能耗',show:true,});
		device_unit_hourly_name_list.push({name:'产量',show:true,});
		device_unit_hourly_name_list.push({name:'单耗',show:true,});
		device_unit_hourly_node_list= new Array();
		for (let iterable_element of str_data) {
			device_unit_hourly_node_list.push({name:iterable_element,show:true,});
		}
	}
	for (var i = 0; i < device_unit_hourly_name_list.length; i++) {
		if(device_unit_hourly_name_list[i].name==data) {
			if(device_unit_hourly_name_list[i].show) {
				device_unit_hourly_name_list[i].show=false;
				$("#button"+id).css("background-color","#9aa09d");
			}else {
				device_unit_hourly_name_list[i].show=true;
				$("#button"+id).css("background-color","#50e090");
			}
		}
	}
	for (var i = 0; i < device_unit_hourly_node_list.length; i++) {
		if(device_unit_hourly_node_list[i].name==data) {
			if(device_unit_hourly_node_list[i].show) {
				device_unit_hourly_node_list[i].show=false;
				$("#button"+id).css("background-color","#9aa09d");
			}else {
				device_unit_hourly_node_list[i].show=true;
				$("#button"+id).css("background-color","#50e090");
			}
		}
	}
	for (var i = 0; i < device_unit_hourly_node_list.length; i++) {
		if(device_unit_hourly_node_list[i].show) {
			count++;
		}
	}
	device_unit_option_hourly.series.splice(0, device_unit_option_hourly.series.length);
	for (var i = 0; i < device_unit_hourly_chart_series.length; i++) {
		for (var j = 0; j < device_unit_hourly_name_list.length; j++) {
			for (var h = 0; h < device_unit_hourly_node_list.length; h++) {
				if(device_unit_hourly_chart_series[i].name.indexOf(device_unit_hourly_name_list[j].name)!=-1&&device_unit_hourly_chart_series[i].name.indexOf(device_unit_hourly_node_list[h].name)!=-1&&device_unit_hourly_name_list[j].show&&device_unit_hourly_node_list[h].show) {
					device_unit_option_hourly.series.push(device_unit_hourly_chart_series[i]);
				}
			}
		}
	}
	if(count==1) {
		point_place=0;
		pointPlacement=0;
		pointPadding1=0.1;
		pointPadding2=0.24;
		pointPadding3=0.4;
	}else if(count==2) { 
		point_place=-0.2;
		pointPlacement=-0.2;
		point_width=0.4;
		pointPadding1=0.3;
		pointPadding2=0.36;
		pointPadding3=0.42;
	}else if(count==3) { 
		point_place=-0.3;
		pointPlacement=-0.3;
		point_width=0.3;
		pointPadding1=0.3;
		pointPadding2=0.36;
		pointPadding3=0.42;
	}else if(count==4) { 
		point_place=-0.3;
		pointPlacement=point_place;
		point_width=0.2;
		pointPadding1=0.4;
		pointPadding2=0.46;
		pointPadding3=0.48;
	}
	var j=0;
	for (var i = 0; i < device_unit_option_hourly.series.length; i++) {
		j++;
		if(device_unit_option_hourly.series[i].name.indexOf('能耗')!=-1){
			device_unit_option_hourly.series[i].pointPadding=pointPadding1;
		}
		if(device_unit_option_hourly.series[i].name.indexOf('产量')!=-1){
			device_unit_option_hourly.series[i].pointPadding=pointPadding2;
		}
		if(device_unit_option_hourly.series[i].name.indexOf('单耗')!=-1){
			break;
		}
		
		device_unit_option_hourly.series[i].pointPlacement=pointPlacement;
		pointPlacement=pointPlacement+point_width;
		if(j==count) {
			pointPlacement=point_place;
			j=0;
		}
	}
	device_unit_chart_hourly = Highcharts.chart('device_unit_chart_hourly', device_unit_option_hourly);
	
	
}

$(function() {
	//部门
	department_box = $("#department_box").dxTagBox({
        items: [],
        value: [],
        multiline: false,
        showSelectionControls: true,
        showMultiTagOnly: false,
        maxDisplayedTags: 3,
        height:36,
        searchEnabled: true,
		onValueChanged: function(args) {
			department_list = sel_items_to_list(args.value);
			if (department_list != "") {
				get_product_line_list();
			}
		},
    }).dxTagBox("instance");
	
	//产线
	line_box = $("#line_box").dxTagBox({
        items: [],
        value: [],
        multiline: false,
        showSelectionControls: true,
        showMultiTagOnly: false,
        maxDisplayedTags: 3,
        height:36,
        selectAllText:"全选",
        searchEnabled: true,
		onValueChanged: function(args) {
			product_line_data="";
			var str=JSON.parse(JSON.stringify(args.value));
			for (var i = 0; i < str.length; i++) {
				product_line_data = str[i].split('>')[0]+'--'+product_line_data;
			}
			product_line_list = sel_items_to_list(args.value);
			product_line_array = args.value;
			if (product_line_list != "") {
				get_device_list();
				load_daily_data();
			}
		},
    }).dxTagBox("instance");
	
	//设备
	device_box = $("#device_box").dxTagBox({
        items: [],
        value: [],
        multiline: false,
        showSelectionControls: true,
        showMultiTagOnly: false,
        maxDisplayedTags: 3,
        height:36,
        selectAllText:"全选",
        searchEnabled: true,
		onValueChanged: function(args) {
			product_line_lists="";
			device_lists="";
			var str=JSON.parse(JSON.stringify(args.value));
			for (var i = 0; i < str.length; i++) {
				product_line_lists = str[i].split('>')[0]+'--'+product_line_lists;
				device_lists=str[i].split('>')[1]+'--'+device_lists;
				str[i]=str[i].split('>')[1];	
			}
			
			device_list = sel_items_to_list(str);
			if (device_list != "") {
				
				get_device_unit_list();
				load_daily_data();
			}
		},
    }).dxTagBox("instance");

	//单元
	device_unit_box = $("#device_unit_box").dxTagBox({
        items: [],
        value: [],
        multiline: false,
        showSelectionControls: true,
        showMultiTagOnly: false,
        maxDisplayedTags: 3,
        height:36,
        placeholder:"设备单元",
        selectAllText:"全选",
        searchEnabled: true,
		onValueChanged: function(args) {
			product_line_listss="";
			device_listss="";
			device_unit_listss="";
			var str=JSON.parse(JSON.stringify(args.value));
			for (var i = 0; i < str.length; i++) {
				product_line_listss = str[i].split('>')[0]+'--'+product_line_listss;
				device_listss=str[i].split('>')[1]+'--'+device_listss;
				device_unit_listss=str[i].split('>')[2]+'--'+device_unit_listss;
				str[i]=str[i].split('>')[2];
			}
		
			device_unit_list = sel_items_to_list(str);
			if (device_unit_list != "") {
				load_daily_data();
			}
		},
    }).dxTagBox("instance");
});

function get_department_list() {

	var items_array = [];
	if (factory_list != "") {
		if(!ajax_json){
	    	factory_cons1_ajax_url=path + "/gynh/factory_chart_view/get_department_list.do";
	    }
	    $.ajax({
//	        url: path + "/gynh/factory_chart_view/get_department_list.do",
	        url:factory_cons1_ajax_url,
	        type: 'post',
	        async: false,
	        data: {
				factory_list: factory_list, 
	        },
	        success: function (data) {
	        	var jsonStr = JSON.stringify(data,null,4);
	        	console.log(jsonStr);
	        	items_array = query_list_to_array(data['data']);
	        },
	        error: function () {
	        	console.log('error');
	        }
	    });
	}
	
	department_box.option("items", items_array);
	department_box.option("value", []);
	line_box.option("items", []);
	line_box.option("value", []);
	device_box.option("items", []);
	device_box.option("value", []);
	device_unit_box.option("items", []);
	device_unit_box.option("value", []);

}

function get_product_line_list() {
	var items_array = [];
	if (department_list != "") {
		if(!ajax_json){
	    	factory_cons2_ajax_url=path + "/gynh/factory_chart_view/get_product_line_list.do";
	    }
		$.ajax({
//			url: path + "/gynh/factory_chart_view/get_product_line_list.do",
			url: factory_cons2_ajax_url,
			type: 'post',
			async: false,
			data: {
				factory_list: factory_list, 
				department_list: department_list, 
			},
			success: function (data) {
				var jsonStr = JSON.stringify(data,null,4);
	        	console.log(jsonStr);
				items_array = query_list_to_array(data['data']);
			},
			error: function () {
	        	console.log('error');
			}
		});
	} 
	
	line_box.option("items", items_array);
	line_box.option("value", []);
	device_box.option("items", []);
	device_box.option("value", []);
	device_unit_box.option("items", []);
	device_unit_box.option("value", []);
}

function get_device_list() {
	var items_array = [];
	if (product_line_list != "") {
		if(!ajax_json){
	    	factory_cons3_ajax_url=path + "/gynh/factory_chart_view/get_device_list.do";
	    }
		$.ajax({
//			url: path + "/gynh/factory_chart_view/get_device_list.do",
			url: factory_cons3_ajax_url,
			type: 'post',
			async: false,
			data: {
				factory_list: factory_list, 
				department_list: department_list, 
				product_line_list: product_line_list, 
			},
			success: function (data) {
				var jsonStr = JSON.stringify(data,null,4);
	        	console.log(jsonStr);
				var str=data['data'];
				for (var i = 0; i < str.length; i++) {
					str[i].str=str[i].eqp+'>'+str[i].str;
				}
				items_array = query_list_to_array(str);
			},
			error: function () {
	        	console.log('error');
			}
		}); 
	}
	device_box.option("items", items_array);
	device_box.option("value", []);
	device_unit_box.option("items", []);
	device_unit_box.option("value", []);
}

function get_device_unit_list() {
	var items_array = [];
	if (device_list != "") {
		if(!ajax_json){
			factory_cons4_ajax_url=path + "/gynh/factory_chart_view/get_device_unit_list.do";
		}
		$.ajax({
			url: factory_cons4_ajax_url,
//			url: path + "/gynh/factory_chart_view/get_device_unit_list.do",
			type: 'post',
			async: false,
			data: {
				factory_list: factory_list, 
				department_list: department_list, 
				product_line_list: product_line_lists, 
				device_list: device_lists, 
			},
			success: function (data) {
				var jsonStr = JSON.stringify(data,null,4);
	        	console.log(jsonStr);
				var str=data['data'];
				for (var i = 0; i < str.length; i++) {
					str[i].str=str[i].eqp+'>'+str[i].de+'>'+str[i].str;
				}
				items_array = query_list_to_array(data['data']);
			},
			error: function () {
	        	console.log('error');
			}
		});
	}
	device_unit_box.option("items", items_array);
	device_unit_box.option("value", []);
}
function query_list_to_array(query_list) {
	var arr = new Array();
	for (var i = 0; i < query_list.length; i++) {
		arr.push(query_list[i].str);
	}
	return arr;
}

function sel_items_to_list(sel_items) {
	if (sel_items.length == 0) {
		return "";
	}
	var list = "(";
	for (var i = 0; i < sel_items.length; i++) {
		list = list + "'" + sel_items[i] + "',"
	}
	list = list.substr(0,list.length-1) + ")";
	return list;
}


function load_daily_data() {
//	var msg_box = top.layer.msg('查询中...',{icon: 16, skin:is_dark?'layui-layer-lan':'', shade:0.1}, 1000000);
//	
//	if (device_unit_list != "") {
//		//get_device_unit_cons();
//		//set_device_unit_chart();
//	} else if (device_list != "") {
//		//set_device_chart();
//	} else if (product_line_list != "") {
//		//set_product_line_chart();
//	} 
//	
//	top.layer.close(msg_box);
//	top.layer.msg('数据加载完成', {icon: 1, skin:is_dark?'layui-layer-lan':''});
}

function set_department_chart() {
	export_init('department');
	var cons_data=new Array();
	var cons_series={};

	get_department_cons();

	department_option.xAxis.categories.splice(0, department_option.xAxis.categories.length);
	department_option.series.splice(0, department_option.series.length);
	
	//y轴值
	var date=start_date;
	while(true) { 
		department_option.xAxis.categories.push(date);
		if(date==end_date){
			break;
		}
		date=getFormatDate2(date,1);
	}
	/*for (var i = 0; i < department_cons.length; i++) {
		if(department_cons[i].e==energy) {
			if(snm!=null&&department_cons[i].snm!=snm){ 
				break;
			}
			var snm=department_cons[i].snm;
			department_option.xAxis.categories.push(department_cons[i].t);
		}
	}*/
	
	//能耗值
	snm=null;
	for (var i = 0; i < department_cons.length; i++) {
		if(department_cons[i].e==energy) {
			if(snm!=null&&department_cons[i].snm!=snm){ 
				cons_series={
				        name: department_cons[i-1].snm+'能耗',
				        type: 'column',
				        data: cons_data,
				        yAxis: 0,
				        color:cons_color,
				 };
				department_option.series.push(cons_series);
				cons_data=new Array();
			}
			snm=department_cons[i].snm;
			cons_data.push(department_cons[i].c);
		}
	}
	cons_series={
	        name: department_cons[department_cons.length-1].snm+'能耗',
	        type: 'column',
	        data: cons_data,
	        yAxis: 0,
	        color:cons_color,
	 };
	department_option.series.push(cons_series);
	cons_data=new Array();
	
	department_chart = Highcharts.chart('department_chart', department_option);
	department_chart_series=JSON.parse(JSON.stringify(department_option.series));
	department_chart_date=JSON.parse(JSON.stringify(department_option.xAxis.categories));
	
}

function set_product_line_chart() {
	product_line_first=true;
	export_init('product_line');
	var cons_data=new Array();
	var cons_series={};
	var output_data=new Array();
	var output_series={};
	var unit_cons_data=new Array();
	var unit_cons_series={};
	var point_place=-0.4;
	var pointPlacement=point_place;
	var point_width=0.2;
	var pointPadding1=0.4;
	var pointPadding2=0.46;
	var pointPadding3=0.48;
	
	if(product_line_data.split("--").length==2) {
		point_place=0;
		pointPlacement=0;
		pointPadding1=0.1;
		pointPadding2=0.24;
		pointPadding3=0.4;
	}else if(product_line_data.split("--").length==3) { 
		point_place=-0.2;
		pointPlacement=-0.2;
		point_width=0.4;
		pointPadding1=0.3;
		pointPadding2=0.36;
		pointPadding3=0.42;
	}else if(product_line_data.split("--").length==4) { 
		point_place=-0.3;
		pointPlacement=-0.3;
		point_width=0.3;
		pointPadding1=0.3;
		pointPadding2=0.36;
		pointPadding3=0.42;
	}else if(product_line_data.split("--").length==5) { 
		var point_place=-0.3;
		var pointPlacement=point_place;
		var point_width=0.2;
		var pointPadding1=0.4;
		var pointPadding2=0.46;
		var pointPadding3=0.48;
	}
	
	var str_data=product_line_data.split("--");
	str_data=str_data.sort();
	var str_html='';
	for (var i = 1; i < str_data.length; i++) {
		str_html=str_html+'<button class="layui-btn layui-btn-xs" style="background-color:#50e090;" id="button'+button_id+'" onclick=set_product_line_select("'+str_data[i]+'",'+button_id+') >'+str_data[i]+'</button>';
		button_id++;
	}
	
	document.getElementById("product_line_chart_product_line").innerHTML=str_html;
	
	get_product_line_cons();
	get_product_line_output();
	get_product_line_unit_cons();
	get_product_line_state_time();
	
	product_line_option.xAxis.categories.splice(0, product_line_option.xAxis.categories.length);
	product_line_option.series.splice(0, product_line_option.series.length);
	
	//y轴值
	
	var date=start_date;
	while(true) { 
		product_line_option.xAxis.categories.push(date);
		if(date==end_date){
			break;
		}
		date=getFormatDate2(date,1);
	}

	//能耗值
	snm=null;
	for (var i = 0; i < product_line_cons.length; i++) {
		if(product_line_cons[i].e==energy) {
			if(snm!=null&&product_line_cons[i].snm!=snm){ 
				cons_series={
				        name: product_line_cons[i-1].snm+'能耗',
				        type: 'column',
				        data: cons_data,
				        pointPadding: pointPadding1, 
						pointPlacement: pointPlacement,
				        yAxis: 0,
				        events:product_line_click_events,
				        color:cons_color,
				 };
				product_line_option.series.push(cons_series);
				cons_data=new Array();
				pointPlacement=pointPlacement+point_width;
			}
			cons_data.push(product_line_cons[i].c);
			snm=product_line_cons[i].snm;
		}
	}
	cons_series={
	        name: product_line_cons[product_line_cons.length-1].snm+'能耗',
	        type: 'column',
	        data: cons_data,
	        pointPadding: pointPadding1, 
			pointPlacement: pointPlacement,
	        yAxis: 0,
	        events:product_line_click_events,
	        color:cons_color,
	 };
	product_line_option.series.push(cons_series);
	cons_data=new Array();
	pointPlacement=point_place;
	
	//产量值
	snm=null;
	for (var i = 0; i < product_line_output.length; i++) {
		if(snm!=null&&product_line_output[i].snm!=snm){ 
			output_series={
			        name: product_line_output[i-1].snm+'产量',
			        type: 'column',
			        data: output_data,
			        pointPadding: pointPadding2,
					pointPlacement: pointPlacement,
			        yAxis: 1,
			        events:product_line_click_events,
			        color:product_color,
			 };
			product_line_option.series.push(output_series);
			output_data=new Array();
			pointPlacement = pointPlacement+point_width;
		}
		output_data.push(product_line_output[i].qty);	
		snm=product_line_output[i].snm;
	}
	output_series={
			name: product_line_output[product_line_output.length-1].snm+'产量',
			type: 'column',
			data: output_data,
			pointPadding: pointPadding2,
			pointPlacement: pointPlacement,
			yAxis: 1,
			events:product_line_click_events,
	        color:product_color,
	 };
	product_line_option.series.push(output_series);
	output_data=new Array();
	pointPlacement=point_place;
	
	//时间状态
	snm=null;
	for (var i = 0; i < product_line_state_time.length; i++) {
		if(snm!=null&&product_line_state_time[i].snm!=snm){ 
			output_series={
			        name: product_line_state_time[i-1].snm+'状态',
			        type: 'column',
			        data: output_data,
			        pointPadding: pointPadding3,
					pointPlacement: pointPlacement,
			        yAxis: 1,
			        events:product_line_click_events,
			        color:state_time_color,
			 };
			product_line_option.series.push(output_series);
			output_data=new Array();
			pointPlacement = pointPlacement+point_width;
		}
		output_data.push(product_line_state_time[i].qty);
		snm=product_line_state_time[i].snm;
	}
	
	output_series={
			name: product_line_state_time[product_line_state_time.length-1].snm+'状态',
			type: 'column',
			data: output_data,
			pointPadding: pointPadding3,
			pointPlacement: pointPlacement,
			yAxis: 1,
			events:product_line_click_events,
			color:state_time_color, 
	 };
	product_line_option.series.push(output_series);
	output_data=new Array();
	
	
	//单耗值
	snm=null;
	for (var i = 0; i < product_line_unit_cons.length; i++) {
		if(product_line_unit_cons[i].e==energy) {
			if(snm!=null&&product_line_unit_cons[i].snm!=snm){ 
				unit_cons_series= {
				        name: product_line_unit_cons[i-1].snm+'单耗',
				        type: 'spline',
				        data: unit_cons_data,
				        yAxis: 2
				};
				product_line_option.series.push(unit_cons_series);
				unit_cons_data=new Array();
			}
			unit_cons_data.push(product_line_unit_cons[i].c);
			snm=product_line_unit_cons[i].snm;
		}
	}
	unit_cons_series= {
			 name: product_line_unit_cons[product_line_unit_cons.length-1].snm+'单耗',
	         type: 'spline',
		     data: unit_cons_data, 
		     yAxis: 2
	 };
	product_line_option.series.push(unit_cons_series);
	unit_cons_data=new Array();
	product_line_chart = Highcharts.chart('product_line_chart', product_line_option);
	product_line_chart_series=JSON.parse(JSON.stringify(product_line_option.series));
	product_line_chart_date=JSON.parse(JSON.stringify(product_line_option.xAxis.categories));
	set_product_line_select('产量',1);
	set_product_line_select('状态',3);
}


function set_product_line_chart_hourly() {
	var cons_data=new Array();
	var cons_series={};
	var output_data=new Array();
	var output_series={};
	var unit_cons_data=new Array();
	var unit_cons_series={};
	var point_place=-0.4;
	var pointPlacement=point_place;
	var point_width=0.2;
	var pointPadding1=0.4;
	var pointPadding2=0.46;
	var pointPadding3=0.48;
	
	if(product_line_data.split("--").length==2) {
		point_place=0;
		pointPlacement=0;
		pointPadding1=0.1;
		pointPadding2=0.24;
		pointPadding3=0.4;
	}else if(product_line_data.split("--").length==3) { 
		point_place=-0.2;
		pointPlacement=-0.2;
		point_width=0.4;
		pointPadding1=0.3;
		pointPadding2=0.36;
		pointPadding3=0.42;
	}else if(product_line_data.split("--").length==4) { 
		point_place=-0.3;
		pointPlacement=-0.3;
		point_width=0.3;
		pointPadding1=0.3;
		pointPadding2=0.36;
		pointPadding3=0.42;
	}else if(product_line_data.split("--").length==5) { 
		point_place=-0.3;
		pointPlacement=point_place;
		point_width=0.2;
		pointPadding1=0.4;
		pointPadding2=0.46;
		 pointPadding3=0.48;
	}
	
	var str_data=product_line_data.split("--");
	str_data=str_data.sort();
	var str_html='';
	for (var i = 1; i < str_data.length; i++) {
		str_html=str_html+'<button class="layui-btn layui-btn-xs" style="background-color:#50e090;"id="button'+button_id+'" onclick=set_product_line_hourly_select("'+str_data[i]+'",'+button_id+') >'+str_data[i]+'</button>';
		button_id++;
	}
	
	document.getElementById("product_line_chart_product_line_hourly").innerHTML=str_html;
	
		product_line_option_hourly.xAxis.categories.splice(0, product_line_option_hourly.xAxis.categories.length);
		product_line_option_hourly.series.splice(0, product_line_option_hourly.series.length);
		
	//y轴值
	for (var i = 1; i < 25; i++) {
		device_option_hourly.xAxis.categories.push(i);
	}
	
	//能耗值
	snm=null;
	for (var i = 0; i < product_line_cons_hourly.length; i++) {
		if(product_line_cons_hourly[i].e==energy) {
			if(snm!=null&&product_line_cons_hourly[i].snm!=snm){ 
				cons_series={
				        name: product_line_cons_hourly[i-1].snm+'能耗',
				        type: 'column',
				        data: cons_data,
				        pointPadding: pointPadding1, 
						pointPlacement: pointPlacement,
				        yAxis: 0,
				        color:cons_color,
				 };
				product_line_option_hourly.series.push(cons_series);
				cons_data=new Array();
				pointPlacement=pointPlacement+point_width;
			}
			cons_data.push(product_line_cons_hourly[i].c);
			snm=product_line_cons_hourly[i].snm;
		}
	}
	if(product_line_cons_hourly.length>0){
		cons_series={
		        name: product_line_cons_hourly[product_line_cons_hourly.length-1].snm+'能耗',
		        type: 'column',
		        data: cons_data,
		        pointPadding: pointPadding1, 
				pointPlacement: pointPlacement,
		        yAxis: 0,
		        color:cons_color,
		 };
	}
	product_line_option_hourly.series.push(cons_series);
	cons_data=new Array();
	pointPlacement=point_place;
	
	//产量值
	snm=null;
	for (var i = 0; i < product_line_output_hourly.length; i++) {
		if(snm!=null&&product_line_output_hourly[i].snm!=snm){ 
			output_series={
			        name: product_line_output_hourly[i-1].snm+'产量',
			        type: 'column',
			        data: output_data,
			        pointPadding: pointPadding3,
					pointPlacement: pointPlacement,
			        yAxis: 1,
			        color:product_color,
			 };
			product_line_option_hourly.series.push(output_series);
			output_data=new Array();
			pointPlacement = pointPlacement+point_width;
		}
		output_data.push(product_line_output_hourly[i].qty);
		snm=product_line_output_hourly[i].snm;
	}
	output_series={
			name: product_line_output_hourly[product_line_output_hourly.length-1].snm+'产量',
			type: 'column',
			data: output_data,
			pointPadding: pointPadding3,
			pointPlacement: pointPlacement,
			yAxis: 1,
	        color:product_color,
	 };
	product_line_option_hourly.series.push(output_series);
	output_data=new Array();
	
	//单耗值
	snm=null;
	for (var i = 0; i < product_line_unit_cons_hourly.length; i++) {
		if(product_line_unit_cons_hourly[i].e==energy) {
			if(snm!=null&&product_line_unit_cons_hourly[i].snm!=snm){ 
				unit_cons_series= {
				        name: product_line_unit_cons_hourly[i-1].snm+'单耗',
				        type: 'spline',
				        data: unit_cons_data,
				        yAxis: 2
				};
				product_line_option_hourly.series.push(unit_cons_series);
				unit_cons_data=new Array();
			}
			unit_cons_data.push(product_line_unit_cons_hourly[i].c);
			snm=product_line_unit_cons_hourly[i].snm;
		}
	}
	if(product_line_unit_cons_hourly.length>0) {
		unit_cons_series= {
				 name: product_line_unit_cons_hourly[product_line_unit_cons_hourly.length-1].snm+'单耗',
		         type: 'spline',
			     data: unit_cons_data, 
			     yAxis: 2
		 };
	}
	
	product_line_option_hourly.series.push(unit_cons_series);
	unit_cons_data=new Array();

	product_line_chart_hourly = Highcharts.chart('product_line_chart_hourly', product_line_option_hourly);
	product_line_hourly_chart_series=JSON.parse(JSON.stringify(product_line_option_hourly.series));
	set_product_line_hourly_select('产量',5);
	
}

function set_device_chart() {
	export_init('device');
	var cons_data=new Array();
	var cons_series={};
	var output_data=new Array();
	var output_series={};
	var unit_cons_data=new Array();
	var unit_cons_series={};
	var point_place=-0.4;
	var pointPlacement=point_place;
	var point_width=0.2;
	var pointPadding1=0.4;
	var pointPadding2=0.46;
	var pointPadding3=0.48;
	
	if(device_lists.split("--").length==2) {
		point_place=0;
		pointPlacement=0;
		pointPadding1=0.1;
		pointPadding2=0.24;
		pointPadding3=0.4;
	}else if(device_lists.split("--").length<=4) { 
		point_place=-0.3;
		pointPlacement=-0.3;
		point_width=0.3;
		pointPadding1=0.3;
		pointPadding2=0.36;
		pointPadding3=0.42;
	}
	if(device_lists.split("--").length==2) {
		point_place=0;
		pointPlacement=0;
		pointPadding1=0.1;
		pointPadding2=0.24;
		pointPadding3=0.4;
	}else if(device_lists.split("--").length==3) { 
		point_place=-0.2;
		pointPlacement=-0.2;
		point_width=0.4;
		pointPadding1=0.3;
		pointPadding2=0.36;
		pointPadding3=0.42;
	}else if(device_lists.split("--").length==4) { 
		point_place=-0.3;
		pointPlacement=-0.3;
		point_width=0.3;
		pointPadding1=0.3;
		pointPadding2=0.36;
		pointPadding3=0.42;
	}else if(device_lists.split("--").length==5) { 
		point_place=-0.3;
		pointPlacement=point_place;
		point_width=0.2;
		pointPadding1=0.4;
		pointPadding2=0.46;
		 pointPadding3=0.48;
	}
	
	device_output=new Array();
	device_state_time=new Array();
	get_product_line_state_time();
	get_device_cons();
	get_product_line_output();
	var h=0;
	for (var i = 0; i < device_cons.length; i++) {
		for (var j = 0; j < product_line_state_time.length; j++) {
			if(device_cons[i].t==product_line_state_time[j].t&&device_cons[i].lnm.indexOf(product_line_state_time[j].snm)!=-1){
				device_state_time.push(JSON.parse(JSON.stringify(product_line_state_time[j])));
				device_state_time[h].snm=device_cons[i].lnm;
				h++;
				break;
			}
		}
		
	}
	h=0;
	for (var i = 0; i < device_cons.length; i++) {
		for (var j = 0; j < product_line_output.length; j++) {
			if(device_cons[i].t==product_line_output[j].t&&device_cons[i].lnm.indexOf(product_line_output[j].snm)!=-1){
				device_output.push(JSON.parse(JSON.stringify(product_line_output[j])));
				device_output[h].snm=device_cons[i].lnm;
				h++;
				break;
			}
		}
		
	}
	//get_device_state_time();
	device_unit_conss=JSON.parse(JSON.stringify(device_cons));
	
	for (var i = 0; i < device_unit_conss.length; i++) {
		for (var j = 0; j < device_unit_conss.length; j++) {
			if(device_unit_conss[i].t==device_output[j].t&&device_unit_conss[i].lnm==device_output[j].snm){
				device_unit_conss[i].c=device_cons[i].c/device_output[i].qty;
			}
		}
		
	}
	
	device_option.xAxis.categories.splice(0, device_option.xAxis.categories.length);
	device_option.series.splice(0, device_option.series.length);
	
	//y轴值
	var date=start_date;
	while(true) { 
		device_option.xAxis.categories.push(date);
		if(date==end_date){
			break;
		}
		date=getFormatDate2(date,1);
	}
//	for (var i = 0; i < device_cons.length; i++) {
//		if(device_cons[i].e==energy) {
//			if(snm!=null&&device_cons[i].lnm!=device_cons[i-1].lnm){ 
//				break;
//			}
//			device_option.xAxis.categories.push(device_cons[i].t);
//		}
//	}
	
	//能耗值
	snm=null;
	for (var i = 0; i < device_cons.length; i++) {
		if(device_cons[i].e==energy) {
			if(snm!=null&&device_cons[i].lnm!=snm){ 
				cons_series={
				        name: device_cons[i-1].lnm+'能耗',
				        type: 'column',
				        data: cons_data,
				        pointPadding: pointPadding1, 
						pointPlacement: pointPlacement,
				        yAxis: 0,
				        events:device_click_events,
				        color:cons_color,
				 };
				device_option.series.push(cons_series);
				cons_data=new Array();
				pointPlacement=pointPlacement+point_width;
			}
			cons_data.push(device_cons[i].c);
			snm=device_cons[i].lnm;
		}
	}
	if(device_cons.length>0) {
		cons_series={
		        name: device_cons[device_cons.length-1].lnm+'能耗',
		        type: 'column',
		        data: cons_data,
		        pointPadding: pointPadding1, 
				pointPlacement: pointPlacement,
		        yAxis: 0,
		        events:device_click_events,
		        color:cons_color,
		 };
	}
	
	device_option.series.push(cons_series);
	cons_data=new Array();
	pointPlacement=point_place;
	
	//产量值
	snm=null;
	for (var i = 0; i < device_output.length; i++) {
		if(snm!=null&&device_output[i].snm!=snm){ 
			output_series={
			        name: device_output[i-1].snm+'产量',
			        type: 'column',
			        data: output_data,
			        pointPadding: pointPadding2,
					pointPlacement: pointPlacement,
			        yAxis: 1,
			        events:device_click_events,
			        color:product_color,
			 };
			device_option.series.push(output_series);
			output_data=new Array();
			pointPlacement = pointPlacement+point_width;
		}
		output_data.push(device_output[i].qty);	
		snm=device_output[i].snm;
	}
	output_series={
			name: device_output[device_output.length-1].snm+'产量',
			type: 'column',
			data: output_data,
			pointPadding: pointPadding2,
			pointPlacement: pointPlacement,
			yAxis: 1,
			events:device_click_events,
	        color:product_color,
	 };
	device_option.series.push(output_series);
	output_data=new Array();
	pointPlacement=point_place;
	
	//时间状态
	snm=null;
	for (var i = 0; i < device_state_time.length; i++) {
		if(snm!=null&&device_state_time[i].snm!=snm){ 
			output_series={
			        name: device_state_time[i-1].snm+'状态',
			        type: 'column',
			        data: output_data,
			        pointPadding: pointPadding3,
					pointPlacement: pointPlacement,
			        yAxis: 1,
			        events:device_click_events,
			        color:state_time_color,
			 };
			device_option.series.push(output_series);
			output_data=new Array();
			pointPlacement = pointPlacement+point_width;
		}
		output_data.push(device_state_time[i].qty);	
		snm=device_state_time[i].snm;
	}
	
	output_series={
			name: device_state_time[device_state_time.length-1].snm+'状态',
			type: 'column',
			data: output_data,
			pointPadding: pointPadding3,
			pointPlacement: pointPlacement,
			yAxis: 1,
			events:device_click_events,
			color:state_time_color,
	 };
	device_option.series.push(output_series);
	output_data=new Array();
	
	
	
	//单耗值
	snm=null;
	for (var i = 0; i < device_unit_conss.length; i++) {
		if(device_unit_conss[i].e==energy) {
			if(snm!=null&&device_unit_conss[i].lnm!=snm){ 
				unit_cons_series= {
				        name: device_unit_conss[i-1].lnm+'单耗',
				        type: 'spline',
				        data: unit_cons_data,
				        yAxis: 2
				};
				device_option.series.push(unit_cons_series);
				unit_cons_data=new Array();
			}
			unit_cons_data.push(device_unit_conss[i].c);
			snm=device_unit_conss[i].lnm;
		}
	}
	unit_cons_series= {
			 name: device_unit_conss[device_unit_conss.length-1].lnm+'单耗',
	         type: 'spline',
		     data: unit_cons_data, 
		     yAxis: 2
	 };
	device_option.series.push(unit_cons_series);
	unit_cons_data=new Array();
	device_chart = Highcharts.chart('device_chart', device_option);
	device_chart_series=JSON.parse(JSON.stringify(device_option.series));
	device_chart_date=JSON.parse(JSON.stringify(device_option.xAxis.categories));
	
	var str_data=new Set();
	for (var i = 0; i < device_output.length; i++) {
		str_data.add(device_output[i].snm);
	}
	var str_html='';
	for (let x of str_data) { // 遍历Set
		str_html=str_html+'<button class="layui-btn layui-btn-xs" style="background-color:#50e090;"id="button'+button_id+'" onclick="set_device_select(&apos;'+x+'&apos;,'+button_id+')">'+x+'</button>';
		button_id++;
		
	}
	document.getElementById("device_chart_device").innerHTML=str_html;	
	
	set_device_select('产量',8);
	set_device_select('状态',10);
}
function set_device_chart_hourly() {
	var cons_data=new Array();
	var cons_series={};
	var output_data=new Array();
	var output_series={};
	var unit_cons_data=new Array();
	var unit_cons_series={};
	var point_place=-0.4;
	var pointPlacement=point_place;
	var point_width=0.2;
	var pointPadding1=0.4;
	var pointPadding2=0.46;
	var pointPadding3=0.48;
	
	if(device_lists.split("--").length==2) {
		point_place=0;
		pointPlacement=0;
		pointPadding1=0.1;
		pointPadding2=0.24;
		pointPadding3=0.4;
	}else if(device_lists.split("--").length==3) { 
		point_place=-0.2;
		pointPlacement=-0.2;
		point_width=0.4;
		pointPadding1=0.3;
		pointPadding2=0.36;
		pointPadding3=0.42;
	}else if(device_lists.split("--").length==4) { 
		point_place=-0.3;
		pointPlacement=-0.3;
		point_width=0.3;
		pointPadding1=0.3;
		pointPadding2=0.36;
		pointPadding3=0.42;
	}else if(device_lists.split("--").length==5) { 
		point_place=-0.3;
		pointPlacement=point_place;
		point_width=0.2;
		pointPadding1=0.4;
		pointPadding2=0.46;
		pointPadding3=0.48;
	}
	
	
	device_output_hourly=new Array();
	
	
	for (var i = 0; i < device_cons_hourly.length; i++) {
		for (var j = 0; j < product_line_output_hourly.length; j++) {
			if(device_cons_hourly[i].t==product_line_output_hourly[j].t&&device_cons_hourly[i].lnm.indexOf(product_line_output_hourly[j].snm)!=-1){
				device_output_hourly.push(JSON.parse(JSON.stringify(product_line_output_hourly[j])));
				device_output_hourly[i].snm=device_cons_hourly[i].lnm;
				break;
			}
		}
		
	}
	//get_device_state_time();
	device_unit_conss_hourly=JSON.parse(JSON.stringify(device_cons_hourly));
	
	for (var i = 0; i < device_unit_conss_hourly.length; i++) {
		for (var j = 0; j < device_unit_conss_hourly.length; j++) {
			if(device_unit_conss_hourly[i].t==device_output_hourly[j].t&&device_unit_conss_hourly[i].lnm==device_output_hourly[j].snm){
				if(device_output_hourly[i].qty==0) {
					device_unit_conss_hourly[i].c=0;
				}else {
					device_unit_conss_hourly[i].c=device_cons_hourly[i].c/device_output_hourly[i].qty;
				}
			}
		}
	}
	
	device_option_hourly.xAxis.categories.splice(0, device_option_hourly.xAxis.categories.length);
	device_option_hourly.series.splice(0, device_option_hourly.series.length);
	
	//y轴值
	for (var i = 1; i < 25; i++) {
		device_option_hourly.xAxis.categories.push(i);
	}
	
	//能耗值
	snm=null;
	for (var i = 0; i < device_cons_hourly.length; i++) {
		if(device_cons_hourly[i].e==energy) {
			if(snm!=null&&device_cons_hourly[i].lnm!=snm){ 
				cons_series={
				        name: device_cons_hourly[i-1].lnm+'能耗',
				        type: 'column',
				        data: cons_data,
				        pointPadding: pointPadding1, 
						pointPlacement: pointPlacement,
				        yAxis: 0,
				        color:cons_color,
				 };
				device_option_hourly.series.push(cons_series);
				cons_data=new Array();
				pointPlacement=pointPlacement+point_width;
			}
			cons_data.push(device_cons_hourly[i].c);
			snm=device_cons_hourly[i].lnm;
		}
	}
	cons_series={
	        name: device_cons_hourly[device_cons_hourly.length-1].lnm+'能耗',
	        type: 'column',
	        data: cons_data,
	        pointPadding: pointPadding1, 
			pointPlacement: pointPlacement,
	        yAxis: 0,
	        color:cons_color,
	 };
	device_option_hourly.series.push(cons_series);
	cons_data=new Array();
	pointPlacement=point_place;
	
	//产量值
	snm=null;
	for (var i = 0; i < device_output_hourly.length; i++) {
		if(snm!=null&&device_output_hourly[i].snm!=snm){ 
			output_series={
			        name: device_output_hourly[i-1].snm+'产量',
			        type: 'column',
			        data: output_data,
			        pointPadding: pointPadding3,
					pointPlacement: pointPlacement,
			        yAxis: 1,
			        color:product_color,
			 };
			device_option_hourly.series.push(output_series);
			output_data=new Array();
			pointPlacement = pointPlacement+point_width;
		}
		output_data.push(device_output_hourly[i].qty);	
		snm=device_output_hourly[i].snm;
	}
	output_series={
			name: device_output_hourly[device_output_hourly.length-1].snm+'产量',
			type: 'column',
			data: output_data,
			pointPadding:pointPadding3,
			pointPlacement: pointPlacement,
			yAxis: 1,
	        color:product_color,
	 };
	device_option_hourly.series.push(output_series);
	output_data=new Array();
	
	//单耗值
	snm=null;
	for (var i = 0; i < device_unit_conss_hourly.length; i++) {
		if(device_unit_conss_hourly[i].e==energy) {
			if(snm!=null&&device_unit_conss_hourly[i].lnm!=snm){ 
				unit_cons_series= {
				        name: device_unit_conss_hourly[i-1].lnm+'单耗',
				        type: 'spline',
				        data: unit_cons_data,
				        yAxis: 2
				};
				device_option_hourly.series.push(unit_cons_series);
				unit_cons_data=new Array();
			}
			unit_cons_data.push(device_unit_conss_hourly[i].c);
			snm=device_unit_conss_hourly[i].lnm;
		}
	}
	if(device_unit_conss_hourly.length>0) {
		unit_cons_series= {
				 name: device_unit_conss_hourly[device_unit_conss_hourly.length-1].lnm+'单耗',
		         type: 'spline',
			     data: unit_cons_data, 
			     yAxis: 2
		 };
	}
	
	device_option_hourly.series.push(unit_cons_series);
	unit_cons_data=new Array();

	device_chart_hourly = Highcharts.chart('device_chart_hourly', device_option_hourly);
	device_hourly_chart_series=JSON.parse(JSON.stringify(device_option_hourly.series));
	device_hourly_chart_date=JSON.parse(JSON.stringify(device_option_hourly.xAxis.categories));
	
	var str_data=new Set();
	for (var i = 0; i < device_output_hourly.length; i++) {
		str_data.add(device_output_hourly[i].snm);
	}
	var str_html='';
	for (let x of str_data) { // 遍历Set
		str_html=str_html+'<button class="layui-btn layui-btn-xs" style="background-color:#50e090;"id="button'+button_id+'" onclick="set_device_hourly_select(&apos;'+x+'&apos;,'+button_id+')">'+x+'</button>';
		button_id++;
	}
	document.getElementById("device_chart_device_hourly").innerHTML=str_html;	
	
	set_device_hourly_select('产量',12);
}



function set_device_unit_chart() {
	export_init('device_unit');
	var cons_data=new Array();
	var cons_series={};
	var output_data=new Array();
	var output_series={};
	var unit_cons_data=new Array();
	var unit_cons_series={};
	var point_place=-0.4;
	var pointPlacement=point_place;
	var point_width=0.2;
	var pointPadding1=0.4;
	var pointPadding2=0.46;
	var pointPadding3=0.48;
	
	if(device_listss.split("--").length==2) {
		point_place=0;
		pointPlacement=0;
		pointPadding1=0.1;
		pointPadding2=0.24;
		pointPadding3=0.4;
	}else if(device_listss.split("--").length==3) { 
		point_place=-0.2;
		pointPlacement=-0.2;
		point_width=0.4;
		pointPadding1=0.3;
		pointPadding2=0.36;
		pointPadding3=0.42;
	}else if(device_listss.split("--").length==4) { 
		point_place=-0.3;
		pointPlacement=-0.3;
		point_width=0.3;
		pointPadding1=0.3;
		pointPadding2=0.36;
		pointPadding3=0.42;
	}else if(device_listss.split("--").length==5) { 
		point_place=-0.3;
		pointPlacement=point_place;
		point_width=0.2;
		pointPadding1=0.4;
		pointPadding2=0.46;
		pointPadding3=0.48;
	}
	
	
	device_unit_output=new Array();
	device_unit_state_time=new Array();
	get_product_line_state_time();
	get_device_unit_cons();
	get_product_line_output();
	
	for (var i = 0; i < device_unit_cons.length; i++) {
		for (var j = 0; j < product_line_state_time.length; j++) {
			if(device_unit_cons[i].t==product_line_state_time[j].t&&device_unit_cons[i].lnm.indexOf(product_line_state_time[j].snm)!=-1){
				device_unit_state_time.push(JSON.parse(JSON.stringify(product_line_state_time[j])));
				device_unit_state_time[i].snm=device_unit_cons[i].lnm;
				break;
			}
		}
		
	}
	for (var i = 0; i < device_unit_cons.length; i++) {
		for (var j = 0; j < product_line_output.length; j++) {
			if(device_unit_cons[i].t==product_line_output[j].t&&device_unit_cons[i].lnm.indexOf(product_line_output[j].snm)!=-1){
				device_unit_output.push(JSON.parse(JSON.stringify(product_line_output[j])));
				device_unit_output[i].snm=device_unit_cons[i].lnm;
				break;
			}
		}
		
	}
	//get_device_unit_state_time();
	device_unit_unit_cons=JSON.parse(JSON.stringify(device_unit_cons));
	
	for (var i = 0; i < device_unit_unit_cons.length; i++) {
		for (var j = 0; j < device_unit_unit_cons.length; j++) {
			if(device_unit_unit_cons[i].t==device_unit_output[j].t&&device_unit_unit_cons[i].lnm==device_unit_output[j].snm){
				device_unit_unit_cons[i].c=device_unit_cons[i].c/device_unit_output[i].qty;
			}
		}
		
	}
	
	device_unit_option.xAxis.categories.splice(0, device_unit_option.xAxis.categories.length);
	device_unit_option.series.splice(0, device_unit_option.series.length);
	
	//y轴值
	var date=start_date;
	while(true) { 
		device_unit_option.xAxis.categories.push(date);
		if(date==end_date){
			break;
		}
		date=getFormatDate2(date,1);
	}
//	for (var i = 0; i < device_unit_cons.length; i++) {
//		if(device_unit_cons[i].e==energy) {
//			if(snm!=null&&device_unit_cons[i].lnm!=device_unit_cons[i-1].lnm){ 
//				break;
//			}
//			device_unit_option.xAxis.categories.push(device_unit_cons[i].t);
//		}
//	}
	
	//能耗值
	snm=null;
	for (var i = 0; i < device_unit_cons.length; i++) {
		if(device_unit_cons[i].e==energy) {
			if(snm!=null&&device_unit_cons[i].lnm!=snm){ 
				cons_series={
				        name: device_unit_cons[i-1].lnm+'能耗',
				        type: 'column',
				        data: cons_data,
				        pointPadding: pointPadding1, 
						pointPlacement: pointPlacement,
				        yAxis: 0,
				        events:device_unit_click_events,
				        color:cons_color,
				 };
				device_unit_option.series.push(cons_series);
				cons_data=new Array();
				pointPlacement=pointPlacement+point_width;
			}
			cons_data.push(device_unit_cons[i].c);
			snm=device_unit_cons[i].lnm;
		}
	}
	cons_series={
	        name: device_unit_cons[device_unit_cons.length-1].lnm+'能耗',
	        type: 'column',
	        data: cons_data,
	        pointPadding: pointPadding1, 
			pointPlacement: pointPlacement,
	        yAxis: 0,
	        events:device_unit_click_events,
	        color:cons_color,
	 };
	device_unit_option.series.push(cons_series);
	cons_data=new Array();
	pointPlacement=point_place;
	
	//产量值
	snm=null;
	for (var i = 0; i < device_unit_output.length; i++) {
		if(snm!=null&&device_unit_output[i].snm!=snm){ 
			output_series={
			        name: device_unit_output[i-1].snm+'产量',
			        type: 'column',
			        data: output_data,
			        pointPadding: pointPadding2,
					pointPlacement: pointPlacement,
			        yAxis: 1,
			        events:device_unit_click_events,
			        color:product_color,
			 };
			device_unit_option.series.push(output_series);
			output_data=new Array();
			pointPlacement = pointPlacement+point_width;
		}
		output_data.push(device_unit_output[i].qty);
		snm=device_unit_output[i].snm;
	}
	output_series={
			name: device_unit_output[device_unit_output.length-1].snm+'产量',
			type: 'column',
			data: output_data,
			pointPadding: pointPadding2,
			pointPlacement: pointPlacement,
			yAxis: 1,
			events:device_unit_click_events,
	        color:product_color,
	 };
	device_unit_option.series.push(output_series);
	output_data=new Array();
	pointPlacement=point_place;
	
	//时间状态
	snm=null;
	for (var i = 0; i < device_unit_state_time.length; i++) {
		if(snm!=null&&device_unit_state_time[i].snm!=snm){ 
			output_series={
			        name: device_unit_state_time[i-1].snm+'状态',
			        type: 'column',
			        data: output_data,
			        pointPadding: pointPadding3,
					pointPlacement: pointPlacement,
			        yAxis: 1,
			        events:device_unit_click_events,
			        color:state_time_color,
			 };
			device_unit_option.series.push(output_series);
			output_data=new Array();
			pointPlacement = pointPlacement+point_width;
		}
		output_data.push(device_unit_state_time[i].qty);	
		snm=device_unit_state_time[i].snm;
	}
	
	output_series={
			name: device_unit_state_time[device_unit_state_time.length-1].snm+'状态',
			type: 'column',
			data: output_data,
			pointPadding: pointPadding3,
			pointPlacement: pointPlacement,
			yAxis: 1,
			events:device_unit_click_events,
			color:state_time_color,
	 };
	device_unit_option.series.push(output_series);
	output_data=new Array();
	
	//单耗值
	snm=null;
	for (var i = 0; i < device_unit_unit_cons.length; i++) {
		if(device_unit_unit_cons[i].e==energy) {
			if(snm!=null&&device_unit_unit_cons[i].lnm!=snm){ 
				unit_cons_series= {
				        name: device_unit_unit_cons[i-1].lnm+'单耗',
				        type: 'spline',
				        data: unit_cons_data,
				        yAxis: 2
				};
				device_unit_option.series.push(unit_cons_series);
				unit_cons_data=new Array();
			}
			unit_cons_data.push(device_unit_unit_cons[i].c);
			snm=device_unit_unit_cons[i].lnm;
		}
	}
	unit_cons_series= {
			 name: device_unit_unit_cons[device_unit_unit_cons.length-1].lnm+'单耗',
	         type: 'spline',
		     data: unit_cons_data, 
		     yAxis: 2
	 };
	device_unit_option.series.push(unit_cons_series);
	unit_cons_data=new Array();
	
	device_unit_chart = Highcharts.chart('device_unit_chart', device_unit_option);
	device_unit_chart_series=JSON.parse(JSON.stringify(device_unit_option.series));
	device_unit_chart_date=JSON.parse(JSON.stringify(device_unit_option.xAxis.categories));
	
	var str_data=new Set();
	for (var i = 0; i < device_unit_output.length; i++) {
		str_data.add(device_unit_output[i].snm);
	}
	var str_html='';
	for (let x of str_data) { // 遍历Set
		str_html=str_html+'<button class="layui-btn layui-btn-xs" style="background-color:#50e090;"id="button'+button_id+'" onclick="set_device_unit_select(&apos;'+x+'&apos;,'+button_id+')">'+x+'</button>';
		button_id++;
	}
	document.getElementById("device_unit_chart_device_unit").innerHTML=str_html;
	
	set_device_unit_select('产量',15);
	set_device_unit_select('状态',17);
	
}


function set_device_unit_chart_hourly() {
	var cons_data=new Array();
	var cons_series={};
	var output_data=new Array();
	var output_series={};
	var unit_cons_data=new Array();
	var unit_cons_series={};
	var point_place=-0.4;
	var pointPlacement=point_place;
	var point_width=0.2;
	var pointPadding1=0.4;
	var pointPadding2=0.46;
	var pointPadding3=0.48;
	
	if(device_listss.split("--").length==2) {
		point_place=0;
		pointPlacement=0;
		pointPadding1=0.1;
		pointPadding2=0.24;
		pointPadding3=0.4;
	}else if(device_listss.split("--").length==3) { 
		point_place=-0.2;
		pointPlacement=-0.2;
		point_width=0.4;
		pointPadding1=0.3;
		pointPadding2=0.36;
		pointPadding3=0.42;
	}else if(device_listss.split("--").length==4) { 
		point_place=-0.3;
		pointPlacement=-0.3;
		point_width=0.3;
		pointPadding1=0.3;
		pointPadding2=0.36;
		pointPadding3=0.42;
	}else if(device_listss.split("--").length==5) { 
		point_place=-0.3;
		pointPlacement=point_place;
		point_width=0.2;
		pointPadding1=0.4;
		pointPadding2=0.46;
		pointPadding3=0.48;
	}
	
	device_unit_output_hourly=new Array();
	
	
	for (var i = 0; i < device_unit_cons_hourly.length; i++) {
		for (var j = 0; j < product_line_output_hourly.length; j++) {
			if(device_unit_cons_hourly[i].t==product_line_output_hourly[j].t&&device_unit_cons_hourly[i].lnm.indexOf(product_line_output_hourly[j].snm)!=-1){
				device_unit_output_hourly.push(JSON.parse(JSON.stringify(product_line_output_hourly[j])));
				device_unit_output_hourly[i].snm=device_unit_cons_hourly[i].lnm;
				break;
			}
		}
		
	}
	//get_device_unit_state_time();
	device_unit_unit_cons_hourly=JSON.parse(JSON.stringify(device_unit_cons_hourly));
	
	for (var i = 0; i < device_unit_unit_cons_hourly.length; i++) {
		for (var j = 0; j < device_unit_unit_cons_hourly.length; j++) {
			if(device_unit_unit_cons_hourly[i].t==device_unit_output_hourly[j].t&&device_unit_unit_cons_hourly[i].lnm==device_unit_output_hourly[j].snm){
				if(device_unit_output_hourly[i].qty==0) {
					device_unit_unit_cons_hourly[i].c=0;
				}else {
					device_unit_unit_cons_hourly[i].c=device_unit_cons_hourly[i].c/device_unit_output_hourly[i].qty;
				}
			}
		}
		
	}
	
	device_unit_option_hourly.xAxis.categories.splice(0, device_unit_option_hourly.xAxis.categories.length);
	device_unit_option_hourly.series.splice(0, device_unit_option_hourly.series.length);
	
	//y轴值
	for (var i = 1; i < 25; i++) {
		device_option_hourly.xAxis.categories.push(i);
	}

	
	//能耗值
	snm=null;
	for (var i = 0; i < device_unit_cons_hourly.length; i++) {
		if(device_unit_cons_hourly[i].e==energy) {
			if(snm!=null&&device_unit_cons_hourly[i].lnm!=snm){ 
				cons_series={
				        name: device_unit_cons_hourly[i-1].lnm+'能耗',
				        type: 'column',
				        data: cons_data,
				        pointPadding: pointPadding1, 
						pointPlacement: pointPlacement,
				        yAxis: 0,
				        color:cons_color,
				 };
				device_unit_option_hourly.series.push(cons_series);
				cons_data=new Array();
				pointPlacement=pointPlacement+point_width;
			}
			cons_data.push(device_unit_cons_hourly[i].c);
			snm=device_unit_cons_hourly[i].lnm;
		}
	}
	if(device_unit_cons_hourly.length>0) {
		cons_series={
		        name: device_unit_cons_hourly[device_unit_cons_hourly.length-1].lnm+'能耗',
		        type: 'column',
		        data: cons_data,
		        pointPadding: pointPadding1, 
				pointPlacement: pointPlacement,
		        yAxis: 0,
		        color:cons_color,
		 };
	}
	
	device_unit_option_hourly.series.push(cons_series);
	cons_data=new Array();
	pointPlacement=point_place;
	
	//产量值
	snm=null;
	for (var i = 0; i < device_unit_output_hourly.length; i++) {
		if(snm!=null&&device_unit_output_hourly[i].snm!=snm){ 
			output_series={
			        name: device_unit_output_hourly[i-1].snm+'产量',
			        type: 'column',
			        data: output_data,
			        pointPadding: pointPadding2,
					pointPlacement: pointPlacement,
			        yAxis: 1,
			        color:product_color,
			 };
			device_unit_option_hourly.series.push(output_series);
			output_data=new Array();
			pointPlacement = pointPlacement+point_width;
		}
		output_data.push(device_unit_output_hourly[i].qty);
		snm=device_unit_output_hourly[i].snm;
	}
	output_series={
			name: device_unit_output_hourly[device_unit_output_hourly.length-1].snm+'产量',
			type: 'column',
			data: output_data,
			pointPadding: pointPadding2,
			pointPlacement: pointPlacement,
			yAxis: 1,
	        color:product_color,
	 };
	device_unit_option_hourly.series.push(output_series);
	output_data=new Array();
	
	//单耗值
	snm=null;
	for (var i = 0; i < device_unit_unit_cons_hourly.length; i++) {
		if(device_unit_unit_cons_hourly[i].e==energy) {
			if(snm!=null&&device_unit_unit_cons_hourly[i].lnm!=snm){ 
				unit_cons_series= {
				        name: device_unit_unit_cons_hourly[i-1].lnm+'单耗',
				        type: 'spline',
				        data: unit_cons_data,
				        yAxis: 2
				};
				device_unit_option_hourly.series.push(unit_cons_series);
				unit_cons_data=new Array();
			}
			unit_cons_data.push(device_unit_unit_cons_hourly[i].c);
			snm=device_unit_unit_cons_hourly[i].lnm;  
		}
	}
	if(device_unit_unit_cons_hourly.length>0) {
		unit_cons_series= {
				 name: device_unit_unit_cons_hourly[device_unit_unit_cons_hourly.length-1].lnm+'单耗',
		         type: 'spline',
			     data: unit_cons_data, 
			     yAxis: 2
		 };
	}
	
	device_unit_option_hourly.series.push(unit_cons_series);
	unit_cons_data=new Array();

	device_unit_chart_hourly = Highcharts.chart('device_unit_chart_hourly', device_unit_option_hourly);
	device_unit_hourly_chart_series=JSON.parse(JSON.stringify(device_unit_option_hourly.series));
	device_unit_hourly_chart_date=JSON.parse(JSON.stringify(device_unit_option_hourly.xAxis.categories));

	
	var str_data=new Set();
	for (var i = 0; i < device_unit_output_hourly.length; i++) {
		str_data.add(device_unit_output_hourly[i].snm);
	}
	var str_html='';
	for (let x of str_data) { // 遍历Set
		str_html=str_html+'<button class="layui-btn layui-btn-xs" style="background-color:#50e090;"id="button'+button_id+'" onclick="set_device_unit_hourly_select(&apos;'+x+'&apos;,'+button_id+')">'+x+'</button>';
		button_id++;
	}
	document.getElementById("device_unit_chart_device_unit_hourly").innerHTML=str_html;
	
	set_device_unit_hourly_select('产量',19);

}

function export_init(data){

	var point_data=new Array();
	var inst_point_data=new Array()
	for(h=0;h<1;h++) {
		for (var i = 0; i < factory_point_data.length; i++) {
			if(factory_point_data[i].en==energy&&department_list.indexOf(factory_point_data[i].d)!=-1) {
				point_data.push(factory_point_data[i])
			}
		}
		for (var i = 0; i < factory_inst_point_data.length; i++) {
			if(factory_inst_point_data[i].en==energy&&department_list.indexOf(factory_inst_point_data[i].d)!=-1) {
				inst_point_data.push(factory_inst_point_data[i])
			}
		}
		factory_point_list=JSON.parse(JSON.stringify(point_data));
		factory_inst_point_list=JSON.parse(JSON.stringify(inst_point_data));
		if(data=='department') {
			break;
		}
		point_data=new Array();
		inst_point_data=new Array()
		for (var i = 0; i < factory_point_list.length; i++) {
			if(product_line_list.indexOf(factory_point_list[i].ln)!=-1&&factory_point_list[i].ln!="") {
				point_data.push(factory_point_list[i])
			}
		}
		for (var i = 0; i < factory_inst_point_list.length; i++) {
			if(product_line_list.indexOf(factory_inst_point_list[i].ln)!=-1&&factory_inst_point_list[i].ln!="") {
				inst_point_data.push(factory_inst_point_list[i])
			}
		}
		factory_point_list=JSON.parse(JSON.stringify(point_data));
		factory_inst_point_list=JSON.parse(JSON.stringify(inst_point_data));
		if(data=='product_line') {
			break;
		}
		
		point_data=new Array();
		inst_point_data=new Array()
		for (var i = 0; i < factory_point_list.length; i++) {
			if(device_list.indexOf(factory_point_list[i].dv)!=-1&&factory_point_list[i].dv!="") {
				point_data.push(factory_point_list[i])
			}
		}
		for (var i = 0; i < factory_inst_point_list.length; i++) {
			if(device_list.indexOf(factory_inst_point_list[i].dv)!=-1&&factory_inst_point_list[i].dv!="") {
				inst_point_data.push(factory_inst_point_list[i])
			}
		}
		factory_point_list=JSON.parse(JSON.stringify(point_data));
		factory_inst_point_list=JSON.parse(JSON.stringify(inst_point_data));
		if(data=='device') {
			break; 
		}
		
		point_data=new Array();
		inst_point_data=new Array()
		for (var i = 0; i < factory_point_list.length; i++) {
			if(device_unit_list.indexOf(factory_point_list[i].un)!=-1&&factory_point_list[i].un!="") {
				point_data.push(factory_point_list[i])
			}
		}
		for (var i = 0; i < factory_inst_point_list.length; i++) {
			if(device_unit_list.indexOf(factory_inst_point_list[i].un)!=-1&&factory_inst_point_list[i].un!="") {
				inst_point_data.push(factory_inst_point_list[i])
			}
		}
		factory_point_list=JSON.parse(JSON.stringify(point_data));
		factory_inst_point_list=JSON.parse(JSON.stringify(inst_point_data));
		if(data=='device_unit') {
			break;
		}
		
	}
	factory_points_grid.option("dataSource", factory_point_list);
	factory_inst_points_grid.option("dataSource", factory_inst_point_list);
}

function set_export_cons(data) {
	var series = new Array();
	var date = new Array();
	if(data=='department') {
		series = department_chart_series;
		date=department_chart_date;

	}else if(data=='product_line') {
		series =  product_line_chart_series;
		date = product_line_chart_date;

	}else if(data=='product_line_hourly') {
		series =  product_line_hourly_chart_series;
		date=product_line_hourly_chart_date;

	}else if(data=='device') {
		series =  device_chart_series;
		date=device_chart_date;

	}else if(data=='device_hourly') {
		series =  device_hourly_chart_series;
		date=device_hourly_chart_date;
		
	}else if(data=='device_unit') {
		series =  device_unit_chart_series;
		date=device_unit_chart_date;

	}else if(data=='device_unit_hourly') {
		series =  device_unit_hourly_chart_series;
		date=device_unit_hourly_chart_date;

	}
	var str = "";
	str += (',时间,');
	for (var j = 0; j < date.length; j++) {
		str += (',' + date[j] + ',');
	}
	
	str += '\r\n';
	
	for (var i = 0; i < series.length; i++) {
		str += (',' + series[i].name + ',');
		for (var j = 0; j < series[i].data.length; j++) {
			str += (',' + series[i].data[j] + ',');
		}
		str += '\r\n';
	}

    let uri = 'data:text/csv;charset=utf-8,\ufeff' + encodeURIComponent(str);
    
    var link = document.createElement("a"); //通过创建a标签实现
    link.href = uri;

    link.download = "table.csv"; //下载文件名
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
}


function get_department_cons() {
	if(!ajax_json){
    	factory_cons5_ajax_url=path + "/gynh/factory_chart_view/get_department_cons.do";
	}
	$.ajax({
		url: factory_cons5_ajax_url,
//		url: path + "/gynh/factory_chart_view/get_department_cons.do",
		type: 'post',
		async: false,
		data: {
			start_date: start_date,
			end_date: end_date,
			factory_list: factory_list, 
			department_list: department_list, 
		},
		success: function (data) {
			var jsonStr = JSON.stringify(data,null,4);
        	console.log(jsonStr);
			department_cons=data['data'];
		},
		error: function () {
        	console.log('error');
		}
	});
}

function get_product_line_cons() {
	if(!ajax_json){
    	factory_cons6_ajax_url=path + "/gynh/factory_chart_view/get_product_line_cons.do";
	}
	$.ajax({
		url: factory_cons6_ajax_url,
//		url: path + "/gynh/factory_chart_view/get_product_line_cons.do",
		type: 'post',
		async: false,
		data: {
			start_date: start_date,
			end_date: end_date,
			factory_list: factory_list, 
			department_list: department_list, 
			product_line_list: product_line_list, 
		},
		success: function (data) {
			var jsonStr = JSON.stringify(data,null,4);
        	console.log(jsonStr);
			product_line_cons=data['data'];
		},
		error: function () {
        	console.log('error');
		}
	});
}

function get_product_line_cons_hourly(date) {
	if(!ajax_json){
    	factory_cons7_ajax_url=path + "/gynh/factory_chart_view/get_product_line_cons_hourly.do";
	}
	$.ajax({
		url: factory_cons7_ajax_url,
//		url: path + "/gynh/factory_chart_view/get_product_line_cons_hourly.do",
		type: 'post',
		async: false,
		data: {
			date: date,
			factory_list: factory_list, 
			department_list: department_list, 
			product_line_list: product_line_list, 
		},
		success: function (data) {
			var jsonStr = JSON.stringify(data,null,4);
        	console.log(jsonStr);
			product_line_cons_hourly=data['data'];
		},
		error: function () {
        	console.log('error');
		}
	});
}

function get_device_cons_hourly(date) {
	if(!ajax_json){
    	factory_cons8_ajax_url=path + "/gynh/factory_chart_view/get_device_cons_hourly.do";
	}
	$.ajax({
		url: factory_cons8_ajax_url,
//		url: path + "/gynh/factory_chart_view/get_device_cons_hourly.do",
		type: 'post',
		async: false,
		data: {
			date: date,
			factory_list: factory_list, 
			department_list: department_list, 
			product_line_list: product_line_lists, 
			device_list: device_lists, 
		},
		success: function (data) {
			var jsonStr = JSON.stringify(data,null,4);
        	console.log(jsonStr);
			device_cons_hourly=data['data'];
		},
		error: function () {
        	console.log('error');
		}
	});
}

function get_device_unit_cons_hourly(date) {
	if(!ajax_json){
    	factory_cons9_ajax_url=path + "/gynh/factory_chart_view/get_device_unit_cons_hourly.do";
	}
	$.ajax({
		url: factory_cons9_ajax_url,
//		url: path + "/gynh/factory_chart_view/get_device_unit_cons_hourly.do",
		type: 'post',
		async: false,
		data: {
			date: date,
			factory_list: factory_list, 
			department_list: department_list, 
			product_line_list: product_line_listss, 
			device_list: device_listss,
			device_unit_list: device_unit_listss, 
		},
		success: function (data) {
			var jsonStr = JSON.stringify(data,null,4);
        	console.log(jsonStr);
			device_unit_cons_hourly=data['data'];
		},
		error: function () {
        	console.log('error');
		}
	});
}

function get_device_cons() {
	if(!ajax_json){
    	factory_cons10_ajax_url=path + "/gynh/factory_chart_view/get_device_cons.do";
	}			
	$.ajax({
		url: factory_cons10_ajax_url,
//		url: path + "/gynh/factory_chart_view/get_device_cons.do",
		type: 'post',
		async: false,
		data: {
			start_date: start_date,
			end_date: end_date,
			factory_list: factory_list, 
			department_list: department_list, 
			product_line_list: product_line_lists, 
			device_list: device_lists, 
		},
		success: function (data) {
			var jsonStr = JSON.stringify(data,null,4);
        	console.log(jsonStr);
			device_cons=data['data'];
		},
		error: function () {
        	console.log('error');
		}
	});
}

function get_device_unit_cons() {
	if(!ajax_json){
    	factory_cons11_ajax_url=path + "/gynh/factory_chart_view/get_device_unit_cons.do";
	}	
	$.ajax({
		url: factory_cons11_ajax_url,
//		url: path + "/gynh/factory_chart_view/get_device_unit_cons.do",
		type: 'post',
		async: false,
		data: {
			start_date: start_date,
			end_date: end_date,
			factory_list: factory_list, 
			department_list: department_list, 
			product_line_list: product_line_listss, 
			device_list: device_listss, 
			device_unit_list: device_unit_listss, 
		},
		success: function (data) {
			var jsonStr = JSON.stringify(data,null,4);
        	console.log(jsonStr);
			device_unit_cons=data['data'];
		},
		error: function () {
        	console.log('error');
		}
	});
}

function get_product_line_output() {
	// 结束时间的后一天的0点
	var datetime = new Date(end_date + " 00:00:00");
	datetime = new Date(datetime.getTime()+3600000*24);
	var formated_date_time = get_formated_date_time(datetime);
	if(!ajax_json){
    	factory_cons12_ajax_url=path + "/gynh/factory_chart_view/get_product_line_output.do";
	}	
	$.ajax({
		url: factory_cons12_ajax_url,
//		url: path + "/gynh/factory_chart_view/get_product_line_output.do",
		type: 'post',
		async: false,
		data: {
			start_time: start_date + " 00:00:00",
			end_time: formated_date_time,
			product_line_list: product_line_list, 
		},
		success: function (data) {
			var jsonStr = JSON.stringify(data,null,4);
        	console.log(jsonStr);
	
			var datas=data['data'];
			var date=start_date;
			var date_array= new Array();
			var str= product_line_data.split("--");
			str=str.sort();
			product_line_output=new Array();
			
			while(true) { 
				date_array.push(date);
				if(date==end_date){
					break;
				}
				date=getFormatDate2(date,1);
			}
			
			for (var i = 1; i < str.length; i++) {
				for (var j = 0; j < date_array.length; j++) {
					product_line_output.push(
						{
							node:str[i],
							snm:str[i],
							t:date_array[j],
							qty:0
						}
					);
				}
			}
			for (var i = 0; i<product_line_output.length; i++) {
				for (var j = 0; j < datas.length; j++) {
					if(product_line_output[i].snm==datas[j].snm&&product_line_output[i].t==datas[j].t) {
						product_line_output[i]=datas[j];
						break;
					}
				
				}
			}
			
		},
		error: function () {
        	console.log('error');
		}
	});
}

function get_product_line_output_hourly(date) {
	// 结束时间的后一天的0点
	var datetime = new Date(date + " 00:00:00");
	datetime = new Date(datetime.getTime()+3600000*24);
	var formated_date_time = get_formated_date_time(datetime);
	if(!ajax_json){
    	factory_cons13_ajax_url=path + "/gynh/factory_chart_view/get_product_line_output_hourly.do";
	}	
	$.ajax({
		url: factory_cons13_ajax_url,
//		url: path + "/gynh/factory_chart_view/get_product_line_output_hourly.do",
		type: 'post',
		async: false,
		data: {
			start_time: date + " 00:00:00",
			end_time: formated_date_time,
			product_line_list: product_line_list, 
		},
		success: function (data) {
			var jsonStr = JSON.stringify(data,null,4);
        	console.log(jsonStr);
			product_line_output_hourly=new Array();
			var str= product_line_data.split("--");
			str=str.sort();
			var h=0;
			var s=1;
			for (var i = 0; i < (str.length-1)*24; i++) {
				if(h==24){
					h=0;
					s++;
				}
				product_line_output_hourly.push(
							{
								node:str[s],
								snm:str[s],
								t:h,
								qty:0
							}
						);
				h++;
			}
			var datas=data['data'];
			
			for (var i = 0; i<product_line_output_hourly.length; i++) {
				for (var j = 0; j < datas.length; j++) {
					if(product_line_output_hourly[i].snm==datas[j].snm&&product_line_output_hourly[i].t==datas[j].t) {
						product_line_output_hourly[i]=datas[j];
						break;
					}
				
				}
			}
			
		},
		error: function () {
        	console.log('error');
		}
	});
}


function get_product_line_unit_cons() {
	// 结束时间的后一天的0点
	var datetime = new Date(end_date + " 00:00:00");
	datetime = new Date(datetime.getTime()+3600000*24);
	var formated_date_time = get_formated_date_time(datetime);
	if(!ajax_json){
    	factory_cons14_ajax_url=path + "/gynh/factory_chart_view/get_product_line_unit_cons.do";
	}	
	$.ajax({
		url: factory_cons14_ajax_url,
//		url: path + "/gynh/factory_chart_view/get_product_line_unit_cons.do",
		type: 'post',
		async: false,
		data: {
			cons_start_date: start_date,
			cons_end_date: end_date,
			output_start_time: start_date + " 00:00:00",
			output_end_time: formated_date_time,
			factory_list: factory_list, 
			department_list: department_list, 
			product_line_list: product_line_list, 
		},
		success: function (data) {
			var jsonStr = JSON.stringify(data,null,4);
        	console.log(jsonStr);
			product_line_unit_cons=data['data'];
		},
		error: function () {
        	console.log('error');
		}
	});
}

function get_product_line_unit_cons_hourly(date) {
	// 结束时间的后一天的0点
	var datetime = new Date(date + " 00:00:00");
	datetime = new Date(datetime.getTime()+3600000*24);
	var formated_date_time = get_formated_date_time(datetime);
	if(!ajax_json){
    	factory_cons15_ajax_url=path + "/gynh/factory_chart_view/get_product_line_unit_cons_hourly.do";
	}	
	$.ajax({
		url: factory_cons15_ajax_url,
//		url: path + "/gynh/factory_chart_view/get_product_line_unit_cons_hourly.do",
		type: 'post',
		async: false,
		data: {
			cons_date: date,
			output_start_time: date + " 00:00:00",
			output_end_time: formated_date_time,
			factory_list: factory_list, 
			department_list: department_list, 
			product_line_list: product_line_list, 
		},
		success: function (data) {
			var jsonStr = JSON.stringify(data,null,4);
        	console.log(jsonStr);
			product_line_unit_cons_hourly=data['data'];
		},
		error: function () {
        	console.log('error');
		}
	});
}

function get_product_line_state_time() {
	// 结束时间的后一天的0点
	var datetime = new Date(end_date + " 00:00:00");
	datetime = new Date(datetime.getTime()+3600000*24);
	var formated_date_time = get_formated_date_time(datetime);
	if(!ajax_json){
    	factory_cons16_ajax_url=path + "/gynh/factory_chart_view/get_product_line_state_time.do";
	}	
	$.ajax({
		url: factory_cons16_ajax_url,
//		url: path + "/gynh/factory_chart_view/get_product_line_state_time.do",
		type: 'post',
		async: false,
		data: {
			start_time: start_date + " 00:00:00",
			end_time: formated_date_time,
			product_line_list: product_line_list, 
		},
		success: function (data) {
			var jsonStr = JSON.stringify(data,null,4);
        	console.log(jsonStr);
			
			var datas=data['data'];
			var date=start_date;
			var date_array= new Array();
			var str= product_line_data.split("--");
			str=str.sort();
			product_line_state_time=new Array();
			
			while(true) { 
				date_array.push(date);
				if(date==end_date){
					break;
				}
				date=getFormatDate2(date,1);
			}
			
			for (var i = 1; i < str.length; i++) {
				for (var j = 0; j < date_array.length; j++) {
					product_line_state_time.push(
						{
							node:str[i],
							snm:str[i],
							t:date_array[j],
							qty:0
						}
					);
				}
			}
			for (var i = 0; i<product_line_state_time.length; i++) {
				for (var j = 0; j < datas.length; j++) {
					if(product_line_state_time[i].snm==datas[j].snm&&product_line_state_time[i].t==datas[j].t) {
						product_line_state_time[i]=datas[j];
						break;
					}
				
				}
			}
		},
		error: function () {
        	console.log('error');
		}
	});
}

var windowHeight = 0;
var windowWidth = 0;
var upPanelPersent = 0.5;
var leftPanelPersent = 0.5;
var bHSplit = true; //true:H横切（默认）， false:V纵切
var factory_points_grid;
var factory_inst_points_grid;
var points_echarts="factory_points_echarts";

var factory_point_list;
var factory_inst_point_list;

var points_values = new Array();
var inst_points_values = new Array();

var point_value_cache = {};
var inst_point_value_cache = {};
var sel_rows = new Array();
var inst_sel_rows = new Array();

var minute=10;
var total_points_value;

var longtabs = [
    { idx:0, z:11, id: "factory_points_grid", 	text: "分厂累计量点位",chart:"factory_points_echarts"},
    { idx:1, z:12, id: "factory_inst_points_grid", 	text: "分厂瞬时量点位",chart:"factory_inst_points_echarts"},
];

$(document).ready(function() {
	get_department_list();
	$("#factory_points_button").css("background-color","#9aa09d");
	$("#factory_inst_points_button").css("background-color","#009688");
	$("#factory_points_button").click(function(){
		var current_z_index = $("#factory_points_grid").css("z-index");
		if (current_z_index != 50) {
			for (var i = 0; i < longtabs.length; i++) {
				$("#"+longtabs[i].id).css("z-index", longtabs[i].z);
				$("#"+longtabs[i].chart).css("z-index", longtabs[i].z);
			}
			$("#factory_points_grid").css("z-index", 50);
			$("#factory_points_echarts").css("z-index", 50);
			points_echarts="factory_points_echarts";
		}
		$("#factory_points_button").css("background-color","#9aa09d");
		$("#factory_inst_points_button").css("background-color","#009688");
	})
	$("#begin_hour_add").click(function(){
		init_begin_date=getTimeDate_hour(init_begin_date,1);
		document.getElementById('beginTimeBox').value =init_begin_date ;
	})
	$("#begin_hour_minus").click(function(){
		init_begin_date=getTimeDate_hour(init_begin_date,-1);
		document.getElementById('beginTimeBox').value =init_begin_date ;
	})
	$("#end_hour_add").click(function(){
		init_end_date=getTimeDate_hour(init_end_date,1);
		document.getElementById('endTimeBox').value =init_end_date ;
	})
	$("#end_hour_minus").click(function(){
		init_end_date=getTimeDate_hour(init_end_date,-1);
		document.getElementById('endTimeBox').value =init_end_date ;
	})

	$("#factory_inst_points_button").click(function(){
		var current_z_index = $("#factory_inst_points_grid").css("z-index");
		if (current_z_index != 50) {
			for (var i = 0; i < longtabs.length; i++) {
				$("#"+longtabs[i].id).css("z-index", longtabs[i].z);
				$("#"+longtabs[i].chart).css("z-index", longtabs[i].z);
			}
			$("#factory_inst_points_grid").css("z-index", 50);
			$("#factory_inst_points_echarts").css("z-index", 50);
			points_echarts="factory_inst_points_echarts";
		}
		$("#factory_points_button").css("background-color","#009688");
		$("#factory_inst_points_button").css("background-color","#9aa09d");
	})
});


function getTimeDate_hour(data_time,delta_hour) {
	var datetime = new Date(data_time);
	var times = datetime.getTime()+3600000*delta_hour;

	var datetime = new Date(times);
	var year = datetime.getFullYear(); //得到年份
	var month = datetime.getMonth();//得到月份
	var date = datetime.getDate();//得到日期
	var day = datetime.getDay();//得到周几
	var hour = datetime.getHours();//得到小时
	var minu = datetime.getMinutes();//得到分钟
	var sec = datetime.getSeconds();//得到秒
	month = month + 1;
	if (month < 10) month = "0" + month;
	if (date < 10) date = "0" + date;
	if (hour < 10) hour = "0" + hour;
	if (minu < 10) minu = "0" + minu;
	if (sec < 10) sec = "0" + sec;
	var time = year + "-" + month + "-" + date+ " " + hour + ":" + minu + ":" + sec;
	return time;
}



function on_resize() {
	var window_height = $(window).height();
	var window_width = $(window).width();
	
   	$("#longtabs").css("width", window_width-880+'px');
   	
   	var chart_div = document.getElementsByClassName('chart_div');
   	var footer = document.getElementsByClassName('footer');
   	
   	for (var i = 0; i < longtabs.length; i++) { 
		$("#"+longtabs[i].id).css("width",footer[0].offsetWidth/2-42+'px');
		$("#"+longtabs[i].id).css("height",footer[0].offsetHeight-36+'px');
	}
	
   	document.getElementById('department_box').style.width = chart_div[0].offsetWidth-186+'px';
   	document.getElementById('line_box').style.width = chart_div[0].offsetWidth-186+'px';
	document.getElementById('device_box').style.width = chart_div[0].offsetWidth-186+'px';
	document.getElementById('device_unit_box').style.width = chart_div[0].offsetWidth-186+'px';
	
   	document.getElementById('factory_points_echarts').style.width = footer[0].offsetWidth/2-40+'px';
	document.getElementById('factory_points_echarts').style.height = footer[0].offsetHeight-30+'px';
	document.getElementById('factory_inst_points_echarts').style.width = footer[0].offsetWidth/2-40+'px';
	document.getElementById('factory_inst_points_echarts').style.height = footer[0].offsetHeight-30+'px';
	 
	document.getElementById('left_div').style.marginTop = footer[0].offsetHeight-94 + 'px';
	document.getElementById('right_div').style.marginTop = footer[0].offsetHeight-94 + 'px';
	document.getElementById('right_div').style.marginLeft = footer[0].offsetWidth/2-196+'px';
	
	factory_points_echarts.resize();
	factory_inst_points_echarts.resize(); 
}

$(function() {
	on_resize();
  	window.onresize =function() {
  		on_resize();   
   	}	 
	document.getElementById('beginTimeBox').value =init_begin_date ;
	document.getElementById('endTimeBox').value =init_end_date;
	init();
	
	var tabsInstance = $("#longtabs > .tabs-container").dxTabs({
		dataSource: longtabs,
	    selectedIndex: 0,
	    onItemClick: function(e) {
	        var current_z_index = $("#"+e.itemData.id).css("z-index");
	        if (current_z_index != 50) {
	        	for (var i = 0; i < longtabs.length; i++) {
        			$("#"+longtabs[i].id).css("z-index", longtabs[i].z);
        			$("#"+longtabs[i].chart).css("z-index", longtabs[i].z);
        		}
        		$("#"+e.itemData.id).css("z-index", 50);
        		$("#"+e.itemData.chart).css("z-index", 50);
        		points_echarts=e.itemData.chart;
        	}
        }
    }).dxTabs("instance");
	
    $("#"+longtabs[0].chart).css("z-index", 50);
    $("#"+longtabs[0].id).css("z-index", 50);
    
    for (var i = 0; i < 21; i++) {
		$("#button"+i).css("background-color","#50e090");
	}
});

function get_total_points_value() {
	var time =getTimeDate_8();
	var index = top.layer.msg('查询中...',{icon: 16, skin:'', shade:0.1});
	if(!ajax_json){
    	export1_ajax_url=path + "/gynh/get_points_value.do";
    }
	$.ajax({
//		url: path + "/gynh/get_points_value.do",
		url: export1_ajax_url,

		type: 'post',
		async: true,                     
		data: {
	    	time: time    
	    },
        success: function (data) {
        	var jsonStr = JSON.stringify(data,null,4);
        	console.log(jsonStr);
         	total_points_value = data['data'];
        	match_point_and_value();
        	factory_points_grid.option("dataSource", factory_point_list);
        	top.layer.close(index);
        },
        error: function () {
        	alert('error');
        }
    });
}         

function getTimeDate_8() {
	var times = new Date().getTime()-60000*minute-3600000*8;

	//var times = new Date().getTime()-3600000*(minute+8);
	var datetime = new Date(times);
	var year = datetime.getFullYear(); //得到年份
	var month = datetime.getMonth();//得到月份
	var date = datetime.getDate();//得到日期
	var day = datetime.getDay();//得到周几
	var hour = datetime.getHours();//得到小时
	var minu = datetime.getMinutes();//得到分钟
	var sec = datetime.getSeconds();//得到秒
	month = month + 1;
	if (month < 10) month = "0" + month;
	if (date < 10) date = "0" + date;
	if (hour < 10) hour = "0" + hour;
	if (minu < 10) minu = "0" + minu;
	if (sec < 10) sec = "0" + sec;
	var time = year + "-" + month + "-" + date+ " " + hour + ":" + minu + ":" + sec;
	return time;
}

function match_point_and_value() {
	var j = 0;
	
	for (var i = 0; i < factory_point_list.length; i++) {
		for (; j < total_points_value.length; j++) {
			if (factory_point_list[i].pn == total_points_value[j].p) {
				factory_point_list[i].t = total_points_value[j].t;
				factory_point_list[i].v = total_points_value[j].v;
				j++;
				break;
			} else if (factory_point_list[i].pn < total_points_value[j].p) {
				break;
			}
		}
	}
}

function init(){
	if(!ajax_json){
    	export2_ajax_url=path + "/gynh/get_export_list.do";
    }
	
	$.ajax({
//		url: path + "/gynh/get_export_list.do",
		url: export2_ajax_url,
		type: 'post',
		async: false,
		data: {param1:param1},
        success: function (data) {
        	var jsonStr = JSON.stringify(data,null,4);
        	console.log(jsonStr);
        	factory_point_list = data['factory_point_list'];
        	factory_inst_point_list = data['factory_inst_point_list'];
        },
        error: function () {
        	alert('error');
        }
    });
	//factory_points_grid
	factory_points_grid=$("#factory_points_grid").dxDataGrid({
		dataSource: null,
	    showBorders: true,
	    showColumnLines: true,
	    allowColumnReordering: true,
	    allowColumnResizing: true,
	    showRowLines: true,
	    rowAlternationEnabled: true,
	    columnResizingMode: "widget",
	    columnMinWidth: 20,
//      filterRow: { visible: true },
	    headerFilter: { visible: true },
	    paging: {
	        enabled: false
	    },
	    scrolling: {
	        mode: "virtual"
	    },
	    columnFixing: { 
	        enabled: true
	    },
	    searchPanel: {
	        visible: true,
	        width: 240,
	        placeholder: "搜索..."
	    },
	    columnChooser: {
	        enabled: true,
	        mode: "select"
	    },   
	    selection: {
            mode: "multiple",
            allowSelectAll: false,
            showCheckBoxesMode:'none',
        },
	    columns: [ 
	    	{
	            dataField: "pn",
	            validationRules: [{ type: "required" }, {
	                type: "pattern",
	            }],
	            caption: "点位编号"
	        },
//	    	{
//	            dataField: "pnm",
//	            validationRules: [{ type: "required" }, {
//	                type: "pattern",
//	            }],
//	            caption: "点位名称"
//	        },
	    	{
	            dataField: "en",
	            validationRules: [{ type: "required" }, {
	                type: "pattern",
	            }],
	            caption: "能源"
	        },
//	    	{
//	            dataField: "f",
//	            validationRules: [{ type: "required" }, {
//	                type: "pattern",
//	            }],
//	            caption: "分厂"
//	        },
//	    	{
//	            dataField: "d",
//	            validationRules: [{ type: "required" }, {
//	                type: "pattern",
//	            }],
//	            caption: "科室"
//	        },
	    	{
	            dataField: "ln",
	            validationRules: [{ type: "required" }, {
	                type: "pattern",
	            }],
	            caption: "产线"
	        },
	    	{
	            dataField: "dv",
	            validationRules: [{ type: "required" }, {
	                type: "pattern",
	            }],
	            caption: "设备"
	        },
	    	{
	            dataField: "un",
	            validationRules: [{ type: "required" }, {
	                type: "pattern",
	            }],
	            caption: "单元"
	        },
	        {
                dataField: "t",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: "时间戳"
            },
            {
                dataField: "v",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: "最新值"
            }
	    ]
	}).dxDataGrid("instance");
	
	factory_point_data=JSON.parse(JSON.stringify(factory_point_list));
	factory_point_list=new Array();
	
	for (var i = 0; i < factory_point_data.length; i++) {
		if(factory_point_data[i].en==energy) {
			factory_point_list.push(factory_point_data[i])
		}
	}
	
	factory_points_grid.option("dataSource", factory_point_list);
	
	//factory_inst_points_grid
	factory_inst_points_grid=$("#factory_inst_points_grid").dxDataGrid({
		dataSource: null,
	    showBorders: true,
	    showColumnLines: true,
	    allowColumnReordering: true,
	    allowColumnResizing: true,
	    showRowLines: true,
	    rowAlternationEnabled: true,
	    columnResizingMode: "widget",
	    columnMinWidth: 20,
//	    filterRow: { visible: true },
	    headerFilter: { visible: true },
	    paging: {
	        enabled: false
	    },
	    scrolling: {
	        mode: "virtual"
	    },
	    columnFixing: { 
	        enabled: true
	    },
	    searchPanel: {
	        visible: true,
	        width: 240,
	        placeholder: "搜索..."
	    },
	    columnChooser: {
	        enabled: true,
	        mode: "select"
	    },   
	    selection: {
            mode: "multiple",
            allowSelectAll: false,
            showCheckBoxesMode:'none',
        },
	    columns: [ 
	    	{
	            dataField: "pn",
	            validationRules: [{ type: "required" }, {
	                type: "pattern",
	            }],
	            caption: "点位编号"
	        },
//	    	{
//	            dataField: "pnm",
//	            validationRules: [{ type: "required" }, {
//	                type: "pattern",
//	            }],
//	            caption: "点位名称"
//	        },
	    	{
	            dataField: "en",
	            validationRules: [{ type: "required" }, {
	                type: "pattern",
	            }],
	            caption: "能源"
	        },
//	    	{
//	            dataField: "f",
//	            validationRules: [{ type: "required" }, {
//	                type: "pattern",
//	            }],
//	            caption: "工厂"
//	        },
//	    	{
//	            dataField: "d",
//	            validationRules: [{ type: "required" }, {
//	                type: "pattern",
//	            }],
//	            caption: "部门"
//	        },
	    	{
	            dataField: "ln",
	            validationRules: [{ type: "required" }, {
	                type: "pattern",
	            }],
	            caption: "产线"
	        },
	    	{
	            dataField: "dv",
	            validationRules: [{ type: "required" }, {
	                type: "pattern",
	            }],
	            caption: "设备"
	        },
	    	{
	            dataField: "un",
	            validationRules: [{ type: "required" }, {
	                type: "pattern",
	            }],
	            caption: "单元"
	        },
	    ]
	}).dxDataGrid("instance");
	
	factory_inst_point_data=JSON.parse(JSON.stringify(factory_inst_point_list));
	factory_inst_point_list=new Array();
	for (var i = 0; i < factory_inst_point_data.length; i++) {
		if(factory_inst_point_data[i].en==energy) {
			factory_inst_point_list.push(factory_inst_point_data[i])
		}
	}
	factory_inst_points_grid.option("dataSource", factory_inst_point_list);
}


function get_echarts_value(){
	if (points_echarts=="factory_points_echarts") {
		get_value("chart");
	}
	if (points_echarts=="factory_inst_points_echarts") {
		get_inst_value("chart");
	}
}

function get_points_value() {
	if (points_echarts=="factory_points_echarts") {
		get_value("export");
	}
	if (points_echarts=="factory_inst_points_echarts") {
		get_inst_value("export");
	}
}

function get_value(action) {
	for (var i = 0; i < sel_rows.length; i++) {
		if (typeof(sel_rows[i].series) != "undefined") {
			delete sel_rows[i].series;
		}
	}
	sel_rows.splice(0,sel_rows.length);
	
	sel_rows =JSON.parse(JSON.stringify(factory_points_grid.getSelectedRowsData()));
//	var point_set = new Set();
//	for (var i = 0; i < sel_rows.length; i++) {
//		point_set.add(sel_rows[i].pn);
//		// 优先从缓存中找
//		if (typeof(point_value_cache[sel_rows[i].pn]) != "undefined") {
//			sel_rows[i].series = point_value_cache[sel_rows[i].pn];
//		}
//	}
//
//	$.each(point_value_cache, function(_key) {
//	    var key = _key;
//	    var value = point_value_cache[_key];
//	    if (!point_set.has(_key)) {
//	    	delete point_value_cache[_key];
//	    }
//	});

	var begin_time = bjtime_to_gmt(init_begin_date);
	var end_time = bjtime_to_gmt(init_end_date);
	var index = top.layer.msg('查询中...',{icon: 16, skin:'', shade:0.1});
	$.ajax({
	    async: true,
        success: function (data) {
			for (var p = 0; p < sel_rows.length; p++) {
				if (typeof(sel_rows[p].series) != "undefined") {
					continue;
				}
				var point_number = sel_rows[p].pn;
				if(!ajax_json){
			    	export3_ajax_url=path + '/gynh/get_points_value_by_point_number.do';
			    }
				
			    $.ajax({
//			        url: path + '/gynh/get_points_value_by_point_number.do',
			        url: export3_ajax_url,
			        type: 'post',
			        async: false,
			        data: {
			        	begin_time: begin_time, 
			        	end_time: end_time, 
			        	point_number: point_number
			        },
			        success: function (data) {
			        	var jsonStr = JSON.stringify(data,null,4);
			        	console.log(jsonStr);
			            sel_rows[p].series = data['point_values'];
			            point_value_cache[point_number] = data['point_values'];
			        },
			        error: function () {
			        }
			    });
			}
			if (action=="chart") {
				set_echarts(sel_rows);
			}
			if (action=="export") {
				set_export(sel_rows);	
			}

			top.layer.close(index);
	    },  
	});
}

function get_inst_value(action){
	for (var i = 0; i < inst_sel_rows.length; i++) {
		if (typeof(inst_sel_rows[i].series) != "undefined") {
			delete inst_sel_rows[i].series;
		}
	}
	inst_sel_rows.splice(0, inst_sel_rows.length);
	inst_sel_rows =JSON.parse(JSON.stringify(factory_inst_points_grid.getSelectedRowsData()));
	
//	var point_set = new Set();
//	for (var i = 0; i < inst_sel_rows.length; i++) {
//		point_set.add(inst_sel_rows[i].pn);
//		// 优先从缓存中找
////		if (typeof(inst_point_value_cache[sel_rows[i].pn]) != "undefined") {
////			inst_sel_rows[i].series = inst_point_value_cache[sel_rows[i].pn];
////		}
//	}
//
//	$.each(inst_point_value_cache, function(_key) {
//	    var key = _key;
//	    var value = inst_point_value_cache[_key];
//	    if (!point_set.has(_key)) {
//	    	delete inst_point_value_cache[_key];
//	    }
//	});
	
	var begin_time = bjtime_to_gmt(init_begin_date);
	var end_time = bjtime_to_gmt(init_end_date);
	var index = top.layer.msg('查询中...',{icon: 16, skin:'', shade:0.1});
	$.ajax({
	    async: true,
        success: function (data) {
			for (var p = 0; p < inst_sel_rows.length; p++) {
				if (typeof(inst_sel_rows[p].series) != "undefined") {
					continue;
				}
				var point_number = inst_sel_rows[p].pn;
				if(!ajax_json){
			    	export4_ajax_url=path + '/gynh/get_inst_points_value_by_point_number.do';
			    }
			    $.ajax({
//			        url: path + '/gynh/get_inst_points_value_by_point_number.do',
			        url:export4_ajax_url ,
			        type: 'post',
			        async: false,
			        data: {
			        	begin_time: begin_time, 
			        	end_time: end_time, 
			        	point_number: point_number
			        },
			        success: function (data) {
			        	var jsonStr = JSON.stringify(data,null,4);
			        	console.log(jsonStr);
			        	inst_sel_rows[p].series = data['inst_point_values'];
			        	//inst_point_value_cache[point_number] = data['inst_point_values'];
			        },
			        error: function () {
			        }
			    });
			}
			if (action=="chart") {
				set_echarts(inst_sel_rows);
			}
			if (action=="export") {
				set_export(inst_sel_rows);
			}
			
			top.layer.close(index);
	    },  
	});
}

function set_echarts(sel_row) {
	var data_series = new Array();
	var colors = new Array();
	colors.push('#AE0000');
	colors.push('#FF44FF');
	colors.push('#0066CC');
	colors.push('#548C00');
	colors.push('#707038');
	colors.push('#5151A2');
	var min=sel_row[0].series[0].v;
	for (var p = 0; p < sel_row.length; p++) {
		var data_point = new Array();
		var data_value=new Array();
		var temp = new Array();
		
		for (var s = 0; s < sel_row[p].series.length; s++) {
			temp = [];
			temp.push(sel_row[p].series[s].t);
			temp.push(sel_row[p].series[s].v);
			if (min>sel_row[p].series[s].v) {
				min=sel_row[p].series[s].v;
			}
			data_point.push({value:temp});
		}
		for (var i=0;i<data_point.length-1;i++) {
			if(data_point[i].value[0]!=data_point[i+1].value[0]) {
				data_value.push(data_point[i]);
			}
		}
		data_value.push(data_point[data_point.length]);
		var c = p%6;
		data_series.push(
			{
				name:sel_row[p].pn,
				type: 'line',
				color: colors[c],
				showSymbol: false,
				hoverAnimation: false,
				data:data_value
			}
		);
	}
	min=min-1;
	if (min<0) {
		min=0;
	}
	if (points_echarts=="factory_points_echarts") {
		factory_points_option.series = data_series;
		factory_points_option.yAxis.min=min;
		
		factory_points_echarts.setOption(factory_points_option, true);
	}
	if (points_echarts=="factory_inst_points_echarts") {
		factory_inst_points_option.series = data_series;
		factory_inst_points_option.yAxis.min=min;
		factory_inst_points_echarts.setOption(factory_inst_points_option, true);
	}
}

function set_export(sel_row) {
	var str = "";
	var max_series = 0;
	for (var p = 0; p < sel_row.length; p++) {
		max_series = Math.max(sel_row[p].series.length, max_series);
	}

	for (var p = 0; p < sel_row.length; p++) {
		str += (',' + sel_row[p].pn + ',');
	}
	str += '\r\n';
	for (var p = 0; p < sel_row.length; p++) {
		str += (',' + sel_row[p].en + ',');
	}
	str += '\r\n';
	for (var p = 0; p < sel_row.length; p++) {
		str += (',' + sel_row[p].f + ',');
	}
	str += '\r\n';
	for (var p = 0; p < sel_row.length; p++) {
		str += (',' + sel_row[p].d + ',');
	}
	str += '\r\n';
	for (var p = 0; p < sel_row.length; p++) {
		str += (',' + sel_row[p].ln + ',');
	}
	str += '\r\n';
	for (var p = 0; p < sel_row.length; p++) {
		str += (',' + sel_row[p].dv + ',');
	}
	str += '\r\n';
	for (var p = 0; p < sel_row.length; p++) {
		str += (',' + sel_row[p].un + ',');
	}
	str += '\r\n';
	for (var p = 0; p < sel_row.length; p++) {
		str += (',' + sel_row[p].pnm + ',');
	}
	str += '\r\n';
	
	for (var s = 0; s < max_series; s++) {
		for (var p = 0; p < sel_row.length; p++) {
			if (s < sel_row[p].series.length) {
				str += (' ' + sel_row[p].series[s].t + ',' + sel_row[p].series[s].v + ',');
			} else {
				str += (',,');
			}
		}
		str += '\r\n';
	}
	
    let uri = 'data:text/csv;charset=utf-8,\ufeff' + encodeURIComponent(str);
    
    var link = document.createElement("a"); //通过创建a标签实现
    link.href = uri;

    link.download = "table.csv"; //下载文件名
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
}




