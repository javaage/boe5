var quota_sava=new Array();
var window_width;
var window_height;
var change=false;
var year;

$(function(){
	window.onresize = function() {
		on_resize();
   	}
    function logEvent(eventName) {
        var logList = $("#events ul"),
            newItem = $("<li>", { text: eventName });
        logList.prepend(newItem);
    }
    on_resize();
    year= new Date().getFullYear();
    $("#year").html(year);
    ajaxForQuota(year);
    
    $("#preYear").click(function(){
    	var y = $("#year").text();
    	$("#year").html(parseInt(y)-1);
    	ajaxForQuota(parseInt(y)-1);
    })
	$("#nextYear").click(function(){
		var y = $("#year").text();
		$("#year").html(parseInt(y)+1);
		ajaxForQuota(parseInt(y)+1);
	})
    function on_resize() {
    	window_height = $(window).height();
    	window_width = $(window).width();
    	
       	document.getElementById('gridContainer').style.height = window_height-624+"px";
       	document.getElementById('gridContainer').style.width = window_width+"px";
       	ajaxForQuota(year);
       	
    }
    function ajaxForQuota(year){
    	change=false;
    	var quotas;
    	$.ajax({
            url: path + '/quota/get_all.do',
            type: 'get',
            data:{year:year},
            async: false,
            success: function (data) {
                //console.info(data);
                quotas = data;
            }
        });
    	
    	$("#gridContainer").handsontable({
            data: quotas,
            colWidths: window_width/14, // 设置所有列宽
            autoColumnSize:true,
    		rowHeights: 35,
    		height: '100%',//表内滚动条
            //fixedColumnsLeft: 2,
            colHeaders: ["分厂","能源","1月","2月","3月","4月","5月","6月","7月","8月","9月","10月","11月","12月"],
            columnSorting: true,
            columns:[
            	{
                    data:'factory',
                    readOnly:true
                },{
                    data:'energy',
                    readOnly:true
                },{
                    data:"month1",
                    
                },{
                    data:"month2",

                },{
                    data:"month3"

                },{
                    data:"month4"

                },{
                    data:"month5"
                },{
                    data:"month6"

                },{
                    data:"month7"

                },{
                    data:"month8"

                },{
                    data:"month9"
                },{
                    data:"month10"

                },{
                    data:"month11"

                },{
                    data:"month12"
                },
            ],
            cells: function (row, col) {
                var cellProperties = {};
                var data = this.instance.getData();
                if (col === 0) {
                  cellProperties.renderer = firstRowRenderer; // uses function directly
                }
                if (col === 1) {
                    cellProperties.renderer = firstRowRenderer; // uses function directly
             	}
                return cellProperties;
            },
           afterChange:function(changes, source){
        	   if(change){
        		   for (var i = 0; i < changes.length; i++) {
        			   var factory=quotas[parseInt(changes[i][0])].factory;
        			   var energy=quotas[parseInt(changes[i][0])].energy;
        			   var val=changes[i][3];
        			   var ym;
        			   if(changes[i][1]=="month1"){
        				   ym= year+"-01-01";
        			   }
        			   if(changes[i][1]=="month2"){
        				   ym= year+"-02-01";
        			   }
        			   if(changes[i][1]=="month3"){
        				   ym= year+"-03-01";
        			   }
        			   if(changes[i][1]=="month4"){
        				   ym= year+"-04-01";
        			   }
        			   if(changes[i][1]=="month5"){
        				   ym= year+"-05-01";
        			   }
        			   if(changes[i][1]=="month6"){
        				   ym= year+"-06-01";
        			   }
        			   if(changes[i][1]=="month7"){
        				   ym= year+"-07-01";
        			   }
        			   if(changes[i][1]=="month8"){
        				   ym= year+"-08-01";
        			   }
        			   if(changes[i][1]=="month9"){
        				   ym= year+"-09-01";
        			   }
        			   if(changes[i][1]=="month10"){
        				   ym= year+"-10-01";
        			   }
        			   if(changes[i][1]=="month11"){
        				   ym= year+"-11-01";
        			   }
        			   if(changes[i][1]=="month12"){
        				   ym= year+"-12-01";
        			   }
        			   var quotas_daily_data={factory:factory,energy:energy,ym:ym,val:val};
        			   quota_sava.push(quotas_daily_data);
        		   }
        	   }
       		}
    	});
    	change=true;
    }
});
function firstRowRenderer(instance, td, row, col, prop, value, cellProperties) {
	Handsontable.renderers.TextRenderer.apply(this, arguments);
	td.style.color = '#000';
	td.style.background = '#EEE';
}
function quota_data(){
	var factory_data="";
	var energy_data="";
	var ym_data="";
	var val_data="";
	for (var i = 0; i < quota_sava.length; i++) {
		factory_data=quota_sava[i].factory+"--"+factory_data;
		energy_data=quota_sava[i].energy+"--"+energy_data;
		ym_data=quota_sava[i].ym+"--"+ym_data;
		val_data=quota_sava[i].val+"--"+val_data;
	}
	$.ajax({
		url: path + '/quota/update_quotas.do',
		type: 'get',
		data:{
			factory:factory_data,energy:energy_data,ym:ym_data,val:val_data
		},
		async: false,
		success: function (data) {
			if(data == "1"){
				alert("保存成功！");
			}
		}
	});
}
