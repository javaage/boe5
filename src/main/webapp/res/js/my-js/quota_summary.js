
var windowHeight = 0;
var windowWidth = 0;

var power_quotas_data=new Array();
var cda_quotas_data=new Array();
var upw_quotas_data=new Array();
var pn2_quotas_data=new Array();

var button = [
    { idx:0, z:12, id: "power_div",},
    { idx:1, z:13, id: "cda_div",},
    { idx:2, z:14, id: "upw_div",},
    { idx:3, z:15, id: "pn2_div",},
];

function on_resize() {
	var window_height = $(window).height();
	var window_width = $(window).width();
	for (var i = 0; i < button.length; i++) {
		$("#"+button[i].id).css("width", '98.2%');
		$("#"+button[i].id).css("margin-left",20+'px');
		$("#"+button[i].id).css("height", window_height-136+"px");
		
		$("#power_quota_chart").css("width",  window_width/2-31+'px');
		$("#power_sum_quota_chart").css("width",  window_width/2-31+'px');
		$("#power_grid").css("width",  window_width-41+'px');
		$("#upw_quota_chart").css("width", window_width/2-31+'px');
		$("#upw_sum_quota_chart").css("width", window_width/2-31+'px');
		$("#upw_grid").css("width",  window_width-41+'px');
		$("#cda_quota_chart").css("width", window_width/2-31+'px');
		$("#cda_sum_quota_chart").css("width", window_width/2-31+'px');
		$("#cda_grid").css("width",  window_width-41+'px');
		$("#pn2_quota_chart").css("width", window_width/2-31+'px');
		$("#pn2_sum_quota_chart").css("width", window_width/2-31+'px');
		$("#pn2_grid").css("width",  window_width-41+'px');
		
		$("#power_quota_chart").css("height", window_height-340+'px');
		$("#power_sum_quota_chart").css("height", window_height-340+'px');
		$("#upw_quota_chart").css("height", window_height-340+'px');
		$("#upw_sum_quota_chart").css("height", window_height-340+'px');
		$("#cda_quota_chart").css("height", window_height-340+'px');
		$("#cda_sum_quota_chart").css("height", window_height-340+'px');
		$("#pn2_quota_chart").css("height", window_height-340+'px');
		$("#pn2_sum_quota_chart").css("height", window_height-340+'px');
		
		power_quota_chart.resize();
		power_sum_quota_chart.resize();
	    upw_quota_chart.resize();
	    upw_sum_quota_chart.resize();
	    cda_quota_chart.resize();
	    cda_sum_quota_chart.resize();
	    pn2_quota_chart.resize();
	    pn2_sum_quota_chart.resize();
	}
}



$(function() {
	
	window.onresize =function() {
  		on_resize();
   	}
	
    $("#"+button[0].id).css("z-index", 50);
    $("#"+button[0].id).css("display", 'inline');
    
    function logEvent(eventName) {
        var logList = $("#events ul"),
            newItem = $("<li>", { text: eventName });

        logList.prepend(newItem);
    }
    
    cur_year = new Date().getFullYear();
    $("#year").html(cur_year);
    ajaxForQuota(cur_year);
    
    $("#preYear").click(function(){
    	cur_year = cur_year - 1;
    	$("#year").html(cur_year);
    	ajaxForQuota(cur_year);
    })
     $("#nextYear").click(function(){
     	cur_year = cur_year + 1;
    	$("#year").html(cur_year);
    	ajaxForQuota(cur_year);
    })
    
    $("#power_button").click(function(){
		var current_z_index = $("#power_div").css("z-index");
		if (current_z_index != 50) {
			for (var i = 0; i < button.length; i++) {
				$("#"+button[i].id).css("z-index", button[i].z);
				$("#"+button[i].id).css("display", 'none');
			}
			$("#power_div").css("z-index", 50);
			$("#power_div").css("display", 'inline');
		}
		changeColor("power_button");
    })

	$("#cda_button").click(function(){
		var current_z_index = $("#cda_div").css("z-index");
		if (current_z_index != 50) {
			for (var i = 0; i < button.length; i++) {
				$("#"+button[i].id).css("z-index", button[i].z);
				$("#"+button[i].id).css("display", 'none');
			}
			$("#cda_div").css("z-index", 50);
			$("#cda_div").css("display", 'inline');
		}
		changeColor("cda_button");
	})

	$("#upw_button").click(function(){
		var current_z_index = $("#upw_div").css("z-index");
		if (current_z_index != 50) {
			for (var i = 0; i < button.length; i++) {
				$("#"+button[i].id).css("z-index", button[i].z);
				$("#"+button[i].id).css("display", 'none');
			}
			$("#upw_div").css("z-index", 50);
			$("#upw_div").css("display", 'inline');
		}
		changeColor("upw_button");
	})

	$("#pn2_button").click(function(){
		var current_z_index = $("#pn2_div").css("z-index");
		if (current_z_index != 50) {
			for (var i = 0; i < button.length; i++) {
				$("#"+button[i].id).css("z-index", button[i].z);
				$("#"+button[i].id).css("display", 'none');
			}
			$("#pn2_div").css("z-index", 50);
			$("#pn2_div").css("display", 'inline');
		}
	    changeColor("pn2_button");
	})
	function changeColor(id) {
    	document.getElementById("power_button").style.backgroundColor="#fff";
    	document.getElementById("cda_button").style.backgroundColor="#fff";
    	document.getElementById("upw_button").style.backgroundColor="#fff";
    	document.getElementById("pn2_button").style.backgroundColor="#fff";
    	document.getElementById(id).style.backgroundColor="red";
    }
	function set_chart() {
    	power_quota_option.series[0].data=get_option_data(power_quotas_data,0);
    	power_quota_option.series[1].data=get_option_data(power_quotas_data,1);
    	power_quota_chart.setOption(power_quota_option);
    	
    	power_sum_quota_option.series[0].data=get_option_data(power_quotas_data,3);
    	power_sum_quota_option.series[1].data=get_option_data(power_quotas_data,4);
    	power_sum_quota_chart.setOption(power_sum_quota_option);
    	
    	upw_quota_option.series[0].data=get_option_data(upw_quotas_data,0);
    	upw_quota_option.series[1].data=get_option_data(upw_quotas_data,1);
    	upw_quota_chart.setOption(upw_quota_option);
    	
    	upw_sum_quota_option.series[0].data=get_option_data(upw_quotas_data,3);
    	upw_sum_quota_option.series[1].data=get_option_data(upw_quotas_data,4);
    	upw_sum_quota_chart.setOption(upw_sum_quota_option);  	
    	
    	cda_quota_option.series[0].data=get_option_data(cda_quotas_data,0);
    	cda_quota_option.series[1].data=get_option_data(cda_quotas_data,1);
    	cda_quota_chart.setOption(cda_quota_option);
    	
    	cda_sum_quota_option.series[0].data=get_option_data(cda_quotas_data,3);
    	cda_sum_quota_option.series[1].data=get_option_data(cda_quotas_data,4);
    	cda_sum_quota_chart.setOption(cda_sum_quota_option);  	
    	
    	pn2_quota_option.series[0].data=get_option_data(pn2_quotas_data,0);
    	pn2_quota_option.series[1].data=get_option_data(pn2_quotas_data,1);
    	pn2_quota_chart.setOption(pn2_quota_option);
    	
    	pn2_sum_quota_option.series[0].data=get_option_data(pn2_quotas_data,3);
    	pn2_sum_quota_option.series[1].data=get_option_data(pn2_quotas_data,4);
    	pn2_sum_quota_chart.setOption(pn2_sum_quota_option);  	
    }
    function get_option_data(quotas_data,i) {
    	var data= new Array();
    	data.push(quotas_data[i].month1);
    	if(cur_year==now_date_year&&now_date_month==1&&i==4||cur_year==now_date_year&&now_date_month==1&&i==1){ return data;}
    	data.push(quotas_data[i].month2);
    	if(cur_year==now_date_year&&now_date_month==2&&i==4||cur_year==now_date_year&&now_date_month==2&&i==1){ return data;}
    	data.push(quotas_data[i].month3);
    	if(cur_year==now_date_year&&now_date_month==3&&i==4||cur_year==now_date_year&&now_date_month==3&&i==1){ return data;}
    	data.push(quotas_data[i].month4);
    	if(cur_year==now_date_year&&now_date_month==4&&i==4||cur_year==now_date_year&&now_date_month==4&&i==1){ return data;}
    	data.push(quotas_data[i].month5);
    	if(cur_year==now_date_year&&now_date_month==5&&i==4||cur_year==now_date_year&&now_date_month==5&&i==1){ return data;}
    	data.push(quotas_data[i].month6);
    	if(cur_year==now_date_year&&now_date_month==6&&i==4||cur_year==now_date_year&&now_date_month==6&&i==1){ return data;}
    	data.push(quotas_data[i].month7);
    	if(cur_year==now_date_year&&now_date_month==7&&i==4||cur_year==now_date_year&&now_date_month==7&&i==1){ return data;}
    	data.push(quotas_data[i].month8);
    	if(cur_year==now_date_year&&now_date_month==8&&i==4||cur_year==now_date_year&&now_date_month==8&&i==1){ return data;}
    	data.push(quotas_data[i].month9);
    	if(cur_year==now_date_year&&now_date_month==9&&i==4||cur_year==now_date_year&&now_date_month==9&&i==1){ return data;}
    	data.push(quotas_data[i].month10);
    	if(cur_year==now_date_year&&now_date_month==10&&i==4||cur_year==now_date_year&&now_date_month==10&&i==1){ return data;}
    	data.push(quotas_data[i].month11);
    	if(cur_year==now_date_year&&now_date_month==11&&i==4||cur_year==now_date_year&&now_date_month==11&&i==1){ return data;}
    	data.push(quotas_data[i].month12);
    	return data;
    
    }
    
    
    function get_quotas_daily(quotas_daily) {
    	var data=new Array();
    	data=quotas_daily;
    	for (var i=1;i<31;i++) {
    		data[0].month1=parseInt(data[i].month1)+parseInt(data[0].month1);
    		data[0].month2=parseInt(data[i].month2)+parseInt(data[0].month2);
    		if (cur_year==now_date_year&&now_date_month<=1) {
    			data[0].month2=""
    		}
    		data[0].month3=parseInt(data[i].month3)+parseInt(data[0].month3);
    		if (cur_year==now_date_year&&now_date_month<=2) {
    			data[0].month3=""
    		}
    		data[0].month4=parseInt(data[i].month4)+parseInt(data[0].month4);
    		if (cur_year==now_date_year&&now_date_month<=3) {
    			data[0].month4=""
    		}
    		data[0].month5=parseInt(data[i].month5)+parseInt(data[0].month5);
    		if (cur_year==now_date_year&&now_date_month<=4) {
    			data[0].month5=""
    		}
    		data[0].month6=parseInt(data[i].month6)+parseInt(data[0].month6);
    		if (cur_year==now_date_year&&now_date_month<=5) {
    			data[0].month6=""
    		}
    		data[0].month7=parseInt(data[i].month7)+parseInt(data[0].month7);
    		if (cur_year==now_date_year&&now_date_month<=6) {
    			data[0].month7=""
    		}
    		data[0].month8=parseInt(data[i].month8)+parseInt(data[0].month8);
    		if (cur_year==now_date_year&&now_date_month<=7) {
    			data[0].month8=""
    		}
    		data[0].month9=parseInt(data[i].month9)+parseInt(data[0].month9);
    		if (cur_year==now_date_year&&now_date_month<=8) {
    			data[0].month9=""
    		}
    		data[0].month10=parseInt(data[i].month10)+parseInt(data[0].month10);
    		if (cur_year==now_date_year&&now_date_month<=9) {
    			data[0].month10=""
    		}
    		data[0].month11=parseInt(data[i].month11)+parseInt(data[0].month11);
    		if (cur_year==now_date_year&&now_date_month<=10) {
    			data[0].month11=""
    		}
    		data[0].month12=parseInt(data[i].month12)+parseInt(data[0].month12);
    		if (cur_year==now_date_year&&now_date_month<=11) {
    			data[0].month12=""
    		}
        }
    	return data[0];
    }
    
    function get_quotas_percent(quotas_data) {
    	var data=new Array();
    	data=JSON.parse(JSON.stringify(quotas_data));
		data[0].month1=(parseInt(data[1].month1)/parseInt(data[0].month1)*100).toFixed(2)+'%';
		data[0].month2=(parseInt(data[1].month2)/parseInt(data[0].month2)*100).toFixed(2)+'%';
		if (cur_year==now_date_year&&now_date_month<=1) {
			data[0].month2=""
		}
		data[0].month3=(parseInt(data[1].month3)/parseInt(data[0].month3)*100).toFixed(2)+'%';
		if (cur_year==now_date_year&&now_date_month<=2) {
			data[0].month3=""
		}
		data[0].month4=(parseInt(data[1].month4)/parseInt(data[0].month4)*100).toFixed(2)+'%';
		if (cur_year==now_date_year&&now_date_month<=3) {
			data[0].month4=""
		}
		data[0].month5=(parseInt(data[1].month5)/parseInt(data[0].month5)*100).toFixed(2)+'%';
		if (cur_year==now_date_year&&now_date_month<=4) {
			data[0].month5=""
		}
		data[0].month6=(parseInt(data[1].month6)/parseInt(data[0].month6)*100).toFixed(2)+'%';
		if (cur_year==now_date_year&&now_date_month<=5) {
			data[0].month6=""
		}
		data[0].month7=(parseInt(data[1].month7)/parseInt(data[0].month7)*100).toFixed(2)+'%';
		if (cur_year==now_date_year&&now_date_month<=6) {
			data[0].month7=""
		}
		data[0].month8=(parseInt(data[1].month8)/parseInt(data[0].month8)*100).toFixed(2)+'%';
		if (cur_year==now_date_year&&now_date_month<=7) {
			data[0].month8=""
		}
		data[0].month9=(parseInt(data[1].month9)/parseInt(data[0].month9)*100).toFixed(2)+'%';
		if (cur_year==now_date_year&&now_date_month<=8) {
			data[0].month9=""
		}
		data[0].month10=(parseInt(data[1].month10)/parseInt(data[0].month10)*100).toFixed(2)+'%';
		if (cur_year==now_date_year&&now_date_month<=9) {
			data[0].month10=""
		}
		data[0].month11=(parseInt(data[1].month11)/parseInt(data[0].month11)*100).toFixed(2)+'%';
		if (cur_year==now_date_year&&now_date_month<=10) {
			data[0].month11=""
		}
		data[0].month12=(parseInt(data[1].month12)/parseInt(data[0].month12)*100).toFixed(2)+'%';
		if (cur_year==now_date_year&&now_date_month<=11) {
			data[0].month12=""
		}
       
    	return data[0];
    }
    
    function get_quotas_total_sum(quotas_data) {
    	var data=new Array();
    	data=JSON.parse(JSON.stringify(quotas_data));
    	data[0].month1=parseInt(data[0].month1);
		data[0].month2=parseInt(data[0].month1)+parseInt(data[0].month2);
		data[0].month3=parseInt(data[0].month2)+parseInt(data[0].month3);
		data[0].month4=parseInt(data[0].month3)+parseInt(data[0].month4);
		data[0].month5=parseInt(data[0].month4)+parseInt(data[0].month5);
		data[0].month6=parseInt(data[0].month5)+parseInt(data[0].month6);
		data[0].month7=parseInt(data[0].month6)+parseInt(data[0].month7);
		data[0].month8=parseInt(data[0].month7)+parseInt(data[0].month8);
		data[0].month9=parseInt(data[0].month8)+parseInt(data[0].month9);
		data[0].month10=parseInt(data[0].month9)+parseInt(data[0].month10);
		data[0].month11=parseInt(data[0].month10)+parseInt(data[0].month11);		
		data[0].month12=parseInt(data[0].month11)+parseInt(data[0].month12);
		
    	return data[0];
    }
    
    function get_quotas_daily_sum(quotas_data) {
    	var data=new Array();
    	data=JSON.parse(JSON.stringify(quotas_data));
    	data[0].month1=parseInt(data[1].month1);
		data[0].month2=parseInt(data[0].month1)+parseInt(data[1].month2);
		if (cur_year==now_date_year&&now_date_month<=1) {
			data[0].month2=""
		}
		data[0].month3=parseInt(data[0].month2)+parseInt(data[1].month3);
		if (cur_year==now_date_year&&now_date_month<=2) {
			data[0].month3=""
		}
		data[0].month4=parseInt(data[0].month3)+parseInt(data[1].month4);
		if (cur_year==now_date_year&&now_date_month<=3) {
			data[0].month4=""
		}
		data[0].month5=parseInt(data[0].month4)+parseInt(data[1].month5);
		if (cur_year==now_date_year&&now_date_month<=4) {
			data[0].month5=""
		}
		data[0].month6=parseInt(data[0].month5)+parseInt(data[1].month6);
		if (cur_year==now_date_year&&now_date_month<=5) {
			data[0].month6=""
		}
		data[0].month7=parseInt(data[0].month6)+parseInt(data[1].month7);
		if (cur_year==now_date_year&&now_date_month<=6) {
			data[0].month7=""
		}
		data[0].month8=parseInt(data[0].month7)+parseInt(data[1].month8);
		if (cur_year==now_date_year&&now_date_month<=7) {
			data[0].month8=""
		}
		data[0].month9=parseInt(data[0].month8)+parseInt(data[1].month9);
		if (cur_year==now_date_year&&now_date_month<=8) {
			data[0].month9=""
		}
		data[0].month10=parseInt(data[0].month9)+parseInt(data[1].month10);
		if (cur_year==now_date_year&&now_date_month<=9) {
			data[0].month10=""
		}
		data[0].month11=parseInt(data[0].month10)+parseInt(data[1].month11);
		if (cur_year==now_date_year&&now_date_month<=10) {
			data[0].month11=""
		}
		data[0].month12=parseInt(data[0].month11)+parseInt(data[1].month12);
		if (cur_year==now_date_year&&now_date_month<=11) {
			data[0].month12=""
		}
       
    	return data[0];
    }
    
    function ajaxForQuota(year){
    	
    	power_quotas_data=new Array();
    	cda_quotas_data=new Array();
    	upw_quotas_data=new Array();
    	pn2_quotas_data=new Array();
    	
    	var quota_total_url;
    	var quota_total_daily;
    	if(factory=="ALL"){
    		quota_total_url='/gynh/get_all_quota_total.do'
    		quota_total_daily='/gynh/get_all_quota_daily.do'
    	}else{
    		quota_total_url='/gynh/get_factory_quota_total.do'
        	quota_total_daily='/gynh/get_factory_quota_daily.do'
    	}
    	if(!ajax_json){
			quota_summary_ajax_url1=path + quota_total_url;
			quota_summary_ajax_url2=path + quota_total_daily;
		}
    	
    	$.ajax({
//            url: path + quota_total_url,
    		url: quota_summary_ajax_url1,
            type: 'get',
            data:{year:year,factory:factory},
            async: false,
            success: function (data) {
            	for(var i=0;i<data.length;i++){
                	if (data[i].energy=="Power") {
                		power_quotas_data.push(data[i]);
                	}
                	if (data[i].energy=="CDA") {
                		cda_quotas_data.push(data[i]);
                	}
                	if (data[i].energy=="UPW") {
                		upw_quotas_data.push(data[i]);
                	}
                	if (data[i].energy=="PN2") {
                		 pn2_quotas_data.push(data[i]);
                	}
                }  
            }
        });
    	$.ajax({
//            url: path + quota_total_daily,
    		url: quota_summary_ajax_url2,
            type: 'get',
            data:{year:year,factory:factory},
            async: false,
            success: function (data) {
            
            	var power_quotas_daily=new Array();
            	var cda_quotas_daily=new Array();
            	var upw_quotas_daily=new Array();
            	var pn2_quotas_daily=new Array();
            	
                for(var i=0;i<data.length;i++){
                	if(factory!="ALL"){
                		data[i].month1=(parseInt(data[i].month1)/1000).toFixed(2);
                		data[i].month2=(parseInt(data[i].month2)/1000).toFixed(2);
                		data[i].month3=(parseInt(data[i].month3)/1000).toFixed(2);
                		data[i].month4=(parseInt(data[i].month4)/1000).toFixed(2);
                		data[i].month5=(parseInt(data[i].month5)/1000).toFixed(2);
                		data[i].month6=(parseInt(data[i].month6)/1000).toFixed(2);
                		data[i].month7=(parseInt(data[i].month7)/1000).toFixed(2);
                		data[i].month8=(parseInt(data[i].month8)/1000).toFixed(2);
                		data[i].month9=(parseInt(data[i].month9)/1000).toFixed(2);
                		data[i].month10=(parseInt(data[i].month10)/1000).toFixed(2);
                		data[i].month11=(parseInt(data[i].month11)/1000).toFixed(2);
                		data[i].month12=(parseInt(data[i].month12)/1000).toFixed(2);
                	}
                	if (data[i].energy=="Power") {
                		power_quotas_daily.push(data[i]);
                	}
                	if (data[i].energy=="CDA") {
                		cda_quotas_daily.push(data[i]);
                	}
                	if (data[i].energy=="UPW") {
                		upw_quotas_daily.push(data[i]);
                	}
                	if (data[i].energy=="PN2") {
                		 pn2_quotas_daily.push(data[i]);
                	}
                } 
                power_quotas_data.push(get_quotas_daily(power_quotas_daily));
                cda_quotas_data.push(get_quotas_daily(cda_quotas_daily));
                upw_quotas_data.push(get_quotas_daily(upw_quotas_daily));
                pn2_quotas_data.push(get_quotas_daily(pn2_quotas_daily));
                
                power_quotas_data.push(get_quotas_percent(power_quotas_data));
                cda_quotas_data.push(get_quotas_percent(cda_quotas_data));
                upw_quotas_data.push(get_quotas_percent(upw_quotas_data));
                pn2_quotas_data.push(get_quotas_percent(pn2_quotas_data));
               
                power_quotas_data.push(get_quotas_total_sum(power_quotas_data));
                cda_quotas_data.push(get_quotas_total_sum(cda_quotas_data));
                upw_quotas_data.push(get_quotas_total_sum(upw_quotas_data));
                pn2_quotas_data.push(get_quotas_total_sum(pn2_quotas_data));
                
                power_quotas_data.push(get_quotas_daily_sum(power_quotas_data));
                cda_quotas_data.push(get_quotas_daily_sum(cda_quotas_data));
                upw_quotas_data.push(get_quotas_daily_sum(upw_quotas_data));
                pn2_quotas_data.push(get_quotas_daily_sum(pn2_quotas_data));
                
                power_quotas_data[0].energy='定额';
                cda_quotas_data[0].energy='定额';
                upw_quotas_data[0].energy='定额';
                pn2_quotas_data[0].energy='定额';
                
                power_quotas_data[1].energy='实际';
                cda_quotas_data[1].energy='实际';
                upw_quotas_data[1].energy='实际';
                pn2_quotas_data[1].energy='实际';
                
                power_quotas_data[2].energy='实际/定额';
                cda_quotas_data[2].energy='实际/定额';
                upw_quotas_data[2].energy='实际/定额';
                pn2_quotas_data[2].energy='实际/定额';
                
                power_quotas_data[3].energy='定额累计';
                cda_quotas_data[3].energy='定额累计';
                upw_quotas_data[3].energy='定额累计';
                pn2_quotas_data[3].energy='定额累计';
                
                power_quotas_data[4].energy='实际累计';
                cda_quotas_data[4].energy='实际累计';
                upw_quotas_data[4].energy='实际累计';
                pn2_quotas_data[4].energy='实际累计';
                
                set_chart();
            
            }
        });
    	
    	 $("#power_grid").dxDataGrid({
 	        dataSource: power_quotas_data,
 	        showBorders: true,
 	        showColumnLines: true,
 	        allowColumnReordering: true,
 	        allowColumnResizing: true,
 	        showRowLines: true,
 	        rowAlternationEnabled: true,
 	        paging: {
 	            enabled: false
 	        },
 	        columnFixing: { 
 	            enabled: true
 	        },
 	        columns: [
 	            {
 	                dataField: "energy",
 	                allowEditing:false,
 	                fixed: true,
 	                cssClass: "fac_enr",
 	                caption: "评价指标"
 	            },
 	            {
 	                dataField: "month1",
 	                validationRules: [{ type: "required" }, {
 	                    type: "pattern",
 	                }],
 	                alignment: "right",
 	                caption: "1月"
 	            },
 	            {
 	                dataField: "month2",
 	                validationRules: [{ type: "required" }, {
 	                    type: "pattern",
 	                }],
 	                alignment: "right",
 	                caption: "2月"
 	            },
 	            {
 	                dataField: "month3",
 	                validationRules: [{ type: "required" }, {
 	                    type: "pattern",
 	                }],
 	                alignment: "right",
 	                caption: "3月"
 	            },
 	            {
 	                dataField: "month4",
 	                validationRules: [{ type: "required" }, {
 	                    type: "pattern",
 	                }],
 	                alignment: "right",
 	                caption: "4月"
 	            },
 	            {
 	                dataField: "month5",
 	                validationRules: [{ type: "required" }, {
 	                    type: "pattern",
 	                }],
 	                alignment: "right",
 	                caption: "5月"
 	            },
 	            {
 	                dataField: "month6",
 	                validationRules: [{ type: "required" }, {
 	                    type: "pattern",
 	                }],
 	                alignment: "right",
 	                caption: "6月"
 	            },
 	            {
 	                dataField: "month7",
 	                validationRules: [{ type: "required" }, {
 	                    type: "pattern",
 	                }],
 	                alignment: "right",
 	                caption: "7月"
 	            },
 	            {
 	                dataField: "month8",
 	                validationRules: [{ type: "required" }, {
 	                    type: "pattern",
 	                }],
 	                alignment: "right",
 	                caption: "8月"
 	            },
 	            {
 	                dataField: "month9",
 	                validationRules: [{ type: "required" }, {
 	                    type: "pattern",
 	                }],
 	                alignment: "right",
 	                caption: "9月"
 	            },
 	            {
 	                dataField: "month10",
 	                validationRules: [{ type: "required" }, {
 	                    type: "pattern",
 	                }],
 	                alignment: "right",
 	                caption: "10月"
 	            },{
 	                dataField: "month11",
 	                validationRules: [{ type: "required" }, {
 	                    type: "pattern",
 	                }],
 	                alignment: "right",
 	                caption: "11月"
 	            },{
 	                dataField: "month12",
 	                validationRules: [{ type: "required" }, {
 	                    type: "pattern",
 	                }],
 	                alignment: "right",
 	                caption: "12月"
 	            }
 	        ],
 	    });
    	 
    	 $("#cda_grid").dxDataGrid({
  	        dataSource: cda_quotas_data,
  	        showBorders: true,
  	        showColumnLines: true,
  	        allowColumnReordering: true,
  	        allowColumnResizing: true,
  	        showRowLines: true,
  	        rowAlternationEnabled: true,
  	        paging: {
  	            enabled: false
  	        },
  	        columnFixing: { 
  	            enabled: true
  	        },
  	      columns: [
	            {
	                dataField: "energy",
	                allowEditing:false,
	                fixed: true,
	                cssClass: "fac_enr",
	                caption: "评价指标"
	            },
	            {
	                dataField: "month1",
	                validationRules: [{ type: "required" }, {
	                    type: "pattern",
	                }],
	                alignment: "right",
	                caption: "1月"
	            },
	            {
	                dataField: "month2",
	                validationRules: [{ type: "required" }, {
	                    type: "pattern",
	                }],
	                alignment: "right",
	                caption: "2月"
	            },
	            {
	                dataField: "month3",
	                validationRules: [{ type: "required" }, {
	                    type: "pattern",
	                }],
	                alignment: "right",
	                caption: "3月"
	            },
	            {
	                dataField: "month4",
	                validationRules: [{ type: "required" }, {
	                    type: "pattern",
	                }],
	                alignment: "right",
	                caption: "4月"
	            },
	            {
	                dataField: "month5",
	                validationRules: [{ type: "required" }, {
	                    type: "pattern",
	                }],
	                alignment: "right",
	                caption: "5月"
	            },
	            {
	                dataField: "month6",
	                validationRules: [{ type: "required" }, {
	                    type: "pattern",
	                }],
	                alignment: "right",
	                caption: "6月"
	            },
	            {
	                dataField: "month7",
	                validationRules: [{ type: "required" }, {
	                    type: "pattern",
	                }],
	                alignment: "right",
	                caption: "7月"
	            },
	            {
	                dataField: "month8",
	                validationRules: [{ type: "required" }, {
	                    type: "pattern",
	                }],
	                alignment: "right",
	                caption: "8月"
	            },
	            {
	                dataField: "month9",
	                validationRules: [{ type: "required" }, {
	                    type: "pattern",
	                }],
	                alignment: "right",
	                caption: "9月"
	            },
	            {
	                dataField: "month10",
	                validationRules: [{ type: "required" }, {
	                    type: "pattern",
	                }],
	                alignment: "right",
	                caption: "10月"
	            },{
	                dataField: "month11",
	                validationRules: [{ type: "required" }, {
	                    type: "pattern",
	                }],
	                alignment: "right",
	                caption: "11月"
	            },{
	                dataField: "month12",
	                validationRules: [{ type: "required" }, {
	                    type: "pattern",
	                }],
	                alignment: "right",
	                caption: "12月"
	            }
	        ],
  	    });
    	 
    	 $("#upw_grid").dxDataGrid({
  	        dataSource: upw_quotas_data,
  	        showBorders: true,
  	        showColumnLines: true,
  	        allowColumnReordering: true,
  	        allowColumnResizing: true,
  	        showRowLines: true,
  	        rowAlternationEnabled: true,
  	        paging: {
  	            enabled: false
  	        },
  	        columnFixing: { 
  	            enabled: true
  	        },
  	      columns: [
	            {
	                dataField: "energy",
	                allowEditing:false,
	                fixed: true,
	                cssClass: "fac_enr",
	                caption: "评价指标"
	            },
	            {
	                dataField: "month1",
	                validationRules: [{ type: "required" }, {
	                    type: "pattern",
	                }],
	                alignment: "right",
	                caption: "1月"
	            },
	            {
	                dataField: "month2",
	                validationRules: [{ type: "required" }, {
	                    type: "pattern",
	                }],
	                alignment: "right",
	                caption: "2月"
	            },
	            {
	                dataField: "month3",
	                validationRules: [{ type: "required" }, {
	                    type: "pattern",
	                }],
	                alignment: "right",
	                caption: "3月"
	            },
	            {
	                dataField: "month4",
	                validationRules: [{ type: "required" }, {
	                    type: "pattern",
	                }],
	                alignment: "right",
	                caption: "4月"
	            },
	            {
	                dataField: "month5",
	                validationRules: [{ type: "required" }, {
	                    type: "pattern",
	                }],
	                alignment: "right",
	                caption: "5月"
	            },
	            {
	                dataField: "month6",
	                validationRules: [{ type: "required" }, {
	                    type: "pattern",
	                }],
	                alignment: "right",
	                caption: "6月"
	            },
	            {
	                dataField: "month7",
	                validationRules: [{ type: "required" }, {
	                    type: "pattern",
	                }],
	                alignment: "right",
	                caption: "7月"
	            },
	            {
	                dataField: "month8",
	                validationRules: [{ type: "required" }, {
	                    type: "pattern",
	                }],
	                alignment: "right",
	                caption: "8月"
	            },
	            {
	                dataField: "month9",
	                validationRules: [{ type: "required" }, {
	                    type: "pattern",
	                }],
	                alignment: "right",
	                caption: "9月"
	            },
	            {
	                dataField: "month10",
	                validationRules: [{ type: "required" }, {
	                    type: "pattern",
	                }],
	                alignment: "right",
	                caption: "10月"
	            },{
	                dataField: "month11",
	                validationRules: [{ type: "required" }, {
	                    type: "pattern",
	                }],
	                alignment: "right",
	                caption: "11月"
	            },{
	                dataField: "month12",
	                validationRules: [{ type: "required" }, {
	                    type: "pattern",
	                }],
	                alignment: "right",
	                caption: "12月"
	            }
	        ],
  	    });
    	 
    	 $("#pn2_grid").dxDataGrid({
  	        dataSource: pn2_quotas_data,
  	        showBorders: true,
  	        showColumnLines: true,
  	        allowColumnReordering: true,
  	        allowColumnResizing: true,
  	        showRowLines: true,
  	        rowAlternationEnabled: true,
  	        paging: {
  	            enabled: false
  	        },
  	        columnFixing: { 
  	            enabled: true
  	        },
  	      columns: [
	            {
	                dataField: "energy",
	                allowEditing:false,
	                fixed: true,
	                cssClass: "fac_enr",
	                caption: "评价指标"
	            },
	            {
	                dataField: "month1",
	                validationRules: [{ type: "required" }, {
	                    type: "pattern",
	                }],
	                alignment: "right",
	                caption: "1月"
	            },
	            {
	                dataField: "month2",
	                validationRules: [{ type: "required" }, {
	                    type: "pattern",
	                }],
	                alignment: "right",
	                caption: "2月"
	            },
	            {
	                dataField: "month3",
	                validationRules: [{ type: "required" }, {
	                    type: "pattern",
	                }],
	                alignment: "right",
	                caption: "3月"
	            },
	            {
	                dataField: "month4",
	                validationRules: [{ type: "required" }, {
	                    type: "pattern",
	                }],
	                alignment: "right",
	                caption: "4月"
	            },
	            {
	                dataField: "month5",
	                validationRules: [{ type: "required" }, {
	                    type: "pattern",
	                }],
	                alignment: "right",
	                caption: "5月"
	            },
	            {
	                dataField: "month6",
	                validationRules: [{ type: "required" }, {
	                    type: "pattern",
	                }],
	                alignment: "right",
	                caption: "6月"
	            },
	            {
	                dataField: "month7",
	                validationRules: [{ type: "required" }, {
	                    type: "pattern",
	                }],
	                alignment: "right",
	                caption: "7月"
	            },
	            {
	                dataField: "month8",
	                validationRules: [{ type: "required" }, {
	                    type: "pattern",
	                }],
	                alignment: "right",
	                caption: "8月"
	            },
	            {
	                dataField: "month9",
	                validationRules: [{ type: "required" }, {
	                    type: "pattern",
	                }],
	                alignment: "right",
	                caption: "9月"
	            },
	            {
	                dataField: "month10",
	                validationRules: [{ type: "required" }, {
	                    type: "pattern",
	                }],
	                alignment: "right",
	                caption: "10月"
	            },{
	                dataField: "month11",
	                validationRules: [{ type: "required" }, {
	                    type: "pattern",
	                }],
	                alignment: "right",
	                caption: "11月"
	            },{
	                dataField: "month12",
	                validationRules: [{ type: "required" }, {
	                    type: "pattern",
	                }],
	                alignment: "right",
	                caption: "12月"
	            }
	        ],
  	    });
    	on_resize();
    }
});