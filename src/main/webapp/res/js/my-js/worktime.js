﻿
function powerUnitChange(e) {
	var pivotState = pivotgrid.getDataSource().state();
	
	if(power_dataField==null){
		return;
	}
	var dataSource = pivotgrid.getDataSource();
	var unit = e.value;
	if (unit == 'kWh' && currentPowerUnit != 'kWh') {
		translatePower(currentPowerUnit, 'kWh');
		for (var i = 0; i < dataSource.fields().length; i++) {
			if (dataSource.field(i).dataField == power_dataField) {
				pivotgrid.option('dataSource.fields['+i+'].caption', "POWER");
				pivotgrid.option('dataSource.fields['+i+'].format.precision', 0);
				break;
			}
		}
		if (worktime_flag) {
			pivotgrid.option("dataSource.store", mark);
		} else {
			pivotgrid.option("dataSource.store", mark);
		}
	}
	if (unit == 'mWh' && currentPowerUnit != 'mWh') {
		translatePower(currentPowerUnit, 'mWh');
		for (var i = 0; i < dataSource.fields().length; i++) {
			if (dataSource.field(i).dataField == power_dataField) {
				pivotgrid.option('dataSource.fields['+i+'].caption', "POWER");
				pivotgrid.option('dataSource.fields['+i+'].format.precision', 3);
				break;
			}
		}
		if (worktime_flag) {
			pivotgrid.option("dataSource.store", mark);
		} else {
			pivotgrid.option("dataSource.store", mark);	
		}
	}
	pivotgrid.getDataSource().state(pivotState);
}

function translatePower(from, to) {
	if(power_dataField=='c'){
		if (from == 'kWh' && to == 'mWh') {
			for (var i = 0; i < mark.length; i++) {
				if (worktime_flag) {
					mark[i].c = mark[i].c/1000.0 ;
				} else {
					if (mark[i].e.toUpperCase() == 'Power'.toUpperCase()) {
						mark[i].pa = mark[i].pa/1000.0;
						mark[i].pc = mark[i].pc/1000.0;
						mark[i].pm = mark[i].pm/1000.0;
						mark[i].ra = mark[i].ra/1000.0;
						mark[i].rc = mark[i].rc/1000.0;
						mark[i].run = mark[i].run/1000.0;
						mark[i].c = mark[i].c/1000.0;
					}	
				}
			}
		}
		if (from == 'mWh' && to == 'kWh') {
			for (var i = 0; i < mark.length; i++) {
				if (worktime_flag) {
					mark[i].c = mark[i].c*1000.0 ;
				} else {
					if (mark[i].e.toUpperCase() == 'Power'.toUpperCase()) {
						mark[i].pa = mark[i].pa*1000.0;
						mark[i].pc = mark[i].pc*1000.0;
						mark[i].pm = mark[i].pm*1000.0;
						mark[i].ra = mark[i].ra*1000.0;
						mark[i].rc = mark[i].rc*1000.0;
						mark[i].run = mark[i].run*1000.0;
						mark[i].c = mark[i].c*1000.0;
						
					}	
				}
			}
		}
	}else{
		if (from == 'kWh' && to == 'mWh') {
			for (var i = 0; i < mark.length; i++) {
				if (worktime_flag) {
					mark[i].pwr = mark[i].pwr/1000.0 ;
				} else {
					if (mark[i].e.toUpperCase() == 'Power'.toUpperCase()) {
						mark[i].pa = mark[i].pa/1000.0;
						mark[i].pc = mark[i].pc/1000.0;
						mark[i].pm = mark[i].pm/1000.0;
						mark[i].ra = mark[i].ra/1000.0;
						mark[i].rc = mark[i].rc/1000.0;
						mark[i].run = mark[i].run/1000.0;
						mark[i].c = mark[i].c/1000.0;
					}	
				}
			}
		}
		if (from == 'mWh' && to == 'kWh') {
			for (var i = 0; i < mark.length; i++) {
				if (worktime_flag) {
					mark[i].pwr = mark[i].pwr*1000.0 ;
				} else {
					if (mark[i].e.toUpperCase() == 'Power'.toUpperCase()) {
						mark[i].pa = mark[i].pa*1000.0;
						mark[i].pc = mark[i].pc*1000.0;
						mark[i].pm = mark[i].pm*1000.0;
						mark[i].ra = mark[i].ra*1000.0;
						mark[i].rc = mark[i].rc*1000.0;
						mark[i].run = mark[i].run*1000.0;
						mark[i].c = mark[i].c*1000.0;
					}
				}
			}
		}
	}
	currentPowerUnit = to;
}
function UnitChange(e) {
	var pivotState = pivotgrid.getDataSource().state();
	var dataSource = pivotgrid.getDataSource();
	var unit = e.value;
	if (e.id == 'select_upw') {
		if(upw_dataField==null){
			return;
		}
		currentUnit = currentUPWUnit;
	}
	if (e.id == 'select_cda') {
		if(cda_dataField==null){
			return;
		}
		currentUnit = currentCDAUnit;
	}
	if (e.id == 'select_pn2') {
		if(pn2_dataField==null){
			return;
		}
		currentUnit = currentPN2Unit;
	}
	var upperCase = e.id.split("_");
	var fields = upperCase[1];
	var caption = upperCase[1].toUpperCase();
	if (unit == 'L' && currentUnit != 'L') {
		translate(currentUnit, 'L',fields)
		if(fields=="upw"){
			for (var i = 0; i < dataSource.fields().length; i++) {
				if (dataSource.field(i).dataField == "diw") {
					pivotgrid.option('dataSource.fields['+i+'].caption', ""+caption);
					pivotgrid.option('dataSource.fields['+i+'].format.precision', 0);
					break;
				}
			}
		}
		for (var i = 0; i < dataSource.fields().length; i++) {
			if (dataSource.field(i).dataField == fields) {
				pivotgrid.option('dataSource.fields['+i+'].caption', ""+caption);
				pivotgrid.option('dataSource.fields['+i+'].format.precision', 0);
				break;
			}
		}
		if (worktime_flag) {
			pivotgrid.option("dataSource.store", mark);
		} else {
			pivotgrid.option("dataSource.store", mark);
		}
	}
	if (unit == 'm³' && currentUnit != 'm³') {
		translate(currentUnit, 'm³',fields)
		if(fields=="upw"){
			for (var i = 0; i < dataSource.fields().length; i++) {
				if (dataSource.field(i).dataField == "diw") {
					pivotgrid.option('dataSource.fields['+i+'].caption', ""+caption);
					pivotgrid.option('dataSource.fields['+i+'].format.precision', 3);
					break;
				}
			}
		}
		for (var i = 0; i < dataSource.fields().length; i++) {
			if (dataSource.field(i).dataField == fields) {
				pivotgrid.option('dataSource.fields['+i+'].caption', ""+caption);
				pivotgrid.option('dataSource.fields['+i+'].format.precision', 3);
				break;
			}
		}
		if (worktime_flag) {
			pivotgrid.option("dataSource.store", mark);
		} else {
			pivotgrid.option("dataSource.store", mark);
		}
	}
	if (unit == 'k(m³)' && currentUnit != 'k(m³)') {
		translate(currentUnit, 'k(m³)',fields)
		if(fields=="upw"){
			for (var i = 0; i < dataSource.fields().length; i++) {
				if (dataSource.field(i).dataField == "diw") {
					pivotgrid.option('dataSource.fields['+i+'].caption', ""+caption);
					pivotgrid.option('dataSource.fields['+i+'].format.precision', 3);
					break;
				}
			}
		}
		for (var i = 0; i < dataSource.fields().length; i++) {
			if (dataSource.field(i).dataField == fields) {
				pivotgrid.option('dataSource.fields['+i+'].caption', ""+caption);
				pivotgrid.option('dataSource.fields['+i+'].format.precision', 3);
				break;
			}
		}
		if (worktime_flag) {
			pivotgrid.option("dataSource.store", mark);
		} else {
			pivotgrid.option("dataSource.store", mark);
		}
	}
	pivotgrid.getDataSource().state(pivotState);
}

function translate(from, to, fields) {
	if(fields.toUpperCase()=='DIW'&&worktime_flag==false){
		var energy_field='UPW';
	}
	else{
		var energy_field=fields.toUpperCase();
	}
	
	if (from == 'L' && to == 'm³') {
		for (var i = 0; i < mark.length; i++) {
			if (worktime_flag) {
				if (fields == cda_dataField) {
					mark[i].cda = mark[i].cda/1000.0  ;
				}
				if (fields == upw_dataField) {
					mark[i].diw = mark[i].diw/1000.0 ;
				}
				if (fields == pn2_dataField) {
					mark[i].pn2 = mark[i].pn2/1000.0;
				}
			} else {
				if (mark[i].e == energy_field) {
					mark[i].pa = mark[i].pa/1000.0;
					mark[i].pc = mark[i].pc/1000.0;
					mark[i].pm = mark[i].pm/1000.0;
					mark[i].ra = mark[i].ra/1000.0;
					mark[i].rc = mark[i].rc/1000.0;
					mark[i].run = mark[i].run/1000.0;
					mark[i].c = mark[i].c/1000.0;
				}	
			}
		}
	}
	if (from == 'L' && to == 'k(m³)') {
		for (var i = 0; i < mark.length; i++) {
			if (worktime_flag) {
				if (fields == cda_dataField) {
					mark[i].cda = mark[i].cda/1000000.0  ;
				}
				if (fields == upw_dataField) {
					mark[i].diw = mark[i].diw/1000000.0 ;
				}
				if (fields == pn2_dataField) {
					mark[i].pn2 = mark[i].pn2/1000000.0;
				}
			} else {
				if (mark[i].e == energy_field) {
					mark[i].pa = mark[i].pa/1000000.0;
					mark[i].pc = mark[i].pc/1000000.0;
					mark[i].pm = mark[i].pm/1000000.0;
					mark[i].ra = mark[i].ra/1000000.0;
					mark[i].rc = mark[i].rc/1000000.0;
					mark[i].run = mark[i].run/1000000.0;
					mark[i].c = mark[i].c/1000000.0;
				}
			}
		}
	}
	if (from == 'm³' && to == 'L') {
		for (var i = 0; i < mark.length; i++) {
			if (worktime_flag) {
				if (fields == cda_dataField) {
					mark[i].cda = mark[i].cda*1000.0  ;
				}
				if (fields == upw_dataField) {
					mark[i].diw = mark[i].diw*1000.0 ;
				}
				if (fields == pn2_dataField) {
					mark[i].pn2 = mark[i].pn2*1000.0 ;
				}
			} else {
				if (mark[i].e == energy_field) {
					mark[i].pa = mark[i].pa*1000.0 ;
					mark[i].pc = mark[i].pc*1000.0 ;
					mark[i].pm = mark[i].pm*1000.0 ;
					mark[i].ra = mark[i].ra*1000.0 ;
					mark[i].rc = mark[i].rc*1000.0 ;
					mark[i].run = mark[i].run*1000.0 ;
					mark[i].c = mark[i].c*1000.0 ;
				}
			}
		}
	}
	if (from == 'm³' && to == 'k(m³)') {
		for (var i = 0; i < mark.length; i++) {
			if (worktime_flag) {
				if (fields == cda_dataField) {
					mark[i].cda = mark[i].cda/1000.0 ;
				}
				if (fields == upw_dataField) {
					mark[i].diw = mark[i].diw/1000.0 ;
				}
				if (fields == pn2_dataField) {
					mark[i].pn2 = mark[i].pn2/1000.0 ;
				}			
			} else {
				if (mark[i].e == energy_field) {
					mark[i].pa = mark[i].pa/1000.0 ;
					mark[i].pc = mark[i].pc/1000.0 ;
					mark[i].pm = mark[i].pm/1000.0 ;
					mark[i].ra = mark[i].ra/1000.0 ;
					mark[i].rc = mark[i].rc/1000.0 ;
					mark[i].run = mark[i].run/1000.0 ;	
					mark[i].c = mark[i].c/1000.0;
				}
			}
		}
	}
	if (from == 'k(m³)' && to == 'L') {
		for (var i = 0; i < mark.length; i++) {
			if (worktime_flag) {
				if (fields == cda_dataField) {
					mark[i].cda = mark[i].cda*1000000.0 ;
				}
				if (fields == upw_dataField) {
					mark[i].diw = mark[i].diw*1000000.0 ;
				}
				if (fields == pn2_dataField) {
					mark[i].pn2 = mark[i].pn2*1000000.0 ;
				}	
			} else {
				if (mark[i].e == energy_field) {
					mark[i].pa = mark[i].pa*1000000.0 ;
					mark[i].pc = mark[i].pc*1000000.0 ;
					mark[i].pm = mark[i].pm*1000000.0 ;
					mark[i].ra = mark[i].ra*1000000.0 ;
					mark[i].rc = mark[i].rc*1000000.0 ;
					mark[i].run = mark[i].run*1000000.0 ;
					mark[i].c = mark[i].c*1000000.0 ;
				}
			}
		}
	}
	if (from == 'k(m³)' && to == 'm³') {
		for (var i = 0; i < mark.length; i++) {
			if (worktime_flag) {
				if (fields == cda_dataField) {
					mark[i].cda = mark[i].cda*1000.0 ;
				}
				if (fields == upw_dataField) {
					mark[i].diw = mark[i].diw*1000.0 ;
				}
				if (fields == pn2_dataField) {
					mark[i].pn2 = mark[i].pn2*1000.0 ;
				}	
			} else {
				if (mark[i].e == energy_field) {
					mark[i].pa = mark[i].pa*1000.0 ;
					mark[i].pc = mark[i].pc*1000.0 ;
					mark[i].pm = mark[i].pm*1000.0 ;
					mark[i].ra = mark[i].ra*1000.0 ;
					mark[i].rc = mark[i].rc*1000.0 ;
					mark[i].run = mark[i].run*1000.0 ;
					mark[i].c = mark[i].c*1000.0 ;
				}
			}
		}
	}
	if (fields == "cda") {
		currentCDAUnit = to;
	}
	if (fields == "upw") {
		currentUPWUnit = to;
	}
	if (fields == "pn2") {
		currentPN2Unit = to;
	}
}


function timeUnitChange() {
	var pivotState = pivotgrid.getDataSource().state();
	var dataSource = pivotgrid.getDataSource();
	var unit = $('#select_hms').val();
	var time_data =time_dataField.split('_');
	if(time_dataField==null){
		return;
	}
	
	if (unit == 'H' && currentTimeUnit != 'H') {
		translateTime(currentTimeUnit, 'H');
		
		for(var j=0;j<time_data.length; j++){
			for (var i = 0; i < dataSource.fields().length; i++) {
				//console.log(dataSource.field(i).dataField);
				if (dataSource.field(i).dataField == time_data[j]) {
					pivotgrid.option('dataSource.fields['+i+'].caption', time_data[j]);
					pivotgrid.option('dataSource.fields['+i+'].format.precision', 2);
					break;
				}
			}
			if (worktime_flag) {
				pivotgrid.option("dataSource.store", mark);
			}
			
		    reloadStateSumTime();
		}
	}
	
	if (unit == 'M' && currentTimeUnit != 'M') {
		translateTime(currentTimeUnit, 'M');
		for(var j=0;j<time_data.length; j++){
			for (var i = 0; i < dataSource.fields().length; i++) {
					//console.log(dataSource.field(i).dataField);
				if (dataSource.field(i).dataField == time_data[j]) {
					pivotgrid.option('dataSource.fields['+i+'].caption', time_data[j]);
					pivotgrid.option('dataSource.fields['+i+'].format.precision', 2);
					break;
				}
			}
			if (worktime_flag) {
				pivotgrid.option("dataSource.store", mark);
			}
		    reloadStateSumTime();
		}
	}

	if (unit == 'S' && currentTimeUnit != 'S') {
		translateTime(currentTimeUnit, 'S');
		for(var j=0;j<time_data.length; j++){
			for (var i = 0; i < dataSource.fields().length; i++) {
				//console.log(dataSource.field(i).dataField);
				if (dataSource.field(i).dataField == time_data[j]) {
					pivotgrid.option('dataSource.fields['+i+'].caption', time_data[j]);
					pivotgrid.option('dataSource.fields['+i+'].format.precision', 0);
					break;
				}
			}
			if (worktime_flag) {
				pivotgrid.option("dataSource.store", mark);
			}
		    reloadStateSumTime();
		}
	}
	pivotgrid.getDataSource().state(pivotState);
}


function translateTime(from, to) {
	if (from == 'S' && to == 'H') {
		for (var i = 0; i < mark.length; i++) {
			mark[i].run = mark[i].run/3600.0  + 0.0000000001;
			mark[i].pm = mark[i].pm/3600.0  + 0.0000000001;
		}
	} else if (from == 'S' && to == 'M') {
		for (var i = 0; i < mark.length; i++) {
			mark[i].run = mark[i].run/60.0  + 0.0000000001;
			mark[i].pm = mark[i].pm/60.0  + 0.0000000001;
		}
	} else if (from == 'M' && to == 'H') {
		for (var i = 0; i < mark.length; i++) {
			mark[i].run = mark[i].run/60.0  + 0.0000000001;			
			mark[i].pm = mark[i].pm/60.0  + 0.0000000001;
		}
	} else if (from == 'M' && to == 'S') {
		for (var i = 0; i < mark.length; i++) {
			mark[i].run = mark[i].run*60.0  + 0.0000000001;
			mark[i].pm = mark[i].pm*60.0  + 0.0000000001;
		}
	} else if (from == 'H' && to == 'M') {
		for (var i = 0; i < mark.length; i++) {
			mark[i].run = mark[i].run*60.0  + 0.0000000001;
			mark[i].pm = mark[i].pm*60.0  + 0.0000000001;
		}
	} else if (from == 'H' && to == 'S') {
		for (var i = 0; i < mark.length; i++) {
			mark[i].run = mark[i].run*3600.0  + 0.0000000001;
			mark[i].pm = mark[i].pm*3600.0  + 0.0000000001;
		}
	} else {
		return;
	}
	currentTimeUnit = to;

}
