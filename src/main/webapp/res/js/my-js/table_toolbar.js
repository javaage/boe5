// 表的tool bar
var table_toolbar_chart = echarts.init(document.getElementById('table_toolbar_div'), "<%=theme%>");


var delShortcutMode = false;

var table_toolbar_option = {
	toolbox: {
		show: true,
		x:'left',
		showTitle:true,
		borderWidth: 0,
		feature: {
			my_loadDbBtn: {
				show: true,
				title: '加载',
				icon: 'image://'+path+'/res/images/toolbarbtn/db.png',
				onclick: function() {
					onClickLoadDbBtn();
				}
			},
			my_HVSplit: {
				show: true,
				title: '横/纵布局',
				icon: 'image://'+path+'/res/images/toolbarbtn/hsplit.png',
				onclick: function() {
					bHSplit = !bHSplit;
					setHVSplitBtnState();
					resizeScreen(true);
				}
			},
			my_optionbtn: {
				show: true,
				title: '显示选项',
				icon: 'image://'+path+'/res/images/toolbarbtn/pivotdisplayoption.png',
				onclick: function() {
					onClickOptionBtn();
				}
			},
			
			my_restbtn: {
				show: true,
				title: '恢复表布局',
				icon: 'image://'+path+'/res/images/toolbarbtn/reset.png',
				onclick: function() {
					onClickResetBtn();
				}
			},
			my_cub_sum_btn: {
				show: pageName=='cub_cons' ? true : false,
				title: '总览图//动态图',
				icon: 'image://'+path+'/res/images/toolbarbtn/save_shortcut.png',
				onclick: function() {
					on_click_cub_sum();
				}
			},
			my_SaveShortcutBtn: {
				show: true,
				title: '保存快捷方式',
				icon: 'image://'+path+'/res/images/toolbarbtn/save_shortcut.png',
				onclick: function() {
					onClickSaveShortcutBtn();
				}
			},
			my_DelShortcutBtn: {
				show: true,
				title: '删除快捷方式',
				icon: 'image://'+path+'/res/images/toolbarbtn/del_shortcut.png',
				onclick: function() {
					if (delShortcutMode) {
						table_toolbar_option.toolbox.feature.my_DelShortcutBtn.icon = 'image://'+path+'/res/images/toolbarbtn/del_shortcut.png';
					} else {
						table_toolbar_option.toolbox.feature.my_DelShortcutBtn.icon = 'image://'+path+'/res/images/toolbarbtn/del_shortcut_sel.png';
					}
					delShortcutMode = !delShortcutMode;
					table_toolbar_chart.setOption(table_toolbar_option);
				}
			},
			my_optionshort: {
				show: true,
				title: '快捷方式信息',
				icon: 'image://'+path+'/res/images/toolbarbtn/shortcuts.png',
				onclick: function() {
					create_shortcut_dxDataGrid();
					var shortcut_dlg = create_modal_dlg("#shortcut_dlg_div", "#shortcut_dlg_close_button");
					shortcut_dlg.init();
					shortcut_dlg.open();
				}
			},
			my_show_hide_shortcut_buttons: {
				show: true,
				title: '显示/隐藏 快捷方式按钮',
				icon: 'image://'+path+'/res/images/toolbarbtn/' + (shortcut_buttons_show ? 'collapse.png' : 'expand.png'),
				onclick: function() {
					show_hide_shortcut_buttons();
				}
			},
		}
	},
	series: [
		{
            name: ' ',
            type: 'pie',
		}
	]
};

table_toolbar_chart.setOption(table_toolbar_option);

//document.getElementById('table_toolbar_div').style.background='white';

