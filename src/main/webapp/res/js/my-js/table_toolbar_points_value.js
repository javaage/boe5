// 表的tool bar
var table_toolbar_chart = echarts.init(document.getElementById('table_toolbar_points_value_div'), "<%=theme%>");

var delShortcutMode = false;
if(pageName=='export'){
	var table_toolbar_option = {
		toolbox: {
			show: true,
			x:'left',
			showTitle:true,
			borderWidth: 0,
			feature: {
				my_loadDbBtn: {
					show: true,
					title: '加载',
					icon: 'image://'+path+'/res/images/toolbarbtn/db.png',
					onclick: function() {
						get_points_value();
					}
				},
				my_echarts: {
					show: true,
					title: '加载',
					icon: 'image://'+path+'/res/images/toolbarbtn/db.png',
					onclick: function() {
						get_echarts_value();
					}
				},
			}
		},
		series: [
			{
	            name: ' ',
	            type: 'pie',
			}
		]
	};
}else{
	var table_toolbar_option = {
		toolbox: {
			show: true,
			x:'left',
			showTitle:true,
			borderWidth: 0,
			feature: {
				my_loadDbBtn: {
					show: true,
					title: '加载',
					icon: 'image://'+path+'/res/images/toolbarbtn/db.png',
					onclick: function() {
						get_points_value();
					}
				}
			}
		},
		series: [
			{
	            name: ' ',
	            type: 'pie',
			}
		]
	};
}

table_toolbar_chart.setOption(table_toolbar_option);

//document.getElementById('table_toolbar_div').style.background='white';

