
var windowHeight = 0;
var windowWidth = 0;
var upPanelPersent = 0.5;
var leftPanelPersent = 0.5;
var bHSplit = true; //true:H横切（默认）， false:V纵切
var factory_points_grid;
var factory_inst_points_grid;
var points_echarts="factory_points_echarts";

var factory_point_list;
var factory_inst_point_list;

var points_values = new Array();
var inst_points_values = new Array();

var point_value_cache = {};
var inst_point_value_cache = {};
var sel_rows = new Array();
var inst_sel_rows = new Array();

var minute=10;
var total_points_value;

var longtabs = [
    { idx:0, z:11, id: "factory_points_grid", 	text: "分厂累计量点位",chart:"factory_points_echarts"},
    { idx:1, z:12, id: "factory_inst_points_grid", 	text: "分厂瞬时量点位",chart:"factory_inst_points_echarts"},
];

$(document).ready(function() {
	
    typeof page_custom_init === "function" ? page_custom_init() : false;
    
	resizeScreen(true);

	$('#rightSplitter').on('resize', function (event) {
	    var panels = event.args.panels;

		if  (bHSplit) { //default,横切纵排
			upPanelPersent = panels[0].size/(panels[0].size+panels[1].size);
			for (var i = 0; i < longtabs.length; i++) {
				document.getElementById(longtabs[i].id).style.height = panels[0].size-66 + 'px';
			}
			document.getElementById('factory_points_echarts').style.width = windowWidth-22 + 'px';
			document.getElementById('factory_points_echarts').style.height = panels[1].size-27 + 'px';
			document.getElementById('factory_inst_points_echarts').style.width = windowWidth-22 + 'px';
			document.getElementById('factory_inst_points_echarts').style.height = panels[1].size-27 + 'px';
			document.getElementById('left_div').style.marginTop =panels[1].size-110 + 'px';
			document.getElementById('right_div').style.marginTop =panels[1].size-110 + 'px';
			document.getElementById('right_div').style.marginLeft =windowWidth-156+'px';
			factory_points_echarts.resize();
			factory_inst_points_echarts.resize(); 
		} 
	});
	$("#factory_points_button").click(function(){
		var current_z_index = $("#factory_points_grid").css("z-index");
		if (current_z_index != 50) {
			for (var i = 0; i < longtabs.length; i++) {
				$("#"+longtabs[i].id).css("z-index", longtabs[i].z);
				$("#"+longtabs[i].chart).css("z-index", longtabs[i].z);
			}
			$("#factory_points_grid").css("z-index", 50);
			$("#factory_points_echarts").css("z-index", 50);
			points_echarts="factory_points_echarts";
		}
	})
	$("#begin_hour_add").click(function(){
		init_begin_date=getTimeDate_hour(init_begin_date,1);
		document.getElementById('beginTimeBox').value =init_begin_date ;
	})
	$("#begin_hour_minus").click(function(){
		init_begin_date=getTimeDate_hour(init_begin_date,-1);
		document.getElementById('beginTimeBox').value =init_begin_date ;
	})
	$("#end_hour_add").click(function(){
		init_end_date=getTimeDate_hour(init_end_date,1);
		document.getElementById('endTimeBox').value =init_end_date ;
	})
	$("#end_hour_minus").click(function(){
		init_end_date=getTimeDate_hour(init_end_date,-1);
		document.getElementById('endTimeBox').value =init_end_date ;
	})

	$("#factory_inst_points_button").click(function(){
		var current_z_index = $("#factory_inst_points_grid").css("z-index");
		if (current_z_index != 50) {
			for (var i = 0; i < longtabs.length; i++) {
				$("#"+longtabs[i].id).css("z-index", longtabs[i].z);
				$("#"+longtabs[i].chart).css("z-index", longtabs[i].z);
			}
			$("#factory_inst_points_grid").css("z-index", 50);
			$("#factory_inst_points_echarts").css("z-index", 50);
			points_echarts="factory_inst_points_echarts";
		}
	})
});


function getTimeDate_hour(data_time,delta_hour) {
	var datetime = new Date(data_time);
	var times = datetime.getTime()+3600000*delta_hour;

	var datetime = new Date(times);
	var year = datetime.getFullYear(); //得到年份
	var month = datetime.getMonth();//得到月份
	var date = datetime.getDate();//得到日期
	var day = datetime.getDay();//得到周几
	var hour = datetime.getHours();//得到小时
	var minu = datetime.getMinutes();//得到分钟
	var sec = datetime.getSeconds();//得到秒
	month = month + 1;
	if (month < 10) month = "0" + month;
	if (date < 10) date = "0" + date;
	if (hour < 10) hour = "0" + hour;
	if (minu < 10) minu = "0" + minu;
	if (sec < 10) sec = "0" + sec;
	var time = year + "-" + month + "-" + date+ " " + hour + ":" + minu + ":" + sec;
	return time;
}

function resizeScreen(force) {
    if (force || windowHeight != $(window).height() || windowWidth != $(window).width()) {
    	windowHeight = $(window).height();
    	windowWidth = $(window).width();

		if (bHSplit) { // default，横切纵排
        	$('#rightSplitter').jqxSplitter({
        		width: $(window).width(), 
        		height: $(window).height(), 
        		orientation: 'horizontal', 
        		panels: [{ size: ((upPanelPersent==1)?0.5:upPanelPersent)*100+'%', collapsible: false }] 
        	});
        	
			for (var i = 0; i < longtabs.length; i++) {
				document.getElementById(longtabs[i].id).style.height = windowHeight*upPanelPersent-66 + 'px';
        	}
			document.getElementById('factory_points_echarts').style.width = windowWidth-22 + 'px';
			document.getElementById('factory_points_echarts').style.height = windowHeight*(1-upPanelPersent)-27 + 'px';
			document.getElementById('factory_inst_points_echarts').style.width = windowWidth-22 + 'px';
			document.getElementById('factory_inst_points_echarts').style.height = windowHeight*(1-upPanelPersent)-27 + 'px';
			document.getElementById('left_div').style.marginTop =windowHeight*(1-upPanelPersent)-110 + 'px';
			document.getElementById('right_div').style.marginTop =windowHeight*(1-upPanelPersent)-110 + 'px';
			document.getElementById('right_div').style.marginLeft =windowWidth-156+'px';

			factory_points_echarts.resize();
			factory_inst_points_echarts.resize(); 
			if (upPanelPersent == 1) {
				$('#rightSplitter').jqxSplitter('collapse');
			}
		} 
    }
    
    typeof page_custom_resize === "function" ? page_custom_resize() : false;
}

function on_resize() {
	var window_height = $(window).height();
	var window_width = $(window).width();
	for (var i = 0; i < longtabs.length; i++) {
		$("#"+longtabs[i].id).css("width", window_width-24+'px');
	}
   	$("#longtabs").css("width", window_width-880+'px');
}

$(function() {
	on_resize();
  	window.onresize =function() {
  		on_resize();
   	    resizeScreen(false);	    
   	}	 
	document.getElementById('beginTimeBox').value =init_begin_date ;
	document.getElementById('endTimeBox').value =init_end_date;
	init();
	
	var tabsInstance = $("#longtabs > .tabs-container").dxTabs({
		dataSource: longtabs,
	    selectedIndex: 0,
	    onItemClick: function(e) {
	        var current_z_index = $("#"+e.itemData.id).css("z-index");
	        if (current_z_index != 50) {
	        	for (var i = 0; i < longtabs.length; i++) {
        			$("#"+longtabs[i].id).css("z-index", longtabs[i].z);
        			$("#"+longtabs[i].chart).css("z-index", longtabs[i].z);
        		}
        		$("#"+e.itemData.id).css("z-index", 50);
        		$("#"+e.itemData.chart).css("z-index", 50);
        		points_echarts=e.itemData.chart;
        	}
        }
    }).dxTabs("instance");
    $("#"+longtabs[0].chart).css("z-index", 50);
    $("#"+longtabs[0].id).css("z-index", 50);
});

function get_total_points_value() {
	var time =getTimeDate_8();
	var index = top.layer.msg('查询中...',{icon: 16, skin:'', shade:0.1});
	if(!ajax_json){
    	export1_ajax_url=path + "/gynh/get_points_value.do";
    }
	$.ajax({
//		url: path + "/gynh/get_points_value.do",
		url: export1_ajax_url,

		type: 'post',
		async: true,                     
		data: {
	    	time: time    
	    },
        success: function (data) {
        	var jsonStr = JSON.stringify(data,null,4);
        	console.log(jsonStr);
         	total_points_value = data['data'];
        	match_point_and_value();
        	factory_points_grid.option("dataSource", factory_point_list);
        	top.layer.close(index);
        },
        error: function () {
        	alert('error');
        }
    });
}         

function getTimeDate_8() {
	var times = new Date().getTime()-60000*minute-3600000*8;

	//var times = new Date().getTime()-3600000*(minute+8);
	var datetime = new Date(times);
	var year = datetime.getFullYear(); //得到年份
	var month = datetime.getMonth();//得到月份
	var date = datetime.getDate();//得到日期
	var day = datetime.getDay();//得到周几
	var hour = datetime.getHours();//得到小时
	var minu = datetime.getMinutes();//得到分钟
	var sec = datetime.getSeconds();//得到秒
	month = month + 1;
	if (month < 10) month = "0" + month;
	if (date < 10) date = "0" + date;
	if (hour < 10) hour = "0" + hour;
	if (minu < 10) minu = "0" + minu;
	if (sec < 10) sec = "0" + sec;
	var time = year + "-" + month + "-" + date+ " " + hour + ":" + minu + ":" + sec;
	return time;
}

function match_point_and_value() {
	var j = 0;
	
	for (var i = 0; i < factory_point_list.length; i++) {
		for (; j < total_points_value.length; j++) {
			if (factory_point_list[i].pn == total_points_value[j].p) {
				factory_point_list[i].t = total_points_value[j].t;
				factory_point_list[i].v = total_points_value[j].v;
				j++;
				break;
			} else if (factory_point_list[i].pn < total_points_value[j].p) {
				break;
			}
		}
	}
}

function init(){
	if(!ajax_json){
    	export2_ajax_url=path + "/gynh/get_export_list.do";
    }
	
	$.ajax({
//		url: path + "/gynh/get_export_list.do",
		url: export2_ajax_url,
		type: 'post',
		async: false,
		data: {param1:param1},
        success: function (data) {
        	var jsonStr = JSON.stringify(data,null,4);
        	console.log(jsonStr);
        	factory_point_list = data['factory_point_list'];
        	factory_inst_point_list = data['factory_inst_point_list'];
        },
        error: function () {
        	alert('error');
        }
    });
	//factory_points_grid
	factory_points_grid=$("#factory_points_grid").dxDataGrid({
		dataSource: null,
	    showBorders: true,
	    showColumnLines: true,
	    allowColumnReordering: true,
	    allowColumnResizing: true,
	    showRowLines: true,
	    rowAlternationEnabled: true,
	    columnResizingMode: "widget",
	    columnMinWidth: 20,
	    filterRow: { visible: true },
	    headerFilter: { visible: true },
	    paging: {
	        enabled: false
	    },
	    scrolling: {
	        mode: "virtual"
	    },
	    columnFixing: { 
	        enabled: true
	    },
	    searchPanel: {
	        visible: true,
	        width: 240,
	        placeholder: "搜索..."
	    },
	    columnChooser: {
	        enabled: true,
	        mode: "select"
	    },   
	    selection: {
            mode: "multiple",
            allowSelectAll: false,
            showCheckBoxesMode:'none',
        },
	    columns: [ 
	    	{
	            dataField: "pn",
	            validationRules: [{ type: "required" }, {
	                type: "pattern",
	            }],
	            caption: "点位编号"
	        },
	    	{
	            dataField: "pnm",
	            validationRules: [{ type: "required" }, {
	                type: "pattern",
	            }],
	            caption: "点位名称"
	        },
	    	{
	            dataField: "en",
	            validationRules: [{ type: "required" }, {
	                type: "pattern",
	            }],
	            caption: "能源"
	        },
	    	{
	            dataField: "f",
	            validationRules: [{ type: "required" }, {
	                type: "pattern",
	            }],
	            caption: "分厂"
	        },
	    	{
	            dataField: "d",
	            validationRules: [{ type: "required" }, {
	                type: "pattern",
	            }],
	            caption: "科室"
	        },
	    	{
	            dataField: "ln",
	            validationRules: [{ type: "required" }, {
	                type: "pattern",
	            }],
	            caption: "产线"
	        },
	    	{
	            dataField: "dv",
	            validationRules: [{ type: "required" }, {
	                type: "pattern",
	            }],
	            caption: "设备"
	        },
	    	{
	            dataField: "un",
	            validationRules: [{ type: "required" }, {
	                type: "pattern",
	            }],
	            caption: "单元"
	        },
	        {
                dataField: "t",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: "时间戳"
            },
            {
                dataField: "v",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: "最新值"
            }
	    ]
	}).dxDataGrid("instance");
	
	factory_points_grid.option("dataSource", factory_point_list);
	
	//factory_inst_points_grid
	factory_inst_points_grid=$("#factory_inst_points_grid").dxDataGrid({
		dataSource: null,
	    showBorders: true,
	    showColumnLines: true,
	    allowColumnReordering: true,
	    allowColumnResizing: true,
	    showRowLines: true,
	    rowAlternationEnabled: true,
	    columnResizingMode: "widget",
	    columnMinWidth: 20,
	    filterRow: { visible: true },
	    headerFilter: { visible: true },
	    paging: {
	        enabled: false
	    },
	    scrolling: {
	        mode: "virtual"
	    },
	    columnFixing: { 
	        enabled: true
	    },
	    searchPanel: {
	        visible: true,
	        width: 240,
	        placeholder: "搜索..."
	    },
	    columnChooser: {
	        enabled: true,
	        mode: "select"
	    },   
	    selection: {
            mode: "multiple",
            allowSelectAll: false,
            showCheckBoxesMode:'none',
        },
	    columns: [ 
	    	{
	            dataField: "pn",
	            validationRules: [{ type: "required" }, {
	                type: "pattern",
	            }],
	            caption: "点位编号"
	        },
	    	{
	            dataField: "pnm",
	            validationRules: [{ type: "required" }, {
	                type: "pattern",
	            }],
	            caption: "点位名称"
	        },
	    	{
	            dataField: "en",
	            validationRules: [{ type: "required" }, {
	                type: "pattern",
	            }],
	            caption: "能源"
	        },
	    	{
	            dataField: "f",
	            validationRules: [{ type: "required" }, {
	                type: "pattern",
	            }],
	            caption: "工厂"
	        },
	    	{
	            dataField: "d",
	            validationRules: [{ type: "required" }, {
	                type: "pattern",
	            }],
	            caption: "部门"
	        },
	    	{
	            dataField: "ln",
	            validationRules: [{ type: "required" }, {
	                type: "pattern",
	            }],
	            caption: "产线"
	        },
	    	{
	            dataField: "dv",
	            validationRules: [{ type: "required" }, {
	                type: "pattern",
	            }],
	            caption: "设备"
	        },
	    	{
	            dataField: "un",
	            validationRules: [{ type: "required" }, {
	                type: "pattern",
	            }],
	            caption: "单元"
	        },
	    ]
	}).dxDataGrid("instance");
	
	factory_inst_points_grid.option("dataSource", factory_inst_point_list);
}

function get_echarts_value(){
	if (points_echarts=="factory_points_echarts") {
		get_value("chart");
	}
	if (points_echarts=="factory_inst_points_echarts") {
		get_inst_value("chart");
	}
}

function get_points_value() {
	if (points_echarts=="factory_points_echarts") {
		get_value("export");
	}
	if (points_echarts=="factory_inst_points_echarts") {
		get_inst_value("export");
	}
}

function get_value(action) {
	for (var i = 0; i < sel_rows.length; i++) {
		if (typeof(sel_rows[i].series) != "undefined") {
			delete sel_rows[i].series;
		}
	}
	sel_rows.splice(0,sel_rows.length);
	
	sel_rows =JSON.parse(JSON.stringify(factory_points_grid.getSelectedRowsData()));
//	var point_set = new Set();
//	for (var i = 0; i < sel_rows.length; i++) {
//		point_set.add(sel_rows[i].pn);
//		// 优先从缓存中找
//		if (typeof(point_value_cache[sel_rows[i].pn]) != "undefined") {
//			sel_rows[i].series = point_value_cache[sel_rows[i].pn];
//		}
//	}
//
//	$.each(point_value_cache, function(_key) {
//	    var key = _key;
//	    var value = point_value_cache[_key];
//	    if (!point_set.has(_key)) {
//	    	delete point_value_cache[_key];
//	    }
//	});

	var begin_time = bjtime_to_gmt(init_begin_date);
	var end_time = bjtime_to_gmt(init_end_date);
	var index = top.layer.msg('查询中...',{icon: 16, skin:'', shade:0.1});
	$.ajax({
	    async: true,
        success: function (data) {
			for (var p = 0; p < sel_rows.length; p++) {
				if (typeof(sel_rows[p].series) != "undefined") {
					continue;
				}
				var point_number = sel_rows[p].pn;
				if(!ajax_json){
			    	export3_ajax_url=path + '/gynh/get_points_value_by_point_number.do';
			    }
				
			    $.ajax({
//			        url: path + '/gynh/get_points_value_by_point_number.do',
			        url: export3_ajax_url,
			        type: 'post',
			        async: false,
			        data: {
			        	begin_time: begin_time, 
			        	end_time: end_time, 
			        	point_number: point_number
			        },
			        success: function (data) {
			        	var jsonStr = JSON.stringify(data,null,4);
			        	console.log(jsonStr);
			            sel_rows[p].series = data['point_values'];
			            point_value_cache[point_number] = data['point_values'];
			        },
			        error: function () {
			        }
			    });
			}
			if (action=="chart") {
				set_echarts(sel_rows);
			}
			if (action=="export") {
				set_export(sel_rows);	
			}

			top.layer.close(index);
	    },  
	});
}

function get_inst_value(action){
	for (var i = 0; i < inst_sel_rows.length; i++) {
		if (typeof(inst_sel_rows[i].series) != "undefined") {
			delete inst_sel_rows[i].series;
		}
	}
	inst_sel_rows.splice(0, inst_sel_rows.length);
	inst_sel_rows =JSON.parse(JSON.stringify(factory_inst_points_grid.getSelectedRowsData()));
	
//	var point_set = new Set();
//	for (var i = 0; i < inst_sel_rows.length; i++) {
//		point_set.add(inst_sel_rows[i].pn);
//		// 优先从缓存中找
////		if (typeof(inst_point_value_cache[sel_rows[i].pn]) != "undefined") {
////			inst_sel_rows[i].series = inst_point_value_cache[sel_rows[i].pn];
////		}
//	}
//
//	$.each(inst_point_value_cache, function(_key) {
//	    var key = _key;
//	    var value = inst_point_value_cache[_key];
//	    if (!point_set.has(_key)) {
//	    	delete inst_point_value_cache[_key];
//	    }
//	});
	
	var begin_time = bjtime_to_gmt(init_begin_date);
	var end_time = bjtime_to_gmt(init_end_date);
	var index = top.layer.msg('查询中...',{icon: 16, skin:'', shade:0.1});
	$.ajax({
	    async: true,
        success: function (data) {
			for (var p = 0; p < inst_sel_rows.length; p++) {
				if (typeof(inst_sel_rows[p].series) != "undefined") {
					continue;
				}
				var point_number = inst_sel_rows[p].pn;
				if(!ajax_json){
			    	export4_ajax_url=path + '/gynh/get_inst_points_value_by_point_number.do';
			    }
			    $.ajax({
//			        url: path + '/gynh/get_inst_points_value_by_point_number.do',
			        url:export4_ajax_url ,
			        type: 'post',
			        async: false,
			        data: {
			        	begin_time: begin_time, 
			        	end_time: end_time, 
			        	point_number: point_number
			        },
			        success: function (data) {
			        	var jsonStr = JSON.stringify(data,null,4);
			        	console.log(jsonStr);
			        	inst_sel_rows[p].series = data['inst_point_values'];
			        	//inst_point_value_cache[point_number] = data['inst_point_values'];
			        },
			        error: function () {
			        }
			    });
			}
			if (action=="chart") {
				set_echarts(inst_sel_rows);
			}
			if (action=="export") {
				set_export(inst_sel_rows);
			}
			
			top.layer.close(index);
	    },  
	});
}

function set_echarts(sel_row) {
	var data_series = new Array();
	var colors = new Array();
	colors.push('#AE0000');
	colors.push('#FF44FF');
	colors.push('#0066CC');
	colors.push('#548C00');
	colors.push('#707038');
	colors.push('#5151A2');
	var min=sel_row[0].series[0].v;
	for (var p = 0; p < sel_row.length; p++) {
		var data_point = new Array();
		var data_value=new Array();
		var temp = new Array();
		
		for (var s = 0; s < sel_row[p].series.length; s++) {
			temp = [];
			temp.push(sel_row[p].series[s].t);
			temp.push(sel_row[p].series[s].v);
			if (min>sel_row[p].series[s].v) {
				min=sel_row[p].series[s].v;
			}
			data_point.push({value:temp});
		}
		for (var i=0;i<data_point.length-1;i++) {
			if(data_point[i].value[0]!=data_point[i+1].value[0]) {
				data_value.push(data_point[i]);
			}
		}
		data_value.push(data_point[data_point.length]);
		var c = p%6;
		data_series.push(
			{
				name:sel_row[p].pn,
				type: 'line',
				color: colors[c],
				showSymbol: false,
				hoverAnimation: false,
				data:data_value
			}
		);
	}
	min=min-1;
	if (min<0) {
		min=0;
	}
	if (points_echarts=="factory_points_echarts") {
		factory_points_option.series = data_series;
		factory_points_option.yAxis.min=min;
		
		factory_points_echarts.setOption(factory_points_option, true);
	}
	if (points_echarts=="factory_inst_points_echarts") {
		factory_inst_points_option.series = data_series;
		factory_inst_points_option.yAxis.min=min;
		factory_inst_points_echarts.setOption(factory_inst_points_option, true);
	}
}

function set_export(sel_row) {
	var str = "";
	var max_series = 0;
	for (var p = 0; p < sel_row.length; p++) {
		max_series = Math.max(sel_row[p].series.length, max_series);
	}

	for (var p = 0; p < sel_row.length; p++) {
		str += (',' + sel_row[p].pn + ',');
	}
	str += '\r\n';
	for (var p = 0; p < sel_row.length; p++) {
		str += (',' + sel_row[p].en + ',');
	}
	str += '\r\n';
	for (var p = 0; p < sel_row.length; p++) {
		str += (',' + sel_row[p].f + ',');
	}
	str += '\r\n';
	for (var p = 0; p < sel_row.length; p++) {
		str += (',' + sel_row[p].d + ',');
	}
	str += '\r\n';
	for (var p = 0; p < sel_row.length; p++) {
		str += (',' + sel_row[p].ln + ',');
	}
	str += '\r\n';
	for (var p = 0; p < sel_row.length; p++) {
		str += (',' + sel_row[p].dv + ',');
	}
	str += '\r\n';
	for (var p = 0; p < sel_row.length; p++) {
		str += (',' + sel_row[p].un + ',');
	}
	str += '\r\n';
	for (var p = 0; p < sel_row.length; p++) {
		str += (',' + sel_row[p].pnm + ',');
	}
	str += '\r\n';
	
	for (var s = 0; s < max_series; s++) {
		for (var p = 0; p < sel_row.length; p++) {
			if (s < sel_row[p].series.length) {
				str += (' ' + sel_row[p].series[s].t + ',' + sel_row[p].series[s].v + ',');
			} else {
				str += (',,');
			}
		}
		str += '\r\n';
	}
	
    let uri = 'data:text/csv;charset=utf-8,\ufeff' + encodeURIComponent(str);
    
    var link = document.createElement("a"); //通过创建a标签实现
    link.href = uri;

    link.download = "table.csv"; //下载文件名
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
}
