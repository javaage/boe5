/*
// 表的toolbar
var table_toolbar_chart = echarts.init(document.getElementById('table_toolbar_div'), "dark");

var table_toolbar_option = {
	toolbox: {
		show: true,
		x:'left',
		showTitle:true,
		borderWidth: 0,
		feature: {
			my_loadDbBtn: {
				show: true,
				title: '加载',
				icon: 'image://'+path+'/res/images/toolbarbtn/db.png',
				onclick: function() {
					var date = $('#beginTimeBox').val();
					loadSummaryData(date, date);
				}
			},
		}
	},
	series: [
		{
            name: ' ',
            type: 'pie',
		}
	]
};

table_toolbar_chart.setOption(table_toolbar_option);
//document.getElementById('table_toolbar_div').style.background='white';
*/

var currentPowerUnit = 'kWh';//kWh,mWh
var currentCDAUnit = 'L';//L,m³,k(m³)
var currentDIWUnit = 'L';//L,m³,k(m³)
var currentPN2Unit = 'L';//L,m³,k(m³)
var currentTimeUnit = 'M'; // H,M,S


var delShortcutMode = false;
var HisStateCons = {};
var HisCons = {};

var totalPower=0;
var totalDIW=0;
var totalCDA=0;
var totalPN2=0;


function loadSummaryData(begin, end) {
	if  (begin > end) {
		var temp=begin;
		begin = end;
		end = temp;
	}
	
	var beginYear = begin.substr(0,4)*1;
	var beginMonth = begin.substr(5,2)*1;
	var beginDay =  begin.substr(8,2)*1;
	var endYear = end.substr(0,4)*1;
	var endMonth = end.substr(5,2)*1;
	var endDay = end.substr(8,2)*1;
	
	var index = top.layer.msg('查询中...', {icon: 16, skin:'layui-layer-lan', shade:0.1});
	$.ajax({
		url: path + "/gynh/summary/get_data.do",
		type: 'post',
		async: false,
		data: {
			beginYear:beginYear, 
			beginMonth:beginMonth, 
			beginDay:beginDay, 
			endYear:endYear, 
			endMonth:endMonth, 
			endDay:endDay, 
			param1:param1,
		},
		success: function (data) {
			var cons_data = data['cons_data'];
			var state_time = data['state_time'];
			var state_cons = data['state_cons'];
			var hist_cons = data['hist_cons'];
			
			HisStateCons = {};
			for (var i = 0; i < state_cons.length; i++) {
				if (typeof(HisStateCons[state_cons[i].eqp_id]) == "undefined") {
					HisStateCons[state_cons[i].eqp_id] = {};
				}
				if (typeof(HisStateCons[state_cons[i].eqp_id][state_cons[i].energy]) == "undefined") {
					HisStateCons[state_cons[i].eqp_id][state_cons[i].energy] = {};
				}
				if (typeof(HisStateCons[state_cons[i].eqp_id][state_cons[i].energy][state_cons[i].state_class]) == "undefined") {
					HisStateCons[state_cons[i].eqp_id][state_cons[i].energy][state_cons[i].state_class] = new Array(31).fill('-');
				}
				
				var dateArray = state_cons[i].ymd.split("-");
				var idx = (new Date(dateArray[0], dateArray[1]-1, dateArray[2]) - new Date(endYear, endMonth-1, endDay))/(24*60*60*1000)+31-1;
				HisStateCons[state_cons[i].eqp_id][state_cons[i].energy][state_cons[i].state_class][idx] = state_cons[i].cons;
			}

			HisCons = {};
			for (var i = 0; i < hist_cons.length; i++) {
				if (typeof(HisCons[hist_cons[i].eqp_id]) == "undefined") {
					HisCons[hist_cons[i].eqp_id] = {};
				}
				if (typeof(HisCons[hist_cons[i].eqp_id][hist_cons[i].energy]) == "undefined") {
					HisCons[hist_cons[i].eqp_id][hist_cons[i].energy] = new Array(31).fill('-');
				}
				
				var dateArray = hist_cons[i].ymd.split("-");
				var idx = (new Date(dateArray[0], dateArray[1]-1, dateArray[2]) - new Date(endYear, endMonth-1, endDay))/(24*60*60*1000)+31-1;
				HisCons[hist_cons[i].eqp_id][hist_cons[i].energy][idx] = hist_cons[i].cons;
			}

			updateEqpChart();

			totalPower = 0;
			totalDIW = 0;
			totalCDA = 0;
			totalPN2 = 0;
			
			var runPower = 0;
			var pmPower = 0;
			var runDIW = 0;
			var pmDIW = 0;
			var runCDA = 0;
			var pmCDA = 0;
			var runPN2 = 0;
			var pmPN2 = 0;

			for (var i = 0; i < cons_data.length; i++) {
				if (cons_data[i].energy == 'Power') {
					totalPower += cons_data[i].cons;
					if (cons_data[i].state_class == "PM") {
						pmPower += cons_data[i].cons;
					} else {
						runPower += cons_data[i].cons;
					}
				} else if (cons_data[i].energy == 'DIW') {
					totalDIW += cons_data[i].cons;
					if (cons_data[i].state_class == "PM") {
						pmDIW += cons_data[i].cons;
					} else {
						runDIW += cons_data[i].cons;
					}
				} else if (cons_data[i].energy == 'CDA') {
					totalCDA += cons_data[i].cons;
					if (cons_data[i].state_class == "PM") {
						pmCDA += cons_data[i].cons;
					} else {
						runCDA += cons_data[i].cons;
					}
				} else if (cons_data[i].energy == 'PN2') {
					totalPN2 += cons_data[i].cons;
					if (cons_data[i].state_class == "PM") {
						pmPN2 += cons_data[i].cons;
					} else {
						runPN2 += cons_data[i].cons;
					}
				} 
			}
			
			$("#power_count").html(totalPower);
			$("#diw_count").html(totalDIW);
			$("#cda_count").html(totalCDA);
			$("#pn2_count").html(totalPN2);

			powerChart.setOption({                
                series:[
    				{
    		            name: 'RUN',
    		            type: 'bar',
    		            stack: '总量',
    		            data: [runPower]
    		        },
    		        {
    		            name: 'PM',
    		            type: 'bar',
    		            stack: '总量',
    		            data: [pmPower]
    		        },
                ]
            });

			diwChart.setOption({                
                series:[
    				{
    		            name: 'RUN',
    		            type: 'bar',
    		            stack: '总量',
    		            data: [runDIW]
    		        },
    		        {
    		            name: 'PM',
    		            type: 'bar',
    		            stack: '总量',
    		            data: [pmDIW]
    		        },
                ]
            });

			cdaChart.setOption({                
                series:[
    				{
    		            name: 'RUN',
    		            type: 'bar',
    		            stack: '总量',
    		            data: [runCDA]
    		        },
    		        {
    		            name: 'PM',
    		            type: 'bar',
    		            stack: '总量',
    		            data: [pmCDA]
    		        },
                ]
            });

			pn2Chart.setOption({                
                series:[
    				{
    		            name: 'RUN',
    		            type: 'bar',
    		            stack: '总量',
    		            data: [runPN2]
    		        },
    		        {
    		            name: 'PM',
    		            type: 'bar',
    		            stack: '总量',
    		            data: [pmPN2]
    		        },
                ]
            });

			var pmTime = 0;
			var runTime = 0;
			for (var i=0; i<state_time.length; i++) {
				if (state_time[i].state_class == 'PM') {
					pmTime += state_time[i].time_long;
				} else {
					runTime += state_time[i].time_long;
				}
			}
			
			stateOption.series = [
    				{
    		            name: 'RUN',
    		            type: 'bar',
    		            stack: '总量',
    		            data: [runTime]
    		        },
    		        {
    		            name: 'PM',
    		            type: 'bar',
    		            stack: '总量',
    		            data: [pmTime]
    		        },
                ];
			stateChart.setOption(stateOption, true);
			
			//stateChart.resize();
			
			
			top.layer.close(index);
			top.layer.msg('数据加载完成', {icon: 1, skin: 'layui-layer-lan'});
		},
		error: function () {
			top.layer.close(index);
			top.layer.msg('数据加载错误', {icon: 1, skin: 'layui-layer-lan'});
		}
	});
}

function updateEqpChart() {
	var xAxis = new Array();
	var date = $('#beginTimeBox').val();

	var endYear = date.substr(0,4)*1;
	var endMonth = date.substr(5,2)*1;
	var endDay = date.substr(8,2)*1;

	for (var i = 0; i < 31; i++) {
	    var endDate = new Date(endYear, endMonth-1, endDay);
	    xAxis[i] = getFormatDate2(endDate, -(31-1-i));
	    xAxis[i] = xAxis[i].substring(8);
	}
	powerHistChartOption.xAxis.data = xAxis;
	diwHistChartOption.xAxis.data = xAxis;
	cdaHistChartOption.xAxis.data = xAxis;
	pn2HistChartOption.xAxis.data = xAxis;
	
	powerHistChartOption.series.splice(0, powerHistChartOption.series.length);
	diwHistChartOption.series.splice(0, diwHistChartOption.series.length);
	cdaHistChartOption.series.splice(0, cdaHistChartOption.series.length);
	pn2HistChartOption.series.splice(0, pn2HistChartOption.series.length);

	var selEqpArr = eqpBox.option().value;
	for (var i = 0; i < selEqpArr.length; i++) {
		
		var eqp = eqpList[selEqpArr[i]].Name;
		
		if (typeof(HisStateCons[eqp]) != "undefined" && typeof(HisStateCons[eqp]['Power']) != "undefined" && typeof(HisStateCons[eqp]['Power']['RUN']) != "undefined") {
			powerHistChartOption.series.push(
				{
		            name:eqp+'_RUN',
		            animation: false,
		            type:'line',
		            data: HisStateCons[eqp]['Power']['RUN'],
		        }
			);
		}
		if (typeof(HisStateCons[eqp]) != "undefined" && typeof(HisStateCons[eqp]['Power']) != "undefined" && typeof(HisStateCons[eqp]['Power']['PM']) != "undefined") {
			powerHistChartOption.series.push(
				{
		            name:eqp+'_PM',
		            animation: false,
		            type:'line',
		            data: HisStateCons[eqp]['Power']['PM'],
		        }
			);
		}
		if (typeof(HisCons[eqp]) != "undefined" && typeof(HisCons[eqp]['Power']) != "undefined") {
			powerHistChartOption.series.push(
				{
		            name:eqp,
		            type:'line',
		            data: HisCons[eqp]['Power'],
		            animation: false,
		            markLine: {
		            	silent: true,
		            	animation: false,
		                data: [
		                    {type: 'average', name: '平均值'}
		                ]
		            }
		        }
			);
		}
		
		
		if (typeof(HisStateCons[eqp]) != "undefined" && typeof(HisStateCons[eqp]['DIW']) != "undefined" && typeof(HisStateCons[eqp]['DIW']['RUN']) != "undefined") {
			diwHistChartOption.series.push(
				{
		            name:eqp+'_RUN',
		            animation: false,
		            type:'line',
		            data: HisStateCons[eqp]['DIW']['RUN'],
		        }
			);
		}
		if (typeof(HisStateCons[eqp]) != "undefined" && typeof(HisStateCons[eqp]['DIW']) != "undefined" && typeof(HisStateCons[eqp]['DIW']['PM']) != "undefined") {
			diwHistChartOption.series.push(
				{
		            name:eqp+'_PM',
		            animation: false,
		            type:'line',
		            data: HisStateCons[eqp]['DIW']['PM'],
		        }
			);
		}
		if (typeof(HisCons[eqp]) != "undefined" && typeof(HisCons[eqp]['DIW']) != "undefined") {
			diwHistChartOption.series.push(
				{
		            name:eqp,
		            type:'line',
		            data: HisCons[eqp]['DIW'],
		            animation: false,
		            markLine: {
		            	silent: true,
		            	animation: false,
		                data: [
		                    {type: 'average', name: '平均值'}
		                ]
		            }
		        }
			);
		}
		
		if (typeof(HisStateCons[eqp]) != "undefined" && typeof(HisStateCons[eqp]['CDA']) != "undefined" && typeof(HisStateCons[eqp]['CDA']['RUN']) != "undefined") {
			cdaHistChartOption.series.push(
				{
		            name:eqp+'_RUN',
		            animation: false,
		            type:'line',
		            data: HisStateCons[eqp]['CDA']['RUN'],
		        }
			);
		}
		if (typeof(HisStateCons[eqp]) != "undefined" && typeof(HisStateCons[eqp]['CDA']) != "undefined" && typeof(HisStateCons[eqp]['CDA']['PM']) != "undefined") {
			cdaHistChartOption.series.push(
				{
		            name:eqp+'_PM',
		            animation: false,
		            type:'line',
		            data: HisStateCons[eqp]['CDA']['PM'],
		        }
			);
		}
		if (typeof(HisCons[eqp]) != "undefined" && typeof(HisCons[eqp]['CDA']) != "undefined") {
			cdaHistChartOption.series.push(
				{
		            name:eqp,
		            type:'line',
		            data: HisCons[eqp]['CDA'],
		            animation: false,
		            markLine: {
		            	silent: true,
		            	animation: false,
		                data: [
		                    {type: 'average', name: '平均值'}
		                ]
		            }
		        }
			);
		}

		if (typeof(HisStateCons[eqp]) != "undefined" && typeof(HisStateCons[eqp]['PN2']) != "undefined" && typeof(HisStateCons[eqp]['PN2']['RUN']) != "undefined") {
			pn2HistChartOption.series.push(
				{
		            name:eqp+'_RUN',
		            animation: false,
		            type:'line',
		            data: HisStateCons[eqp]['PN2']['RUN'],
		        }
			);
		}
		if (typeof(HisStateCons[eqp]) != "undefined" && typeof(HisStateCons[eqp]['PN2']) != "undefined" && typeof(HisStateCons[eqp]['PN2']['PM']) != "undefined") {
			pn2HistChartOption.series.push(
				{
		            name:eqp+'_PM',
		            animation: false,
		            type:'line',
		            data: HisStateCons[eqp]['PN2']['PM'],
		        }
			);
		}
		if (typeof(HisCons[eqp]) != "undefined" && typeof(HisCons[eqp]['PN2']) != "undefined") {
			pn2HistChartOption.series.push(
				{
		            name:eqp,
		            type:'line',
		            data: HisCons[eqp]['PN2'],
		            animation: false,
		            markLine: {
		            	silent: true,
		            	animation: false,
		                data: [
		                    {type: 'average', name: '平均值'}
		                ]
		            }
		        }
			);
		}
	}
	
	powerHistChart.setOption(powerHistChartOption, true);
	diwHistChart.setOption(diwHistChartOption, true);
	cdaHistChart.setOption(cdaHistChartOption, true);
	pn2HistChart.setOption(pn2HistChartOption, true);
}
/*
function UnitChange(e) {
	var dataSource = pivotgrid.getDataSource();
	var unit = e.value;
	if (e.id == 'select_diw') {
		currentUnit = currentDIWUnit;
	}
	if (e.id == 'select_cda') {
		currentUnit = currentCDAUnit;
	}
	if (e.id == 'select_pn2') {
		currentUnit = currentPN2Unit;
	}
	var upperCase = e.id.split("_");
	var fields = upperCase[1];
	var caption = upperCase[1].toUpperCase();
	if (unit == 'L' && currentUnit != 'L') {
		translate(currentUnit, 'L',fields)
		if (pageName == 'worktime') {
			for (var i = 0; i < dataSource.fields().length; i++) {
				if (dataSource.field(i).dataField == fields) {
					pivotgrid.option('dataSource.fields['+i+'].caption', ""+caption+"(L)");
					pivotgrid.option('dataSource.fields['+i+'].format.precision', 0);
					break;
				}
			}
			pivotgrid.option("dataSource.store", mark);
		} else if (pageName == "pointstatecons" || pageName == 'lotpoint') {
			pivotgrid.option("dataSource.store", mark);
		}
	}
	if (unit == 'm³' && currentUnit != 'm³') {
		translate(currentUnit, 'm³',fields)
		if (pageName == 'worktime') {
			for (var i = 0; i < dataSource.fields().length; i++) {
				if (dataSource.field(i).dataField == fields) {
					pivotgrid.option('dataSource.fields['+i+'].caption', ""+caption+"(m³)");
					pivotgrid.option('dataSource.fields['+i+'].format.precision', 2);
					break;
				}
			}
			pivotgrid.option("dataSource.store", mark);
		} else if (pageName == "pointstatecons" || pageName == 'lotpoint') {
			pivotgrid.option("dataSource.store", mark);
		}
	}
	if (unit == 'k(m³)' && currentUnit != 'k(m³)') {
		translate(currentUnit, 'k(m³)',fields)
		if (pageName == 'worktime') {
			for (var i = 0; i < dataSource.fields().length; i++) {
				if (dataSource.field(i).dataField == fields) {
					pivotgrid.option('dataSource.fields['+i+'].caption', ""+caption+"(k(m³))");
					pivotgrid.option('dataSource.fields['+i+'].format.precision', 2);
					break;
				}
			}
			pivotgrid.option("dataSource.store", mark);
		} else if (pageName == "pointstatecons" || pageName == 'lotpoint') {
			pivotgrid.option("dataSource.store", mark);
		}
	}
}

function translate(from, to, fields) {
	if (from == 'L' && to == 'm³') {
		for (var i = 0; i < mark.length; i++) {
			if (pageName == 'worktime') {
				if (fields == 'cda') {
					mark[i].cda = mark[i].cda/1000.0  ;
				}
				if (fields == 'diw') {
					mark[i].diw = mark[i].diw/1000.0 ;
				}
				if (fields == 'pn2') {
					mark[i].pn2 = mark[i].pn2/1000.0;
				}
			} else if (pageName == "pointstatecons" || pageName == 'lotpoint') {
				if (mark[i].e == fields.toUpperCase()) {
					mark[i].c = mark[i].c/1000.0;
				}
			}
		}
	}
	if (from == 'L' && to == 'k(m³)') {
		for (var i = 0; i < mark.length; i++) {
			if (pageName == 'worktime') {
				if (fields == 'cda') {
					mark[i].cda = mark[i].cda/1000000.0  ;
				}
				if (fields == 'diw') {
					mark[i].diw = mark[i].diw/1000000.0 ;
				}
				if (fields == 'pn2') {
					mark[i].pn2 = mark[i].pn2/1000000.0;
				}
			} else if (pageName == "pointstatecons" || pageName == 'lotpoint') {
				if (mark[i].e == fields.toUpperCase()) {
					mark[i].c = mark[i].c/1000000.0;
				}
			}
		}
	}
	if (from == 'm³' && to == 'L') {
		for (var i = 0; i < mark.length; i++) {
			if (pageName == 'worktime') {
				if (fields == 'cda') {
					mark[i].cda = mark[i].cda*1000.0  ;
				}
				if (fields == 'diw') {
					mark[i].diw = mark[i].diw*1000.0 ;
				}
				if (fields == 'pn2') {
					mark[i].pn2 = mark[i].pn2*1000.0 ;
				}
			} else if (pageName == "pointstatecons" || pageName == 'lotpoint') {
				if (mark[i].e == fields.toUpperCase()) {
					mark[i].c = mark[i].c*1000.0 ;
				}
			}
		}
	}
	if (from == 'm³' && to == 'k(m³)') {
		for (var i = 0; i < mark.length; i++) {
			if (pageName == 'worktime') {
				if (fields == 'cda') {
					mark[i].cda = mark[i].cda/1000.0 ;
				}
				if (fields == 'diw') {
					mark[i].diw = mark[i].diw/1000.0 ;
				}
				if (fields == 'pn2') {
					mark[i].pn2 = mark[i].pn2/1000.0 ;
				}			
			} else if (pageName == "pointstatecons" || pageName == 'lotpoint') {
				if (mark[i].e == fields.toUpperCase()) {
					mark[i].c = mark[i].c/1000.0 ;
				}
			}
		}
	}
	if (from == 'k(m³)' && to == 'L') {
		for (var i = 0; i < mark.length; i++) {
			if (pageName == 'worktime') {
				if (fields == 'cda') {
					mark[i].cda = mark[i].cda*1000000.0 ;
				}
				if (fields == 'diw') {
					mark[i].diw = mark[i].diw*1000000.0 ;
				}
				if (fields == 'pn2') {
					mark[i].pn2 = mark[i].pn2*1000000.0 ;
				}	
			} else if (pageName == "pointstatecons" || pageName == 'lotpoint') {
				if (mark[i].e == fields.toUpperCase()) {
					mark[i].c = mark[i].c*1000000.0 ;
				}
			}
		}
	}
	if (from == 'k(m³)' && to == 'm³') {
		for (var i = 0; i < mark.length; i++) {
			if (pageName == 'worktime') {
				if (fields == 'cda') {
					mark[i].cda = mark[i].cda*1000.0 ;
				}
				if (fields == 'diw') {
					mark[i].diw = mark[i].diw*1000.0 ;
				}
				if (fields == 'pn2') {
					mark[i].pn2 = mark[i].pn2*1000.0 ;
				}	
			} else if (pageName == "pointstatecons" ||pageName == 'lotpoint') {
				if (mark[i].e == fields.toUpperCase()) {
					mark[i].c = mark[i].c*1000.0 ;
				}
			}
		}
	}
	if (fields == 'cda') {
		currentCDAUnit = to;
	}
	if (fields == 'diw') {
		currentDIWUnit = to;
	}
	if (fields == 'pn2') {
		currentPN2Unit = to;
	}
}
*/

window.onresize = function() {
	document.getElementById('state_chart').style.width = 20 + 'px';
	document.getElementById('state_chart').style.height = 20 + 'px';

	document.getElementById('power_hist_chart').style.width = 20 + 'px';
	document.getElementById('power_hist_chart').style.height = 20 + 'px';
	document.getElementById('diw_hist_chart').style.width = 20 + 'px';
	document.getElementById('diw_hist_chart').style.height = 20 + 'px';
	document.getElementById('cda_hist_chart').style.width = 20 + 'px';
	document.getElementById('cda_hist_chart').style.height = 20 + 'px';
	document.getElementById('pn2_hist_chart').style.width = 20 + 'px';
	document.getElementById('pn2_hist_chart').style.height = 20 + 'px';
	
	document.getElementById('state_chart').style.width = document.getElementById('state_chart').parentNode.offsetWidth + 'px';
	document.getElementById('state_chart').style.height = document.getElementById('state_chart').parentNode.offsetHeight + 'px';
	stateChart.resize();

	document.getElementById('power_hist_chart').style.width = document.getElementById('power_hist_chart').parentNode.offsetWidth-4 + 'px';
	document.getElementById('power_hist_chart').style.height = document.getElementById('power_hist_chart').parentNode.offsetHeight + 'px';
	powerHistChart.resize();
	document.getElementById('diw_hist_chart').style.width = document.getElementById('diw_hist_chart').parentNode.offsetWidth-4 + 'px';
	document.getElementById('diw_hist_chart').style.height = document.getElementById('diw_hist_chart').parentNode.offsetHeight + 'px';
	diwHistChart.resize();
	document.getElementById('cda_hist_chart').style.width = document.getElementById('cda_hist_chart').parentNode.offsetWidth-4 + 'px';
	document.getElementById('cda_hist_chart').style.height = document.getElementById('cda_hist_chart').parentNode.offsetHeight + 'px';
	cdaHistChart.resize();
	document.getElementById('pn2_hist_chart').style.width = document.getElementById('pn2_hist_chart').parentNode.offsetWidth-4 + 'px';
	document.getElementById('pn2_hist_chart').style.height = document.getElementById('pn2_hist_chart').parentNode.offsetHeight + 'px';
	pn2HistChart.resize();
}

