

function test() {
	var seconds = 15*60;
	var end_time   = get_offset_seconds_time(-8*3600); //- 8 hours
	var begin_time = get_offset_seconds_time(-8*3600-seconds-15*60); //多减15分钟
    $.ajax({
        url: ajax_json 
        	? (path + '/res/json/hw_flow_graph.json')
        	: (path + "/gynh/mchw_flow_graph/get_data.do"),
        type: 'post',
        async: false,
        data: {
			begin_time: begin_time,
			end_time: end_time, 
			seconds: seconds,
			error_interval: 10, //采样误差n秒内
		    system: 'HW'
        },
        success: function(data) {
        	return JSON.stringify(data,null,4);
        },
        error: function() {
        	alert('error');
        }
    });
}
