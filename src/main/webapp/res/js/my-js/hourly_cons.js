
/*var time = getCurrentDate();*/
var h=true;
var windowHeight = 0;
var windowWidth = 0;
var upPanelPersent = 0.5;
var leftPanelPersent = 0.5;
var bHSplit = true; //true:H横切（默认）， false:V纵切
var minute=5;

var department_points_list;
var eqp_id_points_list;
var device_points_list;
var device_unit_points_list;

var department_qty_list=new Array();
var eqp_id_qty_list=new Array();
var device_qty_list=new Array();
var device_unit_qty_list=new Array();

var department_grid;
var eqp_id_grid;
var device_grid;
var device_unit_grid;

var data_series=new Array();

var tab_on = "department_grid";
var longtabs = [
    { idx:1, z:11, id: "department_grid", text: "科室", echartsid: "echarts_department",state_echartsid:"echarts_department_state"},
    { idx:2, z:12, id: "eqp_id_grid", text: "产线", echartsid: "echarts_eqp_id",state_echartsid:"echarts_eqp_id_state"},
    { idx:3, z:13, id: "device_grid", text: "设备", echartsid: "echarts_device",state_echartsid:"echarts_device_state"},
    { idx:4, z:14, id: "device_unit_grid", text: "单元", echartsid: "echarts_device_unit",state_echartsid:"echarts_device_unit_state"},
];

var eqp_list = [];
var device_list = [];
var device_unit_list = [];

$(document).ready(function() {
	
    typeof page_custom_init === "function" ? page_custom_init() : false;
    
	resizeScreen(true);

	$('#rightSplitter').on('resize', function (event) {
	    var panels = event.args.panels;

		if  (bHSplit) { //default,横切纵排
			upPanelPersent = panels[0].size/(panels[0].size+panels[1].size);
			for (var i = 0; i < longtabs.length; i++) {
				document.getElementById(longtabs[i].id).style.height = panels[0].size-5 + 'px';
			}
			document.getElementById('echarts_department_state').style.width = windowWidth + 'px';
			document.getElementById('echarts_department_state').style.height = panels[1].size/2-2 + 'px';
			document.getElementById('echarts_eqp_id_state').style.width = windowWidth + 'px';
			document.getElementById('echarts_eqp_id_state').style.height = panels[1].size/2-2 + 'px';
			document.getElementById('echarts_device_state').style.width = windowWidth + 'px';
			document.getElementById('echarts_device_state').style.height = panels[1].size/2-2 + 'px';
			document.getElementById('echarts_device_unit_state').style.width = windowWidth + 'px';
			document.getElementById('echarts_device_unit_state').style.height = panels[1].size/2-2 + 'px';
			
			document.getElementById('echarts_department').style.marginTop  = panels[1].size/2-2 + 'px';
			document.getElementById('echarts_eqp_id').style.marginTop  = panels[1].size/2-2 + 'px';
			document.getElementById('echarts_device').style.marginTop  = panels[1].size/2-2 + 'px';
			document.getElementById('echarts_device_unit').style.marginTop  = panels[1].size/2-2 + 'px';
			
			document.getElementById('echarts_department').style.width = windowWidth + 'px';
			document.getElementById('echarts_department').style.height = panels[1].size/2-2 + 'px';
			document.getElementById('echarts_eqp_id').style.width = windowWidth + 'px';
			document.getElementById('echarts_eqp_id').style.height = panels[1].size/2-2 + 'px';
			document.getElementById('echarts_device').style.width = windowWidth + 'px';
			document.getElementById('echarts_device').style.height = panels[1].size/2-2 + 'px';
			document.getElementById('echarts_device_unit').style.width = windowWidth + 'px';
			document.getElementById('echarts_device_unit').style.height = panels[1].size/2-2 + 'px';
			
			echarts_department_state.resize(); 
			echarts_eqp_id_state.resize(); 
			echarts_device_state.resize(); 
			echarts_device_unit_state.resize(); 

			echarts_department.resize(); 
			echarts_eqp_id.resize(); 
			echarts_device.resize(); 
			echarts_device_unit.resize(); 
		} 
	});
});

function resizeScreen(force) {
    if (force || windowHeight != $(window).height() || windowWidth != $(window).width()) {
    	windowHeight = $(window).height();
    	windowWidth = $(window).width();

		if (bHSplit) { // default，横切纵排
        	$('#rightSplitter').jqxSplitter({
        		width: $(window).width(), 
        		height: $(window).height(), 
        		orientation: 'horizontal', 
        		panels: [{ size: ((upPanelPersent==1)?0.5:upPanelPersent)*100+'%', collapsible: false }] 
        	});
        	for (var i = 0; i < longtabs.length; i++) {
				document.getElementById(longtabs[i].id).style.height = windowHeight*upPanelPersent-5 + 'px';
        	}
        	
			document.getElementById('echarts_department_state').style.width = windowWidth + 'px';
			document.getElementById('echarts_department_state').style.height = windowHeight*(1-upPanelPersent)/2-2 + 'px';
			document.getElementById('echarts_eqp_id_state').style.width = windowWidth + 'px';
			document.getElementById('echarts_eqp_id_state').style.height = windowHeight*(1-upPanelPersent)/2-2 + 'px';	
			document.getElementById('echarts_device_state').style.width = windowWidth + 'px';
			document.getElementById('echarts_device_state').style.height = windowHeight*(1-upPanelPersent)/2-2 + 'px';	
			document.getElementById('echarts_device_unit_state').style.width = windowWidth + 'px';
			document.getElementById('echarts_device_unit_state').style.height = windowHeight*(1-upPanelPersent)/2-2 + 'px';
			
			document.getElementById('echarts_department').style.marginTop  = windowHeight*(1-upPanelPersent)/2-2 + 'px';
			document.getElementById('echarts_eqp_id').style.marginTop  = windowHeight*(1-upPanelPersent)/2-2 + 'px';
			document.getElementById('echarts_device').style.marginTop  = windowHeight*(1-upPanelPersent)/2-2 + 'px';
			document.getElementById('echarts_device_unit').style.marginTop  = windowHeight*(1-upPanelPersent)/2-2 + 'px';
			
			document.getElementById('echarts_department').style.width = windowWidth + 'px';
			document.getElementById('echarts_department').style.height = windowHeight*(1-upPanelPersent)/2-2 + 'px';
			document.getElementById('echarts_eqp_id').style.width = windowWidth + 'px';
			document.getElementById('echarts_eqp_id').style.height = windowHeight*(1-upPanelPersent)/2-2 + 'px';	
			document.getElementById('echarts_device').style.width = windowWidth + 'px';
			document.getElementById('echarts_device').style.height = windowHeight*(1-upPanelPersent)/2-2 + 'px';	
			document.getElementById('echarts_device_unit').style.width = windowWidth + 'px';
			document.getElementById('echarts_device_unit').style.height = windowHeight*(1-upPanelPersent)/2-2 + 'px';
			
			echarts_department_state.resize(); 
			echarts_eqp_id_state.resize(); 
			echarts_device_state.resize(); 
			echarts_device_unit_state.resize(); 

			echarts_department.resize(); 
			echarts_eqp_id.resize(); 
			echarts_device.resize(); 
			echarts_device_unit.resize(); 
			if (upPanelPersent == 1) {
				$('#rightSplitter').jqxSplitter('collapse');
			}
		} 	
    }
    typeof page_custom_resize === "function" ? page_custom_resize() : false;
}

function on_resize() {
	var window_height = $(window).height();
	var window_width = $(window).width();
	for (var i = 0; i < longtabs.length; i++) {
		$("#"+longtabs[i].id).css("width", window_width-2+'px');
	}
   	$("#longtabs").css("width", window_width-156-385+'px');
}

$(function() {
	on_resize();
	window.onresize =function() {
  		on_resize();
   	    resizeScreen(false);
   	}
   	init();
    var tabsInstance = $("#longtabs > .tabs-container").dxTabs({
        dataSource: longtabs,
        selectedIndex: 0,
        onItemClick: function(e) {
        	var current_z_index = $("#"+e.itemData.id).css("z-index");
        	if (current_z_index != 50) {
        		for (var i = 0; i < longtabs.length; i++) {
        			$("#"+longtabs[i].id).css("z-index", longtabs[i].z);
        			$("#"+longtabs[i].echartsid).css("z-index", longtabs[i].z);
        			$("#"+longtabs[i].state_echartsid).css("z-index", longtabs[i].z);
        		}
        		$("#"+e.itemData.id).css("z-index", 50);
        		$("#"+e.itemData.echartsid).css("z-index", 50);
        		$("#"+e.itemData.state_echartsid).css("z-index", 50);
        		tab_on=e.itemData.id;
        	}
        }
    }).dxTabs("instance");
    
    $("#"+longtabs[0].id).css("z-index", 50);
    $("#"+longtabs[0].echartsid).css("z-index", 50);
    $("#"+longtabs[0].state_echartsid).css("z-index", 50);

});

function get_times(){
	
	begin_day=getTimeDate(-24).substring(0,10); 
	end_day=getTimeDate(0).substring(0,10); 
	begin_hour=getTimeDate(-24).substring(11,13); 
	end_hour=getTimeDate(0).substring(11,13); 
	
	begin_datetime=getTimeDate(-24).substring(0,13)+':00:00';
	end_datetime=getTimeDate(0).substring(0,13)+':00:00'; 
	
	var time_num =begin_hour;
	times_num=new Array();
	for(var i=0;i<24;i++){
		times_num.push(time_num+"");
		time_num++;
		if(time_num>=24){
			time_num=0;
		}	
	}
}

function init(){
	
	get_times();
	
	$.ajax({
		url: path + "/gynh/get_points_list.do",
		type: 'post',
		async: false, 
		data: {
        	begin_day: begin_day, 
        	end_day: end_day,
        	begin_hour: begin_hour, 
        	end_hour: end_hour,
        	factory:factory,
        	energy:energy
        },
        success: function (data) {
        	department_points_list = data['department_points_list'];
        	eqp_id_points_list = data['eqp_id_points_list'];
        	device_points_list = data['device_points_list'];
        	device_unit_points_list = data['device_unit_points_list'];
        },
        error: function () {
        	alert('error');
        }
    });
	set_grid();
}

function init_department_qty(selected_data){
	for(var i=0;i<selected_data.length;i++)
	{
		var department=selected_data[i].d;
		$.ajax({
			url: path + "/gynh/get_department_qty_list.do",
			type: 'post',
			async: false, 
			data: {
	        	begin_datetime: begin_datetime, 
	        	end_datetime: end_datetime,
	        	factory:factory,
	        	department:department
	        },
	        success: function (data) {
	        	var str=data['department_qty_list'];
	        	department_qty_list[i]=str[0];
	        },
	        error: function () {
	        	alert('error');
	        }
	    });
		
	}
}

function init_eqp_id_qty(selected_data){
	for(var i=0;i<selected_data.length;i++)
	{
		var department=selected_data[i].d;
		var eqp_id=selected_data[i].e;

		$.ajax({
			url: path + "/gynh/get_eqp_id_qty_list.do",
			type: 'post',
			async: false, 
			data: {
	        	begin_datetime: begin_datetime, 
	        	end_datetime: end_datetime,
	        	factory:factory,
	        	department:department,
	        	eqp_id:eqp_id,
	        },
	        success: function (data) {
	        	var str=data['eqp_id_qty_list'];
	        	eqp_id_qty_list[i]=str[0];
	        },
	        error: function () {
	        	alert('error');
	        }
	    });
		
	}
}

function init_device_qty(selected_data){
	for(var i=0;i<selected_data.length;i++)
	{
		var department=selected_data[i].d;
		var eqp_id=selected_data[i].e;
		var device=selected_data[i].de;
		$.ajax({
			url: path + "/gynh/get_device_qty_list.do",
			type: 'post',
			async: false, 
			data: {
	        	begin_datetime: begin_datetime, 
	        	end_datetime: end_datetime,
	        	factory:factory,
	        	department:department,
	        	eqp_id:eqp_id,
	        	device:device,
	        },
	        success: function (data) {
	        	var str=data['device_qty_list'];
	        	device_qty_list[i]=str[0];
	        },
	        error: function () {
	        	alert('error');
	        }
	    });
		
	}
}

function init_device_unit_qty(selected_data){
	for(var i=0;i<selected_data.length;i++)
	{
		var department=selected_data[i].d;
		var eqp_id=selected_data[i].e;
		var device=selected_data[i].de;
		var device_unit=selected_data[i].du;
		$.ajax({
			url: path + "/gynh/get_device_unit_qty_list.do",
			type: 'post',
			async: false, 
			data: {
	        	begin_datetime: begin_datetime, 
	        	end_datetime: end_datetime,
	        	factory:factory,
	        	department:department,
	        	eqp_id:eqp_id,
	        	device:device,
	        	device_unit:device_unit
	        },
	        success: function (data) {
	        	var str=data['device_unit_qty_list'];
	        	device_unit_qty_list[i]=str[0];
	        },
	        error: function () {
	        	alert('error');
	        }
	    });
		
	}
}

function get_points_value(){
	
	var selected_data;
	data_series=new Array();
	var cons_style='line';
	var qty_style='bar';
	if (tab_on=="department_grid") {
		selected_data = department_grid.getSelectedRowsData();
		set_data_series(selected_data,cons_style,'0');
		init_department_qty(selected_data);
		set_data_series(department_qty_list,qty_style,'1');
		set_echarts_department();
	} if (tab_on=="eqp_id_grid") {
		selected_data = eqp_id_grid.getSelectedRowsData();
		set_data_series(selected_data,cons_style,'0');
		init_eqp_id_qty(selected_data);
		set_data_series(eqp_id_qty_list,qty_style,'1');
		set_echarts_eqp_id();
		eqp_list = [];
		for (var i = 0; i < selected_data.length; i++) {
			eqp_list.push(selected_data[i].e);
		}
		load_state_time();
	} if (tab_on=="device_grid") {
		selected_data = device_grid.getSelectedRowsData();
		set_data_series(selected_data,cons_style,'0');
		init_device_qty(selected_data);
		set_data_series(device_qty_list,qty_style,'1');
		set_echarts_device();
//		device_list=[];
//		for (var i=0; i < selected_data.length;i++){
//			device_list.push(selected_data[i].e);
//		}
//		load_state_time();
	} if (tab_on=="device_unit_grid") {
		selected_data = device_unit_grid.getSelectedRowsData();
		set_data_series(selected_data,cons_style,'0');
		init_device_unit_qty(selected_data);
		set_data_series(device_unit_qty_list,qty_style,'1');
		set_echarts_device_unit();
//		device_unit_list=[];
//		for (var i=0; i < selected_data.length;i++){
//			device_unit_list.push(selected_data[i].e);
//		}
//		load_state_time();
	}
}

function set_echarts_department(){
	
	option_department.xAxis[0].data=times_num;
	option_department.series = data_series;
	echarts_department.setOption(option_department, true);
}
function set_echarts_eqp_id(){
	
	option_eqp_id.xAxis[0].data=times_num;
	option_eqp_id.series = data_series;
	echarts_eqp_id.setOption(option_eqp_id, true);
}
function set_echarts_device(){
	
	option_device.xAxis[0].data=times_num;
	option_device.series = data_series;
	echarts_device.setOption(option_device, true);
}
function set_echarts_device_unit(){
	
	option_device_unit.xAxis[0].data=times_num;
	option_device_unit.series = data_series;
	echarts_device_unit.setOption(option_device_unit, true);
}
function set_data_series(selected_data,style,y){
	
	var colors=new Array();
	colors.push('#AE0000');
	colors.push('#FF44FF');
	colors.push('#0066CC');
	colors.push('#548C00');
	colors.push('#707038');
	colors.push('#5151A2');
	
	for (var p = 0; p < selected_data.length; p++) {
		if(selected_data[p]==null||selected_data[p]==''){
			continue;
		}
		var temp = new Array();
		
		temp.push(selected_data[p].h0);
		temp.push(selected_data[p].h1);
		temp.push(selected_data[p].h2);
		temp.push(selected_data[p].h3);
		temp.push(selected_data[p].h4);
		temp.push(selected_data[p].h5);
		temp.push(selected_data[p].h6);
		temp.push(selected_data[p].h7);
		temp.push(selected_data[p].h8);
		temp.push(selected_data[p].h9);
		temp.push(selected_data[p].h10);
		temp.push(selected_data[p].h11);
		temp.push(selected_data[p].h12);
		temp.push(selected_data[p].h13);
		temp.push(selected_data[p].h14);
		temp.push(selected_data[p].h15);
		temp.push(selected_data[p].h16);
		temp.push(selected_data[p].h17);
		temp.push(selected_data[p].h18);
		temp.push(selected_data[p].h19);
		temp.push(selected_data[p].h20);
		temp.push(selected_data[p].h21);
		temp.push(selected_data[p].h22);
		temp.push(selected_data[p].h23);	
		var c=p%6;
		var name=null;
		if (selected_data[p].du!=null&&selected_data[p].du!="") {
			name=selected_data[p].du;
		} else if (selected_data[p].de!=null&&selected_data[p].de!="") {
			name=selected_data[p].de;
		} else if (selected_data[p].e!=null&&selected_data[p].e!="") {
			name=selected_data[p].e;
		} else if (selected_data[p].d!=null&&selected_data[p].d!="") {
			name=selected_data[p].d;
		}
		if (style=='line') {
			data_series.push({name:name, yAxisIndex: y,type: style,color: colors[c],itemStyle: {opacity:1},showSymbol: false,hoverAnimation: false,data:temp});
		}
		if (style=='bar') {
			data_series.push({name:name, yAxisIndex: y,type: style,color: colors[c],itemStyle: {opacity:0.72},showSymbol: false,hoverAnimation: false,data:temp});
		}
	}
}
function onClickEnergyBtn(str){
	energy=str;
	init();
}

function set_grid(){
	department_grid =$("#department_grid").dxDataGrid({
		dataSource: department_points_list,
        showBorders: true,
        showColumnLines: true,
        allowColumnReordering: true,
        allowColumnResizing: true,
        showRowLines: true,
        rowAlternationEnabled: true,
	    columnResizingMode: "widget",
        columnAutoWidth: true,
        filterRow: { visible: true },
        headerFilter: { visible: true },
        "export": {
            enabled: true,
            fileName: "Orders"
        },
        paging: {
            enabled: false
        },
        scrolling: {
            mode: "virtual"
        },
        columnFixing: { 
            enabled: true
        },
        searchPanel: {
            visible: true,
            width: 240,
            placeholder: "搜索..."
        },
        columnChooser: {
            enabled: true,
            mode: "select"
        },
        selection: {
        	mode: "multiple",
            allowSelectAll: false,
            showCheckBoxesMode:'none',
        },

        columns: [ 
        	{
                dataField: "d",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: "科室"
            },
            {
                dataField: "h0",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[0]
            },
            {
                dataField: "h1",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[1]
            }
            ,
            {
                dataField: "h2",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[2]
            }
            ,
            {
                dataField: "h3",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[3]
            }
            ,
            {
                dataField: "h4",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption:times_num[4]
            }
            ,
            {
                dataField: "h5",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption:times_num[5]
            },
            {
                dataField: "h6",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[6]
            },
            {
                dataField: "h7",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[7]
            } ,
            {
                dataField: "h8",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[8]
            },
            {
                dataField: "h9",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[9]
            },
            {
                dataField: "h10",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[10]
            } ,
            {
                dataField: "h11",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[11]
            },
            {
                dataField: "h12",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[12]
            },
            {
                dataField: "h13",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[13]
            },
            {
                dataField: "h14",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[14]
            },
            {
                dataField: "h15",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[15]
            },
            {
                dataField: "h16",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[16]
            },
            {
                dataField: "h17",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[17]
            },
            {
                dataField: "h18",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[18]
            },
            {
                dataField: "h19",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[19]
            },
            {
                dataField: "h20",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[20]
            },
            {
                dataField: "h21",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[21]
            },
            {
                dataField: "h22",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[22]
            },
            {
                dataField: "h23",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[23]
            }
            
        ],
    }).dxDataGrid("instance");
	
	eqp_id_grid =$("#eqp_id_grid").dxDataGrid({
		dataSource: eqp_id_points_list,
        showBorders: true,
        showColumnLines: true,
        allowColumnReordering: true,
        allowColumnResizing: true,
        showRowLines: true,
        rowAlternationEnabled: true,
	    columnResizingMode: "widget",
	    columnAutoWidth: true,
        filterRow: { visible: true },
        headerFilter: { visible: true },
        "export": {
            enabled: true,
            fileName: "Orders"
        },
        paging: {
            enabled: false
        },
        scrolling: {
            mode: "virtual"
        },
        columnFixing: { 
            enabled: true
        },
        searchPanel: {
            visible: true,
            width: 240,
            placeholder: "搜索..."
        },
        columnChooser: {
            enabled: true,
            mode: "select"
        },
        selection: {
        	mode: "multiple",
            allowSelectAll: false,
            showCheckBoxesMode:'none',
        },

        columns: [ 
        	{
                dataField: "d",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: "科室"
            },
            {
                dataField: "e",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: "产线"
            },
            {
                dataField: "h0",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[0]
            },
            {
                dataField: "h1",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[1]
            }
            ,
            {
                dataField: "h2",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[2]
            }
            ,
            {
                dataField: "h3",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[3]
            }
            ,
            {
                dataField: "h4",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption:times_num[4]
            }
            ,
            {
                dataField: "h5",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption:times_num[5]
            },
            {
                dataField: "h6",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[6]
            },
            {
                dataField: "h7",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[7]
            } ,
            {
                dataField: "h8",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[8]
            },
            {
                dataField: "h9",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[9]
            },
            {
                dataField: "h10",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[10]
            } ,
            {
                dataField: "h11",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[11]
            },
            {
                dataField: "h12",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[12]
            },
            {
                dataField: "h13",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[13]
            },
            {
                dataField: "h14",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[14]
            },
            {
                dataField: "h15",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[15]
            },
            {
                dataField: "h16",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[16]
            },
            {
                dataField: "h17",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[17]
            },
            {
                dataField: "h18",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[18]
            },
            {
                dataField: "h19",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[19]
            },
            {
                dataField: "h20",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[20]
            },
            {
                dataField: "h21",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[21]
            },
            {
                dataField: "h22",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[22]
            },
            {
                dataField: "h23",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[23]
            }
            
        ],
    }).dxDataGrid("instance");
	
	device_grid =$("#device_grid").dxDataGrid({
		dataSource: device_points_list,
        showBorders: true,
        showColumnLines: true,
        allowColumnReordering: true,
        allowColumnResizing: true,
        showRowLines: true,
        rowAlternationEnabled: true,
	    columnResizingMode: "widget",
	    columnAutoWidth: true,
        filterRow: { visible: true },
        headerFilter: { visible: true },
        "export": {
            enabled: true,
            fileName: "Orders"
        },
        paging: {
            enabled: false
        },
        scrolling: {
            mode: "virtual"
        },
        columnFixing: { 
            enabled: true
        },
        searchPanel: {
            visible: true,
            width: 240,
            placeholder: "搜索..."
        },
        columnChooser: {
            enabled: true,
            mode: "select"
        },
        selection: {
        	mode: "multiple",
            allowSelectAll: false,
            showCheckBoxesMode:'none',
        },
        columns: [ 
        	{
                dataField: "d",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: "科室"
            },
            {
                dataField: "e",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: "产线"
            },
            {
                dataField: "de",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: "设备"
            },
            {
                dataField: "h0",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[0]
            },
            {
                dataField: "h1",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[1]
            }
            ,
            {
                dataField: "h2",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[2]
            }
            ,
            {
                dataField: "h3",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[3]
            }
            ,
            {
                dataField: "h4",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption:times_num[4]
            }
            ,
            {
                dataField: "h5",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption:times_num[5]
            },
            {
                dataField: "h6",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[6]
            },
            {
                dataField: "h7",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[7]
            } ,
            {
                dataField: "h8",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[8]
            },
            {
                dataField: "h9",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[9]
            },
            {
                dataField: "h10",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[10]
            } ,
            {
                dataField: "h11",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[11]
            },
            {
                dataField: "h12",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[12]
            },
            {
                dataField: "h13",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[13]
            },
            {
                dataField: "h14",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[14]
            },
            {
                dataField: "h15",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[15]
            },
            {
                dataField: "h16",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[16]
            },
            {
                dataField: "h17",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[17]
            },
            {
                dataField: "h18",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[18]
            },
            {
                dataField: "h19",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[19]
            },
            {
                dataField: "h20",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[20]
            },
            {
                dataField: "h21",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[21]
            },
            {
                dataField: "h22",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[22]
            },
            {
                dataField: "h23",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[23]
            }
            
        ],
    }).dxDataGrid("instance");
	
	device_unit_grid =$("#device_unit_grid").dxDataGrid({
		dataSource: device_unit_points_list,
        showBorders: true,
        showColumnLines: true,
        allowColumnReordering: true,
        allowColumnResizing: true,
        showRowLines: true,
        rowAlternationEnabled: true,
	    columnResizingMode: "widget",
	    columnAutoWidth: true,
        filterRow: { visible: true },
        headerFilter: { visible: true },
        "export": {
            enabled: true,
            fileName: "Orders"
        },
        paging: {
            enabled: false
        },
        scrolling: {
            mode: "virtual"
        },
        columnFixing: { 
            enabled: true
        },
        searchPanel: {
            visible: true,
            width: 240,
            placeholder: "搜索..."
        },
        columnChooser: {
            enabled: true,
            mode: "select"
        },
        selection: {
        	mode: "multiple",
            allowSelectAll: false,
            showCheckBoxesMode:'none',
        },

        columns: [ 
        	{
                dataField: "d",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: "科室"
            },
            {
                dataField: "e",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: "产线"
            },
            {
                dataField: "de",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: "设备"
            },
            {
                dataField: "du",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: "单元"
            },
            {
                dataField: "h0",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[0]
            },
            {
                dataField: "h1",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[1]
            }
            ,
            {
                dataField: "h2",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[2]
            }
            ,
            {
                dataField: "h3",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[3]
            }
            ,
            {
                dataField: "h4",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption:times_num[4]
            }
            ,
            {
                dataField: "h5",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption:times_num[5]
            },
            {
                dataField: "h6",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[6]
            },
            {
                dataField: "h7",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[7]
            } ,
            {
                dataField: "h8",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[8]
            },
            {
                dataField: "h9",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[9]
            },
            {
                dataField: "h10",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[10]
            } ,
            {
                dataField: "h11",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[11]
            },
            {
                dataField: "h12",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[12]
            },
            {
                dataField: "h13",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[13]
            },
            {
                dataField: "h14",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[14]
            },
            {
                dataField: "h15",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[15]
            },
            {
                dataField: "h16",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[16]
            },
            {
                dataField: "h17",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[17]
            },
            {
                dataField: "h18",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[18]
            },
            {
                dataField: "h19",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[19]
            },
            {
                dataField: "h20",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[20]
            },
            {
                dataField: "h21",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[21]
            },
            {
                dataField: "h22",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[22]
            },
            {
                dataField: "h23",
                validationRules: [{ type: "required" }, {
                    type: "pattern",
                }],
                caption: times_num[23]
            }
            
        ],
    }).dxDataGrid("instance");	
}

function load_state_time() {
	var start_time = begin_datetime;
	var end_time   = end_datetime;
	
	str_eqp_list = "(";
	for (var i = 0; i < eqp_list.length; i++) {
		str_eqp_list += "'"+eqp_list[i] + "',";
	}
	str_eqp_list = str_eqp_list.substr(0, str_eqp_list.length - 1) + ")";
	
    $.ajax({
        url: path + '/gynh/get_state_time_for_hourly_page.do',
        type: 'post',
        async: true,
        data: {
        	eqp_list: str_eqp_list,
        	start_time: start_time, 
        	end_time: end_time, 
        },
        success: function (data) {
        	load_state_time_to_chart(data)
        },
        error: function () {
        	alert('error');
        }
    });
}
function load_state_time_to_chart(data) {
	var state_list = [
		"MAINT",
		"ETC",
		"RUN",
		"UP",
		"ETIME",
		"IDLE",
		"DOWN",
		"JOBCHG",
		];

	var state_color = [
		"red",
		"navajowhite",
		"springgreen",
		"lawngreen",
		"skyblue",
		"silver",
		"darkorange",
		"olivedrab",
		];

	var state_list_num = {
		MAINT:0,
		ETC:1,
		RUN:2,
		UP:3,
		ETIME:4,
		IDLE:5,
		DOWN:6,
		JOBCHG:7,
	};
    var state_time_list = data['state_time_list'];
    var max_count = 0;
    var count = 0;
    var temp_eqp_id = '';
	for (var i = 0; i < state_time_list.length; i++) {
		if (state_time_list[i].ln != temp_eqp_id) {
			temp_eqp_id = state_time_list[i].ln;
			max_count = Math.max(max_count, count);
			count = 0;
		}
		count++;
	}
	max_count = Math.max(max_count, count);
	
	var series_data = new Array();
	for (var i = 0; i < eqp_list.length; i++) {
		series_data.push(0);
	}
	
	var series = [];
	var series_templ = {
        type: 'bar',
        animation: false,
        name: '',
        stack: ' ',
        //label: _label,
        legendHoverLink: false,
        itemStyle: {
            normal: {
                color: 'white',
				label: {
					show: false,
				}
            },
        },
        data: series_data
    };

	for (var i = 0; i < max_count; i++) {
		for (var j = 0; j < state_list.length; j++) {
			var new_series = JSON.parse(JSON.stringify(series_templ));
			new_series.name = state_list[j] + '_' + i;
			new_series.itemStyle.normal.color = state_color[j];
			series.push(new_series);
		}
	}
	
	temp_eqp_id = '';
	var k = 0;
	var eqp_idx = 0;
	for (var i = 0; i < state_time_list.length; i++, k++) {
		if (state_time_list[i].ln != temp_eqp_id) {
			temp_eqp_id = state_time_list[i].ln;
			k = 0;
			for (var e = 0; e < eqp_list.length; e++) {
				if (eqp_list[e] == temp_eqp_id) {
					eqp_idx = e;
				}
			}
		}
		state_time_list[i].ln;
		state_time_list[i].st;
		state_time_list[i].t;
		
		series[state_list.length*k + state_list_num[state_time_list[i].st]].data[eqp_idx] = state_time_list[i].t;
	}
	
	option_eqp_id_state.series = series;
	option_eqp_id_state.yAxis[0].data = eqp_list;
	echarts_eqp_id_state.setOption(option_eqp_id_state, true);
	
}
