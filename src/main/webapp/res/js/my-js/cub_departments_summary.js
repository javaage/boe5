var dep_sys_daily_cons = {};
var dep_unit_cons = {};
var temp_dep_sys_cons;
var temp_dep_unit_cons;

var chs_cons={};

var chs_mchw_cons={};
var chs_lchw_cons={};

var currentRCUnit = 'KW2';//kWh,mWh
var currentPowerUnit = 'kWh';//kWh,mWh
var currentCDAUnit = 'L';//L,m³,k(m³)
var currentUPWUnit = 'L';//L,m³,k(m³)
var currentPN2Unit = 'L';//L,m³,k(m³)
var currentTimeUnit = 'M';



hvac_sys_daily_chart.on('click', function (params) {
	if(params.componentType != "xAxis"&&params.componentType != "yAxis"){
//        var selecte = {};
//        for(i =0; i < hvac_sys_array.length; i++){
//        	selecte[hvac_sys_array[i]] = false;
//        	hvac_sys_daily_chart_option.legend.selected = selecte;
//        }
//        var key_name = params.seriesName;
//    	selecte[key_name] = true;
		hvac_sys_daily_chart_option.legend.data.splice(0, hvac_sys_daily_chart_option.legend.data.length);
		hvac_sys_daily_chart_option.series.splice(0, hvac_sys_daily_chart_option.series.length);
		hvac_sys_daily_chart_option.color.splice(0, hvac_sys_daily_chart_option.color.length);
		if(params.seriesName=="CHS") {
			get_chs_cons();      
		}else if(params.seriesName=="MCHW"||params.seriesName=="LCHW") {
			return;
		}else  {
			hvac_sys_daily_chart_option.color.push(params.color);
			hvac_sys_daily_chart_option.legend.data[0]=params.seriesName;
			for(var i=0;i<hvac_sys_daily_chart_data.length;i++) {
				if(hvac_sys_daily_chart_data[i].name==params.seriesName) {
					var data=hvac_sys_daily_chart_data[i];
					break;
				}
			}
			hvac_sys_daily_chart_option.series.push(data);
		}
		
		
    	
    	hvac_sys_daily_chart.setOption(hvac_sys_daily_chart_option,true);
    }   
})
water_sys_daily_chart.on('click', function (params) {
	if(params.componentType != "xAxis"&&params.componentType != "yAxis"){
        var selecte = {};
        for(i =0; i < water_sys_array.length; i++){
        	selecte[water_sys_array[i]] = false;
        	water_sys_daily_chart_option.legend.selected = selecte;
        }
        var key_name = params.seriesName;
    	selecte[key_name] = true;
    	water_sys_daily_chart_option.legend.selected = selecte;
    	water_sys_daily_chart.setOption(water_sys_daily_chart_option);
    }   
})
gc_sys_daily_chart.on('click', function (params) {
	if(params.componentType != "xAxis"&&params.componentType != "yAxis"){
        var selecte = {};
        for(i =0; i < gc_sys_array.length; i++){
        	selecte[gc_sys_array[i]] = false;
        	gc_sys_daily_chart_option.legend.selected = selecte;
        }
        var key_name = params.seriesName;
    	selecte[key_name] = true;
    	gc_sys_daily_chart_option.legend.selected = selecte;
    	gc_sys_daily_chart.setOption(gc_sys_daily_chart_option);
    }   
})
function get_chs_cons(){
	
	chs_mchw_cons=new Array();
	chs_lchw_cons=new Array();
	
	for(var i=0;i<chs_cons.length;i++) {
		if(chs_cons[i].tf=='MCHW'){
			chs_mchw_cons.push(chs_cons[i].c);
			
		}
		if(chs_cons[i].tf=='LCHW'){
			chs_lchw_cons.push(chs_cons[i].c);
		}
	}
	
	hvac_sys_daily_chart_option.series.push(
		{
            name: 'MCHW',
            type: 'bar',
            stack: 'total',
            data: chs_mchw_cons,
            animation: false,
        }
	);
	hvac_sys_daily_chart_option.series.push(
		{
            name: 'LCHW',
            type: 'bar',
            stack: 'total',
            data: chs_lchw_cons,
            animation: false,
        }
	);
	
	hvac_sys_daily_chart_option.color.push('#fbbf27');
	hvac_sys_daily_chart_option.color.push('#fbbf27a3');
	hvac_sys_daily_chart_option.legend.data[0]='MCHW';
	hvac_sys_daily_chart_option.legend.data[1]='LCHW';
	
}     
     
function load_data_impl() {
	
	var begin_date = '';//$('#beginTimeBox').val();
	var end_date  = '';//$('#endTimeBox').val();
	if (begin_date == "") begin_date = init_begin_date;
	if (end_date == "") end_date = init_end_date;

	var begin_year = begin_date.substr(0,4)*1;
	var begin_month = begin_date.substr(5,2)*1;
	var begin_day = begin_date.substr(8,2)*1;

	var d1 = new Date(begin_date.replace(/-/g,   "/"))
	var d2 = new Date(end_date.replace(/-/g,   "/"))
	var days = (d2.getTime()-d1.getTime()) / (1000*60*60*24) + 1;

	var xAxis = new Array();

	for (var i = 0; i < days; i++) {
	    var temp = new Date(begin_year, begin_month-1, begin_day);
	    xAxis[i] = getFormatDate2(temp, i);
	    xAxis[i] = xAxis[i].substring(8);
	}
	
	hvac_sys_daily_chart_option.xAxis.data = xAxis;
	water_sys_daily_chart_option.xAxis.data = xAxis;
	gc_sys_daily_chart_option.xAxis.data = xAxis;
	
	hvac_unit_cons_chart_option.xAxis.data = xAxis;
	water_unit_cons_chart_option.xAxis.data = xAxis;
	gc_unit_cons_chart_option.xAxis.data = xAxis;
	
	// dep_sys_daily_cons
	dep_sys_daily_cons = {};
	for (var i = 0; i < temp_dep_sys_cons.length; i++) {
		if (typeof(dep_sys_daily_cons[temp_dep_sys_cons[i].dp]) == "undefined") {
			dep_sys_daily_cons[temp_dep_sys_cons[i].dp] = {};
		}
		if (typeof(dep_sys_daily_cons[temp_dep_sys_cons[i].dp][temp_dep_sys_cons[i].sys]) == "undefined") {
			dep_sys_daily_cons[temp_dep_sys_cons[i].dp][temp_dep_sys_cons[i].sys] = new Array(days).fill('-');
		}
		
		var dateArray = temp_dep_sys_cons[i].day .split("-");
		var idx = (new Date(dateArray[0], dateArray[1]-1, dateArray[2]) - new Date(begin_year, begin_month-1, begin_day))/(24*60*60*1000);
		dep_sys_daily_cons[temp_dep_sys_cons[i].dp][temp_dep_sys_cons[i].sys][idx] = temp_dep_sys_cons[i].c;
	}
	
	// dep_unit_cons
	dep_unit_cons = {};
	for (var i = 0; i < temp_dep_unit_cons.length; i++) {
		if (typeof(dep_unit_cons[temp_dep_unit_cons[i].dp]) == "undefined") {
			dep_unit_cons[temp_dep_unit_cons[i].dp] = {};
		}
		if (typeof(dep_unit_cons[temp_dep_unit_cons[i].dp]['in']) == "undefined") {
			dep_unit_cons[temp_dep_unit_cons[i].dp]['in'] = new Array(days).fill('-');
		}
		if (typeof(dep_unit_cons[temp_dep_unit_cons[i].dp]['out']) == "undefined") {
			dep_unit_cons[temp_dep_unit_cons[i].dp]['out'] = new Array(days).fill('-');
		}
		if (typeof(dep_unit_cons[temp_dep_unit_cons[i].dp]['uc']) == "undefined") {
			dep_unit_cons[temp_dep_unit_cons[i].dp]['uc'] = new Array(days).fill('-');
		}
		
		var dateArray = temp_dep_unit_cons[i].day .split("-");
		var idx = (new Date(dateArray[0], dateArray[1]-1, dateArray[2]) - new Date(begin_year, begin_month-1, begin_day))/(24*60*60*1000);
		dep_unit_cons[temp_dep_unit_cons[i].dp]['in'][idx] = temp_dep_unit_cons[i].in;
		dep_unit_cons[temp_dep_unit_cons[i].dp]['out'][idx] = temp_dep_unit_cons[i].out;
		dep_unit_cons[temp_dep_unit_cons[i].dp]['uc'][idx] = temp_dep_unit_cons[i].uc;
	}
	
	hvac_sys_daily_chart_option.series.splice(0, hvac_sys_daily_chart_option.series.length);
	water_sys_daily_chart_option.series.splice(0, water_sys_daily_chart_option.series.length);
	gc_sys_daily_chart_option.series.splice(0, gc_sys_daily_chart_option.series.length);

	hvac_unit_cons_chart_option.series.splice(0, hvac_unit_cons_chart_option.series.length);
	water_unit_cons_chart_option.series.splice(0, water_unit_cons_chart_option.series.length);
	gc_unit_cons_chart_option.series.splice(0, gc_unit_cons_chart_option.series.length);


	for (var i = 0; i < hvac_sys_array.length; i++) {
		var sys_name = hvac_sys_array[i];
		if (typeof(dep_sys_daily_cons['HVAC']) != "undefined" && typeof(dep_sys_daily_cons['HVAC'][sys_name]) != "undefined") {
			hvac_sys_daily_chart_option.series.push(
				{
		            name: sys_name,
		            type: 'bar',
		            stack: 'total',
		            data: dep_sys_daily_cons['HVAC'][sys_name],
		            animation: false,
		        }
			);
		}
	}
	hvac_sys_daily_chart_data=JSON.parse(JSON.stringify(hvac_sys_daily_chart_option.series));
	

	for (var i = 0; i < water_sys_array.length; i++) {
		var sys_name = water_sys_array[i];
		if (typeof(dep_sys_daily_cons['WATER']) != "undefined" && typeof(dep_sys_daily_cons['WATER'][sys_name]) != "undefined") {
			water_sys_daily_chart_option.series.push(
				{
		            name: sys_name,
		            type: 'bar',
		            stack: 'total',
		            data: dep_sys_daily_cons['WATER'][sys_name],
		            animation: false,
		        }
			);
		}
	}
	
	for (var i = 0; i < gc_sys_array.length; i++) {
		var sys_name = gc_sys_array[i];
		if (typeof(dep_sys_daily_cons['G&C']) != "undefined" && typeof(dep_sys_daily_cons['G&C'][sys_name]) != "undefined") {
			gc_sys_daily_chart_option.series.push(
				{
		            name: sys_name,
		            type: 'bar',
		            stack: 'total',
		            data: dep_sys_daily_cons['G&C'][sys_name],
		            animation: false,
		        }
			);
		}
	}
	
	if (typeof(dep_unit_cons['HVAC']) != "undefined" && typeof(dep_unit_cons['HVAC']['in']) != "undefined") {
		hvac_unit_cons_chart_option.series.push(
			{
	            name: hvac_unit_array[0],
	            type: 'bar',
	            data: dep_unit_cons['HVAC']['in'],
	            animation: false,
	            yAxisIndex:0,
	        }
		);
	}
	if (typeof(dep_unit_cons['HVAC']) != "undefined" && typeof(dep_unit_cons['HVAC']['out']) != "undefined") {
		hvac_unit_cons_chart_option.series.push(
			{
	            name: hvac_unit_array[1],
	            type: 'bar',
	            data: dep_unit_cons['HVAC']['out'],
	            animation: false,
	            yAxisIndex:1,
	        }
		);
	}
	if (typeof(dep_unit_cons['HVAC']) != "undefined" && typeof(dep_unit_cons['HVAC']['uc']) != "undefined") {
		hvac_unit_cons_chart_option.series.push(
			{
	            name: hvac_unit_array[2],
	            type: 'line',
	            data: dep_unit_cons['HVAC']['uc'],
	            animation: false,
	            yAxisIndex:2,
	        }
		);
	}
	if (typeof(dep_unit_cons['WATER']) != "undefined" && typeof(dep_unit_cons['WATER']['in']) != "undefined") {
		water_unit_cons_chart_option.series.push(
			{
	            name: water_unit_array[0],
	            type: 'bar',
	            data: dep_unit_cons['WATER']['in'],
	            animation: false,
	            yAxisIndex:0,
	        }
		);
	}
	if (typeof(dep_unit_cons['WATER']) != "undefined" && typeof(dep_unit_cons['WATER']['out']) != "undefined") {
		water_unit_cons_chart_option.series.push(
			{
	            name: water_unit_array[1],
	            type: 'bar',
	            data: dep_unit_cons['WATER']['out'],
	            animation: false,
	            yAxisIndex:1,
	        }
		);
	}
	if (typeof(dep_unit_cons['WATER']) != "undefined" && typeof(dep_unit_cons['WATER']['uc']) != "undefined") {
		water_unit_cons_chart_option.series.push(
			{
	            name: water_unit_array[2],
	            type: 'line',
	            data: dep_unit_cons['WATER']['uc'],
	            animation: false,
	            yAxisIndex:2,
	        }
		);
	}

	if (typeof(dep_unit_cons['G&C']) != "undefined" && typeof(dep_unit_cons['G&C']['in']) != "undefined") {
		gc_unit_cons_chart_option.series.push(
			{
	            name: gc_unit_array[0],
	            type: 'bar',
	            data: dep_unit_cons['G&C']['in'],
	            animation: false,
	            yAxisIndex:0,
	        }
		);
	}
	if (typeof(dep_unit_cons['G&C']) != "undefined" && typeof(dep_unit_cons['G&C']['out']) != "undefined") {
		gc_unit_cons_chart_option.series.push(
			{
	            name: gc_unit_array[1],
	            type: 'bar',
	            data: dep_unit_cons['G&C']['out'],
	            animation: false,
	            yAxisIndex:1,
	        }
		);
	}
	if (typeof(dep_unit_cons['G&C']) != "undefined" && typeof(dep_unit_cons['G&C']['uc']) != "undefined") {
		gc_unit_cons_chart_option.series.push(
			{
	            name: gc_unit_array[2],
	            type: 'line',
	            data: dep_unit_cons['G&C']['uc'],
	            animation: false,
	            yAxisIndex:2,
	        }
		);
	}
	hvac_sys_daily_chart.setOption(hvac_sys_daily_chart_option, true);
	water_sys_daily_chart.setOption(water_sys_daily_chart_option, true);
	gc_sys_daily_chart.setOption(gc_sys_daily_chart_option, true);
	
	hvac_unit_cons_chart.setOption(hvac_unit_cons_chart_option, true);
	water_unit_cons_chart.setOption(water_unit_cons_chart_option, true);
	gc_unit_cons_chart.setOption(gc_unit_cons_chart_option, true);
}

function page_custom_resize() {

	let h = $(window).height();
	let w = $(window).width();
	
	$("#fac_sum_charts_div").width(w);
	$("#fac_sum_charts_div").height(h);

	document.getElementById('hvac_sys_daily_chart').style.width = 20 + 'px';
	document.getElementById('hvac_sys_daily_chart').style.height = 20 + 'px';
	document.getElementById('water_sys_daily_chart').style.width = 20 + 'px';
	document.getElementById('water_sys_daily_chart').style.height = 20 + 'px';
	document.getElementById('gc_sys_daily_chart').style.width = 20 + 'px';
	document.getElementById('gc_sys_daily_chart').style.height = 20 + 'px';

	document.getElementById('hvac_unit_cons_chart').style.width = 20 + 'px';
	document.getElementById('hvac_unit_cons_chart').style.height = 20 + 'px';
	document.getElementById('water_unit_cons_chart').style.width = 20 + 'px';
	document.getElementById('water_unit_cons_chart').style.height = 20 + 'px';
	document.getElementById('gc_unit_cons_chart').style.width = 20 + 'px';
	document.getElementById('gc_unit_cons_chart').style.height = 20 + 'px';

	var right = 74;
	$("#gc_sys_daily_td").width((w-right)/3);
	$("#water_sys_daily_td").width((w-right)/3);
	$("#hvac_sys_daily_td").width((w-right)/3);

	var bottom = 88;
	$("#tr_1").height(25);
	$("#tr_2").height(25);
	$("#tr_4").height(bottom);
	$("#hvac_sys_daily_chart_td").height((h-bottom-25-25)/2);
	$("#hvac_unit_cons_chart_td").height((h-bottom-25-25)/2);

	document.getElementById('hvac_sys_daily_chart').style.width = document.getElementById('hvac_sys_daily_chart').parentNode.offsetWidth-4 + 'px';
	document.getElementById('hvac_sys_daily_chart').style.height = document.getElementById('hvac_sys_daily_chart').parentNode.offsetHeight + 'px';
	hvac_sys_daily_chart.resize();
	document.getElementById('water_sys_daily_chart').style.width = document.getElementById('water_sys_daily_chart').parentNode.offsetWidth-4 + 'px';
	document.getElementById('water_sys_daily_chart').style.height = document.getElementById('water_sys_daily_chart').parentNode.offsetHeight + 'px';
	water_sys_daily_chart.resize();
	document.getElementById('gc_sys_daily_chart').style.width = document.getElementById('gc_sys_daily_chart').parentNode.offsetWidth-4 + 'px';
	document.getElementById('gc_sys_daily_chart').style.height = document.getElementById('gc_sys_daily_chart').parentNode.offsetHeight + 'px';
	gc_sys_daily_chart.resize();

	document.getElementById('hvac_unit_cons_chart').style.width = document.getElementById('hvac_unit_cons_chart').parentNode.offsetWidth-4 + 'px';
	document.getElementById('hvac_unit_cons_chart').style.height = document.getElementById('hvac_unit_cons_chart').parentNode.offsetHeight + 'px';
	hvac_unit_cons_chart.resize();
	document.getElementById('water_unit_cons_chart').style.width = document.getElementById('water_unit_cons_chart').parentNode.offsetWidth-4 + 'px';
	document.getElementById('water_unit_cons_chart').style.height = document.getElementById('water_unit_cons_chart').parentNode.offsetHeight + 'px';
	water_unit_cons_chart.resize();
	document.getElementById('gc_unit_cons_chart').style.width = document.getElementById('gc_unit_cons_chart').parentNode.offsetWidth-4 + 'px';
	document.getElementById('gc_unit_cons_chart').style.height = document.getElementById('gc_unit_cons_chart').parentNode.offsetHeight + 'px';
	gc_unit_cons_chart.resize();
	
}

window.onload = function() {
	load_default_shortcut();
	set_units_on_controles();
	load_data(init_begin_date, init_end_date);
	//loadSnapshot();
}
window.onresize = function() {
	page_custom_resize();
}
window.onunload = function(event) {
	SaveSnapshot(pageName, '', 'icon_path_1', true);
}

function SaveSnapshot(page, name, icon, when_close) {
	var cfg = {
			currentPowerUnit:currentPowerUnit,
			currentCDAUnit:currentCDAUnit,
			currentUPWUnit:currentUPWUnit,
			currentPN2Unit:currentPN2Unit,
			currentTimeUnit:currentTimeUnit,
			currentRCUnit:currentRCUnit,
		};
	var strJson = JSON.stringify(cfg, function(key, val) {
		if (typeof val === 'function') {
			return val + '';
		}
		return val;
	});
	
    $.ajax({
        url: when_close ? (path + "/gynh/biz/save_default_shortcut.do") : (path + "/gynh/biz/save_shortcut.do"),
        type: 'post',
        async: false,
        data: {
        	page:page, 
        	name:name, 
        	icon:icon, 
        	state_json:strJson,
        },
        success: function (data) {
        	if (!when_close) {
        		top.layer.msg('保存成功', {icon: 1, skin:is_dark?'layui-layer-lan':''});
	        	loadSnapshot();
        	}
        },
        error: function () {
        	if (!when_close) {
        		top.layer.msg('保存失败', {icon: 1, skin:is_dark?'layui-layer-lan':''});
        	}
        }
    });
}

function load_default_shortcut() {
	
	$.ajax({
		url: path + "/gynh/biz/load_default_shortcut.do",
		type: 'post',
		async: false,
		data: {
			page_name:pageName, 
		},
		success: function (data) {
			var shortcuts = data['shortcut'];
			if (shortcuts.length == 1) {
				
				apply_shortcut(shortcuts[0].state_json);
				
			}
        },
        error: function () {
        }
	});
	
}

function apply_shortcut(json) {
	var cfg = JSON.parse(json, function(k,v) {
		if(v.indexOf&&v.indexOf('function')>-1) {
			return eval("(function(){return "+v+" })()")
		}
		return v;
	});
	
	currentPowerUnit = cfg.currentPowerUnit;
	currentCDAUnit = cfg.currentCDAUnit;
	currentUPWUnit = cfg.currentUPWUnit;
	currentPN2Unit = cfg.currentPN2Unit;
	currentTimeUnit = cfg.currentTimeUnit;
	currentRCUnit = cfg.currentRCUnit;
    
	if(currentRCUnit==null||currentRCUnit=='')
    {
    	currentRCUnit="KW2";
    }
    if(currentPowerUnit==null||currentPowerUnit=='')
    {
    	currentPowerUnit="mWh";
    }
    if(currentCDAUnit==null||currentCDAUnit=='')
    {
    	currentCDAUnit="k(m³)";
    }
    if(currentUPWUnit==null||currentUPWUnit=='')
    {
    	currentUPWUnit="k(m³)";
    }
    if(currentPN2Unit==null||currentPN2Unit=='')
    {
    	currentPN2Unit="k(m³)";
    }
    if(currentTimeUnit==null||currentTimeUnit=='')
    {
    	currentTimeUnit="M";
    }
}


function onClickLoad() {
	var begin = init_begin_date;
	var end = init_end_date;
	load_data(begin, end);
}

function translate_unit(params,from,to)
{
	translate(params,from,to);
    load_data_impl();

}
function translate(params,from,to)
{
	
	for (var i = 0; i < temp_dep_sys_cons.length; i++) {
		if (from == 'kWh' && to == 'mWh') {
			temp_dep_sys_cons[i].c = temp_dep_sys_cons[i].c/1000.0 ;
		}
		if (from == 'mWh' && to == 'kWh') {
			temp_dep_sys_cons[i].c = temp_dep_sys_cons[i].c*1000.0 ;
		}
	}
	for (var i = 0; i < temp_dep_unit_cons.length; i++) {
		var param=temp_dep_unit_cons[i].product;
		if(param==params){
			if (from == 'KW1' && to == 'KW2') {
				temp_dep_unit_cons[i].out = temp_dep_unit_cons[i].out/1000000.0 ;
			}
			if (from == 'KW2' && to == 'KW1') {
				temp_dep_unit_cons[i].out = temp_dep_unit_cons[i].out*1000000.0 ;
			}
			if (from == 'L' && to == 'm³') {
				temp_dep_unit_cons[i].out = temp_dep_unit_cons[i].out/1000.0 ;
			}
			if (from == 'L' && to == 'k(m³)') {
				temp_dep_unit_cons[i].out = temp_dep_unit_cons[i].out/1000000.0 ;
			}
			if (from == 'm³' && to == 'L') {
				temp_dep_unit_cons[i].out = temp_dep_unit_cons[i].out*1000.0 ;
			}
			if (from == 'm³' && to == 'k(m³)') {
				temp_dep_unit_cons[i].out = temp_dep_unit_cons[i].out/1000.0 ;
			}
			if (from == 'k(m³)' && to == 'L') {
				temp_dep_unit_cons[i].out = temp_dep_unit_cons[i].out*1000000.0 ;
			}
			if (from == 'k(m³)' && to == 'm³') {
				temp_dep_unit_cons[i].out = temp_dep_unit_cons[i].out*1000.0 ;
			}
		}
	}
	for (var i = 0; i < temp_dep_unit_cons.length; i++) {
		if (from == 'kWh' && to == 'mWh') {
			temp_dep_unit_cons[i].in = temp_dep_unit_cons[i].in/1000.0 ;
		}
		if (from == 'mWh' && to == 'kWh') {
			temp_dep_unit_cons[i].in = temp_dep_unit_cons[i].in*1000.0 ;
		}
	}
}


function set_units_on_controles()
{
	var units=unit_show.split('_');
	for(var i=0;i<units.length;i++){
		if(units[i]=='power'){
			document.getElementById("select_power").style.display="inline";
			document.getElementById("font_power").style.display="inline";
		}
		if(units[i]=='rc'){
			document.getElementById("select_rc").style.display="inline";
			document.getElementById("font_rc").style.display="inline";
		}
		if(units[i]=='upw'){
			document.getElementById("select_upw").style.display="inline";
			document.getElementById("font_upw").style.display="inline";
		}
		if(units[i]=='cda'){
			document.getElementById("select_cda").style.display="inline";
			document.getElementById("font_cda").style.display="inline";
		}
		if(units[i]=='pn2'){
			document.getElementById("select_pn2").style.display="inline";
			document.getElementById("font_pn2").style.display="inline";
		}
		if(units[i]=='time'){
			document.getElementById("select_hms").style.display="inline";
			document.getElementById("font_hms").style.display="inline";
		}
	}
	
	var select_hms = document.getElementById("select_hms");
	var select_power = document.getElementById("select_power");
	var select_upw = document.getElementById("select_upw");
	var select_cda = document.getElementById("select_cda");
	var select_pn2 = document.getElementById("select_pn2");
	var select_rc = document.getElementById("select_rc");
	
	for(var i=0; i<select_rc.options.length; i++){ 
		if(select_rc.options[i].value == currentRCUnit){ 
			select_rc.options[i].selected = true; 
		}
	}
	for(var i=0; i<select_power.options.length; i++){ 
		if(select_power.options[i].value == currentPowerUnit){ 
			select_power.options[i].selected = true; 
		}
	}
	for(var i=0; i<select_cda.options.length; i++){ 
		if(select_cda.options[i].value == currentCDAUnit){ 
			select_cda.options[i].selected = true; 
		}
	}
	for(var i=0; i<select_upw.options.length; i++){ 
		if(select_upw.options[i].value == currentUPWUnit){ 
			select_upw.options[i].selected = true; 
		}
	}
	for(var i=0; i<select_pn2.options.length; i++){ 
		if(select_pn2.options[i].value == currentPN2Unit){ 
			select_pn2.options[i].selected = true; 
		}
	}
	for(var i=0; i<select_hms.options.length; i++){ 
		if(select_hms.options[i].value == currentTimeUnit){ 
			select_hms.options[i].selected = true; 
		}
	}
}

function load_data(begin, end) {
	if  (begin > end) {
		var temp=begin;
		begin = end;
		end = temp;
	}
	
	var beginYear = begin.substr(0,4)*1;
	var beginMonth = begin.substr(5,2)*1;
	var beginDay =  begin.substr(8,2)*1;
	var endYear = end.substr(0,4)*1;
	var endMonth = end.substr(5,2)*1;
	var endDay = end.substr(8,2)*1;
	
    var index = top.layer.msg('查询中...',{icon: 16, skin:is_dark?'layui-layer-lan':'', shade:0.1});
    if(!ajax_json){
    	cub_department_summary_ajax_url1= path + requestpath;
    	
    }
    $.ajax({
//        url: path + requestpath,        
        url: cub_department_summary_ajax_url1,
        type: 'post',
        async: false,
        data: {
        	beginYear:beginYear, 
        	beginMonth:beginMonth, 
        	beginDay:beginDay, 
        	endYear:endYear, 
        	endMonth:endMonth, 
        	endDay:endDay, 
        	param1:'',
        },
        success: function (data) {
        	var jsonStr = JSON.stringify(data,null,4);
        	console.log(jsonStr);
        	temp_dep_sys_cons = data['department_power_sum'];
        	temp_dep_unit_cons = data['daily_product'];
        	chs_cons = data['chs_cons']
        	translate('RC','KW1', currentPowerUnit);
        	translate('Power','kWh', currentPowerUnit);
        	translate('CDA','m³', currentCDAUnit);
        	translate('UPW','m³', currentUPWUnit);
        	translate('PN2','m³', currentPN2Unit);        	
            load_data_impl();
        	top.layer.close(index);
        	top.layer.msg('数据加载完成', {icon: 1, skin:is_dark?'layui-layer-lan':''});
        },
        error: function () {
        	top.layer.close(index);
        	top.layer.msg('数据加载错误', {icon: 1, skin:is_dark?'layui-layer-lan':''});
        }
    });
    
    page_custom_resize();
}
