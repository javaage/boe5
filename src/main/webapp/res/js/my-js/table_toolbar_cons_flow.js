// 表的tool bar
var table_toolbar_chart = echarts.init(document.getElementById('table_toolbar_div'), "<%=theme%>");

var delShortcutMode = false;


var table_toolbar_option = {
	toolbox: {
		show: true,
		x:'left',
		showTitle:true,
		borderWidth: 0,
		feature: {
			my_Power: {
				show: true,
				title: 'Power',
				icon: 'image://'+path+'/res/images/energy/power.png',
				onclick: function() {
					var str='Power';
					onClickEnergyBtn(str);
				}
			},
			my_UPW: {
				show: true,
				title: 'UPW',
				icon: 'image://'+path+'/res/images/energy/water.png',
				onclick: function() {
					var str='UPW';
					onClickEnergyBtn(str);
				}
			},
			my_PN2: {
				show: true,
				title: 'PN2',
				icon: 'image://'+path+'/res/images/energy/pn2.png',
				onclick: function() {
					var str='PN2';
					onClickEnergyBtn(str);
				}
			},
			my_CDA: {
				show: true,
				title: 'CDA',
				icon: 'image://'+path+'/res/images/energy/cda.png',
				onclick: function() {
					var str='CDA';
					onClickEnergyBtn(str);
				}
			},
		}
	},
	series: [
		{
            name: ' ',
            type: 'pie',
		}
	]
};



table_toolbar_chart.setOption(table_toolbar_option);

//document.getElementById('table_toolbar_div').style.background='white';

