var window_height;
var window_width;
var power_quotas_daily=new Array();
var cda_quotas_daily=new Array();
var upw_quotas_daily=new Array();
var pn2_quotas_daily=new Array();
var cw_quotas_daily=new Array();
var gas_quotas_daily=new Array();
var quota_sava_data=new Array();
var power_quotas_daily_sava=new Array();
var cda_quotas_daily_sava=new Array();
var upw_quotas_daily_sava=new Array();
var pn2_quotas_daily_sava=new Array();
var cw_quotas_daily_sava=new Array();
var gas_quotas_daily_sava=new Array();
var change=false;

var longtabs = [
    { idx:1, z:11, id: "quota_div", text: "全厂(定额)", button:'quota_grid_am'},
    { idx:2, z:12, id: "power_div", text: "power(实际)", button:'power_quotas_daily_am'},
    { idx:3, z:13, id: "cda_div", text: "cda(实际)", button:'cda_quotas_daily_am'},
    { idx:4, z:14, id: "upw_div", text: "upw(实际)", button:'upw_quotas_daily_am'},
    { idx:5, z:15, id: "pn2_div", text: "pn2(实际)", button:'pn2_quotas_daily_am'},
    { idx:4, z:14, id: "cw_div", text: "cw(实际)", button:'cw_quotas_daily_am'},
    { idx:5, z:15, id: "gas_div", text: "gas(实际)", button:'gas_quotas_daily_am'},
];

var eqp_list = [];
var device_list = [];
var device_unit_list = [];
var cur_year = 0;

$(function() {
	window.onresize = function() {
		on_resize();
   	}
	on_resize();
    var tabsInstance = $("#longtabs > .tabs-container").dxTabs({
        dataSource: longtabs,
        selectedIndex: 0,
        onItemClick: function(e) {
        	var current_z_index = $("#"+e.itemData.id).css("z-index");
        	if (current_z_index != 50) {
        		for (var i = 0; i < longtabs.length; i++) {
        			$("#"+longtabs[i].id).css("z-index", longtabs[i].z);
        			$("#"+longtabs[i].id).css("display", 'none');
        			$("#"+longtabs[i].button).css("z-index", 1000);
        		}
        		$("#"+e.itemData.button).css("z-index", 1100);
        		$("#"+e.itemData.id).css("z-index", 50);
        		$("#"+e.itemData.id).css("display", 'inline');
        	}
        }
    }).dxTabs("instance");
    $("#"+longtabs[0].id).css("z-index", 50);
    $("#"+longtabs[0].id).css("display", 'inline');
    
    function on_resize() {
    	window_height = $(window).height();
    	window_width = $(window).width();
    	for (var i = 0; i < longtabs.length; i++) {
    		$("#"+longtabs[i].id).css("width", window_width-16+'px');
    		$("#"+longtabs[i].id).css("height", window_height-24+"px");
    	}
       	$("#longtabs").css("width", window_width-800+'px');
       	document.getElementById('power_grid').style.height = window_height-624+"px";
       	document.getElementById('upw_grid').style.height =  window_height-624+"px";
       	document.getElementById('cda_grid').style.height =  window_height-624+"px";
       	document.getElementById('pn2_grid').style.height =  window_height-624+"px";
       	document.getElementById('cw_grid').style.height =  window_height-624+"px";
       	document.getElementById('gas_grid').style.height =  window_height-624+"px";
       	
       	document.getElementById('power_grid').style.width = window_width+"px";
       	document.getElementById('upw_grid').style.width =  window_width+"px";
       	document.getElementById('cda_grid').style.width =  window_width+"px";
       	document.getElementById('pn2_grid').style.width =  window_width+"px";
       	document.getElementById('cw_grid').style.width =  window_width+"px";
       	document.getElementById('gas_grid').style.width =  window_width+"px";
       	ajaxForQuota(cur_year);
       	
    }
    function logEvent(eventName) {
        var logList = $("#events ul"),
            newItem = $("<li>", { text: eventName });
        logList.prepend(newItem);
    }
    
    cur_year = new Date().getFullYear();
    $("#year").html(cur_year);
    ajaxForQuota(cur_year);
    
    $("#preYear").click(function(){
    	cur_year = cur_year - 1;
    	$("#year").html(cur_year);
    	ajaxForQuota(cur_year);
    })
	$("#nextYear").click(function(){
		cur_year = cur_year + 1;
		$("#year").html(cur_year);
		ajaxForQuota(cur_year);
    })
    function ajaxForQuota(year){
    	change=false;
    	var quotas;
    	var quotas_daily;
    	power_quotas_daily=new Array();
    	cda_quotas_daily=new Array();
    	upw_quotas_daily=new Array();
    	pn2_quotas_daily=new Array();
    	cw_quotas_daily=new Array();
    	gas_quotas_daily=new Array();
    	$.ajax({
            url: path + '/quota/get_all_total.do',
            type: 'get',
            data:{year:year},
            async: false,
            success: function (data) {
                quotas = data;
            }
        });
    	$.ajax({
            url: path + '/quota/get_all_daily.do',
            type: 'get',
            data:{year:year},
            async: false,
            success: function (data) {
                quotas_daily = data;
                for(var i=0;i<data.length;i++){
                	var d = data[i].day;
      	        	if (d==31) {
      	        		data[i].month2="";
      	        		data[i].month4="";
      	        		data[i].month6="";
      	        		data[i].month9="";
      	        		data[i].month11="";
      	        	}
      	        	if(d==30){
      	        		data[i].month2="";
      	        	}
      	        	if ( d == '29'&& !test_date(cur_year, 2, 29)) {
      	        		data[i].month2="";
      	        	}
                	if (data[i].energy=="Power") {
                		power_quotas_daily.push(data[i]);
                	}
                	if (data[i].energy=="CDA") {
                		cda_quotas_daily.push(data[i]);
                	}
                	if (data[i].energy=="UPW") {
                		upw_quotas_daily.push(data[i]);
                	}
                	if (data[i].energy=="PN2") {
                		 pn2_quotas_daily.push(data[i]);
                	}
                	if (data[i].energy=="CW") {
                		cw_quotas_daily.push(data[i]);
                	}
                	if (data[i].energy=="GAS") {
                		 gas_quotas_daily.push(data[i]);
                	}
                }  
            }
        });
    	
    	/*var container1 = document.getElementById('quota_grid');
    	var quota_handsontable = Handsontable(container1,{*/
    	$("#quota_grid").handsontable({
    		data: quotas,
    		autoColumnSize:true,
    		colWidths: window_width/13, // 设置所有列宽
    		rowHeights: 35,
    		//width: '100%',
            height: '100%',//表内滚动条
            fixedColumnsLeft: 1,//左侧固定列数
            colHeaders: ["能源","1月","2月","3月","4月","5月","6月","7月","8月","9月","10月","11月","12月"],
            columnSorting: true,
            columns:[
            	{
                    data:'energy',
                    readOnly:true
                },{
                    data:"month1",
                },{
                    data:"month2",
                },{
                    data:"month3"
                },{
                    data:"month4"
                },{
                    data:"month5"
                },{
                    data:"month6"
                },{
                    data:"month7"
                },{
                    data:"month8"
                },{
                    data:"month9"
                },{
                    data:"month10"
                },{
                    data:"month11"
                },{
                    data:"month12"
                },
            ],
            cells: function (row, col) {
                var cellProperties = {};
                var data = this.instance.getData();
                if (col === 0) {
                  cellProperties.renderer = firstRowRenderer; // uses function directly
                }
                return cellProperties;
            },
      		afterChange:function(changes, source){
    		if(change){
				for (var i = 0; i < changes.length; i++) {
					var factory=quotas[parseInt(changes[i][0])].factory;
					var energy=quotas[parseInt(changes[i][0])].energy;
					var val=changes[i][3];
					var ym;
					if(changes[i][1]=="month1"){
						ym=year+"-01-01";
					}
					if(changes[i][1]=="month2"){
		    		   ym=year+"-02-01";
					}
					if(changes[i][1]=="month3"){
		    		   ym=year+"-03-01";
		    	   	}
		    	   	if(changes[i][1]=="month4"){
		    		   ym=year+"-04-01";
		    	   	}
		    	   	if(changes[i][1]=="month5"){
		    		   ym=year+"-05-01";
		    	   	}
		    	   	if(changes[i][1]=="month6"){
		    		   ym=year+"-06-01";
		    	   	}
		    	   	if(changes[i][1]=="month7"){
		    		   ym=year+"-07-01";
		    	   	}
		    	   	if(changes[i][1]=="month8"){
		    		   ym=year+"-08-01";
		    	   	}
		    	   	if(changes[i][1]=="month9"){
		    		   ym=year+"-09-01";
		    	   	}
		    	   	if(changes[i][1]=="month10"){
		    		   ym=year+"-10-01";
		    	   	}
		    	   	if(changes[i][1]=="month11"){
		    		   ym=year+"-11-01";
		    	   	}
		    	   	if(changes[i][1]=="month12"){
	    	   			ym=year+"-12-01";
		    	   	}
		    	   	var quota_data = {factory:factory,energy:energy,ymd:ym,val:val};//
		    	   	quota_sava_data.push(quota_data);
					}
        		}
      		}
    	});
		function firstRowRenderer(instance, td, row, col, prop, value, cellProperties) {
			Handsontable.renderers.TextRenderer.apply(this, arguments);
			td.style.color = '#000';
			td.style.background = '#EEE';
		}
    	/*function negativeValueRenderer(instance, td, row, col, prop, value, cellProperties) {
    		  Handsontable.renderers.TextRenderer.apply(this, arguments);*/
        /*var button1 = document.getElementById('export-file1');
        var exportPlugin1 = quota_handsontable.getPlugin('exportFile');
        button1.addEventListener('click', function() {
          exportPlugin1.downloadFile('csv', {
            bom: false,
            columnDelimiter: ',',
            columnHeaders: false,
            exportHiddenColumns: true,
            exportHiddenRows: true,
            fileExtension: 'csv',
            filename: 'Handsontable-CSV-file_[YYYY]-[MM]-[DD]',
            mimeType: 'text/csv',
            rowDelimiter: '\r\n',
            rowHeaders: true
          });
        });*/
        
        
        /*var container_power = document.getElementById('power_grid');
    	var power_handsontable=new Handsontable(container_power,{*/
        $("#power_grid").handsontable({
            data: power_quotas_daily,
            colWidths: window_width/14, // 设置所有列宽
            rowHeights: 35,
            //width: '100%',
            height: '100%',//表内滚动条
            fixedColumnsLeft: 2,
            colHeaders: ["能源","日期","1月","2月","3月","4月","5月","6月","7月","8月","9月","10月","11月","12月"],
            columnSorting: true,
            columns:[
            	{
                    data:"energy",
                    readOnly:true
                },{
                    data:"day",
                    readOnly:true
                },{
                    data:"month1",
                },{
                    data:"month2",
                },{
                    data:"month3"
                },{
                    data:"month4"
                },{
                    data:"month5"
                },{
                    data:"month6"
                },{
                    data:"month7"
                },{
                    data:"month8"
                },{
                    data:"month9"
                },{
                    data:"month10"
                },{
                    data:"month11"
                },{
                    data:"month12"
                },
            ],
            afterChange:function(changes, source){
        	   if(change){
	               for (var i = 0; i < changes.length; i++) {
	        	   //var factory=quotas[parseInt(changes[i][0])].factory;
	        	   var energy=power_quotas_daily[parseInt(changes[i][0])].energy;
	        	   var val=changes[i][3];
	        	   var ym;
	        	   if(changes[i][1]=="month1"){
	        		   ym= getFormatDate(year+"-01-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month2"){
	        		   ym= getFormatDate(year+"-02-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month3"){
	        		   ym= getFormatDate(year+"-03-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month4"){
	        		  ym= getFormatDate(year+"-04-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month5"){
	        		   ym= getFormatDate(year+"-05-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month6"){
	        		   ym= getFormatDate(year+"-06-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month7"){
	        		   ym= getFormatDate(year+"-07-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month8"){
	        		   ym= getFormatDate(year+"-08-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month9"){
	        		   ym= getFormatDate(year+"-09-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month10"){
	        		   ym= getFormatDate(year+"-10-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month11"){
	        		   ym= getFormatDate(year+"-11-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month12"){
	        		   ym= getFormatDate(year+"-12-01",changes[i][0]);
	        	   }
	        	   var power_quotas_daily_data={energy:energy,ym:ym,val:val};
	        	   power_quotas_daily_sava.push(power_quotas_daily_data);
	            }
	          }
           },
			cells: function (row, col, prop) {
				var d = row + 1;
				var m = col - 1;
			    var cellProperties = {};
			    var data = this.instance.getData();
			    if (   (d == '31' && (m == '2' || m == '4' || m == '6' || m == '9' || m == '11'))
	  	        	|| (d == '30' && m == '2')
	  	        	|| (d == '29' && m == '2' && !test_date(cur_year, 2, 29))
	  	        ) {
			    	cellProperties.readOnly = true;
			    }
			    if (col === 0 ) {
			        cellProperties.renderer = firstRowRenderer; // uses function directly
			    }
			    if (col === 1 ) {
			        cellProperties.renderer = firstRowRenderer; // uses function directly
			    }
			    return cellProperties;
			}
    	});
    	/*var button_power = document.getElementById('export-file-power');
        var exportPlugin_power = power_handsontable.getPlugin('exportFile');
        button_power.addEventListener('click', function() {
        	exportPlugin_power.downloadFile('csv', {
        	bom: false,
            columnDelimiter: ',',
            columnHeaders: false,
            exportHiddenColumns: true,
            exportHiddenRows: true,
            fileExtension: 'csv',
            filename: 'Handsontable-CSV-file_[YYYY]-[MM]-[DD]',
            mimeType: 'text/csv',
            rowDelimiter: '\r\n',
            rowHeaders: true
          });
        });*/
    	
       /* var container_cda = document.getElementById('cda_grid');
    	var cda_handsontable=new Handsontable(container_cda,{*/
    	$("#cda_grid").handsontable({
            data: cda_quotas_daily,
            colWidths: window_width/14, // 设置所有列宽为90像素
    		rowHeights: 35,
    		//width: '100%',
    		height: '100%',//表内滚动条
            colHeaders: ["能源","日期","1月","2月","3月","4月","5月","6月","7月","8月","9月","10月","11月","12月"],
            columnSorting: true,
            columns:[
            	{
                    data:'energy',
                    readOnly:true
                },{
                    data:'day',
                    readOnly:true
                },{
                    data:"month1",
                },{
                    data:"month2",
                },{
                    data:"month3"
                },{
                    data:"month4"
                },{
                    data:"month5"
                },{
                    data:"month6"
                },{
                    data:"month7"
                },{
                    data:"month8"
                },{
                    data:"month9"
                },{
                    data:"month10"
                },{
                    data:"month11"
                },{
                    data:"month12"
                },
            ],
   
            afterChange:function(changes, source){
        	   if(change){
	               for (var i = 0; i < changes.length; i++) {
	        	   //var factory=quotas[parseInt(changes[i][0])].factory;
	        	   var energy=cda_quotas_daily[parseInt(changes[i][0])].energy;
	        	   var val=changes[i][3];
	        	   var ym;
	        	   if(changes[i][1]=="month1"){
	        		   ym= getFormatDate(year+"-01-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month2"){
	        		   ym= getFormatDate(year+"-02-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month3"){
	        		   ym= getFormatDate(year+"-03-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month4"){
	        		   ym= getFormatDate(year+"-04-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month5"){
	        		   ym= getFormatDate(year+"-05-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month6"){
	        		   ym= getFormatDate(year+"-06-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month7"){
	        		   ym= getFormatDate(year+"-07-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month8"){
	        		   ym= getFormatDate(year+"-08-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month9"){
	        		   ym= getFormatDate(year+"-09-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month10"){
	        		   ym= getFormatDate(year+"-10-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month11"){
	        		   ym= getFormatDate(year+"-11-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month12"){
	        		   ym= getFormatDate(year+"-12-01",changes[i][0]);
	        	   }
	        	   var cda_quotas_daily_data={energy:energy,ymd:ym,val:val};
	        	   cda_quotas_daily_sava.push(cda_quotas_daily_data);
	               }  
	           	}
           	},
   			cells: function (row, col, prop) {
				var d = row + 1;
				var m = col - 1;
			    var cellProperties = {};
			    if (   (d == '31' && (m == '2' || m == '4' || m == '6' || m == '9' || m == '11'))
	  	        	|| (d == '30' && m == '2')
	  	        	|| (d == '29' && m == '2' && !test_date(cur_year, 2, 29))
	  	        ) {
			    	cellProperties.readOnly = true;
			    }
			    if (col === 0 ) {
			        cellProperties.renderer = firstRowRenderer; // uses function directly
			    }
			    if (col === 1 ) {
			        cellProperties.renderer = firstRowRenderer; // uses function directly
			    }
			    return cellProperties;
			}
    	});
    	/*var button_cda = document.getElementById('export-file-cda');
    	var exportPlugin_cda = cda_handsontable.getPlugin('exportFile');
    	button_cda.addEventListener('click', function() {
    		exportPlugin_cda.downloadFile('csv', {
    			bom: false,
    			columnDelimiter: ',',
    			columnHeaders: false,
    			exportHiddenColumns: true,
    			exportHiddenRows: true,
    			fileExtension: 'csv',
    			filename: 'Handsontable-CSV-file_[YYYY]-[MM]-[DD]',
    			mimeType: 'text/csv',
    			rowDelimiter: '\r\n',
    			rowHeaders: true
    		});
    	});*/
    	
        
        /*var container_upw = document.getElementById('upw_grid');
    	var upw_handsontable=new Handsontable(container_upw,{*/
    	$("#upw_grid").handsontable({
            data: upw_quotas_daily,
            colWidths: window_width/14, // 设置所有列宽为90像素
            rowHeights: 35,
            //width: '100%',
            height: '100%',//表内滚动条
            colHeaders: ["能源","日期","1月","2月","3月","4月","5月","6月","7月","8月","9月","10月","11月","12月"],
            columnSorting: true,
            columns:[
            	{
                    data:'energy',
                    readOnly:true
                },{
                    data:'day',
                    readOnly:true
                },{
                    data:"month1",
                },{
                    data:"month2",
                },{
                    data:"month3"
                },{
                    data:"month4"
                },{
                    data:"month5"
                },{
                    data:"month6"
                },{
                    data:"month7"
                },{
                    data:"month8"
                },{
                    data:"month9"
                },{
                    data:"month10"
                },{
                    data:"month11"
                },{
                    data:"month12"
                },
            ],
            afterChange:function(changes, source){
        	   if(change){
	               for (var i = 0; i < changes.length; i++) {
	        	   //var factory=quotas[parseInt(changes[i][0])].factory;
	        	   var energy=upw_quotas_daily[parseInt(changes[i][0])].energy;
	        	   var val=changes[i][3];
	        	   var ym;
	        	   if(changes[i][1]=="month1"){
	        		   ym= getFormatDate(year+"-01-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month2"){
	        		   ym= getFormatDate(year+"-02-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month3"){
	        		   ym= getFormatDate(year+"-03-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month4"){
	        		   ym= getFormatDate(year+"-04-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month5"){
	        		   ym= getFormatDate(year+"-05-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month6"){
	        		   ym= getFormatDate(year+"-06-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month7"){
	        		   ym= getFormatDate(year+"-07-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month8"){
	        		   ym= getFormatDate(year+"-08-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month9"){
	        		   ym= getFormatDate(year+"-09-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month10"){
	        		   ym= getFormatDate(year+"-10-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month11"){
	        		   ym= getFormatDate(year+"-11-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month12"){
	        		   ym= getFormatDate(year+"-12-01",changes[i][0]);
	        	   }
	        	   var upw_quotas_daily_data={energy:energy,ymd:ym,val:val};
	        	   upw_quotas_daily_sava.push(upw_quotas_daily_data);
	               }  
	           }
           },
   			cells: function (row, col, prop) {
				var d = row + 1;
				var m = col - 1;
			    var cellProperties = {};
			    if (   (d == '31' && (m == '2' || m == '4' || m == '6' || m == '9' || m == '11'))
	  	        	|| (d == '30' && m == '2')
	  	        	|| (d == '29' && m == '2' && !test_date(cur_year, 2, 29))
	  	        ) {
			    	cellProperties.readOnly = true;
			    }
			    if (col === 0 ) {
			        cellProperties.renderer = firstRowRenderer; // uses function directly
			    }
			    if (col === 1 ) {
			        cellProperties.renderer = firstRowRenderer; // uses function directly
			    }
			    return cellProperties;
			}
    	});
    	/*var button_upw = document.getElementById('export-file-upw');
        var exportPlugin_upw = cda_handsontable.getPlugin('exportFile');
        button_upw.addEventListener('click', function() {
        	exportPlugin_upw.downloadFile('csv', {
            bom: false,
            columnDelimiter: ',',
            columnHeaders: false,
            exportHiddenColumns: true,
            exportHiddenRows: true,
            fileExtension: 'csv',
            filename: 'Handsontable-CSV-file_[YYYY]-[MM]-[DD]',
            mimeType: 'text/csv',
            rowDelimiter: '\r\n',
            rowHeaders: true
          });
        });*/
    	
       /* var container_pn2 = document.getElementById('pn2_grid');
    	var pn2_handsontable=new Handsontable(container_pn2,{*/
    	$("#pn2_grid").handsontable({
            data: pn2_quotas_daily,
            colWidths: window_width/14, // 设置所有列宽为90像素
            rowHeights: 35,
            //width: '100%',
            height: '100%',//表内滚动条
            colHeaders: ["能源","日期","1月","2月","3月","4月","5月","6月","7月","8月","9月","10月","11月","12月"],
            columnSorting: true,
            columns:[
            	{
                    data:'energy',
                    readOnly:true
                },{
                    data:'day',
                    readOnly:true
                },{
                    data:"month1",
                },{
                    data:"month2",
                },{
                    data:"month3"
                },{
                    data:"month4"
                },{
                    data:"month5"
                },{
                    data:"month6"
                },{
                    data:"month7"
                },{
                    data:"month8"
                },{
                    data:"month9"
                },{
                    data:"month10"
                },{
                    data:"month11"
                },{
                    data:"month12"
                },
                
            ],

            afterChange:function(changes, source){
        	   if(change){
	               for (var i = 0; i < changes.length; i++) {
	        	   //var factory=quotas[parseInt(changes[i][0])].factory;
	        	   var energy=pn2_quotas_daily[parseInt(changes[i][0])].energy;
	        	   var val=changes[i][3];
	        	   var ym;
	        	   if(changes[i][1]=="month1"){
	        		   ym= getFormatDate(year+"-01-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month2"){
	        		   ym= getFormatDate(year+"-02-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month3"){
	        		   ym= getFormatDate(year+"-03-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month4"){
	        		   ym= getFormatDate(year+"-04-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month5"){
	        		   ym= getFormatDate(year+"-05-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month6"){
	        		   ym= getFormatDate(year+"-06-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month7"){
	        		   ym= getFormatDate(year+"-07-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month8"){
	        		   ym= getFormatDate(year+"-08-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month9"){
	        		   ym= getFormatDate(year+"-09-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month10"){
	        		   ym= getFormatDate(year+"-10-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month11"){
	        		   ym= getFormatDate(year+"-11-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month12"){
	        		   ym= getFormatDate(year+"-12-01",changes[i][0]);
	        	   }
	        	   var pn2_quotas_daily_sava_data={energy:energy,ymd:ym,val:val};
	        	   pn2_quotas_daily_sava.push(pn2_quotas_daily_sava_data);
	        	   
	               }  
	           }
           },
   			cells: function (row, col, prop) {
				var d = row + 1;
				var m = col - 1;
			    var cellProperties = {};
			    if (   (d == '31' && (m == '2' || m == '4' || m == '6' || m == '9' || m == '11'))
	  	        	|| (d == '30' && m == '2')
	  	        	|| (d == '29' && m == '2' && !test_date(cur_year, 2, 29))
	  	        ) {
			    	cellProperties.readOnly = true;
			    }
			    if (col === 0 ) {
			        cellProperties.renderer = firstRowRenderer; // uses function directly
			    }
			    if (col === 1 ) {
			        cellProperties.renderer = firstRowRenderer; // uses function directly
			    }
			    return cellProperties;
			}
    	});
    	/*var button_pn2 = document.getElementById('export-file-pn2');
        var exportPlugin_pn2 = pn2_handsontable.getPlugin('exportFile');
        button_pn2.addEventListener('click', function() {
        	exportPlugin_pn2.downloadFile('csv', {
            bom: false,
            columnDelimiter: ',',
            columnHeaders: false,
            exportHiddenColumns: true,
            exportHiddenRows: true,
            fileExtension: 'csv',
            filename: 'Handsontable-CSV-file_[YYYY]-[MM]-[DD]',
            mimeType: 'text/csv',
            rowDelimiter: '\r\n',
            rowHeaders: true
          });
        });*/
    	
        /*var container_cw = document.getElementById('cw_grid');
    	var cw_handsontable=new Handsontable(container_cw,{*/
    	$("#cw_grid").handsontable({
            data: cw_quotas_daily,
            colWidths: window_width/14, // 设置所有列宽为90像素
            rowHeights: 35,
            //width: '100%',
            height: '100%',//表内滚动条
            colHeaders: ["能源","日期","1月","2月","3月","4月","5月","6月","7月","8月","9月","10月","11月","12月"],
            columnSorting: true,
            columns:[
            	{
                    data:'energy',
                    readOnly:true
                },{
                    data:'day',
                    readOnly:true
                },{
                    data:"month1",
                },{
                    data:"month2",
                },{
                    data:"month3"
                },{
                    data:"month4"
                },{
                    data:"month5"
                },{
                    data:"month6"
                },{
                    data:"month7"
                },{
                    data:"month8"
                },{
                    data:"month9"
                },{
                    data:"month10"
                },{
                    data:"month11"
                },{
                    data:"month12"
                },
            ],
   
            afterChange:function(changes, source){
        	   if(change){
	               for (var i = 0; i < changes.length; i++) {
	        	   //var factory=quotas[parseInt(changes[i][0])].factory;
	        	   var energy=cw_quotas_daily[parseInt(changes[i][0])].energy;
	        	   var val=changes[i][3];
	        	   var ym;
	        	   if(changes[i][1]=="month1"){
	        		   ym= getFormatDate(year+"-01-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month2"){
	        		   ym= getFormatDate(year+"-02-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month3"){
	        		   ym= getFormatDate(year+"-03-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month4"){
	        		   ym= getFormatDate(year+"-04-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month5"){
	        		   ym= getFormatDate(year+"-05-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month6"){
	        		   ym= getFormatDate(year+"-06-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month7"){
	        		   ym= getFormatDate(year+"-07-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month8"){
	        		   ym= getFormatDate(year+"-08-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month9"){
	        		   ym= getFormatDate(year+"-09-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month10"){
	        		   ym= getFormatDate(year+"-10-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month11"){
	        		   ym= getFormatDate(year+"-11-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month12"){
	        		   ym= getFormatDate(year+"-12-01",changes[i][0]);
	        	   }
	        	   var cw_quotas_daily_sava_data={energy:energy,ymd:ym,val:val};
	        	   cw_quotas_daily_sava.push(cw_quotas_daily_sava_data);
	        	   
	               }  
	           }
           },
   			cells: function (row, col, prop) {
				var d = row + 1;
				var m = col - 1;
			    var cellProperties = {};
			    if (   (d == '31' && (m == '2' || m == '4' || m == '6' || m == '9' || m == '11'))
	  	        	|| (d == '30' && m == '2')
	  	        	|| (d == '29' && m == '2' && !test_date(cur_year, 2, 29))
	  	        ) {
			    	cellProperties.readOnly = true;
			    }
			    if (col === 0 ) {
			        cellProperties.renderer = firstRowRenderer; // uses function directly
			    }
			    if (col === 1 ) {
			        cellProperties.renderer = firstRowRenderer; // uses function directly
			    }
			    return cellProperties;
			}
    	});
    	/*var button_cw = document.getElementById('export-file-cw');
        var exportPlugin_cw = cw_handsontable.getPlugin('exportFile');
        button_cw.addEventListener('click', function() {
        	exportPlugin_pn2.downloadFile('csv', {
            bom: false,
            columnDelimiter: ',',
            columnHeaders: false,
            exportHiddenColumns: true,
            exportHiddenRows: true,
            fileExtension: 'csv',
            filename: 'Handsontable-CSV-file_[YYYY]-[MM]-[DD]',
            mimeType: 'text/csv',
            rowDelimiter: '\r\n',
            rowHeaders: true
          });
        });*/
    	
       /* var container_gas = document.getElementById('gas_grid');
    	var cw_handsontable=new Handsontable(container_gas,{*/
    	$("#gas_grid").handsontable({
            data: gas_quotas_daily,
            colWidths: window_width/14, // 设置所有列宽为90像素
            rowHeights: 35,
            //width: '100%',
            height: '100%',//表内滚动条
            colHeaders: ["能源","日期","1月","2月","3月","4月","5月","6月","7月","8月","9月","10月","11月","12月"],
            columnSorting: true,
            columns:[
            	{
                    data:'energy',
                    readOnly:true
                },{
                    data:'day',
                    readOnly:true
                },{
                    data:"month1",
                },{
                    data:"month2",
                },{
                    data:"month3"
                },{
                    data:"month4"
                },{
                    data:"month5"
                },{
                    data:"month6"
                },{
                    data:"month7"
                },{
                    data:"month8"
                },{
                    data:"month9"
                },{
                    data:"month10"
                },{
                    data:"month11"
                },{
                    data:"month12"
                },
                
            ],
   
            afterChange:function(changes, source){
        	   if(change){
	               for (var i = 0; i < changes.length; i++) {
	        	   //var factory=quotas[parseInt(changes[i][0])].factory;
	        	   var energy=gas_quotas_daily[parseInt(changes[i][0])].energy;
	        	   var val=changes[i][3];
	        	   var ym;
	        	   if(changes[i][1]=="month1"){
	        		   ym= getFormatDate(year+"-01-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month2"){
	        		   ym= getFormatDate(year+"-02-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month3"){
	        		   ym= getFormatDate(year+"-03-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month4"){
	        		   ym= getFormatDate(year+"-04-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month5"){
	        		   ym= getFormatDate(year+"-05-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month6"){
	        		   ym= getFormatDate(year+"-06-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month7"){
	        		   ym= getFormatDate(year+"-07-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month8"){
	        		   ym= getFormatDate(year+"-08-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month9"){
	        		   ym= getFormatDate(year+"-09-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month10"){
	        		   ym= getFormatDate(year+"-10-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month11"){
	        		   ym= getFormatDate(year+"-11-01",changes[i][0]);
	        	   }
	        	   if(changes[i][1]=="month12"){
	        		   ym= getFormatDate(year+"-12-01",changes[i][0]);
	        	   }
	        	   var gas_quotas_daily_data={energy:energy,ymd:ym,val:val};
	        	   gas_quotas_daily_sava.push(gas_quotas_daily_data);
	               }
	           }
           },
   			cells: function (row, col, prop) {
				var d = row + 1;
				var m = col - 1;
			    var cellProperties = {};
			    if (   (d == '31' && (m == '2' || m == '4' || m == '6' || m == '9' || m == '11'))
	  	        	|| (d == '30' && m == '2')
	  	        	|| (d == '29' && m == '2' && !test_date(cur_year, 2, 29))
	  	        ) {
			    	cellProperties.readOnly = true;
			    }
			    if (col === 0 ) {
			        cellProperties.renderer = firstRowRenderer; // uses function directly
			    }
			    if (col === 1 ) {
			        cellProperties.renderer = firstRowRenderer; // uses function directly
			    }
			    return cellProperties;
			}
    	});
    	/*var button_gas = document.getElementById('export-file-gas');
        var exportPlugin_gas = cw_handsontable.getPlugin('exportFile');
        button_gas.addEventListener('click', function() {
        	exportPlugin_gas.downloadFile('csv', {
            bom: false,
            columnDelimiter: ',',
            columnHeaders: false,
            exportHiddenColumns: true,
            exportHiddenRows: true,
            fileExtension: 'csv',
            filename: 'Handsontable-CSV-file_[YYYY]-[MM]-[DD]',
            mimeType: 'text/csv',
            rowDelimiter: '\r\n',
            rowHeaders: true
          });
        });*/
    	change=true;
    }

    	/*$("#quota_grid").dxDataGrid({
    		dataSource: quotas,
 	        showBorders: true,
 	        showColumnLines: true,
 	        allowColumnReordering: true,
 	        allowColumnResizing: true,
 	        showRowLines: true,
 	        rowAlternationEnabled: true,
 	        paging: {
 	            enabled: false
 	        },
 	        columnFixing: { 
 	            enabled: true
 	        },
 	        searchPanel: {
 	            visible: true,
 	            width: 240,
 	            placeholder: "搜索..."
 	        },
 	        editing: {
 	            mode: "batch",
 	            allowUpdating: true
 	        }, 
 	        selection: {
 	            mode: "multiple"
 	        },
 	        "export": {
 	            enabled: true,
 	            fileName: "定额设置",
 	            allowExportSelectedData: true
 	        },
 	        columnChooser: {
 	            enabled: true,
 	            mode: "select"
 	        },
 	        columns: [
 	            {
 	                dataField: "energy",
 	                allowEditing:false,
 	                fixed: true,
 	                cssClass: "fac_enr",
 	                caption: "能源"
 	            },
 	            {
 	                dataField: "month1",
 	                validationRules: [{ type: "required" }, {
 	                    type: "pattern",
 	                    message: '您所填写的含有非法字符,只能填写数字.',
 	                    pattern: /^[+]{0,1}(\d+)$/
 	                }],
 	                caption: "1月"
 	            },
 	            {
 	                dataField: "month2",
 	                validationRules: [{ type: "required" }, {
 	                    type: "pattern",
 	                    message: '您所填写的含有非法字符,只能填写数字.',
 	                    pattern: /^[+]{0,1}(\d+)$/
 	                }],
 	                caption: "2月"
 	            },
 	            {
 	                dataField: "month3",
 	                validationRules: [{ type: "required" }, {
 	                    type: "pattern",
 	                    message: '您所填写的含有非法字符,只能填写数字.',
 	                    pattern: /^[+]{0,1}(\d+)$/
 	                }],
 	                caption: "3月"
 	            },
 	            {
 	                dataField: "month4",
 	                validationRules: [{ type: "required" }, {
 	                    type: "pattern",
 	                    message: '您所填写的含有非法字符,只能填写数字.',
 	                    pattern: /^[+]{0,1}(\d+)$/
 	                }],
 	                caption: "4月"
 	            },
 	            {
 	                dataField: "month5",
 	                validationRules: [{ type: "required" }, {
 	                    type: "pattern",
 	                    message: '您所填写的含有非法字符,只能填写数字.',
 	                    pattern: /^[+]{0,1}(\d+)$/
 	                }],
 	                caption: "5月"
 	            },
 	            {
 	                dataField: "month6",
 	                validationRules: [{ type: "required" }, {
 	                    type: "pattern",
 	                    message: '您所填写的含有非法字符,只能填写数字.',
 	                    pattern: /^[+]{0,1}(\d+)$/
 	                }],
 	                caption: "6月"
 	            },
 	            {
 	                dataField: "month7",
 	                validationRules: [{ type: "required" }, {
 	                    type: "pattern",
 	                    message: '您所填写的含有非法字符,只能填写数字.',
 	                    pattern: /^[+]{0,1}(\d+)$/
 	                }],
 	                caption: "7月"
 	            },
 	            {
 	                dataField: "month8",
 	                validationRules: [{ type: "required" }, {
 	                    type: "pattern",
 	                    message: '您所填写的含有非法字符,只能填写数字.',
 	                    pattern: /^[+]{0,1}(\d+)$/
 	                }],
 	                caption: "8月"
 	            },
 	            {
 	                dataField: "month9",
 	                validationRules: [{ type: "required" }, {
 	                    type: "pattern",
 	                    message: '您所填写的含有非法字符,只能填写数字.',
 	                    pattern: /^[+]{0,1}(\d+)$/
 	                }],
 	                caption: "9月"
 	            },
 	            {
 	                dataField: "month10",
 	                validationRules: [{ type: "required" }, {
 	                    type: "pattern",
 	                    message: '您所填写的含有非法字符,只能填写数字.',
 	                    pattern: /^[+]{0,1}(\d+)$/
 	                }],
 	                caption: "10月"
 	            },{
 	                dataField: "month11",
 	                validationRules: [{ type: "required" }, {
 	                    type: "pattern",
 	                    message: '您所填写的含有非法字符,只能填写数字.',
 	                    pattern: /^[+]{0,1}(\d+)$/
 	                }],
 	                caption: "11月"
 	            },{
 	                dataField: "month12",
 	                validationRules: [{ type: "required" }, {
 	                    type: "pattern",
 	                    message: '您所填写的含有非法字符,只能填写数字.',
 	                    pattern: /^[+]{0,1}(\d+)$/
 	                }],
 	                caption: "12月"
 	            }
 	        ],
 	        onEditingStart: function(e) {
 	            logEvent("您正在编辑工厂为:"+e.data.factory+",能源为"+e.data.energy+"的这一条记录。");
 	        },
 	        onInitNewRow: function(e) {
 	            logEvent("InitNewRow");
 	        },
 	        onRowInserting: function(e) {
 	            logEvent("RowInserting");
 	        },
 	        onRowInserted: function(e) {
 	            logEvent("RowInserted");
 	        },
 	        onRowUpdating: function(e) {

 	        	var uptRecords = e.newData;
 	        	var key = e.key;
 	        	var f= "factory";
 	        	var e = "energy";
 	        	var str = "您修改了";
 	        	var data = new Object();
 	        	data.factory = eval("key."+f);
 	        	data.energy = eval("key."+e);
 	        	for(var k in uptRecords){
 	        		str += k.substring(5,k.length)+"月、"
 	        		data[k] = uptRecords[k];
 	        	}
 	        	str += str.substring(0,str.length-1)+"的值。"
 	        	 $.ajax({
 	        	        url: path + '/quota/replace_quota_total.do?year='+year,
 	        	        type: 'get',
 	        	        data:data,
 	        	        async: false,
 	        	        success: function (data) {
 	        	        	if(data == "1"){
 	        	        		logEvent(str);
 	        	        		logEvent("您已修改成功.")
 	        	        	}
 	        	        }
 	        	    });
 	        },      
 	        onRowRemoving: function(e) {
 	            logEvent("RowRemoving");
 	        },
 	        onRowRemoved: function(e) {
 	            logEvent("RowRemoved");
 	        }
 	    });
 	    
    	 
    	 $("#power_grid").dxDataGrid({
 	        dataSource: power_quotas_daily,
 	        showBorders: true,
 	        showColumnLines: true,
 	        allowColumnReordering: true,
 	        allowColumnResizing: true,
 	        showRowLines: true,
 	        rowAlternationEnabled: true,
 	        paging: {
 	            enabled: false
 	        },
 	        columnFixing: { 
 	            enabled: true
 	        },
 	        searchPanel: {
 	            visible: true,
 	            width: 240,
 	            placeholder: "搜索..."
 	        },
 	        editing: {
 	            mode: "batch",
 	            allowUpdating: true
 	        }, 
 	        selection: {
 	            mode: "multiple"
 	        },
 	        "export": {
 	            enabled: true,
 	            fileName: "定额设置",
 	            allowExportSelectedData: true
 	        },
 	        columnChooser: {
 	            enabled: true,
 	            mode: "select"
 	        },
 	        columns: [
 	            {
 	                dataField: "energy",
 	                allowEditing:false,
 	                fixed: true,
 	                cssClass: "fac_enr",
 	                caption: "能源"
 	            },
 	           {
 	                dataField: "day",
 	                allowEditing:false,
 	                fixed: true,
 	                cssClass: "fac_enr",
 	                caption: "日期"
 	            },
 	            {
 	                dataField: "month1",
 	                validationRules: [{ type: "required" }, {
 	                    type: "pattern",
 	                    message: '您所填写的含有非法字符,只能填写数字.',
 	                    pattern: /^[+]{0,1}(\d+)$/
 	                }],
 	                caption: "1月"
 	            },
 	            {
 	                dataField: "month2",
 	                validationRules: [{ type: "required" }, {
 	                    type: "pattern",
 	                    message: '您所填写的含有非法字符,只能填写数字.',
 	                    pattern: /^[+]{0,1}(\d+)$/
 	                }],
 	                caption: "2月"
 	            },
 	            {
 	                dataField: "month3",
 	                validationRules: [{ type: "required" }, {
 	                    type: "pattern",
 	                    message: '您所填写的含有非法字符,只能填写数字.',
 	                    pattern: /^[+]{0,1}(\d+)$/
 	                }],
 	                caption: "3月"
 	            },
 	            {
 	                dataField: "month4",
 	                validationRules: [{ type: "required" }, {
 	                    type: "pattern",
 	                    message: '您所填写的含有非法字符,只能填写数字.',
 	                    pattern: /^[+]{0,1}(\d+)$/
 	                }],
 	                caption: "4月"
 	            },
 	            {
 	                dataField: "month5",
 	                validationRules: [{ type: "required" }, {
 	                    type: "pattern",
 	                    message: '您所填写的含有非法字符,只能填写数字.',
 	                    pattern: /^[+]{0,1}(\d+)$/
 	                }],
 	                caption: "5月"
 	            },
 	            {
 	                dataField: "month6",
 	                validationRules: [{ type: "required" }, {
 	                    type: "pattern",
 	                    message: '您所填写的含有非法字符,只能填写数字.',
 	                    pattern: /^[+]{0,1}(\d+)$/
 	                }],
 	                caption: "6月"
 	            },
 	            {
 	                dataField: "month7",
 	                validationRules: [{ type: "required" }, {
 	                    type: "pattern",
 	                    message: '您所填写的含有非法字符,只能填写数字.',
 	                    pattern: /^[+]{0,1}(\d+)$/
 	                }],
 	                caption: "7月"
 	            },
 	            {
 	                dataField: "month8",
 	                validationRules: [{ type: "required" }, {
 	                    type: "pattern",
 	                    message: '您所填写的含有非法字符,只能填写数字.',
 	                    pattern: /^[+]{0,1}(\d+)$/
 	                }],
 	                caption: "8月"
 	            },
 	            {
 	                dataField: "month9",
 	                validationRules: [{ type: "required" }, {
 	                    type: "pattern",
 	                    message: '您所填写的含有非法字符,只能填写数字.',
 	                    pattern: /^[+]{0,1}(\d+)$/
 	                }],
 	                caption: "9月"
 	            },
 	            {
 	                dataField: "month10",
 	                validationRules: [{ type: "required" }, {
 	                    type: "pattern",
 	                    message: '您所填写的含有非法字符,只能填写数字.',
 	                    pattern: /^[+]{0,1}(\d+)$/
 	                }],
 	                caption: "10月"
 	            },{
 	                dataField: "month11",
 	                validationRules: [{ type: "required" }, {
 	                    type: "pattern",
 	                    message: '您所填写的含有非法字符,只能填写数字.',
 	                    pattern: /^[+]{0,1}(\d+)$/
 	                }],
 	                caption: "11月"
 	            },{
 	                dataField: "month12",
 	                validationRules: [{ type: "required" }, {
 	                    type: "pattern",
 	                    message: '您所填写的含有非法字符,只能填写数字.',
 	                    pattern: /^[+]{0,1}(\d+)$/
 	                }],
 	                caption: "12月"
 	            }
 	        ],
 	        onEditingStart: function(e) {
  	        	var m = e.column.dataField;
  	        	var d = e.key.day;
  	        	if (   (d == '31' && (m == 'month2' || m == 'month4' || m == 'month6' || m == 'month9' || m == 'month11'))
  	        		|| (d == '30' && m == 'month2')
  	        		|| (d == '29' && m == 'month2' && !test_date(cur_year, 2, 29))
  	        	) {
  	        		e.cancel = true;
  	        		return;
  	        	}
 	            logEvent("您正在编辑工厂为:"+e.data.factory+",能源为"+e.data.energy+"的这一条记录。");
 	        },
 	        onInitNewRow: function(e) {
 	            logEvent("InitNewRow");
 	        },
 	        onRowInserting: function(e) {
 	            logEvent("RowInserting");
 	        },
 	        onRowInserted: function(e) {
 	            logEvent("RowInserted");
 	        },
 	        onRowUpdating: function(e) {
 	        	var uptRecords = e.newData;
 	        	var key = e.key;
 	        	var d = "day"
 	        	var e = "energy";
 	        	var str = "您修改了";
 	        	var data = new Object();
 	        	data.energy = eval("key."+e);
 	        	data.day = eval("key."+d);
 	        	for(var k in uptRecords){
 	        		str += k.substring(5,k.length)+"月、"
 	        		data[k] = uptRecords[k];
 	        	}
 	        	str += str.substring(0,str.length-1)+"的值。"
 	        	 $.ajax({
 	        	        url: path + '/quota/replace_quota_daily.do?year='+year,
 	        	        type: 'get',
 	        	        data:data,
 	        	        async: false,
 	        	        success: function (data) {
 	        	        	if(data == "1"){
 	        	        		logEvent(str);
 	        	        		logEvent("您已修改成功.")
 	        	        	}
 	        	        }
 	        	    });
 	        },
 	        onRowRemoving: function(e) {
 	            logEvent("RowRemoving");
 	        },
 	        onRowRemoved: function(e) {
 	            logEvent("RowRemoved");
 	        }
 	    });
    	 
    	 $("#cda_grid").dxDataGrid({
  	        dataSource: cda_quotas_daily,
  	        showBorders: true,
  	        showColumnLines: true,
  	        allowColumnReordering: true,
  	        allowColumnResizing: true,
  	        showRowLines: true,
  	        rowAlternationEnabled: true,
  	        paging: {
  	            enabled: false
  	        },
  	        columnFixing: { 
  	            enabled: true
  	        },
  	        searchPanel: {
  	            visible: true,
  	            width: 240,
  	            placeholder: "搜索..."
  	        },
  	        editing: {
  	            mode: "batch",
  	            allowUpdating: true
  	        }, 
  	        selection: {
  	            mode: "multiple"
  	        },
  	        "export": {
  	            enabled: true,
  	            fileName: "定额设置",
  	            allowExportSelectedData: true
  	        },
  	        columnChooser: {
  	            enabled: true,
  	            mode: "select"
  	        },
  	        columns: [
  	            {
  	                dataField: "energy",
  	                allowEditing:false,
  	                fixed: true,
  	                cssClass: "fac_enr",
  	                caption: "能源"
  	            },
  	           {
  	                dataField: "day",
  	                allowEditing:false,
  	                fixed: true,
  	                cssClass: "fac_enr",
  	                caption: "日期"
  	            },
  	            {
  	                dataField: "month1",
  	                validationRules: [{ type: "required" }, {
  	                    type: "pattern",
  	                    message: '您所填写的含有非法字符,只能填写数字.',
  	                    pattern: /^[+]{0,1}(\d+)$/
  	                }],
  	                caption: "1月"
  	            },
  	            {
  	                dataField: "month2",
  	                validationRules: [{ type: "required" }, {
  	                    type: "pattern",
  	                    message: '您所填写的含有非法字符,只能填写数字.',
  	                    pattern: /^[+]{0,1}(\d+)$/
  	                }],
  	                caption: "2月"
  	            },
  	            {
  	                dataField: "month3",
  	                validationRules: [{ type: "required" }, {
  	                    type: "pattern",
  	                    message: '您所填写的含有非法字符,只能填写数字.',
  	                    pattern: /^[+]{0,1}(\d+)$/
  	                }],
  	                caption: "3月"
  	            },
  	            {
  	                dataField: "month4",
  	                validationRules: [{ type: "required" }, {
  	                    type: "pattern",
  	                    message: '您所填写的含有非法字符,只能填写数字.',
  	                    pattern: /^[+]{0,1}(\d+)$/
  	                }],
  	                caption: "4月"
  	            },
  	            {
  	                dataField: "month5",
  	                validationRules: [{ type: "required" }, {
  	                    type: "pattern",
  	                    message: '您所填写的含有非法字符,只能填写数字.',
  	                    pattern: /^[+]{0,1}(\d+)$/
  	                }],
  	                caption: "5月"
  	            },
  	            {
  	                dataField: "month6",
  	                validationRules: [{ type: "required" }, {
  	                    type: "pattern",
  	                    message: '您所填写的含有非法字符,只能填写数字.',
  	                    pattern: /^[+]{0,1}(\d+)$/
  	                }],
  	                caption: "6月"
  	            },
  	            {
  	                dataField: "month7",
  	                validationRules: [{ type: "required" }, {
  	                    type: "pattern",
  	                    message: '您所填写的含有非法字符,只能填写数字.',
  	                    pattern: /^[+]{0,1}(\d+)$/
  	                }],
  	                caption: "7月"
  	            },
  	            {
  	                dataField: "month8",
  	                validationRules: [{ type: "required" }, {
  	                    type: "pattern",
  	                    message: '您所填写的含有非法字符,只能填写数字.',
  	                    pattern: /^[+]{0,1}(\d+)$/
  	                }],
  	                caption: "8月"
  	            },
  	            {
  	                dataField: "month9",
  	                validationRules: [{ type: "required" }, {
  	                    type: "pattern",
  	                    message: '您所填写的含有非法字符,只能填写数字.',
  	                    pattern: /^[+]{0,1}(\d+)$/
  	                }],
  	                caption: "9月"
  	            },
  	            {
  	                dataField: "month10",
  	                validationRules: [{ type: "required" }, {
  	                    type: "pattern",
  	                    message: '您所填写的含有非法字符,只能填写数字.',
  	                    pattern: /^[+]{0,1}(\d+)$/
  	                }],
  	                caption: "10月"
  	            },{
  	                dataField: "month11",
  	                validationRules: [{ type: "required" }, {
  	                    type: "pattern",
  	                    message: '您所填写的含有非法字符,只能填写数字.',
  	                    pattern: /^[+]{0,1}(\d+)$/
  	                }],
  	                caption: "11月"
  	            },{
  	                dataField: "month12",
  	                validationRules: [{ type: "required" }, {
  	                    type: "pattern",
  	                    message: '您所填写的含有非法字符,只能填写数字.',
  	                    pattern: /^[+]{0,1}(\d+)$/
  	                }],
  	                caption: "12月"
  	            }
  	        ],
  	        onEditingStart: function(e) {
  	        	var m = e.column.dataField;
  	        	var d = e.key.day;
  	        	if (   (d == '31' && (m == 'month2' || m == 'month4' || m == 'month6' || m == 'month9' || m == 'month11'))
  	        		|| (d == '30' && m == 'month2')
  	        		|| (d == '29' && m == 'month2' && !test_date(cur_year, 2, 29))
  	        	) {
  	        		e.cancel = true;
  	        		return;
  	        	}
  	            logEvent("您正在编辑工厂为:"+e.data.factory+",能源为"+e.data.energy+"的这一条记录。");
  	        },
  	        onInitNewRow: function(e) {
  	            logEvent("InitNewRow");
  	        },
  	        onRowInserting: function(e) {
  	            logEvent("RowInserting");
  	        },
  	        onRowInserted: function(e) {
  	            logEvent("RowInserted");
  	        },
  	        onRowUpdating: function(e) {
  	        	var uptRecords = e.newData;
  	        	var key = e.key;
  	        	var d = "day"
  	        	var e = "energy";
  	        	var str = "您修改了";
  	        	var data = new Object();
  	        	data.energy = eval("key."+e);
  	        	data.day = eval("key."+d);
  	        	for(var k in uptRecords){
  	        		str += k.substring(5,k.length)+"月、"
  	        		data[k] = uptRecords[k];
  	        	}
  	        	str += str.substring(0,str.length-1)+"的值。"
  	        	 $.ajax({
  	        	        url: path + '/quota/replace_quota_daily.do?year='+year,
  	        	        type: 'get',
  	        	        data:data,
  	        	        async: false,
  	        	        success: function (data) {
  	        	        	if(data == "1"){
  	        	        		logEvent(str);
  	        	        		logEvent("您已修改成功.")
  	        	        	}
  	        	        }
  	        	    });
  	        	
  	        },
  	        onRowRemoving: function(e) {
  	            logEvent("RowRemoving");
  	        },
  	        onRowRemoved: function(e) {
  	            logEvent("RowRemoved");
  	        }
  	    });
    	 
    	 $("#upw_grid").dxDataGrid({
  	        dataSource: upw_quotas_daily,
  	        showBorders: true,
  	        showColumnLines: true,
  	        allowColumnReordering: true,
  	        allowColumnResizing: true,
  	        showRowLines: true,
  	        rowAlternationEnabled: true,
  	        paging: {
  	            enabled: false
  	        },
  	        columnFixing: { 
  	            enabled: true
  	        },
  	        searchPanel: {
  	            visible: true,
  	            width: 240,
  	            placeholder: "搜索..."
  	        },
  	        editing: {
  	            mode: "batch",
  	            allowUpdating: true
  	        }, 
  	        selection: {
  	            mode: "multiple"
  	        },
  	        "export": {
  	            enabled: true,
  	            fileName: "定额设置",
  	            allowExportSelectedData: true
  	        },
  	        columnChooser: {
  	            enabled: true,
  	            mode: "select"
  	        },
  	        columns: [
  	            {
  	                dataField: "energy",
  	                allowEditing:false,
  	                fixed: true,
  	                cssClass: "fac_enr",
  	                caption: "能源"
  	            },
  	           {
  	                dataField: "day",
  	                allowEditing:false,
  	                fixed: true,
  	                cssClass: "fac_enr",
  	                caption: "日期"
  	            },
  	            {
  	                dataField: "month1",
  	                validationRules: [{ type: "required" }, {
  	                    type: "pattern",
  	                    message: '您所填写的含有非法字符,只能填写数字.',
  	                    pattern: /^[+]{0,1}(\d+)$/
  	                }],
  	                caption: "1月"
  	            },
  	            {
  	                dataField: "month2",
  	                validationRules: [{ type: "required" }, {
  	                    type: "pattern",
  	                    message: '您所填写的含有非法字符,只能填写数字.',
  	                    pattern: /^[+]{0,1}(\d+)$/
  	                }],
  	                caption: "2月"
  	            },
  	            {
  	                dataField: "month3",
  	                validationRules: [{ type: "required" }, {
  	                    type: "pattern",
  	                    message: '您所填写的含有非法字符,只能填写数字.',
  	                    pattern: /^[+]{0,1}(\d+)$/
  	                }],
  	                caption: "3月"
  	            },
  	            {
  	                dataField: "month4",
  	                validationRules: [{ type: "required" }, {
  	                    type: "pattern",
  	                    message: '您所填写的含有非法字符,只能填写数字.',
  	                    pattern: /^[+]{0,1}(\d+)$/
  	                }],
  	                caption: "4月"
  	            },
  	            {
  	                dataField: "month5",
  	                validationRules: [{ type: "required" }, {
  	                    type: "pattern",
  	                    message: '您所填写的含有非法字符,只能填写数字.',
  	                    pattern: /^[+]{0,1}(\d+)$/
  	                }],
  	                caption: "5月"
  	            },
  	            {
  	                dataField: "month6",
  	                validationRules: [{ type: "required" }, {
  	                    type: "pattern",
  	                    message: '您所填写的含有非法字符,只能填写数字.',
  	                    pattern: /^[+]{0,1}(\d+)$/
  	                }],
  	                caption: "6月"
  	            },
  	            {
  	                dataField: "month7",
  	                validationRules: [{ type: "required" }, {
  	                    type: "pattern",
  	                    message: '您所填写的含有非法字符,只能填写数字.',
  	                    pattern: /^[+]{0,1}(\d+)$/
  	                }],
  	                caption: "7月"
  	            },
  	            {
  	                dataField: "month8",
  	                validationRules: [{ type: "required" }, {
  	                    type: "pattern",
  	                    message: '您所填写的含有非法字符,只能填写数字.',
  	                    pattern: /^[+]{0,1}(\d+)$/
  	                }],
  	                caption: "8月"
  	            },
  	            {
  	                dataField: "month9",
  	                validationRules: [{ type: "required" }, {
  	                    type: "pattern",
  	                    message: '您所填写的含有非法字符,只能填写数字.',
  	                    pattern: /^[+]{0,1}(\d+)$/
  	                }],
  	                caption: "9月"
  	            },
  	            {
  	                dataField: "month10",
  	                validationRules: [{ type: "required" }, {
  	                    type: "pattern",
  	                    message: '您所填写的含有非法字符,只能填写数字.',
  	                    pattern: /^[+]{0,1}(\d+)$/
  	                }],
  	                caption: "10月"
  	            },{
  	                dataField: "month11",
  	                validationRules: [{ type: "required" }, {
  	                    type: "pattern",
  	                    message: '您所填写的含有非法字符,只能填写数字.',
  	                    pattern: /^[+]{0,1}(\d+)$/
  	                }],
  	                caption: "11月"
  	            },{
  	                dataField: "month12",
  	                validationRules: [{ type: "required" }, {
  	                    type: "pattern",
  	                    message: '您所填写的含有非法字符,只能填写数字.',
  	                    pattern: /^[+]{0,1}(\d+)$/
  	                }],
  	                caption: "12月"
  	            }
  	        ],
  	        onEditingStart: function(e) {
  	        	var m = e.column.dataField;
  	        	var d = e.key.day;
  	        	if (   (d == '31' && (m == 'month2' || m == 'month4' || m == 'month6' || m == 'month9' || m == 'month11'))
  	        		|| (d == '30' && m == 'month2')
  	        		|| (d == '29' && m == 'month2' && !test_date(cur_year, 2, 29))
  	        	) {
  	        		e.cancel = true;
  	        		return;
  	        	}
  	            logEvent("您正在编辑工厂为:"+e.data.factory+",能源为"+e.data.energy+"的这一条记录。");
  	        },
  	        onInitNewRow: function(e) {
  	            logEvent("InitNewRow");
  	        },
  	        onRowInserting: function(e) {
  	            logEvent("RowInserting");
  	        },
  	        onRowInserted: function(e) {
  	            logEvent("RowInserted");
  	        },
  	        onRowUpdating: function(e) {
  	        	var uptRecords = e.newData;
  	        	var key = e.key;
  	        	var d = "day"
  	        	var e = "energy";
  	        	var str = "您修改了";
  	        	var data = new Object();
  	        	data.energy = eval("key."+e);
  	        	data.day = eval("key."+d);
  	        	for(var k in uptRecords){
  	        		str += k.substring(5,k.length)+"月、"
  	        		data[k] = uptRecords[k];
  	        	}
  	        	str += str.substring(0,str.length-1)+"的值。"
  	        	 $.ajax({
  	        	        url: path + '/quota/replace_quota_daily.do?year='+year,
  	        	        type: 'get',
  	        	        data:data,
  	        	        async: false,
  	        	        success: function (data) {
  	        	        	if(data == "1"){
  	        	        		logEvent(str);
  	        	        		logEvent("您已修改成功.")
  	        	        	}
  	        	        }
  	        	    });
  	        	
  	        },
  	        onRowRemoving: function(e) {
  	            logEvent("RowRemoving");
  	        },
  	        onRowRemoved: function(e) {
  	            logEvent("RowRemoved");
  	        }
  	    });
    	 
    	 $("#pn2_grid").dxDataGrid({
  	        dataSource: pn2_quotas_daily,
  	        showBorders: true,
  	        showColumnLines: true,
  	        allowColumnReordering: true,
  	        allowColumnResizing: true,
  	        showRowLines: true,
  	        rowAlternationEnabled: true,
  	        paging: {
  	            enabled: false
  	        },
  	        columnFixing: { 
  	            enabled: true
  	        },
  	        searchPanel: {
  	            visible: true,
  	            width: 240,
  	            placeholder: "搜索..."
  	        },
  	        editing: {
  	            mode: "batch",
  	            allowUpdating: true
  	        }, 
  	        selection: {
  	            mode: "multiple"
  	        },
  	        "export": {
  	            enabled: true,
  	            fileName: "定额设置",
  	            allowExportSelectedData: true
  	        },
  	        columnChooser: {
  	            enabled: true,
  	            mode: "select"
  	        },
  	        columns: [
  	            {
  	                dataField: "energy",
  	                allowEditing:false,
  	                fixed: true,
  	                cssClass: "fac_enr",
  	                caption: "能源"
  	            },
  	           {
  	                dataField: "day",
  	                allowEditing:false,
  	                fixed: true,
  	                cssClass: "fac_enr",
  	                caption: "日期"
  	            },
  	            {
  	                dataField: "month1",
  	                validationRules: [{ type: "required" }, {
  	                    type: "pattern",
  	                    message: '您所填写的含有非法字符,只能填写数字.',
  	                    pattern: /^[+]{0,1}(\d+)$/
  	                }],
  	                caption: "1月"
  	            },
  	            {
  	                dataField: "month2",
  	                validationRules: [{ type: "required" }, {
  	                    type: "pattern",
  	                    message: '您所填写的含有非法字符,只能填写数字.',
  	                    pattern: /^[+]{0,1}(\d+)$/
  	                }],
  	                caption: "2月"
  	            },
  	            {
  	                dataField: "month3",
  	                validationRules: [{ type: "required" }, {
  	                    type: "pattern",
  	                    message: '您所填写的含有非法字符,只能填写数字.',
  	                    pattern: /^[+]{0,1}(\d+)$/
  	                }],
  	                caption: "3月"
  	            },
  	            {
  	                dataField: "month4",
  	                validationRules: [{ type: "required" }, {
  	                    type: "pattern",
  	                    message: '您所填写的含有非法字符,只能填写数字.',
  	                    pattern: /^[+]{0,1}(\d+)$/
  	                }],
  	                caption: "4月"
  	            },
  	            {
  	                dataField: "month5",
  	                validationRules: [{ type: "required" }, {
  	                    type: "pattern",
  	                    message: '您所填写的含有非法字符,只能填写数字.',
  	                    pattern: /^[+]{0,1}(\d+)$/
  	                }],
  	                caption: "5月"
  	            },
  	            {
  	                dataField: "month6",
  	                validationRules: [{ type: "required" }, {
  	                    type: "pattern",
  	                    message: '您所填写的含有非法字符,只能填写数字.',
  	                    pattern: /^[+]{0,1}(\d+)$/
  	                }],
  	                caption: "6月"
  	            },
  	            {
  	                dataField: "month7",
  	                validationRules: [{ type: "required" }, {
  	                    type: "pattern",
  	                    message: '您所填写的含有非法字符,只能填写数字.',
  	                    pattern: /^[+]{0,1}(\d+)$/
  	                }],
  	                caption: "7月"
  	            },
  	            {
  	                dataField: "month8",
  	                validationRules: [{ type: "required" }, {
  	                    type: "pattern",
  	                    message: '您所填写的含有非法字符,只能填写数字.',
  	                    pattern: /^[+]{0,1}(\d+)$/
  	                }],
  	                caption: "8月"
  	            },
  	            {
  	                dataField: "month9",
  	                validationRules: [{ type: "required" }, {
  	                    type: "pattern",
  	                    message: '您所填写的含有非法字符,只能填写数字.',
  	                    pattern: /^[+]{0,1}(\d+)$/
  	                }],
  	                caption: "9月"
  	            },
  	            {
  	                dataField: "month10",
  	                validationRules: [{ type: "required" }, {
  	                    type: "pattern",
  	                    message: '您所填写的含有非法字符,只能填写数字.',
  	                    pattern: /^[+]{0,1}(\d+)$/
  	                }],
  	                caption: "10月"
  	            },{
  	                dataField: "month11",
  	                validationRules: [{ type: "required" }, {
  	                    type: "pattern",
  	                    message: '您所填写的含有非法字符,只能填写数字.',
  	                    pattern: /^[+]{0,1}(\d+)$/
  	                }],
  	                caption: "11月"
  	            },{
  	                dataField: "month12",
  	                validationRules: [{ type: "required" }, {
  	                    type: "pattern",
  	                    message: '您所填写的含有非法字符,只能填写数字.',
  	                    pattern: /^[+]{0,1}(\d+)$/
  	                }],
  	                caption: "12月"
  	            }
  	        ],
  	        onEditingStart: function(e) {
  	        	var m = e.column.dataField;
  	        	var d = e.key.day;
  	        	if (   (d == '31' && (m == 'month2' || m == 'month4' || m == 'month6' || m == 'month9' || m == 'month11'))
  	        		|| (d == '30' && m == 'month2')
  	        		|| (d == '29' && m == 'month2' && !test_date(cur_year, 2, 29))
  	        	) {
  	        		e.cancel = true;
  	        		return;
  	        	}
  	            logEvent("您正在编辑工厂为:"+e.data.factory+",能源为"+e.data.energy+"的这一条记录。");
  	        },
  	        onInitNewRow: function(e) {
  	            logEvent("InitNewRow");
  	        },
  	        onRowInserting: function(e) {
  	            logEvent("RowInserting");
  	        },
  	        onRowInserted: function(e) {
  	            logEvent("RowInserted");
  	        },
  	        onRowUpdating: function(e) {
  	        	var uptRecords = e.newData;
  	        	var key = e.key;
  	        	var d = "day"
  	        	var e = "energy";
  	        	var str = "您修改了";
  	        	var data = new Object();
  	        	data.energy = eval("key."+e);
  	        	data.day = eval("key."+d);
  	        	for(var k in uptRecords){
  	        		str += k.substring(5,k.length)+"月、"
  	        		data[k] = uptRecords[k];
  	        	}
  	        	str += str.substring(0,str.length-1)+"的值。"
  	        	 $.ajax({
  	        	        url: path + '/quota/replace_quota_daily.do?year='+year,
  	        	        type: 'get',
  	        	        data:data,
  	        	        async: false,
  	        	        success: function (data) {
  	        	        	if(data == "1"){
  	        	        		logEvent(str);
  	        	        		logEvent("您已修改成功.")
  	        	        	}
  	        	        }
  	        	    });
  	        	
  	        },
  	        onRowRemoving: function(e) {
  	            logEvent("RowRemoving");
  	        },
  	        onRowRemoved: function(e) {
  	            logEvent("RowRemoved");
  	        }
  	    });
    	 $("#cw_grid").dxDataGrid({
   	        dataSource: cw_quotas_daily,
   	        showBorders: true,
   	        showColumnLines: true,
   	        allowColumnReordering: true,
   	        allowColumnResizing: true,
   	        showRowLines: true,
   	        rowAlternationEnabled: true,
   	        paging: {
   	            enabled: false
   	        },
   	        columnFixing: { 
   	            enabled: true
   	        },
   	        searchPanel: {
   	            visible: true,
   	            width: 240,
   	            placeholder: "搜索..."
   	        },
   	        editing: {
   	            mode: "batch",
   	            allowUpdating: true
   	        }, 
   	        selection: {
   	            mode: "multiple"
   	        },
   	        "export": {
   	            enabled: true,
   	            fileName: "定额设置",
   	            allowExportSelectedData: true
   	        },
   	        columnChooser: {
   	            enabled: true,
   	            mode: "select"
   	        },
   	        columns: [
   	            {
   	                dataField: "energy",
   	                allowEditing:false,
   	                fixed: true,
   	                cssClass: "fac_enr",
   	                caption: "能源"
   	            },
   	           {
   	                dataField: "day",
   	                allowEditing:false,
   	                fixed: true,
   	                cssClass: "fac_enr",
   	                caption: "日期"
   	            },
   	            {
   	                dataField: "month1",
   	                validationRules: [{ type: "required" }, {
   	                    type: "pattern",
   	                    message: '您所填写的含有非法字符,只能填写数字.',
   	                    pattern: /^[+]{0,1}(\d+)$/
   	                }],
   	                caption: "1月"
   	            },
   	            {
   	                dataField: "month2",
   	                validationRules: [{ type: "required" }, {
   	                    type: "pattern",
   	                    message: '您所填写的含有非法字符,只能填写数字.',
   	                    pattern: /^[+]{0,1}(\d+)$/
   	                }],
   	                caption: "2月"
   	            },
   	            {
   	                dataField: "month3",
   	                validationRules: [{ type: "required" }, {
   	                    type: "pattern",
   	                    message: '您所填写的含有非法字符,只能填写数字.',
   	                    pattern: /^[+]{0,1}(\d+)$/
   	                }],
   	                caption: "3月"
   	            },
   	            {
   	                dataField: "month4",
   	                validationRules: [{ type: "required" }, {
   	                    type: "pattern",
   	                    message: '您所填写的含有非法字符,只能填写数字.',
   	                    pattern: /^[+]{0,1}(\d+)$/
   	                }],
   	                caption: "4月"
   	            },
   	            {
   	                dataField: "month5",
   	                validationRules: [{ type: "required" }, {
   	                    type: "pattern",
   	                    message: '您所填写的含有非法字符,只能填写数字.',
   	                    pattern: /^[+]{0,1}(\d+)$/
   	                }],
   	                caption: "5月"
   	            },
   	            {
   	                dataField: "month6",
   	                validationRules: [{ type: "required" }, {
   	                    type: "pattern",
   	                    message: '您所填写的含有非法字符,只能填写数字.',
   	                    pattern: /^[+]{0,1}(\d+)$/
   	                }],
   	                caption: "6月"
   	            },
   	            {
   	                dataField: "month7",
   	                validationRules: [{ type: "required" }, {
   	                    type: "pattern",
   	                    message: '您所填写的含有非法字符,只能填写数字.',
   	                    pattern: /^[+]{0,1}(\d+)$/
   	                }],
   	                caption: "7月"
   	            },
   	            {
   	                dataField: "month8",
   	                validationRules: [{ type: "required" }, {
   	                    type: "pattern",
   	                    message: '您所填写的含有非法字符,只能填写数字.',
   	                    pattern: /^[+]{0,1}(\d+)$/
   	                }],
   	                caption: "8月"
   	            },
   	            {
   	                dataField: "month9",
   	                validationRules: [{ type: "required" }, {
   	                    type: "pattern",
   	                    message: '您所填写的含有非法字符,只能填写数字.',
   	                    pattern: /^[+]{0,1}(\d+)$/
   	                }],
   	                caption: "9月"
   	            },
   	            {
   	                dataField: "month10",
   	                validationRules: [{ type: "required" }, {
   	                    type: "pattern",
   	                    message: '您所填写的含有非法字符,只能填写数字.',
   	                    pattern: /^[+]{0,1}(\d+)$/
   	                }],
   	                caption: "10月"
   	            },{
   	                dataField: "month11",
   	                validationRules: [{ type: "required" }, {
   	                    type: "pattern",
   	                    message: '您所填写的含有非法字符,只能填写数字.',
   	                    pattern: /^[+]{0,1}(\d+)$/
   	                }],
   	                caption: "11月"
   	            },{
   	                dataField: "month12",
   	                validationRules: [{ type: "required" }, {
   	                    type: "pattern",
   	                    message: '您所填写的含有非法字符,只能填写数字.',
   	                    pattern: /^[+]{0,1}(\d+)$/
   	                }],
   	                caption: "12月"
   	            }
   	        ],
   	        onEditingStart: function(e) {
  	        	var m = e.column.dataField;
  	        	var d = e.key.day;
  	        	if (   (d == '31' && (m == 'month2' || m == 'month4' || m == 'month6' || m == 'month9' || m == 'month11'))
  	        		|| (d == '30' && m == 'month2')
  	        		|| (d == '29' && m == 'month2' && !test_date(cur_year, 2, 29))
  	        	) {
  	        		e.cancel = true;
  	        		return;
  	        	}
   	            logEvent("您正在编辑工厂为:"+e.data.factory+",能源为"+e.data.energy+"的这一条记录。");
   	        },
   	        onInitNewRow: function(e) {
   	            logEvent("InitNewRow");
   	        },
   	        onRowInserting: function(e) {
   	            logEvent("RowInserting");
   	        },
   	        onRowInserted: function(e) {
   	            logEvent("RowInserted");
   	        },
   	        onRowUpdating: function(e) {
   	        	var uptRecords = e.newData;
   	        	var key = e.key;
   	        	var d = "day"
   	        	var e = "energy";
   	        	var str = "您修改了";
   	        	var data = new Object();
   	        	data.energy = eval("key."+e);
   	        	data.day = eval("key."+d);
   	        	for(var k in uptRecords){
   	        		str += k.substring(5,k.length)+"月、"
   	        		data[k] = uptRecords[k];
   	        	}
   	        	str += str.substring(0,str.length-1)+"的值。"
   	        	 $.ajax({
   	        	        url: path + '/quota/replace_quota_daily.do?year='+year,
   	        	        type: 'get',
   	        	        data:data,
   	        	        async: false,
   	        	        success: function (data) {
   	        	        	if(data == "1"){
   	        	        		logEvent(str);
   	        	        		logEvent("您已修改成功.")
   	        	        	}
   	        	        }
   	        	    });
   	        	
   	        },
   	        onRowRemoving: function(e) {
   	            logEvent("RowRemoving");
   	        },
   	        onRowRemoved: function(e) {
   	            logEvent("RowRemoved");
   	        }
   	    });
    	 $("#gas_grid").dxDataGrid({
   	        dataSource: gas_quotas_daily,
   	        showBorders: true,
   	        showColumnLines: true,
   	        allowColumnReordering: true,
   	        allowColumnResizing: true,
   	        showRowLines: true,
   	        rowAlternationEnabled: true,
   	        paging: {
   	            enabled: false
   	        },
   	        columnFixing: { 
   	            enabled: true
   	        },
   	        searchPanel: {
   	            visible: true,
   	            width: 240,
   	            placeholder: "搜索..."
   	        },
   	        editing: {
   	            mode: "batch",
   	            allowUpdating: true
   	        }, 
   	        selection: {
   	            mode: "multiple"
   	        },
   	        "export": {
   	            enabled: true,
   	            fileName: "定额设置",
   	            allowExportSelectedData: true
   	        },
   	        columnChooser: {
   	            enabled: true,
   	            mode: "select"
   	        },
   	        columns: [
   	            {
   	                dataField: "energy",
   	                allowEditing:false,
   	                fixed: true,
   	                cssClass: "fac_enr",
   	                caption: "能源"
   	            },
   	           {
   	                dataField: "day",
   	                allowEditing:false,
   	                fixed: true,
   	                cssClass: "fac_enr",
   	                caption: "日期"
   	            },
   	            {
   	                dataField: "month1",
   	                validationRules: [{ type: "required" }, {
   	                    type: "pattern",
   	                    message: '您所填写的含有非法字符,只能填写数字.',
   	                    pattern: /^[+]{0,1}(\d+)$/
   	                }],
   	                caption: "1月"
   	            },
   	            {
   	                dataField: "month2",
   	                validationRules: [{ type: "required" }, {
   	                    type: "pattern",
   	                    message: '您所填写的含有非法字符,只能填写数字.',
   	                    pattern: /^[+]{0,1}(\d+)$/
   	                }],
   	                caption: "2月"
   	            },
   	            {
   	                dataField: "month3",
   	                validationRules: [{ type: "required" }, {
   	                    type: "pattern",
   	                    message: '您所填写的含有非法字符,只能填写数字.',
   	                    pattern: /^[+]{0,1}(\d+)$/
   	                }],
   	                caption: "3月"
   	            },
   	            {
   	                dataField: "month4",
   	                validationRules: [{ type: "required" }, {
   	                    type: "pattern",
   	                    message: '您所填写的含有非法字符,只能填写数字.',
   	                    pattern: /^[+]{0,1}(\d+)$/
   	                }],
   	                caption: "4月"
   	            },
   	            {
   	                dataField: "month5",
   	                validationRules: [{ type: "required" }, {
   	                    type: "pattern",
   	                    message: '您所填写的含有非法字符,只能填写数字.',
   	                    pattern: /^[+]{0,1}(\d+)$/
   	                }],
   	                caption: "5月"
   	            },
   	            {
   	                dataField: "month6",
   	                validationRules: [{ type: "required" }, {
   	                    type: "pattern",
   	                    message: '您所填写的含有非法字符,只能填写数字.',
   	                    pattern: /^[+]{0,1}(\d+)$/
   	                }],
   	                caption: "6月"
   	            },
   	            {
   	                dataField: "month7",
   	                validationRules: [{ type: "required" }, {
   	                    type: "pattern",
   	                    message: '您所填写的含有非法字符,只能填写数字.',
   	                    pattern: /^[+]{0,1}(\d+)$/
   	                }],
   	                caption: "7月"
   	            },
   	            {
   	                dataField: "month8",
   	                validationRules: [{ type: "required" }, {
   	                    type: "pattern",
   	                    message: '您所填写的含有非法字符,只能填写数字.',
   	                    pattern: /^[+]{0,1}(\d+)$/
   	                }],
   	                caption: "8月"
   	            },
   	            {
   	                dataField: "month9",
   	                validationRules: [{ type: "required" }, {
   	                    type: "pattern",
   	                    message: '您所填写的含有非法字符,只能填写数字.',
   	                    pattern: /^[+]{0,1}(\d+)$/
   	                }],
   	                caption: "9月"
   	            },
   	            {
   	                dataField: "month10",
   	                validationRules: [{ type: "required" }, {
   	                    type: "pattern",
   	                    message: '您所填写的含有非法字符,只能填写数字.',
   	                    pattern: /^[+]{0,1}(\d+)$/
   	                }],
   	                caption: "10月"
   	            },{
   	                dataField: "month11",
   	                validationRules: [{ type: "required" }, {
   	                    type: "pattern",
   	                    message: '您所填写的含有非法字符,只能填写数字.',
   	                    pattern: /^[+]{0,1}(\d+)$/
   	                }],
   	                caption: "11月"
   	            },{
   	                dataField: "month12",
   	                validationRules: [{ type: "required" }, {
   	                    type: "pattern",
   	                    message: '您所填写的含有非法字符,只能填写数字.',
   	                    pattern: /^[+]{0,1}(\d+)$/
   	                }],
   	                caption: "12月"
   	            }
   	        ],
   	        onEditingStart: function(e) {
  	        	var m = e.column.dataField;
  	        	var d = e.key.day;
  	        	if (   (d == '31' && (m == 'month2' || m == 'month4' || m == 'month6' || m == 'month9' || m == 'month11'))
  	        		|| (d == '30' && m == 'month2')
  	        		|| (d == '29' && m == 'month2' && !test_date(cur_year, 2, 29))
  	        	) {
  	        		e.cancel = true;
  	        		return;
  	        	}
   	            logEvent("您正在编辑工厂为:"+e.data.factory+",能源为"+e.data.energy+"的这一条记录。");
   	        },
   	        onInitNewRow: function(e) {
   	            logEvent("InitNewRow");
   	        },
   	        onRowInserting: function(e) {
   	            logEvent("RowInserting");
   	        },
   	        onRowInserted: function(e) {
   	            logEvent("RowInserted");
   	        },
   	        onRowUpdating: function(e) {
   	        	var uptRecords = e.newData;
   	        	var key = e.key;
   	        	var d = "day"
   	        	var e = "energy";
   	        	var str = "您修改了";
   	        	var data = new Object();
   	        	data.energy = eval("key."+e);
   	        	data.day = eval("key."+d);
   	        	for(var k in uptRecords){
   	        		str += k.substring(5,k.length)+"月、"
   	        		data[k] = uptRecords[k];
   	        	}
   	        	str += str.substring(0,str.length-1)+"的值。"
   	        	 $.ajax({
   	        	        url: path + '/quota/replace_quota_daily.do?year='+year,
   	        	        type: 'get',
   	        	        data:data,
   	        	        async: false,
   	        	        success: function (data) {
   	        	        	if(data == "1"){
   	        	        		logEvent(str);
   	        	        		logEvent("您已修改成功.")
   	        	        	}
   	        	        }
   	        	    });
   	        	
   	        },
   	        onRowRemoving: function(e) {
   	            logEvent("RowRemoving");
   	        },
   	        onRowRemoved: function(e) {
   	            logEvent("RowRemoved");
   	        }
   	    });
    	 ();
    }*/
});

function quota_grids(){
	var factory_data="";
	var energy_data="";
	var ym_data="";
	var val_data="";
	for (var i = 0; i < quota_sava_data.length; i++) {
		factory_data=quota_sava_data[i].factory+"--"+factory_data;
		energy_data=quota_sava_data[i].energy+"--"+energy_data;
		ym_data=quota_sava_data[i].ymd+"--"+ym_data;
		val_data=quota_sava_data[i].val+"--"+val_data;
	}
	$.ajax({
		url: path + '/quota/replace_quota_totals.do',
		type: 'get',
		data:{
			factory:factory_data,energy:energy_data,ym:ym_data,val:val_data   //ym是傳給後台的，屬性和後臺方法參數名相同,
		},
		async: false,
		success: function (data) {
			if(data == "1"){
				alert("保存成功！");
			}
		}
	});
}

function power_quotas_daily_sava_data(){
	var energy_data="";
	var ymd_data="";
	var val_data="";
	for (var i = 0; i < power_quotas_daily_sava.length; i++) {
		energy_data=power_quotas_daily_sava[i].energy+"--"+energy_data;
		ymd_data=power_quotas_daily_sava[i].ym+"--"+ymd_data;
		val_data=power_quotas_daily_sava[i].val+"--"+val_data;
	}
	
	$.ajax({
		url: path + '/quota/replace_quota_dailys.do',
		type: 'get',
		 //var power_quotas_daily_data={energy:energy,ymd:ym,val:val};
		data:{
			energy:energy_data,ymd:ymd_data,val:val_data
		},
		async: false,
		success: function (data) {
			if(data == "1"){
				alert("保存成功！");
			}
		}
	});
}

function cda_quotas_daily_sava_data(){
	var energy_data="";
	var ymd_data="";
	var val_data="";
	for (var i = 0; i < cda_quotas_daily_sava.length; i++) {
		energy_data=cda_quotas_daily_sava[i].energy+"--"+energy_data;
		ymd_data=cda_quotas_daily_sava[i].ymd+"--"+ymd_data;
		val_data=cda_quotas_daily_sava[i].val+"--"+val_data;
	}

	$.ajax({
		url: path + '/quota/replace_quota_dailys.do',
		type: 'get',
		data:{
			energy:energy_data,ymd:ymd_data,val:val_data
		},
		async: false,
		success: function (data) {
			if(data == "1"){
				alert("保存成功！");
			}
		}
	});
}

function upw_quotas_daily_sava_data(){
	var energy_data="";
	var ymd_data="";
	var val_data="";
	for (var i = 0; i < upw_quotas_daily_sava.length; i++) {
		energy_data=upw_quotas_daily_sava[i].energy+"--"+energy_data;
		ymd_data=upw_quotas_daily_sava[i].ymd+"--"+ymd_data;
		val_data=upw_quotas_daily_sava[i].val+"--"+val_data;
	}
	$.ajax({
		url: path + '/quota/replace_quota_dailys.do',
		type: 'get',
		data:{
			energy:energy_data,ymd:ymd_data,val:val_data
		},
		async: false,
		success: function (data) {
			if(data == "1"){
				alert("保存成功！");
			}
		}
	});
}

function pn2_quotas_daily_sava_data(){
	var energy_data="";
	var ymd_data="";
	var val_data="";
	for (var i = 0; i < pn2_quotas_daily_sava.length; i++) {
		energy_data=pn2_quotas_daily_sava[i].energy+"--"+energy_data;
		ymd_data=pn2_quotas_daily_sava[i].ymd+"--"+ymd_data;
		val_data=pn2_quotas_daily_sava[i].val+"--"+val_data;
	}
	$.ajax({
		url: path + '/quota/replace_quota_dailys.do',
		type: 'get',
		data:{
			energy:energy_data,ymd:ymd_data,val:val_data
		},
		async: false,
		success: function (data) {
			if(data == "1"){
				alert("保存成功！");
			}
		}
	});
}

function cw_quotas_daily_sava_data(){
	var energy_data="";
	var ymd_data="";
	var val_data="";
	for (var i = 0; i < cw_quotas_daily_sava.length; i++) {
		energy_data=cw_quotas_daily_sava[i].energy+"--"+energy_data;
		ymd_data=cw_quotas_daily_sava[i].ymd+"--"+ymd_data;
		val_data=cw_quotas_daily_sava[i].val+"--"+val_data;
	}
	$.ajax({
		url: path + '/quota/replace_quota_dailys.do',
		type: 'get',
		data:{
			energy:energy_data,ymd:ymd_data,val:val_data
		},
		async: false,
		success: function (data) {
			if(data == "1"){
				alert("保存成功！");
			}
		}
	});
}
function gas_quotas_daily_sava_data(){
	var energy_data="";
	var ymd_data="";
	var val_data="";
	for (var i = 0; i < gas_quotas_daily_sava.length; i++) {
		energy_data=gas_quotas_daily_sava[i].energy+"--"+energy_data;
		ymd_data=gas_quotas_daily_sava[i].ymd+"--"+ymd_data;
		val_data=gas_quotas_daily_sava[i].val+"--"+val_data;
	}
	$.ajax({
		url: path + '/quota/replace_quota_dailys.do',
		type: 'get',
		data:{
			energy:energy_data,ymd:ymd_data,val:val_data
		},
		async: false,
		success: function (data) {
			if(data == "1"){
				alert("保存成功！");
			}
		}
	});
}

function getFormatDate(dates,delta_day) {
	var date= new Date(dates);
    date.setDate(date.getDate()+delta_day); 
    var seperator1 = "-";
    var month = date.getMonth() + 1;
    var strDate = date.getDate();
    if  (month >= 1 && month <= 9) {
        month = "0" + month;
    }
    if  (strDate >= 0 && strDate <= 9) {
        strDate = "0" + strDate;
    }
    var currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate;
    return currentdate;
}


