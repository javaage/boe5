var factory_array = [str_array, str_cf, str_cell, "MD"];
var energy_array = ['Power', str_upw, str_cda];//, str_pn2];
var param_name_array = ['产量', '稼动率', '单耗'];
var param_id_array = ['product', 'state', 'unit_cons'];
var param_chart_type_array = ['bar', 'line', 'line'];
var data_cons;
var data_unit_cons;
var data_base_average;
var data_base_unit_cons;
var upw_product=new Array();
var cda_product=new Array();


var upw_product_average ;
var cda_product_average ;


var chart_map = {
	ARRAY :{
		Power: array_power_chart,
		UPW:   array_upw_chart,
		CDA:   array_cda_chart,
		//PN2:   array_pn2_chart,
	},
	CF :{
		Power: cf_power_chart,
		UPW:   cf_upw_chart,
		CDA:   cf_cda_chart,
		//PN2:   cf_pn2_chart,
	},
	CELL :{
		Power: cell_power_chart,
		UPW:   cell_upw_chart,
		CDA:   cell_cda_chart,
		//PN2:   cell_pn2_chart,
	},
	MD :{
		Power: mdl_power_chart,
		UPW:   mdl_upw_chart,
		CDA:   mdl_cda_chart,
		//PN2:   mdl_pn2_chart,
	},
	CUB :{
		Power: cub_power_chart,
		UPW:   cub_upw_chart,
		CDA:   cub_cda_chart,
		//PN2:   cub_pn2_chart,
	},
};

var chart_option_map = {
	ARRAY :{
		Power: array_power_chart_option,
		UPW:   array_upw_chart_option,
		CDA:   array_cda_chart_option,
		//PN2:   array_pn2_chart_option,
	},
	CF :{
		Power: cf_power_chart_option,
		UPW:   cf_upw_chart_option,
		CDA:   cf_cda_chart_option,
		//PN2:   cf_pn2_chart_option,
	},
	CELL :{
		Power: cell_power_chart_option,
		UPW:   cell_upw_chart_option,
		CDA:   cell_cda_chart_option,
		//PN2:   cell_pn2_chart_option,
	},
	MD :{
		Power: mdl_power_chart_option,
		UPW:   mdl_upw_chart_option,
		CDA:   mdl_cda_chart_option,
		//PN2:   mdl_pn2_chart_option,
	},
	CUB :{
		Power: cub_power_chart_option,
		UPW:   cub_upw_chart_option,
		CDA:   cub_cda_chart_option,
		//PN2:   cub_pn2_chart_option,
	},
};

var all_chart = new Array(
	array_power_chart,
	array_upw_chart,
	array_cda_chart,
	//array_pn2_chart,

	cf_power_chart,
	cf_upw_chart,
	cf_cda_chart,
	//cf_pn2_chart,

	cell_power_chart,
	cell_upw_chart,
	cell_cda_chart,
	//cell_pn2_chart,

	mdl_power_chart,
	mdl_upw_chart,
	mdl_cda_chart,
	//mdl_pn2_chart
	
	cub_power_chart,
	cub_upw_chart,
	cub_cda_chart
	//cub_pn2_chart
);

var all_chart_option = new Array(
	array_power_chart_option,
	array_upw_chart_option,
	array_cda_chart_option,
	//array_pn2_chart_option,

	cf_power_chart_option,
	cf_upw_chart_option,
	cf_cda_chart_option,
	//cf_pn2_chart_option,

	cell_power_chart_option,
	cell_upw_chart_option,
	cell_cda_chart_option,
	//cell_pn2_chart_option,

	mdl_power_chart_option,
	mdl_upw_chart_option,
	mdl_cda_chart_option,
	//mdl_pn2_chart_option,

	cub_power_chart_option,
	cub_upw_chart_option,
	cub_cda_chart_option
	//cub_pn2_chart_option
);

var echart_tooltip =function (params) {
	var str="";
	for (var i=0;i<params.length;i++) {
		if (params[i].seriesName=="稼动率") {
			if (isNaN(params[i].value)) {
				str=str+'<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:'+params[i].color+';"></span>'
				str= '<span style="color:#000000">'+str+ params[i].seriesName+' : %'+params[i].value+'</span>'+'<br>';
			} else {
				str=str+'<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:'+params[i].color+';"></span>'
	    		str= '<span style="color:#000000">'+str+ params[i].seriesName+' : %'+(params[i].value*100).toFixed(2)+'</span>'+'<br>';
			}
		} else {
			if (isNaN(params[i].value)) {
				str=str+'<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:'+params[i].color+';"></span>'
				str= '<span style="color:#000000">'+str+ params[i].seriesName+' : '+params[i].value+'</span>'+'<br>';
			} else {
				str=str+'<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:'+params[i].color+';"></span>'
	    		str= '<span style="color:#000000">'+str+ params[i].seriesName+' : '+params[i].value.toFixed(4)+'</span>'+'<br>';
			}
		}
		
	}
    return str;
};

let chart_name_array = new Array(
	'array_power_chart',
	'array_upw_chart',
	'array_cda_chart',
	//'array_pn2_chart',

	'cf_power_chart',
	'cf_upw_chart',
	'cf_cda_chart',
	//'cf_pn2_chart',

	'cell_power_chart',
	'cell_upw_chart',
	'cell_cda_chart',
	//'cell_pn2_chart',

	'mdl_power_chart',
	'mdl_upw_chart',
	'mdl_cda_chart',
	//'mdl_pn2_chart'

	'cub_power_chart',
	'cub_upw_chart',
	'cub_cda_chart'
	//'cub_pn2_chart'
);

var table_energy=["Power","UPW","CDA"];
var table_factory=["ARRAY","CF","CELL","MD"];
var table_factory_id=0;
var table_energy_id=0;

var echart_style=true;

var year_total_first_summary=new Array();
var month_total_quota=new Array();
var day_total_quota=new Array();
var year_plan_quota=new Array();
var month_plan_quota=new Array();
var day_plan_quota=new Array();
var year_finish_quota=new Array();
var month_finish_quota=new Array();
var day_finish_quota=new Array();

function table_show(){
	

	layui.use('table', function(){
		table = layui.table;
	
		var yesterday_cons;
		var yesterday_unit_cons;
		var before_yesterday_cons;
		var before_yesterday_unit_cons;
		var last_month_cons;
		var last_month_unit_cons;
		
		var before_yesterday_cons_trend;
		var before_yesterday_unit_cons_trend;
		var last_month_cons_trend;
		var last_month_unit_cons_trend;
		
		if(!ajax_json){
			
		
    	
	    	for(var i=0;i<data_cons.length;i++){
	    		if(data_cons[i].param==table_energy[table_energy_id]&&data_cons[i].factory==table_factory[table_factory_id]){
	    			yesterday_cons=data_cons[i].data[6].toFixed(2);
	    			before_yesterday_cons=data_cons[i].data[5].toFixed(2);
	    		}
	    	}
	    	
	    	for(var i=0;i<data_unit_cons.length;i++){
	    		if(data_unit_cons[i].param==(table_energy[table_energy_id]+'_unit_cons')&&data_unit_cons[i].factory==table_factory[table_factory_id]){
	    			yesterday_unit_cons=data_unit_cons[i].data[6].toFixed(2);
	    			before_yesterday_unit_cons=data_unit_cons[i].data[5].toFixed(2);
	    		}
	    	}
	    	
	    	for(var i=0;i<data_base_average.length;i++){
	    		if(data_base_average[i].param==table_energy[table_energy_id]&&data_base_average[i].factory==table_factory[table_factory_id]){
	    			last_month_cons=data_base_average[i].val.toFixed(2);
	    		}
	    	}
	    	for(var i=0;i<data_base_unit_cons.length;i++){
	    		if(data_base_unit_cons[i].param==table_energy[table_energy_id]&&data_base_unit_cons[i].factory==table_factory[table_factory_id]){
	    			last_month_unit_cons=data_base_unit_cons[i].val.toFixed(2);
	    		}
	    	}
	    	if(yesterday_cons>before_yesterday_cons) {
	    		before_yesterday_cons_trend="<img src='"+path+"/res/images/goup.png'>";
	    	}else {
	    		before_yesterday_cons_trend="<img src='"+path+"/res/images/godown.png'>";
	    	}
	    	if(yesterday_unit_cons>before_yesterday_unit_cons) {
	    		before_yesterday_unit_cons_trend="<img src='"+path+"/res/images/goup.png'>";
	    	}else {
	    		before_yesterday_unit_cons_trend="<img src='"+path+"/res/images/godown.png'>";
	    	}
	    	if(yesterday_cons>last_month_cons) {
	    		last_month_cons_trend="<img src='"+path+"/res/images/goup.png'>";
	    	}else {
	    		last_month_cons_trend="<img src='"+path+"/res/images/godown.png'>";
	    	}
	    	if(yesterday_unit_cons>last_month_unit_cons) {
	    		last_month_unit_cons_trend="<img src='"+path+"/res/images/goup.png'>";
	    	}else {
	    		last_month_unit_cons_trend="<img src='"+path+"/res/images/godown.png'>";
	    	}
	    	
	
			table.render({
				elem: '#test'
			,cellMinWidth: 30 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
			,cols: [[
				{field:'id', width:120,title: table_factory[table_factory_id]+" "+table_energy[table_energy_id],align: 'center'}
				,{field:'cons',title: '能耗',align: 'center'} //width 支持：数字、百分比和不填写。你还可以通过 minWidth 参数局部定义当前单元格的最小宽度，layui 2.2.1 新增
				,{field:'cons_trend',width:60,title: '趋势',align: 'center'}
				,{field:'cons_unit', title: '单耗',align: 'center'}
				,{field:'cons_unit_trend',width:60, title: '趋势',align: 'center'}
				  ]]
				,data: [{
					"id": "昨日"
				    ,"cons": yesterday_cons
				    ,"cons_trend":""
				    ,"cons_unit":yesterday_unit_cons
				    ,"cons_unit_trend":""
				},
				{
				"id": "前日"
				    ,"cons": before_yesterday_cons
				    ,"cons_trend":before_yesterday_cons_trend
				    ,"cons_unit": before_yesterday_unit_cons
				    ,"cons_unit_trend": before_yesterday_unit_cons_trend
				},
				{
				"id": "上月平均"
				    ,"cons": last_month_cons
				    ,"cons_trend":last_month_cons_trend
				    ,"cons_unit":last_month_unit_cons
				    ,"cons_unit_trend":last_month_unit_cons_trend
				},]
			});
		}else{
			table.render({
				elem: '#test'
			,cellMinWidth: 30 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
			,cols: [[
				{field:'id', width:120,title: table_factory[table_factory_id]+"/"+table_energy[table_energy_id],align: 'center'}
				,{field:'cons',title: '能耗',align: 'center'} //width 支持：数字、百分比和不填写。你还可以通过 minWidth 参数局部定义当前单元格的最小宽度，layui 2.2.1 新增
				,{field:'cons_trend',width:60,title: '趋势',align: 'center'}
				,{field:'cons_unit', title: '单耗',align: 'center'}
				,{field:'cons_unit_trend',width:60, title: '趋势',align: 'center'}
				  ]]
				,data: [{
					"id": "昨日"
				    ,"cons": 33
				    ,"cons_trend":""
				    ,"cons_unit":33
				    ,"cons_unit_trend":""
				},
				{
				"id": "前日"
				    ,"cons": 33
				    ,"cons_trend":"<img src='"+path+"/res/images/goup.png'>"
				    ,"cons_unit": 33
				    ,"cons_unit_trend": "<img src='"+path+"/res/images/goup.png'>"
				},
				{
				"id": "上月平均"
				    ,"cons": 33
				    ,"cons_trend":"<img src='"+path+"/res/images/goup.png'>"
				    ,"cons_unit":33
				    ,"cons_unit_trend":"<img src='"+path+"/res/images/goup.png'>"
				},]
			});
		}

	});
}

function last_message(){
	table_factory_id--;
	if(table_factory_id==-1){
		table_factory_id=3;
		table_energy_id--;
		if(table_energy_id==-1){
			table_energy_id=2;	
		}
	}
	table_show();
}
function next_message(){
	table_factory_id++;
	if(table_factory_id==4){
		table_factory_id=0
		table_energy_id++;
		if(table_energy_id==3){
			table_energy_id=0;	
		}
	}
	table_show();
}

function get_total_quota() {
	var begin_date = defaut_end_date.substring(0,4)+"-01-01"
	var end_date = defaut_end_date.substring(0,4)+"-12-01"
	if(!ajax_json){
		first_summary_ajax_url1=path + '/gynh/first_summary/get_year_total_quota.do';
	}
	$.ajax({
//        url: path + '/gynh/first_summary/get_year_total_quota.do',
		url: first_summary_ajax_url1,
        type: 'get',
        data:{
        	begin_date:begin_date,
        	end_date:end_date,
        	},
        async: false,
        success: function (data) {
        	year_total_quota = data['year_total_quota'];
        	
        } 
    });
	begin_date = defaut_end_date.substring(0,7)+"-01"
	if(!ajax_json){
		first_summary_ajax_url2=path + '/gynh/first_summary/get_month_total_quota.do';
	}
	$.ajax({
//        url: path + '/gynh/first_summary/get_month_total_quota.do',
		url:first_summary_ajax_url2,
        type: 'get',
        data:{begin_date:begin_date},
        async: false,
        success: function (data) {
        	month_total_quota = data['month_total_quota'];
        	day_total_quota=JSON.parse(JSON.stringify(month_total_quota));
        	for (var i=0;i<day_total_quota.length;i++) {
        		day_total_quota[i].val=(day_total_quota[i].val/GetDate(defaut_end_date.substring(0,4),defaut_end_date.substring(5,7))).toFixed(0);
        	}
        	month_plan_quota= JSON.parse(JSON.stringify(day_total_quota));
        	for (var i=0;i<month_plan_quota.length;i++) {
        		month_plan_quota[i].val=defaut_end_date.substring(8,10)*month_plan_quota[i].val;
        	}
        	day_plan_quota=JSON.parse(JSON.stringify(day_total_quota));
        }
    });
}

function get_plan_quota() {
	var begin_date = defaut_end_date.substring(0,4)+"-01-01"
	var end_date = defaut_end_date.substring(0,7)+"-01"
	if(!ajax_json){
		first_summary_ajax_url3=path + '/gynh/first_summary/get_year_total_quota.do';
	}
	$.ajax({
//        url: path + '/gynh/first_summary/get_year_total_quota.do',
		url: first_summary_ajax_url3,
        type: 'get',
        data:{
        	begin_date:begin_date,
        	end_date:end_date,
        	},
        async: false,
        success: function (data) {
        	
        	year_plan_quota = data['year_total_quota'];
        }
    });
}

function  get_finish_quota() {
	var begin_date = defaut_end_date.substring(0,4)+"-01-01";
	var end_date = defaut_end_date;
	if(!ajax_json){
		first_summary_ajax_url4=path + '/gynh/first_summary/get_year_finish_quota.do';
	}
	$.ajax({
//        url: path + '/gynh/first_summary/get_year_finish_quota.do',
		url: first_summary_ajax_url4,
        type: 'get',
        data:{
        	begin_date:begin_date,
        	end_date:end_date,
        	},
        async: false,
        success: function (data) {
        	year_finish_quota = data['year_finish_quota'];
        }
    });
	begin_date = defaut_end_date.substring(0,7)+"-01"
	end_date = defaut_end_date;
	if(!ajax_json){
		first_summary_ajax_url5= path + '/gynh/first_summary/get_year_finish_quota.do';
	}
	$.ajax({
//        url: path + '/gynh/first_summary/get_year_finish_quota.do',
		url: first_summary_ajax_url5,
        type: 'get',
        data:{
        	begin_date:begin_date,
        	end_date:end_date,
        	},
        async: false,
        success: function (data) {
        	month_finish_quota = data['year_finish_quota'];
        }
    });
	begin_date = defaut_end_date;
	if(!ajax_json){
		first_summary_ajax_url6=path + '/gynh/first_summary/get_day_finish_quota.do';
	}
	$.ajax({
//        url: path + '/gynh/first_summary/get_day_finish_quota.do',
		url: first_summary_ajax_url6,
        type: 'get',
        data:{begin_date:begin_date},
        async: false,
        success: function (data) {
        	day_finish_quota = data['day_finish_quota'];
        }
    });
}

function GetDate(year, month){
    var d = new Date(year, month, 0);
    return d.getDate();
}

function set_gauge_chart(){

	get_gauge_chart_option(power_chart,power_chart_option,'Power');
	get_gauge_chart_option(cw_chart,cw_chart_option,'CW');
	get_gauge_chart_option(pn2_chart,pn2_chart_option,'PN2');
	get_gauge_chart_option(gas_chart,gas_chart_option,'GAS');
	
}
function get_gauge_chart_option(chart,option,energy){
	
	option.series[0].max=get_data_val(energy,month_total_quota);
	option.series[1].max=get_data_val(energy,month_total_quota);
	var max=get_data_val(energy,month_total_quota);

	var l = max.toString().length;
	var splitnumber;
	if (Math.trunc(max/8/Math.pow(10,l-2))>=10){
		max=Math.trunc(max/8/Math.pow(10,l-2))*8*Math.pow(10,l-2);
		splitnumber=Math.trunc(max/8/Math.pow(10,l-2));
	}else if (Math.trunc(max/5/Math.pow(10,l-2))>=10){
		max=Math.trunc(max/5/Math.pow(10,l-2))*5*Math.pow(10,l-2);
		splitnumber=Math.trunc(max/5/Math.pow(10,l-2));
	}else if(Math.trunc(max/4/Math.pow(10,l-2))>=10){
		max=Math.trunc(max/4/Math.pow(10,l-2))*4*Math.pow(10,l-2);
		splitnumber=Math.trunc(max/4/Math.pow(10,l-2));
	}else if(Math.trunc(max/2/Math.pow(10,l-2))>=10){
		max=Math.trunc(max/2/Math.pow(10,l-2))*2*Math.pow(10,l-2);
		splitnumber=Math.trunc(max/2/Math.pow(10,l-2));
	}
	else {
		max=Math.trunc(max/1/Math.pow(10,l-2))*1*Math.pow(10,l-2);
		splitnumber=Math.trunc(max/1/Math.pow(10,l-2));
	}
	option.series[2].splitNumber=splitnumber;
	option.series[2].max=max;
	option.series[2].startAngle=225;
	option.series[2].endAngle=225-270*max/get_data_val(energy,month_total_quota);
	option.series[0].data[0].value=get_data_val(energy, month_plan_quota);
	option.series[1].data[0].value=get_data_val(energy,month_finish_quota);
	option.series[0].data[0].name=energy;
	option.series[1].data[0].name=energy;
	option.title.text="计划："+get_data_val(energy, month_plan_quota)+"\n"+"实际："+get_data_val(energy,month_finish_quota)+"\n"+
	"实际/计划："+(get_data_val(energy,month_finish_quota)/get_data_val(energy, month_plan_quota)*100).toFixed(0)+"%";
	
	var color=new Array();
	var str=new Array();
	
	var m=get_data_val(energy,month_plan_quota)-get_data_val(energy,month_finish_quota);
	if (m>0) {
		str.push(0);
		str.push('#0066FF');
		color.push(str);
		str=new Array();
		
		str.push(get_data_val(energy,month_finish_quota)/get_data_val(energy,month_total_quota));
		str.push('#0066FF');
		color.push(str);
		str=new Array();
		
		str.push(get_data_val(energy,month_plan_quota)/get_data_val(energy,month_total_quota));
		str.push('#00DD00');
		color.push(str);
		str=new Array();
		
		str.push(1);
		str.push('#0066FF');
		color.push(str);
		str=new Array();
	}else {
		str.push(0);
		str.push('#0066FF');
		color.push(str);
		str=new Array();
		
		str.push(get_data_val(energy,month_plan_quota)/get_data_val(energy,month_total_quota));
		str.push('#0066FF');
		color.push(str);
		str=new Array();
		
		str.push(get_data_val(energy,month_finish_quota)/get_data_val(energy,month_total_quota));
		str.push('#CC0000');
		color.push(str);
		str=new Array();
		
		str.push(1);
		str.push('#0066FF');
		color.push(str);
		str=new Array();
	}
	option.series[0].axisLine.lineStyle.color=color;
	option.series[1].axisLine.lineStyle.color=color;
	color=new Array();
	if (m>0) {
		str.push(0);
		str.push('#0066FF');
		color.push(str);
		str=new Array();
		
		str.push(get_data_val(energy,month_finish_quota)/max);
		str.push('#0066FF');
		color.push(str);
		str=new Array();
		
		str.push(get_data_val(energy,month_plan_quota)/max);
		str.push('#00DD00');
		color.push(str);
		str=new Array();
		
		str.push(1);
		str.push('#0066FF');
		color.push(str);
		str=new Array();
	}else {
		str.push(0);
		str.push('#0066FF');
		color.push(str);
		str=new Array();
		
		str.push(get_data_val(energy,month_plan_quota)/max);
		str.push('#0066FF');
		color.push(str);
		str=new Array();
		
		str.push(get_data_val(energy,month_finish_quota)/max);
		str.push('#CC0000');
		color.push(str);
		str=new Array();
		
		str.push(1);
		str.push('#0066FF');
		color.push(str);
		str=new Array();
	}
	option.series[2].axisLine.lineStyle.color=color;
	chart.setOption(option,true);
}

function get_data_val(energy,data) {
	var val=0;
	for (var i=0;i<data.length;i++) {
		if (data[i].energy==energy) {
			val =data[i].val;
			break;
		}
	}if(energy=="Power"){
		val=translate_unit(val, 'mWh','mWh').toFixed(0);
	}else {
		val=translate_unit(val,'k(m³)' ,'W(m³)').toFixed(0);
	}
	
	return val;	
}

function echarts_style(){
	if(echart_style){
		for(var i=0;i<all_chart_option.length;i++){
			all_chart_option[i].yAxis[0].min=null;
			all_chart_option[i].yAxis[0].max=null;
			all_chart_option[i].yAxis[1].min=null;
			all_chart_option[i].yAxis[1].max=null;
			all_chart[i].setOption(all_chart_option[i], true);
		}
		echart_style=false;
	}else{
		for(var i=0;i<all_chart_option.length;i++){
			all_chart_option[i].yAxis[0].min=yAxis_min;
			all_chart_option[i].yAxis[0].max=yAxis_max;
			all_chart_option[i].yAxis[1].min=yAxis_min;
			all_chart_option[i].yAxis[1].max=yAxis_max;
			all_chart[i].setOption(all_chart_option[i], true);
		}
		echart_style=true;
	}
}



function load_data_impl() {
	//test_summary_data;
	for (var i = 0; i< all_chart.length; i++) {
		//all_chart_option[i].tooltip.formatter=echart_tooltip;
		all_chart[i].setOption(all_chart_option[i], true);
	}

	var begin_date = '';//$('#beginTimeBox').val();
	var end_date  = '';//$('#endTimeBox').val();
	if (begin_date == "") begin_date = defaut_begin_date;
	if (end_date == "") end_date = defaut_end_date;

	var begin_year = begin_date.substr(0,4)*1;
	var begin_month = begin_date.substr(5,2)*1;
	var begin_day = begin_date.substr(8,2)*1;

	var d1 = new Date(begin_date.replace(/-/g,   "/"))
	var d2 = new Date(end_date.replace(/-/g,   "/"))
	var days = (d2.getTime()-d1.getTime()) / (1000*60*60*24) + 1;

	var xAxis = new Array();

	for (var i = 0; i < days; i++) {
	    var temp = new Date(begin_year, begin_month-1, begin_day);
	    xAxis[i] = getFormatDate2(temp, i);
	    xAxis[i] = xAxis[i].substring(8);
	}
	
	for (var i = 0; i< all_chart_option.length; i++) {
		all_chart_option[i].xAxis[0].data = xAxis;
		all_chart_option[i].xAxis[1].data = xAxis;
	}
	
	var base_unit_cons_map = {};
	for (var i = 0; i < data_base_unit_cons.length; i++) {
		var factory_name = data_base_unit_cons[i].factory;
		var energy_name = data_base_unit_cons[i].param;
		if (typeof(base_unit_cons_map[factory_name]) == "undefined") {
			base_unit_cons_map[factory_name] = {};
		}
		base_unit_cons_map[factory_name][energy_name] = data_base_unit_cons[i].val;
	}
	
	for (var i = 0; i < data_unit_cons.length; i++) {
		for (var j = 0; j < data_unit_cons[i].data.length; j++) {
			data_unit_cons[i].data[j]=data_unit_cons[i].data[j];
		}
	}
	for (var i = 0; i < data_unit_cons.length; i++) {
		var factory_name = data_unit_cons[i].factory;
		var energy_name = data_unit_cons[i].param.split('_')[0];
		
		chart_option_map[factory_name][energy_name].series.push(
			{
	            name: '单耗',
	            type: 'line',
	            yAxisIndex: 0,
	            xAxisIndex: 0,
	            data: data_unit_cons[i].data,
	            color: energy_colors[energy_name],
	            animation: true,
	            markLine: {
	                silent: true,
	                data: [{
	                    yAxis: (typeof(base_unit_cons_map[factory_name]) == "undefined" || typeof(base_unit_cons_map[factory_name][energy_name])  == "undefined") ? 0 : base_unit_cons_map[factory_name][energy_name],
	                }]
	            },

	        }
		);
	}

	var base_average_map = {};
	for (var i = 0; i < data_base_average.length; i++) {
		var factory_name = data_base_average[i].factory;
		var energy_name = data_base_average[i].param;
		if (typeof(base_average_map[factory_name]) == "undefined") {
			base_average_map[factory_name] = {};
		}
		base_average_map[factory_name][energy_name] = data_base_average[i].val;
	}
	for (var i = 0; i < data_cons.length; i++) {
		for (var j = 0; j < data_cons[i].data.length; j++) {
			data_cons[i].data[j]=data_cons[i].data[j];
		}
	}

	for (var i = 0; i < data_cons.length; i++) {
		var factory_name = data_cons[i].factory;
		var energy_name = data_cons[i].param;
		
		chart_option_map[factory_name][energy_name].series.push(
			{
	            name: '能耗',
	            type: 'line',
	            yAxisIndex: 1,
	            xAxisIndex: 1,
	            data: data_cons[i].data,
	            color: energy_colors[energy_name],
	            animation: true,
	            areaStyle: {},
	            markLine: {
	                silent: true,
	                data: [{
	                    yAxis: (typeof(base_average_map[factory_name]) == "undefined" || typeof(base_average_map[factory_name][energy_name])  == "undefined") ? 0 : base_average_map[factory_name][energy_name],
	                }]
	            },

	        }
		);
	}
	
	chart_option_map['CUB']['UPW'].series.push(
			{
	            name: '能耗',
	            type: 'line',
	            yAxisIndex: 1,
	            xAxisIndex: 1,
	            data: upw_product,
	            color: energy_colors['UPW'],
	            animation: true,
	            areaStyle: {},
	            markLine: {
	                silent: true,
	                data: [{
	                    yAxis:upw_product_average,
	                }]
	            },

	        }
		);
	chart_option_map['CUB']['CDA'].series.push(
			{
	            name: '能耗',
	            type: 'line',
	            yAxisIndex: 1,
	            xAxisIndex: 1,
	            data: cda_product,
	            color: energy_colors['CDA'],
	            animation: true,
	            areaStyle: {},
	            markLine: {
	                silent: true,
	                data: [{
	                    yAxis:cda_product_average,
	                }]
	            },

	        }
		);
	

//	chart_option_map['CUB']['UPW'].series[1].data=upw_product;
//	chart_option_map['CUB']['CDA'].series[1].data=upw_product;
	for (var i = 0; i< all_chart.length; i++) {
		//all_chart_option[i].tooltip.formatter=echart_tooltip;
		all_chart[i].setOption(all_chart_option[i], true);
	}
}

function page_custom_init() {
	$("#cub_sum_charts_div").show();
	$("#echarts_div").hide();
}

function page_custom_resize() {
	
	
	var windowWidth= $(window).width();
	var windowHeight=$(window).height();
	
//	if(windowWidth<1115) {
//		windowWidth=1115;
//	}
//	
//	if(windowHeight<725) {
//		windowHeight=725;
//	}
	
	$("#meter_div").width(windowHeight*0.4);
	
	let h = windowHeight -210;
	let w = windowWidth-15;

	for (var i = 0; i < chart_name_array.length; i++) {
		document.getElementById(chart_name_array[i]).style.width = 20 + 'px';
		document.getElementById(chart_name_array[i]).style.height = 20 + 'px';
	}

	let left = 0;
	let right = 43;
	let top = 40;
	let bottom = 8;
	var chart_w = (w-left-right)/5;
	var chart_h = (h-top-bottom)/3;
	
	$("#r0c6_td").width(right);
	$("#r0c1_td").width(chart_w);
	$("#r0c2_td").width(chart_w);
	$("#r0c3_td").width(chart_w);
	$("#r0c4_td").width(chart_w);
	$("#r0c5_td").width(chart_w);
	
	$("#r0_tr").height(top);
	$("#r5_tr").height(bottom);
	$("#r1c1_td").height(chart_h);
	$("#r2c1_td").height(chart_h);
	$("#r3c1_td").height(chart_h);
	$(".font_div").height(chart_h-10 + 'px');
	
	var font_div=document.getElementsByClassName('font_div');
	
	for (var i = 0; i < font_div.length; i++) {
		font_div[i].style.height = (windowHeight - windowHeight/900*200-80)/3+'px';
		font_div[i].style.fontSize = windowHeight/900*30+'px';
	}
	
	var border2 = document.getElementsByClassName('l_border2');
	var border1 = document.getElementsByClassName('l_border1');
	var border = document.getElementsByClassName('l_border');
	var banner = document.getElementsByClassName('banner');
	
	document.getElementById('chart_body').style.height= windowHeight - windowHeight/900*200+'px';
	document.getElementById('chart_body').style.width= (windowWidth-56)+ 8 - windowHeight/900*5+'px';
	
	for (var i = 0; i < border.length; i++) {
		
		border[i].style.height = (windowHeight - windowHeight/900*200-80)/3+'px';
		border1[i].style.height = (windowHeight - windowHeight/900*200-80)/3+'px';
		border2[i].style.height = (windowHeight - windowHeight/900*200-80)/3+'px';
		//banner[i].style.height = (windowHeight - windowHeight/900*200)/3+'px';
		
		border[i].style.width= (windowWidth-118)/5 + 5 - windowHeight/900*5+'px';
		border1[i].style.width=(windowWidth-118)/5 + 5 - windowHeight/900*5+'px';
		border2[i].style.width=(windowWidth-118)/5 + 5 - windowHeight/900*5+'px';
		banner[i].style.width=(windowWidth-118)/5 + 5 - windowHeight/900*5+'px';
	}
	
	document.getElementById('power_chart').style.width = (windowWidth-70)/1600*300+'px';
	document.getElementById('cw_chart').style.width = (windowWidth-70)/1600*300+'px';
	document.getElementById('pn2_chart').style.width = (windowWidth-70)/1600*300+'px';
	document.getElementById('gas_chart').style.width = (windowWidth-70)/1600*300+'px';
	document.getElementById('message_div').style.width = (windowWidth-70)/1600*400+'px';
	
	document.getElementById('power_chart').style.height = windowHeight/900*200+'px';
	document.getElementById('cw_chart').style.height = windowHeight/900*200+'px';
	document.getElementById('pn2_chart').style.height = windowHeight/900*200+'px';
	document.getElementById('gas_chart').style.height = windowHeight/900*200+'px';
	document.getElementById('message_div').style.height = windowHeight/900*100+'px';
	
	document.getElementById('message_div').style.marginTop = (windowHeight-700)/200*20+'px';
	
	var font_size;
	if(windowHeight/windowWidth>900/1670) {
		font_size=windowWidth/1670;
	}
	else {
		font_size=windowHeight/900;
	}
	
	
	power_chart.resize();
	cw_chart.resize();
	pn2_chart.resize();
	gas_chart.resize();

	for (var i = 0; i < chart_name_array.length; i++) {
		document.getElementById(chart_name_array[i]).style.width = chart_w-4 + 'px';
		document.getElementById(chart_name_array[i]).style.height = chart_h-10 + 'px';
		all_chart[i].resize();
	}
}

window.onload = function() {
	get_total_quota();
	get_plan_quota();
	get_finish_quota();
	set_gauge_chart();
	//load_default_shortcut();
	//set_units_on_controles();
	get_daily_product_data(defaut_begin_date, defaut_end_date);
	load_data(defaut_begin_date, defaut_end_date);
	table_show();
	page_custom_resize();
}

window.onresize = function() {
	page_custom_resize();
}

window.onunload = function(event) {
	SaveSnapshot(pageName, '', 'icon_path_1', true);
}

function SaveSnapshot(page, name, icon, when_close) {
	var cfg = {
			currentPowerUnit:currentPowerUnit,
			currentCDAUnit:currentCDAUnit,
			currentUPWUnit:currentUPWUnit,
			currentPN2Unit:currentPN2Unit,
			currentTimeUnit:currentTimeUnit,
			currentRCUnit:currentRCUnit,
		};
	var strJson = JSON.stringify(cfg, function(key, val) {
		if (typeof val === 'function') {
			return val + '';
		}
		return val;
	});
	
    $.ajax({
        url: when_close ? (path + "/gynh/biz/save_default_shortcut.do") : (path + "/gynh/biz/save_shortcut.do"),
        type: 'post',
        async: false,
        data: {
        	page:page, 
        	name:name, 
        	icon:icon, 
        	state_json:strJson,
        },
        success: function (data) {
        	if (!when_close) {
        		top.layer.msg('保存成功', {icon: 1, skin:is_dark?'layui-layer-lan':''});
	        	loadSnapshot();
        	}
        },
        error: function () {
        	if (!when_close) {
        		top.layer.msg('保存失败', {icon: 1, skin:is_dark?'layui-layer-lan':''});
        	}
        }
    });
}

function load_default_shortcut() {
	
	$.ajax({
		url: path + "/gynh/biz/load_default_shortcut.do",
		type: 'post',
		async: false,
		data: {
			page_name:pageName, 
		},
		success: function (data) {
			var shortcuts = data['shortcut'];
			if (shortcuts.length == 1) {
				
				apply_shortcut(shortcuts[0].state_json);
				
			}
        },
        error: function () {
        }
	});
	
}

function apply_shortcut(json) {
	var cfg = JSON.parse(json, function(k,v) {
		if(v.indexOf&&v.indexOf('function')>-1) {
			return eval("(function(){return "+v+" })()")
		}
		return v;
	});
	
	currentPowerUnit = cfg.currentPowerUnit;
	currentCDAUnit = cfg.currentCDAUnit;
	currentUPWUnit = cfg.currentUPWUnit;
	currentPN2Unit = cfg.currentPN2Unit;
	currentTimeUnit = cfg.currentTimeUnit;
	currentRCUnit = cfg.currentRCUnit;
    
    if(currentRCUnit==null||currentRCUnit=='')
    {
    	currentRCUnit="KW2";
    }
    if(currentPowerUnit==null||currentPowerUnit=='')
    {
    	currentPowerUnit="mWh";
    }
    if(currentCDAUnit==null||currentCDAUnit=='')
    {
    	currentCDAUnit="k(m³)";
    }
    if(currentUPWUnit==null||currentUPWUnit=='')
    {
    	currentUPWUnit="k(m³)";
    }
    if(currentPN2Unit==null||currentPN2Unit=='')
    {
    	currentPN2Unit="k(m³)";
    }
    if(currentTimeUnit==null||currentTimeUnit=='')
    {
    	currentTimeUnit="M";
    }
}

function onClickLoad() {
	
	var begin = defaut_begin_date;
	var end = defaut_end_date;
	load_data(begin, end);
}
	
function translate_unit_array(data, from, to) {
	if (from == to) {
		return data;
	}
	
	if (from == 'kWh' && to == 'mWh'
		|| from == 'L' && to == 'm³'
		|| from == 'm³' && to == 'k(m³)'
	) {
		data = data.map(function (item) {
			  return item/1000.0;
		});
	}
	if (from == 'm³' && to == 'L'
		|| from == 'k(m³)' && to == 'm³'
		|| from == 'mWh' && to == 'kWh'
	) {
		data = data.map(function (item) {
			  return item*1000.0;
		});
	}
	if (from == 'L' && to == 'k(m³)') {
		data = data.map(function (item) {
			  return item/1000000.0;
		});
	}
	if (from == 'k(m³)' && to == 'L') {
		data = data.map(function (item) {
			  return item*1000000.0;
		});
	}
	if (from == 'L' && to == 'W(m³)') {
		data = data.map(function (item) {
			  return item/10000000.0;
		});
	}
	if (from == 'W(m³)' && to == 'L') {
		data = data.map(function (item) {
			  return item*10000000.0;
		});
	}
	if (from == 'm³' && to == 'W(m³)') {
		data = data.map(function (item) {
			  return item/10000.0;
		});
	}
	if (from == 'W(m³)' && to == 'm³') {
		data = data.map(function (item) {
			  return item*10000.0;
		});
	}
	if (from == 'k(m³)' && to == 'W(m³)') {
		data = data.map(function (item) {
			  return item/10.0;
		});
	}
	if (from == 'W(m³)' && to == 'k(m³)') {
		data = data.map(function (item) {
			  return item*10.0;
		});
	} 
	return data;
}

function translate_unit(data, from, to) {
	if (from == to) {
		return data;
	}
	
	if (from == 'kWh' && to == 'mWh'
		|| from == 'L' && to == 'm³'
		|| from == 'm³' && to == 'k(m³)'
	) {
		data = data/1000.0;
	}
	if (from == 'm³' && to == 'L'
		|| from == 'k(m³)' && to == 'm³'
		|| from == 'mWh' && to == 'kWh'
	) {
		data = data*1000.0;
	}
	if (from == 'L' && to == 'k(m³)') {
		data = data/1000000.0;
	}
	if (from == 'k(m³)' && to == 'L') {
		data = data*1000000.0;
	}
	if (from == 'L' && to == 'W(m³)') {
		data =data/10000000.0;
	}
	if (from == 'W(m³)' && to == 'L') {
		data =data*10000000.0;
	}
	if (from == 'm³' && to == 'W(m³)') {
		data =data/10000.0;
	}
	if (from == 'W(m³)' && to == 'm³') {
		data =data*10000.0;
	}
	if (from == 'k(m³)' && to == 'W(m³)') {
		data =data/10.0;
	}
	if (from == 'W(m³)' && to == 'k(m³)') {
		data =data*10.0;
	} 
	return data;
}

function set_units_on_controles()
{
	var units=unit_show.split('_');
	for(var i=0;i<units.length;i++){
		if(units[i]=='power'){
			document.getElementById("select_power").style.display="inline";
			document.getElementById("font_power").style.display="inline";
		}
		if(units[i]=='rc'){
			document.getElementById("select_rc").style.display="inline";
			document.getElementById("font_rc").style.display="inline";
		}
		if(units[i]=='upw'){
			document.getElementById("select_upw").style.display="inline";
			document.getElementById("font_upw").style.display="inline";
		}
		if(units[i]=='cda'){
			document.getElementById("select_cda").style.display="inline";
			document.getElementById("font_cda").style.display="inline";
		}
		if(units[i]=='pn2'){
			document.getElementById("select_pn2").style.display="inline";
			document.getElementById("font_pn2").style.display="inline";
		}
		if(units[i]=='time'){
			document.getElementById("select_hms").style.display="inline";
			document.getElementById("font_hms").style.display="inline";
		}
	}
	var select_hms = document.getElementById("select_hms");
	var select_power = document.getElementById("select_power");
	var select_upw = document.getElementById("select_upw");
	var select_cda = document.getElementById("select_cda");
	var select_pn2 = document.getElementById("select_pn2");
	var select_rc = document.getElementById("select_rc");
	
	for(var i=0; i<select_rc.options.length; i++){ 
		if(select_rc.options[i].value == currentRCUnit){ 
			select_rc.options[i].selected = true; 
		}
	}
	for(var i=0; i<select_power.options.length; i++){ 
		if(select_power.options[i].value == currentPowerUnit){ 
			select_power.options[i].selected = true; 
		}
	}
	for(var i=0; i<select_cda.options.length; i++){ 
		if(select_cda.options[i].value == currentCDAUnit){ 
			select_cda.options[i].selected = true; 
		}
	}
	for(var i=0; i<select_upw.options.length; i++){ 
		if(select_upw.options[i].value == currentUPWUnit){ 
			select_upw.options[i].selected = true; 
		}
	}
	for(var i=0; i<select_pn2.options.length; i++){ 
		if(select_pn2.options[i].value == currentPN2Unit){ 
			select_pn2.options[i].selected = true; 
		}
	}
	for(var i=0; i<select_hms.options.length; i++){ 
		if(select_hms.options[i].value == currentTimeUnit){ 
			select_hms.options[i].selected = true; 
		}
	}
}
function get_daily_product_data(begin,end) {
	if  (begin > end) {
		var temp=begin;
		begin = end;
		end = temp;
	}
	
	var cur_date = new Date();
    var cur_month_first_day = new Date(cur_date.getFullYear(), cur_date.getMonth(), 1);
    var last_moth_end_day = new Date();
    last_moth_end_day.setDate(cur_month_first_day.getDate() - 1); 
	
	var beginYear = begin.substr(0,4)*1;
	var beginMonth = begin.substr(5,2)*1;
	var beginDay =  begin.substr(8,2)*1;
	var endYear = end.substr(0,4)*1;
	var endMonth = end.substr(5,2)*1;
	var endDay = end.substr(8,2)*1;

	$.ajax({
	     
	    url: path+"/gynh/cub_departments_summary/get_daily_product_data.do",
	    type: 'post',
	    async: false,
	    data: {
	    	beginYear:beginYear, 
	    	beginMonth:beginMonth, 
	    	beginDay:beginDay, 
	    	endYear:endYear, 
	    	endMonth:endMonth, 
	    	endDay:endDay,
	    	base_year: last_moth_end_day.getFullYear(), 
        	base_month: last_moth_end_day.getMonth()+1, 
	    	param1:'',
	    },
	    success: function (data) {
	    	
	    	var product_data = data['daily_product'];
	    	var last_average = data['last_average'];
	    	
	    	upw_product=new Array();
	    	cda_product=new Array();
	    	
	    	for (var i = 0; i < product_data.length; i++) {
	    		if(product_data[i].product=='CDA') {
	    			cda_product.push(translate_unit(product_data[i].out,'m³', currentCDAUnit));
	    		}else if(product_data[i].product=='UPW') {
	    			upw_product.push(translate_unit(product_data[i].out,'m³', currentUPWUnit));
	    		}
			}
	    	
	    	for (var i = 0; i < last_average.length; i++) {
	    		if(last_average[i].product=='CDA') {
	    			cda_product_average = translate_unit(last_average[i].out,'m³', currentCDAUnit);
	    		}else if(last_average[i].product=='UPW') {
	    			upw_product_average = translate_unit(last_average[i].out,'m³', currentUPWUnit);
	    		}
			}
	    },
	   
	});
}



function load_data(begin, end) {
	if  (begin > end) {
		var temp=begin;
		begin = end;
		end = temp;
	}
	
    var cur_date = new Date();
    var cur_month_first_day = new Date(cur_date.getFullYear(), cur_date.getMonth(), 1);
    var last_moth_end_day = new Date();
    last_moth_end_day.setDate(cur_month_first_day.getDate() - 1); 

    var index = top.layer.msg('查询中...',{icon: 16, skin:is_dark?'layui-layer-lan':'', shade:0.1});
    if (!ajax_json) {
		first_summary_ajax_url7= path + "/gynh/factories_summary/get_data_for_first_summary.do";
	}
    $.ajax({
//        url: path + "/gynh/factories_summary/get_data_for_first_summary.do",
    	url: first_summary_ajax_url7,
        type: 'post',
        async: false,
        data: {
        	begin_date: begin, 
        	end_date: end, 
        	base_year: last_moth_end_day.getFullYear(), 
        	base_month: last_moth_end_day.getMonth()+1, 
        },
        success: function (data) {
        	
        	data_cons = row_to_col_daily(data['cons'], 'day', 'val', new Date(begin), new Date(end));
        	
        	data_unit_cons = row_to_col_daily(data['unit_cons'], 'day', 'val', new Date(begin), new Date(end));

        	data_base_average = data['base_average'];
        	data_base_unit_cons = data['base_unit_cons'];

        	for (var i = 0; i < data_cons.length; i++) {
        		if (data_cons[i].param == 'Power') {
        			data_cons[i].data = translate_unit_array(data_cons[i].data, 'kWh', currentPowerUnit);
        		} else if (data_cons[i].param == 'CDA') {
        			data_cons[i].data = translate_unit_array(data_cons[i].data, 'm³', currentCDAUnit);
        		} else if (data_cons[i].param == 'UPW') {
        			data_cons[i].data = translate_unit_array(data_cons[i].data, 'm³', currentUPWUnit);
        		} else if (data_cons[i].param == 'PN2') {
        			data_cons[i].data = translate_unit_array(data_cons[i].data, 'm³', currentPN2Unit);
        		}
//        		var str=param.split('_');
//        		param=str[0];
        	}

        	for (var i = 0; i < data_base_average.length; i++) {
        		if (data_base_average[i].param == 'Power') {
        			data_base_average[i].val = translate_unit(data_base_average[i].val, 'kWh', currentPowerUnit);
        		} else if (data_base_average[i].param == 'CDA') {
        			data_base_average[i].val = translate_unit(data_base_average[i].val, 'm³', currentCDAUnit);
        		} else if (data_base_average[i].param == 'UPW') {
        			data_base_average[i].val = translate_unit(data_base_average[i].val, 'm³', currentUPWUnit);
        		} else if (data_base_average[i].param == 'PN2') {
        			data_base_average[i].val = translate_unit(data_base_average[i].val, 'm³', currentPN2Unit);
        		}
//        		var str=param.split('_');
//        		param=str[0];
        	}

            load_data_impl();
            
        	top.layer.close(index);
        	top.layer.msg('数据加载完成', {icon: 1, skin:is_dark?'layui-layer-lan':''});
        },
        error: function () {
        	top.layer.close(index);
        	top.layer.msg('数据加载错误', {icon: 1, skin:is_dark?'layui-layer-lan':''});
        }
    });
    

    page_custom_resize();
}

