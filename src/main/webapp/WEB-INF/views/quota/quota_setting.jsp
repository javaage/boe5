<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/inc/taglibs.jsp"%>

<!DOCTYPE HTML>
<html>
  <head>
   	<meta charset="utf-8"> 
    <title>定额设置</title>
    
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<script type="text/javascript">
		var path = "${path}";
	</script>
	<script src="${path}/res/js/my-js/jquery-3.2.1.min.js"></script>
	<script src="${path}/res/js/my-js/handsontable.full.js"></script>
    <link rel="stylesheet" type="text/css" href="${path}/res/css/my-css/dx.common.css" />
    <link rel="stylesheet" type="text/css" href="${path}/res/css/my-css/handsontable.full.css" />
	<link rel="dx-theme" data-theme="generic.light" href="${path}/res/css/my-css/dx.light.css" />
	<script src="${path}/res/js/my-js/data.js"></script>
	<script src="${path}/res/js/my-js/quoat.js"></script>
	<script src="${path}/res/js/my-js/jszip.min.js"></script>
	<script src="${path}/res/js/my-js/dx.all.js"></script>
    <style type="text/css">
    .dx-row.dx-data-row .fac_enr {
    color: #bf4e6a;
    font-weight: bold;
}
    
    #events {
    background-color: rgba(191, 191, 191, 0.15);
    padding: 20px;
    margin-top: 20px;
}

#events > div {
    padding-bottom: 5px;
}

#events > div:after {
    content: "";
    display: table;
    clear: both;
}

#events #clear {
    float: right;
}

#events .caption {
    float: left;
    font-weight: bold;
    font-size: 115%;
    line-height: 115%;
    padding-top: 7px;
}

#events ul {
    list-style: none;
    max-height: 100px;
    overflow: auto;
    margin: 0;
}

#events ul li {
    padding: 7px 0;
    border-bottom: 1px solid #dddddd;
}

#events ul li:last-child {
    border-bottom: none;
}
</style>
  </head>
  <body class="dx-viewport">
   <div class="demo-container" >
   <div style="float:left; margin-left: 12px;padding-top: 5px;position: fixed; z-index: 100;">
	   <div style="text-align: center;float:left;
		    font-weight: bold;
		    background-color: #E1E5EE;
		    width: 120px;
		    height: 33px;
		    line-height: 33px;">当前年份：<a id="year"></a></div>
		    <div style="float:left;margin-top:6px;margin-left: 12px;">
		    <button id="preYear">上一年</button>
		    <button id="nextYear">下一年</button>
		    <button id="sava" onclick="quota_data()">保存</button>
	    </div>
	</div >
    <div id="gridContainer" style="float:left; margin-left: 0px;padding-top: 6px;position: fixed; z-index: 90px;margin-top:48px"></div>
   	</div>
  </body>
</html>
