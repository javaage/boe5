<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/inc/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../gynh/common/def.jsp"%>
<meta charset="utf-8">
<title></title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="format-detection" content="telephone=no">
<link rel="stylesheet" href="${path}/res/layui/css/layui.css"
	media="all" />
<link rel="stylesheet" href="${path}/res/css/one-css/main.css"
	media="all" />
<script src='${path}/res/js/my-js/jquery-3.1.0.min.js'></script>
<script src="${path}/res/js/my-js/jquery-3.2.1.min.js"></script>
<link rel='stylesheet' type='text/css'
	href='${path}/res/css/my-css/dx.spa.css' />
<link rel='stylesheet' type='text/css'
	href='${path}/res/css/my-css/dx.common.css' />
<link rel='dx-theme' data-theme='generic.<%=theme%>'
	href='${path}/res/css/my-css/dx.<%=theme%>.css' />
<script src="${path}/res/js/my-js/jquery.min.js"></script>
<script src='${path}/res/js/my-js/dx.all.js'></script>
<script src="${path}/res/layui/layui.js"></script>
<script src="${path}/res/js/echarts/echarts.min.js"></script>
<script src='${path}/res/js/my-js/util.js'></script>

<style>
html, body {
	height: 100%;
	width: 100%;
	margin: 0;
	padding: 0; <%--
	background: <%=page_bg_color%>; --%>
	overflow-x: hidden;
	overflow-y: hidden;
}
</style>
<script type="text/javascript">

		var ajax_url="${path}/gynh/summary/get_data.do";
		window.onload=function(){
			var value=getFormatDate(-7)+' - '+getFormatDate(-1);
			document.getElementById('beginTimeBox').value =value ;
			values(value);
			resizeScreen();
			myEchartsFontSize();
		}
		
		window.onresize = function() {
			
			resizeScreen();
			myEchartsFontSize();
		}
		
		//鼠标滑过svg
		function mouseover(p)
		{
			var po=document.getElementById(p);
			po.style.fill="#cccccc";
			po.style.opacity="0.8";
		}
		function mouseout(p)
		{
			var po=document.getElementById(p);
			po.style.fill="#cccccc";
			po.style.opacity="0";
			
		}
		//日期插件
		layui.use('laydate', function(){
			  var laydate = layui.laydate;
			  //日期范围
			  laydate.render({
			    elem: '#beginTimeBox',
			    range: true,
				type: 'date',
				done: function(value) {
					ajax_url="${path}/gynh/summary/get_data.do";
					if(value==null)
						return;
					values(value);
				}
			  });
		});
		
    function resizeScreen() {
    	
        var points01 ="583.5927272727272,156.0420032310178,362.0436363636363,237.0638126009693,371.5,258.06946688206784,502.53818181818184,309.0831987075929,537.6618181818181,321.0864297253635,594.4,301.5811793214863,598.4527272727272,288.07754442649434,686.2618181818182,250.56744749596123,694.3672727272727,253.5682552504039,726.7890909090909,243.06542810985462,725.4381818181818,205.55533117932148,587.6454545454545,154.54159935379644";
		var points02 ="330.97272727272724,241.56502423263328,174.26727272727274,288.07754442649434,158.05636363636364,309.0831987075929,389.06181818181824,408.10985460420034,563.329090909091,336.0904684975767,330.97272727272724,238.56421647819062";
 		var points03 ="152.65272727272728,307.58279483037154,144.54727272727274,342.0920840064621,385.0090909090909,450.1211631663974,407.9745454545454,426.1147011308562,561.9781818181818,358.5965266558966,561.9781818181818,334.5900646203554,386.36,411.11066235864297,156.70545454545456,303.0815831987076";
    	var points04 ="684.9109090909092,252.06785137318258,602.5054545454545,283.57633279483036,593.049090909091,303.0815831987076,556.5745454545455,324.08723747980616,564.68,333.0896607431341,567.3818181818182,351.09450726979,622.7690909090909,372.1001615508885,638.98,378.10177705977384,782.1763636363636,312.0840064620356,783.5272727272727,282.0759289176091,682.209090909091,250.56744749596123";
    	var points05 ="575.4872727272727,364.5981421647819,429.5890909090909,426.1147011308562,409.32545454545453,444.11954765751216,406.62363636363636,472.6272213247173,741.6490909090909,622.6676090468497,745.7018181818182,592.6595315024233,751.1054545454546,583.6571082390952,869.9854545454546,510.13731825525036,875.3890909090909,483.13004846526655,574.1363636363636,361.5973344103393";
    	var points06 ="20,230,350,230,370,300";
    	var points07 ="20,500,350,500,370,420";
    	var points08 ="780,530,810,600,1140,600";
    	var points09 ="725,320,760,390,900,390";
    	var points10 ="700,195,730,250,1060,250";
    	
    	var asleft="30,30,730,760,810,18";
    	var astop="150,420,170,310,520,690";
    	
    	var foleft="40,40,745,770,825,25,145,266,388";
    	var fotop="230,510,250,390,600,825,825,825,825";	
    	
	   	var windowHeight = $(window).height();
	   	var windowWidth = $(window).width();
	   	
	   	var width=windowWidth/1500;
	   	var height=windowHeight/945;
	   	document.getElementById('bu01').style.marginTop = 50*height+'px';
		document.getElementById('bu01').style.marginLeft =1390*width-70-80*height+'px';
	   	
	   	if(windowWidth/windowHeight > 1.6) {
	   		
	   		document.getElementById('svg01').style.width = windowWidth ;
	   		document.getElementById('svg01').style.height = windowHeight;
	   		
   			document.getElementById('svg01').style.backgroundPosition ='0px ' + (windowWidth/1.6-windowHeight)/-2 +'px';
  			document.getElementById('svg01').style.backgroundSize =windowWidth +'px'+' '+windowWidth/1.6 +'px';
 
	   		aswidth(foleft,fotop,asleft,astop,windowWidth,windowHeight);
  			
  	    	document.getElementById('p1').setAttribute('points',svgwidth(points01,windowWidth,windowHeight));
  	    	document.getElementById('p2').setAttribute('points',svgwidth(points02,windowWidth,windowHeight));
  	    	document.getElementById('p3').setAttribute('points',svgwidth(points03,windowWidth,windowHeight));
  	    	document.getElementById('p4').setAttribute('points',svgwidth(points04,windowWidth,windowHeight));
  	    	document.getElementById('p5').setAttribute('points',svgwidth(points05,windowWidth,windowHeight));
  	    	document.getElementById('p6').setAttribute('points',svgwidth(points06,windowWidth,windowHeight));
  	    	document.getElementById('p7').setAttribute('points',svgwidth(points07,windowWidth,windowHeight));
  	    	document.getElementById('p8').setAttribute('points',svgwidth(points08,windowWidth,windowHeight));
  	    	document.getElementById('p9').setAttribute('points',svgwidth(points09,windowWidth,windowHeight));
  	    	document.getElementById('p10').setAttribute('points',svgwidth(points10,windowWidth,windowHeight));
  	    		
	   	} else {
	   		document.getElementById('svg01').style.height = windowHeight;
	   		document.getElementById('svg01').style.width = windowWidth;
	   		
 			document.getElementById('svg01').style.backgroundPosition =(windowHeight*1.6-windowWidth)/-2 +'px'+' 0px';
 	   		document.getElementById('svg01').style.backgroundSize = windowHeight*1.6 +'px'+' '+windowHeight+'px';
 	   		
 	   		asheight(foleft,fotop,asleft,astop,windowWidth,windowHeight);
 	   		
 	   		document.getElementById('p1').setAttribute('points',svgheight(points01,windowWidth,windowHeight));
	    	document.getElementById('p2').setAttribute('points',svgheight(points02,windowWidth,windowHeight));
	    	document.getElementById('p3').setAttribute('points',svgheight(points03,windowWidth,windowHeight));
	    	document.getElementById('p4').setAttribute('points',svgheight(points04,windowWidth,windowHeight));
	    	document.getElementById('p5').setAttribute('points',svgheight(points05,windowWidth,windowHeight));
	    	document.getElementById('p6').setAttribute('points',svgheight(points06,windowWidth,windowHeight));
	    	document.getElementById('p7').setAttribute('points',svgheight(points07,windowWidth,windowHeight));
	    	document.getElementById('p8').setAttribute('points',svgheight(points08,windowWidth,windowHeight));
	    	document.getElementById('p9').setAttribute('points',svgheight(points09,windowWidth,windowHeight));
	    	document.getElementById('p10').setAttribute('points',svgheight(points10,windowWidth,windowHeight));
	   	}
        	
    }
    
	function aswidth(foleft,fotop,asleft,astop,windowWidth,windowHeight){
    	
		 color();
    	var asleft= asleft.split(",");
    	var astop= astop.split(",");
    	
    	var foleft= foleft.split(",");
    	var fotop= fotop.split(",");
    	
    	var w=windowWidth/1500;
    	
      	for( var i=0;i<astop.length;i++){	
      		
      		asleft[i]=asleft[i]*w;
      		astop[i]=astop[i]*w;
      		astop[i]=astop[i]+(windowWidth/1.6-windowHeight)/-2;
      		
    		document.getElementById('as0'+i).style.marginTop = astop[i]+'px';
    		document.getElementById('as0'+i).style.marginLeft =asleft[i]+'px';
    		if(i>=5&&i<6){
    			document.getElementById('as0'+i).style.width = 480*w+'px';
        		document.getElementById('as0'+i).style.height =120*w+'px';
    		}else{
    			document.getElementById('as0'+i).style.width = 320*w+'px';
        		document.getElementById('as0'+i).style.height =80*w+'px';
    		}
    	
    	}
		for( var i=0;i<fotop.length;i++){	
      		
      		foleft[i]=foleft[i]*w;
      		fotop[i]=fotop[i]*w;
      		fotop[i]=fotop[i]+(windowWidth/1.6-windowHeight)/-2;
      		
    		document.getElementById('fo0'+i).style.marginTop = fotop[i]+'px';
    		document.getElementById('fo0'+i).style.marginLeft =foleft[i]+'px';
    		document.getElementById('fo0'+i).style.width = 100*w+'px';
    		document.getElementById('fo0'+i).style.height =60*w+'px';
    		document.getElementById('fo0'+i).style.fontSize=20*w +'px';
    		
    	}
      	
      /* 	document.getElementById('bu01').style.marginTop = 100*w+(windowWidth/1.6-windowHeight)/-2+'px';
		document.getElementById('bu01').style.marginLeft =1240*w+'px'; */
		document.getElementById('bu01').style.width = 600*w+'px';
		document.getElementById('bu01').style.height =90*w+'px';
		
		document.getElementById('beginTimeBox').style.height =30*w+'px';
		document.getElementById('beginTimeBox').style.width =190*w+14+'px';
		document.getElementById('beginTimeBox').style.fontSize =16*w+'px';
	
		var button=document.getElementsByClassName('button');
		
		button[0].style.width =100*w+'px';
		button[0].style.height =30*w+'px';
		button[0].style.fontSize =12*w+'px';
	/* 	button[1].style.width =100*w+'px';
		button[1].style.height =30*w+'px';
		button[1].style.fontSize =12*w+'px' */; 
      	
		var echarts=document.getElementsByClassName('ech01');
		for( var i=0;i<echarts.length;i++){	
      		
			if(i>=17&&i<21){
				echarts[i].style.width =120*w+'px';
				echarts[i].style.height =120*w+'px';
    		}else{
    			echarts[i].style.width =80*w+'px';
    			echarts[i].style.height =80*w+'px';
    		}
    		
    	}
		myecharts();
    }
	
	function asheight(foleft,fotop,asleft,astop,windowWidth,windowHeight){
    	
		var asleft= asleft.split(",");
    	var astop= astop.split(","); 
    	var foleft= foleft.split(",");
    	var fotop= fotop.split(","); 
    	
    	var w=windowHeight/945;
    	
      	for( var i=0;i<asleft.length;i++){	
      		
      		asleft[i]=asleft[i]*w;
      		astop[i]=astop[i]*w;
      		asleft[i]=asleft[i]+(windowHeight*1.6-windowWidth)/-2;
      		
      		document.getElementById('as0'+i).style.marginTop = astop[i]+'px';
    		document.getElementById('as0'+i).style.marginLeft =asleft[i]+'px';
    		if(i>=5&&i<6){
    			document.getElementById('as0'+i).style.width = 480*w+'px';
        		document.getElementById('as0'+i).style.height =120*w+'px';
    		}else{
    			document.getElementById('as0'+i).style.width = 320*w+'px';
        		document.getElementById('as0'+i).style.height =80*w+'px';
    		}
    		
    	}
		for( var i=0;i<foleft.length;i++){	
      		
      		foleft[i]=foleft[i]*w;
      		fotop[i]=fotop[i]*w;
      		foleft[i]=foleft[i]+(windowHeight*1.6-windowWidth)/-2;
      		
      		document.getElementById('fo0'+i).style.marginTop = fotop[i]+'px';
    		document.getElementById('fo0'+i).style.marginLeft =foleft[i]+'px';
    		document.getElementById('fo0'+i).style.width = 100*w+'px';
    		document.getElementById('fo0'+i).style.height =60*w+'px';
    		document.getElementById('fo0'+i).style.fontSize=20*w +'px';
    		
        	
    	}
      	
    /*   	document.getElementById('bu01').style.marginTop = 100*w+'px';
		document.getElementById('bu01').style.marginLeft =1480*w+(windowHeight*1.6-windowWidth)/-2+'px'; */
		document.getElementById('bu01').style.width = 600*w+'px';
		document.getElementById('bu01').style.height =90*w+'px';
		
		document.getElementById('beginTimeBox').style.height =30*w+'px';
		document.getElementById('beginTimeBox').style.width =190*w+14+'px';
		document.getElementById('beginTimeBox').style.fontSize =16*w+'px';
		
		var button=document.getElementsByClassName('button');
		
		button[0].style.width =100*w+'px';
		button[0].style.height =30*w+'px';
		button[0].style.fontSize =12*w+'px';
		/*button[1].style.width =100*w+'px';
		button[1].style.height =30*w+'px';
		button[1].style.fontSize =12*w+'px';
		 */
		var echarts=document.getElementsByClassName('ech01');
		
		for( var i=0;i<echarts.length;i++){	
      		
			if(i>=17&&i<21){
				echarts[i].style.width =120*w+'px';
				echarts[i].style.height =120*w+'px';
    		}else{
    			echarts[i].style.width =80*w+'px';
    			echarts[i].style.height =80*w+'px';
    		}
    	}
		myecharts();
    }
	
	//echarts字体
	function myEchartsFontSize()
	{
		var fontSize01=13;
		var fontSize02=15;
	 	var windowHeight = $(window).height();
	   	var windowWidth = $(window).width();
	  	if(windowWidth/windowHeight > 1.6) {
	  		var length=windowWidth/1500;
	  	}else
	  	{
	  		var length=windowHeight/945;
	  	}
	  	cell_power_chart.setOption({
			series: [{},
    	        {label: {normal: {
 	                    textStyle: { 
								
								fontSize: fontSize01*length,
								}
							},
				 	}
    	        }
    	        
    	    ]
		});cell_upw_chart.setOption({
			series: [{},
    	        {label: {normal: {
 	                    textStyle: { 
								
								fontSize: fontSize01*length,
								}
							},
				 	}
    	        }
    	        
    	    ]
		});cell_pn2_chart.setOption({
			series: [{},
    	        {label: {normal: {
 	                    textStyle: { 
								
								fontSize: fontSize01*length,
								}
							},
				 	}
    	        }
    	        
    	    ]
		});cell_cda_chart.setOption({
			series: [{},
    	        {label: {normal: {
 	                    textStyle: { 
								
								fontSize: fontSize01*length,
								}
							},
				 	}
    	        }
    	        
    	    ]
		});cf_power_chart.setOption({
			series: [{},
    	        {label: {normal: {
 	                    textStyle: { 
								
								fontSize: fontSize01*length,
								}
							},
				 	}
    	        }
    	        
    	    ]
		});cf_upw_chart.setOption({
			series: [{},
    	        {label: {normal: {
 	                    textStyle: { 
								
								fontSize: fontSize01*length,
								}
							},
				 	}
    	        }
    	        
    	    ]
		});cf_pn2_chart.setOption({
			series: [{},
    	        {label: {normal: {
 	                    textStyle: { 
								
								fontSize: fontSize01*length,
								}
							},
				 	}
    	        }
    	        
    	    ]
		});cf_cda_chart.setOption({
			series: [{},
    	        {label: {normal: {
 	                    textStyle: { 
								
								fontSize: fontSize01*length,
								}
							},
				 	}
    	        }
    	        
    	    ]
		});array_power_chart.setOption({
			series: [{},
    	        {label: {normal: {
 	                    textStyle: { 
								
								fontSize: fontSize01*length,
								}
							},
				 	}
    	        }
    	        
    	    ]
		});array_upw_chart.setOption({
			series: [{},
    	        {label: {normal: {
 	                    textStyle: { 
								
								fontSize: fontSize01*length,
								}
							},
				 	}
    	        }
    	        
    	    ]
		});array_pn2_chart.setOption({
			series: [{},
    	        {label: {normal: {
 	                    textStyle: { 
								
								fontSize: fontSize01*length,
								}
							},
				 	}
    	        }
    	        
    	    ]
		});array_cda_chart.setOption({
			series: [{},
    	        {label: {normal: {
 	                    textStyle: { 
								
								fontSize: fontSize01*length,
								}
							},
				 	}
    	        }
    	        
    	    ]
		});cub_power_chart.setOption({
			series: [{},
    	        {label: {normal: {
 	                    textStyle: { 
								
								fontSize: fontSize01*length,
								}
							},
				 	}
    	        }
    	        
    	    ]
		});md_power_chart.setOption({
			series: [{},
    	        {label: {normal: {
 	                    textStyle: { 
								
								fontSize: fontSize01*length,
								}
							},
				 	}
    	        }
    	        
    	    ]
		});cub_upw_chart.setOption({
			series: [{},
    	        {label: {normal: {
 	                    textStyle: { 
								
								fontSize: fontSize01*length,
								}
							},
				 	}
    	        }
    	        
    	    ]
		});cub_pn2_chart.setOption({
			series: [{},
    	        {label: {normal: {
 	                    textStyle: { 
								
								fontSize: fontSize01*length,
								}
							},
				 	}
    	        }
    	        
    	    ]
		});cub_cda_chart.setOption({
			series: [{},
    	        {label: {normal: {
 	                    textStyle: { 
								
								fontSize: fontSize01*length,
								}
							},
				 	}
    	        }
    	        
    	    ]
		});sum_power_chart.setOption({
			series: [{},
    	        {label: {normal: {
 	                    textStyle: { 
								
								fontSize: fontSize02*length,
								}
							},
				 	}
    	        }
    	        
    	    ]
		});sum_upw_chart.setOption({
			series: [{},
    	        {label: {normal: {
 	                    textStyle: { 
								
								fontSize: fontSize02*length,
								}
							},
				 	}
    	        }
    	        
    	    ]
		});
		sum_pn2_chart.setOption({
			series: [{},
    	        {label: {normal: {
 	                    textStyle: { 
								
								fontSize: fontSize02*length,
								}
							},
				 	}
    	        }
    	        
    	    ]
		});
		sum_cda_chart.setOption({
			series: [{},
    	        {label: {normal: {
 	                    textStyle: { 
								
								fontSize: fontSize02*length,
								}
							},
				 	}
    	        }
    	        
    	    ]
		});
	} 
	
	function myecharts(){
		
		cell_power_chart.resize();
		cell_upw_chart.resize();
		cell_pn2_chart.resize();
		cell_cda_chart.resize();
		cf_power_chart.resize();
		cf_upw_chart.resize();
		cf_pn2_chart.resize();
		cf_cda_chart.resize();
		array_power_chart.resize();
		array_upw_chart.resize();
		array_pn2_chart.resize();
		array_cda_chart.resize();
		cub_power_chart.resize();
		md_power_chart.resize();
		cub_upw_chart.resize();
		cub_pn2_chart.resize();
		cub_cda_chart.resize();
		sum_power_chart.resize();
		sum_upw_chart.resize();
		sum_pn2_chart.resize();
		sum_cda_chart.resize();
	}
	
	//显示宽高比例较大 SVG
    function svgwidth(points,windowWidth,windowHeight){
    	
    	var arr= points.split(",");
    	var w=windowWidth/1500;
      	for( var i=0;i<arr.length;i++){	
    		arr[i]=arr[i]*w;
    		if(i%2!=0){
    			arr[i]=arr[i]+(windowWidth/1.6-windowHeight)/-2;
    		}
    	}
      	return arr;
    }
	
  //显示宽高比例较小 SVG
	function svgheight(points,windowWidth,windowHeight){
    	
    	var arr= points.split(",");
    	var h=windowHeight/945;
      	for( var i=0;i<arr.length;i++){	
    		arr[i]=arr[i]*h;
    		if(i%2==0){
    			arr[i]=arr[i]+(windowHeight*1.6-windowWidth)/-2;
    		}
    	}
      	return arr;
    }
	//最近二十四小时
	function one_day() {
		var init_begin_date = getTimeDate(-24);
		var init_end_date = getTimeDate(0);
		ajax_url="${path}/gynh/summary/get_data_one_day.do";                                         
		var value=init_end_date+' - '+init_begin_date;
		values(value);
	}
	//最近一周
	function week() {
		var nowDate=getNowFormatDate();
		var value=addDate(nowDate,-7)+' - '+addDate(nowDate,-1);
		values(value);
	}
	
	//最近半年
	function month() {
		var nowDate=getNowFormatDate();
		var value=addDate(nowDate,-180)+' - '+addDate(nowDate,-1);
		values(value);
	}
	
	//获取当前日期
	function getNowFormatDate() {
        var date = new Date();
        var seperator1 = "-";
        var year = date.getFullYear();
        var month = date.getMonth() + 1;
        var strDate = date.getDate();
        if (month >= 1 && month <= 9) {
            month = "0" + month;
        }
        if (strDate >= 0 && strDate <= 9) {
            strDate = "0" + strDate;
        }
        var currentdate = year + seperator1 + month + seperator1 + strDate;
        return currentdate;
	}
	
	//日期计算
	function addDate(date,days){ 
		
       var d=new Date(date); 
       d.setDate(d.getDate()+days); 
       var m=d.getMonth()+1; 
       return d.getFullYear()+'-'+m+'-'+d.getDate(); 
	} 
	
	//得到后台的数据
	function values(value){
		
		var arr=value.split('-');
		
		
		var param1 =0;
		if(ajax_url=="${path}/gynh/summary/get_data.do") {
			var beginYear = arr[0];
			var beginMonth =arr[1];
			var beginDay = arr[2];
			var endYear =arr[3];
			var endMonth =arr[4];
			var endDay =arr[5];
			var ajax_data={
	  			beginYear:beginYear, 
	  			beginMonth:beginMonth, 
	  			beginDay:beginDay, 
	  			endYear:endYear, 
	  			endMonth:endMonth, 
	  			endDay:endDay, 
	  			param1:param1,
	  			};
		}else {
			var ajax_data={datatime:value};
		}
		
		$.ajax({
	          url: ajax_url,
	          type: "POST",
	          dataType: "json",
	          data: ajax_data,
	          async: false,
	          success: function(data) {
	        	  
	        	//保留小数点后几位
	        	var bit_value=2;
	        	var bit_per =2
	        	
	        	//百分值
        	 	var cell_power_per =0;
		        var cf_power_per =0;
		        var array_power_per =0;
		        var cub_power_per =0;
		        var md_power_per =0;
		        
		        var cell_upw_per =0;
		        var cf_upw_per =0;
		        var array_upw_per =0;
		        var cub_upw_per =0;
		        var md_upw_per =0;
		        
		        var cell_pn2_per =0;
		        var cf_pn2_per =0;
		        var array_pn2_per =0;
		        var cub_pn2_per =0;
		        var md_pn2_per =0;
		        
		        var cell_cda_per =0;
		        var cf_cda_per =0;
		        var array_cda_per =0;
		        var cub_cda_per =0;
		        var md_cda_per =0;
		        
		        cell_power_per =cell_power_per.toFixed(bit_per);
		        cf_power_per =cf_power_per.toFixed(bit_per);
		        array_power_per =array_power_per.toFixed(bit_per);
		        cub_power_per =cub_power_per.toFixed(bit_per);
		        md_power_per =md_power_per.toFixed(bit_per);
		        
		        cell_upw_per =cell_upw_per.toFixed(bit_per);
		        cf_upw_per =cf_upw_per.toFixed(bit_per);
		        array_upw_per =array_upw_per.toFixed(bit_per);
		        cub_upw_per =cub_upw_per.toFixed(bit_per);
		        md_upw_per =md_upw_per.toFixed(bit_per);
		        
		        cell_pn2_per =cell_pn2_per.toFixed(bit_per);
		        cf_pn2_per =cf_pn2_per.toFixed(bit_per);
		        array_pn2_per =array_pn2_per.toFixed(bit_per);
		        cub_pn2_per =cub_pn2_per.toFixed(bit_per);
		        md_pn2_per =md_pn2_per.toFixed(bit_per);
		        
		        cell_cda_per =cell_cda_per.toFixed(bit_per);
		        cf_cda_per =cf_cda_per.toFixed(bit_per);
		        array_cda_per =array_cda_per.toFixed(bit_per);
		        cub_cda_per =cub_cda_per.toFixed(bit_per);
		        md_cda_per =md_cda_per.toFixed(bit_per);
	        	  
	            var cell_power =data['cell_power']/1000;
		        var cf_power =data['cf_power']/1000;
		        var array_power =data['array_power']/1000;
		        var cub_power =data['cub_power']/1000;
		        var md_power =data['md_power']/1000;
		        var other_power =data['other_power']/1000;
		        var sum_power_cons=data['sum_power_cons']/1000;
		        
		        var cell_upw =data['cell_upw']/1000;
		        var cf_upw =data['cf_upw']/1000;
		        var array_upw =data['array_upw']/1000;
		        var cub_upw =data['cub_upw']/1000;
		        var md_upw =data['md_upw']/1000;
		        var other_upw =data['other_upw']/1000;
		        var sum_upw_cons=data['sum_upw_cons']/1000;
		        
		        var cell_pn2 =data['cell_pn2']/1000;
		        var cf_pn2 =data['cf_pn2']/1000;
		        var array_pn2 =data['array_pn2']/1000;
		        var cub_pn2 =data['cub_pn2']/1000;
		        var md_pn2 =data['md_pn2']/1000;
		        var other_pn2 =data['other_pn2']/1000;
		        var sum_pn2_cons=data['sum_pn2_cons']/1000;
		        
		        var cell_cda =data['cell_cda']/1000;
		        var cf_cda =data['cf_cda']/1000;
		        var array_cda =data['array_cda']/1000;
		        var cub_cda =data['cub_cda']/1000;
		        var md_cda =data['md_cda']/1000;
		        var other_cda =data['other_cda']/1000;
		        var sum_cda_cons=data['sum_cda_cons']/1000;
		        
		        cell_power = cell_power.toFixed(bit_value);
		        cf_power = cf_power.toFixed(bit_value);
		        array_power =array_power.toFixed(bit_value);
		        cub_power =cub_power.toFixed(bit_value);
		        md_power =md_power.toFixed(bit_value);
		        other_power =other_power.toFixed(bit_value);
		        sum_power_cons=sum_power_cons.toFixed(bit_value);
		        
		        cell_upw =cell_upw.toFixed(bit_value);
		        cf_upw =cf_upw.toFixed(bit_value);
		        array_upw =array_upw.toFixed(bit_value);
		        cub_upw =cub_upw.toFixed(bit_value);
		        md_upw =md_upw.toFixed(bit_value);
		        other_upw =other_upw.toFixed(bit_value);
		        sum_upw_cons=sum_upw_cons.toFixed(bit_value);
		        
		        cell_pn2 =cell_pn2.toFixed(bit_value);
		        cf_pn2 =cf_pn2.toFixed(bit_value);
		        array_pn2 =array_pn2.toFixed(bit_value);
		        cub_pn2 =cub_pn2.toFixed(bit_value);
		        md_pn2 =md_pn2.toFixed(bit_value);
		        other_pn2 =other_pn2.toFixed(bit_value);
		        sum_pn2_cons=sum_pn2_cons.toFixed(bit_value);
		        
		        cell_cda =cell_cda.toFixed(bit_value);
		        cf_cda =cf_cda.toFixed(bit_value);
		        array_cda =array_cda.toFixed(bit_value);
		        cub_cda =cub_cda.toFixed(bit_value);
		        md_cda =md_cda.toFixed(bit_value);
		        other_cda =other_cda.toFixed(bit_value);
		        sum_cda_cons=sum_cda_cons.toFixed(bit_value);
		        
		        if(sum_power_cons!=0){
		        	cell_power_per =cell_power/sum_power_cons*100;
			        cf_power_per =cf_power/sum_power_cons*100;
			        md_power_per =md_power/sum_power_cons*100;
			        cub_power_per =cub_power/sum_power_cons*100;
			        other_power_per =other_power/sum_power_cons*100;

			        cell_power_per =cell_power_per.toFixed(bit_per);
			        cf_power_per =cf_power_per.toFixed(bit_per);
			        md_power_per =md_power_per.toFixed(bit_per);
			        cub_power_per =cub_power_per.toFixed(bit_per);
			        other_power_per =other_power_per.toFixed(bit_per);
			        
			        array_power_per =100-cell_power_per-cf_power_per-md_power_per-cub_power_per-other_power_per;
			        array_power_per =array_power_per.toFixed(bit_per);
		        }
		        if(sum_upw_cons!=0) {
		        	cell_upw_per =cell_upw/sum_upw_cons*100;
			        cf_upw_per =cf_upw/sum_upw_cons*100;
			        md_upw_per =md_upw/sum_upw_cons*100;
			        cub_upw_per =cub_upw/sum_upw_cons*100;
			        other_upw_per =other_upw/sum_upw_cons*100;

			        cell_upw_per =cell_upw_per.toFixed(bit_per);
			        cf_upw_per =cf_upw_per.toFixed(bit_per);
			        md_upw_per =md_upw_per.toFixed(bit_per);
			        cub_upw_per =cub_upw_per.toFixed(bit_per);
			        other_upw_per =other_upw_per.toFixed(bit_per);
					
			        array_upw_per =100-cell_upw_per-cf_upw_per-md_upw_per-cub_upw_per-other_upw_per;
			        array_upw_per =array_upw_per.toFixed(bit_per);

		        }
		        if(sum_pn2_cons!=0){
		        	cell_pn2_per =cell_pn2/sum_pn2_cons*100;
			        cf_pn2_per =cf_pn2/sum_pn2_cons*100;
			        md_pn2_per =md_pn2/sum_pn2_cons*100;
			        cub_pn2_per =cub_pn2/sum_pn2_cons*100;
			        other_pn2_per =other_pn2/sum_pn2_cons*100;
 
			        cell_pn2_per =cell_pn2_per.toFixed(bit_per);
			        cf_pn2_per =cf_pn2_per.toFixed(bit_per);
			        md_pn2_per =md_pn2_per.toFixed(bit_per);
			        cub_pn2_per =cub_pn2_per.toFixed(bit_per);
			        other_pn2_per =other_pn2_per.toFixed(bit_per);

			        array_pn2_per =100-cell_pn2_per-cf_pn2_per-md_pn2_per-cub_pn2_per-other_pn2_per;
			        array_pn2_per =array_pn2_per.toFixed(bit_per);

		        }
				if(sum_cda_cons!=0){
					cell_cda_per =cell_cda/sum_cda_cons*100;
			        cf_cda_per =cf_cda/sum_cda_cons*100;
			        md_cda_per =md_cda/sum_cda_cons*100;
			        cub_cda_per =cub_cda/sum_cda_cons*100;
			        other_cda_per =other_cda/sum_cda_cons*100;

			        cell_cda_per =cell_cda_per.toFixed(bit_per);
			        cf_cda_per =cf_cda_per.toFixed(bit_per);
			        md_cda_per =md_cda_per.toFixed(bit_per);
			        cub_cda_per =cub_cda_per.toFixed(bit_per);
			        other_cda_per =other_cda_per.toFixed(bit_per);
			        
			        array_cda_per =100-cell_cda_per-cf_cda_per-md_cda_per-cub_cda_per-other_cda_per;
			        array_cda_per =array_cda_per.toFixed(bit_per);

		        }
		         
		       //Power  
	           cell_power_chart.setOption({
	    			series:[{},
	        	        {
	        	          data:[
					            {value:''+cell_power+'', name:'' +cell_power+''},
					            {value:''+sum_power_cons-cell_power+'', name:'' +cell_power_per+'%',label: {fontSize: 10,},}	
	    					]
	        	        }
	        	    ]
	    		});
	            
	          cf_power_chart.setOption({
	    			series: [{},
	        	        {
	        	        	data:[
					            {value:''+cf_power+'', name:'' +cf_power+''},
					            {value:''+sum_power_cons-cf_power+'', name:'' +cf_power_per+'%',label: {fontSize: 10,},}	
	    					]
	        	        }
	        	    ]
	    		});
	            array_power_chart.setOption({
	    			series: [{},
	        	        {
	        	        	data:[
					            {value:''+array_power+'', name:'' +array_power+''},
					            {value:''+sum_power_cons-array_power+'', name:'' +array_power_per+'%',label: {fontSize: 10,},}	
	    					]
	        	        }
	        	    ]
	    		});
	           cub_power_chart.setOption({
	    			series: [{},
	        	        {
	        	        	data:[
					            {value:''+cub_power+'', name:'' +cub_power+''},
					            {value:''+sum_power_cons-cub_power+'', name:'' +cub_power_per+'%',label: {fontSize: 10,},}	
	    					]
	        	        }
	        	    ]
	    		});
	            md_power_chart.setOption({
	    			series: [{},
	        	        {
	        	        	data:[
					            {value:''+md_power+'', name:'' +md_power+''},
					            {value:''+sum_power_cons-md_power+'', name:'' +md_power_per+'%',label: {fontSize: 10,},}	
	    					]
	        	        }
	        	    ]
	    		});
	            
	          //upw  
	            cell_upw_chart.setOption({
	    			series:[{},
	        	        {
	        	          data:[
					            {value:''+cell_upw+'', name:'' +cell_upw+''},
					            {value:''+sum_upw_cons-cell_upw+'', name:'' +cell_upw_per+'%',label: {fontSize: 10,},}	
	    					]
	        	        }
	        	    ]
	    		});
	            
	          cf_upw_chart.setOption({
	    			series: [{},
	        	        {
	        	        	data:[
					            {value:''+cf_upw+'', name:'' +cf_upw+''},
					            {value:''+sum_upw_cons-cf_upw+'', name:'' +cf_upw_per+'%',label: {fontSize: 10,},}	
	    					]
	        	        }
	        	    ]
	    		});
	            array_upw_chart.setOption({
	    			series: [{},
	        	        {
	        	        	data:[
					            {value:''+array_upw+'', name:'' +array_upw+''},
					            {value:''+sum_upw_cons-array_upw+'', name:'' +array_upw_per+'%',label: {fontSize: 10,},}	
	    					]
	        	        }
	        	    ]
	    		});
	           cub_upw_chart.setOption({
	    			series: [{},
	        	        {
	        	        	data:[
					            {value:''+cub_upw+'', name:'' +cub_upw+''},
					            {value:''+sum_upw_cons-cub_upw+'', name:'' +cub_upw_per+'%',label: {fontSize: 10,},}	
	    					]
	        	        }
	        	    ]
	    		});
	           
	         //PN2  
	           cell_pn2_chart.setOption({
	    			series:[{},
	        	        {
	        	          data:[
					            {value:''+cell_pn2+'', name:'' +cell_pn2+''},
					            {value:''+sum_pn2_cons-cell_pn2+'', name:'' +cell_pn2_per+'%',label: {fontSize: 10,},}	
	    					]
	        	        }
	        	    ]
	    		}); 
	            
	          	cf_pn2_chart.setOption({
	    			series: [{},
	        	        {
	        	        	data:[
					            {value:''+cf_pn2+'', name:'' +cf_pn2+''},
					            {value:''+sum_pn2_cons-cf_pn2+'', name:'' +cf_pn2_per+'%',label: {fontSize: 10,},}	
	    					]
	        	        }
	        	    ]
	    		});
	           array_pn2_chart.setOption({
	    			series: [{},
	        	        {
	        	        	data:[
					            {value:''+array_pn2+'', name:'' +array_pn2+''},
					            {value:''+sum_pn2_cons-array_pn2+'', name:'' +array_pn2_per+'%',label: {fontSize: 10,},}	
	    					]
	        	        }
	        	    ]
	    		});
	           cub_pn2_chart.setOption({
	    			series: [{},
	        	        {
	        	        	data:[
					            {value:''+cub_pn2+'', name:'' +cub_pn2+''},
					            {value:''+sum_pn2_cons-cub_pn2+'', name:'' +cub_pn2_per+'%',label: {fontSize: 10,},}	
	    					]
	        	        }
	        	    ]
	    		});
	           
	           //CDA  
	          cell_cda_chart.setOption({
	    			series:[{},
	        	        {
	        	          data:[
					            {value:''+cell_cda+'', name:'' +cell_cda+''},
					            {value:''+sum_cda_cons-cell_cda+'', name:'' +cell_cda_per+'%',label: {fontSize: 10,},}	
	    					]
	        	        }
	        	    ]
	    		});
	            
	          	cf_cda_chart.setOption({
	    			series: [{},
	        	        {
	        	        	data:[
					            {value:''+cf_cda+'', name:'' +cf_cda+''},
					            {value:''+sum_cda_cons-cf_cda+'', name:'' +cf_cda_per+'%',label: {fontSize: 10,},}	
	    					]
	        	        }
	        	    ]
	    		});
	            array_cda_chart.setOption({
	    			series: [{},
	        	        {
	        	        	data:[
					            {value:''+array_cda+'', name:'' +array_cda+''},
					            {value:''+sum_cda_cons-array_cda+'', name:'' +array_cda_per+'%',label: {fontSize: 10,},}	
	    					]
	        	        }
	        	    ]
	    		});
	       
	           cub_cda_chart.setOption({
	    			series: [{},
	        	        {
	        	        	data:[
					            {value:''+cub_cda+'', name:'' +cub_cda+''},
					            {value:''+sum_cda_cons-cub_cda+'', name:'' +cub_cda_per+'%',label: {fontSize: 10,},}	
	    					]
	        	        }
	        	    ]
	    		});


				//总计
	            sum_power_chart.setOption({
	    			series: [{},
	        	        {
	        	        	data:[
					            {value:''+sum_power_cons+'', name:+sum_power_cons+'\n\nmWh'},
					      
	    					],
	    					label: {
	        	                normal: {
	        	                    show: true,
	        	                    position: 'center',
	        	                    textStyle: { 
										color: 'white',
										fontSize: '13'
									}
								},
								emphasis: {
									show: true,
									textStyle: {
										fontSize: '15',
										fontWeight: 'bold'
									}
								}
							},
	        	        }
	        	    ]
	    		});
	            sum_upw_chart.setOption({
	    			series: [{},
	        	        {
	        	        	data:[
					            {value:''+sum_upw_cons+'', name:+sum_upw_cons+'\n\nk(m³)'}
	    					],
	    					label: {
	        	                normal: {
	        	                    show: true,
	        	                    position: 'center',
	        	                    textStyle: { 
										color: 'white',
										fontSize: '13'
									}
								},
								emphasis: {
									show: true,
									textStyle: {
										fontSize: '15',
										fontWeight: 'bold'
									}
								}
							},
	        	        }
	        	    ]
	    		});
	            sum_pn2_chart.setOption({
	    			series: [{},
	        	        {
	        	        	data:[
					            {value:''+sum_pn2_cons+'', name:+sum_pn2_cons+'\n\nk(m³)'}
	    					],
			    			label: {
		    	                normal: {
		    	                    show: true,
		    	                    position: 'center',
		    	                    textStyle: { 
										color: 'white',
										fontSize: '13'
									}
								},
								emphasis: {
									show: true,
									textStyle: {
										fontSize: '15',
										fontWeight: 'bold'
									}
								}
							},
	    			
	        	        }
	        	    ]
	    		});
	            sum_cda_chart.setOption({
	    			series: [{},
	        	        {
	        	        	data:[
					            {value:''+sum_cda_cons+'', name:+sum_cda_cons+'\n\nk(m³)'}
	    					],
			    			label: {
		    	                normal: {
		    	                    show: true,
		    	                    position: 'center',
		    	                    textStyle: { 
										color: 'white',
										fontSize: '13'
									}
								},
								emphasis: {
									show: true,
									textStyle: {
										fontSize: '15',
										fontWeight: 'bold'
									}
								}
							},
	        	        }
	        	    ]
	    		});
	        },
          	error: function() {
          	}
	    });
	}
	//环图的颜色
	function color()
	{   
		var cPower='<%=power_color%>';
		var cUpw='<%=upw_color%>';
		var cPn2='<%=pn2_color%>';
		var cCda='<%=cda_color%>';
		cell_power_chart.setOption({
			color:['<%=power_color%>', 'rgba(36,36, 36, 0.36)'],
		});
		cell_upw_chart.setOption({
			color:['<%=upw_color%>', 'rgba(36,36, 36, 0.36)'],   
		});
		cell_pn2_chart.setOption({
			color:['<%=pn2_color%>', 'rgba(36,36, 36, 0.36)'],  
		});
		cell_cda_chart.setOption({
			color:['<%=cda_color%>', 'rgba(36,36, 36, 0.36)'],   
		});
		cf_power_chart.setOption({
			color:['<%=power_color%>', 'rgba(36,36, 36, 0.36)'],   
		});
		cf_upw_chart.setOption({
			color:['<%=upw_color%>', 'rgba(36,36, 36, 0.36)'],   
		});
		cf_pn2_chart.setOption({
			color:['<%=pn2_color%>', 'rgba(36,36, 36, 0.36)'],   
		});
		cf_cda_chart.setOption({
			color:['<%=cda_color%>', 'rgba(36,36, 36, 0.36)'], 
		});
		array_power_chart.setOption({
			color:['<%=power_color%>', 'rgba(36,36, 36, 0.36)'],   
		});
		array_upw_chart.setOption({
			color:['<%=upw_color%>', 'rgba(36,36, 36, 0.36)'],    
		});
		array_pn2_chart.setOption({
			color:['<%=pn2_color%>', 'rgba(36,36, 36, 0.36)'],   
		});
		array_cda_chart.setOption({
			color:['<%=cda_color%>', 'rgba(36,36, 36, 0.36)'],
		});
		cub_power_chart.setOption({
			color:['<%=power_color%>', 'rgba(36,36, 36, 0.36)'],   
		});
		md_power_chart.setOption({
			color:['<%=power_color%>', 'rgba(36,36, 36, 0.36)'],   
		});
		cub_upw_chart.setOption({
			color:['<%=upw_color%>', 'rgba(36,36, 36, 0.36)'],   
		});
		cub_pn2_chart.setOption({
			color:['<%=pn2_color%>', 'rgba(36,36, 36, 0.36)'],  
		});
		cub_cda_chart.setOption({
			color:['<%=cda_color%>', 'rgba(36,36, 36, 0.36)'],  
		});
		sum_power_chart.setOption({
			color:['<%=power_color%>', 'rgba(36,36, 36, 0.36)'],   
		});
		sum_upw_chart.setOption({
			color:['<%=upw_color%>', 'rgba(36,36, 36, 0.36)'], 
		});
		sum_pn2_chart.setOption({
			color:['<%=pn2_color%>', 'rgba(36,36, 36, 0.36)'], 
		});
		sum_cda_chart.setOption({
			color:['<%=cda_color%>', 'rgba(36,36, 36, 0.36)'],
		});
	}
	
    </script>

<style>
.button {
	-webkit-border-radius: 24;
	-moz-border-radius: 24;
	border-radius: 24px;
	font-family: Arial;
	color: #ffffff;
	font-size: 12px;
	background: #86ce6e;
	border: solid #47b642 1px;
	text-decoration: none;
}

.button:hover {
	color: #ffffff;
	background: #88a046;
	text-decoration: none;
}
</style>
<style>
.ech01 {
	width: 100px;
	height: 100px;
	float: left;
	display: table-cell;
	opacity: 0.88;
	margin-top: 0px;
}
#svg01 {
	background-image: url(${path}/res/images/factory.jpg);
	background-size: 480px 360px;
	position: absolute;
	z-index: 10;
	opacity: 1;
	background-repeat: no-repeat;
}
</style>

</head>
<body>
	<div id="as00"
		style="width: 520px; height: 120px; float: left; margin-top: 100px; position: absolute; z-index: 90; margin-left: 60px;">
		<div id="ep1" class="ech01"></div>
		<div id="ep2" class="ech01"></div>
		<div id="ep4" class="ech01"></div>
		<div id="ep3" class="ech01"></div>
		
	</div>
	<div id="as01"
		style="width: 520px; height: 120px; float: left; margin-top: 480px; position: absolute; z-index: 90; margin-left: 60px;">
		<div id="ep1_02" class="ech01"></div>
		<div id="ep2_02" class="ech01"></div>
		<div id="ep4_02" class="ech01"></div>
		<div id="ep3_02" class="ech01"></div>
		
	</div>
	<div id="as02"
		style="width: 520px; height: 120px; float: left; margin-top: 120px; position: absolute; z-index: 90; margin-left: 980px;">
		<div id="ep1_03" class="ech01"></div>
		<div id="ep2_03" class="ech01"></div>
		<div id="ep4_03" class="ech01"></div>
		<div id="ep3_03" class="ech01"></div>
		
	</div>
	<div id="as03"
		style="width: 520px; height: 120px; float: left; margin-top: 360px; position: absolute; z-index: 90; margin-left: 980px;">
		<div id="ep1_04" class="ech01"></div>
	</div>
	<div id="as04"
		style="width: 520px; height: 120px; float: left; margin-top: 600px; position: absolute; z-index: 90; margin-left: 980px;">
		<div id="ep1_05" class="ech01"></div>
		<div id="ep2_05" class="ech01"></div>
		<div id="ep4_05" class="ech01"></div>
		<div id="ep3_05" class="ech01"></div>
		
	</div>
	<div id="as05"
		style="width: 520px; height: 120px; float: left; margin-top: 800px; position: absolute; z-index: 90; margin-left: 980px;">
		<div id="ep1_06" class="ech01"></div>
		<div id="ep2_06" class="ech01"></div>
		<div id="ep4_06" class="ech01"></div>
		<div id="ep3_06" class="ech01"></div>
		
	</div>

	<div id="fo00"
		style="width: 120px; height: 90px; float: left; margin-top: 240px; font-size: 48px; position: absolute; z-index: 91; color: white; margin-left: 48px">
		CELL</div>
	<div id="fo01"
		style="width: 120px; height: 90px; float: left; margin-top: 620px; font-size: 48px; position: absolute; z-index: 91; color: white; margin-left: 48px">
		CF</div>
	<div id="fo02"
		style="width: 120px; height: 90px; float: left; margin-top: 250px; font-size: 48px; position: absolute; z-index: 91; color: white; margin-left: 980px">
		Array</div>
	<div id="fo03"
		style="width: 120px; height: 90px; float: left; margin-top: 490px; font-size: 48px; position: absolute; z-index: 91; color: white; margin-left: 980px">
		CUB</div>
	<div id="fo04"
		style="width: 120px; height: 90px; float: left; margin-top: 736px; font-size: 48px; position: absolute; z-index: 91; color: white; margin-left: 980px">
		MDL</div>       
	<div id="fo05"
		style="width: 120px; height: 90px; float: left; margin-top: 736px; font-size: 48px; position: absolute; z-index: 91; color: white; margin-left: 980px; text-align: center">
		Power</div>
	<div id="fo06"
		style="width: 120px; height: 90px; float: left; margin-top: 736px; font-size: 48px; position: absolute; z-index: 91; color: white; margin-left: 980px; text-align: center">
		UPW</div>
	<div id="fo07"
		style="width: 120px; height: 90px; float: left; margin-top: 736px; font-size: 48px; position: absolute; z-index: 91; color: white; margin-left: 980px; text-align: center">
		CDA</div>
	<div id="fo08"
		style="width: 120px; height: 90px; float: left; margin-top: 736px; font-size: 48px; position: absolute; z-index: 91; color: white; margin-left: 980px; text-align: center">
		PN2</div>

	<div id="bu01"
		style="width: 120px; height: 90px; float: left; margin-top: 60px; position: absolute; z-index: 91; color: white; margin-left: 1360px; opacity: 0.72;">

		<!-- 	<input class="button" type="button" onclick="week()" value="最近七日"
			style="float: left;"> 
		<input class="button" type="button" onclick="month()" value="最近六月"
			style="float: left       ; margin-left: 12px;"> -->
		
		<input id="beginTimeBox" type="text" placeholder="-"
			class="layui-input"
			style="width: 120px; height: 24px; color: #005757; clear: left; margin-left: 0px;
			-webkit-border-radius: 24;-moz-border-radius: 24;border-radius: 24px;">
		<input class="button" type="button" onclick="one_day()" value="二十四时"
			style="clear: left; margin-left: 0px; margin-top:12px; display:none">
	</div>
	<script type="text/javascript">
        
        var cell_power_chart = echarts.init(document.getElementById('ep1'));
        var cell_upw_chart = echarts.init(document.getElementById('ep2'));
        var cell_pn2_chart = echarts.init(document.getElementById('ep3'));
        var cell_cda_chart = echarts.init(document.getElementById('ep4'));
        
        var cf_power_chart = echarts.init(document.getElementById('ep1_02'));
        var cf_upw_chart = echarts.init(document.getElementById('ep2_02'));
        var cf_pn2_chart = echarts.init(document.getElementById('ep3_02'));
        var cf_cda_chart = echarts.init(document.getElementById('ep4_02'));
        
        var array_power_chart = echarts.init(document.getElementById('ep1_03'));
        var array_upw_chart = echarts.init(document.getElementById('ep2_03'));
        var array_pn2_chart = echarts.init(document.getElementById('ep3_03'));
        var array_cda_chart = echarts.init(document.getElementById('ep4_03'));
        
        var cub_power_chart = echarts.init(document.getElementById('ep1_04'));
        
        var md_power_chart = echarts.init(document.getElementById('ep1_05'));
        var cub_upw_chart = echarts.init(document.getElementById('ep2_05'));
        var cub_pn2_chart = echarts.init(document.getElementById('ep3_05'));
        var cub_cda_chart = echarts.init(document.getElementById('ep4_05'));
        
        var sum_power_chart = echarts.init(document.getElementById('ep1_06'));
        var sum_upw_chart = echarts.init(document.getElementById('ep2_06'));
        var sum_pn2_chart = echarts.init(document.getElementById('ep3_06'));
        var sum_cda_chart = echarts.init(document.getElementById('ep4_06'));
       
        
        option = {
        	   /*  tooltip: {
        	        trigger: 'item',
        	        formatter: "{a} <br/>{b}: {c} ({d}%)"
        	    }, */
        	    color:[/* '#D2691E', '#DAA520', */'#5F9EA0'],
        	   
        	    	
        	    series: [
        	    	{
        	    		itemStyle: {
        	    			opacity: 1,
        	    		},
        	    		color:['#272727'],
        	            type:'pie',
        	            radius: [0, '72%'],
        	            animation: false,
        	            label: {
        	                normal: {
        	                    position: 'inner'
        	                }
        	            },
        	            data:[
        	          
        	                {value:100}
        	            ]
        	        },
        	        {
        	            name:'来源',
        	            type:'pie',
        	            radius: ['72%', '90%'],
        	            avoidLabelOverlap: true,
        	            animation: false,
        	            silent: true,
        	            label: {
        	                normal: {
        	                    show: true,
        	                    position: 'center',
        	                  
        	                    textStyle: { 
									color: 'white',
									fontSize: '12'
								}
							},
							emphasis: {
								show: false,
								textStyle: {
									fontSize: '16',
									fontWeight: 'bold'
								}
							}
						},
						/* labelLine: {
							normal: {
        	                    show: true
							}
						}, */
						data:[
        	               	{value:0, name:' 0 '},
        	            	{value:1, name:'0%'},
        	               	
						]
					}
				]
			};

        // 使用刚指定的配置项和数据显示图表。
       	cell_power_chart.setOption(option);
        cell_upw_chart.setOption(option);
        cell_pn2_chart.setOption(option);
        cell_cda_chart.setOption(option);
        
        cf_power_chart.setOption(option);
        cf_upw_chart.setOption(option);
        cf_pn2_chart.setOption(option);
        cf_cda_chart.setOption(option);
        
        array_power_chart.setOption(option);
        array_upw_chart.setOption(option);
        array_pn2_chart.setOption(option);
        array_cda_chart.setOption(option);
        
        cub_power_chart.setOption(option);
        
        md_power_chart.setOption(option);
        cub_upw_chart.setOption(option);
        cub_pn2_chart.setOption(option);
        cub_cda_chart.setOption(option);
        
        sum_power_chart.setOption(option);
        sum_upw_chart.setOption(option);
      	sum_pn2_chart.setOption(option);
        sum_cda_chart.setOption(option);
       
    </script>

	<svg id="svg01" width="10" height="8">
    		<polygon id="p1"
			points="583.5927272727272,156.0420032310178,362.0436363636363,237.0638126009693,371.5,258.06946688206784,502.53818181818184,309.0831987075929,537.6618181818181,321.0864297253635,594.4,301.5811793214863,598.4527272727272,288.07754442649434,686.2618181818182,250.56744749596123,694.3672727272727,253.5682552504039,726.7890909090909,243.06542810985462,725.4381818181818,205.55533117932148,587.6454545454545,154.54159935379644"
			style="fill:#cccccc;
				stroke-width:1;opacity:0"
			onmouseover="mouseover('p1')" onmouseout="mouseout('p1')">
			</polygon>
			
			<polygon id="p2"
			points="330.97272727272724,241.56502423263328,174.26727272727274,288.07754442649434,158.05636363636364,309.0831987075929,389.06181818181824,408.10985460420034,563.329090909091,336.0904684975767,330.97272727272724,238.56421647819062"
			style="fill:#cccccc;
				stroke-width:1;opacity:0"
			onmouseover="mouseover('p2')" onmouseout="mouseout('p2')"> 
			</polygon>
			
			<polygon id="p3"
			points="152.65272727272728,307.58279483037154,144.54727272727274,342.0920840064621,385.0090909090909,450.1211631663974,407.9745454545454,426.1147011308562,561.9781818181818,358.5965266558966,561.9781818181818,334.5900646203554,386.36,411.11066235864297,156.70545454545456,303.0815831987076"
			style="fill:#cccccc;
				stroke-width:1;opacity:0"
			onmouseover="mouseover('p3')" onmouseout="mouseout('p3')"> 
			</polygon>
			
			<polygon id="p4"
			points="684.9109090909092,252.06785137318258,602.5054545454545,283.57633279483036,593.049090909091,303.0815831987076,556.5745454545455,324.08723747980616,564.68,333.0896607431341,567.3818181818182,351.09450726979,622.7690909090909,372.1001615508885,638.98,378.10177705977384,782.1763636363636,312.0840064620356,783.5272727272727,282.0759289176091,682.209090909091,250.56744749596123"
			style="fill:#cccccc;
				stroke-width:1;opacity:0"
			onmouseover="mouseover('p4')" onmouseout="mouseout('p4')">
			
			</polygon>
			<polygon id="p5"
			points="575.4872727272727,364.5981421647819,429.5890909090909,426.1147011308562,409.32545454545453,444.11954765751216,406.62363636363636,472.6272213247173,741.6490909090909,622.6676090468497,745.7018181818182,592.6595315024233,751.1054545454546,583.6571082390952,869.9854545454546,510.13731825525036,875.3890909090909,483.13004846526655,574.1363636363636,361.5973344103393"
			style="fill:#cccccc;
				stroke-width:1;opacity:0"
			onmouseover="mouseover('p5')" onmouseout="mouseout('p5')">
			</polygon>
			
			<polyline id="p6" points="20,220,450,220,480,340"
			style="fill:none;stroke:#32CD32;stroke-width:1;opacity:0.6;" />
			<polyline id="p7" points="20,600,450,600,480,500"
			style="fill:none;stroke:#32CD32;stroke-width:1;opacity:0.6;" />
			<polyline id="p8" points="800,560,880,720,1400,720 "
			style="fill:none;stroke:#32CD32;stroke-width:1;opacity:0.6;" />
			<polyline id="p9" points="800,380,880,480,1400,480 "
			style="fill:none;stroke:#32CD32;stroke-width:1;opacity:0.6;" />
			<polyline id="p10" points="800,280,880,240,1400,240"
			style="fill:none;stroke:#32CD32;stroke-width:1;opacity:0.6;" />
		</svg>

</body>

<%-- <%=my_laydate_css%> --%>

</html>