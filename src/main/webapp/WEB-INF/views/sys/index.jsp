<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/inc/taglibs.jsp"%>
<!DOCTYPE html>
<html>

<head>
    <%@ include file="../gynh/common/def.jsp" %>
	<meta charset="utf-8">
	<title>BOE-EMS</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link rel="shortcut icon" href="${path }/res/images/Ologo.ico" type="image/x-icon" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	
	<link rel="stylesheet" href="${path}/res/layui/css/layui.css" media="all" >
	<link rel="stylesheet" href="${path}/res/font-awesome-4.7.0/css/font-awesome.css" />
	<link rel="stylesheet" href="${path}/res/css/one-css/layout.css" media="all" />
	<script src="${path}/res/js/my-js/jquery-3.2.1.min.js"></script>
	<link rel="stylesheet" type="text/css" href="${path}/res/css/my-css/dx.common.css" />
	<link rel="dx-theme" data-theme="generic.light" href="${path}/res/css/my-css/dx.light.css" />
	<script src="${path}/res/js/my-js/dx.all.js"></script>
	<script src="${path}/res/js/my-js/index.js"></script>
	
	<script type="text/javascript">
		$(function() {
			$("#divFloat").click(function() {
				var sideWidth = $('.beg-layout-side').width();
				if (sideWidth === 250) {
					$('.beg-layout-body').animate({
						left: '0'
					});
					$('.beg-layout-footer').animate({
						left: '0'
					});
					$('.beg-layout-side').animate({
						width: '0'
					});
					$("#logo_div").animate({
						width : '0'
					});
					$("#divFloat").animate({
						left : '0'
					});
					$("#divFloat").html('>>');
	
				} else {
					$('.beg-layout-body').animate({
						left: '250px'
					});
					$('.beg-layout-footer').animate({
						left: '250px'
					});
					$('.beg-layout-side').animate({
						width: '250px'
					});
					$('#logo_div').animate({
						width: '250px'
					});
					$("#divFloat").animate({
						left : '235'
					});
					$("#divFloat").html('<<');
	
				}
				
				if ($("#divFloat").offset().left == 0) {
					//$("#divFloat").css({'left':'200px'});
				} else {
					//$("#divFloat").css({'left':'0px'});
				}
	
			});
			
		})
		
		window.onload=function(){
			
			var power=document.getElementById('power').value;
			if(power==5){
 				$("#pandect").hide();
			}

		}
		
	</script>
	
	<style type="text/css">
		#simple-treeview, #product-details {
			display: inline-block;
		}
	
		#product-details {
			vertical-align: top;
			width: 400px;
			height: 420px;
			margin-left: 20px;
		}
	
		.dx-item-content span{
			color:#fff;
		}
		.dark #product-details > div {
			color: #f0f0f0;
		}
	
		.hidden {
			visibility: hidden;
		}
	</style>
</head>

<body>
	<div class="layui-layout layui-layout-admin beg-layout-container ">
		<!--侧边导航栏>
		<div class="layui-side beg-layout-side">
			<div class="layui-side-scroll"-->
		<input id="power" type="hidden" value=${user.roleId}>
		<div class="one-logo" id="logo_div" style="height:48px; width:250px; z-index:90; background-color:#393D49; position: fixed; left: 0px; top: 0px;">
			&nbsp;<img src="${path}/res/images/login/login_02.png" style="width:6yyyypx; height:28px;">
			<ul class="layui-nav beg-layout-nav" lay-filter="user" style="float:right;">
				<li class="layui-nav-item" style="height:40px;line-height: 40px;">
					<a href="javascript:;" class="beg-layou-head" style="height:40px; margin-right: 24px;"> <span>${user.loginName} &nbsp;</span>
					</a>
					<dl class="layui-nav-child">
						<dd>
							<a href="javascript:;" data-tab="true"
								data-url='${path}/user/personInfo.do'> <i
								class="fa fa-user-circle" aria-hidden="true"></i> <cite>个人信息</cite>
							</a>
						</dd>
						<dd>
							<a href="javascript:;" data-tab="true"
								data-url="${path }/user/pwd.do"> <i class="fa fa-gear"
								aria-hidden="true"></i> <cite>修改密码</cite>
							</a>
						</dd>
						<dd>
							<a id="logout" href="javascript:;"> <i class="fa fa-sign-out"
								aria-hidden="true"></i> <cite>注销</cite>
							</a>
						</dd>
					</dl>
				</li>
			</ul>
		</div>
		<div class="layui-side beg-layout-side"  style="margin-top: 48px; position: absolute; z-index: 10;">
			<div class="layui-side-scroll">
				<div class="demo-container">
					<div class="form">
						<div id="simple-treeview"></div>
					</div>
				</div>

				<!-- 一级菜单 -- >
				<div class="beg-layout-main beg-layout-menu" id="menu">
					<ul class="layui-nav beg-layout-nav" lay-filter="" >
					</ul>
				</div-->
				
				<!-- 管理员信息 -->
				<div class="user-info" style="height:40px;">
				</div>
				<ul class="layui-nav layui-nav-tree" lay-filter="side" id="side">
				</ul>
			</div>
		</div>
		<!--内容区域-->
		<div class="layui-body beg-layout-body <%=layui_bg_class%>" style="overflow:hidden;">
			<div class="layui-tab layui-tab-brief layout-nav-card"
				lay-filter="layout-tab"
				style="border: 0; margin: 0; box-shadow: none; height: 100%;">
				<ul class="layui-tab-title <%=layui_bg_class%>">
					<li id="pandect" class="layui-this"><a href="javascript:;"> <i
							class="fa fa-home" aria-hidden="true"></i> <cite style="color:<%=sum_tab_text_color%>;">总览</cite>
					</a></li>
				</ul>
				<div class="layui-tab-content">
					<div class="layui-tab-item layui-show">
						<iframe src="${path }/index/main.do"></iframe>
					</div>
				</div>
			</div>
		</div>
		<!--页脚-- >
		<div class="layui-footer beg-layout-footer">
			<p>Powered by obextract.</p>
		</div-->
	</div>

	<div id="divFloat" style="cursor:pointer; z-index: 1000; line-height:41px;  font-size:10px; text-align:center; background-color: #393D49; color:white; width: 15px; height: 40px; position: fixed; left: 235px; top: 0px;"><<</div>
	
	<script src="${path}/res/layui_menu/layui.js"></script>
	<script src="${path}/res/js/layout.js "></script>
	<script type="text/javascript">
		var path = "${path}";
	</script>

</body>
</html>