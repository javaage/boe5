<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/inc/taglibs.jsp"%>
<!DOCTYPE html>

<html>

	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<link rel="shortcut icon" href="${path }/res/images/Ologo.ico" type="image/x-icon" /> 
		<title>BOE工艺能耗管理平台-登录页面</title>
		<link rel="stylesheet" href="${path}/res/layui/css/layui.css" media="all" >
	  	<link rel="stylesheet" href="${path}/res/font-awesome-4.7.0/css/font-awesome.css" />
	  	<link rel="stylesheet" href="${path}/res/css/one-css/login.css" media="all" />
	  	<script src='${path}/res/js/my-js/jquery-3.1.0.min.js'></script>
		<script src="${path}/res/js/my-js/jquery-3.2.1.min.js"></script>
	  	<script type="text/javascript">
	  	
		window.onload=function(){
			
			login();
		}
		window.onresize = function() {
		  		
			login();
		}
		function login()
		{
			var windowHeight = $(window).height();
			
		   	var windowWidth = $(window).width();
		   	
		   	var width=(windowWidth-465)/1080;
			var height=(windowHeight)/800;
			
		    document.getElementById('main_font01').style.marginTop = 500*height-160+'px' ;
		    document.getElementById('main').style.marginLeft = 366*width+'px' ;
		    document.getElementById('left').style.backgroundSize =465+'px'+' '+windowHeight+'px';
		    document.getElementById('left').style.height=windowHeight+'px';
		}
	  	</script>
	<style>
		body{
			width: 1600px;
			height: 100%;
			float: left;
			overflow:hidden;
			
		}
		#left{
			width: 465px;
			height: 800px;
			float: left;
			margin-left: 0px;
			margin-top: 0px;
			background-image: url(${path}/res/images/login/login_01.jpg);
		}
		.left_image{
			width: 100px;
			height: 50px;
			float: left;
			margin-left: 180px;
			margin-top: 280px;
		}
		.left_image img{ width:100px; height:50px}
		
		.left_font{
			width: 300px;
			height: 50px;
			float: left;
			margin-left: 100px;
			margin-top: 20px;
			font-size:28px;
			color:white;
		}
		#main{
			width: 300px;
			height: 800px;
			float: left;
			margin-left: 300px;
			margin-top: 0px;
		}
		.main_font01{
			width: 300px;
			height: 50px;
			float: left;
			margin-left: 6px;
			margin-top: 320px;
			font-size:16px;
			color:#9999A6;
		}
		.main_font02{
			width: 72px;
			height: 50px;
			float: left;
			margin-left: 6px;
			margin-top: 24px;
			font-size:36px;
			color:#019BCC;
		}
		.main_font03{
			width: 72px;
			height: 50px;
			float: left;
			margin-left: 0px;
			margin-top: 24px;
			font-weight:bold;
			font-size:36px;
			color:#019BCC;
		}
		.main_image{
			width: 75px;
			height: 75px;
			float: left;
			margin-left: 0px;
			margin-top:  20px;
		}
		.main_image img{ width:60px; height:60px}
		.main_from{
			width: 160px;
			height: 50px;
			float: left;
			margin-left: 6px;
			margin-top: 8px;
			font-size:16px;
		}
	
</style>
	</head>
	<body >
	
		<div id="left" >
			<div class="left_image" >
				<img src="${path}/res/images/login/login_02.png">
			</div>
			<div class="left_font">
				京东方能源管理系统
			</div>
		</div>
		<div id="main">
			<div id="main_font01" class="main_font01">
				USER LOGIN
			</div>
			<div class="main_font02">
				用户
			</div>
			<div class="main_font03">
				登录
			</div>
			<div class="main_image" >
				<img src="${path}/res/images/login/login_03.png">
			</div>
			<div class="main_from">
			<form action="" class="layui-form" method="post">
					
					<div class="layui-form-item" style="margin-top:3px">
						<input type="text" name="name" lay-verify="required" autocomplete="off" placeholder="这里输入登录名" class="layui-input"  style="width:300px;">
					</div>
					<div class="layui-form-item">
						<input type="password" name="pwd" required lay-verify="required|password" autocomplete="off" placeholder="这里输入密码" class="layui-input" style="width:300px;">
					</div>
					<div class="layui-form-item">
						
						<div class="beg-pull-right">
							<button class="layui-btn " lay-submit lay-filter="submit" style="width:300px;">
                            <i class="layui-icon">&#xe609;</i> 登录
                        </button>
						</div>
					</div>
				</form>
				</div>
			
		</div>
<script src="${path}/res/layui/layui.js"></script>
<script src="${path}/res/js/one-js/login.js"></script>
<script type="text/javascript">
var path = "${path}";
</script>
</body>
</html>