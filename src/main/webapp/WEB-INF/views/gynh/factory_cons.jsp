<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/inc/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
 	<%@ include file="./common/def.jsp" %>
 	<script src="${path}/res/js/my-js/jquery.min.js"></script>
    <script src='${path}/res/js/my-js/default_values.js'></script> 
    <script src='${path}/res/js/my-js/factory_cons.js'></script>
    <script src="${path}/res/js/my-js/util.js"></script>    
    <script src="${path}/res/highcharts/highcharts.js"></script>
    <script src="${path}/res/highcharts/modules/exporting.js"></script>
    <script src="https://img.highcharts.com.cn/highcharts-plugins/highcharts-zh_CN.js"></script>
   
    
    <link rel="stylesheet" href="${path}/res/css/public.css" media="all" />

    <link rel='stylesheet' type='text/css' href='${path}/res/css/my-css/dx.spa.css' /> 
    <link rel='stylesheet' type='text/css' href='${path}/res/css/my-css/dx.common.css' /> 
    <link rel='dx-theme' data-theme='generic.<%=theme%>' href='${path}/res/css/my-css/dx.<%=theme%>.css' />
    <link rel="stylesheet" href="${path}/res/css/my-css/jqx.base.css" type="text/css" />
    
    <script src="${path}/res/js/my-js/jqxcore.js"></script>
    <script src="${path}/res/js/my-js/jqxsplitter.js"></script>
    <script src='${path}/res/js/my-js/jszip.min.js'></script> 
    <script src='${path}/res/js/my-js/dx.all.js'></script> 
    <script src="${path}/res/js/echarts/echarts.min.js"></script>
    
    <link rel="stylesheet" type ='text/css' href="${path}/res/css/public-css/css/font-awesome.min.css">
    <link rel="stylesheet" type ='text/css' href="${path}/res/css/public-css/css/jquery.toolbar.css">
    <script src="${path}/res/js/my-js/viewer.js"></script>
    <link rel="stylesheet" type ='text/css' href="${path}/res/css/my-css/viewer.css">
    <link rel='stylesheet' type='text/css' href='${path}/res/css/my-css/app.min.css' /> 
    <link rel="stylesheet" href="${path}/res/layui/css/layui.css" media="all" >
    <script src='${path}/res/js/my-js/dx.all.js'></script> 
    <script src="${path}/res/layui/layui.js"></script>
    <link rel="stylesheet" href="${path}/res/css/my-css/swiper.min.css">
    <script type="text/javascript">
        var path = "${path}";
    </script>
    <script>

    var param1 = "${export_name}";
	var factory_list = ${factory_name};
	var pageName = 'factory_chart_view_'+factory_list;
	var start_date = getFormatDate(-7);
	var end_date = getFormatDate(-1);
	
	var start_date = '2019-01-01';
	var end_date = '2019-01-07';
	
	var init_begin_date = getTimeDate(-1);
	var init_end_date = getTimeDate(0);
	
	
	
	layui.use('laydate', function(){
		var laydate = layui.laydate;
		laydate.render({
		    elem: '#beginTimeBox',
		    type: 'datetime',
		    done: function(value) {
				init_begin_date = value;
				//point_value_cache = {};
				//inst_point_value_cache = {};
			}
		  });
		laydate.render({
		    elem: '#endTimeBox',
			type: 'datetime',
		    done: function(value) {
				init_end_date = value;
				//point_value_cache = {};
				//inst_point_value_cache = {};
			}
		  });
		laydate.render({
		   elem: '#date',
		   range: true,
		   done: function(value) {
			 /*   begintime=value.substr(0,10)+" 00:00:00";
			   endtime=value.substr(13,10)+" 00:00:00";
			   begintime=bjtime_to_gmt(begintime);
			   endtime=bjtime_to_gmt(endtime); */
			}
		});	
	});
	
	if(ajax_json){
		var factory_cons1_ajax_url=path + '/res/json/factory_cons1.json';
    	var factory_cons2_ajax_url=path + '/res/json/factory_cons2.json';
    	var factory_cons3_ajax_url=path + '/res/json/factory_cons3.json';
    	var factory_cons4_ajax_url=path + '/res/json/factory_cons4.json';
    	var factory_cons5_ajax_url=path + '/res/json/factory_cons5.json';
    	var factory_cons6_ajax_url=path + '/res/json/factory_cons6.json';
    	var factory_cons7_ajax_url=path + '/res/json/factory_cons7.json';
    	var factory_cons8_ajax_url=path + '/res/json/factory_cons8.json';
    	var factory_cons9_ajax_url=path + '/res/json/factory_cons9.json';
    	var factory_cons10_ajax_url=path + '/res/json/factory_cons10.json';
    	var factory_cons11_ajax_url=path + '/res/json/factory_cons11.json';
    	var factory_cons12_ajax_url=path + '/res/json/factory_cons12.json';
    	var factory_cons13_ajax_url=path + '/res/json/factory_cons13.json';
    	var factory_cons14_ajax_url=path + '/res/json/factory_cons14.json';
    	var factory_cons15_ajax_url=path + '/res/json/factory_cons15.json';
    	var factory_cons16_ajax_url=path + '/res/json/factory_cons16.json';
    	var export1_ajax_url=path + '/res/json/export1.json';
    	var export2_ajax_url=path + '/res/json/export2.json';
    	var export3_ajax_url=path + '/res/json/export3.json';
    	var export4_ajax_url=path + '/res/json/export4.json';
    }
	
</script>
	<style>
		html, body {
			position: relative;
			height: 100%;
		}
		body {
			font-size: 14px;
			color: #000;
			background: #dddddd70;
		}
		
		.swiper-container {
			width: 100%;
			height: 64.00%;
			overflow: hidden;
		}
		
		.swiper-slide {
			text-align: left;
			font-size: 18px;
			display: -webkit-box;
			display: -ms-flexbox;
			display: -webkit-flex;
			display: flex;
			-webkit-box-pack: center;
			-ms-flex-pack: center;
			-webkit-justify-content: center;
			justify-content: center;
			-webkit-box-align: center;
			-ms-flex-align: center;
			-webkit-align-items: center;
			align-items: center;
		}
		
		.swiper-slide {
			display: flex;
			align-items: center;
			justify-content: center;
		}
		
		.swiper-slide>div {
			width: 100%;
			height: inherit;
			box-sizing: border-box;
			background: #dddddd70;
		}
		
		.chart_div {
			padding: 0 12px;
			height: 68%;
		}
		
		.chart_div>div {
			box-sizing: border-box;
			height: 50%;
			 box-shadow: 2px 2px 2px 2px #2a1c1c8f;
		}
		
		.chart_div>div:nth-of-type(1) {
			margin-bottom: 5px;
			height: 97%;
		    margin-top: 5px;
		    display: block;
		    background: #fff;
		}
		
		.chart_div>div:nth-of-type(2) {
			display: none;
		}
		
		.divColse{
			height: 100%;
		    background: #fff;
		}
		
		.footer {
			width: 100%;
			height: 28%;
			box-sizing: border-box;
			display: flex;
			justify-content: center;
			align-items: center;
		}
		
		.footer>div {
		    width: 49%;
		    height: 94%;
		    margin: 12px;
		    margin-left: 12px;
		   	box-sizing: border-box;
		    background: #fff;
		    text-align: center;
		    box-shadow: 2px 2px 2px 2px #2a1c1c8f;
		}
		
		.posirelative {
			position: relative;
		}
		
		.select-out-div {
			width: 160px;
			overflow: hidden;
		}
		
		select.m-wrap {
			background-color: #ffffff;
			background-image: none !important;
			filter: none !important;
			border: 1px solid #e5e5e5;
			outline: none;
			height: 25px !important;
			line-height: 25px;
		}
		
		.select-hide-span {
			height: 25px;
			position: absolute;
			top: 0;
			border-right: 1px solid #e5e5e5;
			right: 0;
			width: 20px !important;
			z-index: 999;
		}
		
		.select-show-b {
			border-color: #888 transparent transparent transparent;
			border-style: solid;
			border-width: 5px 4px 0 4px;
			margin-left: -4px;
			margin-top: 10px;
			position: absolute;
		}
		
    </style>
</head>
<body>

	<div style="height:48px;background-color: #ffffff; box-shadow: 2px 2px 2px 2px #2a1c1c8f; margin-top:12px; margin-bottom: 12px;margin-left: 12px; margin-right: 12px">
		<div class="layui-btn-group" style=" float:left;margin-left:120px; margin-top:5px;">
			<button class="layui-btn" onclick="set_energy('Power')" >Power</button>
		    <button class="layui-btn" onclick="set_energy('UPW')" >UPW</button>
		    <button class="layui-btn" onclick="set_energy('PN2')">PN2</button>
		    <button class="layui-btn" onclick="set_energy('CDA')">CDA</button>
		</div>
		<div class="layui-btn-group" style=" float:left;margin-left:60px;margin-top:5px;">
		    <button class="layui-btn" onclick="one_month()" style="margin-left:60px">近30天</button>
		    <button class="layui-btn" onclick="one_week()">近一周</button>
		    <button class="layui-btn" onclick="yesterday()">昨日</button>
		    <button class="layui-btn" onclick="custom()">自定义跨度</button>
		</div>
		<input type="text" class="layui-input" id="date" placeholder=" - " style=" float:left; margin-left:20px;width:175px;display: none; margin-top:5px;">
	</div>
	
	<div class="swiper-container">
	    <div class="swiper-wrapper">
	    	  <div class="swiper-slide">
	            <div class="chart_div">
	                <div class="day_div" >
	                	<div style="height:48px;background-color: #ffffff; box-shadow: 2px 2px 2px 2px #2a1c1c8f; margin-top:8px; margin-bottom: 12px;margin-left: 0px; margin-right: 0px">
							<div style="float:left; margin-top: 12px; margin-left: 20px">科室:</div>
							<div id = 'department_box' style="width:80%;height:36px;float:left;margin-top:6px;margin-left:6px;"></div>
							<button class="layui-btn" onclick='set_department_chart()'style="margin-left:6px; margin-top:5px;">加载</button>
						</div>
						<button class="layui-btn layui-btn-xs" onclick="set_export_cons('department')" style="position: absolute; z-index: 101;">导出</button>
						   
						<div id="department_chart" style="width:100%;height:500px;"> </div>
	                </div>
	                <div class="hour_div" >
	                </div>
	            </div>
	        </div>
	        <div class="swiper-slide">
	            <div class="chart_div">
	                <div class="day_div" >
	                	<div style="height:48px;background-color: #ffffff; box-shadow: 2px 2px 2px 2px #2a1c1c8f; margin-top:8px; margin-bottom: 12px;margin-left: 0px; margin-right: 0px">
							<div style="float:left; margin-top: 12px; margin-left: 20px">产线:</div>
							<div id = 'line_box' style="width:80%;height:36px;float:left;margin-top:6px;margin-left:6px;"></div>
							<button class="layui-btn" onclick='set_product_line_chart()' style="margin-left:6px; margin-top:5px;">加载</button>
						</div>
						<button class="layui-btn layui-btn-xs" onclick="set_export_cons('product_line')" style="position: absolute; z-index: 101;">导出</button>
						<div class="layui-btn-group" style="left:30%;position: absolute; z-index: 101;">
							<button class="layui-btn layui-btn-xs" id='button0' onclick="set_product_line_select('能耗',0)" >能耗</button>
						    <button class="layui-btn layui-btn-xs" id='button1' onclick="set_product_line_select('产量',1)" >产量</button>
						    <button class="layui-btn layui-btn-xs" id='button2' onclick="set_product_line_select('单耗',2)">单耗</button>
						    <button class="layui-btn layui-btn-xs" id='button3' onclick="set_product_line_select('状态',3)">状态</button>
						</div>
						<div class="layui-btn-group" id='product_line_chart_product_line' style="left:30%;top:92%;position: absolute; z-index: 101;">
							
						</div>
						<div id="product_line_chart" style="width:100%;height:500px;"> 
						</div>
						
	                </div>
	                <div class="hour_div" >
	                
						<button class="layui-btn" onclick="close_day(1)" style="left:1%;bottom:2px;position: absolute; z-index: 101;">X</button>
						<button class="layui-btn layui-btn-xs" onclick="set_export_cons('product_line_hourly')" style="position: absolute; z-index: 101;">导出</button>
	                	<div class="layui-btn-group" style="left:30%;position: absolute; z-index: 101;">
							<button class="layui-btn layui-btn-xs" id='button4' onclick="set_product_line_hourly_select('能耗',4)" >能耗</button>
						    <button class="layui-btn layui-btn-xs" id='button5' onclick="set_product_line_hourly_select('产量',5)" >产量</button>
						    <button class="layui-btn layui-btn-xs" id='button6' onclick="set_product_line_hourly_select('单耗',6)">单耗</button>
						</div>
						<div class="layui-btn-group" id='product_line_chart_product_line_hourly' style="left:30%;bottom:2px;position: absolute; z-index: 101;">
							
						</div>
	                	<div id="product_line_chart_hourly" style="width:100%;height:500px;"> </div>
	                	
	                </div>
	            </div>
	        </div>
	        <div class="swiper-slide">
	            <div class="chart_div">
	                <div class="day_div"">
	                	<div style="height:48px;background-color: #ffffff; box-shadow: 2px 2px 2px 2px #2a1c1c8f; margin-top:8px; margin-bottom: 12px;margin-left: 0px; margin-right: 0px">
							<div style="float:left; margin-top: 12px; margin-left: 20px">设备:</div>
		                	<div id = 'device_box' style="width:80%;float:left;margin-top:6px;margin-left:6px;"></div>
		                	<button class="layui-btn" onclick='set_device_chart()' style="margin-left:6px; margin-top:5px;">加载</button>
						</div>
	                	<button class="layui-btn layui-btn-xs" onclick="set_export_cons('device')" style="position: absolute; z-index: 101;">导出</button>
	                	<div class="layui-btn-group" style="left:30%;position: absolute; z-index: 101;">
							<button class="layui-btn layui-btn-xs" id='button7' onclick="set_device_select('能耗',7)" >能耗</button>
						    <button class="layui-btn layui-btn-xs" id='button8' onclick="set_device_select('产量',8)" >产量</button>
						    <button class="layui-btn layui-btn-xs" id='button9' onclick="set_device_select('单耗',9)">单耗</button>
						    <button class="layui-btn layui-btn-xs" id='button10' onclick="set_device_select('状态',10)">状态</button>
						</div>
						<div class="layui-btn-group" id='device_chart_device' style="left:30%;top:92%;position: absolute; z-index: 101;">
							
						</div>
	                	<div id="device_chart" style="width:100%;height:500px;"> </div>
	                </div>
	                <div class="hour_div" >
	                	<button class="layui-btn" onclick="close_day(2)" style="left:1%;bottom:2px;position: absolute; z-index: 101;">X</button>
	                	<button class="layui-btn layui-btn-xs" onclick="set_export_cons('device_hourly')" style="position: absolute; z-index: 101;">导出</button>
	                	<div class="layui-btn-group" style="left:30%;position: absolute; z-index: 101;">
							<button class="layui-btn layui-btn-xs" id='button11' onclick="set_device_hourly_select('能耗',11)" >能耗</button>
						    <button class="layui-btn layui-btn-xs" id='button12' onclick="set_device_hourly_select('产量',12)" >产量</button>
						    <button class="layui-btn layui-btn-xs" id='button13' onclick="set_device_hourly_select('单耗',13)">单耗</button>
						</div>
						<div class="layui-btn-group" id='device_chart_device_hourly' style="left:30%;bottom:2px;position: absolute; z-index: 101;">
							
						</div>
	                	<div id="device_chart_hourly" style="width:100%;height:500px;"> </div>
	                </div>
	            </div>
	        </div>
	        <div class="swiper-slide">
	            <div class="chart_div">
	                <div class="day_div">
	                	<div style="height:48px;background-color: #ffffff; box-shadow: 2px 2px 2px 2px #2a1c1c8f; margin-top:8px; margin-bottom: 12px;margin-left: 0px; margin-right: 0px">
							<div style="float:left; margin-top: 12px; margin-left: 20px">单元:</div>
	                		<div id = 'device_unit_box' style="width:80%;float:left;margin-top:6px;margin-left:6px;"></div>
	                		<button class="layui-btn" onclick='set_device_unit_chart()' style="margin-left:6px; margin-top:5px;">加载</button>
						</div>
	                	<button class="layui-btn layui-btn-xs" onclick="set_export_cons('device_unit')" style="position: absolute; z-index: 101;">导出</button>
	                	<div class="layui-btn-group" style="left:30%;position: absolute; z-index: 101;">
							<button class="layui-btn layui-btn-xs" id='button14' onclick="set_device_unit_select('能耗',14)" >能耗</button>
						    <button class="layui-btn layui-btn-xs" id='button15' onclick="set_device_unit_select('产量',15)" >产量</button>
						    <button class="layui-btn layui-btn-xs" id='button16' onclick="set_device_unit_select('单耗',16)">单耗</button>
						    <button class="layui-btn layui-btn-xs" id='button17' onclick="set_device_unit_select('状态',17)">状态</button>
						</div>
						<div class="layui-btn-group" id='device_unit_chart_device_unit' style="left:30%;top:92%;position: absolute; z-index: 101;">
							
						</div>
	                	<div id="device_unit_chart" style="width:100%;height:500px;"> </div>
	                </div>
	                <div class="hour_div">
	                	<button class="layui-btn" onclick="close_day(3)" style="left:1%;bottom:2px;position: absolute; z-index: 101;">X</button>
	                	<button class="layui-btn layui-btn-xs" onclick="set_export_cons('device_unit_hourly')" style="position: absolute; z-index: 101;">导出</button>
	                	<div class="layui-btn-group" style="left:30%;position: absolute; z-index: 101;">
							<button class="layui-btn layui-btn-xs" id='button18' onclick="set_device_unit_hourly_select('能耗',18)" >能耗</button>
						    <button class="layui-btn layui-btn-xs" id='button19' onclick="set_device_unit_hourly_select('产量',19)" >产量</button>
						    <button class="layui-btn layui-btn-xs" id='button20' onclick="set_device_unit_hourly_select('单耗',20)">单耗</button>
						</div>
						<div class="layui-btn-group" id='device_unit_chart_device_unit_hourly' style="left:30%;bottom:2px;position: absolute; z-index: 101;">
							
						</div>
	                	<div id="device_unit_chart_hourly" style="width:100%;height:500px;"> </div>
	                </div>
	            </div>
	        </div>
	    </div>
	    <!--Add Arrows -->
	    <div class="swiper-button-next"></div>
	    <div class="swiper-button-prev"></div>
	</div>
	<div class="footer">
	    <div>
			<div style="background:white; float:left; margin-left:12px;margin-top:10px;width:480px;height:37px; position: absolute; z-index: 95;">
				<input class='toolbar_btn' type="image" title="加载" onclick="get_echarts_value()" src="${path}/res/images/toolbarbtn/curve.png"
	       			style=" float:left; width:28px;height:28px; margin-left: 12px;"/>	
				<input class='toolbar_btn' type="image" title="导出" onclick="get_points_value()" src="${path}/res/images/toolbarbtn/export_excel.png"
	       			style=" float:left; width:28px;height:28px; margin-left: 12px;"/>
				<input class='toolbar_btn' type="image" title="刷新点位最新值" onclick="get_total_points_value()" src="${path}/res/images/toolbarbtn/refresh.png"
	       			style=" float:left; width:28px;height:28px; margin-left: 12px;"/>
	       		<div class="layui-btn-group" style="margin-left:12px">
					<button class="layui-btn" id="factory_points_button" >分厂累计量点位</button>
			 		<button class="layui-btn" id="factory_inst_points_button">分厂瞬时量点位</button>
				</div>
	       		
			</div>
			<div class="demo-container" style="float:left;margin-left:1px;position: absolute; z-index: 90;">
			    <div id="factory_points_grid" style="position: absolute; z-index: 12;left:10px;top:10px;width:100px;height:240px;"></div>
			    <div id="factory_inst_points_grid" style="position: absolute; z-index: 12;left:10px;top:10px;width:100px;height:240px;"></div>	    	    
			</div>
		</div>
	    <div>
	    	<div>
				<div id="left_div" style="float:left; position: absolute; z-index: 101; margin-left: 14px;width:120px;">
					<button class="layui-btn" id="begin_hour_add" style="margin-left: 0px;">+</button>
			 		<button class="layui-btn" id="begin_hour_minus" style="margin-left: 12px;">-</button>
		        	<input type="text" class="layui-input" id="beginTimeBox" placeholder="yyyy-MM-dd HH:mm:ss" style="width:160px" >
		      	</div>
		      	<div id="right_div"style="float:left; position: absolute; z-index: 101;width:120px;">
		      		<button class="layui-btn" id="end_hour_add" style="clear:left;float:right;margin-right:0px;">+</button>
			 		<button class="layui-btn" id="end_hour_minus" style="clear:left;float:right;margin-right:0px;">-</button>
		        	<input type="text" class="layui-input" id="endTimeBox" placeholder="yyyy-MM-dd HH:mm:ss" style="width:160px">
		      	</div>
				<div id="factory_points_echarts" style="position: absolute; z-index: 11;margin-left:10px;margin-top:12px;width:100%;height:320px;;background-color:#FFFFFF"></div>
				<div id="factory_inst_points_echarts" style="position: absolute; z-index: 12;margin-left:10px;margin-top:12px;width:100%;height:320px;background-color:#FFFFFF"></div>
			</div>	 
	    </div>
	</div>
    <script src="${path}/res/js/my-js/swiper.min.js"></script>
     <script>
    
    var swiper = new Swiper('.swiper-container', {
        slidesPerView: 2,
         spaceBetween: 0,
        loopFillGroupWithBlank: true,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });
   
    function open_day(index) {
    	var chart_div = document.getElementsByClassName('chart_div');
    	var day_div = document.getElementsByClassName('day_div');
    	var hour_div = document.getElementsByClassName('hour_div');
   
        day_div[index].style.height = chart_div[index].offsetHeight/2+28-12+'px';
        hour_div[index].style.height = chart_div[index].offsetHeight/2-28-8+'px';
       
        if(index==1) {
        	document.getElementById('product_line_chart').style.height = day_div[index].offsetHeight-60+'px';
            document.getElementById('product_line_chart_hourly').style.height = '100%';
    	    document.getElementById('product_line_chart_product_line').style.top = '44%';
    	    product_line_chart.reflow();
    	    product_line_chart_hourly.reflow(); 
           
        }if(index==2) {
        	document.getElementById('device_chart').style.height = day_div[index].offsetHeight-60+'px';
            document.getElementById('device_chart_hourly').style.height = '100%';
    	    document.getElementById('device_chart_device').style.top = '44%';
    	    device_chart.reflow();
    	    device_chart_hourly.reflow();
        }if(index==3) {
        	document.getElementById('device_unit_chart').style.height= day_div[index].offsetHeight-60+'px';
            document.getElementById('device_unit_chart_hourly').style.height = '100%';
    	    document.getElementById('device_unit_chart_device_unit').style.top = '44%';
    	    device_unit_chart.reflow();
    	    device_unit_chart_hourly.reflow();
    	    
        }
        hour_div[index].style.display = 'block';
    }
    function close_day(index) {
    	
    	var chart_div = document.getElementsByClassName('chart_div');
    	var day_div = document.getElementsByClassName('day_div');
    	var hour_div = document.getElementsByClassName('hour_div');
    	
    	day_div[index].style.height = chart_div[index].offsetHeight-12+'px';
        hour_div[index].style.display = 'none';
      
        if(index==0) {
        	document.getElementById('product_line_chart_product_line').style.top = '92%';
        	
        }if(index==1) {
        	document.getElementById('product_line_chart').style.height = day_div[index].offsetHeight-60+'px';
        	document.getElementById('product_line_chart_product_line').style.top = '92%';
        	product_line_chart.reflow();
        	product_line_chart_hourly.reflow(); 
        }if(index==2) {
        	document.getElementById('device_chart').style.height = day_div[index].offsetHeight-60+'px';
     	  	document.getElementById('device_chart_device').style.top = '92%';
     	   	device_chart.reflow();
     	  	device_chart_hourly.reflow();
        }if(index==3) {
        	document.getElementById('device_unit_chart').style.height = day_div[index].offsetHeight-60+'px';
     	    document.getElementById('device_unit_chart_device_unit').style.top = '92%';
     	   	device_unit_chart.reflow();
           	device_unit_chart_hourly.reflow();
        }
        
    }

</script>

<script type="text/javascript">
var product_line_option={
		xAxis: {
			categories: [
			]
		},
		chart:{
			marginTop: 60,
			marginLeft: 60,
			marginRight: 150,
			marginBottom: 60,
		},
		credits: {
			enabled: false,
		},
		title:{
			text: ''
		},
		yAxis: [{
			min: 0,
			title: {
				text: '能耗',
				offset: 0,
				rotation: 0,
				y: -10,
				align: 'high',
			}
		}, {
			title: {
				text: '产量',
				offset: 0,
				rotation: 0,
				y: -10,
				align: 'high',
			},
			opposite: true
		}, {
			title: {
				text: '单耗',
				offset: 0,
				rotation: 0,
				y: -10,
				align: 'high',
			},
			opposite: true
		}],
		legend: {
			shadow: false,
			enabled: false,
		},
		tooltip: {
			shared: true
		},
		plotOptions: {
			column: {
				grouping: false,
				shadow: false,
				borderWidth: 0
			},    
			series: {
				animation:false,
			}
		},
		series: []
	};
var department_option={
		xAxis: {
			categories: [
			]
		},
		credits: {
			enabled: false,
		},
		title:{
			text: ''
		},
		yAxis: [{
			min: 0,
			title: {
				text: '能耗',
				offset: 0,
				rotation: 0,
				y: -10,
				align: 'high',
			}
		}],
		chart: {
			animation:false,
			marginTop: 60,
			marginBottom: 60,
			marginLeft: 60,
			marginRight: 60,
		},
		
		legend: {
			shadow: false,
			enabled: false,
		},
		tooltip: {
			shared: true
		},
		
		series: []
	};
var device_option=JSON.parse(JSON.stringify(product_line_option));
var device_unit_option=JSON.parse(JSON.stringify(product_line_option));

var product_line_option_hourly=JSON.parse(JSON.stringify(product_line_option));
var device_option_hourly=JSON.parse(JSON.stringify(product_line_option));
var device_unit_option_hourly=JSON.parse(JSON.stringify(product_line_option));

var department_chart = Highcharts.chart('department_chart', department_option);

var product_line_chart = Highcharts.chart('product_line_chart', product_line_option);
var device_chart = Highcharts.chart('device_chart', product_line_option);
var device_unit_chart = Highcharts.chart('device_unit_chart', product_line_option);

var product_line_chart_hourly = Highcharts.chart('product_line_chart_hourly', product_line_option);
var device_chart_hourly = Highcharts.chart('device_chart_hourly', product_line_option);
var device_unit_chart_hourly = Highcharts.chart('device_unit_chart_hourly', product_line_option);

</script>


<script  type="text/javascript">
   		var factory_points_echarts = echarts.init(document.getElementById('factory_points_echarts'));
   		var factory_inst_points_echarts = echarts.init(document.getElementById('factory_inst_points_echarts'));
   		
   		factory_points_option = {
			legend: {
 		        right:180,
 				top:0,
 				itemGap: 16,
 				itemWidth: 24,
 				itemHeight: 12,
 		        textStyle: {
 					color: '#23243a',
 					fontStyle: 'normal',
 					fontFamily: '微软雅黑',
 					fontSize: 16,            
 		        }
 		    },
 		   	title : {
 		        text: '累积量', //主标题
 		        left: 'center' //标题位置
 		    }, 
 		   	toolbox: {
 		        feature: {
 		            dataZoom: {
 		                xAxisIndex: 'none'
 		            },
 		            restore: {},
 		            saveAsImage: {}
 		        }
 		    },
		    tooltip: {
		    	trigger: 'axis',
		        axisPointer: {
		            type: 'cross'
		        }
		    },
		    grid:{
		       left:180,
		       right:180,
		       bottom:80,
		       
		    },
		     dataZoom: [
		        {
		            type: 'slider',
		            xAxisIndex: 0,
		            height:36,
		            realtime: true,
		        },
		        {
		            type: 'inside',
		            xAxisIndex: 0,
		            realtime: true,
		        }, 
		    ], 
		    xAxis: {
		        type: 'time',
		        splitLine: {
		            show: false
		        }
		    },
		    yAxis: {
		    	type: 'value',
		    	min:'dataMin',
		    	max:'dataMax',
		        splitLine: {
		            show: false
		        },
		        axisLabel: {
 		           	formatter: function (value, index) {
 		           		return value.toFixed(0);
 		        	}
 		        },
		    },
		    series: []
		};
		
   		factory_inst_points_option = {
   				legend: {
   	 		        right:180,
   	 				top:0,
   	 				itemGap: 16,
   	 				itemWidth: 24,
   	 				itemHeight: 12,
   	 		        textStyle: {
   	 					color: '#23243a',
   	 					fontStyle: 'normal',
   	 					fontFamily: '微软雅黑',
   	 					fontSize: 16,            
   	 		        }
   	 		    },
	 		   	title : {
	 		        text: '瞬时量', //主标题
	 		        left: 'center' //标题位置
	 		    },
			    tooltip: {
			    	trigger: 'axis',
			        axisPointer: {
			            type: 'cross'
			        }
			    },
			    grid:{
			       left:180,
			       right:180,
			       bottom:80,
			       
			    },
			    toolbox: {                                           
	 		        feature: {
	 		            dataZoom: {
	 		                xAxisIndex: 'none'
	 		            },
	 		            restore: {},
	 		            saveAsImage: {}
	 		        }
	 		    },
			    dataZoom: [
			        {
			            type: 'slider',
			            xAxisIndex: 0,
			            realtime: true,
			        },
			        {
			            type: 'inside',
			            xAxisIndex: 0,
			            realtime: true,
			        },
			       
			    ],
			    xAxis: {
			        type: 'time',
			        splitLine: {
			            show: false
			        }
			    },
			    yAxis: {
			    	min:'dataMin',
			    	max:'dataMax',
			        type: 'value',
			        splitLine: {
			            show: false
			        },
			        axisLabel: {
	 		           	formatter: function (value, index) {
	 		           		return value.toFixed(0);
	 		        	}
	 		        },
			        
			    },
			    series: []
			};
		factory_points_echarts.setOption(factory_points_option);
		factory_inst_points_echarts.setOption(factory_inst_points_option);

	</script>

</body>
</html>
