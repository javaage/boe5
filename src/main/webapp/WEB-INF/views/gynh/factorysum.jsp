<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/inc/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
    <script src='${path}/res/js/my-js/default_values.js'></script> 
	<%@ include file="./common/head.jsp"%>
    
    <script src='${path}/res/js/my-js/_worktime_data.js'></script> 
    <script src='${path}/res/js/my-js/_CUD_data.js'></script> 
    <script src='${path}/res/js/my-js/worktime.js'></script>
    
	<script type="text/javascript">
		var param1 = ${factory};
		var title = "";
		var pageName = 'factorypointstatecons';
		var path = "${path}";
		var pivotGridId = "#pivot_grid";
		var requestpath = "/gynh/factorypointstatecons/get_data.do";
		var request_count_path = "/gynh/factorypointstatecons/get_count.do";
		var layoutStorageKey = "boe-gynh-pivotgrid-factorypointstatecons";
		var bLoadFromDb = true;
		var filterOutFileds = ["开始时间", "结束时间"];
		var is_zt = false;
		var need_worktime = true;
        var worktime_flag = true;
        var unit_show="power_cda_pn2_upw";
		var stateTimeSum;
		var cda_dataField= null;
        var power_dataField= null;
        var pn2_dataField= null;
        var upw_dataField= null;
        var time_dataField=null;
        
        chart_style = [{
            name: "能耗",
            type:"bar",
            yAxis:"0",
            color:""  
        }];
		
		var tableLayout = [
            {
                caption: "分厂",
                dataType: "string",
                dataField: "fc",
                area: "filter",
                headerFilter: {
                    allowSearch: true
                }, 
            }
			,
            {
                caption: "科室",
                dataType: "string",
                dataField: "dp",
                area: "filter",
                headerFilter: {
                    allowSearch: true
                }, 
            }
            ,
            {
                caption: "产线",
                dataType: "string",
                dataField: "ln",
                area: "row",
                headerFilter: {
                    allowSearch: true
                }, 
            }
			,
      		{
	            caption: "设备",
	            dataType: "string",
	            dataField: "dv",
	            area: "row",
                headerFilter: {
                    allowSearch: true
                }, 
     		}
	        ,
      		{
	            caption: "设备单元",
	            dataType: "string",
	            dataField: "un",
	            area: "row",
                headerFilter: {
                    allowSearch: true
                }, 
     		}
	        ,
      		{
	            caption: "地址",
	            dataType: "string",
	            dataField: "pt",
	            area: "row",
                headerFilter: {
                    allowSearch: true
                }, 
     		}
	        ,
	        {
	            caption: "状态类型",
	            dataField: "sc",
	            dataType: "string",
	            area: "row",
                headerFilter: {
                    allowSearch: true
                },
	        }
            ,
            {
                caption: "时间",
                dataType: "string",
                dataField: "tm",
                area: "row",
                headerFilter: {
                    allowSearch: true
                }, 
            }
	        ,
      		{
	            caption: "能源",
	            dataType: "string",
	            dataField: "e",
	            area: "row",
                headerFilter: {
                    allowSearch: true
                }, 
     		}
	        ,	        
	        {
	            caption: "能耗",
	            dataField: "c",
	            dataType: "number",
	            summaryType: "sum",
	            format: {
	            	type: "fixedPoint",
	            	precision: 2
	            },
	            area: "data"
			}
		];
    </script>
    
</head>
<body class='dx-viewport' style="overflow:hidden;">
	<jsp:include flush="true" page="./common/shortcut.jsp"/>
	<div id="rightSplitter" style="margin:0px;padding:0px;border:0px;">
		<div class='pivot-container' style="<%=background%>" > 
			<jsp:include flush="true" page="./common/grid_toolbar.jsp"/>
	        <div id='pivot_grid' ></div> 
	        <div id='grid-popup'></div> 
	        <div id='options-popup'></div> 
		</div> 
		<div class='chart-container' style="<%=background%>">
			<div id="echarts_div" style="z-index:1;height:300px;"></div>
			<c:import url="CUDiagram.jsp"></c:import>
		</div>
	</div>
	<div class="tip tooltip-content clearfix" style="position: absolute;display: none;z-index:999;">&nbsp; |<br><span class="pointQTY">265</span></div>

    <!-- for chart -->
	<!--script src="${path}/res/js/echarts/echarts-all.js"></script-->
	<script src="${path}/res/js/echarts/echarts.min.js"></script>
    <script src='${path}/res/js/my-js/table_toolbar.js'></script>
    <script src='${path}/res/js/my-js/chart.js'></script>
    <script src='${path}/res/js/my-js/worktime_chart.js'></script>

	<%=splitter_style%>
	
	<%=my_laydate_css%>>
	
</body>
</html>