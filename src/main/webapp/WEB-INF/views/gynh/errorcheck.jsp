<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/inc/taglibs.jsp"%>
<!DOCTYPE HTML>
<html>
  <head>
   	<meta charset="utf-8"> 
    <title>定额设置</title>
    
    <script src='${path}/res/js/my-js/default_values.js'></script> 
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<script type="text/javascript">
		var path = "${path}";
	</script>	
	<script src="${path}/res/js/my-js/jquery.min.js"></script>
	<script src="${path}/res/js/my-js/jquery-3.2.1.min.js"></script>
    <link rel="stylesheet" type="text/css" href="${path}/res/css/my-css/dx.common.css" />
    <link rel="stylesheet" href="${path}/res/layui/css/layui.css" media="all" />
	<link rel="dx-theme" data-theme="generic.light" href="${path}/res/css/my-css/dx.light.css" />
	<script src="${path}/res/js/my-js/data.js"></script>
	<script src="${path}/res/js/my-js/errorcheck.js"></script>
	<script src="${path}/res/js/my-js/jszip.min.js"></script>
	<script src="${path}/res/js/my-js/dx.all.js"></script>
	<script src="${path}/res/layui/layui.js"></script>
	<script src="${path}/res/js/echarts/echarts.min.js"></script>
    <style type="text/css">
    .dx-row.dx-data-row .fac_enr {
	    color: #bf4e6a;
	    font-weight: bold;
	}
    </style>
  </head>
  
  <body class="dx-viewport">
  
  	<div style="background:white; float:left; margin-left:12px;margin-top:6px;width:240px;height:37px; position: absolute; z-index: 95;">
        <input type="text" id="day" style="float:left; margin-left:0px;margin-top:2px;width:80px;">
        <div id = "table_toolbar_errorcheck_div" style="height:37px;width:50px; float:left; margin-left:10px;margin-right:0px;"></div>
  	</div>
  	<div class="demo-container" style=" position: absolute; z-index: 90;">
        <div id="gridContainer" style="OVERFLOW:auto;WIDTH:100%;"></div>
    </div>
    <script src='${path}/res/js/my-js/table_toolbar_errorcheck.js'></script>
    
  </body>
</html>
