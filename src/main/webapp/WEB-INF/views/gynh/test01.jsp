<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/inc/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
 	<%@ include file="./common/def.jsp" %>
 	<script src="${path}/res/js/my-js/jquery.min.js"></script>
    <script src='${path}/res/js/my-js/default_values.js'></script> 
    <script src='${path}/res/js/my-js/test01.js'></script>
    <script src="${path}/res/js/my-js/util.js"></script>    
    <script src="${path}/res/highcharts/highcharts.js"></script>
    <script src="${path}/res/highcharts/modules/exporting.js"></script>
    <script src="https://img.highcharts.com.cn/highcharts-plugins/highcharts-zh_CN.js"></script>
    <link rel='stylesheet' type='text/css' href='${path}/res/css/my-css/dx.spa.css' /> 
    <link rel='stylesheet' type='text/css' href='${path}/res/css/my-css/dx.common.css' /> 
    <link rel='dx-theme' data-theme='generic.<%=theme%>' href='${path}/res/css/my-css/dx.<%=theme%>.css' />
    <script src='${path}/res/js/my-js/dx.all.js'></script> 
    <script src="${path}/res/layui/layui.js"></script>
    <link rel="stylesheet" href="${path}/res/css/my-css/swiper.min.css">
    <script type="text/javascript">
        var path = "${path}";
    </script>
    <script>

	
	var factory_list = ${factory_name};
	var pageName = 'factory_chart_view_'+factory_list;
	var start_date = getFormatDate(-7);
	var end_date = getFormatDate(-1);
	

</script>
   
    <!-- Demo styles -->
    <style>
        html, body {
            position: relative;
            height: 100%;
        }
        body {
            font-family: Helvetica Neue, Helvetica, Arial, sans-serif;
            font-size: 14px;
            color:#000;
            margin: 0;
            padding: 0;
            background: #ffffff;
        }
        .swiper-container {
            width: 100%;
            height: 66.66%;
            overflow: hidden;
        }
        .swiper-slide {
            text-align: center;
            font-size: 18px;
            background: #fff;

            /* Center slide text vertically */
            display: -webkit-box;
            display: -ms-flexbox;
            display: -webkit-flex;
            display: flex;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            -webkit-justify-content: center;
            justify-content: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            -webkit-align-items: center;
            align-items: center;
        }
        .swiper-slide{
            display: flex;
            align-items: center;
            width: 50%;
            justify-content: center;
        }
        .swiper-slide>div{
            border: 1px solid red;
            width: 100%;
            height: inherit;
            box-sizing: border-box;
        }
        .i{
            padding: 0 10px;
            height: 66.66%;
        }
        .i>div{
            box-sizing: border-box;
            border: 1px solid red;
            height: 50%;
        }
        .i>div:nth-of-type(1){
            height: 100%;
            display: block;
        }
        .i>div:nth-of-type(2){
            display: none;
        }
        .s{
            position: relative;
        }
        .s>img{
            position: absolute;
            top: 10px;
            left: 10px;
        }
        .footer{
            width: 100%;
            height: 33.33%;
            border: 1px solid red;
            box-sizing: border-box;
            display: flex;
            justify-content: center;
            align-items: center;
        }
        .footer>div{
            flex:1;
            width: 50%;
            height:100%;
            border: 1px solid red;
            box-sizing: border-box;
            text-align: center;
        }
    </style>
</head>
<body>
<div class="swiper-container">
    <div class="swiper-wrapper">
        <div class="swiper-slide">
            <div class="i">
                <div class="q" >
               		<div style="float:left;">科室</div>
					<div id = 'department_box' style="width:25%;height:36px;float:left;margin:0.5%;"></div>
					<div style="float:left;">产线</div>
					<div id = 'line_box' style="width:60%;height:36px;float:left;margin:0.5%;"></div>
					 <div id="container" style="width:100%;height:560px;"> </div>
                </div>
                <div class="s" onclick="f2(0)"></div>
            </div>
        </div>
        <div class="swiper-slide">
            <div class="i">
                <div class="q" onclick="f1(1)">
                	<div style="float:left;">设备</div><div id = 'device_box' style="width:80%;float:left;margin:0.5%;"></div>
                </div>
                <div class="s" onclick="f2(1)"></div>
            </div>
        </div>
        <div class="swiper-slide">
            <div class="i">
                <div class="q" onclick="f1(2)">
                	<div style="float:left;">单元</div><div id = 'device_unit_box' style="width:80%;float:left;margin:0.5%;"></div>
                </div>
                <div class="s" onclick="f2(2)"></div>
            </div>
        </div>
    </div>
    <!--Add Arrows -->
    <div class="swiper-button-next"></div>
    <div class="swiper-button-prev"></div>
</div>
<div class="footer">
    <div>3</div>
    <div>4</div>
</div>
    <script src="${path}/res/js/my-js/swiper.min.js"></script>
     <script>

	
	


    var swiper = new Swiper('.swiper-container', {
        slidesPerView: 2,
         spaceBetween: 0,
        // loop: true,
        loopFillGroupWithBlank: true,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });
    var qs = document.getElementsByClassName('q');
    var ss = document.getElementsByClassName('s');
    function f1(index) {
       qs[index].style.height = '50%';
        ss[index].style.display = 'block'
    }
    function f2(index) {
        qs[index].style.height = '100%';
        ss[index].style.display = 'none'
    }

</script>

<script type="text/javascript">
var option={
		chart: {
			type: 'column'
		},
		title: {
			text: '产线能耗'
		},
		xAxis: {
			categories: [
				'杭州总部',
				'上海分部',
				'北京分部'
			]
		},
		yAxis: [{
			min: 0,
			title: {
				text: '能耗'
			}
		}, {
			title: {
				text: '产量'
			},
			opposite: true
		}],
		legend: {
			shadow: false
		},
		tooltip: {
			shared: true
		},
		plotOptions: {
			column: {
				grouping: false,
				shadow: false,
				borderWidth: 0
			}
		},
		series: [{
			name: '雇员',
			data: [],
			pointPadding: 0.40, // 通过 pointPadding 和 pointPlacement 控制柱子位置
			pointPlacement: -0.4
		}, {
			name: '优化的员工',
			data: [],
			pointPadding: 0.46,
			pointPlacement: -0.4,
			yAxis: 1
		}, {
			name: '利润',
			data: [],
			pointPadding: 0.40,
			pointPlacement: -0.2,
			yAxis: 0  // 指定数据列所在的 yAxis
		}, {
			name: '优化的利润',
			data: [],
			pointPadding: 0.46,
			pointPlacement: -0.2,
			yAxis: 1
		}]
	};

var chart = Highcharts.chart('container', option);

</script>

</body>
</html>
