<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/inc/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
    <script src='${path}/res/js/my-js/default_values.js'></script> 
    <script src='${path}/res/js/my-js/util.js'></script> 
	<%@ include file="./common/head.jsp"%>

    <meta charset="utf-8">
    <title></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <link rel="stylesheet" href="${path}/res/layui/css/layui.css" media="all" >
    <link rel="stylesheet" href="${path}/res/css/public.css" media="all" />
    <script src="${path}/res/js/echarts/echarts.min.js"></script>
    <script src='${path}/res/js/my-js/chart.js'></script>

    <script src="${path}/res/js/my-js/jquery-3.2.1.min.js"></script>
        
    <link rel="stylesheet" type ='text/css' href="${path}/res/css/public-css/css/font-awesome.min.css">
    <script src="${path}/res/js/my-js/viewer.js"></script>
    <link rel="stylesheet" type ='text/css' href="${path}/res/css/my-css/viewer.css">
 	<script src="${path}/res/layui/layui.js"></script>
 	<script src='${path}/res/js/my-js/mesotherm.js'></script>

    <script type="text/javascript">
  
    	var path = "${path}";
 		
    </script>
    
    <style>
		html,body{
			height: 100%;
			width: 100%;
			margin: 0;
			padding: 0;
			background:white;

		}
		
		
	</style>
  
</head>
<body class='dx-viewport' style="overflow:hidden;">
	<button onclick="button_click()">button</button>
	<div id="mesotherm_dlg_div" class="modal" style="width:100%; height:800px;overflow-x: hidden; overflow-y: hidden;">
		<header class="modal-header" >
			&nbsp;&nbsp;&nbsp;&nbsp;列表
			<button id="mesotherm_dlg_close_button" class="modal-header-btn right modal-close" title="Close Modal">X</button>
		</header>
		<div class="modal-body" style="	overflow-x: hidden; overflow-y: hidden;">
			<div class="demo-container" style=" position: absolute; z-index: 1000;width:100%">
				<div class="layui-btn-group" style="margin-top:20px;margin-left:120px">
				    <button class="layui-btn" onclick="one_month()">近30天</button>
				    <button class="layui-btn" onclick="one_week()">近一周</button>
				    <button class="layui-btn" onclick="today()">今日</button>
				    <button class="layui-btn" onclick="yesterday()">昨日</button>
				    <button class="layui-btn" onclick="custom()">自定义跨度</button>
  				</div>
  				 <div class="layui-input-inline" style="margin-top:20px;margin-left:20px;width:200px;">
        			<input type="text" class="layui-input" id="date" placeholder=" - " style="display: none">
     			 </div>
	      		<div id="cold_chart" style="width:100%;height:200px;margin-top:20px;"></div>
	      		<div id="flow_chart" style="width:100%;height:200px;margin-top:20px;"></div>
	      		<div id="temperature_chart" style="width:100%;height:200px;margin-top:20px;"></div>
	  		</div>
		</div>
	</div>
	
	
	
	<script>
	var cold_chart = echarts.init(document.getElementById('cold_chart'));
	var flow_chart = echarts.init(document.getElementById('flow_chart'));
	var temperature_chart = echarts.init(document.getElementById('temperature_chart'));
	
	
	cold_chart_option = {
		    title : {
		        text: '冷量',
		        x: 'center',
		        align: 'right'
		    },
		    
		    grid: {
		    	  left:120,
			      right:120,
			      bottom:80,
		    },
		    toolbox: {
		    	right:120,
		        feature: {
		            dataZoom: {
		                yAxisIndex: 'none'
		            },
		            restore: {},
		            saveAsImage: {}
		        }
		    },
		    tooltip : {
		        trigger: 'axis',
		        axisPointer: {
		            type: 'cross',
		            animation: false,
		            label: {
		                backgroundColor: '#505765'
		            }
		        }
		    },
		    dataZoom: [
		        {
		            show: true,
		            realtime: true,
		        },

		    ],
		    xAxis : [
		        {
		            type : 'time',
		            boundaryGap : false,
		            axisLine: {onZero: false},
		            splitLine: {
			            show: false
			        },
		           
		        }
		    ],
		    yAxis: [
		        {
		            name: 'kW',
		            type: 'value',
		           
		        }
		    ],
		    series: [
		        {
		            name:'冷量',
		            type:'line',
		            animation: false,
		            areaStyle: {
		            	color: '#00AEAE',
		            },
		            lineStyle: {
		                width: 4,
		                color: '#00AEAE',
		            },
		             data:[
		                    {value:['2009/10/1 1:00',321]},
		                    {value:['2009/10/1 1:30',213]}  
		                    ,{value:['2009/10/1 2:00',242]}
		                    ,{value:['2009/10/1 3:00',467]},
		                    {value:['2009/10/1 4:00',243]},
		                    {value:['2009/10/1 5:00',362]},
		                    {value:['2009/10/1 6:00',224]},
		                    {value:['2009/10/1 7:00',265]}
		               ],
		        },
		       
		    ]
		};
	
		var flow_chart_option=JSON.parse(JSON.stringify(cold_chart_option));
		var temperature_chart_option=JSON.parse(JSON.stringify(cold_chart_option));

		flow_chart_option.yAxis[0].name="m³";
		temperature_chart_option.yAxis[0].name="℃";
		flow_chart_option.title.text="流量";
		temperature_chart_option.title.text="温差";
		flow_chart_option.series[0].name="流量";
		temperature_chart_option.series[0].name="温差";	
		
        flow_chart_option.series[0].areaStyle.color="#009100";
        flow_chart_option.series[0].lineStyle.color="#009100";
        
        temperature_chart_option.series[0].areaStyle.color="rgba(0, 0, 0, 0.0)";
        temperature_chart_option.series[0].lineStyle.color="#AE8F00";
		
		
		/* cold_chart.setOption(cold_chart_option,true);
		flow_chart.setOption(flow_chart_option,true);
		temperature_chart.setOption(temperature_chart_option,true); */
		
	</script>
	<%=my_laydate_css%>
	
</body>
</html>