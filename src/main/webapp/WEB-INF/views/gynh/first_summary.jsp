<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/inc/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
    <script src='${path}/res/js/my-js/default_values.js'></script> 
    <script src='${path}/res/js/my-js/util.js'></script> 
	<script>
		var defaut_begin_date = getFormatDate(-7);
		var defaut_end_date = getFormatDate(-1);
	</script>
	<%-- <%@ include file="./common/head.jsp"%> --%>
    <%@ include file="./common/def.jsp" %>

    <meta charset="utf-8">
    <title></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <link rel="stylesheet" href="${path}/res/layui/css/layui.css" media="all" >
    <link rel="stylesheet" href="${path}/res/css/public.css" media="all" />

    <script src="${path}/res/js/my-js/jquery-3.2.1.min.js"></script>
        
    <link rel="stylesheet" type ='text/css' href="${path}/res/css/public-css/css/font-awesome.min.css">
	<script src='${path}/res/js/my-js/factories_summary_unit.js'></script>
    <script src="${path}/res/js/my-js/viewer.js"></script>
    <link rel="stylesheet" type ='text/css' href="${path}/res/css/my-css/viewer.css">
 	<script src="${path}/res/layui/layui.js"></script>
  
    <script type="text/javascript">
        var param1 = "";
        var title = "";
        var pageName = 'first_summary';
        var path = "${path}";
        var pivotGridId = "#pivot_grid";
        //var requestpath = "/gynh/first_summary/get_data.do";
        var request_count_path = "/gynh/factories_line_state_cons/get_count.do";
        var layoutStorageKey = "boe-gynh-pivotgrid-first_summary";
        var bLoadFromDb = true;
        var filterOutFileds = ["开始时间", "结束时间"];
        var is_zt = false;
        var need_worktime = true;
        var worktime_flag = true;
        var unit_show="power_cda_pn2_upw";
        var stateTimeSum;
        var cda_dataField= null;
        var power_dataField= null;
        var pn2_dataField= null;
        var upw_dataField= null;
        var time_dataField=null;
          // init date control
        layui.use('laydate', function(){
        	var laydate = layui.laydate;
        	  //日期范围
        	laydate.render({
        	elem: '#beginTimeBox',
        	range: true,
        	theme: is_dark?'#444444':'',
        	type: 'date',
        	value: defaut_begin_date + ' - ' + defaut_end_date,
        	done: function(value) {
        		defaut_begin_date=value.substr(0,10);
        		defaut_end_date=value.substr(13,10);
        		load_data(defaut_begin_date, defaut_end_date);
        		page_custom_resize();
        		}
        	});
        });
        if(ajax_json){
        	first_summary_ajax_url1=path + '/res/json/first_summary1.json';
        	first_summary_ajax_url2=path + '/res/json/first_summary2.json';
        	first_summary_ajax_url3=path + '/res/json/first_summary3.json';
        	first_summary_ajax_url4=path + '/res/json/first_summary4.json';
        	first_summary_ajax_url5=path + '/res/json/first_summary5.json';
        	first_summary_ajax_url6=path + '/res/json/first_summary6.json';
        	first_summary_ajax_url7=path + '/res/json/first_summary7.json';
        }
    </script>
    
    <style>
		html,body{
			height: 100%;
			width: 100%;
			margin: 0;
			padding: 0;
			background:white;
			
		}
		
		table {
			background: rgba(245, 247, 250);
		}
		.font_div{
	width: 67%;
    font-size: 33px;
    text-align: center;
    line-height: 100%;
    margin-left: 20px;
    color: #fff;
    margin-top: 10px; 
		}
		.l_border {
			float: left;
    width: 19%;
    margin-top: 10px;
    background: #fff;
    margin-left: 10px;
		}
		.l_border1 {
			float: left;
    width: 19%;
    margin-top: 10px;
    background: #fff;
    margin-left: 10px;
		}
		.l_border2 {
			float: left;
    width: 19%;
    margin-top: 10px;
    background: #fff;
    margin-left: 10px;
		}
		.r_border {
			border-right:10px solid #DDE1E4;
		}
		.t_border {
			border-top:10px solid #DDE1E4;
		}
		.b_border {
			border-bottom:10px solid #DDE1E4;
		}
		.td_bg_color {
			background:#DDE1E4;
		}
		.banner{
    float: left;
    margin-left: 10px;
    width: 19%;
    height: 28px;
    line-height: 26px;
    text-align: center;
    font-size: 28px;
    color: #fff;
    background-color: #009999;

		}
		
	</style>
  
</head>
<body class='dx-viewport' style="overflow:hidden;background-color: #f3f3f4;">
	<!--div style="background:white; float:left; margin-left:12px;margin-top:0px;width:360px;height:37px;">
        <input type="text" class="<%=layui_bg_class%>" id="beginTimeBox" style="float:left; margin-left:0px;margin-top:2px;width:160px;">
        <div id = "table_toolbar_summary_div" style="height:37px;width:50px; float:left; margin-left:10px;margin-right:0px;"></div>
      
        <input class='toolbar_btn' type="image" title="天/小时" onclick="echarts_style()" src="${path}/res/images/toolbarbtn/calendar.png"
        	style=" float:left; width:24px;height:24px;"/>
      
        <jsp:include flush="true" page="./common/factories_summary_grid_toolbar.jsp"/>
        
  	</div-->
  	<div id='' style="margin-top:3px;float:left; height:0px;"> &nbsp; </div>
  	
	<div id='fac_sum_charts_div' style="float: left;width:100%;height:23%;">
         <div id="power_div"><div id='power_chart' style="float:left;margin-left: 36px;margin-top:0px;"></div></div>
         <div id="cw_div" ><div id='cw_chart' style="float:left;margin-left: 0px;margin-top:0px;"></div></div>
         <div id="pn2_div" ><div id='pn2_chart' style="float:left;margin-left: 0px;margin-top:0px;"></div></div>
         <div id="gas_div"><div id='gas_chart' style="float:left;margin-left: 0px;margin-top:0px;"></div></div>
		<div id='message_div' style=" width: 400px; float: right; margin-right: 20px; margin-top: 20px; ">
			<table class="layui-hide" id="test" ></table>
			<!--<div id="message_button_div" style="font-size:15px;float:left; margin-left:0px;margin-top:6px">
				<div class="layui-btn-group">
					<button id="last" class="layui-btn layui-btn-sm" onclick="last_message()">上一个</button>
					<button id="next" class="layui-btn layui-btn-sm" onclick="next_message()">下一个</button>
				 </div>
				<div class="layui-btn-group">
					<button id="table_power" class="layui-btn layui-btn-sm" onclick="power_message()">Power</button>
					<button id="table_cda" class="layui-btn layui-btn-sm" onclick="cda_message()">CDA</button>
					<button id="table_upw" class="layui-btn layui-btn-sm" onclick="upw_message()">UPW</button>
				 </div>
			</div>	 -->	
		</div>
  		<div id='' style="margin-top:5px;height:5px;"> &nbsp; </div>
    <div><img style="    height: 23px;
    float: right;
    margin-right: -7px;
    /* z-index: 999; */
    /* display: block; */
    position: relative;
    top: 107px;
    /* transform: initial; */
    alt="" src="${path}/res/images/left.png" onclick="last_message()"></div>
	<div><img style="    height: 23px;
    float: right;
    margin-right: -423px;
    /* z-index: 999; */
    /* display: block; */
    position: relative;
    top: 107px;" alt="" src="${path}/res/images/right.png" onclick="next_message()"></div>
	</div>
	<div style="width: 30;margin-top: 29px;float:left;height:100%;">
			
	   <div style="  background: #37fdebcf;" class="font_div"><br/>P<br/>o<br/>w<br/>e<br/>r</div>
	   <div style="background:#1997e0bd;" class="font_div"><br/>U<br/><br/>P<br/><br/>W</div>
	   <div style="background:#389e1e;" class="font_div"><br/>C<br/><br/>D<br/><br/>A</div>
	</div>
	<div id ='chart_body' style="    width: 96%;
    float: left;
    height: 77%;
    "> 
    	
       <div id="text_div">
           <span class="banner"><%=str_array%></span>
		    <span class="banner"><%=str_cf%></span>
		    <span class="banner"><%=str_cell%></span>
		    <span  class="banner">MD</span>
            <span  class="banner" >CUB</span>
     </div>
     
     <div>
    <div class = 'l_border'>
      <div id='array_power_chart' style='margin:0;padding:0;color:white;'></div>
    </div>
     <div class = 'l_border '>
       <div id='cf_power_chart' style='margin:0;padding:0;color:white;'></div>
    </div>
    <div class = 'l_border '>
       <div id='cell_power_chart' style='margin:0;padding:0;color:white;'></div>
    </div>
    <div class = 'l_border '>
       <div id='mdl_power_chart' style='margin:0;padding:0;color:white;'></div>
    </div>
    <div class = 'l_border '>
       <div id='cub_power_chart' style='margin:0;padding:0;color:white;'></div>
    </div>
     
     </div>
     
     <div>
    <div class = 'l_border1 '>
      <div id='array_upw_chart' style='margin:0;padding:0;color:white;'></div>
    </div>
     <div class = 'l_border1 '>
       <div id='cf_upw_chart' style='margin:0;padding:0;color:white;'></div>
    </div>
    <div class = 'l_border1 '>
       <div id='cell_upw_chart' style='margin:0;padding:0;color:white;'></div>
    </div>
    <div class = 'l_border1 '>
       <div id='mdl_upw_chart' style='margin:0;padding:0;color:white;'></div>
    </div>
    <div class = 'l_border1 '>
       <div id='cub_upw_chart' style='margin:0;padding:0;color:white;'></div>
    </div>
     </div>
     
     <div>
    <div class = 'l_border2 '>
      <div id='array_cda_chart' style='margin:0;padding:0;color:white;'></div>
    </div>
     <div class = 'l_border2 '>
       <div id='cf_cda_chart' style='margin:0;padding:0;color:white;'></div>
    </div>
    <div class = 'l_border2 '>
       <div id='cell_cda_chart' style='margin:0;padding:0;color:white;'></div>
    </div>
    <div class = 'l_border2 '>
       <div id='mdl_cda_chart' style='margin:0;padding:0;color:white;'></div>
    </div>
    <div class = 'l_border2 '>
       <div id='cub_cda_chart' style='margin:0;padding:0;color:white;'></div>
    </div>  
     
     </div>
     
    </div>
	<script src="${path}/res/js/echarts/echarts.min.js"></script>
    <script src='${path}/res/js/my-js/table_toolbar_summary.js'></script>
	<script>
		var currentRCUnit = 'KW2';//kWh,mWh
		var currentPowerUnit = 'mWh';//kWh,mWh
		var currentCDAUnit = 'k(m³)';//L,m³,k(m³)
		var currentUPWUnit = 'k(m³)';//L,m³,k(m³)
		var currentPN2Unit = 'k(m³)';//L,m³,k(m³)
		var currentTimeUnit = 'M';

		var power_legend = ["Power", '产量', '稼动率', '单耗'];
		var upw_legend   = [str_upw,   '产量', '稼动率', '单耗'];
		var cda_legend   = [str_cda,   '产量', '稼动率', '单耗'];
		var pn2_legend   = [str_pn2,   '产量', '稼动率', '单耗'];
		
		var power_color_array = ['<%=power_color%>', '#66FFCC', '#883333', '#3333CC']
		var upw_color_array   = ['<%=upw_color%>',   '#66FFCC', '#883333', '#3333CC']
		var cda_color_array   = ['<%=cda_color%>',   '#66FFCC', '#883333', '#3333CC']
		var pn2_color_array   = ['<%=pn2_color%>',   '#66FFCC', '#883333', '#3333CC']
		
		var power_chart = echarts.init(document.getElementById('power_chart'));
		var cw_chart = echarts.init(document.getElementById('cw_chart'));
		var pn2_chart = echarts.init(document.getElementById('pn2_chart'));
		var gas_chart = echarts.init(document.getElementById('gas_chart'));

		var power_chart_option = {
				tooltip : {
				        formatter: "{a} <br/>{c} {b}"
				    },
			    title: {
			        x: "center",
			        bottom: 30,
			        text: '',
		        	textStyle:{
		        		fontWeight: 'normal',
		        		fontSize: 10,
		        		//color:"#CC0000",
		        	} ,
			    },
			    series : [ 
			    	 {
				            name: '月/定额',
				            type: 'gauge',
				            z: 3,
				            min: 0,
				            max: 1000,
				            splitNumber: 10,
				            radius: '93%',
				            axisLine: {            // 坐标轴线
				                lineStyle: {       // 属性lineStyle控制线条样式
				                    width: 8
				                }
				            },
				            itemStyle: { //仪表盘指针样式
				                normal: {
				                    color: '#CC0000 ',
				                    shadowColor: 'rgba(0, 0, 0, 0.5)',
				                    shadowBlur: 10,
				                    shadowOffsetX: 2,
				                    shadowOffsetY: 2
				                }
				            },
				            axisTick: {  
				            	show:false,			// 坐标轴小标记
				                length: 12,        // 属性length控制线长
				                lineStyle: {       // 属性lineStyle控制线条样式
				                    color: 'auto'
				                }
				            },
				            splitLine: { 
				            	show:false,			// 分隔线
				                length: 16,         // 属性length控制线长
				                lineStyle: {       // 属性lineStyle（详见lineStyle）控制线条样式
				                    color: 'auto'
				                }
				            },
				            pointer: {
				                width:2
				            },
				            axisLabel: {
				                formatter: function (value) {
				                	return "";       
				                }
				            },
				            detail: {fontSize:12,formatter: function (value) {
				                return "";    
				            }},
				            data:[{value: 400, name: 'L'}]
				        },
				        {
				            name: '月/实际',
				            type: 'gauge',
				            z: 3,
				            min: 0,
				            max: 1000,
				            splitNumber: 10,
				            radius: '93%',
				            axisLine: {            // 坐标轴线
				                lineStyle: {       // 属性lineStyle控制线条样式
				                    width: 8
				                }
				            },
				            itemStyle: { //仪表盘指针样式
				                normal: {
				                    color: '#0000AA',
				                    shadowColor: 'rgba(0, 0, 0, 0.5)',
				                    shadowBlur: 10,
				                    shadowOffsetX: 2,
				                    shadowOffsetY: 2
				                }
				            },
				            axisTick: { 
				            	show:false,// 坐标轴小标记
				                length: 12,        // 属性length控制线长
				                lineStyle: {       // 属性lineStyle控制线条样式
				                    color:'auto'
				                }
				            },
				            splitLine: {           // 分隔线
				            	show:false,
				                length: 16,         // 属性length控制线长
				                lineStyle: {       // 属性lineStyle（详见lineStyle）控制线条样式
				                    color: 'auto'
				                }
				            },
				            pointer: {
				                width:2
				            },
				            axisLabel: {
				                formatter: function (value) {
				                    return "";    
				                }
				            },
				            //detail: {fontSize:12,formatter:'月\n{value}'},
				            detail: {fontSize:12,formatter: function (value) {
				                return "";    
				            }},
				            data:[{value: 380, name: 'L'}]
				        },
				        {
				            name: '刻度',
				            type: 'gauge',
				            z: 3,
				            min: 0,
				            max: 1000,
				            splitNumber: 10,
				            radius: '93%',
				            axisLine: {            // 坐标轴线
				                lineStyle: {       // 属性lineStyle控制线条样式
				                    width: 8
				                }
				            },
				            itemStyle: { //仪表盘指针样式
				                normal: {
				                    color: '#0000AA',
				                    shadowColor: 'rgba(0, 0, 0, 0.5)',
				                    shadowBlur: 10,
				                    shadowOffsetX: 2,
				                    shadowOffsetY: 2
				                }
				            },
				            axisLabel: {
				            	color:'#000000',
				            	fontSize:9,
				            	distance:0,
				                formatter: function (value) {
				                	return value;
				                }
				            },
				            axisTick: {            // 坐标轴小标记
				                length: 12,        // 属性length控制线长
				                lineStyle: {       // 属性lineStyle控制线条样式
				                    color:'auto'
				                }
				            },
				            splitLine: {           // 分隔线
				                length: 16,         // 属性length控制线长
				                lineStyle: {       // 属性lineStyle（详见lineStyle）控制线条样式
				                    color: 'auto'
				                }
				            },
				            pointer: {
				                width:0
				            },
				            //detail: {fontSize:12,formatter:'月\n{value}'},
				            detail: {show:false},
				            data:[{value: 380, name: ''}]
				        },
				        {
				            name: '辅助饼图黑色',
				            type: 'pie',
				            radius: '100%',
				            center: ["50%", "50%"],
				            z: 1,
				            tooltip: {
				            	show:false,
					        },
				            hoverAnimation: false,
				            label: {
				                normal: {
				                    show: false
				                }
				            },
				            labelLine: {
				                normal: {
				                    show: false
				                }
				            },
				            itemStyle: {
				                normal: {
				                    color: {
				                        type: 'radial',
				                        x: 0.1,
				                        y: -0.1,
				                        r: 1,
				                        colorStops: [{
				                            offset: 0,
				                            color: '#DDDDDD' // 0% 处的颜色
				                        }, {
				                            offset: 0.6,
				                            color: 'rgba(180, 180, 180, 0.2)' // 50% 处的颜色
				                        }, {
				                            offset: 0.71,
				                            color: 'rgba(80, 80, 80, 0.2)' // 51% 处的颜色
				                        }, {
				                            offset: 1,
				                            color: 'rgba(0, 0, 0, 0.2)' // 100% 处的颜色
				                        }],
				                        globalCoord: true // 缺省为 false
				                    }
				                }
				            },
				            data: [{
				                value: 1,
				                name: '辅助饼图黑色'
				            }],
				        },
				        {
				            name: '辅助饼图黑色',
				            type: 'pie',
				            radius: '8%',
				            center: ["50%", "50%"],
				            z: 4,
				            hoverAnimation: false,
				            tooltip: {
				            	show:false,
					        },
				            label: {
				                normal: {
				                    show: false
				                }
				            },
				            labelLine: {
				                normal: {
				                    show: false
				                }
				            },
				            itemStyle: {
				                normal: {
				                    color: {
				                        type: 'radial',
				                        x: 0.1,
				                        y: -0.1,
				                        r: 1,
				                        colorStops: [{
				                            offset: 0,
				                            color: '#DDDDDD' // 0% 处的颜色
				                        }, {
				                            offset: 0.7,
				                            color: '#1B1811' // 50% 处的颜色
				                        }, {
				                            offset: 0.71,
				                            color: '#1B1811' // 51% 处的颜色
				                        }, {
				                            offset: 1,
				                            color: '#1B1811' // 100% 处的颜色
				                        }],
				                        globalCoord: true // 缺省为 false
				                    }
				                }
				            },
				            data: [{
				                value: 1,
				                name: '辅助饼图黑色'
				            }],
				        }, {
				            name: '辅助饼图红色',
				            type: 'pie',
				            radius: '4%',
				            z: 4,
				            tooltip: {
				               show:false,
				            },
				            center: ["50%", "50%"],
				            hoverAnimation: false,
				            label: {
				                normal: {
				                    show: false
				                }
				            },
				            labelLine: {
				                normal: {
				                    show: false
				                }
				            },
				            itemStyle: {
				                normal: {
				                    color: "#E50505"
				                }
				            },
				            data: [{
				                value: 1,
				                name: '辅助饼图红色'
				            }],
				        }, 
			    ]
		
			};
		var cw_chart_option=power_chart_option;
		var pn2_chart_option=power_chart_option;
		var gas_chart_option=power_chart_option;
		
		//power_chart.setOption(power_chart_option);
		//cw_chart.setOption(cw_chart_option);
		//pn2_chart.setOption(pn2_chart_option);
		//gas_chart.setOption(gas_chart_option);

		var array_power_chart = echarts.init(document.getElementById('array_power_chart'), "<%=theme%>");
		var array_power_chart_option = {
		    title: {
		    	show:false,
		        x: "center",
		        top: 0,
		        text: 'POWER',
	        	textStyle:{
	        		fontWeight: 'normal',
	        		fontSize: 13,
	        		//color:"#CC0000",
	        	} ,
		    },
		    tooltip: {
		        trigger: 'axis',
	        	backgroundColor:'rgba(156,156,156,0.6)',
		    },
		    //legend: {
		    //	type: 'scroll',
		    //    textStyle: {color:'<%=text_color%>'},
		    //    data:power_legend,
		    //    y:'bottom',
		    //},
		    grid: [
			    {
			        left: '55px',
			        right: '55px',
			        bottom: '65%',
			        top:'36px',
			        //containLabel: false,
			    },
			    {
			        left: '55px',
			        right: '55px',
			        bottom: '33px',
			        top:'45%',
			        //containLabel: false,
			    },
		    ],
		    xAxis: [
			    {
			    	show: false,
					axisLabel: {interval: 0},
			        textStyle: {color:'white'},
			        type: 'category',
			        //boundaryGap: false,
			        gridIndex: 0,
			        data: []
			    },
			    {
					axisLabel: {interval: 0},
			        textStyle: {color:'white'},
			        type: 'category',
			        gridIndex: 1,
			        //boundaryGap: false,
			        data: []
			    },
		    ],
		    yAxis: [
			    {
			    	name: '       单耗',
			    	nameGap: 0,
			    	nameTextStyle: {fontSize: 11},
			        textStyle: {color:'white'},
			        //min: 'dataMin',
			        /*function(value) {
			        	var m=value.min-(value.max-value.min)*0.1;
			        	var n=m;
			        	for(var i=0;i<10;i++){
			        		if(n.toPrecision(2)<m){
			        			break;
			        		}else{
			        			n=n*0.95;
			        		}	
			        	}
			        	if (n.toPrecision(2)>0) {
			        		return n.toPrecision(2);
			        	} else {
			        		return 0;
			        	}
			            	
			        },*/
			        //max: 'dataMax',
			       
			        /*function(value) {
			        	var m=value.max+(value.max-value.min)*0.5;
			        	var n=m;
			        	for(var i=0;i<10;i++){
			        		if(n.toPrecision(2)>m){
			        			break;
			        		}else{
			        			n=n*1.06;
			        		}	
			        	}
			            return n.toPrecision(2);
			        },*/ 
			        type: 'value',
			        position: 'left',
			        splitNumber: 3,
			        offset: 0,
			        axisLabel:{
			            inside: false,
			        },
			        splitLine:{
			        	show:false,
			        },
			    },
			    {
			    	name: '能耗',
			    	nameGap: 0,
			    	nameTextStyle: {fontSize: 11},
			        gridIndex: 1,
			        textStyle: {color:'white'},
			        type: 'value',
			        //min: 'dataMin',
					/*
			        min:function(value) {
			        	var m=value.min-(value.max-value.min)*0.1;
			        	var n=m;
			        	for(var i=0;i<10;i++){
			        		if(n.toPrecision(2)<m){
			        			break;
			        		}else{
			        			n=n*0.95;
			        		}	
			        	}
			        	if (n.toPrecision(2)>0) {
			        		return n.toPrecision(2);
			        	} else {
			        		return 0;
			        	}
			        },
			        */
			        //max: 'dataMax',
			        /*
			        max: function(value) {
			        	var m=value.max+(value.max-value.min)*0.5;
			        	var n=m;
			        	for(var i=0;i<10;i++){
			        		if(n.toPrecision(2)>m){
			        			break;
			        		}else{
			        			n=n*1.06;
			        		}	
			        	}
			            return n.toPrecision(2);
			        },
			        */
			        position: 'left',
			        offset: 0,
			        axisLabel:{
			            inside: false,
			        },
			        splitLine:{ 
						show:false,
			        },
			    },
			],
		    color: power_color_array,
		    series: [
				{
		            name: ' ',
		            type: 'bar',
		            stack: 'total',
		            label: {
		                normal: {
		                    show: false,
		                }
		            },
		            data: ["-"]
		        },
		    ],
		};
		
		array_power_chart_option.yAxis[1].name = '         能耗' + '(' + currentPowerUnit + ')';
		array_power_chart.setOption(array_power_chart_option);
		
		var yAxis_max=function(value) {
			var m=value.max+(value.max-value.min)*0.5;
        	var n=m;
        	for(var i=0;i<10;i++){
        		if(n.toPrecision(2)>m){
        			break;
        		}else{
        			n=n*1.06;
        		}	
        	}
            return n.toPrecision(2);
        };
        var yAxis_min=function(value) {
        	var m=value.min-(value.max-value.min)*0.1;
        	var n=m;
        	for(var i=0;i<10;i++){
        		if(n.toPrecision(2)<m){
        			break;
        		}else{
        			n=n*0.95;
        		}	
        	}
        	if (n.toPrecision(2)>0) {
        		return n.toPrecision(2);
        	} else {
        		return 0;
        	}
        };
		
		var cf_power_chart_option = JSON.parse(JSON.stringify(array_power_chart_option));
		
		/*
		cf_power_chart_option.yAxis[1].max=yAxis_max;
		cf_power_chart_option.yAxis[0].min=yAxis_min;
		cf_power_chart_option.yAxis[1].min=yAxis_min;
		*/
		
		var cell_power_chart_option = JSON.parse(JSON.stringify(array_power_chart_option));
		/*
		cell_power_chart_option.yAxis[0].max=yAxis_max;
		cell_power_chart_option.yAxis[1].max=yAxis_max;
		cell_power_chart_option.yAxis[0].min=yAxis_min;
		cell_power_chart_option.yAxis[1].min=yAxis_min;
		*/

		var mdl_power_chart_option = JSON.parse(JSON.stringify(array_power_chart_option));
		/*
		mdl_power_chart_option.yAxis[0].max=yAxis_max;
		mdl_power_chart_option.yAxis[1].max=yAxis_max;
		mdl_power_chart_option.yAxis[0].min=yAxis_min;
		mdl_power_chart_option.yAxis[1].min=yAxis_min;
		*/

		var cub_power_chart_option = JSON.parse(JSON.stringify(array_power_chart_option));
		/*
		cub_power_chart_option.yAxis[0].max=yAxis_max;
		cub_power_chart_option.yAxis[1].max=yAxis_max;
		cub_power_chart_option.yAxis[0].min=yAxis_min;
		cub_power_chart_option.yAxis[1].min=yAxis_min;
		*/

		var array_upw_chart_option = JSON.parse(JSON.stringify(array_power_chart_option));
		array_upw_chart_option.yAxis[1].name = '         能耗' + '(' + currentUPWUnit + ')';
		/*
		array_upw_chart_option.yAxis[0].max=yAxis_max;
		array_upw_chart_option.yAxis[1].max=yAxis_max;
		array_upw_chart_option.yAxis[0].min=yAxis_min;
		array_upw_chart_option.yAxis[1].min=yAxis_min;
		*/
		//array_upw_chart_option.legend.data = upw_legend;
		array_upw_chart_option.color = upw_color_array;
		array_upw_chart_option.title.text = 'UPW';
	
		var cf_upw_chart_option = JSON.parse(JSON.stringify(array_power_chart_option));
		cf_upw_chart_option.yAxis[1].name = '         能耗' + '(' + currentUPWUnit + ')';
		/*
		cf_upw_chart_option.yAxis[0].max=yAxis_max;
		cf_upw_chart_option.yAxis[1].max=yAxis_max;
		cf_upw_chart_option.yAxis[0].min=yAxis_min;
		cf_upw_chart_option.yAxis[1].min=yAxis_min;
		*/
		//cf_upw_chart_option.legend.data = upw_legend;
		cf_upw_chart_option.color = upw_color_array;
		cf_upw_chart_option.title.text = 'UPW';
		
		var cell_upw_chart_option = JSON.parse(JSON.stringify(array_power_chart_option));
		cell_upw_chart_option.yAxis[1].name = '         能耗' + '(' + currentUPWUnit + ')';
		/*
		cell_upw_chart_option.yAxis[0].max=yAxis_max;
		cell_upw_chart_option.yAxis[1].max=yAxis_max;
		cell_upw_chart_option.yAxis[0].min=yAxis_min;
		cell_upw_chart_option.yAxis[1].min=yAxis_min;
		*/
		//cell_upw_chart_option.legend.data = upw_legend;
		cell_upw_chart_option.color = upw_color_array;
		cell_upw_chart_option.title.text = 'UPW';
		
		var mdl_upw_chart_option = JSON.parse(JSON.stringify(array_power_chart_option));
		mdl_upw_chart_option.yAxis[1].name = '         能耗' + '(' + currentUPWUnit + ')';
		/*
		mdl_upw_chart_option.yAxis[0].max=yAxis_max;
		mdl_upw_chart_option.yAxis[1].max=yAxis_max;
		mdl_upw_chart_option.yAxis[0].min=yAxis_min;
		mdl_upw_chart_option.yAxis[1].min=yAxis_min;
		*/
		//mdl_upw_chart_option.legend.data = upw_legend;
		mdl_upw_chart_option.color = upw_color_array;
		mdl_upw_chart_option.title.text = 'UPW';
		
		var cub_upw_chart_option = JSON.parse(JSON.stringify(array_power_chart_option));
		cub_upw_chart_option.yAxis[1].name = '         能耗' + '(' + currentUPWUnit + ')';
		/*
		cub_upw_chart_option.yAxis[0].max=yAxis_max;
		cub_upw_chart_option.yAxis[1].max=yAxis_max;
		cub_upw_chart_option.yAxis[0].min=yAxis_min;
		cub_upw_chart_option.yAxis[1].min=yAxis_min;
		*/
		//cub_upw_chart_option.legend.data = upw_legend;
		cub_upw_chart_option.color = upw_color_array;
		cub_upw_chart_option.title.text = 'UPW';
		

		var array_cda_chart_option = JSON.parse(JSON.stringify(array_power_chart_option));
		array_cda_chart_option.yAxis[1].name = '         能耗' + '(' + currentCDAUnit + ')';
		/*
		array_cda_chart_option.yAxis[0].max=yAxis_max;
		array_cda_chart_option.yAxis[1].max=yAxis_max;
		array_cda_chart_option.yAxis[0].min=yAxis_min;
		array_cda_chart_option.yAxis[1].min=yAxis_min;
		*/
		//array_cda_chart_option.legend.data = cda_legend; 
		array_cda_chart_option.color = cda_color_array;
		array_cda_chart_option.title.text = 'CDA';
		
		var cf_cda_chart_option = JSON.parse(JSON.stringify(array_power_chart_option));
		cf_cda_chart_option.yAxis[1].name = '         能耗' + '(' + currentCDAUnit + ')';
		/*
		cf_cda_chart_option.yAxis[0].max=yAxis_max;
		cf_cda_chart_option.yAxis[1].max=yAxis_max;
		cf_cda_chart_option.yAxis[0].min=yAxis_min;
		cf_cda_chart_option.yAxis[1].min=yAxis_min;
		*/
		//cf_cda_chart_option.legend.data = cda_legend;
		cf_cda_chart_option.color = cda_color_array;
		cf_cda_chart_option.title.text = 'CDA';
		
		var cell_cda_chart_option = JSON.parse(JSON.stringify(array_power_chart_option));
		cell_cda_chart_option.yAxis[1].name = '         能耗' + '(' + currentCDAUnit + ')';
		/*
		cell_cda_chart_option.yAxis[0].max=yAxis_max;
		cell_cda_chart_option.yAxis[1].max=yAxis_max;
		cell_cda_chart_option.yAxis[0].min=yAxis_min;
		cell_cda_chart_option.yAxis[1].min=yAxis_min;
		*/
		//cell_cda_chart_option.legend.data = cda_legend;
		cell_cda_chart_option.color = cda_color_array;
		cell_cda_chart_option.title.text = 'CDA';
		
		var mdl_cda_chart_option = JSON.parse(JSON.stringify(array_power_chart_option));
		mdl_cda_chart_option.yAxis[1].name = '         能耗' + '(' + currentCDAUnit + ')';
		/*
		mdl_cda_chart_option.yAxis[0].max=yAxis_max;
		mdl_cda_chart_option.yAxis[1].max=yAxis_max;
		mdl_cda_chart_option.yAxis[0].min=yAxis_min;
		mdl_cda_chart_option.yAxis[1].min=yAxis_min;
		*/
		//mdl_cda_chart_option.legend.data = cda_legend;
		mdl_cda_chart_option.color = cda_color_array;
		mdl_cda_chart_option.title.text = 'CDA';

		var cub_cda_chart_option = JSON.parse(JSON.stringify(array_power_chart_option));
		cub_cda_chart_option.yAxis[1].name = '         能耗' + '(' + currentCDAUnit + ')';
		/*
		cub_cda_chart_option.yAxis[0].max=yAxis_max;
		cub_cda_chart_option.yAxis[1].max=yAxis_max;
		cub_cda_chart_option.yAxis[0].min=yAxis_min;
		cub_cda_chart_option.yAxis[1].min=yAxis_min;
		*/
		//cub_cda_chart_option.legend.data = cda_legend;
		cub_cda_chart_option.color = cda_color_array;
		cub_cda_chart_option.title.text = 'CDA';
		
		
		//var array_pn2_chart_option = JSON.parse(JSON.stringify(array_power_chart_option));
		//array_pn2_chart_option.yAxis[0].max=yAxis_max;
		//array_pn2_chart_option.yAxis[1].max=yAxis_max;
		//array_pn2_chart_option.yAxis[0].min=yAxis_min;
		//array_pn2_chart_option.yAxis[1].min=yAxis_min;
		//array_pn2_chart_option.legend.data = pn2_legend;
		//array_pn2_chart_option.color = pn2_color_array;
		
		//var cf_pn2_chart_option = JSON.parse(JSON.stringify(array_power_chart_option));
		//cf_pn2_chart_option.yAxis[0].max=yAxis_max;
		//cf_pn2_chart_option.yAxis[1].max=yAxis_max;
		//cf_pn2_chart_option.yAxis[0].min=yAxis_min;
		//cf_pn2_chart_option.yAxis[1].min=yAxis_min;
		//cf_pn2_chart_option.legend.data = pn2_legend;
		//cf_pn2_chart_option.color = pn2_color_array;
		
		//var cell_pn2_chart_option = JSON.parse(JSON.stringify(array_power_chart_option));
		//cell_pn2_chart_option.yAxis[0].max=yAxis_max;
		//cell_pn2_chart_option.yAxis[1].max=yAxis_max;
		//cell_pn2_chart_option.yAxis[0].min=yAxis_min;
		//cell_pn2_chart_option.yAxis[1].min=yAxis_min;
		//cell_pn2_chart_option.legend.data = pn2_legend;
		//cell_pn2_chart_option.color = pn2_color_array;
		
		//var mdl_pn2_chart_option = JSON.parse(JSON.stringify(array_power_chart_option));
		//mdl_pn2_chart_option.yAxis[0].max=yAxis_max;
		//mdl_pn2_chart_option.yAxis[1].max=yAxis_max;
		//mdl_pn2_chart_option.yAxis[0].min=yAxis_min;
		//mdl_pn2_chart_option.yAxis[1].min=yAxis_min;
		//mdl_pn2_chart_option.legend.data = pn2_legend;
		//mdl_pn2_chart_option.color = pn2_color_array;
		
		

		var cf_power_chart = echarts.init(document.getElementById('cf_power_chart'), "<%=theme%>");
		cf_power_chart.setOption(cf_power_chart_option);
		var cell_power_chart = echarts.init(document.getElementById('cell_power_chart'), "<%=theme%>");
		cell_power_chart.setOption(cell_power_chart_option);
		var mdl_power_chart = echarts.init(document.getElementById('mdl_power_chart'), "<%=theme%>");
		mdl_power_chart.setOption(mdl_power_chart_option);
		var cub_power_chart = echarts.init(document.getElementById('cub_power_chart'), "<%=theme%>");
		cub_power_chart.setOption(cub_power_chart_option);
		
		var array_upw_chart = echarts.init(document.getElementById('array_upw_chart'), "<%=theme%>");
		array_upw_chart.setOption(array_upw_chart_option);
		var cf_upw_chart = echarts.init(document.getElementById('cf_upw_chart'), "<%=theme%>");
		cf_upw_chart.setOption(cf_upw_chart_option);
		var cell_upw_chart = echarts.init(document.getElementById('cell_upw_chart'), "<%=theme%>");
		cell_upw_chart.setOption(cell_upw_chart_option);
		var mdl_upw_chart = echarts.init(document.getElementById('mdl_upw_chart'), "<%=theme%>");
		mdl_upw_chart.setOption(mdl_upw_chart_option);
		var cub_upw_chart = echarts.init(document.getElementById('cub_upw_chart'), "<%=theme%>");
		cub_upw_chart.setOption(cub_upw_chart_option);

		var array_cda_chart = echarts.init(document.getElementById('array_cda_chart'), "<%=theme%>");
		array_cda_chart.setOption(array_cda_chart_option);
		var cf_cda_chart = echarts.init(document.getElementById('cf_cda_chart'), "<%=theme%>");
		cf_cda_chart.setOption(cf_cda_chart_option);
		var cell_cda_chart = echarts.init(document.getElementById('cell_cda_chart'), "<%=theme%>");
		cell_cda_chart.setOption(cell_cda_chart_option);
		var mdl_cda_chart = echarts.init(document.getElementById('mdl_cda_chart'), "<%=theme%>");
		mdl_cda_chart.setOption(mdl_cda_chart_option);
		var cub_cda_chart = echarts.init(document.getElementById('cub_cda_chart'), "<%=theme%>");
		cub_cda_chart.setOption(cub_cda_chart_option);

		//var array_pn2_chart = echarts.init(document.getElementById('array_pn2_chart'), "<%=theme%>");
		//array_pn2_chart.setOption(array_pn2_chart_option);
		//var cf_pn2_chart = echarts.init(document.getElementById('cf_pn2_chart'), "<%=theme%>");
		//cf_pn2_chart.setOption(cf_pn2_chart_option);
		//var cell_pn2_chart = echarts.init(document.getElementById('cell_pn2_chart'), "<%=theme%>");
		//cell_pn2_chart.setOption(cell_pn2_chart_option);
		//var mdl_pn2_chart = echarts.init(document.getElementById('mdl_pn2_chart'), "<%=theme%>");
		//mdl_pn2_chart.setOption(mdl_pn2_chart_option);
		
	</script>
	
	<script src='${path}/res/js/my-js/first_summary.js'></script>
	
	<%=my_laydate_css%>
	
</body>
</html>