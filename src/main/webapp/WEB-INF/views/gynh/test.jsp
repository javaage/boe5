<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/inc/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
    <script src='${path}/res/js/my-js/default_values.js'></script>
    <%@ include file="./common/def.jsp" %>
    <script src="${path}/res/js/my-js/util.js"></script>
    <script src="${path}/res/js/my-js/jquery-3.2.1.min.js"></script>

	<script type="text/javascript">
    	var pageName = 'test';
        var title = "";
        var path = "${path}";
    </script>
    

    <script src='${path}/res/js/my-js/test.js'></script>
    
</head>
<body class='dx-viewport' style="overflow:hidden;">

	<div id=''>
		<button onclick='test()'>click me</button>
	</div>

	
</body>
</html>