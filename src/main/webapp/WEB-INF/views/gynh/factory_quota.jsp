<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/inc/taglibs.jsp"%>

<!DOCTYPE HTML>
<html>
  <head>
   	<meta charset="utf-8"> 
    <title>定额设置</title> 
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<script type="text/javascript">
		var path = "${path}";
	</script>
	<script src="${path}/res/js/my-js/util.js"></script>
	<script src="${path}/res/js/my-js/jquery-3.2.1.min.js"></script>
	<script src="${path}/res/js/my-js/handsontable.full.js"></script>

    <link rel="stylesheet" type="text/css" href="${path}/res/css/my-css/dx.common.css" />
    <link rel="stylesheet" type="text/css" href="${path}/res/css/my-css/handsontable.full.css" />
	<link rel="dx-theme" data-theme="generic.light" href="${path}/res/css/my-css/dx.light.css" />
	<script src="${path}/res/js/my-js/data.js"></script>
	<script src="${path}/res/js/my-js/factory_quota.js"></script>
	<script src="${path}/res/js/my-js/jszip.min.js"></script>
	<script src="${path}/res/js/my-js/dx.all.js"></script>
    <style type="text/css">
    .dx-row.dx-data-row .fac_enr {
    color: #bf4e6a;
    font-weight: bold;
}
    #events {
    background-color: rgba(191, 191, 191, 0.15);
    padding: 20px;
    margin-top: 20px;
}
#events > div {
    padding-bottom: 5px;
}
#events > div:after {
    content: "";
    display: table;
    clear: both;
}
#events #clear {
    float: right;
}
#events .caption {
    float: left;
    font-weight: bold;
    font-size: 115%;
    line-height: 115%;
    padding-top: 7px;
}

#events ul {
    list-style: none;
    max-height: 100px;
    overflow: auto;
    margin: 0;
}

#events ul li {
    padding: 7px 0;
    border-bottom: 1px solid #dddddd;
}

#events ul li:last-child {
    border-bottom: none;
}
    </style>
  </head>
  
  <body class="dx-viewport" >
   <div class="demo-container" style="float:left">
   		<button  id="quota_grid_am" onclick="quota_grids()" style="float:left; margin-left:270px;position: absolute; z-index: 1100; margin-top: 12px">保存</button>
   		<button  id="power_quotas_daily_am" onclick="power_quotas_daily_sava_data()" style="float:left; margin-left:270px;position: absolute; z-index: 1000; margin-top: 12px">保存</button>
   		<button  id="cda_quotas_daily_am" onclick="cda_quotas_daily_sava_data()" style="float:left; margin-left:270px;position: absolute; z-index: 1000; margin-top: 12px">保存</button>
	 	<button  id="upw_quotas_daily_am" onclick="upw_quotas_daily_sava_data()" style="float:left; margin-left:270px;position: absolute; z-index: 1000; margin-top: 12px">保存</button>
	  	<button  id="pn2_quotas_daily_am" onclick="pn2_quotas_daily_sava_data()" style="float:left; margin-left:270px;position: absolute; z-index: 1000; margin-top: 12px">保存</button>
	   	<button id="cw_quotas_daily_am" onclick="cw_quotas_daily_sava_data()" style="float:left; margin-left:270px;position: absolute; z-index: 1000; margin-top: 12px">保存</button>
   		<button  id="gas_quotas_daily_am"onclick="gas_quotas_daily_sava_data()" style="float:left; margin-left:270px;position: absolute; z-index: 1000; margin-top: 12px">保存</button>
   		<div style="float:left; margin-left: 12px;padding-top: 6px;position: absolute; z-index: 80;">
	   		<div style=" float:left;text-align: center;font-weight: bold;background-color: #E1E5EE;width: 120px;
	    	height: 33px;line-height: 33px; margin-left: 12px;">当前年份：<a id="year"></a>
	   		</div>
	    	<button id="preYear"  style="float:left; margin-left: 12px; margin-top:6px;">上一年</button>
		 	<button id="nextYear" style="float:left; margin-left: 12px; margin-top:6px;">下一年</button>
	    </div>
	  
        <div style="margin-left:1px;position: fixed; z-index: 90;">
	        <div id="longtabs" style="position: absolute; z-index: 93;left:400px;top:0px;WIDTH:100%;">
	            <div class="tabs-container"></div> 	
	        </div>
		    <div id="quota_div" style="position: absolute; z-index: 11;left:0px;top:5px;width:100px;height:100px;background—color:white;display:none;margin-top:45px">
		    	 <div id="quota_grid" ></div>
		    </div>
		    <div id="power_div" style="position: absolute; z-index: 12;left:0px;top:5px;width:100px;height:100px;background—color:white;display:none;margin-top:45px">
		    <!-- <button  id="export-file-power">导出2</button> -->
		    	<div id="power_grid"></div>
		    </div>
		    <div id="cda_div" style="position: absolute; z-index: 12;left:0px;top:5px;width:100px;height:100px;background—color:white;display:none;margin-top:45px">
		    	<div id="cda_grid"></div>
		    </div>
		    <div id="upw_div" style="position: absolute; z-index: 12;left:0px;top:5px;width:100px;height:100px;background—color:white;display:none;margin-top:45px">
		    	<div id="upw_grid"></div>
		    </div>
		    <div id="pn2_div" style="position: absolute; z-index: 12;left:0px;top:5px;width:100px;height:100px;background—color:white;display:none;margin-top:45px">
		    	<div id="pn2_grid"></div>
		    </div>
		     <div id="cw_div" style="position: absolute; z-index: 12;left:0px;top:5px;width:100px;height:100px;background—color:white;display:none;margin-top:45px">
		    	<div id="cw_grid"></div>
		    </div>
		    <div id="gas_div" style="position: absolute; z-index: 12;left:0px;top:5px;width:100px;height:100px;background—color:white;display:none;margin-top:45px">
		    	<div id="gas_grid"></div>
	    	</div>
		</div>  
    </div>
  </body>
</html>
