<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/inc/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
    <script src='${path}/res/js/my-js/default_values.js'></script>
    <%@ include file="./common/def.jsp" %>
    <script src="${path}/res/js/my-js/util.js"></script>
    <script src="${path}/res/js/my-js/jquery-3.2.1.min.js"></script>
    <link rel='stylesheet' type='text/css' href='${path}/res/css/my-css/dx.spa.css' /> 
    <link rel='stylesheet' type='text/css' href='${path}/res/css/my-css/dx.common.css' /> 
    <link rel='dx-theme' data-theme='generic.<%=theme%>' href='${path}/res/css/my-css/dx.<%=theme%>.css' />
    <script src='${path}/res/js/my-js/dx.all.js'></script> 
    <script src="${path}/res/layui/layui.js"></script>

	<script type="text/javascript">
		percent_fileds = ["实际/定额"];
		line_fileds = ["实际/定额", "定额月累计"];

		var bDataFieldAreaInRow = true;
		var factory_list = ${factory_name};
    	var pageName = 'factory_chart_view_'+factory_list;
        var temp_date = new Date();
		var start_date = getFormatDate(-7);
		var end_date = getFormatDate(-1);
		var hourly_date = getFormatDate(-1);
	</script>
	
   
    <script type="text/javascript">
        var param1 = "";
        var title = "";
        var path = "${path}";
       
        var pivotGridId = "#pivot_grid";
        var requestpath = "/gynh/factory_chart_view/get_data.do";
        var request_count_path = "/gynh/factory_chart_view/get_count.do";
        var layoutStorageKey = "boe-gynh-pivotgrid-factory_chart_view";
        var bLoadFromDb = true;
        var filterOutFileds = [];
        var is_zt = false;
        var need_worktime = true;
        var worktime_flag = false;
        var unit_show="power_cda_upw_pn2";
        var stateTimeSum;
        var cda_dataField= "c";
        var power_dataField= "c";
        var pn2_dataField= "c";
        var upw_dataField= "c";
        var time_dataField=null;
    </script>
    
    <style>
		.dx-tag-content {
			padding: 0px 19px 0px 3px;
			margin: 1px 3px 1px 3px;
		}
		.dx-placeholder:before {
		    padding: 1px 9px 8px;
		}
		.dx-list-select-all-label {
		    line-height: 1;
		    padding: 0 10px;
		    margin-top: 2px;
		    font-size: 12px;
		}
		.dx-field-label {
		    width: 1%;
	    }
		.dx-field-value:not(.dx-switch):not(.dx-checkbox):not(.dx-button), .dx-field-value-static {
		    width: 90%;
		}
		.toolbar_btn {
			height: 24px;
			width: 24px;
		}
		
		#series_box .dx-texteditor-input {
		    background: #ddffdd;
		}
		
	</style>
    
    <script src='${path}/res/js/my-js/factory_chart_view.js'></script>
    
</head>
<body class='dx-viewport' style="overflow:hidden;">
    <div id='toobar_div' style="background:white;">
    
		<!-- <div class='_space' style="width:0.8%;float:left;">&nbsp;</div>
		<div id = 'factory_box' style="width:18%;float:left;padding:10;margin:0.5%;"></div> -->
		<div class='_space' style="width:0.8%;float:left;">&nbsp;</div>
		<div id = 'department_box' style="width:23%;float:left;padding:10;margin:0.5%;"></div>
		<div class='_space' style="width:0.8%;float:left;">&nbsp;</div>
		<div id = 'line_box' style="width:23%;float:left;padding:10;margin:0.5%;"></div>
		<div class='_space' style="width:0.8%;float:left;">&nbsp;</div>
		<div id = 'device_box' style="width:23%;float:left;padding:10;margin:0.5%;"></div>
		<div class='_space' style="width:0.8%;;float:left;">&nbsp;</div>
		<div id = 'device_unit_box' style="width:22.8%;float:left;padding:10;margin:0.5%;"></div>
		<div class='_space' style="width:0.8%;float:left;">&nbsp;</div>
		
    	<div style="float:left;margin-left:1.3%; ">
		<input class='toolbar_btn' type="image" title="天/小时" onclick="switch_daily_hourly()" src="${path}/res/images/toolbarbtn/calendar.png" />
        &nbsp;
        </div>
    	<div style="float:left">
        <input type="text" class="<%=layui_bg_class%>" readonly id="daily_box" style="width:160px;text-align:center;background:#ddffdd;">
        <input type="text" class="<%=layui_bg_class%>" readonly id="hourly_box" style="width:160px;text-align:center;background:#ddffdd;display:none">
        </div>
        
		<div class='_space' style="width:10px;float:left;">&nbsp;</div>
    	<div style="float:left">
        <select id="select_energy" class="<%=layui_bg_class%>" onchange="select_erengy_changed()" style="float:left;height:24px;width:67px;background:#ddffdd;">
         	<option value="Power">Power</option>
        	<option value="UPW">UPW</option>
            <option value="CDA">CDA</option>
            <option value="PN2">PN2</option>
        </select>
        <div style="float:left;">
        	&nbsp;
			<input class='toolbar_btn' type="image" title="加载" onclick="load_data()" src="${path}/res/images/toolbarbtn/db.png" />
			<input class='toolbar_btn' type="image" title="图序" onclick="switch_group_type_in_chart()" src="${path}/res/images/toolbarbtn/chart_group.png" />
        </div>
        </div>
		<!-- <div class='_space' style="width:10px;float:left;">&nbsp;</div>
		<div id = 'series_box' style="width:255px;float:left;padding:10;margin:10;background:#ddffdd;"></div> -->
		
	    <div id = "unit_bar" style="float:right; margin-right: 1.5%">
	        <font id="font_hms" style="">时长:</font>
	        <select id="select_hms" class="<%=layui_bg_class%>" onchange="timeUnitChange(this)" style="height:22px;width:56px;">
	         	<option value="S">秒</option>
	        	<option value="M" selected>分钟</option>
	            <option value="H">小时</option>
	        </select>
	        <font id="font_power" style="">Power:</font>
	        <select id="select_power" class="<%=layui_bg_class%>" onchange="powerUnitChange(this)" style="height:22px;width:60px;">
	            <option value="kWh">kWh</option>
	            <option value="mWh">mWh</option>
	        </select>
	        <font id="font_upw" style="">UPW:</font>
	        <select id="select_upw" class="<%=layui_bg_class%>" onchange="UnitChange(this)" style="height:22px;width:62px;">
	            <option value="L">L</option>
	            <option value="m³">m³</option>
	            <option value="k(m³)">k(m³)</option>
	        </select>
	        <font id="font_cda" style="">CDA:</font>
	        <select id="select_cda" class="<%=layui_bg_class%>" onchange="UnitChange(this)" style="height:22px;width:62px;">
	            <option value="L">L</option>
	            <option value="m³">m³</option>
	            <option value="k(m³)">k(m³)</option>
	        </select>
	        <font id="font_pn2" style="">PN2:</font>
	        <select id="select_pn2" class="<%=layui_bg_class%>" onchange="UnitChange(this)" style="height:22px;width:62px;">
	            <option value="L">L</option>
	            <option value="m³">m³</option>
	            <option value="k(m³)">k(m³)</option>
	        </select>
	    </div>
		<div style="clear:both"></div>
   	
    </div> 
	
	<div id='gap_div1' style="background:#ccc;height:10px;"></div>
	
    <div id='daily_chart1'></div> 
    <div id='hourly_chart1' style="height:0px;"></div> 
    
	<div id='gap_div2' style="background:#ccc;height:10px;"></div>
    
    <div id='daily_chart2'></div> 
    <div id='hourly_chart2' style="height:0px;"></div> 

	<div id='gap_div3' style="background:#ccc;height:10px;"></div>
 
    <div id='state_time_chart' style="height:0px;"></div> 
    <div id='hourly_state_time_chart' style="height:0px;"></div> 
    
	<div id='gap_div4' style="background:#ccc;height:10px;"></div>
    
	<div id='state_bar' style="height:100px;"></div>

    <!-- for chart -->
	<script src="${path}/res/js/echarts/echarts.min.js"></script>
	<script>
		var daily_chart1 = echarts.init(document.getElementById('daily_chart1'), "<%=theme%>");
		var daily_chart2 = echarts.init(document.getElementById('daily_chart2'), "<%=theme%>");
		var hourly_chart1 = echarts.init(document.getElementById('hourly_chart1'), "<%=theme%>");
		var hourly_chart2 = echarts.init(document.getElementById('hourly_chart2'), "<%=theme%>");
		var hourly_state_time_chart = echarts.init(document.getElementById('hourly_state_time_chart'), "<%=theme%>");
		var daily_chart1_option = {
			title:{
				text:'能耗与产量（天）',
			    left:'100px',
			    top:'6px'
			},
		    tooltip: {
		        trigger: 'axis',
		        backgroundColor:'rgba(156,156,156,0.6)',
		        formatter: function (params) {
		        	var str="";
		        	for(var i=0;i<daily_chart1_option.series.length;i++){
		        		if(isNaN(params[i].value)){
		        			str=str+'<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:'+params[i].color+';"></span>'
			        		str= '<span style="color:#000000">'+str+daily_chart1_option.series[i].means+' '+params[i].seriesName+' : '+params[i].value+'</span>'+'<br>';
		        		}else{
		        			str=str+'<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:'+params[i].color+';"></span>'
			        		str= '<span style="color:#000000">'+str+daily_chart1_option.series[i].means+' '+params[i].seriesName+' : '+params[i].value.toFixed(3)+'</span>'+'<br>';
		        		}
		        	}
		            return str;
		        }
		    },
		    legend: {
		    	right: "center",
		    	type: 'scroll',
		        textStyle: {color:'<%=text_color%>'},
		    	//align: 'right',
		    	//right: '80px',
		        data:[],
		    },
		    grid: {
		        left: '90px',
		        right: '160px',
		        bottom: '30px',
		        top:'72px',
				padding:'0 0 0 0',
		        containLabel: false,
		        show:true,
		        backgroundColor:'rgba(128, 128, 128, 0.36)', 
		    },
		    toolbox: {
		    	top:'6px',
	            right: '160px',
	            feature: {
	                dataZoom: {
	                	iconStyle: {
	                        borderColor:'#14ABEB',
	                    },
	                    yAxisIndex: 'none'
	                },
	                restore: {
	                	iconStyle: {
	                        borderColor:'#14ABEB',
	                    },
	                },
	                saveAsImage: {
	                	iconStyle: {
	                        borderColor:'#14ABEB',
	                    },
	                },
	    			my_energy_chart: {
	    				show: true,
	    				title: '能耗',
	    				icon: 'image://'+path+'/res/images/toolbarbtn/energy_btn.png',
	    				onclick: function() {
	    					var chart="daily_chart1";
	    					energy_chart_show(chart);
	    				}
	    			},
	    			my_output_chart: {
	    				show: true,
	    				title: '产量',
	    				icon: 'image://'+path+'/res/images/toolbarbtn/output_btn.png',
	    				onclick: function() {
	    					var chart="daily_chart1";
	    					output_chart_show(chart);
	    				}
	    			},
	    			my_unit_cons_chart: {
	    				show: true,
	    				title: '单耗',
	    				icon: 'image://'+path+'/res/images/toolbarbtn/unit_cons_btn.png',
	    				onclick: function() {
	    					var chart="daily_chart1";
	    					unit_cons_chart_show(chart);
	    				}
	    			},
	    			my_state_time_chart: {
	    				show: false,
	    				title: '时间',
	    				icon: 'image://'+path+'/res/images/toolbarbtn/state_time_btn.png',
	    				onclick: function() {
	    					var chart="daily_chart1";
	    					state_time_chart_show(chart);
	    				}
	    			},
	            }
	        },
		    dataZoom: [{
	            type: 'inside'
	        }],
		    xAxis: [
		    	{
					axisLabel: {interval: 0},
			        textStyle: {color:'white'},
			        type: 'category',
			        //boundaryGap: false,
			        data: []
		    	},
		    ],
		    yAxis: [
			    {
			    	name: '能耗',
			    	show:true,
			        textStyle: {color:'white'},
			        type: 'value',
			        position: 'left',
			        offset: 0,
			        axisLabel:{
			            inside: false,
			        },
			        splitLine: {
			            lineStyle: {
			                color: ['#aaa']
			            }
			        }
			    },
			    {
			    	name: '产量',
			    	show:true,
			        textStyle: {color:'white'},
			        type: 'value',
			        position: 'right',
			        offset: 0,
			        axisLabel:{
			            inside: false,
			        },
			        splitLine:{
						show:false,
			        },
			    },
			    {
			    	name: '单耗',
			    	show:true,
			        textStyle: {color:'white'},
			        type: 'value',
			        offset: 72,
			        position: 'right',
			        axisLabel:{
			            inside: false,
			        },
			        splitLine:{
			        	show:false,
			        },
			    },
			    {
			    	name: '状态时间',
			    	show:false,
			        textStyle: {color:'white'},
			        type: 'value',
			        offset: 144,
			        position: 'right',
			        axisLabel:{
			            inside: false,
			        },
			        splitLine:{
			        	show:false,
			        },
			    },
			],
		    color: my_colors,
		    series: []
		};
		
		var formatter_daily_chart2 = function (params) {
        	var str="";
        	for(var i=0;i<daily_chart2_option.series.length;i++){
        		if(isNaN(params[i].value)){
        			str=str+'<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:'+params[i].color+';"></span>'
        			str= '<span style="color:#000000">'+str+daily_chart2_option.series[i].means+' '+params[i].seriesName+" : "+params[i].value+'</span>'+'<br>';
        		}else{
        			str=str+'<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:'+params[i].color+';"></span>'
        			str= '<span style="color:#000000">'+str+daily_chart2_option.series[i].means+' '+params[i].seriesName+" : "+params[i].value.toFixed(3)+'</span>'+'<br>';
        		}
        	}
            return str;
        }
		daily_chart1.setOption(daily_chart1_option);
		var daily_chart2_option = JSON.parse(JSON.stringify(daily_chart1_option));
		
		daily_chart2_option.tooltip.formatter=formatter_daily_chart2;
		daily_chart2_option.title.text="能耗与时间（天）";
		daily_chart2_option.yAxis[0].show=true;
		daily_chart2_option.yAxis[1].show=false;
		daily_chart2_option.yAxis[2].show=true;
		daily_chart2_option.yAxis[3].show=true;
		daily_chart2_option.yAxis[2].offset=72;
		daily_chart2_option.yAxis[3].offset=0;
		daily_chart2_option.toolbox.feature.my_energy_chart.show=true;
		daily_chart2_option.toolbox.feature.my_output_chart.show=false;
		daily_chart2_option.toolbox.feature.my_unit_cons_chart.show=false;
		daily_chart2_option.toolbox.feature.my_state_time_chart.show=true;
		daily_chart2_option.toolbox.feature.my_energy_chart.onclick=function() {
			var chart="daily_chart2";
			energy_chart_show(chart);
		};
		daily_chart2_option.toolbox.feature.my_output_chart.onclick=function() {
			var chart="daily_chart2";
			output_chart_show(chart);
		};
		daily_chart2_option.toolbox.feature.my_unit_cons_chart.onclick=function() {
			var chart="daily_chart2";
			unit_cons_chart_show(chart);
		};
		daily_chart2_option.toolbox.feature.my_state_time_chart.onclick=function() {
			var chart="daily_chart2";
			state_time_chart_show(chart);
		};
		daily_chart2.setOption(daily_chart2_option);
		
		
		var formatter_hourly_chart1 = function (params) {
        	var str="";
        	for(var i=0;i<hourly_chart1_option.series.length;i++){
        		if(isNaN(params[i].value)){
        			str=str+'<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:'+params[i].color+';"></span>'
            		str= '<span style="color:#000000">'+str+hourly_chart1_option.series[i].means+' '+params[i].seriesName+" : "+params[i].value+'</span>'+'<br>';
        		}else{
        			str=str+'<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:'+params[i].color+';"></span>'
            		str= '<span style="color:#000000">'+str+hourly_chart1_option.series[i].means+' '+params[i].seriesName+" : "+params[i].value.toFixed(3)+'</span>'+'<br>';
        		}
        	}
            return str;
        }
		var hourly_chart1_option = JSON.parse(JSON.stringify(daily_chart1_option));
		hourly_chart1_option.tooltip.formatter=formatter_hourly_chart1;
		hourly_chart1_option.title.text="能耗与产量（时）";
		hourly_chart1_option.toolbox.feature.my_energy_chart.show=true;
		hourly_chart1_option.toolbox.feature.my_output_chart.show=true;
		hourly_chart1_option.toolbox.feature.my_unit_cons_chart.show=true;
		hourly_chart1_option.toolbox.feature.my_state_time_chart.show=false;
		hourly_chart1_option.toolbox.feature.my_energy_chart.onclick=function() {
			var chart="hourly_chart1";
			energy_chart_show(chart);
		};
		hourly_chart1_option.toolbox.feature.my_output_chart.onclick=function() {
			var chart="hourly_chart1";
			output_chart_show(chart);
		};
		hourly_chart1_option.toolbox.feature.my_unit_cons_chart.onclick=function() {
			var chart="hourly_chart1";
			unit_cons_chart_show(chart);
		};
		hourly_chart1_option.toolbox.feature.my_state_time_chart.onclick=function() {
			var chart="hourly_chart1";
			state_time_chart_show(chart);
		};
		hourly_chart1_option.xAxis[0].data = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23];
		hourly_chart1.setOption(hourly_chart1_option);
		
		
		var formatter_hourly_chart2 = function (params) {
        	var str="";
        	for(var i=0;i<hourly_chart2_option.series.length;i++){
        		if(isNaN(params[i].value)){
        			str=str+'<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:'+params[i].color+';"></span>'
            		str= '<span style="color:#000000">'+str+hourly_chart2_option.series[i].means+' '+params[i].seriesName+" : "+params[i].value+'</span>'+'<br>';
        		}else{
        			str=str+'<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:'+params[i].color+';"></span>'
            		str= '<span style="color:#000000">'+str+hourly_chart2_option.series[i].means+' '+params[i].seriesName+" : "+params[i].value.toFixed(3)+'</span>'+'<br>';
        		}
        	}
            return str;
        }
		var hourly_chart2_option = JSON.parse(JSON.stringify(daily_chart2_option));
		hourly_chart2_option.tooltip.formatter=formatter_hourly_chart2;
		hourly_chart2_option.title.text="能耗与时间（时）";
		hourly_chart2_option.toolbox.feature.my_energy_chart.show=true;
		hourly_chart2_option.toolbox.feature.my_output_chart.show=false;
		hourly_chart2_option.toolbox.feature.my_unit_cons_chart.show=false;
		hourly_chart2_option.toolbox.feature.my_state_time_chart.show=true;
		hourly_chart2_option.toolbox.feature.my_energy_chart.onclick=function() {
			var chart="hourly_chart2";
			energy_chart_show(chart);
		};
		hourly_chart2_option.toolbox.feature.my_output_chart.onclick=function() {
			var chart="hourly_chart2";
			output_chart_show(chart);
		};
		hourly_chart2_option.toolbox.feature.my_unit_cons_chart.onclick=function() {
			var chart="hourly_chart2";
			unit_cons_chart_show(chart);
		};
		hourly_chart2_option.toolbox.feature.my_state_time_chart.onclick=function() {
			var chart="hourly_chart2";
			state_time_chart_show(chart);
		};
		hourly_chart2_option.xAxis[0].data = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23];
		hourly_chart2.setOption(hourly_chart2_option);

		var hourly_state_time_chart_option = {
 	 	    /*legend: {
 	 	    	 right:10,
	 				top:0,
	 				itemGap: 16,
	 				itemWidth: 18,
	 				itemHeight: 10,
 	 	        data: legendData,
 	 	        textStyle: {
 	 	            color: 'blue'
 	 	        }
 	 	    },*/
			grid: {
				left: '90px',
				right: '160px',
				bottom: '20px',
				top: '8px',
				padding:'0 0 0 0',
				containLabel: false,
			},
 	 	    tooltip: {
 	 	        show: true,
 	 	        backgroundColor: '#fff',
 	 	        borderColor: '#ddd',
 	 	        borderWidth: 1,
 	 	        textStyle: {
 	 	            color: '#3c3c3c',
 	 	        },
				position: function (point, params, dom, rect, size) {
					return point;
				},
 	 	        formatter: function(p) {
 	 	            return p.seriesName.split('_')[0];
 	 	        },
 	 	    },
 	 	    xAxis: [
	 	 	    {
	 	 	    	show: false,
	 	 	        axisLabel: {
	 	 	            show: false,
	 	 	        },
	 	 	        axisLine: {
	 	 	            show: false,
	 	 	        },
	 	 	        axisTick: {
	 	 	            show: false,
	 	 	        },
	 	 	        splitLine: {
	 	 	            show: false,
	 	 	        },
	 	 	        max: 86400,
	 	 	    },
		    	{
	 	 	    	position: 'bottom',
					axisLabel: {interval: 0},
			        //textStyle: {color:'white'},
			        type: 'category',
			        //boundaryGap: false,
			        data: [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23],
		    	},
 	 	    ],
 	 	    yAxis: [{
 	 	        data: [],
				interval: 0,
 	 	        axisLabel: {
 	 	            color: 'blue',
					interval: 0,
					showMinLabel: true,
					showMaxLabel: true,
					fontSize: 10,
					formatter: function(v) {
 	 	                return v;
 	 	            } 	 	        
 	 	    	},
 	 	        axisLine: {
 	 	            show: false,
 	 	        },
 	 	        axisTick: {
 	 	            show: false,
 	 	        },
 	 	        splitLine: {
 	 	            show: false,
 	 	        }
 	 	    }],
 	 	    series: [
 	 	    	{
 	 	          type: 'bar',
 	 	          animation: false,
 	 	          name: '',
 	 	          stack: ' ',
 	 	          // label: _label,
 	 	          legendHoverLink: false,
 	 	          itemStyle: {
 	 	              normal: {
 	 	                  color: 'red',
 	 	  				label: {
 	 	  					show: false,
 	 	  				}
 	 	              },
 	 	          },
 	 	          data: []
 	 	      }
			]
 	 	};

	    hourly_state_time_chart.setOption(hourly_state_time_chart_option);
	</script>


	<%=my_laydate_css%>
	
</body>
</html>