    <%@ include file="./def.jsp" %>

    <meta charset="utf-8">
    <title></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <link rel="stylesheet" href="${path}/res/layui/css/layui.css" media="all" >
    <link rel="stylesheet" href="${path}/res/css/public.css" media="all" />

    <script src="${path}/res/js/my-js/jquery-3.2.1.min.js"></script>
    <link rel='stylesheet' type='text/css' href='${path}/res/css/my-css/dx.spa.css' /> 
    <link rel='stylesheet' type='text/css' href='${path}/res/css/my-css/dx.common.css' /> 
    <link rel='dx-theme' data-theme='generic.<%=theme%>' href='${path}/res/css/my-css/dx.<%=theme%>.css' />
    <link rel="stylesheet" href="${path}/res/css/my-css/jqx.base.css" type="text/css" />
        
    <script src="${path}/res/js/my-js/jqxcore.js"></script>
    <script src="${path}/res/js/my-js/jqxsplitter.js"></script>
    <script src='${path}/res/js/my-js/jszip.min.js'></script> 
    <script src='${path}/res/js/my-js/dx.all.js'></script> 
    <!--<link rel='stylesheet' type ='text/css' href ='${path}/res/css/my-css/styles.css' />--> 

    <link rel="stylesheet" type ='text/css' href="${path}/res/css/public-css/css/font-awesome.min.css">
    <link rel="stylesheet" type ='text/css' href="${path}/res/css/public-css/css/jquery.toolbar.css">
    <link rel="stylesheet" type="text/css" href="${path}/res/modal_dlg/css/normalize.css" />
	<link rel="stylesheet" href="${path}/res/modal_dlg/css/modal_dlg.css">
	<link rel="stylesheet" href="${path}/res/modal_dlg/css/modal_page.css">    

    <script src="${path}/res/js/my-js/viewer.js"></script>
    <link rel="stylesheet" type ='text/css' href="${path}/res/css/my-css/viewer.css">

    <script src="${path}/res/layui/layui.js"></script>
    <script src='${path}/res/js/my-js/util.js'></script> 
    <script src='${path}/res/js/my-js/main.js'></script>
    <script src='${path}/res/js/my-js/pivot_grid.js'></script>
    
    <script src="${path}/res/modal_dlg/js/draggabilly.pkgd.js"></script>
	<script src="${path}/res/modal_dlg/js/modal_dlg.js"></script>
	<script type="text/javascript">
		
	</script>
    
