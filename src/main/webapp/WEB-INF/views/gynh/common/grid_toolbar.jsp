<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="./def.jsp" %>

<meta charset="utf-8">
<div class='long-title' style="height:39px;<%=background%>">
    <div class="<%=layui_bg_class%>" style="z-index:100;position:fixed; top:2px;left:12px;background:white;">
        <input type="text" class="<%=layui_bg_class%>" readonly id="beginTimeBox" style="width:145px;text-align:center">
    </div>
    <div id = "table_toolbar_div" style="z-index:99;height:37px;position:fixed; top:1px;left:163px;"></div>
    <div id = "unit_bar" style="z-index:100;position:fixed; top:2px;right:2px;">
        <font id="font_hms" style="display:none">时长:</font>
        <select id="select_hms" class="<%=layui_bg_class%>" onchange="timeUnitChange()" style="height:22px;width:56px;display:none">
         	<option value="S">秒</option>
        	<option value="M" selected>分钟</option>
            <option value="H">小时</option>
        </select>
        <font id="font_power" style="display:none">Power:</font>
        <select id="select_power" class="<%=layui_bg_class%>" onchange="powerUnitChange(this)" style="height:22px;width:60px;display:none">
            <option value="kWh">kWh</option>
            <option value="mWh">mWh</option>
        </select>
        <font id="font_upw" style="display:none">UPW:</font>
        <select id="select_upw" class="<%=layui_bg_class%>" onchange="UnitChange(this)" style="height:22px;width:62px;display:none">
            <option value="L">L</option>
            <option value="m³">m³</option>
            <option value="k(m³)">k(m³)</option>
        </select>
        <font id="font_cda" style="display:none">CDA:</font>
        <select id="select_cda" class="<%=layui_bg_class%>" onchange="UnitChange(this)" style="height:22px;width:62px;display:none">
            <option value="L">L</option>
            <option value="m³">m³</option>
            <option value="k(m³)">k(m³)</option>
        </select>
        <font id="font_pn2" style="display:none">PN2:</font>
        <select id="select_pn2" class="<%=layui_bg_class%>" onchange="UnitChange(this)" style="height:22px;width:62px;display:none">
            <option value="L">L</option>
            <option value="m³">m³</option>
            <option value="k(m³)">k(m³)</option>
        </select>
    </div>
</div>

