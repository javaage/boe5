<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<meta charset="utf-8">
<div id="shortcut_dlg_div" class="modal" style="width:480px; height:360p;overflow-x: hidden; overflow-y: hidden;">
	<header class="modal-header">
		&nbsp;&nbsp;&nbsp;&nbsp;快捷方式列表
		<button id="shortcut_dlg_close_button" class="modal-header-btn right modal-close" title="Close Modal">X</button>
	</header>
	<div class="modal-body" style="	overflow-x: hidden; overflow-y: hidden;">
		<div class="demo-container" style=" position: absolute; z-index: 1000;">
      			<div id="gridContainer" style="OVERFLOW:auto;WIDTH:100%;height:330px"></div>
  			</div>
		
	</div>
</div>
<div id="chart_style_dlg_div" class="modal" style="width:480px; height:360px;overflow-x: hidden; overflow-y: hidden;">
	<header class="modal-header">
		&nbsp;&nbsp;&nbsp;&nbsp;图表样式
		<button id="chart_style_dlg_close_button" class="modal-header-btn right modal-close" title="Close Modal">X</button>
	</header>
	<div class="modal-body" style="	overflow-x: hidden; overflow-y: hidden;height:330px">
		<div class="demo-container">
      			<div id="grid_chart_style" style="OVERFLOW:auto;WIDTH:100%;height:330px"></div>
  			</div>
		
	</div>
</div>