    <%!
        boolean bDark = false;
    	String is_dark = bDark ? "true" : "false";
        String theme = bDark ? "dark" : "light";
        String background = bDark ? "background:#333333;" : " ";
        String layui_bg_class = bDark ? "layui-bg-black" : " ";
        String font_color = bDark ? "color:white;" : " ";
        String skin_layui_layer = bDark ? "skin:'layui-layer-lan'" : " ";
        String tree_text_color = bDark ? "white" : "#333";
        String page_bg_color = bDark ? "#3d3d3d" : "white";
        String sum_tab_text_color = bDark ? "#ddd" : "black";
        String text_color = bDark ? "white" : "black";
       
        String power_color 	= "#33CCCC";
        String upw_color 	= "#0270C0";
        String cda_color 	= "#007500";
        String pn2_color 	= "#FFC000";

        String product_color 	= "#0066CC";
       
        String str_power = "POWER";
        String str_upw 	 = "UPW";
        String str_cda 	 = "CDA";
        String str_pn2 	 = "PN2";
        
        String str_cub 		= "CUB";
        String str_array 	= "ARRAY";
        String str_cf 	 	= "CF";
        String str_cell 	= "CELL";
        String str_mdl 	 	= "MDL";


        String splitter_style = bDark ? 
        "<style>" + 
			".jqx-fill-state-normal {" + 
			    "border-color: #666666;" + 
			    "background: #555555;" + 
			"}" + 
			".jqx-fill-state-hover {" + 
			    "border-color: #888888;" + 
			    "background: #999999;" + 
			"}" + 
			".jqx-fill-state-pressed {" + 
			    "border-color: #999;" + 
			    "background: #223344;" + 
			"}" + 
		"</style>"
		: " ";
		
		String my_laydate_css = bDark ?  "<link rel='stylesheet' type='text/css' href='${path}/res/css/my-css/my_laydate.css' />":"";
		

    %>
    
    <script>
	    var power_color = '<%=power_color%>';
	    var upw_color 	= '<%=upw_color%>';
	    var cda_color 	= '<%=cda_color%>';
	    var pn2_color 	= '<%=pn2_color%>';
	    
	    var product_color 	= '<%=product_color%>';
	    
	    var energy_colors = {
	    	Power: '<%=power_color%>',
		    UPW: '<%=upw_color%>',
		    CDA: '<%=cda_color%>',
			PN2: '<%=pn2_color%>'
	    };

	    var str_power = '<%=str_power%>';
	    var str_upw   = '<%=str_upw%>';
	    var str_cda   = '<%=str_cda%>';
	    var str_pn2   = '<%=str_pn2%>';
	    
	    var str_cub 	= '<%=str_cub%>';
	    var str_array 	= '<%=str_array%>';
	    var str_cf 	 	= '<%=str_cf%>';
	    var str_cell 	= '<%=str_cell%>';
	    var str_mdl 	= '<%=str_mdl%>';

	    var is_dark = <%=is_dark%>;
	    var ajax_json=true;
    </script>
