<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/inc/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
    <script src='${path}/res/js/my-js/default_value.js'></script> 
	<%@ include file="./common/head.jsp"%>

    <script src='${path}/res/js/my-js/_analysis_data.js'></script> 
	<script type="text/javascript">
		var param1 = '("SPUTTER")';
		var title = "";
		var pageName = 'analysis';
		var path = "${path}";
		var pivotGridId = "#pivot_grid";
		var requestpath = "/gynh/analysis/get_data.do";
		var request_count_path = "/gynh/analysis/get_count.do";
		var layoutStorageKey = "boe-gynh-pivotgrid-analysis";
		var bLoadFromDb = true;
		var filterOutFileds = ["开始时间", "结束时间"];
		var is_zt = false;
		var need_worktime = false;
        var worktime_flag = true;
        var unit_show=null;
		var stateTimeSum;
		
		var cda_dataField= null;
        var power_dataField= null;
        var pn2_dataField= null;
        var upw_dataField= null;
        var time_dataField=null;
        
        chart_style = [];
        
		
		var tableLayout = [
			{
                caption: "分厂",
                dataField: "factory",
                dataType: "string",
                area: "row",
                headerFilter: {
                    allowSearch: true
                },
            }
            ,
            {
                caption: "科室",
                dataField: "department",
                dataType: "string",
                area: "row",
                headerFilter: {
                    allowSearch: true
                },
            }
            ,
            {
                caption: "设备类别",
                dataField: "eqp_type",
                dataType: "string",
                area: "row",
                headerFilter: {
                    allowSearch: true
                },
            }
            ,
            {
                caption: "设备ID",
                dataField: "eqp_id",
                dataType: "string",
                area: "row",
                headerFilter: {
                    allowSearch: true
                },
            }
            ,
            {
                caption: "参数名",
                dataField: "parameter_name",
                dataType: "string",
                area: "row",
                headerFilter: {
                    allowSearch: true
                },
            }
            ,
            /*{
                caption: "lot",
                dataField: "lot",
                dataType: "string",
                area: "row",
                headerFilter: {
                    allowSearch: true
                },
            }
            ,
            {
                caption: "年",
                dataField: "year",
                dataType: "number",
                area: "column",
                headerFilter: {
                    allowSearch: true
                },
            }
            ,
            {
                caption: "月",
                dataField: "month",
                dataType: "number",
                area: "column",
                headerFilter: {
                    allowSearch: true
                },
            }
            ,
            {
                caption: "日",
                dataField: "day",
                dataType: "number",
                area: "column",
                headerFilter: {
                    allowSearch: true
                },
            }
            ,*/
            {
                caption: "相关系数",
                dataField: "para_import_value",
                dataType: "number",
                summaryType: "sum",
                format: {
                    type: "decimal",
                    precision: 9
                },
                area: "data"
            }
		];
		
		function loadOtherData(data) {
			
		}

    </script>

</head>
<body class='dx-viewport' style="overflow:hidden;">
	<div id="rightSplitter" style="margin:0px;padding:0px;border:0px;">
		<div class='pivot-container' style="<%=background%>" > 
			<!-- jsp:include flush="true" page="./common/grid_toolbar.jsp"/ -->
	        <div id='pivot_grid' ></div> 
	        <div id='grid-popup'></div> 
	        <div id='options-popup'></div> 
		</div> 
		<div class='chart-container' style="<%=background%>">
			<div id="echarts_div" style="z-index:1;height:300px;"></div>
		</div>
	</div>

    <!-- for chart -->
	<!--script src="${path}/res/js/echarts/echarts-all.js"></script-->
	<script src="${path}/res/js/echarts/echarts.min.js"></script>
    <script src='${path}/res/js/my-js/table_toolbar.js'></script>
    <script src='${path}/res/js/my-js/chart.js'></script>

	<%=splitter_style%>
	
	<%=my_laydate_css%>>

</body>
</html>