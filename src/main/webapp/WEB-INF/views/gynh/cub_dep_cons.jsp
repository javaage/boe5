<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/inc/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
    <script src='${path}/res/js/my-js/default_values.js'></script> 
    <script src='${path}/res/js/my-js/util.js'></script> 
	<script>
		var defaut_begin_date = getFormatDate(-7);
		var defaut_end_date = getFormatDate(-1);
	</script>
	
	<%@ include file="./common/head.jsp"%>

    <script src='${path}/res/js/my-js/_worktime_data.js'></script> 
    <script src='${path}/res/js/my-js/_CUD_data.js'></script> 
    <script src='${path}/res/js/my-js/worktime.js'></script>
    
    <script type="text/javascript">
        var param1 = ${department};
        var title = "";
        var pageName = 'cub_dep_cons';
        var path = "${path}";
        var pivotGridId = "#pivot_grid";
        var requestpath = "/gynh/cub_dep_cons/get_data.do";
        var request_count_path = "/gynh/cub_dep_cons/get_count.do";
        var layoutStorageKey = "boe-gynh-pivotgrid-cub_dep_cons";
        var bLoadFromDb = true;
        var filterOutFileds = ["开始时间", "结束时间"];
        var is_zt = false;
        var need_worktime = true;
        var worktime_flag = true;
        var unit_show='power';
        var stateTimeSum;
        var cda_dataField= null;
        var power_dataField= 'c';
        var pn2_dataField= null;
        var upw_dataField= null;
        var time_dataField=null;
        
       chart_style = [{
            name: "POWER",
            type:"bar",
            yAxis:"0",
            color:""  
        }];

        var tableLayout = [
            {
                caption: "日期",
                dataType: "string",
                dataField: "day",
                area: "column",
                headerFilter: {
                    allowSearch: true
                }, 
            }
	        ,
      		{
	            caption: "系统",
	            dataType: "string",
	            dataField: "system",
	            area: "row",
                headerFilter: {
                    allowSearch: true
                }, 
     		}
	        ,
      		{
	            caption: "分厂",
	            dataType: "string",
	            dataField: "to_factory",
	            area: "row",
                headerFilter: {
                    allowSearch: true
                }, 
     		}
            ,
          	{
	            caption: "点位",
	            dataType: "string",
	            dataField: "c3",
	            area: "row",
                headerFilter: {
                    allowSearch: true
                }, 
     		}
	        ,
      		{
	            caption: "地址",
	            dataType: "string",
	            dataField: "point_number",
	            area: "filter",
                headerFilter: {
                    allowSearch: true
                }, 
     		}
			,      
	        {
	            caption: "POWER",
	            dataField: "c",
	            dataType: "number",
	            summaryType: "sum",
	            format: {
	            	type: "fixedPoint",
	            	precision: 2
	            },
	            area: "data"
			}
		];
    </script>
    
</head>
<body class='dx-viewport' style="overflow:hidden;">
	<jsp:include flush="true" page="./common/shortcut.jsp"/>
	<div id="rightSplitter" style="margin:0px;padding:0px;border:0px;">
		<div class='pivot-container' style="<%=background%>" > 
			<jsp:include flush="true" page="./common/grid_toolbar.jsp"/>
	        <div id='pivot_grid' ></div> 
	        <div id='grid-popup'></div> 
	        <div id='options-popup'></div> 
		</div> 
		<div class='chart-container' style="<%=background%>">
			<div id="echarts_div" style="z-index:1;height:300px;"></div>
			<c:import url="CUDiagram.jsp"></c:import>
		</div>
	</div>
	<div class="tip tooltip-content clearfix" style="position: absolute;display: none;z-index:999;">&nbsp; |<br><span class="pointQTY">265</span></div>

    <!-- for chart -->
	<!--script src="${path}/res/js/echarts/echarts-all.js"></script-->
	<script src="${path}/res/js/echarts/echarts.min.js"></script>
    <script src='${path}/res/js/my-js/table_toolbar.js'></script>
    <script src='${path}/res/js/my-js/chart.js'></script>
    <script src='${path}/res/js/my-js/worktime_chart.js'></script>

	<%=splitter_style%>

	<%=my_laydate_css%>>
	
</body>
</html>