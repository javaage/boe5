<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/inc/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
    <script src='${path}/res/js/my-js/default_values.js'></script> 
	<%@ include file="./common/head.jsp"%>

    <script src='${path}/res/js/my-js/_worktime_data.js'></script> 
    <script src='${path}/res/js/my-js/worktime.js'></script>
    
	<script type="text/javascript">
		var param1 = '("SPUTTER")';
		var title = "";
		var pageName = 'worktime';
		var path = "${path}";
		var pivotGridId = "#pivot_grid";
		var requestpath = "/gynh/worktime/get_data.do";
		var request_count_path = "/gynh/worktime/get_count.do";
		var layoutStorageKey = "boe-gynh-pivotgrid-worktime";
		var bLoadFromDb = true;
		var filterOutFileds = ["开始时间", "结束时间"];
		var is_zt = false;

		var stateTimeSum;

		var tableLayout = [
            {
                caption: "分厂",
                dataType: "string",
                dataField: "fc",
                area: "filter",
                headerFilter: {
                    allowSearch: true
                }, 
            }
			,
            {
                caption: "科室",
                dataType: "string",
                dataField: "dp",
                area: "filter",
                headerFilter: {
                    allowSearch: true
                }, 
            }
            ,
            {
                caption: "产线",
                dataType: "string",
                dataField: "ln",
                area: "row",
                headerFilter: {
                    allowSearch: true
                }, 
            }
			,
      		{
	            caption: "设备ID",
	            dataType: "string",
	            dataField: "dv",
	            area: "row",
                headerFilter: {
                    allowSearch: true
                }, 
     		}
	        ,
	        {
	            caption: "状态类型",
	            dataField: "sc",
	            dataType: "string",
	            area: "row",
                headerFilter: {
                    allowSearch: true
                },
	        }
	        ,
	        {
	            caption: "状态",
	            dataField: "st",
	            dataType: "string",
	            area: "row",
                headerFilter: {
                    allowSearch: true
                },
	        }
            ,
            {
                caption: "年",
                dataField: "y",
                dataType: "number",
                area: "column",
                headerFilter: {
                    allowSearch: true
                },
            }
            ,
            {
                caption: "月",
                dataField: "m",
                dataType: "number",
                area: "column",
                headerFilter: {
                    allowSearch: true
                },
            }
            ,
            {
                caption: "日",
                dataField: "d",
                dataType: "number",
                area: "column",
                headerFilter: {
                    allowSearch: true
                },
            }
            ,
            {
                caption: "时间",
                width:140,
                dataType: "string",
                dataField: "t",
                area: "row",
                headerFilter: {
                    allowSearch: true
                }, 
            }
	        ,
	        {
	            caption: "时长(分钟)",
	            dataField: "tl",
	            dataType: "number",
	            summaryType: "sum",
	            format: {
	            	type: "fixedPoint",
	            	precision: 2
	            },
	            area: "data"
			}
	        ,
	        {
	            caption: "POWER(kWh)",
	            dataField: "pwr",
	            dataType: "number",
	            summaryType: "sum",
	            format: {
	            	type: "fixedPoint",
	            	precision: 0
	            },
	            area: "data"
			}
	        ,
	        {
	            caption: "DIW(L)",
	            dataField: "diw",
	            dataType: "number",
	            summaryType: "sum",
	            format: {
	            	type: "fixedPoint",
	            	precision: 0
	            },
	            area: "data"
			},
	        {
	            caption: "CDA(L)",
	            dataField: "cda",
	            dataType: "number",
	            summaryType: "sum",
	            format: {
	            	type: "fixedPoint",
	            	precision: 0
	            },
	            area: "data"
			}
	        ,
	        {
	            caption: "PN2(L)",
	            dataField: "pn2",
	            dataType: "number",
	            summaryType: "sum",
	            format: {
	            	type: "fixedPoint",
	            	precision: 0
	            },
	            area: "data"
			}
	        
		];

    </script>
    
</head>
<body class='dx-viewport' style="overflow:hidden;">
	<jsp:include flush="true" page="./common/shortcut.jsp"/>
	<div id="rightSplitter" style="margin:0px;padding:0px;border:0px;">
		<div class='pivot-container' style="<%=background%>" > 
			<jsp:include flush="true" page="./common/grid_toolbar.jsp"/>
	        <div id='pivot_grid' ></div> 
	        <div id='grid-popup'></div> 
	        <div id='options-popup'></div> 
		</div> 
		<div class='chart-container' style="background:#333333">
			<div id = "echarts_div" style="height:300px;"></div>
		</div>
	</div>

    <!-- for chart -->
	<!--script src="${path}/res/js/echarts/echarts-all.js"></script-->
	<script src="${path}/res/js/echarts/echarts.min.js"></script>
    <script src='${path}/res/js/my-js/table_toolbar.js'></script>
    <script src='${path}/res/js/my-js/chart.js'></script>
    <script src='${path}/res/js/my-js/worktime_chart.js'></script>

	<%=splitter_style%>

</body>
</html>