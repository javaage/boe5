<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>

<div id='cub_sum_charts_div'>
	<table border="1" width=99.7% height=99.7% >
		<tr height=20px>
			<td id='hvac_sys_daily_td'>
			HVAC
			</td>
			<td id='water_sys_daily_td'>
			WATER
			</td>
			<td id='gc_sys_daily_td'>
			G&C
			</td>
		</tr>
		<tr>
			<td id = 'hvac_sys_daily_chart_td'>
				<div id='hvac_sys_daily_chart' style='margin:0;padding:0;color:white;'></div>
			</td>
			<td id = 'water_sys_daily_chart_td'>
				<div id='water_sys_daily_chart' style='margin:0;padding:0;color:white;'></div>
			</td>
			<td id = 'gc_sys_daily_chart_td'>
				<div id='gc_sys_daily_chart' style='margin:0;padding:0;color:white;'></div>
			</td>
		</tr>
		<tr height=20px>
			<td id='hvac_unit_cons_td'>
			CHS
			</td>
			<td id='water_unit_cons_td'>
			UPW
			</td>
			<td id='gc_unit_cons_td'>
			CDA
			</td>
		</tr>
		<tr>
			<td id='hvac_unit_cons_chart_td'>
				<div id='hvac_unit_cons_chart' style='margin:0;padding:0;color:white;'></div>
			</td>
			<td id='water_unit_cons_chart_td'>
				<div id='water_unit_cons_chart' style='margin:0;padding:0;color:white;'></div>
			</td>
			<td id='gc_unit_cons_chart_td'>
				<div id='gc_unit_cons_chart' style='margin:0;padding:0;color:white;'></div>
			</td>
		</tr>
		<tr height=20px>
			<td id='hvac_waring_td'>
			HVAC
			</td>
			<td id='water_waring_td'>
			WATER
			</td>
			<td id='gc_waring_td'>
			G&C
			</td>
		</tr>
		<tr>
			<td id='hvac_waring_chart_td'>
				<div id='hvac_waring_chart' style='margin:0;padding:0;color:white;'></div>
			</td>
			<td id='water_waring_chart_td'>
				<div id='water_waring_chart' style='margin:0;padding:0;color:white;'></div>
			</td>
			<td id='gc_waring_chart_td'>
				<div id='gc_waring_chart' style='margin:0;padding:0;color:white;'></div>
			</td>
		</tr>
	</table>
</div>

<style>

</style>

<script>

</script>		
