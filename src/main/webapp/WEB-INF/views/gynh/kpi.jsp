<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/inc/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
    <script src='${path}/res/js/my-js/default_values.js'></script> 
	<script type="text/javascript">
		percent_fileds = ["实际/定额"];
		line_fileds = ["实际/定额", "定额月累计"];

		var bDataFieldAreaInRow = true;
    	var pageName = 'kpi';
        var temp_date = new Date();
		var defaut_begin_date = temp_date.getFullYear() + '-01-01';
		var defaut_end_date = defaut_begin_date

	</script>
	
	<%@ include file="./common/head.jsp"%>

    <script src='${path}/res/js/my-js/_worktime_data.js'></script> 
    <script src='${path}/res/js/my-js/_CUD_data.js'></script> 
    <script src='${path}/res/js/my-js/worktime.js'></script>
    
    <script type="text/javascript">
        var param1 = ${kpi};
        var title = "";
        var path = "${path}";
        var pivotGridId = "#pivot_grid";
        var requestpath = "/gynh/kpi/get_kpi_plan_real_cons.do";
        var request_count_path = "/gynh/kpi/get_count.do";
        var layoutStorageKey = "boe-gynh-pivotgrid-kpi";
        var bLoadFromDb = true;
        var filterOutFileds = [];
        var is_zt = false;
        var need_worktime = true;
        var worktime_flag = false;
        var unit_show="power_cda_upw_pn2";
        var stateTimeSum;
        var cda_dataField= "c";
        var power_dataField= "c";
        var pn2_dataField= "c";
        var upw_dataField= "c";
        var time_dataField=null;
        
        chart_style = [{
            name: "定 额",
            type:"bar",
            yAxis:"0",
            color:""  
        }, {
        	name: "实 际",
            type:"bar",
            yAxis:"0",
            color:""  
        }, {
        	name: "定额累计",
            type:"line",
            yAxis:"0",
            color:""  
        }, {
        	name: "实际累计",
            type:"bar",
            yAxis:"0",
            color:""  
        }, {
        	name: "实际/定额",
            type:"bar",
            yAxis:"3",
            color:""  
        }];
        
        var tableLayout = [
            {
                caption: "能源",
                dataType: "string",
                dataField: "e",
                area: "row",
                headerFilter: {
                    allowSearch: true,
                    height: 500,
                }, 
            }
            ,
            {
                caption: "年",
                dataType: "string",
                dataField: "y",
                area: "filter",
                headerFilter: {
                    allowSearch: true,
                    height: 500,
                }, 
            }
            ,
            {
                caption: "月",
                dataType: "string",
                dataField: "m",
                area: "column",
                headerFilter: {
                    allowSearch: true,
                    height: 500,
                }, 
            }
            ,
            {
                caption: "日",
                dataType: "string",
                dataField: "d",
                area: "column",
                headerFilter: {
                    allowSearch: true,
                    height: 500,
                }, 
            }
            ,
	        {
	            caption: "定 额",
	            dataField: "pc",
	            dataType: "number",
	            summaryType: "sum",
	            format: {
	            	type: "fixedPoint",
	            	precision: 0
	            },
	            area: "data"
			}
            ,
	        {
	            caption: "实 际",
	            dataField: "rc",
	            dataType: "number",
	            summaryType: "sum",
	            format: {
	            	type: "fixedPoint",
	            	precision: 0
	            },
	            area: "data"
			}
			,
	        {
	            caption: "定额累计",
	            dataField: "pa",
	            dataType: "number",
	            summaryType: "max",
	            format: {
	            	type: "fixedPoint",
	            	precision: 0
	            },
	            area: "data"
			}
			,
	        {
	            caption: "实际累计",
	            dataField: "ra",
	            dataType: "number",
	            summaryType: "max",
	            format: {
	            	type: "fixedPoint",
	            	precision: 0
	            },
	            area: "data"
			}
			,
			{
				caption: "实际/定额",
	            area: "data",
	            summaryType: "avg",
	            selector: function(data) {
	                return data.pc != 0 ? data.rc / data.pc : 0;
	            },
	            format: {
	            	type: "percent",
	            	precision: 2
	            }
			}
            
		];
        if(ajax_json){
        	main_ajax_url=path + '/res/json/main1.json';
        }
    </script>
    
</head>
<body class='dx-viewport' style="overflow:hidden;">
	<jsp:include flush="true" page="./common/shortcut.jsp"/>
	<div id="rightSplitter" style="margin:0px;padding:0px;border:0px;">
		<div class='pivot-container' style="<%=background%>" > 
			<jsp:include flush="true" page="./common/grid_toolbar.jsp"/>
	        <div id='pivot_grid' ></div> 
	        <div id='grid-popup'></div> 
	        <div id='options-popup'></div> 
		</div> 
		<div class='chart-container' style="<%=background%>">
			<div id="echarts_div" style="z-index:1;height:300px;"></div>
			<c:import url="CUDiagram.jsp"></c:import>
		</div>
	</div>
	<div class="tip tooltip-content clearfix" style="position: absolute;display: none;z-index:999;">&nbsp; |<br><span class="pointQTY">265</span></div>

    <!-- for chart -->
	<!--script src="${path}/res/js/echarts/echarts-all.js"></script-->
	<script src="${path}/res/js/echarts/echarts.min.js"></script>
    <script src='${path}/res/js/my-js/table_toolbar.js'></script>
    <script src='${path}/res/js/my-js/chart.js'></script>
    <script src='${path}/res/js/my-js/worktime_chart.js'></script>

	<%=splitter_style%>

	<%=my_laydate_css%>>
	
</body>
</html>