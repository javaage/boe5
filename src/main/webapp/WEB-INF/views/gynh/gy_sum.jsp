<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/inc/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
    <script src='${path}/res/js/my-js/default_values.js'></script> 
    <%@ include file="./common/def.jsp" %>
	<meta charset="utf-8">
	<title></title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" href="${path}/res/layui/css/layui.css" media="all" />
	<link rel="stylesheet" href="${path}/res/css/one-css/font_eolqem241z66flxr.css" media="all" />
	<link rel="stylesheet" href="${path}/res/css/one-css/main.css" media="all" />
	
	
	<script src="${path}/res/js/my-js/jquery-3.2.1.min.js"></script>
    <link rel='stylesheet' type='text/css' href='${path}/res/css/my-css/dx.spa.css' /> 
    <link rel='stylesheet' type='text/css' href='${path}/res/css/my-css/dx.common.css' /> 
    <link rel='dx-theme' data-theme='generic.<%=theme%>' href='${path}/res/css/my-css/dx.<%=theme%>.css' /> 
    <script src='${path}/res/js/my-js/dx.all.js'></script> 
    
    <script src='${path}/res/js/my-js/util.js'></script> 
 
 	<style>
		html,body{
			height: 100%;
			width: 100%;
			margin: 0;
			padding: 0;
			background:#3d3d3d;
		}
		td {
			border-top:3px solid black;
			border-right:3px solid black;
			border-bottom:3px solid black;
			border-left:3px solid black;
			<%=background%>;
		}
	</style>
    
	<script type="text/javascript">
		var pageName = 'summary';
		var param1 = '("SPUTTER")';
		var path = "${path}";
    	var is_dark = <%=is_dark%>;

		var eqpList = [{
		    "ID": 0,
		    "Name": "5ATSP01",
		}, {
		    "ID": 1,
		    "Name": "5ATSP02",
		}, {
		    "ID": 2,
		    "Name": "5ATSP03",
		}, {
		    "ID": 3,
		    "Name": "5ATSP04",
		}, {
		    "ID": 4,
		    "Name": "5ATSP05",
		}, {
		    "ID": 5,
		    "Name": "5ATSP06",
		}, {
		    "ID": 6,
		    "Name": "5ATSP07",
		}, {
		    "ID": 7,
		    "Name": "5ATSP08",
		}, {
		    "ID": 8,
		    "Name": "5ATSP09",
		}, {
		    "ID": 9,
		    "Name": "5ATSP10",
		}, {
		    "ID": 10,
		    "Name": "5ATSP11",
		}, {
		    "ID": 11,
		    "Name": "5ATSP12",
		}, {
		    "ID": 12,
		    "Name": "5ATSP13",
		} ];
		
	
		var timerId = 0;
		var eqpBox;
		var autoPlayBox;
		var init_begin_date = '2018-09-01';//getFormatDate(-73);// -1
		
		$(function() {
			eqpBox = $("#eqp_list").dxTagBox({
		        items: eqpList,
		        value: [],
		        displayExpr: "Name",
		        valueExpr: "ID",
		        showSelectionControls: true,
		        maxDisplayedTags: 1,
		        height:22,
		        placeholder:"设备列表",
		        selectAllText:"全选",
				onValueChanged: function(args) {
					if (args.value.length == 0) {
						autoPlayBox.option('value', false);
					}
					updateEqpChart();
				},
				onOpened: function(args) {
					autoPlayBox.option('value', false);
				},
		        onMultiTagPreparing: function(args) {
	                args.text = args.text.replace("selected", "个设备");
		        },
		        
		    }).dxTagBox("instance");
			
			autoPlayBox = $("#auto_play").dxCheckBox({
		        value: true,
		        width: 80,
		        text: "轮播",
				onValueChanged: function(args) {
					if (args.value) {
						nextEqp();
						timerId = self.setInterval("ontime()",10000);
					} else {
						window.clearInterval(timerId);
					}
				},
		    }).dxCheckBox("instance");
			
			// init date control
			layui.use('laydate', function() {
				var laydate = layui.laydate;
				laydate.render({
					elem: '#beginTimeBox', // 指定元素
					theme: is_dark?'#444444':'',
					type: 'date',
					value: init_begin_date,
					done: function(value, date, endDate) {
						loadSummaryData(value, value);
					}

				});
				/*
				laydate.render({
					elem: '#endTimeBox', // 指定元素
					type: 'date',
					value: init_end_date,
				});
				*/
			});

			nextEqp();
			loadSummaryData(init_begin_date, init_begin_date);
			timerId = self.setInterval("ontime()",10000);
		});
		
		window.onload = function() {
		};
		
		function ontime() {
			nextEqp();
		}
		
		function prevEqp() {
			var val = eqpBox.option().value;
			if (val.length != 1) {
				eqpBox.option('value', [0]);
			} else {
				val[0]--;
				if (val[0] < eqpList[0].ID) val[0] = eqpList[eqpList.length-1].ID;
				eqpBox.option('value', val);
			}
		}
		
		function nextEqp() {
			var val = eqpBox.option().value;
			if (val.length != 1) {
				eqpBox.option('value', [0]);
			} else {
				val[0]++;
				if (val[0] > eqpList[eqpList.length-1].ID) val[0] = eqpList[0].ID;
				eqpBox.option('value', val);
			}
		}
		
		function beginDateCallBack() {
			alert(1);
		}

		
	</script>
	
</head>

<body class="childrenBody">

	<table border="0" width=99.7% height=99.7% >
		<tr height=43px>
			<td colspan="2">
				<div class='long-title' style="height:38px;<%=background%>;font-size: 13px;">
					<div class="<%=layui_bg_class%>" style="z-index:100;position:fixed; top:6px;left:6px;background:white;">
						<input type="text" class="<%=layui_bg_class%>" readonly id="beginTimeBox" style="width:68px;">
					</div>
					<div id = "table_toolbar_div" style="z-index:99;width:200px;height:37px;position:fixed; top:5px;left:76px;"></div>
					<!--div id = "unit_bar" style="z-index:100;position:fixed; top:6px;right:20px;">
						<font style="color:white;">时长:</font>
						<select id="select_hms" class="< %=layui_bg_class% >" onchange="timeUnitChange()" style="height:22px;width:50px;">
							<option value="H">小时</option>
							<option value="M" selected>分钟</option>
							<option value="S">秒</option>
						</select>
						<font style="color:white;">Power:</font>
						<select id="select_power" class="< %=layui_bg_class% >" onchange="powerUnitChange(this)" style="height:22px;width:57px;">
							<option value="kWh">kWh</option>
							<option value="mWh">mWh</option>
						</select>
						<font style="color:white;">DIW:</font>
						<select id="select_diw" class="< %=layui_bg_class% >" onchange="UnitChange(this)" style="height:22px;width:62px;">
							<option value="L">L</option>
							<option value="m³">m³</option>
							<option value="k(m³)">k(m³)</option>
						</select>
						<font style="color:white;">CDA:</font>
						<select id="select_cda" class="< %=layui_bg_class% >" onchange="UnitChange(this)" style="height:22px;width:62px;">
							<option value="L">L</option>
							<option value="m³">m³</option>
							<option value="k(m³)">k(m³)</option>
						</select>
						<font style="color:white;">PN2:</font>
						<select id="select_pn2" class="< %=layui_bg_class% >" onchange="UnitChange(this)" style="height:22px;width:62px;">
							<option value="L">L</option>
							<option value="m³">m³</option>
							<option value="k(m³)">k(m³)</option>
						</select>
					</div-->
				</div>
			
				<input type="image" name="" src=${path}/res/images/toolbarbtn/prev.png onclick="prevEqp()" value=" " style="height:22px;z-index:102;position:fixed;top:6px;left:119px;"/>
				<input type="image" name="" src=${path}/res/images/toolbarbtn/next.png onclick="nextEqp()" value=" " style="height:22px;z-index:102;position:fixed;top:6px;left:276px;"/>
				
				<div class="dx-field" style="height:22px;z-index:102;position:fixed;top:0px;left:336px;">
                    <div class="dx-field-value">
                        <div id="auto_play" style="font-size:13px;"></div>
                    </div>
                </div>
				
				<div class="dx-fieldset" style="margin:0;padding:0;z-index:101;width:170px;height:30px;min-height:30px;position:fixed; top:6px;left:106px;">
		            <div class="dx-field" style="min-height:20px;">
		                <div class="dx-field-label"style="height:0px;width:0px;"></div>
		                <div class="dx-field-value" style="height:30px;font-size:10px;">
		                    <div id="eqp_list" style="font-size:13px;padding:0;margin:0;"></div>
		                </div>
		            </div>
				</div>
			
			</td>
		</tr>
		<tr height=20px>
			<td colspan="2">
				<div id="state_chart" style="width:100%;height:100%;"></div>
			</td>
		</tr>
		<tr>
			<td width="240px">
				<div class="panel col" style="">
					<a href="javascript:;" data-url="page/user/allUsers.html">
						<div class="panel_icon" style="background-color:#333333;">
							<img src="${path}/res/images/POWER.png"  width="89px" alt="" />
						</div>
						<div class="panel_word" style="color:#D81E06;">
							<cite style="font-size:21px;">POWER</cite>
							<span id='power_count'>0</span>
							<div id="power_chart" style="width:100%;height:20px;"></div>
						</div>
					</a>
				</div>
			</td>
			<td>
				<div id='power_hist_chart' style='margin:0;padding:0;width:100%;height:130px;color:white;'></div>
			</td>
		</tr>
		<tr>
			<td>
				<div class="panel col" style="">
					<a href="javascript:;" data-url="page/message/message.html">
						<div class="panel_icon" style="background-color:#333333;">
							<img src="${path}/res/images/DIW.png"  width="89px" alt="" />
						</div>
						<div class="panel_word" style="color:#0555FB;">
							<cite style="font-size:23px;">DIW</cite>
							<span id='diw_count'>0</span>
							<div id="diw_chart" style="width:100%;height:20px;"></div>
						</div>
					</a>
				</div>
			</td>
			<td>
				<div id='diw_hist_chart' style='width:100%;height:130px;color:white;'></div>
			</td>
		</tr>
		<tr>
			<td>
				<div class="panel col" style="">
					<a href="javascript:;" data-url="page/user/allUsers.html">
						<div class="panel_icon" style="background-color:#333333;">
							<img src="${path}/res/images/CDA.png"  width="80px" alt="" />
						</div>
						<div class="panel_word" style="color:#1AFA29;">
							<cite style="font-size:23px;">CDA</cite>
							<span id='cda_count'>0</span>
							<div id="cda_chart" style="width:100%;height:20px;"></div>
						</div>
					</a>
				</div>
			</td>
			<td>
				<div id='cda_hist_chart' style='width:100%;height:130px;color:white;'></div>
			</td>
		</tr>
		<tr>
			<td>
				<div class="panel col" style="">
					<a href="javascript:;" data-url="page/img/images.html">
						<div class="panel_icon" style="background-color:#333333;">
							<img src="${path}/res/images/PN2.png"  width="80px" alt="" />
						</div>
						<div class="panel_word" style="color:#F4EA2A;">
							<cite style="font-size:23px;">PN2</cite>
							<span id='pn2_count'>0</span>
							<div id="pn2_chart" style="width:100%;height:20px;"></div>
						</div>
					</a>
				</div>
			</td>
			<td>
				<div id='pn2_hist_chart' style='width:100%;height:130px;color:white;'></div>
			</td>
		</tr>
	</table>

	<script src="${path}/res/layui/layui.js"></script>
	<script src="${path}/res/js/echarts/echarts.min.js"></script>
    <script src='${path}/res/js/my-js/summary_toolbar.js'></script>
    
	<script type="text/javascript">
		var powerChart = echarts.init(document.getElementById('power_chart'));
		var powerOption = {
			title: {text:''},
		    xAxis:  {
		        show:false,
		        max: 'dataMax',
		        type: 'value'
		    },
		    yAxis: {
		        show:false,
		        type: 'category',
		        data: ['']
		    },
			series: [
				{
		            name: 'RUN',
		            type: 'bar',
		            stack: '总量',
		            label: {
		                normal: {
		                    show: false,
		                    position: 'insideRight'
		                }
		            },
		            data: [0]
		        },
		        {
		            name: 'PM',
		            type: 'bar',
		            stack: '总量',
		            label: {
		                normal: {
		                    show: false,
		                    position: 'insideRight'
		                }
		            },
		            data: [0]
		        },
	        ],
			color:['#00ff8b', '#F26067']
		};
		powerChart.setOption(powerOption);
		
		var diwChart = echarts.init(document.getElementById('diw_chart'));
		var diwOption = {
			title: {text:''},
		    xAxis:  {
		        show:false,
		        max: 'dataMax',
		        type: 'value'
		    },
		    yAxis: {
		        show:false,
		        type: 'category',
		        data: ['']
		    },
			series: [
				{
		            name: 'RUN',
		            type: 'bar',
		            stack: '总量',
		            label: {
		                normal: {
		                    show: false,
		                    position: 'insideRight'
		                }
		            },
		            data: [0]
		        },
		        {
		            name: 'PM',
		            type: 'bar',
		            stack: '总量',
		            label: {
		                normal: {
		                    show: false,
		                    position: 'insideRight'
		                }
		            },
		            data: [0]
		        },
	        ],
			color:['#00ff8b', '#F26067']
		};
		diwChart.setOption(diwOption);

		var cdaChart = echarts.init(document.getElementById('cda_chart'));
		var cdaOption = {
			title: {text:''},
		    xAxis:  {
		        show:false,
		        max: 'dataMax',
		        type: 'value'
		    },
		    yAxis: {
		        show:false,
		        type: 'category',
		        data: ['']
		    },
			series: [
				{
		            name: 'RUN',
		            type: 'bar',
		            stack: '总量',
		            label: {
		                normal: {
		                    show: false,
		                    position: 'insideRight'
		                }
		            },
		            data: [0]
		        },
		        {
		            name: 'PM',
		            type: 'bar',
		            stack: '总量',
		            label: {
		                normal: {
		                    show: false,
		                    position: 'insideRight'
		                }
		            },
		            data: [0]
		        },
	        ],
			color:['#00ff8b', '#F26067']
		};
		cdaChart.setOption(cdaOption);

		var pn2Chart = echarts.init(document.getElementById('pn2_chart'));
		var pn2Option = {
			title: {text:''},
		    /*tooltip: {
		        trigger: 'item',
		        position: ['50%', '0%'],
	        	formatter : function(params, ticket, callback) {
	                var percent = params.value/(pn2Option.series[0].data[0] + pn2Option.series[1].data[0])*100;
	                return  params.seriesName +" " + params.name + ": " + params.value + ", (" + percent.toFixed(2) + "%)";        
	            },
		    },*/
		    xAxis:  {
		        show:false,
		        max: 'dataMax',
		        type: 'value'
		    },
		    yAxis: {
		        show:false,
		        type: 'category',
		        data: ['']
		    },
			series: [
				{
		            name: 'RUN',
		            type: 'bar',
		            stack: '总量',
		            label: {
		                normal: {
		                    show: false,
		                    position: 'insideRight'
		                }
		            },
		            data: [0]
		        },
		        {
		            name: 'PM',
		            type: 'bar',
		            stack: '总量',
		            label: {
		                normal: {
		                    show: false,
		                    position: 'insideRight'
		                }
		            },
		            data: [0]
		        },
	        ],
			color:['#00ff8b', '#F26067']
		};
		pn2Chart.setOption(diwOption);

		var stateChart = echarts.init(document.getElementById('state_chart'));
		var stateOption = {
			title: {text:''},
		    tooltip: {
		        trigger: 'item',
		        position: ['50%', '0%'],
	        	formatter : function(params, ticket, callback) {
	                var percent = params.value/(stateOption.series[0].data[0] + stateOption.series[1].data[0])*100;
	                return params.seriesName +" " + params.name + ": "
	                //+ params.value
	                //+ ", ("
	                + percent.toFixed(2) + "%"
	                //+ ")"
	                ;        
	            },
		    },
		    grid: {
    	        left: '3px',
    	        right: '0%',
    	        bottom: '0%',
    	        top: '0%'
		    },
		    xAxis:  {
		        show:false,
		        type: 'value',
		        max: 'dataMax',
		    },
		    yAxis: {
		        show:false,
		        type: 'category',
		        data: ['状态时间']
		    },
			series: [
				{
		            name: 'RUN',
		            type: 'bar',
		            stack: '总量',
		            label: {
		                normal: {
		                    show: false,
		                }
		            },
		            data: [1]
		        },
		        {
		            name: 'PM',
		            type: 'bar',
		            stack: '总量',
		            label: {
		                normal: {
		                    show: false,
		                }
		            },
		            data: [1]
		        },
	        ],
			color:['#00ff8b', '#F26067']
		};
		stateChart.setOption(stateOption);
		
		
		var powerHistChart = echarts.init(document.getElementById('power_hist_chart'), "<%=theme%>");
		var powerHistChartOption = {
		    tooltip: {
		        trigger: 'axis'
		    },
		    /*legend: {
		        textStyle: {color:'white'},
		        data:['5ATSP01','5ATSP01平均','5ATSP02','5ATSP02平均','5ATSP03','5ATSP03平均'
		             ,'5ATSP04','5ATSP01平均','5ATSP05','5ATSP05平均','5ATSP06','5ATSP06平均'
		             ,'5ATSP07','5ATSP07平均','5ATSP08','5ATSP08平均','5ATSP09','5ATSP09平均'
		             ,'5ATSP10','5ATSP10平均','5ATSP11','5ATSP11平均','5ATSP12','5ATSP12平均'
		             ,'5ATSP13','5ATSP13平均']
		    },*/
		    grid: {
		        left: '65px',
		        right: '55px',
		        bottom: '20px',
		        top:'8px',
		        containLabel: false,
		    },
		    xAxis: {
				axisLabel: {interval: 0},
		        textStyle: {color:'white'},
		        type: 'category',
		        boundaryGap: false,
		        data: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31]
		    },
		    yAxis: {
		        textStyle: {color:'white'},
		        type: 'value',
		        axisLabel:{
		            inside: false,
		        },
		    },
		    color: ['SpringGreen', 'Tomato', 'Yellow', 'DodgerBlue', 'Sienna', 'MediumOrchid', 'Orange', 'SkyBlue', 'MediumTurquoise', 'Indigo', 'gray', 'blue', 'green'],
		    series: [
		        {
		            name:'',
		            type:'line',
		            data:["-"]
		        }
		    ]
		};

		powerHistChart.setOption(powerHistChartOption);
		
		var diwHistChart = echarts.init(document.getElementById('diw_hist_chart'), "<%=theme%>");
		var diwHistChartOption = {
		    tooltip: {
		        trigger: 'axis'
		    },
		    grid: {
		        left: '65px',
		        right: '55px',
		        bottom: '20px',
		        top:'8px',
		        containLabel: false,
		    },
		    xAxis: {
				axisLabel: {interval: 0},
		        textStyle: {color:'white'},
		        type: 'category',
		        boundaryGap: false,
		        data: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31]
		    },
		    yAxis: {
		        textStyle: {color:'white'},
		        type: 'value',
		        axisLabel:{
		            inside: false,
		        },
		    },
		    color: ['SpringGreen', 'Tomato', 'Yellow', 'DodgerBlue', 'Sienna', 'MediumOrchid', 'Orange', 'SkyBlue', 'MediumTurquoise', 'Indigo', 'gray', 'blue', 'green'],
		    series: [
		        {
		            name:'',
		            type:'line',
		            data:["-"]
		        }
		    ]
		};

		diwHistChart.setOption(diwHistChartOption);
		
		var cdaHistChart = echarts.init(document.getElementById('cda_hist_chart'), "<%=theme%>");
		var cdaHistChartOption = {
		    tooltip: {
		        trigger: 'axis'
		    },
		    grid: {
		        left: '65px',
		        right: '55px',
		        bottom: '20px',
		        top:'8px',
		        containLabel: false,
		    },
		    xAxis: {
				axisLabel: {interval: 0},
		        textStyle: {color:'white'},
		        type: 'category',
		        boundaryGap: false,
		        data: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31]
		    },
		    yAxis: {
		        textStyle: {color:'white'},
		        type: 'value',
		        axisLabel:{
		            inside: false,
		        },
		    },
		    color: ['SpringGreen', 'Tomato', 'Yellow', 'DodgerBlue', 'Sienna', 'MediumOrchid', 'Orange', 'SkyBlue', 'MediumTurquoise', 'Indigo', 'gray', 'blue', 'green'],
		    series: [
		        {
		            name:'',
		            type:'line',
		            data:["-"]
		        }
		    ]
		};

		cdaHistChart.setOption(cdaHistChartOption);
		
		var pn2HistChart = echarts.init(document.getElementById('pn2_hist_chart'), "<%=theme%>");
		var pn2HistChartOption = {
		    tooltip: {
		        trigger: 'axis'
		    },
		    grid: {
		        left: '65px',
		        right: '55px',
		        bottom: '20px',
		        top:'8px',
		        containLabel: false,
		    },
		    xAxis: {
				axisLabel: {interval: 0},
		        textStyle: {color:'white'},
		        type: 'category',
		        boundaryGap: false,
		        data: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31]
		    },
		    yAxis: {
		        textStyle: {color:'white'},
		        type: 'value',
		        axisLabel:{
		            inside: false,
		        },
		    },
		    color: ['SpringGreen', 'Tomato', 'Yellow', 'DodgerBlue', 'Sienna', 'MediumOrchid', 'Orange', 'SkyBlue', 'MediumTurquoise', 'Indigo', 'gray', 'blue', 'green'],
		    series: [
		        {
		            name:'',
		            type:'line',
		            data:["-"]
		        }
		    ]
		};

		pn2HistChart.setOption(pn2HistChartOption);

	</script>
</body>

	<%=my_laydate_css%>>
	<style>
		.dx-tag-content {
			padding: 0px 30px 0px 10px;
			margin:1px 10px 1px 10px;
		}
		.dx-placeholder:before {
		    padding: 1px 9px 8px;
		}
		.dx-list-select-all-label {
		    line-height: 1;
		    padding: 0 10px;
		    margin-top: 2px;
		    font-size:12px;
		}
		.dx-field-label {
		    width: 1%;
	    }
		.dx-field-value:not(.dx-switch):not(.dx-checkbox):not(.dx-button), .dx-field-value-static {
		    width: 80%;
		}
		
	</style>

</html>