<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/inc/taglibs.jsp"%>

<!DOCTYPE HTML>
<html>
  <head>
  	<%@ include file="./common/def.jsp" %>
 	<script src='${path}/res/js/my-js/util.js'></script> 
	<script>
		var now_date = getFormatDate(0);
		var now_date_year=parseInt(now_date.substring(0,4));
		var now_date_month=parseInt(now_date.substring(5,7)); 
	</script>
   	<meta charset="utf-8"> 
    <title>定额设置</title> 
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<script type="text/javascript">
		var path = "${path}";
	</script>
	<script src="${path}/res/js/my-js/util.js"></script>
	<script src="${path}/res/js/my-js/jquery-3.2.1.min.js"></script>
    <link rel="stylesheet" type="text/css" href="${path}/res/css/my-css/dx.common.css" />
	<link rel="dx-theme" data-theme="generic.light" href="${path}/res/css/my-css/dx.light.css" />
	<script src="${path}/res/js/my-js/data.js"></script>
	<script src="${path}/res/js/my-js/quota_summary.js"></script>
	<script src="${path}/res/js/my-js/jszip.min.js"></script>
	<script src="${path}/res/js/my-js/dx.all.js"></script>
	<script src="${path}/res/js/echarts/echarts.min.js"></script>
	<link rel='stylesheet' type='text/css' href='${path}/res/css/my-css/app.min.css' /> 
	<script type="text/javascript">
		var factory ="${factory_name}";
		var quota_summary_ajax_url1;
		var quota_summary_ajax_url2;
		if(ajax_json){
			quota_summary_ajax_url1=path + '/res/json/quota_summary1.json';
			quota_summary_ajax_url2=path + '/res/json/quota_summary2.json';
		}
	</script>
    <style type="text/css">
    ::-webkit-scrollbar{
		display:none;
		}
		body::-webkit-scrollbar {
		width: 0px;
		}
    	body{
    	background: #f3f3f4;
    	}
		.button {
		    height: 33px;
		    width:80px;
		    float:left;
		    margin-bottom: 10px;
            margin-top: 10px;
		}
		.button1 {
		        height: 33px;
			    width: 80px;
			    float: right;
			    margin: 10px 0px 0px 20px;
		}
		.dx-row{
			font-size:18px;
		    font-weight: bold;
		}
		.dx-datagrid-content .dx-datagrid-table .dx-row > td, .dx-datagrid-content .dx-datagrid-table .dx-row > tr > td {
		    vertical-align: top;
		    background-color: #fff;
		  
		}
		.imgClass{
		    height: 26px;
	    float: left;
	    margin-top: 14px;
	    margin-left: 5px;
		}
		#power_grid{
		}
    </style>
  </head>
  
  <body class="dx-viewport" style="background: #f3f3f4;">
   <div class="demo-container" >
   		<div style="background: #c2d8c49e;position: absolute; z-index: 100;width: 100%;margin-top: 5px;height: 50px;">
   		<div><img id="preYear" style="height: 22px;
    float: left;
    margin-left: 12px;
    /* z-index: 999; */
    /* display: block; */
    position: relative;
    top: 16px;"
    alt="" src="${path}/res/images/left.png"></div>
	   		<div style=" float:left;text-align: center;font-weight: bold;width: 80px;height: 33px;line-height: 33px; margin-left: 0px;;margin-top: 10px;">
	   			<a id="year"></a>
	   		</div>
	   		<div><img id="nextYear" style="height: 23px;
    float: left;
    /* z-index: 999; */
    /* display: block; */
    position: relative;
    top: 16px;" alt="" src="${path}/res/images/right.png"></div>
	<img class="imgClass" src="${path}/res/images/l0.png" id="power_button">
    <img class="imgClass" src="${path}/res/images/l2.png" id="cda_button">
    <img class="imgClass" src="${path}/res/images/l1.png" id="upw_button">
    <img class="imgClass" src="${path}/res/images/l3.png" id="pn2_button">
		 	
	    </div>
        <div style="margin-left:0px;margin-top:100px;position: absolute; z-index: 90;background: #f3f3f4;margin:0px;width:100%">
		    <div id="power_div" style="    position: absolute;z-index: 50;
left: 0px;
    /* top: 5px; */
    width: 98%;
    height: 792px;
    background:#f3f3f4;
    display: inline;
    margin: 0;
    padding: 0;
    margin-left: 15px;top: 60px;">
		    	<div id="power_quota_chart" style="float:left; margin-left: 0px; margin-top:6px;width: 50%;height:500px;background-color:#fFF;box-shadow: 2px 2px 2px 2px #ddd;"></div>
		    	<div id="power_sum_quota_chart" style="float:left; margin-left: 20px; margin-top:6px;width:720px;height:500px;background-color:#fff;box-shadow: 2px 2px 2px 2px #ddd;"></div>
		    	<div style="clear:left;height: 20px;
   "></div>
		    	<div id="power_grid" style="clear:left ;box-shadow: 2px 2px 2px 2px #ddd;"></div>
		    </div>
		    
		    <div id="upw_div" style="position: absolute; z-index: 12;left:0px;top:5px; width: 98%;height:100px;background: rgba(245, 247, 250);display:none;top: 60px;">
		    	<div id="upw_quota_chart" style="float:left; margin-left: 0px; margin-top:6px; width: 50%;height:500px;background-color:#fff;box-shadow: 2px 2px 2px 2px #ddd;"></div>
		    	<div id="upw_sum_quota_chart" style="float:left; margin-left: 20px; margin-top:6px;width: 50%;height:500px;background-color:#fff;box-shadow: 2px 2px 2px 2px #ddd;"></div>
		    	<div style="clear:left;height: 20px;
"></div>
		    	<div id="upw_grid" style="clear:left;box-shadow: 2px 2px 2px 2px #ddd;"></div>
		    </div>
		    <div id="cda_div" style="position: absolute; z-index: 12;left:0px;top:5px; width: 98%;height:100px;background: rgba(245, 247, 250);display:none;top: 60px;">
		    	<div id="cda_quota_chart" style="float:left; margin-left: 0px; margin-top:6px; width: 50%;height:500px;background-color:#fff;box-shadow: 2px 2px 2px 2px #ddd;"></div>
		    	<div id="cda_sum_quota_chart" style="float:left; margin-left: 20px; margin-top:6px;width: 50%;height:500px;background-color:#fff;box-shadow: 2px 2px 2px 2px #ddd;"></div>
		    	<div style="clear:left;height: 20px;
    "></div>
		    	<div id="cda_grid" style="clear:left;box-shadow: 2px 2px 2px 2px #ddd;"></div>
		    </div>
		        <div id="pn2_div" style="position: absolute; z-index: 12;left:0px;top:5px; width: 98%;height:100px;background: rgba(245, 247, 250);display:none;top: 60px;">
		    	<div id="pn2_quota_chart" style="float:left; margin-left: 0px; margin-top:6px; width: 50%;height:500px;background-color:#fff;box-shadow: 2px 2px 2px 2px #ddd;"></div>
		    	<div id="pn2_sum_quota_chart" style="float:left; margin-left: 20px; margin-top:6px;width: 50%;height:500px;background-color:#fff;box-shadow: 2px 2px 2px 2px #ddd;"></div>
		    	<div style="clear:left;height: 20px;
   "></div>
		    	<div id="pn2_grid" style="clear:left;box-shadow: 2px 2px 2px 2px #ddd;m"></div>
		    </div>
		</div>  
    </div>
    <script type="text/javascript">
	 	var power_quota_chart = echarts.init(document.getElementById('power_quota_chart'));
	 	var power_sum_quota_chart = echarts.init(document.getElementById('power_sum_quota_chart'));
	 	
	 	var upw_quota_chart = echarts.init(document.getElementById('upw_quota_chart'));
	 	var upw_sum_quota_chart = echarts.init(document.getElementById('upw_sum_quota_chart'));
	 	
	 	var cda_quota_chart = echarts.init(document.getElementById('cda_quota_chart'));
	 	var cda_sum_quota_chart = echarts.init(document.getElementById('cda_sum_quota_chart'));
	 	
	 	var pn2_quota_chart = echarts.init(document.getElementById('pn2_quota_chart'));
	 	var pn2_sum_quota_chart = echarts.init(document.getElementById('pn2_sum_quota_chart'));

	 	var power_quota_option = {
	 			title:{
					text:'定额与实际',
				    left:'left',
				    top:'6px'
				},
	 		    tooltip: {
	 		        trigger: 'axis',
	 		        axisPointer: {
	 		            type: 'shadow'
	 		        }                                                                                           
	 		    },
	 		    legend: {
	 		        data: ['定额', '实际'],
	 		        align: 'right',
	 		        right: 10,
	 		        textStyle: {
	 		            color: "#003377"
	 		        },
	 		        itemWidth: 10,
	 		        itemHeight: 10,
	 		        itemGap: 35
	 		    },
	 		    grid: {
	 		        left: '3%',
	 		        right: '4%',
	 		        bottom: '3%',
	 		        containLabel: true
	 		    },
	 		    xAxis: [{
	 		        type: 'category',
	 		        data: ['1月','2月','3月','4月','5月','6月', '7月', '8月', '9月','10月 ','11月 ','12月 ', ],
	 		        
	 		        axisLine: {
	 		            show: true,
	 		            lineStyle: {
	 		                color: "#063374",
	 		                width: 1,
	 		                type: "solid"
	 		            }
	 		        },
	 		        axisTick: {
	 		            show: false,
	 		        },
	 		        axisLabel: {
	 		            show: true,
	 		            textStyle: {
	 		                color: "#003377",
	 		            }
	 		        },
	 		    }],
	 		    yAxis: [{
	 		    	name:'Power(mWh)',
	 		        type: 'value',
	 		        axisLabel: {
	 		            formatter: '{value}'
	 		        },
	 		       
	 		        axisLine: {
	 		            show: true,
	 		            lineStyle: {
	 		                color: "#003377",
	 		                width: 1,
	 		                type: "solid"
	 		            },
	 		        },
	 		       splitLine: {
	 		            show: false
	 		       }
	 		    }],
	 		    series: [{
	 		        name: '定额',
	 		        type: 'bar',
	 		        data: [20, 50, 80, 58, 83, 68, 57, 80, 42, 66],
	 		        itemStyle: {
	 		            normal: {
	 		            	 color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
	 		            		 offset: 0,
	 		            		 color: '#00da9c'
	 		                	 }, {
	 		                     offset: 1,
	 		                     color: '#007a55'
	 		                 }]),
	 		                 opacity: 1,
	 		            }
	 		        }
	 		    }, {
	 		        name: '实际',
	 		        type: 'bar',
	 		        data: [50, 70, 60, 61, 75, 87, 60, 62, 86, 46],
	 		        itemStyle: {
	 		            normal: {
	 		            	color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
	 		                    offset: 0,
	 		                    color: '#008cff'
	 		               	}, {
	 		                    offset: 1,
	 		                    color: '#005193'
	 		                }]),
	 		               	opacity: 1,
	 		            }
	 		        }
	 		    },]
	 		};
	 	power_quota_chart.setOption(power_quota_option);
	 	
	 	var upw_quota_option = JSON.parse(JSON.stringify(power_quota_option));
	 	upw_quota_option.yAxis[0].name='UPW(k(m³))';
	 	var cda_quota_option = JSON.parse(JSON.stringify(power_quota_option));
	 	cda_quota_option.yAxis[0].name='CDA(k(m³))';
	 	var pn2_quota_option = JSON.parse(JSON.stringify(power_quota_option));
	 	pn2_quota_option.yAxis[0].name='PN2(k(m³))';
	 	
	 	upw_quota_chart.setOption(power_quota_option);
	 	cda_quota_chart.setOption(power_quota_option);
	 	pn2_quota_chart.setOption(power_quota_option);
	 	
	 	power_sum_quota_option=JSON.parse(JSON.stringify(power_quota_option));
	 	power_sum_quota_option.title.text='定额累计与定额实际'
	 	power_sum_quota_option.series[0].type="line";
	 	power_sum_quota_option.series[1].type="line";
	 	
	 	var upw_sum_quota_option = JSON.parse(JSON.stringify(power_sum_quota_option));
	 	upw_sum_quota_option.yAxis[0].name='UPW(k(m³))';
	 	var cda_sum_quota_option = JSON.parse(JSON.stringify(power_sum_quota_option));
	 	cda_sum_quota_option.yAxis[0].name='CDA(k(m³))';
	 	var pn2_sum_quota_option = JSON.parse(JSON.stringify(power_sum_quota_option));
	 	pn2_sum_quota_option.yAxis[0].name='PN2(k(m³))';
	 	
	 	upw_sum_quota_chart.setOption(upw_sum_quota_option);
	 	cda_sum_quota_chart.setOption(cda_sum_quota_option);
	 	pn2_sum_quota_chart.setOption(pn2_sum_quota_option);
	 	
	 	power_sum_quota_chart.setOption(power_sum_quota_option);
	</script>
    
    
  </body>
</html>
