<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/inc/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
    <script src='${path}/res/js/my-js/default_values.js'></script>
    <%@ include file="./common/def.jsp" %>
    <script src="${path}/res/js/my-js/util.js"></script>
    <script src="${path}/res/js/my-js/jquery-3.2.1.min.js"></script>

	<script type="text/javascript">
    	var pageName = 'hw_flow_graph';
        var title = "";
        var path = "${path}";
    </script>
    
    <!-- 引入 G6 文件 -->
    <script src="${path}/res/g6/g6.js"></script>
    <script src="${path}/res/g6/plugins.js"></script>

	<style>
		.card-container {
			width: 220px;
			height: 23px;
			border: 2px solid #DBDBDB;
			background: #009688;
			border-radius: 6px 6px 6px 6px;
			//box-shadow: 1px 1px 1px #c3bbbb;
		}
		.ce-button:hover{
			cursor: pointer;
		}
		.card-container .main-text{
			color: white;
			text-align: left;
			padding: 0px 0px 0px 4px;
			margin-bottom: 0px;
			font-size: 12px;
			float: left;
			border-radius: 5px 0 0 5px;
			background: #009688;
			margin-top: 0px;
			width: 130px;
			height: 22px;
			line-height:22px;
			margin: 0px;
		}
		.card-container p{
			font-size: 12px;
			margin: 3px;
			text-align: right;
			  
		}
		.card-container .value-text{
			//font-family: cursive;
			//text-shadow:0px 0px 8px #fff, 0px 0px 42px #f72, 0px 0px 72px #f84,0px 0px 150px #fa5;
			color: #fff;
			 
		}
		.card-container .value1-text{
			letter-spacing: 0;
			//text-shadow:0px 0px 8px #fff, 0px 0px 42px #f72, 0px 0px 72px #f84,0px 0px 150px #fa5;
			color: #fff;
		}
		.card-container .percent-text{
			color: transparent;
			-webkit-text-stroke: 1px  #001135;
			letter-spacing: 0.04em;
		}
		.rightMenu{
		  width: 150px;
		  height: 100px;
		  border: 1px solid black;
		  position: absolute;
		  display: none;
		  z-index:9999;
		  background-color: white;
		  }
		.rightMenu li:hover{
		  cursor: pointer;
		  background-color: aquamarine;
		}
		
	</style>

    <script src='${path}/res/js/my-js/mchw_flow_graph.js'></script>
    
</head>
<body class='dx-viewport' style="">
	<div id="mountNode">
	  <div id="rightMenu" class="rightMenu">
        <ul>
          <li id="openOrClose" class="openOrClose">展开/收起</li>
          <li id="openAll" class="openAll">展开全部</li>
          <li id="closeAll" class="closeAll">收起全部</li>
        </ul>
      </div>
	</div>
	<script>
		var seconds = 15*60;
		var end_time   = get_offset_seconds_time(-8*3600); //- 8 hours
		var begin_time = get_offset_seconds_time(-8*3600-seconds-15*60); //多减15分钟
		$.ajax({
	        url: ajax_json 
	        	? (path + '/res/json/hw_flow_graph.json')
	        	: (path + "/gynh/mchw_flow_graph/get_data.do"),
	        type: 'post',
	        async: false,
	        data: {
				begin_time: begin_time,
				end_time: end_time, 
				seconds: seconds,
				error_interval: 10, //采样误差n秒内
			    system: 'HW'
	        },
	        success: function(data) {
	        	var jsonArray = data.data;
	        	const collapseButtonUrl = '${path}/res/g6/collapse_button.svg';
	    		const expandButtonUrl = '${path}/res/g6/expand_button.svg';      
				let rightMenu = document.querySelector('.rightMenu');
				  document.addEventListener('contextmenu', ( event ) => {
					event.preventDefault();
					rightMenu.style.left = event.layerX + 'px';
					rightMenu.style.top = event.layerY + 'px';
					rightMenu.style.display = 'block';
					console.log('右键点击');
					console.log(event + 'px');
					console.log(event.clientY + 'px');
					console.log(document.scrollTop);
				  }
				  );
				  document.addEventListener('click', (event) => {
					rightMenu.style.display = 'none';
				  });

				  let rightMenuLi = document.querySelector('.rightMenu ul');
				  rightMenuLi.addEventListener('click', function (event) {
					console.log(event.target.textContent);
				  });
					
				let targetItem = null;
				let openOrClose = $('#openOrClose');
				let closeAll = $('#closeAll');
				let openAll = $('#openAll');

				 
	    		openOrClose.on('click',function () {
						  if (targetItem.model.collapsed) {
							tree.update(targetItem, {
							  collapsed: false,
							});
						  } else {
							tree.update(targetItem, {
							  collapsed: true,
						});
						  }
	    		});
	    		
	    		closeAll.on('click',function(){
					// 获取root节点
					let d = tree._cfg._itemMap[1];
					
					  tree.update(tree.find(d.id), {
							collapsed: true //close
						  });

	    		});
	    		
	    		
	    		openAll.on('click',function(){
					// 获取root节点
					let d = tree._cfg._itemMap[1];
					
					  tree.update(tree.find(d.id), {
							collapsed: false //open
						  });

	    		});

				  
	    		$('#collapseButton').append('<div style="width: 18%;float: left"><img id="fsctrl" class="ce-button" src="'+collapseButtonUrl+'"></div>');
	    		$('#collapseButton').append('<div style="width: 18%;float: left"><img id="ssctrl" class="ce-button" src="'+collapseButtonUrl+'"></div>');
	    		$('#collapseButton').append('<div style="width: 18%;float: left"><img id="tsctrl" class="ce-button" src="'+collapseButtonUrl+'"></div>');
	    		$('#collapseButton').append('<div style="width: auto;"><img id="foursctrl" class="ce-button" src="'+collapseButtonUrl+'"></div>');
	    		let fsctrl = $('#fsctrl');
	    		let ssctrl = $('#ssctrl');
	    		let tsctrl = $('#tsctrl');
	    		let foursctrl = $('#foursctrl');
	    		// 第一级控制
	    		fsctrl.on('click',function () {
					closeAll();
	    		});

	    		// 第二级控制
	    		ssctrl.on('click',function () {
	    			// 获取root节点
	    			let d = tree._cfg._itemMap[1].model;
	    			// 获得二级节点
	    			if(d.children != null){
	    				if(ssctrl[0].className=='ce-button'){
	    					G6.Util.each(d.children,function (item) {
	    						tree.update(tree.find(item.id), {
	    							collapsed: true //close
	    						});

	    					});

	    					ssctrl[0].classList.remove('ce-button');
	    					$('#ssctrl').attr('src',expandButtonUrl);
	    				}else {
	    					G6.Util.each(d.children,function (item) {
	    						tree.update(tree.find(item.id), {
	    							collapsed: false
	    						});

	    					});
	    					ssctrl[0].classList.add('ce-button');
	    					$('#ssctrl').attr('src',collapseButtonUrl);
	    				}
	    			};
	    		});

	    		function getThirdItems() {
	    			let thirdItems = new Array();
	    			// 获取root节点
	    			let d = tree._cfg._itemMap[1].model;



	    			// 获取二级节点
	    			const sendItems = d.children;
	    			// 获取三级节点
	    			for(let item of sendItems){
	    				const tItems = item.children;
	    				if(tItems !=null){

	    					for(let thirdItem of tItems){
	    						thirdItems.push(thirdItem)
	    					}
	    				}
	    			}
	    			return thirdItems;
	    		}

	    		// 第三级节点控制
	    		tsctrl.on('click',function () {

	    			let thirdItems = getThirdItems();

	    			// 遍历三级节点
	    			if(thirdItems != null){
	    				if(tsctrl[0].className=='ce-button'){
	    					G6.Util.each(thirdItems,function (item) {
	    						tree.update(tree.find(item.id), {
	    							collapsed: true //close
	    						});

	    					});

	    					tsctrl[0].classList.remove('ce-button');
	    					$('#tsctrl').attr('src',expandButtonUrl);
	    				}else {
	    					G6.Util.each(thirdItems,function (item) {
	    						tree.update(tree.find(item.id), {
	    							collapsed: false //open
	    						});

	    					});

	    					tsctrl[0].classList.add('ce-button');
	    					$('#tsctrl').attr('src',collapseButtonUrl);

	    				}
	    			}
	    		});

	    		// 第四级控制
	    		foursctrl.on('click',function () {
	    			// 获取第三级节点
	    			let thirdItems = getThirdItems();
	    			// 获取第四级节点
	    			let fItems = new Array();
	    			for(let item of thirdItems){
	    				const tItems = item.children;
	    				if(tItems !=null){

	    					for(let fItem of tItems){
	    						fItems.push(fItem)
	    					}
	    				}
	    			};

	    			if(fItems != null){
	    				if(foursctrl[0].className=='ce-button'){
	    					G6.Util.each(fItems,function (item) {
	    						tree.update(tree.find(item.id), {
	    							collapsed: true //close
	    						});

	    					});

	    					foursctrl[0].classList.remove('ce-button');
	    					$('#foursctrl').attr('src',expandButtonUrl);
	    				}else {
	    					G6.Util.each(fItems,function (item) {
	    						tree.update(tree.find(item.id), {
	    							collapsed: false //open
	    						});

	    					});

	    					foursctrl[0].classList.add('ce-button');
	    					$('#foursctrl').attr('src',collapseButtonUrl);

	    				}
	    			}


	    		});
	    		G6.registerEdge('horizontal-smooth', {
	    			getPath(item) {
	    				var points = item.getPoints();
	    				var start = points[0];
	    				e = item.getModel();
	    				e.size = 1;
	    				e.color = '#444444';
	    				var end = points[points.length - 1];
	    				var hgap = Math.abs(end.x - start.x);
	    				if(end.x > start.x) {
	    					return [
	    						['M', start.x, start.y],
	    						['C', start.x + hgap / 4, start.y, end.x - hgap / 2, end.y, end.x, end.y]
	    					];
	    				}
	    				return [
	    					['M', start.x, start.y],
	    					['C', start.x - hgap / 4, start.y, end.x + hgap / 2, end.y, end.x, end.y]
	    				];
	    			}
	    		});
	    	    
	    		G6.registerNode('card', {
	    			collapseButtonUrl: '${path}/res/g6/collapse_button.svg',
	    			expandButtonUrl: '${path}/res/g6/expand_button.svg',
	    			draw(item) {
	    				const group = item.getGraphicGroup();
	    				const model = item.getModel();
	    				model.size=9;
	    				const width = 227;
	    				const height = 30;
	    				const buttonWidth = 15;
	    				const buttonHeight = 17;
	    				let button = '';
	    				if(model.children && model.children.length > 0) {
	    					button = '<img class="ce-button" src=' + (model.collapsed ? this.expandButtonUrl : this.collapseButtonUrl) + '>';
	    				}
	    				var html1 = '<div id="card-container_div" title='+model.title+' class="card-container"><h1 class="main-text">'+model.main+'</h1><p>'+model.value+'</p></div>';
	    				const html = G6.Util.createDOM(html1);
	    				const keyShape = group.addShape('dom', {
	    					attrs: {
	    						x: 0,
	    						y: 0,
	    						width,
	    						height,
	    						html
	    					}
	    				});
	    				group.addShape('dom', {
	    					attrs: {
	    						x: width - buttonWidth / 2,
	    						y: height / 2 - buttonHeight / 2 - 3,
	    						width: buttonWidth,
	    						height: buttonHeight,
	    						html: button
	    					}
	    				});
	    				return keyShape;
	    			},
	    			anchor: [
	    				[1, 0.5],
	    				[0, 0.5]
	    			]
	    		});
	    		const data1 = {//node: #1-1  t:时间        m3h:瞬时流量  in0c:供水温度  out0c:回水温度   totalrc:冷量   totalrcm3h:累计流量    td0c:温差
	    				roots: [{
		    				id:'1',
		    				node:'#hw',
		    				main: '热水系统冷源',
		    				value: setValue('#hw'),
		    				title:setTitleValue('#hw'),
		    				percent: '40%',
		    				type: 'b',
		    				children: [{
		    					node:'#1+',
		    					main: '#1建筑及化学品库房#1、#4CCSS、#7SG、#8、#9、#10',
		    					value: setValue('#1+'),
		    					title:setTitleValue('#1+'),
		    					percent: '39%',
		    					type: 'b',
		    					children: [{
		    						node: '#1-e_s-mau_ahu-2',
		    						main: '2层东南侧MAU/AHU',
		    						value: setValue('#1-e_s-mau_ahu-2'),
		    						title:setTitleValue('#1-e_s-mau_ahu-2'),
		    						percent: '90%',
		    						type: 'a',
		    					}, 
		    					{
		    						node: '#1-e_n-mau_ahu-2',
		    						main: '2层东北侧MAU/AHU',
		    						value: setValue('#1-e_n-mau_ahu-2'),
		    						title:setTitleValue('#1-e_n-mau_ahu-2'),
		    						percent: '90%',
		    						type: 'a',
		    					},
		    					{
		    						node: '#1-w-mau_ahu-2',
		    						main: '2层西侧MAU/AHU',
		    						value: setValue('#1-w-mau_ahu-2'),
		    						title:setTitleValue('#1-w-mau_ahu-2'),
		    						percent: '90%',
		    						type: 'a',
		    					}, 
		    					{
		    						node: '#1-e-mau_ahu-4',
		    						main: '4层东侧MAU/AHU',
		    						value: setValue('#1-e-mau_ahu-4'),
		    						title:setTitleValue('#1-e-mau_ahu-4'),
		    						percent: '90%',
		    						type: 'a',
		    					}, 
		    					{
		    						node: '#1-w-mau_ahu-4',
		    						main: '4层西侧MAU/AHU',
		    						value: setValue('#1-w-mau_ahu-4'),
		    						title:setTitleValue('#1-w-mau_ahu-4'),
		    						percent: '90%',
		    						type: 'a',
		    					}, 
		    					{
		    						node: '',
		    						main: '其他库房区域',
		    						value: setValue(''),
		    						title:setTitleValue(''),
		    						percent: '90%',
		    						type: 'a',
		    					}]
		    				}, {
		    					node: '#2_3_12',
		    					main: '#2、#3建筑、#12楼',
		    					value: setValue('#2_3_12'),
		    					title:setTitleValue('#2_3_12'),
		    					percent: '79%',
		    					type: 'b',
		    					children: [{
		    						    node: '#2_3_12-2',
		    							main: '#2建筑',
		    							value: setValue('#2_3_12-2'),
		    							title:setTitleValue('#2_3_12-2'),
		    							percent: '90%',
		    							type: 'a',
		    							children: [{
	    								    node: '#2_3_12-n-mau_ahu-1',
	    									main: '1层北侧MAU/AHU',
	    									value: setValue('#2_3_12-n-mau_ahu-1'),
	    									title:setTitleValue('#2_3_12-n-mau_ahu-1'),
	    									percent: '90%',
	    									type: 'a',
	    								},
	    								{
	    								    node: '#2_3_12-s-mau_ahu-1',
	    									main: '1层南侧MAU/AHU',
	    									value: setValue('#2_3_12-s-mau_ahu-1'),
	    									title:setTitleValue('#2_3_12-s-mau_ahu-1'),
	    									percent: '90%',
	    									type: 'a',
	    								},
	    								{
	    								    node: '#2_3_12-n-mau_ahu-2',
	    									main: '2层北侧MAU/AHU',
	    									value: setValue('#2_3_12-n-mau_ahu-2'),
	    									title:setTitleValue('#2_3_12-n-mau_ahu-2'),
	    									percent: '90%',
	    									type: 'a',
	    								},
	    								{
	    								    node: '#2_3_12-s-mau_ahu-2',
	    									main: '2层南侧MAU/AHU',
	    									value: setValue('#2_3_12-s-mau_ahu-2'),
	    									title:setTitleValue('#2_3_12-s-mau_ahu-2'),
	    									percent: '90%',
	    									type: 'a',
	    								},
	    								{
	    								    node: '#2_3_12-n-mau_ahu-4',
	    									main: '4层北侧MAU/AHU',
	    									value: setValue('#2_3_12-n-mau_ahu-4'),
	    									title:setTitleValue('#2_3_12-n-mau_ahu-4'),
	    									percent: '90%',
	    									type: 'a',
	    								},
	    								{
	    								    node: '#2_3_12-s-mau_ahu-e',
	    									main: '4层南侧MAU/AHU',
	    									value: setValue('#2_3_12-s-mau_ahu-e'),
	    									title:setTitleValue('#2_3_12-s-mau_ahu-e'),
	    									percent: '90%',
	    									type: 'a',
	    								}
	    							]
		    						},
		    						{
		    							node: '',
										main: '#3建筑',
										value: setValue(''),
										title:setTitleValue(''),
										percent: '90%',
										type: 'a',
										children: [{
	    								    node: '',
	    									main: '#12楼',
	    									value: setValue(''),
	    									title:setTitleValue(''),
	    									percent: '90%',
	    									type: 'a',
	    								},
	    								{
	    								    node: '',
	    									main: '#3建筑1层MAU/AHU',
	    									value: setValue(''),
	    									title:setTitleValue(''),
	    									percent: '90%',
	    									type: 'a',
	    								},
	    								{
	    								    node: '',
	    									main: '#3建筑2层MAU/AHU',
	    									value: setValue(''),
	    									title:setTitleValue(''),
	    									percent: '90%',
	    									type: 'a',
	    								},
	    								{
	    								    node: '',
	    									main: '#3建筑3层MAU/AHU',
	    									value: setValue(''),
	    									title:setTitleValue(''),
	    									percent: '90%',
	    									type: 'a',
	    								}
	    							]
		    						}
		    					]
		    				}, {
		    					node: '#5',
		    					main: '#5CUB',
		    					value: setValue('#5'),
		    					title:setTitleValue('#5'),
		    					percent: '90%',
		    					type: 'a',
		    				}, {
		    					node: 'tm',
		    					main: 'TM #20、#21',
		    					value: setValue('tm'),
		    					title:setTitleValue('tm'),
		    					percent: '90%',
		    					type: 'a',
		    				}]
		    			}]
		    		};
	    		const tree = new G6.Tree({
	    			container: 'mountNode',
	    			height: 875,
	    			renderer: 'svg',
	    			layout: new G6.Layouts.CompactBoxTree({
	    				direction: 'LR', // 方向（LR/RL/H/TB/BT/V）
	    				getHGap: function getHGap() /* d */ {
	    					// 横向间距
	    					return 60;
	    				},
	    				getVGap: function getVGap() /* d */ {
	    					// 竖向间距
	    					return 18.5;
	    				},
	    				direction: 'LR'
	    			}),
	    			fitView: 'tc'
	    		});
	    		tree.node({
	    			shape: 'card'
	    		});
	    		tree.edge({
	    			shape: 'horizontal-smooth'
	    		});
	    		tree.on('node:click', ev => {
	    			const {
	    				domEvent,
	    				item
	    			} = ev;
	    			const {
	    				target
	    			} = domEvent;
	    			const {
	    				collapsed
	    			} = item.getModel();
					targetItem = item;
	    			if(target.className === 'ce-button') {
	    				if(collapsed) {
	    					tree.update(item, {
	    						collapsed: false,
	    					});
	    				} else {
	    					tree.update(item, {
	    						collapsed: true,
	    					});
	    				}
	    			}
	    		});
	    		tree.read(data1);
	    		function setValue(id) {
	    			for(var i=0;i<jsonArray.length;i++) {
	    				if(jsonArray[i].node == id) {
	    					return '<span class="value-text">'+jsonArray[i].totalrc+'&nbsp;KW</span>';
	    				}
	    			}
	    	    	return '<span class="value-text">&nbsp;&nbsp;KW</span>';
	    	    }
	    		function setTitleValue(id) {
	    			var str = '""';
	    			for(var i=0;i<jsonArray.length;i++) {
	    				if(jsonArray[i].node == id) {
	    					if(jsonArray[i].t != null){
	    						str = '"'+jsonArray[i].shortname + '\n'
	    						+ '冷量:'+jsonArray[i].totalrc + 'kw\n' 
	    						+ '累计流量:'+jsonArray[i].totalrcm3h + 'm³\n' 
		    					+ '瞬时流量:'+jsonArray[i].m3h + 'm³/h \n' 
		    					+ '温差:'+jsonArray[i].td0c + '℃  \n' 
		    					+ '供水温度:'+jsonArray[i].in0c + '℃ \n' 
		    					+ '回水温度:'+jsonArray[i].out0c+'℃ "'
		    				;
	    					}else{
	    						str = '"'+jsonArray[i].shortname + '\n'
	    						+ '冷量:'+jsonArray[i].totalrc + 'kw\n' 
	    						+ '累计流量:'+jsonArray[i].totalrcm3h + 'm³"'
		    				;
	    					}
	    					
		    				break;
	    				}
	    			}
	    	    	return str;
	    	    }
	    		
	        },
	        error: function() {
	        	alert('error');
	        }
	    });
	</script>
</body>
</html>