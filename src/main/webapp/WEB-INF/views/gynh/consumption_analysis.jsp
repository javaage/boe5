<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/inc/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
    <%@ include file="./common/def.jsp" %>
	<meta charset="utf-8">
	<title>冷机系统页面布局</title>
	<script src="${path}/res/js/echarts/echarts.min.js"></script>
	<script src="${path}/res/js/my-js/swiper.min.js"></script>
	<script src='${path}/res/js/my-js/default_values.js'></script> 
    <script src="${path}/res/js/my-js/jquery.min.js"></script>
    <link rel='stylesheet' type='text/css' href='${path}/res/css/my-css/dx.spa.css' /> 
    <link rel='stylesheet' type='text/css' href='${path}/res/css/my-css/dx.common.css' /> 
    <link rel='stylesheet' type='text/css' href='${path}/res/css/my-css/swiper.min.css' /> 
    <link rel='dx-theme' data-theme='generic.<%=theme%>' href='${path}/res/css/my-css/dx.<%=theme%>.css' />
    <script src='${path}/res/js/my-js/dx.all.js'></script> 
    <script src="${path}/res/layui/layui.js"></script>
    <link rel="stylesheet" href="${path}/res/layui/css/layui.css"
	media="all" />
	<link rel="stylesheet" type="text/css" href="http://cdn.amazeui.org/amazeui/2.4.2/css/amazeui.min.css"/>
    <script type="text/javascript" src="http://libs.baidu.com/jquery/1.11.3/jquery.min.js"></script>
    <script type="text/javascript" src="http://cdn.amazeui.org/amazeui/2.4.2/js/amazeui.min.js"></script>
    <script src='${path}/res/js/my-js/util.js'></script>
    
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport"
		content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<style>
        html, body {
            position: relative;
            height: 100%;
        }
        body {
            background: #eee;
            font-family: Helvetica Neue, Helvetica, Arial, sans-serif;
            font-size: 14px;
            color:#000;
            margin: 0;
            padding: 0;
        }
        .swiper-container {
            width: 100%;
            height: 100%;
        }
        .swiper-slide {
            text-align: center;
            font-size: 18px;
            background: #fff;
            /* Center slide text vertically */
        }
        .swiper-button-prev{
            position: absolute;
            left: 49%;
        }
        .header{
            display: flex;
            padding: 10px;
            align-items: center;
            height: 10%;
        }
        .header>span{
            display: inline-block;
            margin-right: 4px;
        }
        .header>div:nth-of-type(1){
            flex: 3;
            text-align: left;
        }
        .header>div:nth-of-type(2){
            flex: 1;
            text-align: center;
        }
        .header>div:nth-of-type(2)>span{
            display: inline-block;
            width: 60px;
            height: 30px;
            line-height: 30px;
            border: 1px solid #007DDB;
            border-radius: 6px;
            font-weight: normal;
            font-size: 16px;
            color: #007DDB;
            text-align: center;
        }
        .text>span{
            display: block;
         } 
        .time{
           width:400px;
        }
       .footer{
            height: 90%;
            background-size: 100% 100%;
            /* display: none; */
            overflow:auto;
        }
        /* .footer>img{
            width: 100%;
        } */
        #footer{
            display: block;
            box-sizing: border-box;
            border: 1px solid red;
            /* background: url("${path}/res/images/bg_1.png") no-repeat;
            background-size: 100% 100%; */
        }
        #footer2{
            display: none;
        }
        .header2{
            display: flex;
            padding: 10px;
            height: 10%;
            align-items: center;
        }
        .header2>span{
            display: inline-block;
            margin-right: 4px;
        }
       .header2>div:nth-of-type(1){
            margin-right: 10px;
        } 
        #footer3{
            width: 100%;
            height: 90%;
            box-sizing: border-box;
            /* overflow: hidden; */
        }
        .pic{
            width: 103%;
            height: 100%;
            overflow-y: scroll;
            overflow-x: hidden;
        }
        .pic>div{
            height: 25%;
            box-sizing: border-box;
            border: 1px solid red;
        }
        .divbackground{
	        
	        width: 130px;
		    height: 113px;
		    background: #0e90d2;
		    margin-left: 380px;
		    margin-top: 10px;
        }
        .top21{
        	margin-top: 21px;
        }
        .title{
        width: 76px;height: 30px;margin-top:6px
        }
    </style>	
</head>
<body>
   <div class="swiper-container">
    <div class="swiper-wrapper">
        <div class="swiper-slide" id="swiper-slide">
            <div class="header">
                <span>设备:</span>
                <div>
                    <select  id="js-selected" data-am-selected="{maxHeight: 220}">
                        <!--<option value=""></option>-->
                        <option value="1" selected>中温冷冻机组</option>
                        <option value="2">中温热回收冷冻机组</option>
                        <option value="3">低温冷冻机组</option>
                    </select>                                   
                </div>
                <div><span onclick="details()" id="details">详情</span></div>
            </div>
            <div class="footer" id="footer">
                <div  class="text" id="text">
                </div>
            </div>
            <div class="footer" id="footer2">
                <div  class="text top21" id="text2">
                	
                </div>
                
                	
            </div>
        </div>
        <div class="swiper-slide" id="swiper-slide2">
            <div class="header2">
                <span>设备:</span>
                <div>
                    <select  id="js-selected2" multiple data-am-selected="{maxHeight: 220}">
                    </select>
                </div> 
                <span>时间:</span>
                <div class="time">
                   <input id="beginTimeBox" type="text" placeholder="-" class="layui-input">
                </div>
            </div>
            <div  id="footer3">
               <div class="pic">
                   <div class="view">1</div>
                   <div class="view">2</div>
                   <div class="view">3</div>
                   <div class="view">4</div>
                   <div class="view">5</div>
                   <div class="view">6</div>
                   <div class="view">7</div>
                   <div class="view">8</div>
                   <div class="view">9</div>
                   <div class="view">10</div>
                   <div class="view">11</div>
                   <div class="view">12</div>
                </div>
            </div>
        </div>
    </div>
    <!--Add Arrows -->
    <!--<div class="swiper-button-next"></div>-->
    <div class="swiper-button-prev" id="swiper-button-prev" onclick="scaling()"></div>
</div>
<script>
    window.onload = function () {
    	//getNowFormatDate();//获取结束时间
    	//getNowFormatDate2();//获取开始时间
    	loadBackgroundImage();//加载背景图片
    	loadData()//加载下拉框2的数据
    	getData();//向后台请求数据
    	getData2();//向后台请求数据2
        iconDisplay();//展示没数据折线图
    };
    var swiper = new Swiper('.swiper-container', {
        slidesPerView: 2,
        spaceBetween: 0,
    });
    var flag = true;
    
    var flag2 = true;
    var sel = document.getElementById("js-selected");
    var sel_2 = document.getElementById("js-selected2");
    //var text = document.getElementById('text');
    //var text2 = document.getElementById('text2');
    var index = 0;
    var path = "${path}";
    var system = "MCHW-CH";
    var beginDateTime; //开始时间
    var endDateTime;  //结束时间
    var keyword = "中温冷冻机组"; //下拉框刚开始选择中温冷冻机组
    var getHislistbay = [];//存放data.getHislistbay的数据
    //var temp = [];//存放getHislistbay去重后的数据
    var views = document.getElementsByClassName('view');  
     option = {
            	    title: {
            	        text: ''
            	    },
            	    tooltip: {
            	        trigger: 'axis'
            	    },
            	    legend: {
            	        
            	    },
            	    grid: {
            	        left: '3%',
            	        right: '4%',
            	        bottom: '3%',
            	        containLabel: true
            	    },
            	    toolbox: {
            	        feature: {
            	            //saveAsImage: {}
            	        }
            	    },
            	    xAxis: {
            	        type: 'category',
            	        boundaryGap: false,
            	        data: []
            	    },
            	    yAxis: {
            	        type: 'value'
            	    },
            	    series: [{
            	    	type:'line',
            	    	data:[],
            	    }
            	        
            	    ]
            	};

    //当选框1发生改变        
    sel.onchange = function () {
    	index = this.selectedIndex;
    	keyword = this.options[index].text;
    	if(keyword=="中温冷冻机组"){
    		 system ="MCHW-CH";
        }else if(keyword=="中温热回收冷冻机组"){
    		 system ="MCHW-CHR";
        }else{
    		 system ="LCHW-CH";
        }
    	loadData();
        getData();//向后台请求数据
        /* document.getElementById('footer').style.display='none';
        document.getElementById('footer2').style.display='block'; */
        loadBackgroundImage();//更换背景图片
     };
   //当下选框2发生改变
    sel_2.onchange = function () {
	    getData2();
	    var str = [];
    	for(i=0;i<this.options.length;i++){
            if(this.options[i].selected){
                str.push(this.options[i].text);
            }
        }
    	if(!str.length){
    		option.title.text="";
    		option.xAxis.data.splice(0, option.xAxis.data.length);
    	}
    	for(var i = 0;i<views.length;i++ ){
    		option.series.splice(0, option.series.length);
    		var myChart = echarts.init(views[i]);
			myChart.setOption(option,true);
		}
        set_chart(str);
    	//screenData(str);
    };
    //日期组件
    layui.use('laydate', function(){
   	  var laydate = layui.laydate;
   	  //日期范围
   	  laydate.render({
   	    elem: '#beginTimeBox',
   	    range: true,
   		type: 'datetime',
   		done: function(value) {
   			var time = value.split(" ");
   			var begintime = time[0]+" "+time[1];
   			var endtime = time[3]+" "+time[4];
   			console.log(value);
   			beginDateTime = getTimeDate2(begintime,-8);
   			endDateTime = getTimeDate2(endtime,-8);
   			console.log(beginDateTime);
   		    console.log(endDateTime);
   		    getData2();//选择开始-结束时间后向后台请求数据
   		  }
   	  });
    });
    //对右侧折线图数据进行处理
    function set_chart(str) {
       console.log(str);
    	var ss= new Array();
    	
    	var set= new Set();
    	for (var h = 0; h < str.length; h++) {
    		for(var j=0;j<getHislistbay.length;j++){
	             if(str[h]==getHislistbay[j].node){
	            	 set.add(getHislistbay[j].type);
	             }
	          }
	   	 }
   	 	
    	for (var h = 0; h < str.length; h++) {
			 var arr=new Array();
	    	 for(var j=0;j<getHislistbay.length;j++){
	             if(str[h]==getHislistbay[j].node){
	            	 arr.push(getHislistbay[j]);
	             }
	          }
	    	 var str01=new Array();
	    	 for (let iterable_element of set) {
	    		 var data_array= new Array();
	        	 var time_array=new Array();
	    		 for (var i = 0; i < arr.length; i++) {
	    			 if(iterable_element==arr[i].type){
	    				 data_array.push(arr[i].gvalues);
	    				 time_array.push(arr[i].times);
	                 }
	    		}
	    		 str01.push({
	    			 data:data_array,
	    			 time:time_array,
	    			 type:iterable_element,
	    			 name:str[h],
	    		 })
				
			}
	    	 ss.push(str01);
    	}
    	 
    	console.log(ss);
        for(var i = 0;i<views.length;i++ ){
			var myChart = echarts.init(views[i]);
			option.series.splice(0, option.series.length);
			option.title.text="";
    		option.xAxis.data.splice(0, option.xAxis.data.length);
			//option.xAxis.data.splice(0, option.xAxis.data.length);
			option.title.text=ss[0][i].type;
			option.xAxis.data=ss[0][i].time;
			var series = new Array();
			for (var j = 0;j < ss.length; j++) {
				if(ss[j][i].data.length==0){
					continue;
				}
				series.push({
					name:ss[j][i].name,
					type:'line',
					data:ss[j][i].data
					
				});
			}
			
			option.series=series;
			 // 使用刚指定的配置项和数据显示图表。
			myChart.setOption(option,true);
		}
    }
    //加载背景图片
    function loadBackgroundImage(){
    	//alert(index);
    	$("#footer").css("background-image","url(${path}/res/images/bg_"+(index+1)+".png)");
        $("#footer2").css("background-image","url(${path}/res/images/flow_"+(index+1)+".png)");
        if(index+1==2){
        	$("#text2").html(
        	"<div style='float: left;margin-left: 139px;margin-top: 35px;'><div title='#01 CT' class='title'></div>"+
        	"<div title='#01 CT' class='title'></div>"+
        	"<div title='#02 CT' class='title'></div>"+
        	"<div title='#03 CT' class='title'></div>"+
        	"<div title='#04 CT' class='title'></div>"+
        	"<div title='#05 CT' class='title'></div>"+
        	"<div title='#06 CT' class='title'></div>"+
        	"<div title='#07 CT' class='title'></div>"+
        	"<div title='#08 CT' class='title'></div>"+
        	"<div title='#09 CT' class='title'></div>"+
        	"<div title='#10 CT' class='title'></div>"+
        	"<div title='#11 CT' class='title'></div>"+
        	"<div title='#12 CT' class='title'></div>"+
        	"<div title='#13 CT' class='title'></div>"+
        	"<div title='#14 CT' class='title'></div>"+
        	"<div title='#15 CT' class='title'></div>"+
        	"<div title='#16 CT' class='title'></div>"+
        	"<div title='#17 CT' class='title'></div>"+
        	"<div title='#18 CT' class='title'></div>"+
        	"</div>"+
        	"<div id='divbackground1' class ='divbackground' onclick='getGround(1)' >MCHW_CHR01</div>"+
        	"<div id='divbackground2' class ='divbackground' onclick='getGround(2)' >MCHW_CHR02</div>"+
        	"<div id='divbackground3' class ='divbackground' onclick='getGround(3)' >MCHW_CHR03</div>"+
        	"<div id='divbackground4' class ='divbackground' onclick='getGround(4)' >MCHW_CHR04</div>"+
        	"<div id='divbackground5' class ='divbackground' onclick='getGround(5)' >MCHW_CHR05</div>"+
        	"<div id='divbackground6' class ='divbackground' onclick='getGround(6)' >MCHW_CHR06</div>"
        	)
        }
    }
    function getGround(num){
    	$("#divbackground"+num).css("background","#FFEB3B");
    }
    //加载下拉框2的数据
    function loadData(){
    	//alert(keyword);
    	var s1 = [
            "MCHW-CH-1","MCHW-CH-2","MCHW-CH-3",
            "MCHW-CH-4","MCHW-CH-5","MCHW-CH-6",
            "MCHW-CH-7","MCHW-CH-8","MCHW-CH-9",
            "MCHW-CH-10","MCHW-CH-11","MCHW-CH12"
            ];
        var s2 = [
            "MCHW-CHR-1","MCHW-CHR-2","MCHW-CHR-3",
            "MCHW-CHR-4","MCHW-CHR-5","MCHW-CHR-6",
           ];
        var s3 = ["LCHW-CH-1","LCHW-CH-2","LCHW-CH-3","LCHW-CH-4"];
        var s = [];
         if(keyword=="中温冷冻机组"){
		     s=s1;
	     }else if(keyword=="中温热回收冷冻机组"){
		     s=s2;
	     }else{
		     s=s3;
	     }
         sel_2.innerHTML = "";
         /* var sel_2 = document.getElementById("js-selected2");
         while(sel_2.hasChildNodes()) //当div下还存在子节点时 循环继续
         {
        	 sel_2.removeChild(sel_2.firstChild);
         } */
         for(var i =0;i<s.length;i++){
        	 var option = document.createElement('option');
        	 option.innerHTML = s[i];
        	 option.value = i;
        	/*  if(i==0){
        		 var str = [];
        		 str.push(s[i]);
        		 option.selected="true";
        		 set_chart(str);
        	 } */
             sel_2.appendChild(option);
         }
         //console.log(s);
    } 
    //获取数据
    function getData() {
        //console.log(system);
        //console.log(beginDateTime);
        //console.log(endDateTime);
        removeAllChild();
        $.ajax({
            url:path+'/consumption/consumptionbak/get_analysis.do',
            type: 'get',
            async: false,
            data: {
            	system :system,
               },
            success: function (data) {
                console.log(data);
                for(var i = 0;i<data.data.length;i++){
            	   var span = document.createElement("span");
                	span.innerHTML = data.data[i].node+":"+data.data[i].gvalues;
                	text.appendChild(span);
                } 
               //iconDisplay();
               //getData2();
            }
        });
    }
    //获取数据2
    function getData2() {
    	console.log(system);
        console.log(beginDateTime);
        console.log(endDateTime);
        $.ajax({
            url:path+'/consumpump/consumpumpbak/get_pumpsis.do',
            type: 'get',
            async: false,
            data: {
            	system :system,
                beginDateTime:beginDateTime,
                endDateTime:endDateTime
            },
            success: function (data) {
            	console.log(data);
            	getHislistbay = data.getHislistbay;
            	//console.log(getHislistbay);
            }
        });
    }
    //筛选设备数据
   /*  function screenData(str) {
    	console.log(str);
    	for(var i=0;i<str.length;i++){
    		  var arr = []; 	
        	  for(var j=0;j<getHislistbay.length;j++){
                 if(str[i]==getHislistbay[j].node){
                	 arr.push(getHislistbay[j]);
                 }
              }	
        	  console.log(arr);
        }
    } */
   //获取当前日期时间“yyyy-MM-dd HH:MM:SS”
   //结束时间
    function getNowFormatDate() {
        var date = new Date();
        var seperator1 = "-";
        var seperator2 = ":";
        var month = date.getMonth() + 1;
        var strDate = date.getDate();
        if (month >= 1 && month <= 9) {
            month = "0" + month;
        }
        if (strDate >= 0 && strDate <= 9) {
            strDate = "0" + strDate;
        }
        var currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate
            + " " + date.getHours() + seperator2 + date.getMinutes()
            + seperator2 + date.getSeconds();
            endtime = currentdate;
            endDateTime = getTimeDate2(endtime,-8);
    }
   //获取当前时间，格式YYYY-MM-DD
   //开始时间
    function getNowFormatDate2() {
        var date = new Date();
        var seperator1 = "-";
        var year = date.getFullYear();
        var month = date.getMonth() + 1;
        var strDate = date.getDate();
        if (month >= 1 && month <= 9) {
            month = "0" + month;
        }
        if (strDate >= 0 && strDate <= 9) {
            strDate = "0" + strDate;
        }
        var currentdate = year + seperator1 + month + seperator1 + strDate;
            begintime = currentdate + " 00:00:00";
        	beginDateTime = getTimeDate2(begintime,-8);
		}
    //点击详情或概览
    function details() {
    	//alert(keyword);
        //alert(index);
        getData();
        if(flag){
            document.getElementById('details').innerText='概览';
            document.getElementById('footer').style.display='none';
            document.getElementById('footer2').style.display='block';
            flag=false;
        }else {
            document.getElementById('details').innerText='详情';
            document.getElementById('footer').style.display='block';
            document.getElementById('footer2').style.display='none';
            flag=true;
        }
    }
    //缩放
    function scaling() {
        if(flag2){
            document.getElementById('swiper-slide2').style.width='100%';
            document.getElementById('swiper-slide').style.width='0%';
            document.getElementById('swiper-button-prev').style.left='0%';
            document.getElementById('swiper-button-prev').style.transform='rotate(180deg)';
            flag2=false;
        }else {
            document.getElementById('swiper-slide2').style.width='50%';
            document.getElementById('swiper-slide').style.width='50%';
            document.getElementById('swiper-button-prev').style.left='49%';
            document.getElementById('swiper-button-prev').style.transform='rotate(360deg)';
            flag2=true;
        }
    }
    //数组去重
   /*  function uniq(array){
        for(var i = 0; i < array.length; i++) {
            //如果当前数组的第i项在当前数组中第一次出现的位置是i，才存入数组；否则代表是重复的
            if(array.indexOf(array[i]) == i){
                temp.push(array[i])
            }
        }
        console.log(temp);
    } */
   //删除所有子元素
   function removeAllChild()
    {
        var text = document.getElementById("text");
        while(text.hasChildNodes()) //当div下还存在子节点时 循环继续
        {
            text.removeChild(text.firstChild);
        }
    }
    //空数据折线图展示
    function iconDisplay() {
        var views = document.getElementsByClassName('view');
        for(var i = 0;i<views.length;i++ ){
            var myChart = echarts.init(views[i]);
            option = {
            	    title: {
            	        text: ''
            	    },
            	    tooltip: {
            	        trigger: 'axis'
            	    },
            	    legend: {
            	        
            	    },
            	    grid: {
            	        left: '3%',
            	        right: '4%',
            	        bottom: '3%',
            	        containLabel: true
            	    },
            	    toolbox: {
            	        feature: {
            	            //saveAsImage: {}
            	        }
            	    },
            	    xAxis: {
            	        type: 'category',
            	        boundaryGap: false,
            	        data: []
            	    },
            	    yAxis: {
            	        type: 'value'
            	    },
            	    series: [{
            	    	type:'line',
            	    	data:[],
            	    }
            	        
            	    ]
            	};
            myChart.setOption(option);
        }
    } 
</script>
</body>
</html>
   
    