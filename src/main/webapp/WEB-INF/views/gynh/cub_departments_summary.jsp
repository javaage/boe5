<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/inc/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
    <script src='${path}/res/js/my-js/default_values.js'></script> 
	<%@ include file="./common/head.jsp"%>
	<script src='${path}/res/js/my-js/factories_summary_unit.js'></script>
    
    <script type="text/javascript">
        var param1 = "";
        var title = "";
        var pageName = 'cub_departments_summary';
        var path = "${path}";
        var pivotGridId = "#pivot_grid";
        var requestpath = "/gynh/cub_departments_summary/get_data.do";
        var layoutStorageKey = "boe-gynh-pivotgrid-cub_departments_summary";
        var bLoadFromDb = true;
        var filterOutFileds = ["开始时间", "结束时间"];
        var is_zt = false;
        var need_worktime = true;
        var worktime_flag = true;
        var unit_show="power_rc_upw_cda";
        var stateTimeSum;
        var cda_dataField= null;
        var power_dataField= null;
        var pn2_dataField= null;
        var upw_dataField= null;
        var time_dataField=null;
        var hvac_sys_daily_chart_data=new Array();
		
        var init_begin_date =  getFormatDate(-7);;
        var init_end_date = getFormatDate(-1);
        
        
        // init date control
        layui.use('laydate', function(){
        	var laydate = layui.laydate;
        	  //日期范围.
        	laydate.render({
        	elem: '#beginTimeBox',
        	range: true,
        	theme: is_dark?'#444444':'',
        	type: 'date',
        	value: init_begin_date + ' - ' + init_end_date,
        	done: function(value) {
        		init_begin_date=value.substr(0,10);
        		init_end_date=value.substr(13,10);
        		load_data(init_begin_date, init_end_date);	
        		}
        	});
        });
        
        if(ajax_json){
        	cub_department_summary_ajax_url1=path + '/res/json/cub_departments_summary1.json';
        }
    </script>
  
      <style>
		html,body{
			height: 100%;
			width: 100%;
			margin: 0;
			padding: 0;
			background:#DDE1E4;
			
		}

		.l_border {
		     float: left;
    width: 94%;
    height: 44%;
    background: #fff;
    margin-top: 40px;
    margin-left: 2.9%;
		}
		.l_border1 {
		      float: left;
    width: 94%;
    height: 44%;
    background: #fff;
    margin-top: 10px;
    margin-left: 2.9%;
    
		}
		.r_border {
			border-right:10px solid #DDE1E4;
		}
		.t_border {
			border-top:10px solid #DDE1E4;
		}
		.b_border {
			border-bottom:10px solid #DDE1E4;
		}
		.td_bg_color {
			background:#DDE1E4;
		}
		.banner{
		
		}
		.selectAll{
			width: 60px;
		    display: inline;
		    /* background: #5fb878; */
		    height: 27px;
		    /* width: 180px; */
		    line-height: 28px;
		    border: 1px solid #9bc0dd;
		    -moz-border-radius: 2px;
		    -webkit-border-radius: 2px;
		    border-radius: 3px;
		}
	</style>  
    
</head>
<body class='dx-viewport' style="background: #f3f3f4;overflow:hidden;">

	<div style=" margin-left:15px;margin-top:0px;width:240px;height:37px;">
        <input type="text" class="<%=layui_bg_class%>" id="beginTimeBox" style="    float: left;
    margin-left: 0px;
    margin-right: 0px;
    width: 160px;
    height: 28px;
    text-align: center;
    line-height: 28px;
    font-size: 14px;
    margin-top: 8px;
    box-shadow: 2px 2px 2px 2px #ddd;">
        <div id = "table_toolbar_summary_div" style="height:37px;width:50px; float:left; margin-top: 10px; margin-top: 0px; margin-left:10px;margin-right:0px;"></div>
        <jsp:include flush="true" page="./common/factories_summary_grid_toolbar.jsp"/>  
  		</div>
  	<div id='cub_sum_charts_div' style="width: 100%;
    height: 100%;">
  	<div style="   height: 100%;
    width: 33%;
    float: left;
    margin-top: 10px;">
  	 <span class="banner " style="    float: left;
    margin-top: 6px;
    position: absolute;
    margin-left: 1%;
    width: 31%;
    height: 28px;
    line-height: 26px;
    box-shadow: 2px 2px 2px 2px #ddd;
    text-align: center;
    /* border-radius: 10px; */
    font-size: 28px;
    color: #fff;
    background-color: #009999;">HVAC</span>
     <div class = 'l_border '>
  	   	  	 	<div id='hvac_sys_daily_chart' style="margin:0;padding:0;color:white;height:40%;">
  	   	   </div>
  	   	   </div>
    
  	 <span class="banner " style="    float: left;
    margin-top: 461px;
    position: absolute;
    height: 26px;
    margin-left: -18%;
    width: 72px;
    line-height: 26px;
    text-align: center;
    border-radius: 10px;
    font-size: 20px;
    color: #666;">CHS</span>
  	
  	 <div id = 'hvac_unit_cons_chart_div' class = 'l_border1 '><div id='hvac_unit_cons_chart' style='margin:0;padding:0;color:white;height:40%;'></div></div>
  	</div>
  	
  	
  	<div style="    height: 100%;
    width: 33%;
    float: left;
    margin-top: 10px;">
  	     <span class="banner" style="    float: left;
    margin-top: 5px;
    position: absolute;
    margin-left: 0.8%;
    width: 31.1%;
    height: 28px;
    line-height: 26px;
    box-shadow: 2px 2px 2px 2px #ddd;
    text-align: center;
    /* border-radius: 10px; */
    font-size: 28px;
    color: #fff;
    background-color: #009999;">WATER</span>
  	
  	  <div id = 'water_sys_daily_chart_div' class = 'l_border '>
          
           <div id='water_sys_daily_chart' style="margin:0;padding:0;color:white;height:40%;">
           
           </div></div>
  	  <span class="banner" style="float: left;
    margin-top: 460px;
    position: absolute;
    height: 26px;
    margin-left: -18%;
    width: 72px;
    line-height: 26px;
    text-align: center;
    border-radius: 10px;
    font-size: 20px;
    color: #666;">UPW</span>
  	
  	 <div id = 'water_unit_cons_chart_div' class = 'l_border1 '> <div id='water_unit_cons_chart' style='margin:0;padding:0;color:white;height:40%;'></div></div>
  	</div>
  	<div style="    height: 100%;
    width: 33%;
    float: left;
    margin-top: 10px;">
  	
  	  <span class="banner" style="    float: left;
    margin-top: 5px;
    position: absolute;
    margin-left: 0.8%;
    width: 31.1%;
    height: 28px;
    line-height: 26px;
    box-shadow: 2px 2px 2px 2px #ddd;
    text-align: center;
    /* border-radius: 10px; */
    font-size: 28px;
    color: #fff;
    background-color: #009999;">G&C</span>
  	<div id = 'gc_sys_daily_chart_td' class = 'l_border '>
  	        
  	       <div id='gc_sys_daily_chart' style="margin:0;padding:0;color:white;height:40%;">
  	       
  	       </div></div>
  	
		  	         <span class="banner" style="float: left;
    margin-top: 460px;
    position: absolute;
    height: 26px;
    margin-left: -17%;
    width: 72px;
    line-height: 26px;
    text-align: center;
    border-radius: 10px;
    font-size: 20px;
    color: #666;">CDA</span>
  	      
          
  	       <div id = 'gc_unit_cons_chart_div' class = 'l_border1 ' ><div id='gc_unit_cons_chart' style='margin:0;padding:0;color:white;height:40%;'></div></div>
  	
  	</div>
  	    
	</div>

    <!-- for chart -->
	<!--script src="${path}/res/js/echarts/echarts-all.js"></script-->
	<script src="${path}/res/js/echarts/echarts.min.js"></script>
    <script src='${path}/res/js/my-js/table_toolbar_summary.js'></script>
    <script src='${path}/res/js/my-js/chart.js'></script>
    <script src='${path}/res/js/my-js/worktime_chart.js'></script>


	<script>
		var hvac_sys_array = ['CHS', 'FFU', 'PCW', 'SCR', 'VOC', 'FAN', 'RS', 'MAU', 'PGEX'];
		var water_sys_array = ['UPW', 'CCSS', 'WWT'];
		var gc_sys_array = ['CDA', 'PVHV', 'CCSS', 'GAS'];
		
		var hvac_unit_array = ['Power', 'RC', 'EER'];
		var water_unit_array = ['Power', 'UPW', 'EER'];
		var gc_unit_array = ['Power', 'CDA', 'EER'];

		var hvac_sys_daily_chart = echarts.init(document.getElementById('hvac_sys_daily_chart'), "<%=theme%>");
		var hvac_sys_daily_chart_option = {
		    tooltip: {
		        trigger: 'axis'
		    },
		    toolbox: {
		        feature: {
		            myTool1: {
		                show: true,
		                title: '恢复',
		                icon: 'image://'+path+'/res/images/echart_back.png',
		                onclick: function (){
		                	var selecte = {};
		                	hvac_sys_daily_chart_option.legend.data.splice(0, hvac_sys_daily_chart_option.legend.data.length);
	                		hvac_sys_daily_chart_option.series.splice(0, hvac_sys_daily_chart_option.series.length);
		                    for(i =0; i < hvac_sys_daily_chart_data.length; i++){
		                    	hvac_sys_daily_chart_option.series.push(hvac_sys_daily_chart_data[i]);
		                    }
		                    for(i =0; i < hvac_sys_daily_chart_data.length; i++){
		                    	hvac_sys_daily_chart_option.legend.data.push(hvac_sys_daily_chart_data[i].name);
		                    }
		                   
		                    
		                    var colors=['#FBBF27', '#FFE26D', '#EEF263', '#BDEB90', '#93E48B', '#4ECBCB', '#50CEEA', '#34A7D8', '#2A84BD'];

		                    hvac_sys_daily_chart_option.color=colors;
		                 	hvac_sys_daily_chart.setOption(hvac_sys_daily_chart_option,true);
		                }
		            },
		        }
		    },
		    legend: {
		    	type: 'scroll',
		    	itemGap: 6,
 				itemWidth: 12,
 				itemHeight: 12,
		        textStyle: {color:'<%=text_color%>'},
		        data:hvac_sys_array,
		        y:'bottom',
		        selected: {},
		    },
		    grid: {
		        //left: '5px',
		        //right: '5px',
		        bottom: '40px',
		        top:'9px',
		        //containLabel: false,
		    },
		    xAxis: {
				axisLabel: {interval: 0},
		        textStyle: {color:'white'},
		        type: 'category',
		        //boundaryGap: false,
		        data: []
		    },
		    yAxis: {
		        textStyle: {color:'white'},
		        type: 'value',
		        axisLabel:{
		            inside: false,
		        },
		        splitLine:{
		        	show:false,
		        },
		    },
		    color: ['#FBBF27', '#FFE26D', '#EEF263', '#BDEB90', '#93E48B', '#4ECBCB', '#50CEEA', '#34A7D8', '#2A84BD'],
		    series: [
				{
		            name: ' ',
		            type: 'bar',
		            stack: 'total',
		            label: {
		                normal: {
		                    show: false,
		                }
		            },
		            data: ["-"]
		        },
		    ]
		};
		hvac_sys_daily_chart.setOption(hvac_sys_daily_chart_option);

		var water_sys_daily_chart = echarts.init(document.getElementById('water_sys_daily_chart'), "<%=theme%>");
		var water_sys_daily_chart_option = {
		    tooltip: {
		        trigger: 'axis'
		    },
		    toolbox: {
		        show: true,
		        feature: {
		            magicType: {type: ['line', 'bar', 'stack', 'tiled']},
		            saveAsImage: {}
		        }
		    },
		    legend: {
		        textStyle: {color:'<%=text_color%>'},
		        data: water_sys_array,
		        y:'bottom',
		        itemGap: 6,
 				itemWidth: 12,
 				itemHeight: 12,
 				selected: {},
		    },
		    toolbox: {
		        feature: {
		            myTool1: {
		                show: true,
		                title: '恢复',
		                icon: 'image://'+path+'/res/images/echart_back.png',
		                onclick: function (){
		                	var selecte = {};
		                    for(i =0; i < water_sys_array.length; i++){
		                     	selecte[water_sys_array[i]] = true;
		                     	water_sys_daily_chart_option.legend.selected = selecte;
		                    }
		                 	water_sys_daily_chart.setOption(water_sys_daily_chart_option);
		                }
		            },
		        }
		    },
		    grid: {
		        //left: '5px',
		        //right: '5px',
		        bottom: '40px',
		        top:'9px',
		        //containLabel: false,
		    },
		    xAxis: {
				axisLabel: {interval: 0},
		        textStyle: {color:'white'},
		        type: 'category',
		        //boundaryGap: false,
		        data: []
		    },
		    yAxis: {
		        textStyle: {color:'white'},
		        type: 'value',
		        axisLabel:{
		            inside: false,
		        },
		        splitLine:{
		        	show:false,
		        },
		    },
		    color: ['#FFDF70', '#92E48D', '#3FC3CF'],
		    series: [
				{
		            name: 'RUN',
		            type: 'bar',
		            stack: 'total',
		            label: {
		                normal: {
		                    show: false,
		                }
		            },
		            data: ["-"]
		        },
		        {
		            name: 'PM',
		            type: 'bar',
		            stack: 'total',
		            label: {
		                normal: {
		                    show: false,
		                }
		            },
		            data: ["-"]
		        },
		    ]
		};
		water_sys_daily_chart.setOption(water_sys_daily_chart_option);

		var gc_sys_daily_chart = echarts.init(document.getElementById('gc_sys_daily_chart'), "<%=theme%>");
		var gc_sys_daily_chart_option = {
		    tooltip: {
		        trigger: 'axis'
		    },
		    legend: {
		        textStyle: {color:'<%=text_color%>'},
		        data: gc_sys_array,
		        y:'bottom',
		        itemGap: 6,
 				itemWidth: 12,
 				itemHeight: 12,
 				selected: {},
		    },
		    toolbox: {
		        feature: {
		            myTool1: {
		                show: true,
		                title: '恢复',
		                icon: 'image://'+path+'/res/images/echart_back.png',
		                onclick: function (){
		                	var selecte = {};
		                    for(i =0; i < gc_sys_array.length; i++){
		                     	selecte[gc_sys_array[i]] = true;
		                     	gc_sys_daily_chart_option.legend.selected = selecte;
		                    }
		                 	gc_sys_daily_chart.setOption(gc_sys_daily_chart_option);
		                }
		            },
		        }
		    },
		    grid: {
		        //left: '5px',
		        //right: '5px',
		        bottom: '40px',
		        top:'9px',
		        //containLabel: false,
		    },
		    xAxis: {
				axisLabel: {interval: 0},
		        textStyle: {color:'white'},
		        type: 'category',
		        //boundaryGap: false,
		        data: []
		    },
		    yAxis: {
		        textStyle: {color:'white'},
		        type: 'value',
		        axisLabel:{
		            inside: false,
		        },
		        splitLine:{
		        	show:false,
		        },
		    },

		    color: ['#FFDF70', '#B9F089', '#47CEC8', '#3A95CC', '#3A95FF'],
		    series: [
				{
		            name: 'RUN',
		            type: 'bar',
		            stack: 'total',
		            label: {
		                normal: {
		                    show: false,
		                }
		            },
		            data: ["-"]
		        },
		    ]
		};

		gc_sys_daily_chart.setOption(gc_sys_daily_chart_option);

		
		var hvac_unit_cons_chart = echarts.init(document.getElementById('hvac_unit_cons_chart'), "<%=theme%>");
		var hvac_unit_cons_chart_option = {
		    tooltip: {
		        trigger: 'axis'
		    },
		    legend: {
		        textStyle: {color:'<%=text_color%>'},
		        data: hvac_unit_array,
		        y:'bottom',
		        itemGap: 6,
 				itemWidth: 12,
 				itemHeight: 12,
		    },
		    //legend: [
		    //{
		    //    textStyle: {color:'white', fontSize:'0',lineHeight:'0'},
		    //    data: hvac_unit_array,
		    //    //y:'bottom',
		    //    bottom:'10px',
		    //    //orient: 'vertical',
		    //},
		    //{
			//    textStyle: {color:'< %=text_color%>'},
		    //    data: hvac_unit_array,
		    //    bottom:'0px',
		    //    itemWidth:0,
		    //},],
		    grid: {
		        //left: '60px',
		        //right: '5px',
		        bottom: '40px',
		        top:'9px',
		        //containLabel: false,
		    },
		    xAxis: {
				axisLabel: {interval: 0},
		        textStyle: {color:'white'},
		        type: 'category',
		        //boundaryGap: false,
		        data: []
		    },
		    yAxis: [
		    {
		        textStyle: {color:'white'},
		        type: 'value',
		        position: 'left',
		        offset: 0,
		        axisLabel:{
		            inside: false,
		        },
		        splitLine:{
		        	show:false,
		        },
		    },
		    {
		        textStyle: {color:'white'},
		        type: 'value',
		        position: 'right',
		        offset: 0,
		        axisLabel:{
		            inside: false,
		        },
		        splitLine:{
					show:false,
		        },
		    },
		    {
		    	show:false,
		        textStyle: {color:'white'},
		        type: 'value',
		        position: 'right',
		        axisLabel:{
		            inside: false,
		        },
		        splitLine:{
					show:false,
		        },
		    },
		    ],
		    color: ['<%=power_color%>', '#0080FF'],
		    series: [
				{
		            name: 'RUN',
		            type: 'bar',
		            stack: 'total',
		            label: {
		                normal: {
		                    show: false,
		                }
		            },
		            data: ["-"]
		        },
		    ]
		};

		hvac_unit_cons_chart.setOption(hvac_unit_cons_chart_option);

		
		var water_unit_cons_chart = echarts.init(document.getElementById('water_unit_cons_chart'), "<%=theme%>");
		var water_unit_cons_chart_option = {
		    tooltip: {
		        trigger: 'axis'
		    },
		    legend: {
		        textStyle: {color:'<%=text_color%>'},
		        data: water_unit_array,
		        y:'bottom',
		        itemGap: 6,
 				itemWidth: 12,
 				itemHeight: 12,
		    },
		    grid: {
		        //left: '5px',
		        //right: '5px',
		        bottom: '40px',
		        top:'9px',
		        //containLabel: false,
		    },
		    xAxis: {
				axisLabel: {interval: 0},
		        textStyle: {color:'white'},
		        type: 'category',
		        //boundaryGap: false,
		        data: []
		    },
		    yAxis: [
			    {
			        textStyle: {color:'white'},
			        type: 'value',
			        position: 'left',
			        offset: 0,
			        axisLabel:{
			            inside: false,
			        },
			        splitLine:{
			        　		show:false,
			        },
			    },
			    {
			        textStyle: {color:'white'},
			        type: 'value',
			        position: 'right',
			        offset: 0,
			        axisLabel:{
			            inside: false,
			        },
			        splitLine:{
			        	show:false,
			        },
			    },
			    {
			    	show:false,
			        textStyle: {color:'white'},
			        type: 'value',
			        position: 'right',
			        axisLabel:{
			            inside: false,
			        },
			        splitLine:{
			        	show:false,
			        },
			    },
			],

			color: ['<%=power_color%>', '<%=upw_color%>'],
		    series: [
				{
		            name: 'RUN',
		            type: 'bar',
		            stack: 'total',
		            label: {
		                normal: {
		                    show: false,
		                }
		            },
		            data: ["-"]
		        },
		    ]
		};

		water_unit_cons_chart.setOption(water_unit_cons_chart_option);
		
		
		var gc_unit_cons_chart = echarts.init(document.getElementById('gc_unit_cons_chart'), "<%=theme%>");
		var gc_unit_cons_chart_option = {
		    tooltip: {
		        trigger: 'axis'
		    },
		    legend: [{
		        textStyle: {color:'<%=text_color%>'},
		        data: gc_unit_array,
		        y:'bottom',
		        itemGap: 6,
 				itemWidth: 12,
 				itemHeight: 12,
		    }],
		    grid: {
		        //left: '5px',
		        //right: '5px',
		        bottom: '40px',
		        top:'9px',
		        //containLabel: false,
		    },
		    xAxis: {
				axisLabel: {interval: 0},
		        textStyle: {color:'white'},
		        type: 'category',
		        //boundaryGap: false,
		        data: []
		    },
		    yAxis: [
			    {
			        textStyle: {color:'white'},
			        type: 'value',
			        position: 'left',
			        offset: 0,
			        axisLabel:{
			            inside: false,
			        },
			        splitLine:{
			        	show:false,
			        },
			    },
			    {
			        textStyle: {color:'white'},
			        type: 'value',
			        position: 'right',
			        offset: 0,
			        axisLabel:{
			            inside: false,
			        },
			        splitLine:{
			        	show:false,
			        },
			    },
			    {
			    	show:false,
			        textStyle: {color:'white'},
			        type: 'value',
			        position: 'right',
			        axisLabel:{
			            inside: false,
			        },
			        splitLine:{
			        	show:false,
			        },
			    },
			],

		    color: ['<%=power_color%>', '<%=cda_color%>'],
		    series: [
				{
		            name: 'RUN',
		            type: 'bar',
		            stack: 'total',
		            label: {
		                normal: {
		                    show: false,
		                }
		            },
		            data: ["-"]
		        },
		    ]
		};

		gc_unit_cons_chart.setOption(gc_unit_cons_chart_option);

	</script>
	
	
	<script src='${path}/res/js/my-js/cub_departments_summary.js'></script>
	
	<%=my_laydate_css%>
	
</body>
</html>