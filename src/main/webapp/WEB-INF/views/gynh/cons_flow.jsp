<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/inc/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
    <script src='${path}/res/js/my-js/default_values.js'></script> 
	<!--%@ include file="./common/head.jsp"%-->
	
	<%@ include file="./common/def.jsp" %>
	<meta charset="utf-8">
	<title></title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport"
		content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">	
    <script src="${path}/res/js/my-js/jquery-3.2.1.min.js"></script>
        <script src="${path}/res/js/my-js/app.js"></script>
        <script src="${path}/res/js/my-js/styling.js"></script>
        <script src="${path}/res/js/my-js/bower-libs.min.js"></script>
        <script src="${path}/res/js/my-js/helpers.js"></script>
        <script src="${path}/res/js/my-js/jquery-plus-ui.min.js"></script>
        <script src="${path}/res/js/my-js/jquery-ui-slider-pips.js"></script>
        <script src="${path}/res/js/my-js/modernizr.min.js"></script>
    
    
    <link rel="stylesheet" href="${path}/res/layui/css/layui.css" media="all" >
    <script src="${path}/res/layui/layui.js"></script>
    <script src='${path}/res/js/my-js/util.js'></script> 
    <link rel='stylesheet' type='text/css' href='${path}/res/css/my-css/dx.spa.css' /> 
    <link rel='stylesheet' type='text/css' href='${path}/res/css/my-css/dx.common.css' /> 
    <link rel='stylesheet' type='text/css' href='${path}/res/css/my-css/app.min.css' /> 
   	<link rel='stylesheet' type='text/css' href='${path}/res/css/my-css/default.css' /> 
    <link rel='stylesheet' type='text/css' href='${path}/res/css/my-css/font.css' /> 
    <link rel='stylesheet' type='text/css' href='${path}/res/css/my-css/jqueryui.min.css' /> 
    <link rel='stylesheet' type='text/css' href='${path}/res/css/my-css/jquery-ui-slider-pips.min.css' /> 
                   
    <link rel='dx-theme' data-theme='generic.<%=theme%>' href='${path}/res/css/my-css/dx.<%=theme%>.css' />
    <script src='${path}/res/js/my-js/dx.all.js'></script> 
	<script>
		var init_begin_date = getFormatDate(-7);
		var init_end_date = getFormatDate(-1);
		var current_energy = 'Power';
		var use_echarts = true;
	</script>
	
	<script src='${path}/res/js/my-js/plotly-1.44.1.js'></script>
	
	<script src='${path}/res/js/my-js/cons_flow.js'></script>

    
    <script type="text/javascript">
        var param1 ="${factories_name}";
        var factory_list= ${factory_name};;
        var department_list="";
        var product_line_list="";
        var title = "";
        var pageName = 'cons_flow';
        var path = "${path}";
        var requestpath = "/gynh/cons_flow/get_data.do";
        var links_sum=40;
        var height_times=1;
        var width_times=1
        var max_height_times=1;
        var nodeGap=0;
        var min_level=1;
        var max_level=4;
        var scroll_flag=true;
        window.onload = function() {
        	
        	var nowDate=getNowFormatDate();
			var value=addDate(nowDate,-7)+' - '+addDate(nowDate,-1);
			var str01=param1.split('_');
			if(str01[0]=="CUB"){
				$("#power").hide();
				$("#upw").hide();
				$("#pn2").hide();
				$("#cda").hide();
			}else if(str01[0]=="ARRAY"){
				$("#hvav").hide();
				$("#water").hide();
				$("#gc").hide();
			} 
			document.getElementById('TimeBox').value =value ;
			if(param1!="factories_Power"){
        		select_box();
        		document.getElementById("example-hanzi-labels").style.display="inline";
        		document.getElementById("button_size").style.display="inline";
        		
        	}else{
        		max_level=6;
        		
        	}
        	load_data(init_begin_date, init_end_date);
        	
        	page_custom_resize();
        }
//   		$(window).scroll(function() {
	//        	if (!scroll_flag) {
	//        		var scroll_top =$(document).scrollTop();
	//       		document.getElementById("toobar_div").style.marginTop=scroll_top+'px';
	//        	}
//   		}); 
        function size(){
        	if (scroll_flag) {
        		scroll_flag=false;
        		height_times=max_height_times;
            	nodeGap=10;
        	} else {
        		scroll_flag=true;
        		height_times=1;
            	nodeGap=0;
        	} 
        	page_custom_resize();

        }
   		(function($) {
   		    "use strict";
   		    $(function() {
   		        $.extend( $.ui.slider.prototype.options, { 
   		            animate: "fast",
   		            stop: function() {
   		                var ident = this.id || this.className;
   		            }
   		        });
   		        var hanzi = ["科室", "产线", "设备", "单元"];
   		        $("#hanzi-labels-slider")
   		            .slider({ min: 0, max: hanzi.length-1,range: true, values: [0, 3]})
   		            .slider("pips", {
   		                rest: "label",
   		                labels: hanzi
   		            })
   		            .on("slidechange", function( e, ui ) {
   		            	min_level=4-ui.values[1];
   		            	max_level=4-ui.values[0];
   		            });
   		    });

   		}(jQuery));
   		var cons_flow_ajax_url1;
		var cons_flow_ajax_url2;
		var cons_flow_ajax_url3;
		if(ajax_json){
			cons_flow_ajax_url1=path + '/res/json/cons_flow1.json';
			cons_flow_ajax_url2=path + '/res/json/cons_flow2.json';
			cons_flow_ajax_url3=path + '/res/json/cons_flow3.json';
		}
    </script>
    
    <style>
		html,body{
			height: 100%;
			width: 100%;
			margin: 0;
			padding: 0;
		}
		.button {
			font-family: Arial;
			color: #ffffff;
			font-size: 15px;
			background: #86ce6e;
			border: solid #47b642 1px;
		}
		.button:hover {
			color: #ffffff;
			background: #88a046;
		}
	
	</style>
</head>

<body class='dx-viewport' >
	<div id='toobar_div' style="background:white;position:fixed;width:1600px;top:6px;z-index:10;">
	 	<input id="TimeBox" type="text" placeholder="-"class="layui-input" style="width: 180px; height: 35px;color:#005757; float: left; margin-left: 12px;">
		<div id = "table_toolbar_div" style="height:48px;width:120px; float:left; margin-left:10px;margin-top:6px;margin-right:0px;"></div>
		<div id = 'department_box' style="width:200px;float:left;margin-left:10px;"></div>
		<div id = 'line_box' style="width:200px;float:left;margin-left:20px;"></div>
		<button id="button_size" onclick ="size()"style="width:120px; height: 35px; float: left; margin-left: 12px; font-size: 12px;display:none;">最大化</button>
		<section class="sub-block" id="example-hanzi-labels" style="width:400px;float:left;margin-left:20px;margin-top:0px;height:48px;padding:6px;display:none;">
	        <div class="tabs-content" style="width:400px; float:left; margin-top:0px; height:48px;">
	            <div id="hanzi-labels-slider" ></div>
	        </div>
      	</section>
	</div> 
	<div id='fac_sum_charts_div' style="padding-top:66px;">
		<div id='sankey_chart' style='margin:0;padding:0;color:white; width:900px;height:600px'></div>
	</div>
	
    <!-- for chart -->
	<script src="${path}/res/js/echarts/echarts.min.js"></script>
    <script src='${path}/res/js/my-js/table_toolbar_cons_flow.js'></script>
    <script src='${path}/res/js/my-js/chart.js'></script>
    <script src='${path}/res/js/my-js/worktime_chart.js'></script>

	<script>
		var sankey_chart = echarts.init(document.getElementById('sankey_chart'));
		
		sankey_chart_option = {
	        tooltip: {
	            trigger: 'item',
	            triggerOn: 'mousemove'
	        },
	        series: [
	        	{
	        		top:16,
	        		bottom:12,
	            	label: 
	            	{
	            		fontSize: 10,
	            	},
	            	nodeGap:12,
	            	layoutIterations:32,
	            	nodeWidth: 80,
	                type: 'sankey',
	                layout: 'none',
	                data: [
	                ],
	                links: [
	                ],
	                itemStyle: {
	                    normal: {
	                        borderWidth: 1,
	                        borderColor: '#aaa',
//	                        color: 'red',
	                        opacity:1,
	                    }
	                },
	                lineStyle: {
	                    normal: {
	                        color: energy_colors[current_energy],
	                        curveness: 0.35,
	                        opacity: 0.7,
	                    }
	                }
	            }
	        ]
	    };
	
	</script>
	
	
	
	<%=my_laydate_css%>
	
</body>
</html>