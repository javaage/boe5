<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/inc/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
    <script src='${path}/res/js/my-js/default_values.js'></script> 
    <script src='${path}/res/js/my-js/util.js'></script> 
	<script>
		var defaut_begin_date = getFormatDate(-7);
		var defaut_end_date = getFormatDate(-1);
	</script>
	
	<%@ include file="./common/head.jsp"%>
    <script src='${path}/res/js/my-js/_worktime_data.js'></script> 
    <script src='${path}/res/js/my-js/_CUD_data.js'></script> 
    <script src='${path}/res/js/my-js/worktime.js'></script>
    
	<style>
		td {
			border-top:1px solid #e0e0e0;
			border-right:1px solid #e0e0e0;
			border-bottom:1px solid #e0e0e0;
			border-left:1px solid #e0e0e0;
			<%=background%>;
		}
	</style>
    
    <script type="text/javascript">
        var param1 = "";
        var title = "";
        var pageName = 'cub_cons';
        var path = "${path}";
        var pivotGridId = "#pivot_grid";
        var requestpath = "/gynh/cub_cons/get_data.do";
        var request_count_path = "/gynh/cub_cons/get_count.do";
        var layoutStorageKey = "boe-gynh-pivotgrid-cub_cons";
        var bLoadFromDb = true;
        var filterOutFileds = ["开始时间", "结束时间"];
        var is_zt = false;
        var need_worktime = true;
        var worktime_flag = true;
        var unit_show="power";
        var stateTimeSum;
    	var cda_dataField= null;
        var power_dataField= "c";
        var pn2_dataField= null;
        var upw_dataField= null;
        var time_dataField=null;
        
        chart_style = [{
            name: "POWER",
            type:"bar",
            yAxis:"0",
            color:""  
        }];

        var tableLayout = [
            {
                caption: "日期",
                dataType: "string",
                dataField: "day",
                area: "column",
                headerFilter: {
                    allowSearch: true
                },
            }
	        ,
            {
                caption: "科室",
                dataType: "string",
                dataField: "dp",
                area: "row",
                headerFilter: {
                    allowSearch: true
                }, 
            }
            ,
      		{
	            caption: "系统",
	            dataType: "string",
	            dataField: "system",
	            area: "row",
                headerFilter: {
                    allowSearch: true
                }, 
     		}
            ,
      		{
	            caption: "分厂",
	            dataType: "string",
	            dataField: "to_factory",
	            area: "row",
                headerFilter: {
                    allowSearch: true
                }, 
     		}
	        ,
	        {
	            caption: "POWER",
	            dataField: "c",
	            dataType: "number",
	            summaryType: "sum",
	            format: {
	            	type: "fixedPoint",
	            	precision: 2
	            },
	            area: "data"
			}
		];
    </script>
    
</head>
<body class='dx-viewport' style="overflow:hidden;">
	<jsp:include flush="true" page="./common/shortcut.jsp"/>
	<div id="rightSplitter" style="margin:0px;padding:0px;border:0px;">
		<div class='pivot-container' style="<%=background%>" > 
			<jsp:include flush="true" page="./common/grid_toolbar.jsp"/>
	        <div id='pivot_grid' ></div> 
	        <div id='grid-popup'></div> 
	        <div id='options-popup'></div> 
		</div> 
		<div class='chart-container' style="<%=background%>">
			<div id="echarts_div" style="z-index:1;height:300px;"></div>
			<c:import url="cub_summary_charts.jsp"></c:import>
		</div>
	</div>
	<div class="tip tooltip-content clearfix" style="position: absolute;display: none;z-index:999;">&nbsp; |<br><span class="pointQTY">265</span></div>

    <!-- for chart -->
	<!--script src="${path}/res/js/echarts/echarts-all.js"></script-->
	<script src="${path}/res/js/echarts/echarts.min.js"></script>
    <script src='${path}/res/js/my-js/table_toolbar.js'></script>
    <script src='${path}/res/js/my-js/chart.js'></script>
    <script src='${path}/res/js/my-js/cub_summary_charts.js'></script>

	<script>
		var hvac_sys_daily_chart = echarts.init(document.getElementById('hvac_sys_daily_chart'), "<%=theme%>");
		var hvac_sys_daily_chart_option = {
		    tooltip: {
		        trigger: 'axis'
		    },
		    legend: {
		    	type: 'scroll',
		        textStyle: {color:'<%=text_color%>'},
		        data:hvac_sys_array,
		        y:'bottom',
		    },
		    grid: {
		        //left: '5px',
		        //right: '5px',
		        bottom: '40px',
		        top:'9px',
		        //containLabel: false,
		    },
		    xAxis: {
				axisLabel: {interval: 0},
		        textStyle: {color:'white'},
		        type: 'category',
		        //boundaryGap: false,
		        data: []
		    },
		    yAxis: {
		        textStyle: {color:'white'},
		        type: 'value',
		        axisLabel:{
		            inside: false,
		        },
		        splitLine:{
		        	show:false,
		        },
		    },
		    color: ['#FBBF27', '#FFE26D', '#EEF263', '#BDEB90', '#93E48B', '#4ECBCB', '#50CEEA', '#34A7D8', '#2A84BD'],
		    series: [
				{
		            name: ' ',
		            type: 'bar',
		            stack: 'total',
		            label: {
		                normal: {
		                    show: false,
		                }
		            },
		            data: ["-"]
		        },
		    ]
		};
		hvac_sys_daily_chart.setOption(hvac_sys_daily_chart_option);

		var water_sys_daily_chart = echarts.init(document.getElementById('water_sys_daily_chart'), "<%=theme%>");
		var water_sys_daily_chart_option = {
		    tooltip: {
		        trigger: 'axis'
		    },
		    legend: {
		        textStyle: {color:'<%=text_color%>'},
		        data: water_sys_array,
		        y:'bottom',
		    },
		    grid: {
		        //left: '5px',
		        //right: '5px',
		        bottom: '40px',
		        top:'9px',
		        //containLabel: false,
		    },
		    xAxis: {
				axisLabel: {interval: 0},
		        textStyle: {color:'white'},
		        type: 'category',
		        //boundaryGap: false,
		        data: []
		    },
		    yAxis: {
		        textStyle: {color:'white'},
		        type: 'value',
		        axisLabel:{
		            inside: false,
		        },
		        splitLine:{
		        	show:false,
		        },
		    },
		    color: ['#FFDF70', '#92E48D', '#3FC3CF'],
		    series: [
				{
		            name: 'RUN',
		            type: 'bar',
		            stack: 'total',
		            label: {
		                normal: {
		                    show: false,
		                }
		            },
		            data: ["-"]
		        },
		        {
		            name: 'PM',
		            type: 'bar',
		            stack: 'total',
		            label: {
		                normal: {
		                    show: false,
		                }
		            },
		            data: ["-"]
		        },
		    ]
		};
		water_sys_daily_chart.setOption(water_sys_daily_chart_option);

		var gc_sys_daily_chart = echarts.init(document.getElementById('gc_sys_daily_chart'), "<%=theme%>");
		var gc_sys_daily_chart_option = {
		    tooltip: {
		        trigger: 'axis'
		    },
		    legend: {
		        textStyle: {color:'<%=text_color%>'},
		        data: gc_sys_array,
		        y:'bottom',
		    },
		    grid: {
		        //left: '5px',
		        //right: '5px',
		        bottom: '40px',
		        top:'9px',
		        //containLabel: false,
		    },
		    xAxis: {
				axisLabel: {interval: 0},
		        textStyle: {color:'white'},
		        type: 'category',
		        //boundaryGap: false,
		        data: []
		    },
		    yAxis: {
		        textStyle: {color:'white'},
		        type: 'value',
		        axisLabel:{
		            inside: false,
		        },
		        splitLine:{
		        	show:false,
		        },
		    },

		    color: ['#FFDF70', '#B9F089', '#47CEC8', '#3A95CC', '#3A95FF'],
		    series: [
				{
		            name: 'RUN',
		            type: 'bar',
		            stack: 'total',
		            label: {
		                normal: {
		                    show: false,
		                }
		            },
		            data: ["-"]
		        },
		    ]
		};

		gc_sys_daily_chart.setOption(gc_sys_daily_chart_option);

		
		var hvac_unit_cons_chart = echarts.init(document.getElementById('hvac_unit_cons_chart'), "<%=theme%>");
		var hvac_unit_cons_chart_option = {
		    tooltip: {
		        trigger: 'axis'
		    },
		    legend: {
		        textStyle: {color:'<%=text_color%>'},
		        data: hvac_unit_array,
		        y:'bottom',
		    },
		    //legend: [
		    //{
		    //    textStyle: {color:'white', fontSize:'0',lineHeight:'0'},
		    //    data: hvac_unit_array,
		    //    //y:'bottom',
		    //    bottom:'10px',
		    //    //orient: 'vertical',
		    //},
		    //{
			//    textStyle: {color:'< %=text_color%>'},
		    //    data: hvac_unit_array,
		    //    bottom:'0px',
		    //    itemWidth:0,
		    //},],
		    grid: {
		        //left: '60px',
		        //right: '5px',
		        bottom: '40px',
		        top:'9px',
		        //containLabel: false,
		    },
		    xAxis: {
				axisLabel: {interval: 0},
		        textStyle: {color:'white'},
		        type: 'category',
		        //boundaryGap: false,
		        data: []
		    },
		    yAxis: [
		    {
		        textStyle: {color:'white'},
		        type: 'value',
		        position: 'left',
		        offset: 0,
		        axisLabel:{
		            inside: false,
		        },
		        splitLine:{
		        	show:false,
		        },
		    },
		    {
		        textStyle: {color:'white'},
		        type: 'value',
		        position: 'right',
		        offset: 0,
		        axisLabel:{
		            inside: false,
		        },
		        splitLine:{
					show:false,
		        },
		    },
		    {
		    	show:false,
		        textStyle: {color:'white'},
		        type: 'value',
		        position: 'right',
		        axisLabel:{
		            inside: false,
		        },
		        splitLine:{
					show:false,
		        },
		    },
		    ],
		    color: ['<%=power_color%>', '#0080FF', '#02F78E'],
		    series: [
				{
		            name: 'RUN',
		            type: 'bar',
		            stack: 'total',
		            label: {
		                normal: {
		                    show: false,
		                }
		            },
		            data: ["-"]
		        },
		    ]
		};

		hvac_unit_cons_chart.setOption(hvac_unit_cons_chart_option);

		
		var water_unit_cons_chart = echarts.init(document.getElementById('water_unit_cons_chart'), "<%=theme%>");
		var water_unit_cons_chart_option = {
		    tooltip: {
		        trigger: 'axis'
		    },
		    legend: {
		        textStyle: {color:'<%=text_color%>'},
		        data: water_unit_array,
		        y:'bottom',
		    },
		    grid: {
		        //left: '5px',
		        //right: '5px',
		        bottom: '40px',
		        top:'9px',
		        //containLabel: false,
		    },
		    xAxis: {
				axisLabel: {interval: 0},
		        textStyle: {color:'white'},
		        type: 'category',
		        //boundaryGap: false,
		        data: []
		    },
		    yAxis: [
			    {
			        textStyle: {color:'white'},
			        type: 'value',
			        position: 'left',
			        offset: 0,
			        axisLabel:{
			            inside: false,
			        },
			        splitLine:{
			        　		show:false,
			        },
			    },
			    {
			        textStyle: {color:'white'},
			        type: 'value',
			        position: 'right',
			        offset: 0,
			        axisLabel:{
			            inside: false,
			        },
			        splitLine:{
			        	show:false,
			        },
			    },
			    {
			    	show:false,
			        textStyle: {color:'white'},
			        type: 'value',
			        position: 'right',
			        axisLabel:{
			            inside: false,
			        },
			        splitLine:{
			        	show:false,
			        },
			    },
			],

			color: ['<%=power_color%>', '#0080FF', '#02F78E'],
		    series: [
				{
		            name: 'RUN',
		            type: 'bar',
		            stack: 'total',
		            label: {
		                normal: {
		                    show: false,
		                }
		            },
		            data: ["-"]
		        },
		    ]
		};

		water_unit_cons_chart.setOption(water_unit_cons_chart_option);
		
		
		var gc_unit_cons_chart = echarts.init(document.getElementById('gc_unit_cons_chart'), "<%=theme%>");
		var gc_unit_cons_chart_option = {
		    tooltip: {
		        trigger: 'axis'
		    },
		    legend: {
		        textStyle: {color:'<%=text_color%>'},
		        data: gc_unit_array,
		        y:'bottom',
		    },
		    grid: {
		        //left: '5px',
		        //right: '5px',
		        bottom: '40px',
		        top:'9px',
		        //containLabel: false,
		    },
		    xAxis: {
				axisLabel: {interval: 0},
		        textStyle: {color:'white'},
		        type: 'category',
		        //boundaryGap: false,
		        data: []
		    },
		    yAxis: [
			    {
			        textStyle: {color:'white'},
			        type: 'value',
			        position: 'left',
			        offset: 0,
			        axisLabel:{
			            inside: false,
			        },
			        splitLine:{
			        	show:false,
			        },
			    },
			    {
			        textStyle: {color:'white'},
			        type: 'value',
			        position: 'right',
			        offset: 0,
			        axisLabel:{
			            inside: false,
			        },
			        splitLine:{
			        	show:false,
			        },
			    },
			    {
			    	show:false,
			        textStyle: {color:'white'},
			        type: 'value',
			        position: 'right',
			        axisLabel:{
			            inside: false,
			        },
			        splitLine:{
			        	show:false,
			        },
			    },
			],

		    color: ['<%=power_color%>', '#0080FF', '#02F78E'],
		    series: [
				{
		            name: 'RUN',
		            type: 'bar',
		            stack: 'total',
		            label: {
		                normal: {
		                    show: false,
		                }
		            },
		            data: ["-"]
		        },
		    ]
		};

		gc_unit_cons_chart.setOption(gc_unit_cons_chart_option);

	</script>
	
	
	<%=splitter_style%>

	<%=my_laydate_css%>
	
</body>
</html>