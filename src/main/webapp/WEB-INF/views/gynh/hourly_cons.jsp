<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/inc/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
    <script src='${path}/res/js/my-js/default_values.js'></script> 
    <%@ include file="./common/def.jsp" %>
	<meta charset="utf-8">
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport"
		content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<script type="text/javascript">
		var path = "${path}";
		var pageName="hourly_cons";
	</script>
	<link rel="stylesheet" href="${path}/res/layui/css/layui.css" media="all" >
    <link rel="stylesheet" href="${path}/res/css/public.css" media="all" />

    <script src="${path}/res/js/my-js/jquery-3.2.1.min.js"></script>
    <link rel='stylesheet' type='text/css' href='${path}/res/css/my-css/dx.spa.css' /> 
    <link rel='stylesheet' type='text/css' href='${path}/res/css/my-css/dx.common.css' /> 
    <link rel='dx-theme' data-theme='generic.<%=theme%>' href='${path}/res/css/my-css/dx.<%=theme%>.css' />
    <link rel="stylesheet" href="${path}/res/css/my-css/jqx.base.css" type="text/css" />
        
    <script src="${path}/res/js/my-js/jqxcore.js"></script>
    <script src="${path}/res/js/my-js/jqxsplitter.js"></script>
    <script src='${path}/res/js/my-js/jszip.min.js'></script> 
    <script src='${path}/res/js/my-js/dx.all.js'></script> 
    <script src="${path}/res/js/echarts/echarts.min.js"></script>
  	<script src='${path}/res/js/my-js/util.js'></script> 	
    
    <link rel="stylesheet" type ='text/css' href="${path}/res/css/public-css/css/font-awesome.min.css">
    <link rel="stylesheet" type ='text/css' href="${path}/res/css/public-css/css/jquery.toolbar.css">
	<script src="${path}/res/js/my-js/hourly_cons.js"></script>
    <script src="${path}/res/js/my-js/viewer.js"></script>
    <link rel="stylesheet" type ='text/css' href="${path}/res/css/my-css/viewer.css">
    <script type="text/javascript">
	    var begin_day=getTimeDate(-24).substring(0,10); 
		var end_day=getTimeDate(0).substring(0,10); 
		var begin_hour=getTimeDate(-24).substring(11,13); 
		var end_hour=getTimeDate(0).substring(11,13); 
		var begin_datetime=getTimeDate(-24).substring(0,13)+':00:00';
		var end_datetime=getTimeDate(0).substring(0,13)+':00:00';
		var factory='ARRAY'; 
		var energy='Power';
		var times_num = new Array();
    </script>
    
	<style>
		.tabpanel-item {
		    height: 200px;
		    -webkit-touch-callout: none;
		    -webkit-user-select: none;
		    -khtml-user-select: none;
		    -moz-user-select: none;
		    -ms-user-select: none;
		    user-select: none;
		    padding-left: 25px;
		    padding-top: 55px;
		}
		
		.mobile .tabpanel-item {
		    padding-top: 10px;
		}
		
		.tabpanel-item  > div {
		    float: left;
		    padding: 0 85px 10px 10px
		}
		
		.tabpanel-item  p {
		    font-size: 16px;
		    margin: 0;
		}
		
		.item-box {
		    font-size: 16px;
		    margin: 15px 0 45px 10px;
		}
		
		.options {
		    padding: 20px;
		    background-color: rgba(191, 191, 191, 0.15);
		    margin-top: 20px;
		}
		
		.caption {
		    font-size: 18px;
		    font-weight: 500;
		}

		.option {
		    margin-top: 10px;
		}
		
		.dx-datagrid-header-panel .dx-toolbar {
		    margin-bottom: 4px;
		}
	</style>
	<link rel='stylesheet' type='text/css' href='${path}/res/css/my-css/selected_color.css' /> 
	

</head>

<body class="dx-viewport">
	<div id="rightSplitter" style="margin:0px;padding:0px;border:0px;">
		<div>
			<div style="background:white; float:left; margin-left:12px;margin-top:8px;width:160px;height:37px; position: absolute; z-index: 95;">
				<div id = "table_toolbar_div" style="height:37px;width:120px; float:left; margin-left:20px;margin-right:0px;"></div>
			</div>
			<div class="demo-container" style="margin-left:1px;position: absolute; z-index: 90;">
		        <div id="longtabs" style="position: absolute; z-index: 93;left:240px;top:0px;WIDTH:100%;">
		            <div class="tabs-container"></div>
		        </div>
			    <div id="department_grid" style="position: absolute; z-index: 11;left:0px;top:5px;width:100px;heght:100px;"></div>
			    <div id="eqp_id_grid" style="position: absolute; z-index: 12;left:0px;top:5px;width:100px;heght:100px;"></div>
			    <div id="device_grid" style="position: absolute; z-index: 12;left:0px;top:5px;width:100px;heght:100px;"></div>
			    <div id="device_unit_grid" style="position: absolute; z-index: 12;left:0px;top:5px;width:100px;heght:100px;"></div>
			</div>
		</div>
		<div>
			<table style="margin-left:0px;margin-top:0px;width:100%;">
				<tr><td>
						<div id="echarts_department_state" style="position: absolute; z-index: 11;margin-left:1px;margin-top:0px;width:100%;height:180px;background:white;">
						</div>
						<div id="echarts_eqp_id_state" style="position: absolute; z-index: 12;margin-left:1px;margin-top:0px;width:100%;height:180px;background:white;">
						</div>
						<div id="echarts_device_state" style="position: absolute; z-index: 12;margin-left:1px;margin-top:0px;width:100%;height:180px;background:white;">
						</div>
						<div id="echarts_device_unit_state" style="position: absolute; z-index: 12;margin-left:1px;margin-top:0px;width:100%;height:180px;background:white;">
						</div>
				</td></tr>
				<tr><td>
						<div id="echarts_department" style="position: absolute; z-index: 11;margin-left:1px;margin-top:0px;width:100%;height:180px;background:white;">
						</div>
						<div id="echarts_eqp_id" style="position: absolute; z-index: 12;margin-left:1px;margin-top:0px;width:100%;height:180px;background:white;">
						</div>
						<div id="echarts_device" style="position: absolute; z-index: 12;margin-left:1px;margin-top:0px;width:100%;height:180px;background:white;">
						</div>
						<div id="echarts_device_unit" style="position: absolute; z-index: 12;margin-left:1px;margin-top:0px;width:100%;height:180px;background:white;">
						</div>
				</td></tr>
			</table >
		</div>		
	</div>
    <script src='${path}/res/js/my-js/table_toolbar_hourly_cons.js'></script>
 	<script type="text/javascript">
 	
	 	var echarts_department_state = echarts.init(document.getElementById('echarts_department_state'));
	 	var echarts_eqp_id_state = echarts.init(document.getElementById('echarts_eqp_id_state'));
	 	var echarts_device_state = echarts.init(document.getElementById('echarts_device_state'));
	 	var echarts_device_unit_state = echarts.init(document.getElementById('echarts_device_unit_state'));
	 	
 	 	var echarts_department = echarts.init(document.getElementById('echarts_department'));
 	 	var echarts_eqp_id = echarts.init(document.getElementById('echarts_eqp_id'));
 	 	var echarts_device = echarts.init(document.getElementById('echarts_device'));
 	 	var echarts_device_unit = echarts.init(document.getElementById('echarts_device_unit'));
 	 	
 	 	option_department_state = {
 	 	    /*legend: {
 	 	    	 right:10,
	 				top:0,
	 				itemGap: 16,
	 				itemWidth: 18,
	 				itemHeight: 10,
 	 	        data: legendData,
 	 	        textStyle: {
 	 	            color: 'blue'
 	 	        }
 	 	    },*/
			grid: {
				left: '55px',
				right: '15px',
				bottom: '0px',
				top: '0px',
				padding:'0 0 0 0',
				containLabel: false,
			},
 	 	    tooltip: {
 	 	        show: true,
 	 	        backgroundColor: '#fff',
 	 	        borderColor: '#ddd',
 	 	        borderWidth: 1,
 	 	        textStyle: {
 	 	            color: '#3c3c3c',
 	 	        },
				position: function (point, params, dom, rect, size) {
					return point;
				},
 	 	        formatter: function(p) {
 	 	            return p.seriesName.split('_')[0];
 	 	        },
 	 	    },
 	 	    xAxis: {
 	 	        axisLabel: {
 	 	            show: false,
 	 	        },
 	 	        axisLine: {
 	 	            show: false
 	 	        },
 	 	        axisTick: {
 	 	            show: false
 	 	        },
 	 	        splitLine: {
 	 	            show: false
 	 	        },
 	 	        max: 86400,
 	 	    },
 	 	    yAxis: [{
 	 	        data: [],
				interval: 0,
 	 	        axisLabel: {
 	 	            color: 'blue',
					interval: 0,
					showMinLabel: true,
					showMaxLabel: true,
					fontSize: 10,
					formatter: function(v) {
 	 	                return v;
 	 	            } 	 	        
 	 	    	},
 	 	        axisLine: {
 	 	            show: false
 	 	        },
 	 	        axisTick: {
 	 	            show: false
 	 	        },
 	 	        splitLine: {
 	 	            show: false
 	 	        }
 	 	    }],
 	 	    series: []
 	 	};
 	 	
 	 	option_eqp_id_state = {
 	 	    /*legend: {
 	 	    	 right:10,
	 				top:0,
	 				itemGap: 16,
	 				itemWidth: 18,
	 				itemHeight: 10,
 	 	        data: legendData,
 	 	        textStyle: {
 	 	            color: 'blue'
 	 	        }
 	 	    },*/
			grid: {
				left: '55px',
				right: '15px',
				bottom: '0px',
				top: '0px',
				padding:'0 0 0 0',
				containLabel: false,
			},
 	 	    tooltip: {
 	 	        show: true,
 	 	        backgroundColor: '#fff',
 	 	        borderColor: '#ddd',
 	 	        borderWidth: 1,
 	 	        textStyle: {
 	 	            color: '#3c3c3c',
 	 	        },
				position: function (point, params, dom, rect, size) {
					return point;
				},
 	 	        formatter: function(p) {
 	 	            return p.seriesName.split('_')[0];
 	 	        },
 	 	    },
 	 	    xAxis: {
 	 	        axisLabel: {
 	 	            show: false,
 	 	        },
 	 	        axisLine: {
 	 	            show: false
 	 	        },
 	 	        axisTick: {
 	 	            show: false
 	 	        },
 	 	        splitLine: {
 	 	            show: false
 	 	        },
 	 	        max: 86400,
 	 	    },
 	 	    yAxis: [{
 	 	        data: [],
				interval: 0,
 	 	        axisLabel: {
 	 	            color: 'blue',
					interval: 0,
					showMinLabel: true,
					showMaxLabel: true,
					fontSize: 10,
					formatter: function(v) {
 	 	                return v;
 	 	            } 	 	        
 	 	    	},
 	 	        axisLine: {
 	 	            show: false
 	 	        },
 	 	        axisTick: {
 	 	            show: false
 	 	        },
 	 	        splitLine: {
 	 	            show: false
 	 	        }
 	 	    }],
 	 	    series: []
 	 	};
 	 	
 	 	option_device_state = {
 	 	 	    /*legend: {
 	 	 	    	 right:10,
 		 				top:0,
 		 				itemGap: 16,
 		 				itemWidth: 18,
 		 				itemHeight: 10,
 	 	 	        data: legendData,
 	 	 	        textStyle: {
 	 	 	            color: 'blue'
 	 	 	        }
 	 	 	    },*/
 				grid: {
 					left: '55px',
 					right: '15px',
 					bottom: '0px',
 					top: '0px',
 					padding:'0 0 0 0',
 					containLabel: false,
 				},
 	 	 	    tooltip: {
 	 	 	        show: true,
 	 	 	        backgroundColor: '#fff',
 	 	 	        borderColor: '#ddd',
 	 	 	        borderWidth: 1,
 	 	 	        textStyle: {
 	 	 	            color: '#3c3c3c',
 	 	 	        },
 					position: function (point, params, dom, rect, size) {
 						return point;
 					},
 	 	 	        formatter: function(p) {
 	 	 	            return p.seriesName.split('_')[0];
 	 	 	        },
 	 	 	    },
 	 	 	    xAxis: {
 	 	 	        axisLabel: {
 	 	 	            show: false,
 	 	 	        },
 	 	 	        axisLine: {
 	 	 	            show: false
 	 	 	        },
 	 	 	        axisTick: {
 	 	 	            show: false
 	 	 	        },
 	 	 	        splitLine: {
 	 	 	            show: false
 	 	 	        },
 	 	 	        max: 86400,
 	 	 	    },
 	 	 	    yAxis: [{
 	 	 	        data: [],
 					interval: 0,
 	 	 	        axisLabel: {
 	 	 	            color: 'blue',
 						interval: 0,
 						showMinLabel: true,
 						showMaxLabel: true,
 						fontSize: 10,
 						formatter: function(v) {
 	 	 	                return v;
 	 	 	            } 	 	        
 	 	 	    	},
 	 	 	        axisLine: {
 	 	 	            show: false
 	 	 	        },
 	 	 	        axisTick: {
 	 	 	            show: false
 	 	 	        },
 	 	 	        splitLine: {
 	 	 	            show: false
 	 	 	        }
 	 	 	    }],
 	 	 	    series: []
 	 	 	};
 	 	
 	 	option_device_unit_state = {
 	 	 	    /*legend: {
 	 	 	    	 right:10,
 		 				top:0,
 		 				itemGap: 16,
 		 				itemWidth: 18,
 		 				itemHeight: 10,
 	 	 	        data: legendData,
 	 	 	        textStyle: {
 	 	 	            color: 'blue'
 	 	 	        }
 	 	 	    },*/
 				grid: {
 					left: '55px',
 					right: '15px',
 					bottom: '0px',
 					top: '0px',
 					padding:'0 0 0 0',
 					containLabel: false,
 				},
 	 	 	    tooltip: {
 	 	 	        show: true,
 	 	 	        backgroundColor: '#fff',
 	 	 	        borderColor: '#ddd',
 	 	 	        borderWidth: 1,
 	 	 	        textStyle: {
 	 	 	            color: '#3c3c3c',
 	 	 	        },
 					position: function (point, params, dom, rect, size) {
 						return point;
 					},
 	 	 	        formatter: function(p) {
 	 	 	            return p.seriesName.split('_')[0];
 	 	 	        },
 	 	 	    },
 	 	 	    xAxis: {
 	 	 	        axisLabel: {
 	 	 	            show: false,
 	 	 	        },
 	 	 	        axisLine: {
 	 	 	            show: false
 	 	 	        },
 	 	 	        axisTick: {
 	 	 	            show: false
 	 	 	        },
 	 	 	        splitLine: {
 	 	 	            show: false
 	 	 	        },
 	 	 	        max: 86400,
 	 	 	    },
 	 	 	    yAxis: [{
 	 	 	        data: [],
 					interval: 0,
 	 	 	        axisLabel: {
 	 	 	            color: 'blue',
 						interval: 0,
 						showMinLabel: true,
 						showMaxLabel: true,
 						fontSize: 10,
 						formatter: function(v) {
 	 	 	                return v;
 	 	 	            } 	 	        
 	 	 	    	},
 	 	 	        axisLine: {
 	 	 	            show: false
 	 	 	        },
 	 	 	        axisTick: {
 	 	 	            show: false
 	 	 	        },
 	 	 	        splitLine: {
 	 	 	            show: false
 	 	 	        }
 	 	 	    }],
 	 	 	    series: []
 	 	 	};

 	 	var option_department = {
 	 			tooltip: {
 			    	trigger: 'axis',
 			        axisPointer: {
 			            type: 'cross'
 			        }
 			    },
 	 			grid: {
 					left: '55px',
 					right: '55px',
 					bottom: '30px',
 					top: '15px',
 					padding:'0 0 0 0',
 					containLabel: false,
 	 			},
 	 		    legend: {//图例组件，颜色和名字
 	 		        right:60,
 	 				top:0,
 	 				itemGap: 16,
 	 				itemWidth: 18,
 	 				itemHeight: 10,
 	 		        textStyle: {
 	 					color: '#23243a',
 	 					fontStyle: 'normal',
 	 					fontFamily: '微软雅黑',
 	 					fontSize: 12,            
 	 		        }
 	 		    },
 	 			xAxis: [
 	 				{
 	 					type: 'category',
 	 					boundaryGap: true,//坐标轴两边留白
 	 					data: [],
 	 					axisLabel: { //坐标轴刻度标签的相关设置。
 	 						interval: 0,//设置为 1，表示『隔一个标签显示一个标签』
 	 						margin:15,
 	 						textStyle: {
 	 							color: '#23243a',
 	 							fontStyle: 'normal',
 	 							fontFamily: '微软雅黑',
 	 							fontSize: 12,
 	 						}
 	 					},
 	 					axisTick:{//坐标轴刻度相关设置。
 	 						show: false,
 	 					},
 	 					axisLine:{//坐标轴轴线相关设置
 	 						lineStyle:{
 	 							color:'#23243a',
 	 							opacity:0.8
 	 						}
 	 					},
 	 					splitLine: { //坐标轴在 grid 区域中的分隔线。
 	 						show: false,
 	 					}
 	 				}
 	 			],
 	 			yAxis: [
 	 				{
 	 					type: 'value',
 	 					splitNumber: 5,
 	 					position: 'left',
 	 					axisLabel: {
 	 						textStyle: {
 	 							color: '#23243a',
 	 							fontStyle: 'normal',
 	 							fontFamily: '微软雅黑',
 	 							fontSize: 12,
 	 						}
 	 					},
 	 					axisLine:{
 	 						show: false
 	 					},
 	 					axisTick:{
 	 						show: false
 	 					},
 	 					splitLine: {
 	 						show: true,
 	 						lineStyle: {
 	 							color: ['#23243a'],
 	 							opacity:0.12
 	 						}
 	 					}
 	 				},
 	 				{
 	 					type: 'value',
 	 					splitNumber: 5,
 	 					position: 'right',
 	 					axisLabel: {
 	 						textStyle: {
 	 							color: '#23243a',
 	 							fontStyle: 'normal',
 	 							fontFamily: '微软雅黑',
 	 							fontSize: 12,
 	 						}
 	 					},
 	 					axisLine:{
 	 						show: false
 	 					},
 	 					axisTick:{
 	 						show: false
 	 					},
 	 					splitLine: {
 	 						show: true,
 	 						lineStyle: {
 	 							color: ['#23243a'],
 	 							opacity:0.12
 	 						}
 	 					}
 	 				}
 	 			],
 	 		    series : [
 	 		        {},
 	 		        {}
 	 		    ]
 	 		};
 	 	
 	 	var option_eqp_id = {
 	 			tooltip: {
 			    	trigger: 'axis',
 			        axisPointer: {
 			            type: 'cross'
 			        }
 			    },
 	 			grid: {
 					left: '55px',
 					right: '55px',
 					bottom: '30px',
 					top: '15px',
 					padding:'0 0 0 0',
 					containLabel: false,
 	 			},
 	 		    legend: {//图例组件，颜色和名字
 	 		        right:60,
 	 				top:0,
 	 				itemGap: 16,
 	 				itemWidth: 18,
 	 				itemHeight: 10,
 	 		        textStyle: {
 	 					color: '#23243a',
 	 					fontStyle: 'normal',
 	 					fontFamily: '微软雅黑',
 	 					fontSize: 12,            
 	 		        }
 	 		    },
 	 			xAxis: [
 	 				{
 	 					type: 'category',
 	 					boundaryGap: true,//坐标轴两边留白
 	 					data: [],
 	 					axisLabel: { //坐标轴刻度标签的相关设置。
 	 						interval: 0,//设置为 1，表示『隔一个标签显示一个标签』
 	 						margin:15,
 	 						textStyle: {
 	 							color: '#23243a',
 	 							fontStyle: 'normal',
 	 							fontFamily: '微软雅黑',
 	 							fontSize: 12,
 	 						}
 	 					},
 	 					axisTick:{//坐标轴刻度相关设置。
 	 						show: false,
 	 					},
 	 					axisLine:{//坐标轴轴线相关设置
 	 						lineStyle:{
 	 							color:'#23243a',
 	 							opacity:0.8
 	 						}
 	 					},
 	 					splitLine: { //坐标轴在 grid 区域中的分隔线。
 	 						show: false,
 	 					}
 	 				}
 	 			],
 	 			yAxis: [
 	 				{
 	 					type: 'value',
 	 					splitNumber: 5,
 	 					position: 'left',
 	 					axisLabel: {
 	 						textStyle: {
 	 							color: '#23243a',
 	 							fontStyle: 'normal',
 	 							fontFamily: '微软雅黑',
 	 							fontSize: 12,
 	 						}
 	 					},
 	 					axisLine:{
 	 						show: false
 	 					},
 	 					axisTick:{
 	 						show: false
 	 					},
 	 					splitLine: {
 	 						show: true,
 	 						lineStyle: {
 	 							color: ['#23243a'],
 	 							opacity:0.12
 	 						}
 	 					}
 	 				},
 	 				{
 	 					type: 'value',
 	 					splitNumber: 5,
 	 					position: 'right',
 	 					axisLabel: {
 	 						textStyle: {
 	 							color: '#23243a',
 	 							fontStyle: 'normal',
 	 							fontFamily: '微软雅黑',
 	 							fontSize: 12,
 	 						}
 	 					},
 	 					axisLine:{
 	 						show: false
 	 					},
 	 					axisTick:{
 	 						show: false
 	 					},
 	 					splitLine: {
 	 						show: true,
 	 						lineStyle: {
 	 							color: ['#23243a'],
 	 							opacity:0.12
 	 						}
 	 					}
 	 				}
 	 			],
 	 		    series : [
 	 		        {},
 	 		        {}
 	 		    ]
 	 		};
 	 	
 	 	var option_device = {
 	 			tooltip: {
 			    	trigger: 'axis',
 			        axisPointer: {
 			            type: 'cross'
 			        }
 			    },
 	 			grid: {
 					left: '55px',
 					right: '55px',
 					bottom: '30px',
 					top: '15px',
 					padding:'0 0 0 0',
 					containLabel: false,
 	 			},
 	 		    legend: {//图例组件，颜色和名字
 	 		        right:60,
 	 				top:0,
 	 				itemGap: 16,
 	 				itemWidth: 18,
 	 				itemHeight: 10,
 	 		        textStyle: {
 	 					color: '#23243a',
 	 					fontStyle: 'normal',
 	 					fontFamily: '微软雅黑',
 	 					fontSize: 12,            
 	 		        }
 	 		    },
 	 			xAxis: [
 	 				{
 	 					type: 'category',
 	 					boundaryGap: true,//坐标轴两边留白
 	 					data: [],
 	 					axisLabel: { //坐标轴刻度标签的相关设置。
 	 						interval: 0,//设置为 1，表示『隔一个标签显示一个标签』
 	 						margin:15,
 	 						textStyle: {
 	 							color: '#23243a',
 	 							fontStyle: 'normal',
 	 							fontFamily: '微软雅黑',
 	 							fontSize: 12,
 	 						}
 	 					},
 	 					axisTick:{//坐标轴刻度相关设置。
 	 						show: false,
 	 					},
 	 					axisLine:{//坐标轴轴线相关设置
 	 						lineStyle:{
 	 							color:'#23243a',
 	 							opacity:0.8
 	 						}
 	 					},
 	 					splitLine: { //坐标轴在 grid 区域中的分隔线。
 	 						show: false,
 	 					}
 	 				}
 	 			],
 	 			yAxis: [
 	 				{
 	 					type: 'value',
 	 					splitNumber: 5,
 	 					position: 'left',
 	 					axisLabel: {
 	 						textStyle: {
 	 							color: '#23243a',
 	 							fontStyle: 'normal',
 	 							fontFamily: '微软雅黑',
 	 							fontSize: 12,
 	 						}
 	 					},
 	 					axisLine:{
 	 						show: false
 	 					},
 	 					axisTick:{
 	 						show: false
 	 					},
 	 					splitLine: {
 	 						show: true,
 	 						lineStyle: {
 	 							color: ['#23243a'],
 	 							opacity:0.12
 	 						}
 	 					}
 	 				},
 	 				{
 	 					type: 'value',
 	 					splitNumber: 5,
 	 					position: 'right',
 	 					axisLabel: {
 	 						textStyle: {
 	 							color: '#23243a',
 	 							fontStyle: 'normal',
 	 							fontFamily: '微软雅黑',
 	 							fontSize: 12,
 	 						}
 	 					},
 	 					axisLine:{
 	 						show: false
 	 					},
 	 					axisTick:{
 	 						show: false
 	 					},
 	 					splitLine: {
 	 						show: true,
 	 						lineStyle: {
 	 							color: ['#23243a'],
 	 							opacity:0.12
 	 						}
 	 					}
 	 				}
 	 			],
 	 		    series : [
 	 		        {},
 	 		        {}
 	 		    ]
 	 		};
 	 	
 	 	var option_device_unit = {
 	 			tooltip: {
 			    	trigger: 'axis',
 			        axisPointer: {
 			            type: 'cross'
 			        }
 			    },
 	 			grid: {
 					left: '55px',
 					right: '55px',
 					bottom: '30px',
 					top: '15px',
 					padding:'0 0 0 0',
 					containLabel: false,
 	 			},
 	 		    legend: {//图例组件，颜色和名字
 	 		        right:60,
 	 				top:0,
 	 				itemGap: 16,
 	 				itemWidth: 18,
 	 				itemHeight: 10,
 	 		        textStyle: {
 	 					color: '#23243a',
 	 					fontStyle: 'normal',
 	 					fontFamily: '微软雅黑',
 	 					fontSize: 12,            
 	 		        }
 	 		    },
 	 			xAxis: [
 	 				{
 	 					type: 'category',
 	 					boundaryGap: true,//坐标轴两边留白
 	 					data: [],
 	 					axisLabel: { //坐标轴刻度标签的相关设置。
 	 						interval: 0,//设置为 1，表示『隔一个标签显示一个标签』
 	 						margin:15,
 	 						textStyle: {
 	 							color: '#23243a',
 	 							fontStyle: 'normal',
 	 							fontFamily: '微软雅黑',
 	 							fontSize: 12,
 	 						}
 	 					},
 	 					axisTick:{//坐标轴刻度相关设置。
 	 						show: false,
 	 					},
 	 					axisLine:{//坐标轴轴线相关设置
 	 						lineStyle:{
 	 							color:'#23243a',
 	 							opacity:0.8
 	 						}
 	 					},
 	 					splitLine: { //坐标轴在 grid 区域中的分隔线。
 	 						show: false,
 	 					}
 	 				}
 	 			],
 	 			yAxis: [
 	 				{
 	 					type: 'value',
 	 					splitNumber: 5,
 	 					position: 'left',
 	 					axisLabel: {
 	 						textStyle: {
 	 							color: '#23243a',
 	 							fontStyle: 'normal',
 	 							fontFamily: '微软雅黑',
 	 							fontSize: 12,
 	 						}
 	 					},
 	 					axisLine:{
 	 						show: false
 	 					},
 	 					axisTick:{
 	 						show: false
 	 					},
 	 					splitLine: {
 	 						show: true,
 	 						lineStyle: {
 	 							color: ['#23243a'],
 	 							opacity:0.12
 	 						}
 	 					}
 	 				},
 	 				{
 	 					type: 'value',
 	 					splitNumber: 5,
 	 					position: 'right',
 	 					axisLabel: {
 	 						textStyle: {
 	 							color: '#23243a',
 	 							fontStyle: 'normal',
 	 							fontFamily: '微软雅黑',
 	 							fontSize: 12,
 	 						}
 	 					},
 	 					axisLine:{
 	 						show: false
 	 					},
 	 					axisTick:{
 	 						show: false
 	 					},
 	 					splitLine: {
 	 						show: true,
 	 						lineStyle: {
 	 							color: ['#23243a'],
 	 							opacity:0.12
 	 						}
 	 					}
 	 				}
 	 			],
 	 		    series : [
 	 		        {},
 	 		        {}
 	 		    ]
 	 		};
	</script>
	
</body>
</html>