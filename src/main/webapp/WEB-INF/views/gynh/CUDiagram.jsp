<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
</head>
<body class='dx-viewport'>
	<div class="CUD" style="position:absolute;z-index:-1;display:none;left:50%;top:-1%;width:100%;height:100%;background-color:#333333"></div>
	<div class="CUD" style="z-index:-1;display:none;position:absolute; top:43.5px;left:25px;width:100%;height:100%;background-color:#333333">
		<div style="width:100%;height:100%;" class="dummy dummy-text">

			<div class="layui-row" style="width:100%;height:35%;position:relative; color:white;">
				<div class="layui-col-xs2 rowwidth" style="border-left:1px solid white">
					<div class="grid-demo grid-demo-bg1">
						<div class="firstWordCenter">LD</div>
						<div class="secondWordCenter">上料</div>
						<div id = "LDs">
						</div>
					</div>
				</div>
				<div class="layui-col-xs2 rowwidth">
					<div class="grid-demo">
						<div class="firstWordCenter">Ap</div>
						<div class="secondWordCenter">除静电</div>
						<div id = "Apc">
						</div>
					</div>
				</div>
				<div class="layui-col-xs2 rowwidth">
					<div class="grid-demo grid-demo-bg1">
						<div class="firstWordCenter">RB</div>
						<div class="secondWordCenter">毛刷</div>
						<div id = "RBm">
						</div>
					</div>
				</div>
				<div class="layui-col-xs2 rowwidth">
					<div class="grid-demo">
						<div class="firstWordCenter">Ap</div>
						<div class="secondWordCenter">清洗</div>
						<div id = "Apq">
						</div>
					</div>
				</div>
				<div class="layui-col-xs2 rowwidth">
					<div class="grid-demo grid-demo-bg1 ">
						<div class="firstWordCenter">LD</div>
						<div class="secondWordCenter">风刀</div>
						<div id = "LDf">
						</div>
					</div>
				</div>
				<div class="layui-col-xs2 rowwidth" style="border-right:1px solid white">
					<div class="grid-demo">
						<div class="firstWordCenter">RB</div>
						<div class="secondWordCenter">下料</div>
						<div id = "RBx">
						</div>
					</div>
				</div>
				<button class="layui-btn layui-btn-primary layui-btn-xs allShow" style="right:10%;position: absolute;">全部显示</button>
			</div>
			<div class="layui-row" style="width:10%;height:10%;position:relative;">
				<div class="layui-col-xs2 " style="width:100%;height:100%;border-left:1px solid white;border-right:1px solid white;">
					<div class="grid-demo grid-demo-bg1"></div>
				</div>
			</div>

			<div class="layui-row" style="width:100%;height:35%;position:relative; color:white;">
				<div class="layui-col-xs2 rowwidth" style="border-top:0px;border-left:1px solid white;">
					<div class="grid-demo grid-demo-bg1">
						<div class="firstWordCenter">INDEX</div>
						<div class="secondWordCenter">搬运</div>
						<div id = "INDEX">
						</div>
					</div>
				</div>
				<div class="layui-col-xs2 rowwidth">
					<div class="grid-demo">
						<div class="firstWordCenter">Pos</div>
						<div class="secondWordCenter">翻转</div>
						<div id = "Pos">
						</div>
					</div>
				</div>
				<div class="layui-col-xs2 rowwidth">
					<div class="grid-demo grid-demo-bg1">
						<div class="firstWordCenter">L/UL</div>
						<div class="secondWordCenter">上/下</div>
						<div id = "L">
						</div>
					</div>
				</div>
				<div class="layui-col-xs2 secondRowWidth " style="color:white;">
					<div class="grid-demo">
						<div class="firstWordCenter">H2</div>
						<div class="secondWordCenter">加</div>
						<div id = "H2">
						</div>
					</div>
				</div>
				<div class="layui-col-xs2 secondRowWidth " style="color:white;">
					<div class="grid-demo grid-demo-bg1 ">
						<div class="firstWordCenter">S3</div>
						<div class="secondWordCenter">成膜</div>
						<div id = "S3">
						</div>
					</div>
				</div>
				<div class="layui-col-xs2 secondRowWidth">
					<div class="grid-demo">
						<div class="firstWordCenter">S4</div>
						<div class="secondWordCenter">成膜</div>
						<div id = "S4">
						</div>
					</div>
				</div>
				<div class="layui-col-xs2 secondRowWidth " style="border-right:1px solid white">
					<div class="grid-demo grid-demo-bg1 ">
						<div class="firstWordCenter">ST</div>
						<div class="secondWordCenter">成膜</div>
						<div id = "ST">
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
	
	<style>
		.rowwidth {
			width: 10%;
			height: 100%;
			border-top: 1px white solid;
			border-bottom: 1px white solid;
			border-right: 1px white solid;
		}
		
		.secondRowWidth {
			width: 15%;
			height: 100%;
			border-top: 1px white solid;
			border-bottom: 1px white solid;
			border-right: 1px white solid;
		}
		
		.firstWordCenter {
			text-align: center;
			width: 100%;
			position: absolute;
			left: 50%;
			top: 40%;
			transform: translateX(-50%) translateY(-50%);
		}
		
		.secondWordCenter {
			text-align: center;
			width: 100%;
			position: absolute;
			left: 50%;
			top: 80%;
			transform: translateX(-50%) translateY(-50%);
		}
		.layui-timeline-axis{ background-color:transparent;}
		
	</style>
	<script>
	function loadPoint(){
		for (var i = 0;i<CUD.length;i++) {
			if ($('#'+CUD[i].unit+'')) {
				if (CUD[i].energy == 'Power') {
					$("<div class='"+CUD[i].point_id+" layui-timeline-axis'><span class='layui-badge-dot'></span><font class='CUDValue' style='display:block;margin-top:-20px;margin-left:20px'>"+CUD[i].value+"</font></div>").appendTo($('#'+CUD[i].unit+''));
				} else if (CUD[i].energy == 'CDA') {
					$("<div class='"+CUD[i].point_id+" layui-timeline-axis'><span class='layui-badge-dot layui-bg-orange'></span><font class='CUDValue' style='display:block;margin-top:-20px;margin-left:20px'>"+CUD[i].value+"</font></div>").appendTo($('#'+CUD[i].unit+''));
				} else if (CUD[i].energy == 'PN2') {
					$("<div class='"+CUD[i].point_id+" layui-timeline-axis'><span class='layui-badge-dot layui-bg-green'></span><font class='CUDValue' style='display:block;margin-top:-20px;margin-left:20px'>"+CUD[i].value+"</font></div>").appendTo($('#'+CUD[i].unit+''));
				} else if (CUD[i].energy == 'DIW') {
					$("<div class='"+CUD[i].point_id+" layui-timeline-axis'><span class='layui-badge-dot layui-bg-cyan'></span><font class='CUDValue' style='display:block;margin-top:-20px;margin-left:20px'>"+CUD[i].value+"</font></div>").appendTo($('#'+CUD[i].unit+''));
				}
				$("."+CUD[i].point_id+"").css({
					top: CUD[i].Y,
					left: CUD[i].X
				});
			}
		}
	}
		/* var Text;
		//console.log(mark);
		$(".demo").hover(function(e) {
			console.log(1);
			if(!allShow){
				var x = $(this).offset().left;
				var y = $(this).offset().top;
				y = y + 20;
				$('.tip').css({
					top: y,
					left: x
				});
				$(".tip").show();
				Text = $(this).children().text();
				var data= mark.filter(function(item){
				     return item.point == Text.substring(0,Text.length-1);
				})
				$('.pointQTY').text(data[0].c);
			}
		},function(e) {
			if(!allShow){
				$(".tip").hide();
			}
		}
		); */
		
		var allShow = false;
		$('.allShow').click(function(){
			if (allShow) {
				$(".CUDValue").show();
				allShow = false;
			} else {
				$(".CUDValue").hide();
				allShow = true;
			}
		});
	</script>		
		
</body>
</html>