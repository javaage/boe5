<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/inc/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
    <script src='${path}/res/js/my-js/default_values.js'></script> 
    <%@ include file="./common/def.jsp" %>
	<meta charset="utf-8">
	<title></title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport"
		content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">	
    <link rel="stylesheet" href="${path}/res/layui/css/layui.css" media="all" >
    <link rel="stylesheet" href="${path}/res/css/public.css" media="all" />

    <script src="${path}/res/js/my-js/jquery-3.2.1.min.js"></script>
    <link rel='stylesheet' type='text/css' href='${path}/res/css/my-css/dx.spa.css' /> 
    <link rel='stylesheet' type='text/css' href='${path}/res/css/my-css/dx.common.css' /> 
    <link rel='dx-theme' data-theme='generic.<%=theme%>' href='${path}/res/css/my-css/dx.<%=theme%>.css' />
    <link rel="stylesheet" href="${path}/res/css/my-css/jqx.base.css" type="text/css" />
    
    <script src="${path}/res/js/my-js/jquery.min.js"></script>
    <script src="${path}/res/js/my-js/jqxcore.js"></script>
    <script src="${path}/res/js/my-js/jqxsplitter.js"></script>
    <script src='${path}/res/js/my-js/jszip.min.js'></script> 
    <script src='${path}/res/js/my-js/dx.all.js'></script> 
    <script src="${path}/res/js/echarts/echarts.min.js"></script>
    
    <link rel="stylesheet" type ='text/css' href="${path}/res/css/public-css/css/font-awesome.min.css">
    <link rel="stylesheet" type ='text/css' href="${path}/res/css/public-css/css/jquery.toolbar.css">
    <script src="${path}/res/js/my-js/viewer.js"></script>
    <script src="${path}/res/layui/layui.js"></script>
    <script src='${path}/res/js/my-js/util.js'></script> 	
    <link rel="stylesheet" type ='text/css' href="${path}/res/css/my-css/viewer.css">
    <link rel='stylesheet' type='text/css' href='${path}/res/css/my-css/app.min.css' /> 
    
	<style>
		#search_input{
			width: 99%;
			height: 31px;
			padding-top: 3px;
			margin-top: 3px;
		}
		.itemhide{
			display:none;
		}
	</style>

	<script type="text/javascript">
		var path = "${path}";
		var param1 = "${export_name}";
    	var pageName="export"
		var init_begin_date = getTimeDate(-1);
		var init_end_date = getTimeDate(0);
	</script>
	<script src="${path}/res/js/my-js/export.js"></script>

	<script type="text/javascript">
		layui.use('laydate', function(){
			var laydate = layui.laydate;
			laydate.render({
			    elem: '#beginTimeBox',
			    type: 'datetime',
			    done: function(value) {
					init_begin_date = value;
					//point_value_cache = {};
					//inst_point_value_cache = {};
				}
			  });
			laydate.render({
			    elem: '#endTimeBox',
				type: 'datetime',
			    done: function(value) {
					init_end_date = value;
					//point_value_cache = {};
					//inst_point_value_cache = {};
				}
			  });
		});
		if(ajax_json){
        	export1_ajax_url=path + '/res/json/export1.json';
        	export2_ajax_url=path + '/res/json/export2.json';
        	export3_ajax_url=path + '/res/json/export3.json';
        	export4_ajax_url=path + '/res/json/export4.json';
        }
	</script>
	<style>
		.tabpanel-item {
		    height: 200px;
		    -webkit-touch-callout: none;
		    -webkit-user-select: none;
		    -khtml-user-select: none;
		    -moz-user-select: none;
		    -ms-user-select: none;
		    user-select: none;
		    padding-left: 25px;
		    padding-top: 55px;
		}
		
		.mobile .tabpanel-item {
		    padding-top: 10px;
		}
		
		.tabpanel-item  > div {
		    float: left;
		    padding: 0 85px 10px 10px
		}
		
		.tabpanel-item  p {
		    font-size: 16px;
		    margin: 0;
		}
		
		.item-box {
		    font-size: 16px;
		    margin: 15px 0 45px 10px;
		}
		
		.options {
		    padding: 20px;
		    background-color: rgba(191, 191, 191, 0.15);
		    margin-top: 20px;
		}
		
		.caption {
		    font-size: 18px;
		    font-weight: 500;
		}

		.option {
		    margin-top: 10px;
		}
		
		.dx-datagrid-header-panel .dx-toolbar {
		    margin-bottom: 4px;
		}
		
		.dx-row{
			font-size:16px;
		}
		 .dx-datagrid-content .dx-datagrid-table .dx-row > td, .dx-datagrid-content .dx-datagrid-table .dx-row > tr > td {
		    vertical-align: top;
		   
		    border:2px solid #AAAAAA;
		} 
		 #factory_points_grid{
		    border:3px solid #AAAAAA;
		}
		#factory_inst_points_grid{
		    border:3px solid #AAAAAA;
		} 
		.dx-datagrid-nowrap .dx-header-row > td > .dx-datagrid-text-content {
		    white-space: nowrap;
		    color: #063374;
		}
		.button {
		    height: 33px;
		    width:180px;
		    float:left;
		    margin-left: 12px;
		}
	</style>
	<link rel='stylesheet' type='text/css' href='${path}/res/css/my-css/selected_color.css' /> 
</head>

<body class="dx-viewport">
	<div id="rightSplitter" style="margin:0px;padding:0px;border:0px;">
		<div>
			<div style="background:white; float:left; margin-left:12px;margin-top:10px;width:600px;height:37px; position: absolute; z-index: 95;">
				<input class='toolbar_btn' type="image" title="加载" onclick="get_echarts_value()" src="${path}/res/images/toolbarbtn/curve.png"
        			style=" float:left; width:28px;height:28px; margin-left: 12px;"/>	
				<input class='toolbar_btn' type="image" title="导出" onclick="get_points_value()" src="${path}/res/images/toolbarbtn/export_excel.png"
        			style=" float:left; width:28px;height:28px; margin-left: 12px;"/>
				<input class='toolbar_btn' type="image" title="刷新点位最新值" onclick="get_total_points_value()" src="${path}/res/images/toolbarbtn/refresh.png"
        			style=" float:left; width:28px;height:28px; margin-left: 12px;"/>
        		<button class="button" id="factory_points_button" style="margin-left: 36px;">分厂累计量点位</button>
		 		<button class="button" id="factory_inst_points_button">分厂瞬时量点位</button>
			</div>
			<div class="demo-container" style="float:left;margin-left:1px;position: absolute; z-index: 90;">
			    <div id="factory_points_grid" style="position: absolute; z-index: 12;left:10px;top:50px;width:100px;height:100px;border:3px solid #AAAAAA;"></div>
			    <div id="factory_inst_points_grid" style="position: absolute; z-index: 12;left:10px;top:50px;width:100px;height:100px;border:3px solid #AAAAAA;"></div>	    	    
			</div>
		</div>
		<div>
			<div id="left_div" style="float:left; position: absolute; z-index: 101; margin-left: 14px;width:140px;font-weight: bold;">
				<button class="button" id="begin_hour_add" style="margin-left: 0px; width:32px;">+</button>
		 		<button class="button" id="begin_hour_minus" style="margin-left: 12px; width:32px;">-</button>
	        	<input type="text" class="layui-input" id="beginTimeBox" placeholder="yyyy-MM-dd HH:mm:ss" style="border:2px solid #AAAAAA;padding:2px 5px 2px 5px;">
	      	</div>
	      	<div id="right_div"style="float:left; position: absolute; z-index: 101;width:140px;font-weight: bold;">
	      		<button class="button" id="end_hour_add" style="clear:left;float:right;margin-right:0px; width:32px;">+</button>
		 		<button class="button" id="end_hour_minus" style="clear:left;float:right;margin-right:0px; width:32px;">-</button>
	        	<input type="text" class="layui-input" id="endTimeBox" placeholder="yyyy-MM-dd HH:mm:ss" style="border:2px solid #AAAAAA;padding:2px 5px 2px 5px;">
	      	</div>
			<div id="factory_points_echarts" style="position: absolute; z-index: 11;margin-left:10px;margin-top:12px;width:100%;height:320px;border:3px solid #AAAAAA;background-color:#FFFFFF"></div>
			<div id="factory_inst_points_echarts" style="position: absolute; z-index: 12;margin-left:10px;margin-top:12px;width:100%;height:320px;border:3px solid #AAAAAA;background-color:#FFFFFF"></div>
		</div>	
	</div>
	
 	<script src='${path}/res/js/my-js/table_toolbar_points_value.js'></script>
	<script  type="text/javascript">
   		var factory_points_echarts = echarts.init(document.getElementById('factory_points_echarts'));
   		var factory_inst_points_echarts = echarts.init(document.getElementById('factory_inst_points_echarts'));
   		
   		factory_points_option = {
			legend: {
 		        right:180,
 				top:0,
 				itemGap: 16,
 				itemWidth: 24,
 				itemHeight: 12,
 		        textStyle: {
 					color: '#23243a',
 					fontStyle: 'normal',
 					fontFamily: '微软雅黑',
 					fontSize: 16,            
 		        }
 		    },
 		   	title : {
 		        text: '累积量', //主标题
 		        left: 'center' //标题位置
 		    }, 
 		   	toolbox: {
 		        feature: {
 		            dataZoom: {
 		                xAxisIndex: 'none'
 		            },
 		            restore: {},
 		            saveAsImage: {}
 		        }
 		    },
		    tooltip: {
		    	trigger: 'axis',
		        axisPointer: {
		            type: 'cross'
		        }
		    },
		    grid:{
		       left:180,
		       right:180,
		       bottom:80,
		       
		    },
		     dataZoom: [
		        {
		            type: 'slider',
		            xAxisIndex: 0,
		            height:36,
		            realtime: true,
		        },
		        {
		            type: 'inside',
		            xAxisIndex: 0,
		            realtime: true,
		        }, 
		    ], 
		    xAxis: {
		        type: 'time',
		        splitLine: {
		            show: false
		        }
		    },
		    yAxis: {
		    	type: 'value',
		    	min:'dataMin',
		    	max:'dataMax',
		        splitLine: {
		            show: false
		        },
		        axisLabel: {
 		           	formatter: function (value, index) {
 		           		return value.toFixed(0);
 		        	}
 		        },
		    },
		    series: []
		};
		
   		factory_inst_points_option = {
   				legend: {
   	 		        right:180,
   	 				top:0,
   	 				itemGap: 16,
   	 				itemWidth: 24,
   	 				itemHeight: 12,
   	 		        textStyle: {
   	 					color: '#23243a',
   	 					fontStyle: 'normal',
   	 					fontFamily: '微软雅黑',
   	 					fontSize: 16,            
   	 		        }
   	 		    },
	 		   	title : {
	 		        text: '瞬时量', //主标题
	 		        left: 'center' //标题位置
	 		    },
			    tooltip: {
			    	trigger: 'axis',
			        axisPointer: {
			            type: 'cross'
			        }
			    },
			    grid:{
			       left:180,
			       right:180,
			       bottom:80,
			       
			    },
			    toolbox: {                                           
	 		        feature: {
	 		            dataZoom: {
	 		                xAxisIndex: 'none'
	 		            },
	 		            restore: {},
	 		            saveAsImage: {}
	 		        }
	 		    },
			    dataZoom: [
			        {
			            type: 'slider',
			            xAxisIndex: 0,
			            realtime: true,
			        },
			        {
			            type: 'inside',
			            xAxisIndex: 0,
			            realtime: true,
			        },
			       
			    ],
			    xAxis: {
			        type: 'time',
			        splitLine: {
			            show: false
			        }
			    },
			    yAxis: {
			    	min:'dataMin',
			    	max:'dataMax',
			        type: 'value',
			        splitLine: {
			            show: false
			        },
			        axisLabel: {
	 		           	formatter: function (value, index) {
	 		           		return value.toFixed(0);
	 		        	}
	 		        },
			        
			    },
			    series: []
			};
		factory_points_echarts.setOption(factory_points_option);
		factory_inst_points_echarts.setOption(factory_inst_points_option);

	</script>
</body>
</html>