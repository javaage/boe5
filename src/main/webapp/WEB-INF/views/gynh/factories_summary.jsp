<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/inc/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
    <script src='${path}/res/js/my-js/default_values.js'></script> 
    <script src='${path}/res/js/my-js/util.js'></script> 
	<script>
		var defaut_begin_date = getFormatDate(-7);
		var defaut_end_date = getFormatDate(-1);
	</script>
	
    <%@ include file="./common/def.jsp" %>

    <meta charset="utf-8">
    <title></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <link rel="stylesheet" href="${path}/res/layui/css/layui.css" media="all" >
    <link rel="stylesheet" href="${path}/res/css/public.css" media="all" />

    <script src="${path}/res/js/my-js/jquery-3.2.1.min.js"></script>
        
    <link rel="stylesheet" type ='text/css' href="${path}/res/css/public-css/css/font-awesome.min.css">
	<script src='${path}/res/js/my-js/factories_summary_unit.js'></script>
    <script src="${path}/res/js/my-js/viewer.js"></script>
    <link rel="stylesheet" type ='text/css' href="${path}/res/css/my-css/viewer.css">
 	<script src="${path}/res/layui/layui.js"></script>

    <script type="text/javascript">
        var param1 = "";
        var title = "";
        var pageName = 'factories_summary';
        var path = "${path}";
        var pivotGridId = "#pivot_grid";
        //var requestpath = "/gynh/factories_summary/get_data.do";
        var request_count_path = "/gynh/factories_line_state_cons/get_count.do";
        var layoutStorageKey = "boe-gynh-pivotgrid-factories_summary";
        var bLoadFromDb = true;
        var filterOutFileds = ["开始时间", "结束时间"];
        var is_zt = false;
        var need_worktime = true;
        var worktime_flag = true;
        var unit_show="power_cda_pn2_upw";
        var stateTimeSum;
        var cda_dataField= null;
        var power_dataField= null;
        var pn2_dataField= null;
        var upw_dataField= null;
        var time_dataField=null;
          // init date control
        layui.use('laydate', function(){
        	var laydate = layui.laydate;
        	  //日期范围
        	laydate.render({
        	elem: '#beginTimeBox',
        	range: true,
        	theme: is_dark?'#444444':'',
        	type: 'date',
        	value: defaut_begin_date + ' - ' + defaut_end_date,
        	done: function(value) {
        		defaut_begin_date=value.substr(0,10);
        		defaut_end_date=value.substr(13,10);
        		load_data(defaut_begin_date, defaut_end_date);
        		page_custom_resize();
        		}
        	});
        });
    </script>
    
    <style>
		html,body{
			height: 100%;
			width: 100%;
			margin: 0;
			padding: 0;
			background:white;
			
		}
		
		table {
			background:white;
		}

		.l_border {
			border-left:10px solid #DDE1E4;
		}
		.r_border {
			border-right:10px solid #DDE1E4;
		}
		.t_border {
			border-top:10px solid #DDE1E4;
		}
		.b_border {
			border-bottom:10px solid #DDE1E4;
		}
		.td_bg_color {
			background:#DDE1E4;
		}
		
	</style>
  
</head>
<body class='dx-viewport' style="overflow:hidden;">
	<div style="background:white; float:left; margin-left:12px;margin-top:0px;width:360px;height:37px;">
        <input type="text" class="<%=layui_bg_class%>" id="beginTimeBox" style="float:left; margin-left:0px;margin-top:2px;width:160px;">
        <div id = "table_toolbar_summary_div" style="height:37px;width:50px; float:left; margin-left:10px;margin-right:0px;"></div>
      
        <input class='toolbar_btn' type="image" title="天/小时" onclick="echarts_style()" src="${path}/res/images/toolbarbtn/calendar.png"
        	style=" float:left; width:24px;height:24px;"/>
      
        <jsp:include flush="true" page="./common/factories_summary_grid_toolbar.jsp"/>
        
  	</div>
	<div id='fac_sum_charts_div'>
		<table border="0" width=100% height=100% >
			<tr height=20px id='r0_tr'>
				<td id='r0c0_td' class = ' '>
				
				</td>
				<td id='r0c1_td' class = 'l_border t_border b_border'>
					<div style="color:#222;font-weight:bold;text-align:center;"><%=str_array%></div>
				</td>
				<td id='r0c2_td' class = 'l_border t_border b_border'>
					<div style="color:#222;font-weight:bold;text-align:center;"><%=str_cf%></div>
				</td>
				<td id='r0c3_td' class = 'l_border t_border b_border'>
					<div style="color:#222;font-weight:bold;text-align:center;"><%=str_cell%></div>
				</td>
				<td id='r0c4_td' class = 'l_border t_border b_border'>
					<div style="color:#222;font-weight:bold;text-align:center;">MD</div>
				</td>
				<td id='r0c5_td' width = 1px class = 'l_border t_border b_border td_bg_color'>
				</td>
			</tr>
			<tr>
				<td id='r1c0_td' class = ' '>
					<div style="color:<%=power_color%>;font-weight:bold;text-align:center;"><%=str_power%></div>
				</td>
				<td id = '' class = 'l_border '>
					<div id='array_power_chart' style='margin:0;padding:0;color:white;'></div>
				</td>
				<td id = '' class = 'l_border '>
					<div id='cf_power_chart' style='margin:0;padding:0;color:white;'></div>
				</td>
				<td id = '' class = 'l_border '>
					<div id='cell_power_chart' style='margin:0;padding:0;color:white;'></div>
				</td>
				<td id = '' class = 'l_border '>
					<div id='mdl_power_chart' style='margin:0;padding:0;color:white;'></div>
				</td>
				<td id='' class = 'l_border t_border td_bg_color'>
					 
				</td>
			</tr>
			<tr>
				<td id='r2c0_td' class = ' '>
					<div style="color:<%=upw_color%>;font-weight:bold;text-align:center;"><%=str_upw%></div>
				</td>
				<td id = '' class = 'l_border '>
					<div id='array_upw_chart' style='margin:0;padding:0;color:white;'></div>
				</td>
				<td id = '' class = 'l_border '>
					<div id='cf_upw_chart' style='margin:0;padding:0;color:white;'></div>
				</td>
				<td id = '' class = 'l_border '>
					<div id='cell_upw_chart' style='margin:0;padding:0;color:white;'></div>
				</td>
				<td id = '' class = 'l_border '>
					<div id='mdl_upw_chart' style='margin:0;padding:0;color:white;'></div>
				</td>
				<td id='' class = 'l_border t_border td_bg_color'>
					 
				</td>
			</tr>
			<tr>
				<td id='r3c0_td' class = ' '>
					<div style="color:<%=cda_color%>;font-weight:bold;text-align:center;"><%=str_cda%></div>
				</td>
				<td id = '' class = 'l_border '>
					<div id='array_cda_chart' style='margin:0;padding:0;color:white;'></div>
				</td>
				<td id = '' class = 'l_border '>
					<div id='cf_cda_chart' style='margin:0;padding:0;color:white;'></div>
				</td>
				<td id = '' class = 'l_border '>
					<div id='cell_cda_chart' style='margin:0;padding:0;color:white;'></div>
				</td>
				<td id = '' class = 'l_border '>
					<div id='mdl_cda_chart' style='margin:0;padding:0;color:white;'></div>
				</td>
				<td id='' class = 'l_border t_border td_bg_color'>
					 
				</td>
			</tr>
			<tr>
				<td id='r4c0_td' class = ' '>
					<div style="color:<%=pn2_color%>;font-weight:bold;text-align:center;"><%=str_pn2%></div>
				</td>
				<td id = '' class = 'l_border '>
					<div id='array_pn2_chart' style='margin:0;padding:0;color:white;'></div>
				</td>
				<td id = '' class = 'l_border '>
					<div id='cf_pn2_chart' style='margin:0;padding:0;color:white;'></div>
				</td>
				<td id = '' class = 'l_border '>
					<div id='cell_pn2_chart' style='margin:0;padding:0;color:white;'></div>
				</td>
				<td id = '' class = 'l_border '>
					<div id='mdl_pn2_chart' style='margin:0;padding:0;color:white;'></div>
				</td>
				<td id='' class = 'l_border t_border td_bg_color'>
					 
				</td>
			</tr>
			<tr id='r5_tr'>
				<td id='r5c0_td' class = ' '>
				</td>
				<td id = '' class = 't_border b_border l_border td_bg_color'>
				</td>
				<td id = '' class = 't_border b_border l_border td_bg_color'>
				</td>
				<td id = '' class = 't_border b_border l_border td_bg_color'>
				</td>
				<td id = '' class = 't_border b_border l_border td_bg_color'>
				</td>
				<td id='' class = 't_border b_border l_border td_bg_color'>
				</td>
			</tr>
		</table>
	</div>
	<script src="${path}/res/js/echarts/echarts.min.js"></script>
    <script src='${path}/res/js/my-js/table_toolbar_summary.js'></script>
    <script src='${path}/res/js/my-js/chart.js'></script>
    <script src='${path}/res/js/my-js/worktime_chart.js'></script>
	<script>
		var power_legend = ["Power", '产量', '稼动率', '单耗'];
		var upw_legend   = [str_upw,   '产量', '稼动率', '单耗'];
		var cda_legend   = [str_cda,   '产量', '稼动率', '单耗'];
		var pn2_legend   = [str_pn2,   '产量', '稼动率', '单耗'];
		
		var power_color_array = ['<%=power_color%>', '#66FFCC', '#883333', '#3333CC']
		var upw_color_array   = ['<%=upw_color%>',   '#66FFCC', '#883333', '#3333CC']
		var cda_color_array   = ['<%=cda_color%>',   '#66FFCC', '#883333', '#3333CC']
		var pn2_color_array   = ['<%=pn2_color%>',   '#66FFCC', '#883333', '#3333CC']

		var array_power_chart = echarts.init(document.getElementById('array_power_chart'), "<%=theme%>");
		var array_power_chart_option = {
		    tooltip: {
		        trigger: 'axis',
	        	backgroundColor:'rgba(156,156,156,0.6)',
		    },
		    legend: {
		    	type: 'scroll',
		        textStyle: {color:'<%=text_color%>'},
		        data:power_legend,
		        y:'bottom',
		    },
		    grid: {
		        left: '55px',
		        right: '55px',
		        bottom: '42px',
		        top:'12px',
		        //containLabel: false,
		    },
		    xAxis: {
				axisLabel: {interval: 0},
		        textStyle: {color:'white'},
		        type: 'category',
		        //boundaryGap: false,
		        data: []
		    },
		    yAxis: [
			    {
			        textStyle: {color:'white'},
			        min:function(value) {
			        	var m=value.min-(value.max-value.min)*0.1;
			        	var n=m;
			        	for(var i=0;i<10;i++){
			        		if(n.toPrecision(2)<m){
			        			break;
			        		}else{
			        			n=n*0.95;
			        		}	
			        	}
			        	if (n.toPrecision(2)>0) {
			        		return n.toPrecision(2);
			        	} else {
			        		return 0;
			        	}
			            	
			        },
			        max: function(value) {
			        	var m=value.max+(value.max-value.min)*0.5;
			        	var n=m;
			        	for(var i=0;i<10;i++){
			        		if(n.toPrecision(2)>m){
			        			break;
			        		}else{
			        			n=n*1.06;
			        		}	
			        	}
			            return n.toPrecision(2);
			        }, 
			        type: 'value',
			        position: 'left',
			        offset: 0,
			        axisLabel:{
			            inside: false,
			        },
			        splitLine:{
			        	show:false,
			        },
			    },
			    {
			        textStyle: {color:'white'},
			        type: 'value',
			        min:function(value) {
			        	var m=value.min-(value.max-value.min)*0.1;
			        	var n=m;
			        	for(var i=0;i<10;i++){
			        		if(n.toPrecision(2)<m){
			        			break;
			        		}else{
			        			n=n*0.95;
			        		}	
			        	}
			        	if (n.toPrecision(2)>0) {
			        		return n.toPrecision(2);
			        	} else {
			        		return 0;
			        	}
			        },
			        max: function(value) {
			        	var m=value.max+(value.max-value.min)*0.5;
			        	var n=m;
			        	for(var i=0;i<10;i++){
			        		if(n.toPrecision(2)>m){
			        			break;
			        		}else{
			        			n=n*1.06;
			        		}	
			        	}
			            return n.toPrecision(2);
			        },
			        position: 'right',
			        offset: 0,
			        axisLabel:{
			            inside: false,
			        },
			        splitLine:{ 
						show:false,
			        },
			    },
			    {
			    	show:false,
			        textStyle: {color:'white'},
			        type: 'value',
			        position: 'right',
			        axisLabel:{
			            inside: false,
			        },
			        splitLine:{
			        	show:false,
			        },
			    },
			    {
			    	show:false,
			        textStyle: {color:'white'},
			        type: 'value',
			        position: 'right',
			        axisLabel:{
			            inside: false,
			        },
			        splitLine:{
			        	show:false,
			        },
			    },
			],
		    color: power_color_array,
		    series: [
				{
		            name: ' ',
		            type: 'bar',
		            stack: 'total',
		            label: {
		                normal: {
		                    show: false,
		                }
		            },
		            data: ["-"]
		        },
		    ]
		};
		array_power_chart.setOption(array_power_chart_option);
		var yAxis_max=function(value) {
			var m=value.max+(value.max-value.min)*0.5;
        	var n=m;
        	for(var i=0;i<10;i++){
        		if(n.toPrecision(2)>m){
        			break;
        		}else{
        			n=n*1.06;
        		}	
        	}
            return n.toPrecision(2);
        };
        var yAxis_min=function(value) {
        	var m=value.min-(value.max-value.min)*0.1;
        	var n=m;
        	for(var i=0;i<10;i++){
        		if(n.toPrecision(2)<m){
        			break;
        		}else{
        			n=n*0.95;
        		}	
        	}
        	if (n.toPrecision(2)>0) {
        		return n.toPrecision(2);
        	} else {
        		return 0;
        	}
        };
		
		var cf_power_chart_option = JSON.parse(JSON.stringify(array_power_chart_option));
		cf_power_chart_option.yAxis[0].max=yAxis_max;
		cf_power_chart_option.yAxis[1].max=yAxis_max;
		cf_power_chart_option.yAxis[0].min=yAxis_min;
		cf_power_chart_option.yAxis[1].min=yAxis_min;
		
		var cell_power_chart_option = JSON.parse(JSON.stringify(array_power_chart_option));
		cell_power_chart_option.yAxis[0].max=yAxis_max;
		cell_power_chart_option.yAxis[1].max=yAxis_max;
		cell_power_chart_option.yAxis[0].min=yAxis_min;
		cell_power_chart_option.yAxis[1].min=yAxis_min;

		var mdl_power_chart_option = JSON.parse(JSON.stringify(array_power_chart_option));
		mdl_power_chart_option.yAxis[0].max=yAxis_max;
		mdl_power_chart_option.yAxis[1].max=yAxis_max;
		mdl_power_chart_option.yAxis[0].min=yAxis_min;
		mdl_power_chart_option.yAxis[1].min=yAxis_min;
		
		var array_upw_chart_option = JSON.parse(JSON.stringify(array_power_chart_option));
		array_upw_chart_option.yAxis[0].max=yAxis_max;
		array_upw_chart_option.yAxis[1].max=yAxis_max;
		array_upw_chart_option.yAxis[0].min=yAxis_min;
		array_upw_chart_option.yAxis[1].min=yAxis_min;
		array_upw_chart_option.legend.data = upw_legend;
		array_upw_chart_option.color = upw_color_array;
	
		var cf_upw_chart_option = JSON.parse(JSON.stringify(array_power_chart_option));
		cf_upw_chart_option.yAxis[0].max=yAxis_max;
		cf_upw_chart_option.yAxis[1].max=yAxis_max;
		cf_upw_chart_option.yAxis[0].min=yAxis_min;
		cf_upw_chart_option.yAxis[1].min=yAxis_min;
		cf_upw_chart_option.legend.data = upw_legend;
		cf_upw_chart_option.color = upw_color_array;
		
		var cell_upw_chart_option = JSON.parse(JSON.stringify(array_power_chart_option));
		cell_upw_chart_option.yAxis[0].max=yAxis_max;
		cell_upw_chart_option.yAxis[1].max=yAxis_max;
		cell_upw_chart_option.yAxis[0].min=yAxis_min;
		cell_upw_chart_option.yAxis[1].min=yAxis_min;
		cell_upw_chart_option.legend.data = upw_legend;
		cell_upw_chart_option.color = upw_color_array;
		
		var mdl_upw_chart_option = JSON.parse(JSON.stringify(array_power_chart_option));
		mdl_upw_chart_option.yAxis[0].max=yAxis_max;
		mdl_upw_chart_option.yAxis[1].max=yAxis_max;
		mdl_upw_chart_option.yAxis[0].min=yAxis_min;
		mdl_upw_chart_option.yAxis[1].min=yAxis_min;
		mdl_upw_chart_option.legend.data = upw_legend;
		mdl_upw_chart_option.color = upw_color_array;
		
		

		var array_cda_chart_option = JSON.parse(JSON.stringify(array_power_chart_option));
		array_cda_chart_option.yAxis[0].max=yAxis_max;
		array_cda_chart_option.yAxis[1].max=yAxis_max;
		array_cda_chart_option.yAxis[0].min=yAxis_min;
		array_cda_chart_option.yAxis[1].min=yAxis_min;
		array_cda_chart_option.legend.data = cda_legend; 
		array_cda_chart_option.color = cda_color_array;
		
		var cf_cda_chart_option = JSON.parse(JSON.stringify(array_power_chart_option));
		cf_cda_chart_option.yAxis[0].max=yAxis_max;
		cf_cda_chart_option.yAxis[1].max=yAxis_max;
		cf_cda_chart_option.yAxis[0].min=yAxis_min;
		cf_cda_chart_option.yAxis[1].min=yAxis_min;
		cf_cda_chart_option.legend.data = cda_legend;
		cf_cda_chart_option.color = cda_color_array;
		
		var cell_cda_chart_option = JSON.parse(JSON.stringify(array_power_chart_option));
		cell_cda_chart_option.yAxis[0].max=yAxis_max;
		cell_cda_chart_option.yAxis[1].max=yAxis_max;
		cell_cda_chart_option.yAxis[0].min=yAxis_min;
		cell_cda_chart_option.yAxis[1].min=yAxis_min;
		cell_cda_chart_option.legend.data = cda_legend;
		cell_cda_chart_option.color = cda_color_array;
		
		var mdl_cda_chart_option = JSON.parse(JSON.stringify(array_power_chart_option));
		mdl_cda_chart_option.yAxis[0].max=yAxis_max;
		mdl_cda_chart_option.yAxis[1].max=yAxis_max;
		mdl_cda_chart_option.yAxis[0].min=yAxis_min;
		mdl_cda_chart_option.yAxis[1].min=yAxis_min;
		mdl_cda_chart_option.legend.data = cda_legend;
		mdl_cda_chart_option.color = cda_color_array;

		
		
		var array_pn2_chart_option = JSON.parse(JSON.stringify(array_power_chart_option));
		array_pn2_chart_option.yAxis[0].max=yAxis_max;
		array_pn2_chart_option.yAxis[1].max=yAxis_max;
		array_pn2_chart_option.yAxis[0].min=yAxis_min;
		array_pn2_chart_option.yAxis[1].min=yAxis_min;
		array_pn2_chart_option.legend.data = pn2_legend;
		array_pn2_chart_option.color = pn2_color_array;
		
		var cf_pn2_chart_option = JSON.parse(JSON.stringify(array_power_chart_option));
		cf_pn2_chart_option.yAxis[0].max=yAxis_max;
		cf_pn2_chart_option.yAxis[1].max=yAxis_max;
		cf_pn2_chart_option.yAxis[0].min=yAxis_min;
		cf_pn2_chart_option.yAxis[1].min=yAxis_min;
		cf_pn2_chart_option.legend.data = pn2_legend;
		cf_pn2_chart_option.color = pn2_color_array;
		
		var cell_pn2_chart_option = JSON.parse(JSON.stringify(array_power_chart_option));
		cell_pn2_chart_option.yAxis[0].max=yAxis_max;
		cell_pn2_chart_option.yAxis[1].max=yAxis_max;
		cell_pn2_chart_option.yAxis[0].min=yAxis_min;
		cell_pn2_chart_option.yAxis[1].min=yAxis_min;
		cell_pn2_chart_option.legend.data = pn2_legend;
		cell_pn2_chart_option.color = pn2_color_array;
		
		var mdl_pn2_chart_option = JSON.parse(JSON.stringify(array_power_chart_option));
		mdl_pn2_chart_option.yAxis[0].max=yAxis_max;
		mdl_pn2_chart_option.yAxis[1].max=yAxis_max;
		mdl_pn2_chart_option.yAxis[0].min=yAxis_min;
		mdl_pn2_chart_option.yAxis[1].min=yAxis_min;
		mdl_pn2_chart_option.legend.data = pn2_legend;
		mdl_pn2_chart_option.color = pn2_color_array;
		
		

		var cf_power_chart = echarts.init(document.getElementById('cf_power_chart'), "<%=theme%>");
		cf_power_chart.setOption(cf_power_chart_option);
		var cell_power_chart = echarts.init(document.getElementById('cell_power_chart'), "<%=theme%>");
		cell_power_chart.setOption(cell_power_chart_option);
		var mdl_power_chart = echarts.init(document.getElementById('mdl_power_chart'), "<%=theme%>");
		mdl_power_chart.setOption(mdl_power_chart_option);

		var array_upw_chart = echarts.init(document.getElementById('array_upw_chart'), "<%=theme%>");
		array_upw_chart.setOption(array_upw_chart_option);
		var cf_upw_chart = echarts.init(document.getElementById('cf_upw_chart'), "<%=theme%>");
		cf_upw_chart.setOption(cf_upw_chart_option);
		var cell_upw_chart = echarts.init(document.getElementById('cell_upw_chart'), "<%=theme%>");
		cell_upw_chart.setOption(cell_upw_chart_option);
		var mdl_upw_chart = echarts.init(document.getElementById('mdl_upw_chart'), "<%=theme%>");
		mdl_upw_chart.setOption(mdl_upw_chart_option);

		var array_cda_chart = echarts.init(document.getElementById('array_cda_chart'), "<%=theme%>");
		array_cda_chart.setOption(array_cda_chart_option);
		var cf_cda_chart = echarts.init(document.getElementById('cf_cda_chart'), "<%=theme%>");
		cf_cda_chart.setOption(cf_cda_chart_option);
		var cell_cda_chart = echarts.init(document.getElementById('cell_cda_chart'), "<%=theme%>");
		cell_cda_chart.setOption(cell_cda_chart_option);
		var mdl_cda_chart = echarts.init(document.getElementById('mdl_cda_chart'), "<%=theme%>");
		mdl_cda_chart.setOption(mdl_cda_chart_option);

		var array_pn2_chart = echarts.init(document.getElementById('array_pn2_chart'), "<%=theme%>");
		array_pn2_chart.setOption(array_pn2_chart_option);
		var cf_pn2_chart = echarts.init(document.getElementById('cf_pn2_chart'), "<%=theme%>");
		cf_pn2_chart.setOption(cf_pn2_chart_option);
		var cell_pn2_chart = echarts.init(document.getElementById('cell_pn2_chart'), "<%=theme%>");
		cell_pn2_chart.setOption(cell_pn2_chart_option);
		var mdl_pn2_chart = echarts.init(document.getElementById('mdl_pn2_chart'), "<%=theme%>");
		mdl_pn2_chart.setOption(mdl_pn2_chart_option);
		
	</script>
	
	<script src='${path}/res/js/my-js/factories_summary.js'></script>
	
	<%=my_laydate_css%>
	
</body>
</html>